package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;

public interface Key extends Remote
{
     public Vector getKeyInformation(String SKeyCardNo) throws RemoteException;
     public void insertKeyInformation(Vector VInfo) throws RemoteException;
     public Vector getUserInformation() throws RemoteException;
}
