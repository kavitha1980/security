package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;
import java.sql.*;

public interface VehicleInfo extends Remote
{

        public void updateFuelData(String SVehicleNo,int iStKm,int iFuelKm) throws RemoteException;

        public int getCheckCardStatus(String CardNo) throws RemoteException;

        public void insertData(Vector vect) throws RemoteException;

        public void insertVehicleDetailsData(Vector vect) throws RemoteException;

        public void insertVData(Vector vect) throws RemoteException;

        public Vector getInfo (String CardNo,int i) throws RemoteException;

        public Vector getDriverInfo() throws RemoteException;

        public Vector getVehicleModelInfo(int InDate) throws RemoteException;

        public Vector getPurpose() throws RemoteException;

        public Vector getCurrentInfo(int idate) throws RemoteException;

        public Vector getCurrentInInfo(int idate) throws RemoteException;

        public Vector getSecurityInfo() throws RemoteException;    

        public Vector getPlaces() throws RemoteException;

        public Vector getKm(String  CardNo) throws RemoteException;

        public void updateVehicleKm(String CardNo,int EndKm) throws RemoteException;

        public Vector getVehicleInInfo(String cardNo) throws RemoteException;
     
        public int getVehicleCardNo(String VCardNo,int vdate)  throws RemoteException;
       
        public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int IEndKm , String OutTime,int KDate,int KNo,int KQty) throws RemoteException;
	
        public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int IEndKm , String OutTime) throws RemoteException;
	
        public Vector getVehicleData() throws RemoteException;  

        public Vector getVehicleNames() throws RemoteException;

        public ResultSet getVehicleReport(String QS) throws RemoteException;

        public ResultSet getVehicleFuelReport(String QS) throws RemoteException;
        
}
 
                                                          
