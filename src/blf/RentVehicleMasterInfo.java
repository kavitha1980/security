package blf;

import java.util.*;
import java.rmi.*;
import util.*;

public interface RentVehicleMasterInfo extends Remote
{

     public String getMaxOwnerCode() throws RemoteException;

     public void insertOwnerName(String name,String Code) throws RemoteException;

     public Vector getOwnerInfo() throws RemoteException;

     public Vector getPurpose() throws RemoteException;

     public String getMaxRVId() throws RemoteException;

     public void insertRentVehicle(Vector InfoVector) throws RemoteException;

     public Vector getRVInfo(String SOwnerCode) throws RemoteException;


}


