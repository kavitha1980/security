package blf;

import util.*;
import java.util.*;
import java.rmi.*;

public interface VisitorIn extends Remote
{

     public void setMessageText(String str,int id) throws RemoteException;

     public int getSlNo(String seq) throws RemoteException;
     public Vector getVisitorReport(int StDate,int EndDate) throws RemoteException;
     public Vector getVisitorCheckReport(int StDate,int EndDate) throws RemoteException;
     public Vector getVisitorReport(int StDate,int EndDate,String SPurpose) throws RemoteException;
     public Vector getVisitorReport(int iSlipNo,int StDate,int EndDate) throws RemoteException;
     public Vector getVisitorReport1(int iSlipNo,int StDate,int EndDate) throws RemoteException;


     public Vector getErrectorReport(int iSlipNo,int StDate,int EndDate,String sNewVersion) throws RemoteException;
     public Vector getErrectorReport1(int iSlipNo,int StDate,int EndDate,String sNewVersion) throws RemoteException;
     public Vector getErrectorReport(int StDate,int EndDate) throws RemoteException;
     public Vector getErrectorCheckReport(int StDate,int EndDate) throws RemoteException;
     public Vector getVisitorCompanyInfo() throws RemoteException;
     public Vector getErrectorCompanyInfo() throws RemoteException;
     public Vector getVisitorAbstractReport(int StDate,int EndDate,int CompanyCode,int RepCode) throws RemoteException;
     public Vector getVisitorRepInfo(int CompanyCode) throws RemoteException;
     public int getNextId() throws RemoteException;
     public boolean isCardAlreadyExists(String SCardNo) throws RemoteException;

     public Vector getErrectorSlipNo(int iStDate,int iEndDate) throws RemoteException;
     public Vector getErrectorAsOnReport(int EndDate) throws RemoteException;
     public Vector getErrectorAsOnReport1(int EndDate) throws RemoteException;
     public Vector getErrectorInfo(String SCompanyCode) throws RemoteException;
     public Vector getErrectorWiseReport(int StDate,int EndDate,int ErrectorCode) throws RemoteException;

     public Vector getVisitorSlipNo(int iStDate,int iEndDate) throws RemoteException;
     public Vector getVisitorInInfo(int StDate,int EndDate) throws RemoteException;
     public Vector getVisitorOutInfo(int StDate,int EndDate) throws RemoteException;
     public Vector getErrectorInInfo(int StDate,int EndDate) throws RemoteException;
     public Vector getErrectorOutInfo(int StDate,int EndDate) throws RemoteException;

     public void insertCompanyName(String name,String Code) throws RemoteException;
     public void insertVisitorPurpose(String name,String Code) throws RemoteException;
     public void insertRep(String name,String Code) throws RemoteException;
     public void insertReg(String name,String Code) throws RemoteException;
     public void insertDriver(String name,String Code) throws RemoteException;
     public void insertStaff(int Code,String Name) throws RemoteException;
     public void insertDept(int Code,String Name) throws RemoteException;

     public String getMaxCode() throws RemoteException;
     public String getMaxPurposeCode() throws RemoteException;
     public String getMaxRepCode() throws RemoteException;
     public String getMaxRegCode() throws RemoteException;
     public String getMaxDriverCode() throws RemoteException;

     public String getMaxStaffCode() throws RemoteException;
     public String getMaxDeptCode() throws RemoteException;
     public String getMaxCompanyCode() throws RemoteException;

     //public String getPurposeCode() throws RemoteException;

     public Vector getCompanyInfo(int iVisitorType) throws RemoteException;
     public Vector getPurposeInfo() throws RemoteException;
     public Vector getRepInfo(int CCode) throws RemoteException;
	 public Vector getRepInfo(String SCode) throws RemoteException;
     public Vector getVehInfo(int CCode) throws RemoteException;
     public Vector getDriverInfo(int CCode) throws RemoteException;
     public Vector getStaffInfo() throws RemoteException;
     public Vector getDeptInfo() throws RemoteException;

     public int getDeptCode(int iStaffCode) throws RemoteException;
     public int getHodCode(int iDeptCode) throws RemoteException;
     public Vector sendMessageSource(int id,RowSet rowset) throws RemoteException;
     public void sendMessageTarget(RowSet rowset) throws RemoteException;
     public String getHostId(int iDeptCode) throws RemoteException;

     public void saveData(String slipNo,Vector vect) throws RemoteException;
     public void saveErrectorData(String slipNo,Vector vect) throws RemoteException;

     public String getNextSlipNo(int iIndicator) throws RemoteException;
     public int getSlipNo(String cardno) throws RemoteException;
     public int getErrectorSlipNo(String cardno) throws RemoteException;
     public Vector getVect(int slipno) throws RemoteException;
     public Vector getErrectorVect(int slipno) throws RemoteException;
     public void setOutTime(String STime,int RepCode) throws RemoteException;
     public void setErrectorOutTime(String STime,int RepCode) throws RemoteException;

     //public Vector getFinanceYear() throws RemoteException;

     public String getVisitorInitialCheck(String QS) throws RemoteException;

}
