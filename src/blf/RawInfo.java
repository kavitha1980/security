package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;

public interface RawInfo extends Remote
{
        public void insertInwardData(RowSet rowset) throws RemoteException;
        public void updateInwardData(RowSet rowset,int i) throws RemoteException;
}
 
                                                          
