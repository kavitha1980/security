package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;
import java.sql.*;

public interface RentVehicleInfo extends Remote
{

        public Vector getPurpose() throws RemoteException;

        public Vector getPlaces() throws RemoteException;

        public int getRVCheckCardAvailable(String CardNo) throws RemoteException;

        public int getRVCheckCardStatus(String CardNo) throws RemoteException;

        public Vector getRentVehicleInfo (String CardNo) throws RemoteException;

        public void insertRVData(Vector vect) throws RemoteException;

        public Vector getRVCurrentInfo(int idate) throws RemoteException;

        public String getMaxRVTripNo(int iId) throws RemoteException;

        public Vector getRVVehicleOutInfo(String SCardNo) throws RemoteException;

        public Vector getRVActiveID() throws RemoteException;

        public Vector getRVCurrentOutInfo(Vector VActiveId) throws RemoteException;

        public void getRVVehicleUpdate(String SCardNo,String SOutTime,int iOutDate,int iEndKm,int iRunKm,String STripNo,String SId,int iRentStatus,String SLoad) throws RemoteException;
        
}
 
                                                          
