package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;

public interface Info extends Remote
{
     public void getGuestInsertData(String QS) throws RemoteException;
     public Vector getGuestNames() throws RemoteException;
     public Vector getGuestInitValues(int Date) throws RemoteException;
     public Vector getGuestValues(String slno) throws RemoteException;
     public int getGuestUpdateSlipNo(String CardNo) throws RemoteException;
     public void setGuestUpdateTime(int slno,String OutTime,int OutDate,String InTime) throws RemoteException;
     public int getGuestSlipNo() throws RemoteException;
     public Vector getToMeetCode() throws RemoteException;
     public Vector getGuestInInitValues(int Date) throws RemoteException;
     public int getInterviewCard(String sl) throws RemoteException;
     public int getCheckCard(String CardNo,int date)  throws RemoteException;
     public int getInterviewInitialCheck(String QS) throws RemoteException;

     public Vector getGuestName() throws RemoteException;

     public int getSlipNo() throws RemoteException;
     public Vector getCategory() throws RemoteException;
//     public Vector getHostelCode() throws RemoteException;
     public Vector getAgent() throws RemoteException;
     public Vector getHod() throws RemoteException;
     public Vector getQuali() throws RemoteException;
     public Vector getCategoryCode() throws RemoteException;
     public Vector getAgentCode() throws RemoteException;
     public Vector getHodCode() throws RemoteException;
     public Vector getQualiCode() throws RemoteException;
     public int getId() throws RemoteException;
     public int setCode(int code,Vector vect) throws RemoteException;
     public void insertValues(Vector infoVector) throws RemoteException;
     public Vector initValues(int Date) throws RemoteException;
     public Vector initValues(int iFromDate,int iToDate) throws RemoteException;
     public Vector getValues(String slno) throws RemoteException;
     public Vector getNames() throws RemoteException;
     public Vector getEmployeeNames() throws RemoteException;
     public void getUpdateTimeAndDate(String OutTime,int OutDate,int id,int iResult,String SHod) throws RemoteException;
     public Vector getOutValues(int iDate) throws RemoteException;
     public Vector getInterviewValues(int iDate1,int iDate2) throws RemoteException;
     public int setId(String sl) throws RemoteException;
     public Vector getReportVector(int iStDate,int iEndDate) throws RemoteException;
     public Vector getAsOnReportVector(int iStDate,int iEndDate) throws RemoteException;
     public Vector getInsideInterviewReportVector(int iStDate,int iEndDate) throws RemoteException;
     public Vector getReportCheckVector(int iStDate,int iEndDate) throws RemoteException;
     public Vector getReportVector1(int iStDate,int iEndDate,String SResult,String SCatogory,String SAgent,String SPlace,String SQualification ) throws RemoteException; 
     //public Vector getOutInfo(int cardNo,int Date) throws Exception; 
     public int getGuestInitialCheck(String Card) throws RemoteException;
//     public int getMaxFoodId() throws RemoteException;
     public Vector getWarden() throws RemoteException; 
}
