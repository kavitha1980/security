
import java.util.*;
import java.rmi.*;
//import util.RowSet;

public interface Stores extends Remote
{
     public void setUpdateStoresData(String no,String column) throws RemoteException;
     public int getNo(String QS) throws RemoteException;
     public Vector getInvName() throws RemoteException;
     public Vector getInvCode() throws RemoteException;
     public Vector getPendingNames(String QS) throws RemoteException;
     public Vector getSupplierData(String QS) throws RemoteException;
     public Vector getListVector(String QS) throws RemoteException;
     public Vector getRDCData(String QS) throws RemoteException;
     public Vector getRDCOutData(String QS) throws RemoteException;
     public void insertGISDetails(String QS) throws RemoteException;
     public void insertGIDSDetails(String QS) throws RemoteException;
     public void insertRDCInData(String QS) throws RemoteException;
     public void updateRDC(String QS)  throws RemoteException;

}
