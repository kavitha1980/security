/*
          Business Logic Foundation - VitalData

          The Collection of master data

          see domain/jdbc/vital/DataBean for how to do

          author : Rajasubramaniam.K
*/

package blf;

import java.rmi.*;
import java.util.*;

public interface vital extends Remote
{
     /*
          To return a collection of values
          corresponding to the Attribute
     */
     public Vector getData(String SAttribute) throws RemoteException;
}
