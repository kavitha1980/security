package blf;

import java.util.*;
import java.rmi.*;
import util.RowSet;
import java.sql.*;

public interface ThabalInfo extends Remote
{
      public Vector getThabalDespMode() throws RemoteException;

      public Vector getThabalType() throws RemoteException;

      public Vector getDistinctCourier(String SDespModeCode,int iDate) throws RemoteException;

      public Vector getThabalSummary(Vector VDespCode,Vector VTypeCode,int iDate) throws RemoteException;

      public void getUpdateThabalStatus(String SDespCode,int iDate) throws RemoteException;

      public Vector getCourierDetails() throws RemoteException;

      public Vector getMessengerDetails() throws RemoteException;

      public Vector getGateThabalInward() throws RemoteException;

      public String getMaxThabalGateInward() throws RemoteException;

      public void insertThabalGateInward(Vector VValues) throws RemoteException;

      public String getThabalGateInwardStatus(String SNo) throws RemoteException;

      public void getUpdateOldThabalGI(String SNo) throws RemoteException;

}
 
                                                          
