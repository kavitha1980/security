package guiutil;
import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import util.*;
import guiutil.*;
public class SerialReaderNew implements SerialPortEventListener,Runnable
{
     FractionNumberField TLValue1,TAValue1,TBValue1;
     FractionNumberField TLValue2,TAValue2,TBValue2;
     FractionNumberField TLValue3,TAValue3,TBValue3;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;

     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     int    ctr=0;
     boolean heartAttack = false;
     StringBuffer inputBuffer = new StringBuffer();
     String Str="";
     String StrL="",StrA="",StrB="";

     int iLCount=0;

     public SerialReaderNew(FractionNumberField TLValue1,FractionNumberField TAValue1,FractionNumberField TBValue1,FractionNumberField TLValue2,FractionNumberField TAValue2,FractionNumberField TBValue2,FractionNumberField TLValue3,FractionNumberField TAValue3,FractionNumberField TBValue3)
     {
          this.TLValue1 = TLValue1;
          this.TAValue1 = TAValue1;
          this.TBValue1 = TBValue1;
          this.TLValue2 = TLValue2;
          this.TAValue2 = TAValue2;
          this.TBValue2 = TBValue2;
          this.TLValue3 = TLValue3;
          this.TAValue3 = TAValue3;
          this.TBValue3 = TBValue3;

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          ctr=0;
          iLCount=0;

          TLValue1.setText("");
          TAValue1.setText("");
          TBValue1.setText("");
          TLValue2.setText("");
          TAValue2.setText("");
          TBValue2.setText("");
          TLValue3.setText("");
          TAValue3.setText("");
          TBValue3.setText("");

          System.out.println("Port:"+serialPort);
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               System.out.println("Port"+portList.hasMoreElements());
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();

                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         System.out.println("PortId:"+portId.getName());
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent     .DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                         while (newData != -1)
                         {
                              newData = inputStream.read();

                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                         }

                         Str = new String(inputBuffer);

                         StrL="";
                         StrA="";
                         StrB="";

                         int iIndex = Str.indexOf("Online");
                         //System.out.println("ON Index:"+iIndex);

                         if(iIndex>=0)
                         {

                              int iIndex1 = Str.indexOf("L*");
                              int iIndex2 = Str.indexOf("a*");
                              int iIndex3 = Str.indexOf("b*");

                              //System.out.println("Index:"+iIndex1+":"+iIndex2+":"+iIndex3);

                              StrL = Str.substring(iIndex1+4,iIndex2-1);
                              StrA = Str.substring(iIndex2+4,iIndex3-1);
                              StrB = Str.substring(iIndex3+4,iIndex3+12);

                              //System.out.println("Str-L:"+StrL+":"+StrA+":"+StrB);

                              heartAttack = true;
                         }
                         else
                         {
                              heartAttack = false;
                              Str = "";
                              freeze();
                              createComponents();
                         }

                         //System.out.println("Final:"+Str);
                         getWeight();
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     public void getWeight()
     {
          try
          {
               if(heartAttack)
               {
                      setWeight();
                      heartAttack = false;
                      inputBuffer = new StringBuffer();
               }
               else
               {
                    freeze();
                    createComponents();
               }
          }
          catch(Exception ex)
          {}
     }


     private void setWeight()
     {
          try
          {
               iLCount++;

               //System.out.println("Values:"+StrL+":"+StrA+":"+StrB);

               StrL = checkNegativeValue(StrL);
               StrA = checkNegativeValue(StrA);
               StrB = checkNegativeValue(StrB);

               if(iLCount==1)
               {
                    TLValue1.setText(common.getRound(common.toDouble(StrL.trim()),3));
                    TAValue1.setText(common.getRound(common.toDouble(StrA.trim()),3));
                    TBValue1.setText(common.getRound(common.toDouble(StrB.trim()),3));
               }
               else
               if(iLCount==2)
               {
                    TLValue2.setText(common.getRound(common.toDouble(StrL.trim()),3));
                    TAValue2.setText(common.getRound(common.toDouble(StrA.trim()),3));
                    TBValue2.setText(common.getRound(common.toDouble(StrB.trim()),3));
               }
               else
               {
                    TLValue3.setText(common.getRound(common.toDouble(StrL.trim()),3));
                    TAValue3.setText(common.getRound(common.toDouble(StrA.trim()),3));
                    TBValue3.setText(common.getRound(common.toDouble(StrB.trim()),3));
                    iLCount=0;
               }
          }
          catch(Exception ex)
          {}
     }
     public String checkNegativeValue(String Strx)
     {
          Strx = Strx.trim();

          if(Strx.startsWith("-"))
          {
             Strx = "-"+(Strx.substring(1,Strx.length())).trim();
          }
          return Strx;
     }

     public void freeze()
     {
          serialPort.close();
     }


}
