
package utility;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import util.*;

public class StatusPanel extends JPanel
{
     JLabel Label1,Label3,Label4;
     JPanel Panel1,Panel2,Panel3,Panel4;
     Common common = new Common();

     public StatusPanel()
     {
          Panel1 = new JPanel(true);
          Panel2 = new JPanel(true);
          Panel3 = new JPanel(true);
          Panel4 = new JPanel(true);

          Panel1.setBorder(new TitledBorder("Financial Year"));
          Panel2.setBorder(new TitledBorder("Status"));
          Panel3.setBorder(new TitledBorder("Author"));
          Panel4.setBorder(new TitledBorder("For Amarjothi"));

          Label1 = new JLabel(common.getFinancePeriod(),JLabel.LEFT);
          Label3 = new JLabel("Rajasubramaniam.K",JLabel.LEFT);
          Label4 = new JLabel(common.getUser(),JLabel.LEFT);

          Panel1.setLayout(new BorderLayout());
          Panel2.setLayout(new BorderLayout());
          Panel3.setLayout(new FlowLayout());
          Panel4.setLayout(new BorderLayout());

          setLayout(new GridLayout(1,4));

          add(Panel1);
          add(Panel2);
          add(Panel3);
          add(Panel4);

          Panel1.add(Label1);
          Panel3.add(Label3);
          Panel4.add(Label4);
          updateUI();
     }
}
