package utility;
import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class NoNumberField extends JTextField {

	int iColumns = 30;
     double myDouble = 0.0;
     boolean allowSpace=false;

     public NoNumberField()
	{
		super();
          addKeyListener(new KeyList());
	}

    public NoNumberField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
        addKeyListener(new KeyList());
    }

    public NoNumberField(int columns) {
        super(columns);
		iColumns = columns;
          addKeyListener(new KeyList());
    }

    public NoNumberField(int columns,boolean allowSpace) {
          super(columns);
          this.allowSpace=allowSpace;
		iColumns = columns;
          addKeyListener(new KeyList());
    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

               if (str == null || getText(0, getLength()).length() == iColumns)
               {
                    return;
               }

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {
               if(allowSpace)
     
                     if (Character.isLetter(source[i]) || Character.isDigit(source[i]) || source[i] == '-' || source[i] == ' ')
                     {
                          result[j++] = Character.toUpperCase(source[i]);
                     }
                     else
                     {
                         Toolkit.getDefaultToolkit().beep();
                     }
               else
     
               {
                     if (Character.isLetter(source[i]) || Character.isDigit(source[i]) || source[i] == '-' )
                     {
                          result[j++] = Character.toUpperCase(source[i]);
                     }
                     else
                     {
                         Toolkit.getDefaultToolkit().beep();
                     }
                }

            }
            super.insertString(offs, new String(result, 0, j), a);
        }
    }

     public class KeyList implements KeyListener
     {
          int i=0;
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}

          public void keyReleased(KeyEvent ke)
          {
               int myLength = getText().trim().length();
               if (myLength==0)
                    {
                         setText("");
                         return;
                    }
               try
               {
                    myDouble = Double.parseDouble(getText());
               }
               catch(NumberFormatException nfe)
               {
                    //setText("");
               }

               if(!(myDouble==0))
               {
                     setText("");
                     myDouble = 0.0;
               }
          }
     }
}
