//package javafiles;

package utility;

import java.awt.*;
import java.io.*;
import javax.swing.text.*;
import javax.swing.*;

class Notepad extends JPanel
{
    JTextComponent editor; 
    public Notepad(File f,StatusPanel SPanel)
    {
          super(true);

          editor = createEditor();
          editor.setFont(new Font("monospaced", Font.PLAIN, 12));
          JScrollPane scroller = new JScrollPane();
          JViewport port = scroller.getViewport();
          port.add(editor);
          setLayout(new BorderLayout());
          add("Center", scroller);
          setNotePad(f,SPanel);
    }

    protected JTextComponent createEditor()
    {
          return new JTextArea();
    }

    protected JTextComponent getEditor()
    {
          return editor;
    }

    public void setNotePad(File f,StatusPanel SPanel)
    {
          Document oldDoc = getEditor().getDocument();
		getEditor().setDocument(new PlainDocument());
          Thread loader = new FileLoader(f, editor.getDocument(),SPanel);
		loader.start();
    }
}
