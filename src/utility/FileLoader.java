//package javafiles;

package utility;

import java.io.*;
import javax.swing.text.*;
import javax.swing.*;
public class FileLoader extends Thread
{
         File f;
         Document doc;
         StatusPanel SPanel;
         FileLoader(File f, Document doc,StatusPanel SPanel)
         {
               setPriority(4);
               this.f      = f;
               this.doc    = doc;
               this.SPanel = SPanel;
         }

        public void run()
        {
             try
             {
                    JProgressBar progress = new JProgressBar();
                    progress.setMinimum(0);
                    progress.setMaximum((int) f.length());
                    SPanel.Panel2.add(progress);

                    // try to start reading
                    Reader in = new FileReader(f);
                    char[] buff = new char[4096];
                    int nch;
                    while ((nch = in.read(buff, 0, buff.length)) != -1)
                    {
                        doc.insertString(doc.getLength(), new String(buff, 0, nch), null);
                        progress.setValue(progress.getValue() + nch);
                    }
                    SPanel.Panel2.removeAll();
             }
             catch (IOException e)
             {
                     System.err.println(e.toString());
             }
             catch (BadLocationException e)
             {     
                     System.err.println(e.getMessage());
             }
             File f;
             Document doc;
      }

}

