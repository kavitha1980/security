package client;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import client.guest.*;

public class GuestAuth
{
     AcceptDeny AD;

     JDesktopPane   DeskTop;


     protected      JLayeredPane Layer;
     int iUserCode;

     GuestInFrameNew       guestinframenew;
     GuestInFramePrint     guestinprint;
     GuestStatusFrame      gueststatus;
     GuestStatusFrameNew   gueststatusnew;
     GuestStatusTest       gueststatustest;

     int  iCode;
     public GuestAuth()
     {
          try
          {
               AD = new AcceptDeny();
               AD.setVisible(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
          try
          {
               AD.show();

               AD.BAccept.addActionListener(new ADList());
               AD.BDeny  .addActionListener(new ADList());

          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public GuestAuth(JLayeredPane Layer,int iCode)
     {
          this.Layer     = Layer;
          this.iCode     = iCode;

          System.out.println("iCode"+iCode);

          try
          {
               AD = new AcceptDeny();
               AD.setVisible(false);
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
          try
          {
               AD.show();

               AD.BAccept.addActionListener(new ADList());
               AD.BDeny  .addActionListener(new ADList());
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
     public static void main (String args[])
     {
          new GuestAuth();
     }
     public class ADList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(AD.BDeny == ae.getSource())
               {
                    AD.setVisible(false);
                    System.exit(0);
               }
               if(ae.getSource()==AD.BAccept)
               {
                    if(AD.isPassValid() ==true)
                    {
                         AD.setVisible(false);
                         startRePrint();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Enter a Valid PassWord","Information",JOptionPane.INFORMATION_MESSAGE);
                         AD.TA.setText("");
                         AD.TA.requestFocus();
                    }
               }
          }
          
     }
     public void startRePrint()
     {
          if(AD.isPassValid())
          {
               if(iCode==1)
               {           
                    try
                    {
                         if(guestinframenew!=null)
                         {
                              Layer.remove(guestinframenew);
                              Layer.updateUI();
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    try
                    {
                         guestinframenew = new GuestInFrameNew(Layer,AD.iUserCode);
                         Layer     . add(guestinframenew);
                         Layer     . repaint();
                         guestinframenew  . setSelected(true);
                         Layer     . updateUI();
                         guestinframenew.show();
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    
               }
               if(iCode==2)
               {
                    try
                    {
                         if(guestinprint!=null)
                         {
                              Layer.remove(guestinprint);
                              Layer.updateUI();
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    try
                    {
                         guestinprint = new GuestInFramePrint(Layer,AD.iUserCode);
                         Layer   . add(guestinprint);
                         guestinprint.setSelected(true);
                         Layer   . repaint();
                         Layer   . updateUI();
                         guestinprint.show();
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }

               if(iCode==3)
               {
                    try
                    {
                         if(gueststatus!=null)
                         {
                              Layer.remove(gueststatus);
                              Layer.updateUI();
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    try
                    {
                         gueststatus = new GuestStatusFrame(Layer,AD.iUserCode);
                         Layer   . add(gueststatus);
                         gueststatus.setSelected(true);
                         Layer   . repaint();
                         Layer   . updateUI();
                         gueststatus.show();
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }
               if(iCode==4)
               {
                    try
                    {
                         if(gueststatusnew!=null)
                         {
                              Layer.remove(gueststatusnew);
                              Layer.updateUI();
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    try
                    {
                         gueststatusnew = new GuestStatusFrameNew(Layer,AD.iUserCode);
                         Layer   . add(gueststatusnew);
                         gueststatusnew.setSelected(true);
                         Layer   . repaint();
                         Layer   . updateUI();
                         gueststatusnew.show();
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }
               if(iCode==5)
               {
                    try
                    {
                         if(gueststatustest!=null)
                         {
                              Layer.remove(gueststatustest);
                              Layer.updateUI();
                         }
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
                    try
                    {
                         gueststatustest = new GuestStatusTest(Layer,AD.iUserCode);
                         Layer   . add(gueststatustest);
                         gueststatustest.setSelected(true);
                         Layer   . repaint();
                         Layer   . updateUI();
                         gueststatustest.show();
                    }
                    catch(Exception ex)
                    {
                         ex.printStackTrace();
                    }
               }


          }
     }

}
