
 /*

     To acquire details of Thabal(s) inward


*/

package client.thabal;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.util.GregorianCalendar;

import utility.*;
import utility.Control;

import util.TimeField;
import util.WholeNumberField;
import util.ClockField;
import util.DateField1;
import util.MyComboBox;
import util.MyLabel;
import util.Common;

import java.sql.*;
import blf.*;

import java.rmi.*;
import java.rmi.registry.*;

public class ThabalInFrame extends JInternalFrame implements rndi.CodedNames
{

     protected JLayeredPane Layer;

     JPanel      TopPanel,BottomPanel;

     DateField1       TDate;  
     MyComboBox       JCMode;
     MyComboBox       JCCourier;  
     WholeNumberField TLocal;
     WholeNumberField TOther;
     WholeNumberField TInter;
     WholeNumberField TParcel;
     WholeNumberField TTotal;

     TabReport   tabreport;

     JButton BSave,BUpdate,BCancel,BExit;


     Vector VSlNo,VGIDate,VMode,VName,VCode,VLocal,VOther,VInter,VParcel,VTotal;


     Vector VDespModeCode,VDespModeName;
     Vector VCourierCode,VCourierName,VMessengerCode,VMessengerName;

     Vector VValues;

     Common common = new Common();
     TimeField time = new TimeField();
     String SDate="";
     int iDate=0;
     String SNo="";
     String SSlNo="";

     Object        RowData[][];
     String ColumnData[]  = {"Sl.No.","In Date","Mode","Name","Local","Other States","International","Parcel","Total"};
     String ColumnType[]  = {"S"     ,"S"      ,"S"   ,"S"   ,"S"    ,"S"           ,"S"            ,"S"     ,"S"};

     ThabalInfo thabalDomain;

     public ThabalInFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;

          setDomain();
          setData();  
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setCourier(0);
     }

     private void setDomain()
     {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               thabalDomain        =(ThabalInfo)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }

     private void createComponents()
     {

          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();

          TDate          = new DateField1();

          JCMode         = new MyComboBox(VDespModeName);
          JCCourier      = new MyComboBox();
          TLocal         = new WholeNumberField(3);
          TOther         = new WholeNumberField(3);
          TInter         = new WholeNumberField(3);
          TParcel        = new WholeNumberField(3);
          TTotal         = new WholeNumberField(4);

          BSave          = new JButton("Save");
          BUpdate        = new JButton("Update");
          BCancel        = new JButton("Cancel");
          BExit          = new JButton("Exit");

          VValues        = new Vector();

          BSave.setMnemonic('S');
          BUpdate.setMnemonic('U');
          BCancel.setMnemonic('C');
          BExit.setMnemonic('E');
          BUpdate.setEnabled(false);


     }

     private void setLayouts()
     {

          setTitle("Thabal Inward");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,600,500);

          TopPanel   .setLayout(new GridLayout(8,2,20,5));
          BottomPanel.setLayout(new FlowLayout());

          TopPanel   .setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {

          TopPanel.add(new MyLabel("Date"));
          TopPanel.add(TDate);
          TopPanel.add(new MyLabel("Select Mode of Despatch"));
          TopPanel.add(JCMode);
          TopPanel.add(new MyLabel("Select Name"));
          TopPanel.add(JCCourier);
          TopPanel.add(new MyLabel("Local Thabals"));
          TopPanel.add(TLocal);
          TopPanel.add(new MyLabel("Other State Thabals"));
          TopPanel.add(TOther);
          TopPanel.add(new MyLabel("International Thabals"));
          TopPanel.add(TInter);
          TopPanel.add(new MyLabel("Parcel"));
          TopPanel.add(TParcel);
          TopPanel.add(new MyLabel("Total No. of Thabals"));
          TopPanel.add(TTotal);

          BottomPanel.add(BSave);
          BottomPanel.add(BUpdate);
          BottomPanel.add(BCancel);
          BottomPanel.add(BExit);

          getContentPane().add("North" ,TopPanel);
          getContentPane().add("South" ,BottomPanel);

          TDate.setTodayDate();
          TDate.setEditable(false);
          TTotal.setEditable(false);

          JCMode.requestFocus();
          setTabReport();
     }

     private void addListeners()
     {
          JCMode.addItemListener(new ItemList());
          TLocal.addKeyListener(new TotalList());
          TOther.addKeyListener(new TotalList());
          TInter.addKeyListener(new TotalList());
          TParcel.addKeyListener(new TotalList());

          BUpdate.addActionListener(new UpdateList());
          BSave.addActionListener(new ActList());
          BCancel.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private class ItemList implements ItemListener
     {
          public void itemStateChanged(ItemEvent a)
          {
               try
               {

                    int index = JCMode.getSelectedIndex();

                    setCourier(index);

               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }


          }
     }
     private void setCourier(int index)
     {
          if(index==0)
          {
               JCCourier.removeAllItems();
               for(int i=0;i<VCourierName.size();i++)
               {
                    JCCourier.addItem(VCourierName.elementAt(i));
               }

          }
          else
          {

               JCCourier.removeAllItems();
               for(int i=0;i<VMessengerName.size();i++)
               {
                    JCCourier.addItem(VMessengerName.elementAt(i));
               }

          }

     }

     private class TotalList implements KeyListener
     {
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}
          public void keyReleased(KeyEvent ke)
          {
               try
               {
                    setTotal();
               }
               catch(NumberFormatException nfe)
               {
                    TTotal.setText("");
               }
          }
     }
     private void setTotal()
     {

          String STotal = String.valueOf(common.toInt(TLocal.getText())+common.toInt(TOther.getText())+common.toInt(TInter.getText())+common.toInt(TParcel.getText()));
          TTotal.setText(STotal);

     }

     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    try
                    {
                        int iRow = tabreport.ReportTable.getSelectedRow();
                        SNo = (String)tabreport.ReportTable.getValueAt(iRow,0);
                        if(!isActive(SNo))
                              return;
                        setData(SNo,iRow);
                        BUpdate.setEnabled(true);
                        BSave.setEnabled(false);
                    }
                    catch(Exception ex)
                    {
                        System.out.println(ex);
                    }
               }

          }
     }
     private boolean isActive(String SNo)
     {

          try
          {
               String SStatus= thabalDomain.getThabalGateInwardStatus(SNo);

               if(common.toInt(SStatus)==1)
               {
                    JOptionPane.showMessageDialog(null,"This is already received","Error",JOptionPane.ERROR_MESSAGE);
                    return false;
               }

               return true;
          }
          catch(Exception e)
          {
               return false;
          }

     }
     
     private void setData(String SNo,int iRow)
     {
          try
          {

               String SGIDate = (String)tabreport.ReportTable.getValueAt(iRow,1);
               String SMode   = (String)tabreport.ReportTable.getValueAt(iRow,2);
               String SDesp   = (String)tabreport.ReportTable.getValueAt(iRow,3);
               String SLocal  = (String)tabreport.ReportTable.getValueAt(iRow,4);
               String SOther  = (String)tabreport.ReportTable.getValueAt(iRow,5);
               String SInter  = (String)tabreport.ReportTable.getValueAt(iRow,6);
               String SParcel = (String)tabreport.ReportTable.getValueAt(iRow,7);
               String STotal  = (String)tabreport.ReportTable.getValueAt(iRow,8);

               TDate.setString(SGIDate);
               JCMode.setSelectedItem(SMode);
               JCCourier.setSelectedItem(SDesp);
               TLocal.setText(SLocal);
               TOther.setText(SOther);
               TInter.setText(SInter);
               TParcel.setText(SParcel);
               TTotal.setText(STotal);
               JCMode.requestFocus();

          }
          catch(Exception ex)
          {
                //System.out.println("setTabReport : "+ex.printStackTrace());
                ex.printStackTrace();
          }
     }

     private class UpdateList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {

             String STitle = "Update";
             boolean ok = checkFields(STitle);
             if(ok)
             {
                     try
                     {
                          BUpdate.setEnabled(false);
                          thabalDomain.getUpdateOldThabalGI(SNo);
                          insertValues(VValues);
                          VValues.removeAllElements();
                          BExit.requestFocus();
                     }
                     catch(Exception ex)
                     {
                          ex.printStackTrace();
                     }
             }
             else
             {
                    JOptionPane.showMessageDialog(null,"Any one of the field Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                    TLocal.requestFocus();
             }
         }
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                  if(ae.getSource()==BSave)
                  {
                          String STitle = "Save";
                          BUpdate.setEnabled(false);
                          boolean ok = checkFields(STitle);
                          if(ok)
                          {
                                  insertValues(VValues);
                                  VValues.removeAllElements();
                                  BExit.requestFocus();
                          }
                          else
                          {
                                 JOptionPane.showMessageDialog(null,"Any one of the field Must be filled ","Error",JOptionPane.ERROR_MESSAGE);
                                 TLocal.requestFocus();
                          }
                  }
      
                  if(ae.getSource()==BCancel)
                  {
                          clearFields();
                  }                                
                  if(ae.getSource()==BExit)
                  {
                          setVisible(false);
                  }
          }
     }


     private boolean checkFields(String STitle)
     {

          boolean flag = true;
          try
          {
      
               if(common.toInt(TTotal.getText())==0)
               {
                     flag=false;
                     return flag;
               }
               String SDespatcherCode="";

               if(STitle.equals("Save"))
               {
                     String SMaxSlNo  = thabalDomain.getMaxThabalGateInward();
                     SSlNo     = String.valueOf(common.toInt(SMaxSlNo)+1);
               }
               else
               {
                     SSlNo = SNo;
               }

               SDate = TDate.toNormal();
               iDate = common.toInt(SDate);
               String SInDate   = String.valueOf(iDate);

               String SDespMode = (String)VDespModeCode.elementAt(JCMode.getSelectedIndex());

               if(JCMode.getSelectedIndex()==0)
               {
                    SDespatcherCode  = (String)VCourierCode.elementAt(JCCourier.getSelectedIndex());
               }
               else
               {
                    SDespatcherCode  = (String)VMessengerCode.elementAt(JCCourier.getSelectedIndex());
               }

               String SLocal    = String.valueOf(common.toInt(TLocal.getText()));
               String SOther    = String.valueOf(common.toInt(TOther.getText()));
               String SInter    = String.valueOf(common.toInt(TInter.getText()));
               String SParcel   = String.valueOf(common.toInt(TParcel.getText()));
               String STotal    = String.valueOf(common.toInt(TTotal.getText()));

               String SUserCode ="";
               String SUserTime = time.getTimeNow();

               VValues.addElement(SSlNo);
               VValues.addElement(SInDate);
               VValues.addElement(SDespMode);
               VValues.addElement(SDespatcherCode);
               VValues.addElement(SLocal);
               VValues.addElement(SOther);
               VValues.addElement(SInter);
               VValues.addElement(SParcel);
               VValues.addElement(STotal);
               VValues.addElement(common.parseNull(SUserCode));
               VValues.addElement(SUserTime);

          }
          catch(Exception e)
          {                                 
                  flag=false;
                  JOptionPane.showMessageDialog(null,"Incorrect Values");

          }
          return flag;

     }
     public void insertValues(Vector VValues)
     {
          try
          {
                  thabalDomain.insertThabalGateInward(VValues);
                  clearFields();
                  setTabReport();
          }
          catch(Exception e)        
          {
                  e.printStackTrace();
          }
     }

     private void clearFields()
     {
          JCMode.setSelectedIndex(0);
          JCCourier.setSelectedIndex(0);
          TDate.setTodayDate();
          TLocal.setText("");
          TOther.setText("");
          TInter.setText("");
          TParcel.setText("");
          TTotal.setText("");
          JCMode.requestFocus();
          BUpdate.setEnabled(false);
          BSave.setEnabled(true);
     }



     private void setTabReport()
     {
            setDataIntoVector();
            setRowData();
            try
            {
                  getContentPane().remove(tabreport);
            }
            catch(Exception ex){}
            try
            {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  tabreport.setBorder(new TitledBorder(""));
                  for(int i=0;i<ColumnData.length;i++)
                  {
                      tabreport.setHeaderColor(ColumnData[i]);
                  }

                  tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

                  getContentPane().add(tabreport,BorderLayout.CENTER);

                  setSelected(true);
                  Layer.repaint();
                  Layer.updateUI();
                  tabreport.requestFocus();
                  tabreport.ReportTable.addKeyListener(new KeyList());
                  BSave.setEnabled(true);
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }

     }


     private void setDataIntoVector()
     {

          try
          {
                  SDate = TDate.toNormal();
                  iDate = common.toInt(SDate);
      
                  VSlNo          = new Vector();
                  VGIDate        = new Vector();
                  VMode          = new Vector();
                  VName          = new Vector();
                  VLocal         = new Vector();
                  VOther         = new Vector();
                  VInter         = new Vector();
                  VParcel        = new Vector();
                  VTotal         = new Vector();
                  VCode          = new Vector();
      
      
                  Vector V1         = new Vector();
      
                  V1 = thabalDomain.getGateThabalInward();

                  int m=0;
      
                  for(int i=0;i<V1.size()/10;i++)
                  {
                        VMode.addElement((String)V1.elementAt(0+m));
                        VName.addElement((String)V1.elementAt(1+m));
                        VLocal.addElement((String)V1.elementAt(2+m));
                        VOther.addElement((String)V1.elementAt(3+m));
                        VInter.addElement((String)V1.elementAt(4+m));
                        VParcel.addElement((String)V1.elementAt(5+m));
                        VTotal.addElement((String)V1.elementAt(6+m));
                        VSlNo.addElement((String)V1.elementAt(7+m));
                        VCode.addElement((String)V1.elementAt(8+m));
                        VGIDate.addElement((String)V1.elementAt(9+m));
                        m=m+10;
                  }

            }
            catch(Exception ex)
            {

                   try
                   {
                         PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Raj.err")));
                         ex.printStackTrace(out);
                         out.close();
                   }
                   catch(Exception e){}

                  //ex.printStackTrace();
          }


     }

     public void setRowData()
     {
            RowData     = new Object[VMode.size()][ColumnData.length];
            for(int i=0;i<VMode.size();i++)
            {
                  String SMode   = (String)VMode.elementAt(i);

                  RowData[i][0]  = (String)VSlNo.elementAt(i);
                  RowData[i][1]  = (String)VGIDate.elementAt(i);
                  RowData[i][2]  = (String)VMode.elementAt(i);
                  RowData[i][3]  = (String)VName.elementAt(i);
                  RowData[i][4]  = (String)VLocal.elementAt(i);
                  RowData[i][5]  = (String)VOther.elementAt(i);
                  RowData[i][6]  = (String)VInter.elementAt(i);
                  RowData[i][7]  = (String)VParcel.elementAt(i);
                  RowData[i][8]  = (String)VTotal.elementAt(i);
            }  
     }


     private void setData()
     {
          try
          {
               VDespModeCode     = new Vector();
               VDespModeName     = new Vector();
               VCourierCode      = new Vector();
               VCourierName      = new Vector();
               VMessengerCode    = new Vector();
               VMessengerName    = new Vector();


               Vector V1         = new Vector();
               V1                = thabalDomain.getThabalDespMode();
      
               int m=0;
      
               for(int i=0;i<V1.size()/2;i++)
               {
                    VDespModeCode.addElement((String)V1.elementAt(0+m));
                    VDespModeName.addElement((String)V1.elementAt(1+m));
                    m=m+2;
               }

               Vector V2         = new Vector();
               V2                = thabalDomain.getCourierDetails();

               int n=0;
      
               for(int i=0;i<V2.size()/2;i++)
               {
                    VCourierCode.addElement((String)V2.elementAt(0+n));
                    VCourierName.addElement((String)V2.elementAt(1+n));
                    n=n+2;
               }

               Vector V3         = new Vector();
               V3                = thabalDomain.getMessengerDetails();
      
               int k=0;
      
               for(int i=0;i<V3.size()/2;i++)
               {
                    VMessengerCode.addElement((String)V3.elementAt(0+k));
                    VMessengerName.addElement((String)V3.elementAt(1+k));
                    k=k+2;
               }


          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }


}


