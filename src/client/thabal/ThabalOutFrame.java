                
 /*

     To acquire details of Thabal(s) outward


*/

package client.thabal;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.util.GregorianCalendar;

import utility.*;
import utility.Control;

import util.TimeField;
import util.ClockField;
import util.DateField1;
import util.MyComboBox;
import util.Common;

import java.sql.*;
import blf.*;

import java.rmi.*;
import java.rmi.registry.*;

public class ThabalOutFrame extends JInternalFrame implements rndi.CodedNames
{

      protected JLayeredPane Layer;

      JPanel        TopPanel,BottomPanel;
      JPanel        DatePanel,CourierPanel,ApplyPanel;

      TabReport     tabreport;

      DateField1    TDate;
      MyComboBox    JCCourier;
      JButton       BApply;

      Object        RowData[][];
      String ColumnData[] = {"Despatcher Name","Local","Other State","International","Parcel","Total","Despatch Mark"};
      String ColumnType[] = {"S"              ,"N"    ,"N"          ,"N"            ,"N"     ,"N"    ,"B"};

      Vector VDespName,VDespCode,VLocal,VOther,VInter,VParcel,VTotal;

      Vector VDespModeName,VDespModeCode;

      TimeField      theTime;
      String SDate="";
      int iDate=0;

      MyButton       BOkay;
      JButton        BExit; 

      Control        control;
      Common         common;

      ThabalInfo thabalDomain;

      public ThabalOutFrame(JLayeredPane Layer)
      {
            this.Layer = Layer;
            setDomain();
            setData();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      private void setDomain()
      {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               thabalDomain        =(ThabalInfo)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private void createComponents()
      {
            TopPanel       = new JPanel();
            BottomPanel    = new JPanel();
            DatePanel      = new JPanel();
            CourierPanel   = new JPanel();
            ApplyPanel     = new JPanel();

            TDate          = new DateField1();
            JCCourier      = new MyComboBox(VDespModeName);
            BApply         = new JButton("Apply");

            theTime        = new TimeField();

            BExit          = new JButton("Exit");
            BOkay          = new MyButton("Okay");

            common         = new Common();
            control        = new Control();

            BOkay.setMnemonic('O');
            BExit.setMnemonic('X');
            BApply.setMnemonic('A');

            TDate.setTodayDate();
            TDate.setEditable(false);

            BOkay.setEnabled(false);

      }

      private void setLayouts()
      {

            setTitle("Thabal Outward");
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,650,500);

            TopPanel.setLayout(new GridLayout(1,3));
            DatePanel.setLayout(new GridLayout(1,2));
            CourierPanel.setLayout(new GridLayout(1,2));
            ApplyPanel.setLayout(new BorderLayout());
          
      }


      private void addComponents()
      {
            TopPanel.add(DatePanel);
            TopPanel.add(CourierPanel);
            TopPanel.add(ApplyPanel);

            DatePanel.add(new JLabel("Date"));
            DatePanel.add(TDate);

            CourierPanel.add(new JLabel("Select Despatch Mode"));
            CourierPanel.add(JCCourier);

            ApplyPanel.add("Center",BApply);

            DatePanel.setBorder(new TitledBorder("Period"));
            CourierPanel.setBorder(new TitledBorder("Choice"));
            ApplyPanel.setBorder(new TitledBorder("Control"));

            BottomPanel.add(BOkay);
            BottomPanel.add(BExit);

            getContentPane().add(TopPanel,BorderLayout.NORTH);
            getContentPane().add(BottomPanel,BorderLayout.SOUTH);
      }

      private void addListeners()
      {
            BApply.addActionListener(new ApplyList());
            BOkay.addActionListener(new SaveList());
            BExit.addActionListener(new exitList());

      }
      private class exitList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVisible(false);
            }

      }

      private class SaveList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  BOkay.setEnabled(false);
                  updateData();
                  BOkay.setEnabled(true);
            }

      }
      private void updateData()
      {

            if(!isSelect())
            {
                  JOptionPane.showMessageDialog(null,"No Row Selected","Error",JOptionPane.ERROR_MESSAGE);
                  return;
            }
            persist();
            removeHelpFrame();

      }


      private boolean isSelect()
      {

            for(int i=0;i<RowData.length;i++)
            {
                  Boolean bValue = (Boolean)RowData[i][6];
                  if(!bValue.booleanValue())
                        continue;
                  return true;
            }
            return false;
      }

      private void persist()
      {
            try
            {

                  for(int i=0;i<RowData.length;i++)
                  {
                        Boolean bValue = (Boolean)RowData[i][6];
                        if(!bValue.booleanValue())
                              continue;
                        String SDespCode = (String)VDespCode.elementAt(i);
                        thabalDomain.getUpdateThabalStatus(SDespCode,iDate);
                  }

            }
            catch(Exception ex){}

      }



      private void removeHelpFrame()
      {
            try
            {
                  Layer.remove(this);
                  Layer.updateUI();
                  Layer.repaint();
            }
            catch(Exception ex){}

      }


      public class ApplyList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setDataIntoVector();
                  setRowData();
                  try
                  {
                        getContentPane().remove(tabreport);
                  }
                  catch(Exception ex){}
                  try
                  {
                        tabreport = new TabReport(RowData,ColumnData,ColumnType);
                        tabreport.setBorder(new TitledBorder(""));
                        for(int i=0;i<ColumnData.length;i++)
                        {
                            tabreport.setHeaderColor(ColumnData[i]);
                        }

                        tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

                        //tabreport.ReportTable.getColumn("Despatcher Name").setPreferredWidth(200);
                        //tabreport.ReportTable.getColumn("Local").setPreferredWidth(75);
                        //tabreport.ReportTable.getColumn("Other State").setPreferredWidth(75);
                        //tabreport.ReportTable.getColumn("International").setPreferredWidth(75);
                        //tabreport.ReportTable.getColumn("Parcel").setPreferredWidth(75);
                        //tabreport.ReportTable.getColumn("Total").setPreferredWidth(75);
      
                        getContentPane().add(tabreport,BorderLayout.CENTER);

                        setSelected(true);
                        Layer.repaint();
                        Layer.updateUI();
                        tabreport.requestFocus();
                        BOkay.setEnabled(true);
                  }
                  catch(Exception ex)
                  {
                        System.out.println(ex);
                  }
            }
      }
      private void setDataIntoVector()
      {

            try
            {
                  SDate = TDate.toNormal();
                  iDate = common.toInt(SDate);
                  String SDespModeCode = (String)VDespModeCode.elementAt(JCCourier.getSelectedIndex());
                  Vector VTypeCode  = new Vector();
                  Vector VTypeName  = new Vector();
      
                  VDespName         = new Vector();
                  VDespCode         = new Vector();
                  VLocal            = new Vector();
                  VOther            = new Vector();
                  VInter            = new Vector();
                  VParcel           = new Vector();
                  VTotal            = new Vector();
      
      
                  Vector V1         = new Vector();
                  Vector V2         = new Vector();
                  Vector V3         = new Vector();
      
                  V1 = thabalDomain.getDistinctCourier(SDespModeCode,iDate);
                  int m=0;
      
                  for(int i=0;i<V1.size()/2;i++)
                  {
                        String SCode = (String)V1.elementAt(0+m);
                        String SName = (String)V1.elementAt(1+m);
                        VDespCode.addElement(SCode);
                        VDespName.addElement(SName);
                        m=m+2;
                  }
      
                  V2 = thabalDomain.getThabalType();
      
                  int n=0;
      
                  for(int i=0;i<V2.size()/2;i++)
                  {
                        String SCode = (String)V2.elementAt(0+n);
                        String SName = (String)V2.elementAt(1+n);
                        VTypeCode.addElement(SCode);
                        VTypeName.addElement(SName);
                        n=n+2;
                  }
      
                  V3 = thabalDomain.getThabalSummary(VDespCode,VTypeCode,iDate);
      
                  int k=0;

                  int iSize = V3.size()/VTypeCode.size();

                  for(int i=0;i<iSize;i++)
                  {
                        String SLocal  = common.parseNull((String)V3.elementAt(0+k));
                        String SOther  = common.parseNull((String)V3.elementAt(1+k));
                        String SInter  = common.parseNull((String)V3.elementAt(2+k));
                        String SParcel = common.parseNull((String)V3.elementAt(3+k));
                        String STotal  = String.valueOf(common.toInt(SLocal)+common.toInt(SOther)+common.toInt(SInter)+common.toInt(SParcel));
                        VLocal.addElement(SLocal);
                        VOther.addElement(SOther);
                        VInter.addElement(SInter);
                        VParcel.addElement(SParcel);
                        VTotal.addElement(STotal);
                        k=k+4;
                  }

            }
            catch(Exception ex)
            {

                   try
                   {
                         PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Raj.err")));
                         ex.printStackTrace(out);
                         out.close();
                   }
                   catch(Exception e){}

                  //ex.printStackTrace();
            }


      }

      public void setRowData()
      {
            RowData     = new Object[VDespName.size()][ColumnData.length];
            for(int i=0;i<VDespName.size();i++)
            {
                  RowData[i][0]  = (String)VDespName.elementAt(i);
                  RowData[i][1]  = (String)VLocal.elementAt(i);
                  RowData[i][2]  = (String)VOther.elementAt(i);
                  RowData[i][3]  = (String)VInter.elementAt(i);
                  RowData[i][4]  = (String)VParcel.elementAt(i);
                  RowData[i][5]  = (String)VTotal.elementAt(i);
                  RowData[i][6]  = new Boolean(false);
            }  
      }


      private void setData()
      {
            try
            {
                  VDespModeCode     = new Vector();
                  VDespModeName     = new Vector();
      
                  Vector V1         = new Vector();
                  V1                = thabalDomain.getThabalDespMode();
      
                  int m=0;
      
                  for(int i=0;i<V1.size()/2;i++)
                  {
                        String SCode = (String)V1.elementAt(0+m);
                        String SName = (String)V1.elementAt(1+m);
                        VDespModeCode.addElement(SCode);
                        VDespModeName.addElement(SName);
                        m=m+2;
                  }
            }
            catch(Exception e)
            {
                  e.printStackTrace();
            }

      }


}
