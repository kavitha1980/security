package client.visitor;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import java.lang.*;

import util.*;

public class VisitorsTodayOutModel extends DefaultTableModel implements rndi.CodedNames
{

     String columnname[]={"CompanyName","SlipNo","Representative Name","Purpose","To Meet","Card No","InTime","OutTime","VisitorDate" };
     String ColumnType[]={"N","N","N","N","N","N","N","N","N"};     

     Object values[][];

     Common common  = new Common();


     Vector v1;
     int sNo;

     VisitorsTodayOutModel(Vector v1)
     {
               this.v1 = v1;
               setDataVector(getdata(),columnname);
     }

     private Object[][] getdata()
     {

               int sno=1;

               int size = (v1.size())/9;

               Object values[][]=new Object[size][columnname.length];
     
               int m=0;
               try
               {
                    for(int i=0;i<size;i++)
                    {

                         values[i][0]   =    v1.elementAt(m+0);

                         values[i][1]   =    v1.elementAt(m+1);

                         values[i][2]   =    v1.elementAt(m+2);

                         values[i][3]   =    v1.elementAt(m+3);

                         values[i][4]   =    v1.elementAt(m+4);

                         values[i][5]   =    v1.elementAt(m+5);

                         values[i][6]   =    v1.elementAt(m+6);

                         values[i][7]   =    v1.elementAt(m+7);

                         System.out.println(common.parseDate((String)v1.elementAt(m+8)));

                         values[i][8]   =    String.valueOf(common.parseDate((String)v1.elementAt(m+8)));

                         m=m+9;   

                    }
                }

                     catch(Exception e)
                         {
                              System.out.println("Exception in vector");
                         }
               return values;
     }

     public int getSno(Vector vect1)
     {
               
               int i;
               for(i=0;i<vect1.size();i++)
               {
               }
               return i;
     }

     //public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }

     public void setVector(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }

     public  boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="N")
               return false;
          return true;
     }

     public void setVector1(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }
     
}

