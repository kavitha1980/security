                
 /*

     To acquire details of visitor(s) inward

     The First Class to instantiated for a visitor
                   ex
     Refer the helpers

          CompanyModel 
          VisitorModel

*/

package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.util.Date;
import java.util.GregorianCalendar;

import utility.*;
import utility.Control;

import util.TimeField;
import util.ClockField;
import util.DateField1;
import util.Common;
import util.MyComboBox;

import java.sql.*;
import blf.*;

import java.rmi.*;
import java.rmi.registry.*;

public class VisitorInFrameNew extends JInternalFrame implements rndi.CodedNames
{

      protected JLayeredPane Layer;

      JTabbedPane    IntabPane;

      JPanel         TopPanel,MiddlePanel,BottomPanel,TypePanel;

      JTextField     TSlipNo;
      DateField1      TDate;

      JTextField     TTime;

      CompanyModel   theCompany;

      VisitorInModelNew theModel;

      JTable         theTable;
      TimeField      theTime; 
      MyComboBox     JCType;

   // JTextField     TRegNo;
      MyButton       BOkay;
      JButton        BExit; 

      Control        control;
      Common         common;

      String         SMaxSlipNo;
      VisitorIn visitorDomain;

      public VisitorInFrameNew(JLayeredPane Layer)
      {
            this.Layer = Layer;
            setDomain();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      private void setDomain()
      {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       =(VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private void createComponents()
      {
            IntabPane      = new JTabbedPane();   

            TopPanel       = new JPanel(true);
            MiddlePanel    = new JPanel(true);
            BottomPanel    = new JPanel(true);
            TypePanel      = new JPanel(true);   

            TSlipNo        = new JTextField();
            TDate          = new DateField1();
            TTime          = new JTextField();
            JCType         = new MyComboBox();

            JCType.addItem("By Walk");
            JCType.addItem("By Two  Wheeler");
            JCType.addItem("By Four Wheeler");

            theCompany     = new CompanyModel();
            theCompany.setMnemonic('C');    

            theModel       = new VisitorInModelNew(theCompany);
            theTable       = new JTable(theModel);

            theTime        = new TimeField();

            BExit          = new JButton("Exit");
            BOkay          = new MyButton("Okay");
            BOkay.setMnemonic('O');
            BExit.setMnemonic('X');

            common         = new Common();
            control        = new Control();

      }

      private void setLayouts()
      {

            setTitle("Visitor Information Feeder - Inward(3.0)");
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,798,520);

            TopPanel.setLayout(new GridLayout(1,4));
            MiddlePanel.setLayout(new BorderLayout());

            TopPanel.setBorder(new TitledBorder("Company"));
            MiddlePanel.setBorder(new TitledBorder("Visitor Information"));

            TypePanel.setLayout(new BorderLayout());
            TypePanel.setBorder(new TitledBorder("Arrived By"));
            TDate.setBorder(new TitledBorder("Date"));
            TTime.setBorder(new TitledBorder("In-Time"));
          
            TSlipNo.setEditable(false);
            TTime.setEditable(false);

            setPresets();
      }


      private void addComponents()
      {

            TypePanel.add(JCType);
            TopPanel.add(TypePanel);
            TopPanel.add(TDate);
            TopPanel.add(TTime);
            TopPanel.add(theCompany);

            MiddlePanel.add("North",theTable.getTableHeader());
            MiddlePanel.add("Center",new JScrollPane(theTable));

            setTitle("Visitor In Frame(3.0)");

            IntabPane.setFont(new Font("Copperplate Gothic Bold",Font.BOLD,11));
            IntabPane.addTab("VisitorInfo",MiddlePanel);
            IntabPane.addTab("Today's Visitor Info",new VisitorsTabInModel());
               
            BottomPanel.add(BOkay);
            BottomPanel.add(BExit);
            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",IntabPane);
            getContentPane().add("South",BottomPanel);
      }

      private void addListeners()
      {
            theTable.addKeyListener(new TableKeyList());
            BOkay.addActionListener(new SaveList());
            BExit.addActionListener(new exitList());

      }
      private class exitList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVisible(false);
            }

      }      


      private class TableKeyList extends KeyAdapter
      {

            public void keyPressed(KeyEvent ke)
            {
                  if(ke.getKeyCode()==KeyEvent.VK_INSERT)
                  {
                        appendRow();
                  }

                  if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                  {
                        deleteRow(theTable.getSelectedRow());
                  }

                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                        showHelpFrame(theTable.getSelectedRow(),theTable.getSelectedColumn());
                  }
            }
      }


      private void appendRow()
      {

            theModel.appendRow();
      }

      private void deleteRow(int iRow)
      {
            theModel.deleteRow(iRow);
      }

      private void showHelpFrame(int iRow,int iCol)
      {
            theModel.showHelpFrame(iRow,iCol);
      }


      private class SaveList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  boolean OkFlag = checkFields();
                  if(!OkFlag)
                  {
                        JOptionPane.showMessageDialog(null,"All Fields must be filled");
                        boolean EFlag = theCompany.isEnabled();
                        if(!EFlag)
                        {
                              theTable.requestFocus();
                        }
                        else
                        {
                              theCompany.requestFocus();
                        }
                  }
                  else
                  {
                        try
                        {
                             boolean BExists = isAlreadyExists((String)theModel.getCard(0));
                             if(!BExists)
                             {
                                  BOkay.setEnabled(false);
                                  saveData(SMaxSlipNo);
                                  setVisible(false);
                             } 
                             else
                             { 
                                  JOptionPane.showMessageDialog(null,"CardNo Already Exists ");
                             }    
                        }
                        catch(Exception e)
                        {
                             e.printStackTrace();
                        } 
                  }
            }
      }

      public class VisitorsTabInModel extends JPanel
      {
          JTable todaytable;

          public VisitorsTabInModel()
          {
               try
               {
                    DateField1 da = new DateField1();
                    
                    da.setTodayDate();
                    int iDate      = Integer.parseInt(da.toNormal());

                    setLayout(new BorderLayout());
                    Vector initVector = visitorDomain.getVisitorInInfo(iDate,iDate);    
                    VisitorsTodayInModel tmodel = new VisitorsTodayInModel(initVector);
                    todaytable = new JTable(tmodel);
                    //JScrollPane sPane = new JScrollPane(todaytable);
                    setPrefferredWidth();
                    add(todaytable.getTableHeader());
                    add(new JScrollPane(todaytable));
               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }
          }
          private void setPrefferredWidth()
          {
               todaytable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (todaytable.getColumn("CompanyName")).setPreferredWidth(200);
               (todaytable.getColumn("SlipNo")).setPreferredWidth(50);
               (todaytable.getColumn("Representative Name")).setPreferredWidth(100);
               (todaytable.getColumn("Purpose")).setPreferredWidth(180);
               (todaytable.getColumn("To Meet")).setPreferredWidth(180);
//               (todaytable.getColumn("CardNo")).setPreferredWidth(180);
               (todaytable.getColumn("InTime")).setPreferredWidth(180);
               (todaytable.getColumn("OutTime")).setPreferredWidth(180);
               (todaytable.getColumn("VisitorDate")).setPreferredWidth(180);

          }

     }

     private boolean checkFields()
     {
            boolean flag=true;

            int count = theModel.getRows();


            for(int i=0;i<count;i++)
            {

                 theTable.changeSelection(i,0,false,false);
     
     
                 if(theModel.getCard(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getRep(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getStaff(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getPurpose(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }
		 if(theModel.getDept(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }
            }
            return flag;
      }

      private void saveData(String SMaxSlipNo)
      {
            try
            {
                 SMaxSlipNo = visitorDomain.getNextSlipNo(0);
                 theModel.saveData(SMaxSlipNo,JCType.getSelectedIndex());
                 JOptionPane.showMessageDialog(null,"Details Saved,SlipNo:  "+SMaxSlipNo);
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }   

      }

      private boolean isAlreadyExists(String SCardNo)
      {
          boolean isexists = true;
          try
          {
               isexists = visitorDomain.isCardAlreadyExists(SCardNo);
          }                      
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return isexists;
      }

      private void refresh()
      {
            theModel.setNumRows(0);
            theModel.appendRow();
            theCompany.setEnabled(true);
            theCompany.setText("Company");
            setPresets();
            //theModel.refresh();
            BOkay.setEnabled(true);
      }


      private void setPresets()
      {
          try
          {

               //SMaxSlipNo = visitorDomain.getNextSlipNo(0);
               TSlipNo.setText("To Be Determined");
               TDate.setTodayDate();
               TDate.setEditable(false);
               TTime.setText(getCurrentTime());
               

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      private String getCurrentTime()
      {
            return theTime.getTimeNow();
          //TDate.requestFocus();
      }
}
          /*rowset.appendRow(getMessageVector());
          Vector VId     = theMessageDomain.sendMessageSource(rowset);

          int id         = Integer.parseInt((String)VId.elementAt(0));


          theMessageDomain.setMessageText(SMessage,id);

          rowset1.appendRow(getTargetVector(id));
          theMessageDomain.sendMessageTarget(rowset1);
      
     public Vector getTargetVector(int iId)
     {
          Vector TVect   = new Vector();

          String STargetUserCode = (String)session.getValue("TargetUserCode");
          iTargetUserCode = Integer.parseInt(STargetUserCode);

          TVect.addElement(String.valueOf(iId));
          TVect.addElement(String.valueOf(iTargetUserCode));
          TVect.addElement(String.valueOf(0));

          return TVect;
     }

     public Vector getMessageVector()
     {
          Vector vect    = new Vector();
          SUserName      = (String)session.getValue("SUserName");
          SUserCode      = (String)session.getValue("UserCode");

          iUserCode      = Integer.parseInt(SUserCode);
          SMsgTime       = (String)session.getValue("SMsgTime");
          SMsgDate       = (String)session.getValue("SMsgDate");
          iMsgDate       = Integer.parseInt(SMsgDate);
          SSubjectCode   =(String)session.getValue("SSubjectCode");
          iSubjectCode   =Integer.parseInt(SSubjectCode);
                         
          String SUserName = (String) session.getValue("TargetUserName");

          vect.addElement(String.valueOf(iUserCode));
          vect.addElement(String.valueOf(iSubjectCode));
          vect.addElement(SMsgDate);
          vect.addElement(SMsgTime);

          return vect;
          
     }*/

