/*

     The Representative Object is meant for
     allowing the User to select a Representative
     belongs to a particular Company.

     If the Representative in ? is not available a provision
     to insert On line the new Representative.

*/
package client.visitor;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import util.Common;

public class DriverModel  implements rndi.CodedNames
{

      protected VisitorInModelCopy theModel;
      protected   int          iRow,iCol;

      JTextField   TName,TCode;
      Vector       VCode,VName;
      Vector       VSelectedDriverCode;

      Common      common = new Common();
      VisitorIn visitorDomain;
     

      public DriverModel(VisitorInModelCopy theModel)
      {
            this.theModel      = theModel;
            setDomain();
            TName              = new JTextField();
            TCode              = new JTextField();
            VSelectedDriverCode   = new Vector();

      }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void showDriverFrame(int iRow,int iCol)
     {
            this.iRow          = iRow;
            this.iCol          = iCol;

            setDataIntoVector();
            TCode.setText(theModel.getRep(iRow));

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Representative Selector",true); 
            DriverSearchPanel RSP = new DriverSearchPanel(this,TCode,TName,VCode,VName,theDialog);

            theDialog.getContentPane().add(RSP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {

            if(theModel.isRepInList(TCode.getText()))
                  return;

            String SCode = TCode.getText();

            int index    = VCode.indexOf(SCode);

            if(index == -1)
                  setData();
            else
                  TCode.setText(SCode);
            theModel.setDriver(TCode.getText(),TName.getText(),iRow);
            VSelectedDriverCode.addElement((String)TCode.getText());
      }

      private void setData()
      {
            try
            {
                  visitorDomain.insertDriver((String)TName.getText(),theModel.getCompanyCode());

                  TCode.setText((String)visitorDomain.getMaxDriverCode());

                  setDataIntoVector();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setDataIntoVector()
      {
            VCode        = new Vector();
            VName        = new Vector();             
            Vector VTemp = new Vector();
            try
            {
                  VTemp  = visitorDomain.getDriverInfo(Integer.parseInt(theModel.getCompanyCode()));
                  int m=0;
                  int size=(VTemp.size())/2;
                  for(int i=0;i<size;i++)
                  {
                        VCode.addElement(VTemp.elementAt(0+m));
                        VName.addElement(VTemp.elementAt(1+m));
                        m=m+2; 
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

}
