
/*
      The Model for handling Visitor informations
      Name
      CardNo
      Purpose
      To Meet whom
*/
package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;
import java.sql.*;

import util.RowSet;
import util.TimeField;
import util.DateField1;
import util.Common;

import blf.*;

import java.net.*;

import java.rmi.*;
import java.rmi.registry.*;

public class VisitorInModelNew extends DefaultTableModel implements rndi.CodedNames
{

      protected CompanyModel theCompany;


      // Column Ids
      int SLNO   = 0;
      int REG    = 1;
      int DRIVER = 2;
      int REP    = 3;
      int PURPOSE= 4;
      int STAFF  = 5;
      int CARDNO = 6;
	  int DEPT   = 7;

      RepModelNew     theRep;
      PurposeModelNew thePurpose;
      StaffModelNew   theStaff;
      RegModelNew     theReg;
      DriverModelNew  theDriver;
      VisitorDepartmentModel theDept;

      TimeField    theTime;
      DateField1    theDate;
//      DepartmentModel theDept;
      VisitorIn visitorDomain;

      Vector VDeptCode,VRepCode,VPurposeCode,VStaffCode,VRegCode,VDriverCode;

      String ColumnName[] = {"Sl No","RegNo","Driver","Representative","Purpose","To meet","Card No","Dept"};

      String ColumnType[] = {  "S"  ,"S"    ,"S"     ,      "S"       ,   "S"   ,   "S"   , "E"     ,"S"   };
     
      Common common = new Common();

      RowSet rowset,rowset1;
 
      //-- The Constructor Method as referenced by the VisitorFrame


      public VisitorInModelNew(CompanyModel theCompany)
      {
            this.theCompany = theCompany;

            VRepCode        = new Vector();
            VPurposeCode    = new Vector();
            VStaffCode      = new Vector();
            VDeptCode       = new Vector();
            VRegCode        = new Vector();
            VDriverCode     = new Vector();

            theRep          = new RepModelNew(this);
            thePurpose      = new PurposeModelNew(this);
            theStaff        = new StaffModelNew(this);
            theReg          = new RegModelNew(this);
            theDriver       = new DriverModelNew(this);
     	    theDept         = new VisitorDepartmentModel(this);

            theDate         = new DateField1();

            setDomain();
            setDataVector(getRowData(),ColumnName);

            removeRow(0);
            appendRow();
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private Object[][] getRowData()
      {
            Object RowData[][] = new Object[1][ColumnName.length];
            for(int i=0;i<ColumnName.length;i++)
            {
                  RowData[0][i] = "";

            }
            return RowData;
      }

      //-- Overriding the methods of Default Table Model for additional features


      public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }


      public boolean isCellEditable(int row,int col)
      {
            if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
            return false;
      }

      boolean okflag=true;
      public void setValueAt(Object aValue, int row, int column)
      {
            try
            {
                  Vector rowVector = (Vector)super.dataVector.elementAt(row);
                  rowVector.setElementAt(aValue, column);

                  if (column==6)
                  {
                        try
                        {
                              String Scol4 = common.parseNull((String)rowVector.elementAt(5));
                              if(Scol4.equals(""))
                              {
                                   JOptionPane.showMessageDialog(null,"CardNo must be filled");
                              }                   
                              /*else
                              {
                                   String QS = "Select Out from Visitor where CardNo='"+Scol4+"' And OutTime='0' " ;
                                   String st =  visitorDomain.getVisitorInitialCheck(QS);
                                   if(st==null)
                                   {
                                        okflag=true;
                                   }
                                   else
                                   {
                                        okflag=false;
                                        JOptionPane.showMessageDialog(null,"Already a person having This Card");
                                   }
                              }*/
                        }
                        catch(Exception ne)
                        {
                              //JOptionPane.showMessageDialog(null,"Numbers only allowed!...Please Enter a Number...");
                              rowVector.set(4,"");
                        }
                        fireTableChanged(new TableModelEvent(this, row, row, column));
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public int getRows()
      {

            return super.dataVector.size();
      }
      public void refresh()
      {
          setNumRows(0);
      }

      public void appendRow()
      {
            Vector vect = new Vector();
            for(int i=0;i<ColumnName.length;i++)                     
            {
                  vect.addElement("");

                  VRegCode.addElement("-1");
                  VDriverCode.addElement("-1");
                  VRepCode.addElement("-1");
                  VPurposeCode.addElement("-1");
                  VStaffCode.addElement("-1");
                  VDeptCode.addElement("-1");
            }               
            insertRow(getRows(),vect);
            setRowId();
      }

      public void deleteRow(int iRow)
      {
            if(getRows()==1)
                  return;
            removeRow(iRow);

            VRegCode.removeElementAt(iRow);
            VDriverCode.removeElementAt(iRow);
            VRepCode.removeElementAt(iRow);
            VPurposeCode.removeElementAt(iRow);
            VStaffCode.removeElementAt(iRow);
            VDeptCode.removeElementAt(iRow);
            setRowId();
      }

      private void setRowId()
      {
            for(int i=0;i<getRows();i++)
            {
                  setValueAt(String.valueOf(i+1),i,SLNO);
            }
      }


      public void showHelpFrame(int iRow,int iCol)
      {
            if(iCol == REP)
            {
                  theRep.showRepFrame(iRow,iCol);
            }
            if(iCol == PURPOSE)
            {
                  thePurpose.showPurposeFrame(iRow,iCol);
            }
            if(iCol == STAFF)
            {
                  theStaff.showStaffFrame(iRow,iCol);               
            }
            if(iCol == REG)
            {
                  theReg.showRegFrame(iRow,iCol);
            }  
            if(iCol == DRIVER)
            {
                  theDriver.showDriverFrame(iRow,iCol);
            }
			if(iCol == DEPT)
            {
                  theDept.showDeptFrame(iRow,iCol);
            }
      }

      public String getCompanyCode()
      {
            return theCompany.getCode();
      }

    
      public void setRep(String SCode,String SName,int iRow)
      {
            try
            {
                  VRepCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,REP);
            }
            catch(Exception ex){}
      }
      public void setReg(String SCode,String SName,int iRow)
      {
            try
            {
                  VRegCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,REG);
            }
            catch(Exception ex){}
      }
      public void setDriver(String SCode,String SName,int iRow)
      {
            try
            {
                  VDriverCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,DRIVER);
            }
            catch(Exception ex){}
      }


      public void setPurpose(String SCode,String SName,int iRow)
      {
            try
            {
                  VPurposeCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,PURPOSE);
            }
            catch(Exception ex){}
      }

      public void setStaff(String SCode,String SName,int iRow)
      {
            try
            {
                  VStaffCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,STAFF);
            }
            catch(Exception ex){}
      }
	  public void setDept(String SCode,String SName,int iRow)
      {
            try
            {
                  VDeptCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,DEPT);
            }
            catch(Exception ex){}
      }

      public String getRep(int iRow)
      {
            return (String)VRepCode.elementAt(iRow);
      }
      public String getReg(int iRow)
      {
            return (String)VRegCode.elementAt(iRow);
      }
      public String getDriver(int iRow)
      {
            return (String)VDriverCode.elementAt(iRow);
      }

      public String getPurpose(int iRow)
      {
            return (String)VPurposeCode.elementAt(iRow);
      }

      public String getStaff(int iRow)
      {
            return (String)VStaffCode.elementAt(iRow);
      }
      public String getCard(int iRow)
      { 
            return ((String)getValueAt(iRow,CARDNO)).toUpperCase();
      }
	  public String getDept(int iRow)
      {
            return (String)VDeptCode.elementAt(iRow);
      }

      public boolean isRepInList(String SCode)
      {
            int index = VRepCode.indexOf(SCode);
            if(index == -1)
                  return false;
            return true;
      }

      public void saveData(String slipno,int iVehicleType)
      {
            try
            {
                  int ccode = Integer.parseInt((String)theCompany.TCode.getText());

                  theTime = new TimeField();
                  String theSTime =theTime.getTimeNow();

                  theDate.setTodayDate();

                  int vslipno = Integer.parseInt(slipno);
               

                  for(int i=0;i<theRep.VSelectedRepCode.size();i++)
                  {

                        //System.out.println("VSelected:"+theRep.VSelectedRepCode.size());
                        
                        int rpcode      = Integer.parseInt((String)theRep.VSelectedRepCode.elementAt(i));
                        int ppcode      = Integer.parseInt((String)thePurpose.VSelectedPurposeCode.elementAt(i));
                        int stcode      = Integer.parseInt((String)theStaff.VSelectedStaffCode.elementAt(i));
						int ideptcode   = Integer.parseInt((String)theDept.VSelectedDeptCode.elementAt(i));
                        int slno        = visitorDomain.getSlNo("visitor_seq");
                        int iVehicle    = iVehicleType;
                        int iregcode    =0;
                        if(!(common.parseNull((String)getValueAt(i,REG)).equals("")))
                              iregcode    = Integer.parseInt((String)theReg.VSelectedRegCode.elementAt(i));
                        int idrivercode=0;
                        if(!(common.parseNull((String)getValueAt(i,DRIVER)).equals("")))
                              idrivercode = Integer.parseInt((String)theDriver.VSelectedDriverCode.elementAt(i));

                        int iDate = common.toInt(common.pureDate(theDate.toNormal()));

                        Vector vect     = new Vector();

                         /*
                         System.out.println("-----------Records----------");
                         System.out.println("Slno      : "+slno);
                         System.out.println("slipno    : "+vslipno);
                         System.out.println("compa     : "+ccode);
                         System.out.println("repre     : "+rpcode);
                         System.out.println("purpose   : "+ppcode);
                         System.out.println("staff     : "+stcode);
                         System.out.println("cardno    : "+getCard(i));
                         System.out.println("sttime    : "+theSTime);
                         System.out.println("date      : "+iDate);
                         System.out.println("vehicle   : "+iVehicle);
                         System.out.println("reg       : "+iregcode);
                         System.out.println("driver    : "+idrivercode);
                         System.out.println("-----------xxxxxxx----------");
                         */

                        vect.addElement(String.valueOf(slno));
                        vect.addElement(String.valueOf(vslipno));
                        vect.addElement(String.valueOf(ccode));
                        vect.addElement(String.valueOf(rpcode));
                        vect.addElement(String.valueOf(ppcode));
                        vect.addElement(String.valueOf(stcode));
                        vect.addElement(getCard(i));
                        vect.addElement(theSTime);
                        vect.addElement(String.valueOf(iDate));
                        vect.addElement(String.valueOf(iVehicle));
                        vect.addElement(String.valueOf(iregcode));
                        vect.addElement(String.valueOf(idrivercode));
						vect.addElement(String.valueOf(ideptcode));

                        visitorDomain.saveData(slipno,vect);



                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
      
      public boolean checkDate(String Date)
      {
            boolean flag=false;
            /*try
            {

                  Vector VDate      = visitorDomain.getFinanceYear();
                  int stDate        = Integer.parseInt((String)VDate.elementAt(0));
                  int eDate         = Integer.parseInt((String)VDate.elementAt(1));

                  int idate         = common.toInt(common.pureDate(Date));

                  if( (idate >=stDate ) && (idate<=eDate) )
                        flag =  true;
                  
            }
            catch(Exception e)
            {
                  e.printStackTrace();
            }*/
            return flag;
      }

}


                        /*rowset    = new RowSet();
                         
                        Vector VMessage           = new Vector();
                        try
                        {
               
                              int iDepCode        = visitorDomain.getDeptCode(stcode);
                              int iHodCode        = visitorDomain.getHodCode(iDepCode);
                              String SHost        = visitorDomain.getHostId(iDepCode);

                              String STime             = common.getTime();

                              VMessage.addElement(String.valueOf(10));
                              VMessage.addElement(String.valueOf(1));
                              VMessage.addElement(String.valueOf(iDate));
                              VMessage.addElement(STime);

                              String SMsg=" A Visitor coming to ur Department";

                              rowset.appendRow(VMessage);

                              int iId                  = visitorDomain.getNextId(); 
                                                       

                              iId                      = iId+1;

                              Vector thePrimaryVector  =  visitorDomain.sendMessageSource(iId,rowset);


                              visitorDomain.setMessageText(SMsg,iId);
               
               
                              for(int j=0; j<thePrimaryVector.size(); j++)
                              {
                                   rowset1       = new RowSet(); 
                                   Vector VTarget            = new Vector();
                                   VTarget.addElement(String.valueOf(iId));
                                   VTarget.addElement(String.valueOf(iHodCode));
                                   VTarget.addElement(String.valueOf(0));
                                   rowset1.appendRow(VTarget);
                                   visitorDomain.sendMessageTarget(rowset1);
                              }                                          
                         }
                         catch(Exception ex)
                         {
                              System.out.println("Persist"+ex);
                         }*/
                          


