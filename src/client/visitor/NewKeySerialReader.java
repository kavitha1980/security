
package client.visitor;

import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import java.awt.Font;
import java.awt.Color;
import domain.jdbc.*;
import java.sql.*;
import util.*;

public class NewKeySerialReader implements SerialPortEventListener,Runnable{
    protected JLabel LKeyCode,LKeyName;
    protected JButton BOkay;
    protected JList KeyCodeList;
    protected DefaultListModel Items;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;
     Connection con;	
     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     String sKeyCode = "";
     String sTransType = "";
     int    ctr=0;
     int    iMultiply = 1;
     boolean heartAttack = false;
     Font KeyCodeFont =   new Font("Times New Roman", Font.BOLD, 20);
     Font KeyNameFont =   new Font("Times New Roman", Font.BOLD, 18);
     StringBuffer inputBuffer = new StringBuffer();
     JTextField         txtKeyCode, txtKeyName;
     JButton            btnSave;
  
  //public NewKeySerialReader(JLabel LKeyCode,JLabel LKeyName,JButton BOkay,JList KeyCodeList,DefaultListModel Items, String sTransType)
     public NewKeySerialReader(JTextField txtKeyCode,JTextField txtKeyName,JButton btnSave)
     {
              this.txtKeyCode      = txtKeyCode;
              this.txtKeyName      = txtKeyName;
              this.btnSave         = btnSave;

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }
       private void createComponents()
     {
          //System.out.println("createComponents 1");
		  
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();  
			   System.out.println("After portList "+portList);        
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
					
					System.out.println("After portId --> "+portId.getName()); 
					
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println("Comm Error : "+ex);
			   JOptionPane.showMessageDialog(null,"Already Running");
			   System.exit(0);
          }
     }
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
        				  //System.out.println("KeyCode  --->1 "+(char)newData);

                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              //System.out.println("KeyCode "+(char)newData);

                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setKeyCode(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                //if(ctr < 10)
                                  outputStream.write((int)'a');
                                //else
//                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
 private void setKeyCode(String str)
     {	 	  
          String sKeyCode= "", sKeyName="";
          try{
              sKeyCode = str.substring(1,str.length()-1);
              txtKeyCode.setText(sKeyCode.trim());
              btnSave.setEnabled(true); 
          }
          catch(Exception ex){}
     }

public void freeze(){
          serialPort.close();
  }
 
}
