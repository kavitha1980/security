package client.visitor;

public class PartyVisitorClass
{
     String SPartyCode,SPartyName,SSubject,SVisitDate,SVisitInTime,SOutTime;
     String SInRepName,SInPurpose,SInVehicleNo,SInDriverName,SInToMeet,SInCardNo,SInInTime,SInOutTime;

     public PartyVisitorClass(String SPartyCode,String SPartyName,String SSubject,String SVisitDate,String SVisitInTime,String SOutTime)
     {
          this . SPartyCode        = SPartyCode;
          this . SPartyName        = SPartyName;
          this . SSubject          = SSubject;
          this . SVisitDate        = SVisitDate;
          this . SVisitInTime      = SVisitInTime;
          this . SOutTime          = SOutTime;
     }

     public PartyVisitorClass(String SInRepName,String SInPurpose,String SInVehicleNo,String SInDriverName,String SInToMeet,String SInCardNo,String SInInTime,String SInOutTime)
     {
          this . SInRepName        = SInRepName;
          this . SInPurpose        = SInPurpose;
          this . SInVehicleNo      = SInVehicleNo;
          this . SInDriverName     = SInDriverName;
          this . SInToMeet         = SInToMeet;
          this . SInCardNo         = SInCardNo;
          this . SInInTime         = SInInTime;
          this . SInOutTime        = SInOutTime;
     }
}
