package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;
import java.sql.*;

import util.RowSet;
import util.TimeField;
import util.DateField1;
import util.Common;

import blf.*;

import java.net.*;

import java.rmi.*;
import java.rmi.registry.*;

public class VisitorFrameModel extends DefaultTableModel 
{
	String ColumnName[] = {"Sl No","RegNo","Driver","Company","Representative","Purpose","To meet","Card No","Dept"};
	String ColumnType[] = {  "S"  ,"S"    ,"S"     ,   "S"   ,   "S"          ,   "S"   ,   "S"   , "E"     , "S"  };

	public VisitorFrameModel()
     {
          setDataVector(getRowData(), ColumnName);
     }

    public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0; i<ColumnName.length; i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(), theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
     public void appendEmptyRow()
     {
          Vector curVector = new Vector();
          for(int i=0;i<ColumnName.length;i++) {
               if(i==ColumnName.length-1) {
                  curVector.addElement(new Boolean(false));
               }
               else {
                  if(i==0)
                     curVector.addElement(String.valueOf(getRows()+(i+1)));
                  else
                     curVector.addElement(" ");
               }
          }
          insertRow(getRows(),curVector);

     }

}                          


