package client.visitor;

import java.awt.*;              
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import java.sql.*;
import java.util.*;

public class PartyVisitorModel extends DefaultTableModel
{
     String ColumnName[] = {"S.No.","Rep. Name","Purpose","Vehicle No.","Driver Name","To Meet","Card No.","In Time","Out Time"};
     String ColumnType[] = {"S"    ,"E"        ,"S"      ,"E"          ,"E"          ,"E"      ,"E"       ,"S"      ,"S"       };
     int  iColumnWidth[] = {40     ,150        ,180      ,70           ,140          ,140      ,90        ,120      ,120       };

     public PartyVisitorModel()
     {
          setDataVector(getRowData(),ColumnName);
     }

     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}


