package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import java.util.Date;
import java.util.GregorianCalendar;

import utility.*;
import utility.Control;

import util.*;
import domain.jdbc.*;

import java.sql.*;
import blf.*;

import java.rmi.*;
import java.rmi.registry.*;

public class PartyVisitorFrame extends JInternalFrame
{
     protected           JLayeredPane Layer;

     JPanel              TopPanel,MiddlePanel,BottomPanel;

     DateField1          TDate;
     MyComboBox          JCPartyName,JCType;

     String              STime;

     PartyVisitorModel   theModel;
     JTable              theTable;

     JButton             BApply,BSave,BExit;

     Common              common;

     Vector              theVector,theInVector;
     Connection          theConnection;

     public PartyVisitorFrame(JLayeredPane Layer)
     {
          this           . Layer = Layer;

          common         = new Common();
          
          createComponents();
          setLayouts();
          addComponents();
          addListeners();

          theModel       . setNumRows(0);
     }

     private void createComponents()
     {
          TopPanel            = new JPanel();
          MiddlePanel         = new JPanel();
          BottomPanel         = new JPanel();

          TDate               = new DateField1();

          JCPartyName         = new MyComboBox();

          JCType              = new MyComboBox();
          JCType              . addItem("IN");
          JCType              . addItem("OUT");

          theModel            = new PartyVisitorModel();
          theTable            = new JTable(theModel);

          theTable            . setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

          TableColumnModel TC = theTable.getColumnModel();

          for(int i=0;i<theModel.ColumnName.length;i++)
          {
               TC             . getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }

          BApply              = new JButton("Apply");
          BSave               = new JButton("Save");
          BExit               = new JButton("Exit");
     }

     private void setLayouts()
     {
          setTitle("Party Visitor Frame");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,550);

          TopPanel            . setLayout(new GridLayout(2,4,5,5));
          MiddlePanel         . setLayout(new BorderLayout());
          BottomPanel         . setLayout(new FlowLayout());

          TopPanel            . setBorder(new TitledBorder(""));
          MiddlePanel         . setBorder(new TitledBorder(""));
          BottomPanel         . setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {
          TopPanel            . add(new JLabel("Date"));
          TopPanel            . add(TDate);
          TopPanel            . add(new JLabel(""));
          TopPanel            . add(BApply);

          TopPanel            . add(new JLabel("Party"));
          TopPanel            . add(JCPartyName);
          TopPanel            . add(new JLabel("In / Out"));
          TopPanel            . add(JCType);

          MiddlePanel         . add(new JScrollPane(theTable));

          BottomPanel         . add(BSave);
          BottomPanel         . add(BExit);

          getContentPane()    . add("North",TopPanel);
          getContentPane()    . add("Center",MiddlePanel);
          getContentPane()    . add("South",BottomPanel);
     }

     private void addListeners()
     {
          BApply              . addActionListener(new ActList());
          BSave               . addActionListener(new ActList());
          BExit               . addActionListener(new ActList());

          JCPartyName         . addItemListener(new ActList());
          JCType              . addItemListener(new ActList());

          theTable            . addKeyListener(new KeyList());
     }

     private class ActList implements ActionListener,ItemListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BApply)
               {
                    getApplyAction();
               }

               if(ae.getSource() == BSave)
               {
                    if(CheckValidation())
                    {
                         if(JOptionPane.showConfirmDialog(null,"Confirm Save the Data???","Confirm??",JOptionPane.YES_NO_OPTION) == 0)
                         {
                              if(JCType.getSelectedIndex() == 0)
                              {
                                   SaveDetails();
                              }
                              else
                              {
                                   UpdateDetails();
                              }
                         }
                    }
               }

               if(ae.getSource() == BExit)
               {
                    removeHelpFrame();
               }
          }

          public void itemStateChanged(ItemEvent ie)
          {
               if(ie.getSource() == JCPartyName)
               {
                    setPurpose();
               }

               if(ie.getSource() == JCType)
               {
                    if(JCType.getSelectedIndex() == 0)
                    {
                         theModel.setNumRows(0);
                         theTable.setEnabled(true);

                         insertNewRow();
                    }
                    else
                    {
                         theModel.setNumRows(0);
                         theTable.setEnabled(false);

                         setVectorValues();
                         setAlreadyInData();
                         FillInDetails();
                    }
               }
          }
     }

     private void getApplyAction()
     {
          theModel  . setNumRows(0);

          setVectorValues();
          setPartyComboBox();
          insertNewRow();
     }

     private class KeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode() == KeyEvent.VK_INSERT)
               {
                    insertNewRow();
               }

               if(ke.getKeyCode() == KeyEvent.VK_DELETE)
               {
                    try
                    {
                         theModel.removeRow(theTable.getSelectedRow());
                         setSlNos();
                    }
                    catch(Exception ex){}
               }
          }
     }

     private void setPartyComboBox()
     {
          JCPartyName                   . removeAllItems();

          for(int i=0;i<theVector.size();i++)
          {
               PartyVisitorClass theClass = (PartyVisitorClass)theVector.elementAt(i);

               JCPartyName              . addItem(theClass.SPartyName);
          }
     }

     private void insertNewRow()
     {
          try
          {
               if(JCType.getSelectedIndex() == 0)
               {
                    Vector theVector    = new Vector();
     
                    theVector           . addElement("");
                    theVector           . addElement("");
                    theVector           . addElement(getPurpose(common.parseNull((String)JCPartyName.getSelectedItem())));
                    theVector           . addElement("");
                    theVector           . addElement("");
                    theVector           . addElement("");
                    theVector           . addElement("");
                    theVector           . addElement(STime);
                    theVector           . addElement("");
     
                    theModel            . appendRow(theVector);
               }

               setSlNos();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void FillInDetails()
     {
          for(int i=0;i<theInVector.size();i++)
          {
               PartyVisitorClass theClass = (PartyVisitorClass)theInVector.elementAt(i);

               Vector theVector    = new Vector();

               theVector           . addElement(String.valueOf(i+1));
               theVector           . addElement(theClass.SInRepName);
               theVector           . addElement(theClass.SInPurpose);
               theVector           . addElement(theClass.SInVehicleNo);
               theVector           . addElement(theClass.SInDriverName);
               theVector           . addElement(theClass.SInToMeet);
               theVector           . addElement(theClass.SInCardNo);
               theVector           . addElement(theClass.SInInTime);
               theVector           . addElement(STime);

               theModel            . appendRow(theVector);
          }
     }

     private String getPurpose(String SPartyName)
     {
          for(int i=0;i<theVector.size();i++)
          {
               PartyVisitorClass theClass = (PartyVisitorClass)theVector.elementAt(i);

               if(SPartyName.equals(theClass.SPartyName))
               {
                    return theClass.SSubject;
               }
          }

          return "";
     }

     private String getPartyCode(String SPartyName)
     {
          for(int i=0;i<theVector.size();i++)
          {
               PartyVisitorClass theClass = (PartyVisitorClass)theVector.elementAt(i);

               if(SPartyName.equals(theClass.SPartyName))
               {
                    return theClass.SPartyCode;
               }
          }

          return "";
     }

     private void setSlNos()
     {
          for(int i=0;i<theModel.getRowCount();i++)
          {
               theModel . setValueAt(String.valueOf(i+1),i,0);
          }
     }

     private void setPurpose()
     {
          for(int i=0;i<theModel.getRowCount();i++)
          {
               theModel.setValueAt(getPurpose(common.parseNull((String)JCPartyName.getSelectedItem())),i,2);
          }
     }

     private void removeHelpFrame()
     {
          try
          {
               Layer     . remove(this);
               Layer     . updateUI();
          }
          catch(Exception ex){}
     }

     private boolean CheckValidation()
     {
          if(common.parseNull((String)JCPartyName.getSelectedItem()).length() == 0)
          {
               JOptionPane.showMessageDialog(null,"Party Name is Empty...","Error",JOptionPane.ERROR_MESSAGE);
               return false;
          }               

          for(int i=0;i<theModel.getRowCount();i++)
          {
               String SRepName     = common.parseNull((String)theModel.getValueAt(i,1));
               String SToMeet      = common.parseNull((String)theModel.getValueAt(i,5));
               String SCardNo      = common.parseNull((String)theModel.getValueAt(i,6));

               if(SRepName.length() == 0)
               {
                    JOptionPane.showMessageDialog(null,"Please Fill the RepName in Row "+(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    setTableSelection(i);
                    return false;
               }

               if(SToMeet.length() == 0)
               {
                    JOptionPane.showMessageDialog(null,"Please Fill the Whom To Meet in Row "+(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    setTableSelection(i);
                    return false;
               }

               if(SCardNo.length() == 0)
               {
                    JOptionPane.showMessageDialog(null,"Please Fill the Card No. in Row "+(i+1),"Error",JOptionPane.ERROR_MESSAGE);
                    setTableSelection(i);
                    return false;
               }
          }

          return true;
     }

     private void setTableSelection(int i)
     {
          try
          {
               theTable       . setRowSelectionInterval(i,i);
               Rectangle Rect = theTable.getCellRect(i,0,true);
               theTable       . scrollRectToVisible(Rect);
          }
          catch(Exception ex){}
     }

     private void SaveDetails()
     {
          try
          {
               CallableStatement theCallable = theConnection.prepareCall("Call InsertGateVisitorEntry(?,?,?,?,?,?,?,?,?)");
               String SPartyCode             = getPartyCode(common.parseNull((String)JCPartyName.getSelectedItem()));

               for(int i=0;i<theModel.getRowCount();i++)
               {
                    String SRepName          = common.parseNull((String)theModel.getValueAt(i,1));
                    String SPurpose          = common.parseNull((String)theModel.getValueAt(i,2));
                    String SVehicleNo        = common.parseNull((String)theModel.getValueAt(i,3));
                    String SDriverName       = common.parseNull((String)theModel.getValueAt(i,4));
                    String SToMeet           = common.parseNull((String)theModel.getValueAt(i,5));
                    String SCardNo           = common.parseNull((String)theModel.getValueAt(i,6));
                    String SInTime           = common.parseNull((String)theModel.getValueAt(i,7));

                    theCallable              . setString(1,SPartyCode);
                    theCallable              . setInt(2,common.toInt(TDate.toNormal()));
                    theCallable              . setString(3,SRepName);
                    theCallable              . setString(4,SPurpose);
                    theCallable              . setString(5,SVehicleNo);
                    theCallable              . setString(6,SDriverName);
                    theCallable              . setString(7,SToMeet);
                    theCallable              . setString(8,SCardNo);
                    theCallable              . setString(9,SInTime);

                    theCallable              . executeUpdate();
               }

               JOptionPane.showMessageDialog(null,"Data Saved Successfully","Information",JOptionPane.INFORMATION_MESSAGE);
               getApplyAction();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Data Not Saved..\n"+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
               ex.printStackTrace();
          }
     }

     private void UpdateDetails()
     {
          String SPartyCode                  = getPartyCode(common.parseNull((String)JCPartyName.getSelectedItem()));
          String SOutTime                    = (String)theModel.getValueAt(0,8);

          try
          {
               Statement theStatement        = theConnection.createStatement();
               theStatement                  . executeQuery("Update GateVisitorEntry set OutTime = '"+SOutTime+"' where PartyCode = '"+SPartyCode+"' and VisitDate = "+TDate.toNormal());
               theStatement                  . executeQuery("Update VisitorEntry set GateOutStatus = 1 where PartyCode = '"+SPartyCode+"' and VisitDate = "+TDate.toNormal());
          
               JOptionPane.showMessageDialog(null,"Data Sucessfully Updated","Information",JOptionPane.INFORMATION_MESSAGE);
               getApplyAction();
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Data Not Saved..\n"+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
               ex.printStackTrace();
          }
     }

     private void setVectorValues()
     {
          theVector      = new Vector();
          theVector      . removeAllElements();

          theConnection  = null;

          STime          = "";

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement theStatement   = theConnection.createStatement();

               ResultSet rs             = theStatement.executeQuery(getQS());
               while(rs.next())
               {
                    PartyVisitorClass   theClass = null;

                    String SPartyCode   = common.parseNull(rs.getString(1));
                    String SPartyName   = common.parseNull(rs.getString(2));
                    String SSubject     = common.parseNull(rs.getString(3));
                    String SVisitDate   = common.parseNull(rs.getString(4));
                    String SVisitInTime = common.parseNull(rs.getString(5));
                    String SOutTime     = common.parseNull(rs.getString(6));

                    theClass            = new PartyVisitorClass(SPartyCode,SPartyName,SSubject,SVisitDate,SVisitInTime,SOutTime);

                    theVector           . addElement(theClass);
               }
               rs                       . close();

               rs                       = theStatement.executeQuery("Select to_Char(SysDate,'hh12:mi:ss AM') from Dual");
               if(rs.next())
               {
                    STime               = rs.getString(1);
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void setAlreadyInData()
     {
          theInVector                   = new Vector();

          String SPartyCode             = getPartyCode(common.parseNull((String)JCPartyName.getSelectedItem()));
          
          try                           
          {
               Statement theStatement   = theConnection.createStatement();
               ResultSet rs             = theStatement.executeQuery(getInQS(SPartyCode));

               while(rs.next())
               {
                    PartyVisitorClass theClass = null;

                    String SInRepName   = common.parseNull(rs.getString(1));
                    String SInPurpose   = common.parseNull(rs.getString(2));
                    String SInVehicleNo = common.parseNull(rs.getString(3));
                    String SInDriverName= common.parseNull(rs.getString(4));
                    String SInToMeet    = common.parseNull(rs.getString(5));
                    String SInCardNo    = common.parseNull(rs.getString(6));
                    String SInInTime    = common.parseNull(rs.getString(7));
                    String SInOutTime   = common.parseNull(rs.getString(8));

                    theClass            = new PartyVisitorClass(SInRepName,SInPurpose,SInVehicleNo,SInDriverName,SInToMeet,SInCardNo,SInInTime,SInOutTime);

                    theInVector         . addElement(theClass);
               }

               rs                       . close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getInQS(String SPartyCode)
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select GateVisitorEntry.RepName,GateVisitorEntry.Purpose,GateVisitorEntry.VehicleNo, ");
          SB.append(" GateVisitorEntry.DriverName,GateVisitorEntry.ToMeet,GateVisitorEntry.CardNo,GateVisitorEntry.InTime,GateVisitorEntry.OutTime from GateVisitorEntry ");
          SB.append(" Inner Join VisitorEntry on VisitorEntry.PartyCode = GateVisitorEntry.PartyCode ");
          SB.append(" where GateVisitorEntry.PartyCode = '"+SPartyCode+"' and GateVisitorEntry.VisitDate = "+TDate.toNormal()+" ");
//          SB.append(" and VisitorEntry.CompleteStatus = 1 ");

          return SB.toString();
     }

     private String getQS()
     {
          StringBuffer SB = new StringBuffer();

          SB.append(" Select VisitorEntry.PartyCode,Accounts.Name,VisitorEntry.Subject,VisitDate,VisitInTime,VisitOutTime from VisitorEntry ");
          SB.append(" Inner Join Accounts on Accounts.AcCode = VisitorEntry.PartyCode ");
          SB.append(" where VisitDate = "+TDate.toNormal()+" and ConfirmStatus = 1 and GateOutStatus = 0 ");

          return SB.toString();
     }
}
