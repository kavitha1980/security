/*                                              

     To acquire details of visitor(s) outward

   
*/
package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.util.Date;
import java.sql.*;
import java.util.GregorianCalendar;

import utility.*;
import utility.Control;

import util.DateField1;
import util.TimeField;
import util.Common;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;


public class VisitorOutFrame extends JInternalFrame implements rndi.CodedNames
{

      protected JLayeredPane       Layer;

      JTabbedPane        outTabPane;

      JPanel             TopPanel,MiddlePanel,BottomPanel;
      
      JTextField         TCardNo;
      JTextField         TCompany;
      JTextField         TSlipNo;
      
      DateField1          TDate;
      JTextField         TInTime;
      JTextField         TOutTime;
      JCheckBox          VisitorOutChk;
      
      VisitorOutModel    theOutModel;
      
      JTable             theTable;
      
      TimeField          theTime;
      
      JButton            BOkay,BExit;
      
      Control            control;
      Common             common;
      
      Vector             vRepName;
      Vector             vPurpose;
      Vector             vStaff;
      Vector             vCardNo;
      Vector             vInTime;
      Vector             vRepCode;
     //int                chkCardNo=0;
      String chkCardNo="";
      VisitorIn visitorDomain;

      public VisitorOutFrame(JLayeredPane Layer)
      {

            try
            {
                  this.Layer = Layer;
                  setDomain();
                  createComponents();
                  setLayouts();
                  addComponents();
                  addListeners();
            }
            catch(Exception ex)
            {
                  System.out.println(ex+"61");
            }
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      private void createComponents()
      {

            outTabPane     = new JTabbedPane();

            TopPanel       = new JPanel(true);

            MiddlePanel    = new JPanel(true);
            BottomPanel    = new JPanel(true);

            TCardNo        = new JTextField(8);
            TCompany       = new JTextField();
            TSlipNo        = new JTextField();

            TDate          = new DateField1();
            TInTime        = new JTextField();
            TOutTime       = new JTextField();
            VisitorOutChk  = new JCheckBox();

         // TRegNo         = new JTextField();
          
            theOutModel    = new VisitorOutModel();
            theTable       = new JTable(theOutModel);

            theTime        = new TimeField();

            BOkay          = new JButton("Okay");
            BExit          = new JButton("Exit");
            BOkay.setMnemonic('O');
            BExit.setMnemonic('X');

            common         = new Common();
            control        = new Control();

      }

      private void setLayouts()
      {

            setTitle("Visitor Information Feeder - Outward(4.0)");
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,798,520);

            TopPanel.setLayout(new GridLayout(2,3,10,10));
            MiddlePanel.setLayout(new BorderLayout());

            TopPanel.setBorder(new TitledBorder("Company"));
            MiddlePanel.setBorder(new TitledBorder("Visitor Information"));

            TCardNo.setBorder(new TitledBorder("Card No"));
            TCompany.setBorder(new TitledBorder("Company"));
            TSlipNo.setBorder(new TitledBorder("Slip No"));

            TDate.setBorder(new TitledBorder("Date"));
            TInTime.setBorder(new TitledBorder("In-Time"));
            TOutTime.setBorder(new TitledBorder("Out-Time"));          

            TCompany.setEditable(false);
            TSlipNo.setEditable(false);

            TInTime.setEditable(false);
            TOutTime.setEditable(false);

            setPresets();
      }


      private void addComponents()
      {
            TopPanel.add(TCardNo);
            TopPanel.add(TCompany);
            TopPanel.add(TSlipNo);

            TopPanel.add(TDate);
            TopPanel.add(TInTime);
            TopPanel.add(TOutTime);

            MiddlePanel.add("North",theTable.getTableHeader());
            MiddlePanel.add("Center",new JScrollPane(theTable));

            outTabPane.setFont(new Font("Copperplate Gothic Bold",Font.BOLD,11));
            outTabPane.addTab("VisitorInfo",MiddlePanel);
            outTabPane.addTab("Today's Visitor's Out Info",new VisitorsTabOutModel());

            BottomPanel.add(BOkay);
            BottomPanel.add(BExit);

            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",outTabPane);
            getContentPane().add("South",BottomPanel);
      }

      private void addListeners()
      {
            TCardNo.addKeyListener(new KeyList());
            BOkay.addActionListener(new SaveList());
            BExit.addActionListener(new exitList());

      }
      private class exitList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVisible(false);
            }
      }

      private class KeyList extends KeyAdapter
      {

            public void keyPressed(KeyEvent ke)
            {

                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     
                        try
                        {
                              
                               //chkCardNo = Integer.parseInt(TCardNo.getText());
                                 chkCardNo   = common.parseNull((TCardNo.getText().trim()).toUpperCase());

                              if(chkCardNo.equals(""))
                              {
                                   JOptionPane.showMessageDialog(null,"Plz Enter Card No");
                              }
                              else
                              {
                                   checkData();

                                   theOutModel.setCardNo(chkCardNo,vRepCode,vRepName,vPurpose,vStaff,vCardNo);

                              }
                        }
                        catch(Exception ne)
                        {
                              //JOptionPane.showMessageDialog(null,"Numbers only allowed!...Please Enter a Number...");
                              TCardNo.setText("");
                        }
                           
                  }
            }
      }

      private class SaveList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  BOkay.setEnabled(false);

                  theOutModel.saveData();
                  setVisible(false);
                  //refresh();
            }
      }


      private void refresh()
      {
            setPresets();
            BOkay.setEnabled(true);
      }


      private void setPresets()
      {
            TDate.setTodayDate(); 
            TDate.setEditable(false);
            TOutTime.setText(getCurrentTime());
      }

      private String getCurrentTime()
      {
            return theTime.getTimeNow();
      }


      public class VisitorsTabOutModel extends JPanel
      {
          JTable todaytable;

          public VisitorsTabOutModel()
          {
               try
               {
                    DateField1 da = new DateField1();
                    
                    da.setTodayDate();
                    int iDate      = Integer.parseInt(da.toNormal());

                    setLayout(new BorderLayout());
                    Vector initVector = visitorDomain.getVisitorOutInfo(iDate,iDate);    
                    VisitorsTodayInModel tmodel = new VisitorsTodayInModel(initVector);
                    todaytable = new JTable(tmodel);
                    //JScrollPane sPane = new JScrollPane(todaytable);
                    setPrefferredWidth();
                    add(todaytable.getTableHeader());
                    add(new JScrollPane(todaytable));
               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }
          }
          private void setPrefferredWidth()
          {
               todaytable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (todaytable.getColumn("CompanyName")).setPreferredWidth(200);
               (todaytable.getColumn("SlipNo")).setPreferredWidth(80);
               (todaytable.getColumn("Representative Name")).setPreferredWidth(150);
               (todaytable.getColumn("Purpose")).setPreferredWidth(150);
               (todaytable.getColumn("To Meet")).setPreferredWidth(150);
//               (todaytable.getColumn("CardNo")).setPreferredWidth(60);
               (todaytable.getColumn("InTime")).setPreferredWidth(100);
               (todaytable.getColumn("OutTime")).setPreferredWidth(100);
               (todaytable.getColumn("VisitorDate")).setPreferredWidth(100);

          }

     }


      public void checkData()
      {
            vRepCode = new Vector();
            vRepName = new Vector();
            vPurpose = new Vector();
            vStaff   = new Vector();
            vCardNo  = new Vector();

            try
            {
                  //"Select cardno,slipno from visitor where Out = No and cardno = "+chkCardNo+" ";


                  int islipno = visitorDomain.getSlipNo(chkCardNo);

                  

                  /*VQS =

                  "Select Visitor.slipno,Company.Name,Representative.Name,VisitorPurpose.Name,"+
                  " Staff.Staffname,Visitor.cardno,Visitor.intime,Visitor.repcode"+
                  " from"+
                  " Company,Representative,VisitorPurpose,Staff,Visitor"+
                  " where"+
                  " Visitor.CompanyCode =   Company.code and"+
                  " Visitor.repcode     =   Representative.code and"+
                  " Visitor.purposecode =   VisitorPurpose.Code and"+
                  " Visitor.staffcode   =   Staff.staffcode and"+
                  " Visitor.out         =   No and"+
                  " Visitor.slipno      =   "+islipno+" ";

                  ResultSet result         = stat.executeQuery(VQS);*/

                  Vector vect      = new Vector();

                  vect             = visitorDomain.getVect(islipno);
                 

                  int x=0;

                  int m=0;

                  for(int i=0;i<(vect.size()/8);i++) 
                  {
                          if (x==0)
                          {
                                 TSlipNo.setText((String)vect.elementAt(0));
                                 TCompany.setText((String)vect.elementAt(1));
                                 TInTime.setText((String)vect.elementAt(6));
                           }

                           vRepName.addElement((String)vect.elementAt(2+m));
                           vPurpose.addElement((String)vect.elementAt(3+m));
                           vStaff.addElement((String)vect.elementAt(4+m));
                           vCardNo.addElement((String)vect.elementAt(5+m));
                           vRepCode.addElement((String)vect.elementAt(7+m));
                           m=m+8;
                           x=x+1;
               
                  }
            }
         catch(Exception ex)
         {
               JOptionPane.showMessageDialog(null,"There is no matching Records...");
               //System.out.println(ex);
         }
          
      }
}
