/*

     The Representative Object is meant for
     allowing the User to select a Representative
     belongs to a particular Company.

     If the Representative in ? is not available a provision
     to insert On line the new Representative.

*/
package client.visitor;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;

import util.Common;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
public class PurposeModelNew implements rndi.CodedNames
{

      protected VisitorInModelNew theModel;
      protected int iRow,iCol;
      VisitorIn visitorDomain;
      JTextField     TName,TCode;
      Vector         VCode,VName;
      Vector         VSelectedPurposeCode;

      Common common = new Common();

      public PurposeModelNew(VisitorInModelNew theModel)
      {
            this.theModel = theModel;
            setDomain();
            TName = new JTextField();
            TCode = new JTextField();

            VSelectedPurposeCode = new Vector();

      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      public void showPurposeFrame(int iRow,int iCol)
      {
            this.iRow = iRow;
            this.iCol = iCol;

            setDataIntoVector();

            TCode.setText(theModel.getPurpose(iRow));

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Purpose Selector",true); 
            PurposeSearchPanelNew PSP = new PurposeSearchPanelNew(this,TCode,TName,VCode,VName,theDialog);

            theDialog.getContentPane().add(PSP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {

            String SCode = TCode.getText();

            int index    = VCode.indexOf(SCode);

            if(index == -1)
                  setData();
            else
                  TCode.setText(SCode);


            theModel.setPurpose(TCode.getText(),TName.getText(),iRow);

            VSelectedPurposeCode.addElement(TCode.getText());
      }

      private void setData()
      {

            //String QS1 = "Insert Into VisitorPurpose (Name) Values ("+
                  //"'"+TName.getText()+"')";

            //String QS2 = "Select Max(Code) From VisitorPurpose";

            try
            {
                  String SMax        = visitorDomain.getMaxPurposeCode();
                  int iMax           = Integer.parseInt(SMax);
                  iMax               = iMax+1;
                  SMax               = Integer.toString(iMax);
                  visitorDomain.insertVisitorPurpose((String)TName.getText(),SMax);

                  //ResultSet result = stat.executeQuery(QS2);
                  //if(result.next())
                        TCode.setText(SMax);

                  //conn.close();
                  setDataIntoVector();
            }
            catch(Exception ex)
            {
                  //System.out.println(ex);
            }
      }

      private void setDataIntoVector()
      {
            VCode        = new Vector();
            VName        = new Vector();
            Vector VTemp        = new Vector();       

            //String QString = " Select Code,Name From VisitorPurpose  "+
                  //" Order By 2";
            int m=0;
            try
            {
                  //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                  //Connection conn  = DriverManager.getConnection(common.getDSN(),"","");
                  // Statement stat   = conn.createStatement();
                  //ResultSet res = stat.executeQuery(QString);
                  //while(res.next())
                  VTemp  = visitorDomain.getPurposeInfo();
                  int size= (VTemp.size())/2;
                  for(int i=0;i<size;i++)
                  {
                        VCode.addElement(VTemp.elementAt(0+m));
                        VName.addElement(VTemp.elementAt(1+m));
                        m=m+2;
                  }
                  //conn.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

}

