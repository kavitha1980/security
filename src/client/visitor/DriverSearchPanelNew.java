/*
      The Component for Preparing the Selection Object
      usually instantiated from Search and Selection Component
      as a part of the JOptionPane
*/
package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

import util.Common;
import util.TimeField;
import util.DateField;

public class DriverSearchPanelNew extends JPanel
{
           
      protected DriverModelNew theModel;
      protected JTextField TCode,TName;
      protected Vector     VCode,VName;
      protected JDialog    theDialog;

      JPanel         BottomPanel;
      JTextField     TIndicator;
      JList          BrowList;

      String str="";
      String SPreCode="";

      Common common = new Common();
           
      public DriverSearchPanelNew(DriverModelNew theModel,JTextField TCode,JTextField TName,Vector VCode,Vector VName,JDialog theDialog)
      {
            this.theModel = theModel;
            this.TCode    = TCode;
            this.TName    = TName;
            this.VCode    = VCode;
            this.VName    = VName;
            this.theDialog= theDialog;

            TIndicator    = new JTextField();
            BrowList      = new JList(VName);
            BottomPanel   = new JPanel(true);
                        
            setLayout(new BorderLayout());
            BottomPanel.setLayout(new GridLayout(1,2));

            BottomPanel.add(TIndicator);
            BottomPanel.add(TCode);

            add("Center",new JScrollPane(BrowList));
            add("South",BottomPanel);
      
            BrowList.addKeyListener(new KeyList());
            BrowList.addMouseListener(new MouseList());

            TIndicator.setEditable(false);

            setPreferredSize(new Dimension(400,350));

            preset();
      }
     
      private class KeyList extends KeyAdapter
      {
            public void keyReleased(KeyEvent ke)
            {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {            
                        if(ke.getKeyCode()==8)
                        {
                              str=str.substring(0,(str.length()-1));
                              setCursor();
                        }
                        else

                              if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar>='0' && lastchar <= '9'))
                              {
                                    str=str+lastchar;
                                    setCursor();
                              }
                              else
                                    setCode();
                  }
                  catch(Exception ex){}
            }
            public void keyPressed(KeyEvent ke)
            {
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                        setSelectionDet();
                        str="";
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                  {
                        setPreviousDet();
                  }
            }
      }

      private class MouseList extends MouseAdapter
      {

            public void mouseClicked(MouseEvent me)
            {
                  setCode();
            }
      }

      private void setCursor()
      {
            TIndicator.setText(str);            
            int index=0;
            for(index=0;index<VName.size();index++)
            {
                  String str1 = ((String)VName.elementAt(index)).toUpperCase();
                  if(str1.startsWith(str))
                  {
                        BrowList.setSelectedValue(str1,true);
                        TCode.setText((String)VCode.elementAt(index));
                        BrowList.ensureIndexIsVisible(index+10);
                        return;
                  }
            }
            TCode.setText("");
      }
                
      private void setCode()
      {
            int i = BrowList.getSelectedIndex();                  
            TCode.setText((String)VCode.elementAt(i));
      }
      
      public void setSelectionDet()
      {
            int index = VCode.indexOf(TCode.getText());
            if(index == -1)
            {
                  TName.setText(TIndicator.getText());
            }
            else
            {
                  TName.setText((String)VName.elementAt(index));
            }
            theDialog.setVisible(false);
            theModel.setDetails();
      }

      public void setPreviousDet()
      {

            TCode.setText(SPreCode);
            theDialog.setVisible(false);
      }


      public void preset()
      {
            BrowList.requestFocus();
            SPreCode = TCode.getText();
            int index  = VCode.indexOf(SPreCode);
            if(index == -1)
                  return;
            String xtr = (String)VName.elementAt(index);            
            for(index=0;index<VName.size();index++)
            {
                  String str1 = ((String)VName.elementAt(index)).toUpperCase();
                  if(str1.startsWith(xtr))
                  {
                        BrowList.setSelectedValue(str1,true);
                        break;
                  }
            }
      }
}

