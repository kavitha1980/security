/*                             
      The Model for handling Visitor informations
      Name
      CardNo
      Purpose
      To Meet whom
*/
package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;
import java.sql.*;

import util.RowSet;
import util.TimeField;
import util.DateField1;
import util.Common;

import blf.*;

import java.net.*;

import java.rmi.*;
import java.rmi.registry.*;

public class VisitorInModel extends DefaultTableModel implements rndi.CodedNames
{

      protected CompanyModel theCompany;


      // Column Ids
      int SLNO   = 0;
      int REP    = 1;
      int PURPOSE= 2;
      int STAFF  = 3;
      int CARDNO = 4;  
      int DEPT   = 4;    

      RepModel     theRep;
      PurposeModel thePurpose;
      StaffModel   theStaff;
      TimeField    theTime;
      DateField1    theDate;
      VisitorIn visitorDomain;

      Vector VDeptCode,VRepCode,VPurposeCode,VStaffCode;

      String ColumnName[] = {"Sl No","Representative","Purpose","To meet","Card No"};

      String ColumnType[] = {  "S"  ,      "S"       ,   "S"   ,   "S"   , "E"   };
     
      Common common = new Common();

      RowSet rowset,rowset1;
 
      //-- The Constructor Method as referenced by the VisitorFrame


      public VisitorInModel(CompanyModel theCompany)
      {
            this.theCompany = theCompany;

            VRepCode        = new Vector();
            VPurposeCode    = new Vector();
            VStaffCode      = new Vector();
            VDeptCode       = new Vector();

            theRep          = new RepModel(this);
            thePurpose      = new PurposeModel(this);
            theStaff        = new StaffModel(this);

            theDate         = new DateField1();

            setDomain();
            setDataVector(getRowData(),ColumnName);

            removeRow(0);
            appendRow();
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private Object[][] getRowData()
      {
            Object RowData[][] = new Object[1][ColumnName.length];
            for(int i=0;i<ColumnName.length;i++)
            {
                  RowData[0][i] = "";

            }
            return RowData;
      }

      //-- Overriding the methods of Default Table Model for additional features


      public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }


      public boolean isCellEditable(int row,int col)
      {
            if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
            return false;
      }

      boolean okflag=true;
      public void setValueAt(Object aValue, int row, int column)
      {
            try
            {
                  Vector rowVector = (Vector)super.dataVector.elementAt(row);
                  rowVector.setElementAt(aValue, column);

                  if (column==5)
                  {
                        try
                        {
                              String Scol4 = common.parseNull((String)rowVector.elementAt(4));
                              if(Scol4.equals(""))
                              {
                                   JOptionPane.showMessageDialog(null,"CardNo must be filled");
                              }                   
                              /*else
                              {
                                   String QS = "Select Out from Visitor where CardNo='"+Scol4+"' And OutTime='0' " ;
                                   String st =  visitorDomain.getVisitorInitialCheck(QS);
                                   if(st==null)
                                   {
                                        okflag=true;
                                   }
                                   else
                                   {
                                        okflag=false;
                                        JOptionPane.showMessageDialog(null,"Already a person having This Card");
                                   }
                              }*/
                        }
                        catch(Exception ne)
                        {
                              //JOptionPane.showMessageDialog(null,"Numbers only allowed!...Please Enter a Number...");
                              rowVector.set(4,"");
                        }
                        fireTableChanged(new TableModelEvent(this, row, row, column));
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public int getRows()
      {

            return super.dataVector.size();
      }
      public void refresh()
      {
          setNumRows(0);
      }

      public void appendRow()
      {
            Vector vect = new Vector();
            for(int i=0;i<ColumnName.length;i++)                     
            {
                  vect.addElement("");

                  VRepCode.addElement("-1");
                  VPurposeCode.addElement("-1");
                  VStaffCode.addElement("-1");
                  VDeptCode.addElement("-1");
            }               
            insertRow(getRows(),vect);
            setRowId();
      }

      public void deleteRow(int iRow)
      {
            if(getRows()==1)
                  return;
            removeRow(iRow);

            VRepCode.removeElementAt(iRow);
            VPurposeCode.removeElementAt(iRow);
            VStaffCode.removeElementAt(iRow);
            VDeptCode.removeElementAt(iRow);
            setRowId();
      }

      private void setRowId()
      {
            for(int i=0;i<getRows();i++)
            {
                  setValueAt(String.valueOf(i+1),i,SLNO);
            }
      }


      public void showHelpFrame(int iRow,int iCol)
      {
            if(iCol == REP)
            {
                  theRep.showRepFrame(iRow,iCol);
            }
            if(iCol == PURPOSE)
            {
                  thePurpose.showPurposeFrame(iRow,iCol);
            }
            if(iCol == STAFF)
            {
                  theStaff.showStaffFrame(iRow,iCol);               
            }
            /*if(iCol == DEPT)
            {
                  theDept.showDeptFrame(iRow,iCol);
            } */ 
      }

      public String getCompanyCode()
      {
            return theCompany.getCode();
      }

    
      public void setRep(String SCode,String SName,int iRow)
      {
            try
            {
                  VRepCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,REP);
            }
            catch(Exception ex){}
      }

      public void setPurpose(String SCode,String SName,int iRow)
      {
            try
            {
                  VPurposeCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,PURPOSE);
            }
            catch(Exception ex){}
      }

      public void setStaff(String SCode,String SName,int iRow)
      {
            try
            {
                  VStaffCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,STAFF);
            }
            catch(Exception ex){}
      }

      public String getRep(int iRow)
      {
            return (String)VRepCode.elementAt(iRow);
      }

      public String getPurpose(int iRow)
      {
            return (String)VPurposeCode.elementAt(iRow);
      }

      public String getStaff(int iRow)
      {
            return (String)VStaffCode.elementAt(iRow);
      }
      public String getCard(int iRow)
      { 
            return ((String)getValueAt(iRow,CARDNO)).toUpperCase();
      }

      public boolean isRepInList(String SCode)
      {
            int index = VRepCode.indexOf(SCode);
            if(index == -1)
                  return false;
            return true;
      }

      public void saveData(String slipno)
      {
            try
            {
                  int ccode = Integer.parseInt((String)theCompany.TCode.getText());

                  theTime = new TimeField();
                  String theSTime =theTime.getTimeNow();

                  theDate.setTodayDate();

                  //if(checkDate(theDate.toNormal()))
                  //{
                       int vslipno = Integer.parseInt(slipno);
                    
                       for(int i=0;i<theRep.VSelectedRepCode.size();i++)
                       {
                             int rpcode = Integer.parseInt((String)theRep.VSelectedRepCode.elementAt(i));
                             int ppcode = Integer.parseInt((String)thePurpose.VSelectedPurposeCode.elementAt(i));
                             int stcode = Integer.parseInt((String)theStaff.VSelectedStaffCode.elementAt(i));
                             int slno    = visitorDomain.getSlNo("visitor_seq");

                             Vector vect = new Vector();
                             vect.addElement(String.valueOf(slno));
                             vect.addElement(String.valueOf(vslipno));
                             vect.addElement(String.valueOf(ccode));
                             vect.addElement(String.valueOf(rpcode));
                             vect.addElement(String.valueOf(ppcode));
                             vect.addElement(String.valueOf(stcode));
                             vect.addElement(getCard(i));
                             vect.addElement(theSTime);
                             int iDate = common.toInt(common.pureDate(theDate.toNormal()));
                             vect.addElement(String.valueOf(iDate));
     
                             visitorDomain.saveData(slipno,vect);


                             /*rowset    = new RowSet();
                              
                             Vector VMessage           = new Vector();
                             try
                             {
                    
                                   int iDepCode        = visitorDomain.getDeptCode(stcode);
                                   int iHodCode        = visitorDomain.getHodCode(iDepCode);
                                   String SHost        = visitorDomain.getHostId(iDepCode);

                                   String STime             = common.getTime();

                                   VMessage.addElement(String.valueOf(10));
                                   VMessage.addElement(String.valueOf(1));
                                   VMessage.addElement(String.valueOf(iDate));
                                   VMessage.addElement(STime);

                                   String SMsg=" A Visitor coming to ur Department";

                                   rowset.appendRow(VMessage);

                                   int iId                  = visitorDomain.getNextId(); 
                                                            

                                   iId                      = iId+1;

                                   Vector thePrimaryVector  =  visitorDomain.sendMessageSource(iId,rowset);


                                   visitorDomain.setMessageText(SMsg,iId);
                    
                    
                                   for(int j=0; j<thePrimaryVector.size(); j++)
                                   {
                                        rowset1       = new RowSet(); 
                                        Vector VTarget            = new Vector();
                                        VTarget.addElement(String.valueOf(iId));
                                        VTarget.addElement(String.valueOf(iHodCode));
                                        VTarget.addElement(String.valueOf(0));
                                        rowset1.appendRow(VTarget);
                                        visitorDomain.sendMessageTarget(rowset1);
                                   }                                          
                              }
                              catch(Exception ex)
                              {
                                   System.out.println("Persist"+ex);
                              }*/

                       }
                        
                  //}
                  /*else
                  {
                    
                       JOptionPane.showMessageDialog(null,"Date should be in Current Financial Year"); 
                  }*/                    
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
      
      public boolean checkDate(String Date)
      {
            boolean flag=false;
            /*try
            {

                  Vector VDate      = visitorDomain.getFinanceYear();
                  int stDate        = Integer.parseInt((String)VDate.elementAt(0));
                  int eDate         = Integer.parseInt((String)VDate.elementAt(1));

                  int idate         = common.toInt(common.pureDate(Date));

                  if( (idate >=stDate ) && (idate<=eDate) )
                        flag =  true;
                  
            }
            catch(Exception e)
            {
                  e.printStackTrace();
            }*/
            return flag;
      }

}


                          


