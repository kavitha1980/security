/*

     The Company Object is meant for
     allowing the User to select a company a visitor
     belongs to.

     If the company in ? is not available a provision
     to insert On line the new Company.

*/
package client.visitor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;

import util.Common;
import java.rmi.*;
import java.rmi.registry.*;

import blf.*;

public class CompanyModel extends JButton implements ActionListener,rndi.CodedNames
{

      JTextField     TCode;
      Vector         VCode,VName;
     
      Common common = new Common();
      VisitorIn visitorDomain;
      public CompanyModel()
      {
            setDomain();
            TCode = new JTextField();
            setText("Company");
            setBorder(new javax.swing.border.BevelBorder(0));
            setBackground(new Color(128,128,255));
            setForeground(Color.white);

            setDataIntoVector();
           
            addActionListener(this);
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn) registry.lookup(SECURITYDOMAIN);

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      public void actionPerformed(ActionEvent ae)
      {

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Company Selector",true); 
            SearchPanel SP     = new SearchPanel(this,TCode,VCode,VName,theDialog);

            theDialog.getContentPane().add(SP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {
            String SName = getText();
            String SCode = TCode.getText();

            int index = VCode.indexOf(SCode);

            if(index == -1)
                  setData();
            else
            {
                  TCode.setText(SCode);
            }   
            setEnabled(false);

      }

      private void setData()
      {
            String QS1 = "Insert Into Company (Name) Values ("+
                  "'"+getText()+"')";


            //String QS2 = "Select Max(Code) From Company ";

            try
            {
                  String SCode       = visitorDomain.getMaxCompanyCode();
                  int iCode          = Integer.parseInt(SCode);
                  iCode  = iCode+1;
                  SCode              = Integer.toString(iCode);
                  visitorDomain.insertCompanyName(getText(),SCode);
                  //      TCode.setText(visitorDomain.getNextSlipNo());

                  //conn.close();
                  setDataIntoVector();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public void setDataIntoVector()
      {
            VCode      = new Vector();
            VName      = new Vector();

            try
            {
                  Vector VTemp     = new Vector();
                  VTemp            = visitorDomain.getCompanyInfo(1);
                  int m=0;
                  int size=(VTemp.size())/2;
                  for(int i=0;i<size;i++)
                  {
                        VCode.addElement(VTemp.elementAt(0+m));
                        VName.addElement(VTemp.elementAt(1+m));
                        m=m+2; 
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public String getCode()
      {
            return TCode.getText();
      }
     
}
