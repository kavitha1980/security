
package client.visitor;
import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;
import guiutil.*;


public class NewKeyEntryModel extends DefaultTableModel{
    String ColumnName[] = {"KeyCode","KeyName"};
    String ColumnType[] = { "S", "S"};
    int  iColumnWidth[] = {80  ,80    };

     Common common = new Common();
     Vector vect;
 public NewKeyEntryModel(){
          setDataVector(getRowData(),ColumnName);
 }
public Class getColumnClass(int iCol){
          return getValueAt(0,iCol).getClass();
}

public boolean isCellEditable(int iRow,int iCol){
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
 }

private Object[][] getRowData(){

          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
 }

 public void appendRow(Vector theVect){
          insertRow(getRows(),theVect);
 }

public int getRows(){

          return super.dataVector.size();
     }
}
