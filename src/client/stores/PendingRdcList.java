package client.stores;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import utility.*;

public class PendingRdcList implements ActionListener
{
      JLayeredPane     Layer;
      GIRdcMiddlePanel    MiddlePanel;

      JList          BrowList,SelectedList;
      JScrollPane    BrowScroll,SelectedScroll;
      JTextField     TIndicator;
      JButton        BOk;
      JPanel         LeftPanel,RightPanel;
      JInternalFrame MaterialFrame;
      JPanel         MFMPanel,MFBPanel;

      Vector VPendName;
      Vector VSelectedName;
      JTextField TSupCode;     
      String str="";
      int iMFSig=0;
      ActionEvent ae;
      Common common = new Common();
      PendingRdcList(JLayeredPane Layer,GIRdcMiddlePanel MiddlePanel,JTextField TSupCode)
      {
          this.Layer         = Layer;
          this.MiddlePanel   = MiddlePanel;
          this.TSupCode      = TSupCode;
      }

      public void createComponents()
      {
          VPendName = new Vector();
          VSelectedName = new Vector();

          BrowList      = new JList(getPendName());
          SelectedList  = new JList();
          BrowScroll    = new JScrollPane(BrowList);
          SelectedScroll= new JScrollPane(SelectedList);
          LeftPanel     = new JPanel(true);
          RightPanel    = new JPanel(true);
          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);
          MFMPanel      = new JPanel(true);
          MFBPanel      = new JPanel(true);
          MaterialFrame = new JInternalFrame("Materials Expected From this Supplier");
          MaterialFrame.show();
          MaterialFrame.setBounds(80,100,550,350);
          MaterialFrame.setClosable(true);
          MaterialFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          BrowList.requestFocus();
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               if(VSelectedName.size()==0)
               {
                    JOptionPane.showMessageDialog(null,"No Material is Selected","Information",JOptionPane.INFORMATION_MESSAGE);
                    BrowList.requestFocus();
                    return;
               }
               BOk.setEnabled(false);
               setMiddlePanel();
               removeHelpFrame();
               ((JButton)ae.getSource()).setEnabled(false);
               ((JButton)ae.getSource()).requestFocus();
               str="";
          }
      }
      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;
          JButton source = (JButton)ae.getSource();
          createComponents();
          if(VPendName.size()==0)
          {
                JOptionPane.showMessageDialog(null,"No Materials are Pending","Information",JOptionPane.INFORMATION_MESSAGE);                
                return;
          }
          TIndicator.setText(str);
          BOk.setEnabled(true);
          source.setEnabled(false);
          if(iMFSig==0)
          {
               MFMPanel.setLayout(new GridLayout(1,2));
               MFBPanel.setLayout(new GridLayout(1,2));
               MFMPanel.add(BrowScroll);
               MFMPanel.add(SelectedScroll);
               MFBPanel.add(TIndicator);
               MFBPanel.add(BOk);
               BOk.addActionListener(new ActList());
               MaterialFrame.getContentPane().add("Center",MFMPanel);
               MaterialFrame.getContentPane().add("South",MFBPanel);
               iMFSig=1;
          }
          removeHelpFrame();
          try
          {
               Layer.add(MaterialFrame);
               MaterialFrame.moveToFront();
               MaterialFrame.setSelected(true);
               MaterialFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  
      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar=='"') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     String SMatName = (String)VPendName.elementAt(index);
                     addMatDet(SMatName);
                     str="";
                     TIndicator.setText(str);
                  }
                  if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                  {
                     setMiddlePanel();
                     removeHelpFrame();
                     ((JButton)ae.getSource()).setEnabled(false);
                     str="";
                  }

             }
         }
         public void setCursor()
         {
            int index=0;
            TIndicator.setText(str);
            for(index=0;index<VPendName.size();index++)
            {
                 String str1 = ((String)VPendName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);   
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MaterialFrame);
               Layer.repaint();
               Layer.updateUI();
            }
            catch(Exception ex) { }
         }
         public boolean addMatDet(String SMatName)
         {
               int iIndex=VSelectedName.indexOf(SMatName);
               if (iIndex==-1)
               {
                    VSelectedName.addElement(SMatName);
               }
               else
               {
                    VSelectedName.removeElementAt(iIndex);
               }
               SelectedList.setListData(VSelectedName);
               return true;
         }
         public void setMiddlePanel()
         {
             MiddlePanel.setRowData(VSelectedName);
             MiddlePanel.createComponents();
         }
         public Vector getPendName()
         {
               VPendName.removeAllElements();
               String QS = "Select RDC.Descript,sum(RDC.Qty-RDC.RecQty) as Pending "+
                           "From RDC "+
                           "Group By RDC.Descript,RDC.Sup_Code "+   
                           "Having RDC.Sup_Code = '"+TSupCode.getText()+"'"+
                           "Order By 1";

               try
               {
                     Class.forName("oracle.jdbc.OracleDriver");
                     Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
                     Statement theStatement   = theConnection.createStatement();
                     ResultSet theResult      = theStatement.executeQuery(QS);
                     while(theResult.next())
                     {
                         double dPending = common.toDouble(theResult.getString(2));
                         if(dPending > 0)
                         {
                            VPendName.addElement(theResult.getString(1));
                         }
                     }
                     theConnection.close();
               }
               catch(Exception ex)
               {
                    System.out.println(QS);
                    System.out.println(ex);
               }
               return VPendName;
         }
}
