package client.stores;

import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

import util.*;

public class GateTableModel extends DefaultTableModel
{
      
        Object    RowData[][],ColumnNames[],ColumnType[];
        Common common = new Common();
        public GateTableModel(Object[][] RowData, Object[] ColumnNames,Object[] ColumnType)
        {
            super(RowData,ColumnNames);
            this.RowData     = RowData;
            this.ColumnNames = ColumnNames;
            this.ColumnType  = ColumnType;
        }
       public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }            
       public boolean isCellEditable(int row,int col)
       {
               if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
               return false;
       }
       public void setValueAt(Object aValue, int row, int column)
       {
            try
            {
                   Vector rowVector = (Vector)super.dataVector.elementAt(row);
                   rowVector.setElementAt(aValue, column);
                   fireTableChanged(new TableModelEvent(this, row, row, column));
            }
            catch(Exception ex)
            {
                System.out.println("4 : "+ex);
            }
      }
    public Object[][] getFromVector()
    {
        Object FinalData[][] = new Object[super.dataVector.size()][ColumnNames.length];
        for(int i=0;i<super.dataVector.size();i++)
        {
             Vector curVector = (Vector)super.dataVector.elementAt(i);
             for(int j=0;j<curVector.size();j++)
                FinalData[i][j] = (String)curVector.elementAt(j);
        }
        return FinalData;
    }
    public Vector getCurVector(int i)
    {
        Vector curVector = (Vector)super.dataVector.elementAt(i);
        return curVector;
    }

}
