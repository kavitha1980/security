package client.stores;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import blf.*;

public class GISFrame extends JInternalFrame implements rndi.CodedNames
{

     JButton        BOk,BCancel,BPending,BSupplier;

     NextField      TGateNo;
     DateField      TGateDate,TInvDate,TDCDate;
     JTextField     TSupCode,TInvNo,TDCNo;
     GIMiddlePanel  MiddlePanel;
     JPanel         TopPanel,BottomPanel;
     Stores    stDomain;
     protected JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel  SPanel;

     Common common   = new Common();
     Control control = new Control();
          
     public GISFrame(JLayeredPane DeskTop,StatusPanel SPanel)
     {
          super("Gate Inward of Stores & Spares Materials");
          this.DeskTop            = DeskTop;
          this.VCode              = VCode;
          this.VName              = VName;
          this.SPanel             = SPanel;
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          show();
     }

     public void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               stDomain            = (Stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void createComponents()
     {
          try
          {
               BOk        = new JButton("Okay");
               BCancel    = new JButton("Abort");
               BPending   = new JButton("Pending Materials");
               BSupplier  = new JButton("Supplier");
     
               TGateDate   = new DateField();
               TInvDate    = new DateField();
               TDCDate     = new DateField();
               TGateNo     = new NextField();
               TInvNo      = new JTextField();
               TDCNo       = new JTextField();
               TSupCode    = new JTextField();
               MiddlePanel = new GIMiddlePanel(DeskTop,SPanel);
               TopPanel    = new JPanel();
               BottomPanel = new JPanel();
     
               TGateDate.setTodayDate();
               TGateNo.setText(String.valueOf(stDomain.getNo("Select GINo From book")+1));
               TGateNo.setEditable(true);

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,520);

          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(5,4,5,5));
          BottomPanel         .setLayout(new FlowLayout());
     }
     public void addComponents()
     {

          TopPanel.add(new JLabel("Supplier"));
          TopPanel.add(BSupplier);

          TopPanel.add(new JLabel("SupplierCode"));
          TopPanel.add(TSupCode);

          TopPanel.add(new JLabel("Gate Inward No"));
          TopPanel.add(TGateNo);

          TopPanel.add(new JLabel("Gate Inward Date"));
          TopPanel.add(TGateDate);

          TopPanel.add(new JLabel("Invoice No"));
          TopPanel.add(TInvNo);

          TopPanel.add(new JLabel("Invoice Date"));
          TopPanel.add(TInvDate);

          TopPanel.add(new JLabel("DC No"));
          TopPanel.add(TDCNo);

          TopPanel.add(new JLabel("DC Date"));
          TopPanel.add(TDCDate);

          TopPanel.add(new JLabel("Fetch Expected Materials"));
          TopPanel.add(BPending);

          TopPanel.add(new JLabel(""));
          TopPanel.add(new JLabel(""));

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          getContentPane().add(TopPanel,BorderLayout.NORTH);
          getContentPane().add(MiddlePanel,BorderLayout.CENTER);
          getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
           BOk.addActionListener(new ActList());
           BCancel.addActionListener(new ActList());
           BPending.addActionListener(new PendingList(DeskTop,MiddlePanel,TSupCode));
           BSupplier.addActionListener(new SupplierSearch(DeskTop,TSupCode));
     }
     public class ActList implements ActionListener
     {
            public void actionPerformed(ActionEvent ae)
            {
                    if(ae.getSource()==BOk)
                    {
                        BOk.setEnabled(false);
                        boolean bSig = false;
                        try
                        {
                              bSig = allOk();
                        }
                        catch(Exception ex)
                        {
                              System.out.println(ex);
                        }
                        if(bSig)
                        {
                             try
                             {
                                  insertGISDetails();
                                  stDomain.setUpdateStoresData((String)TGateNo.getText(),"GINO");
                                  //control.setID(TGateNo.getText(),"GINo");
                                  removeHelpFrame();                                  
                             }
                             catch(Exception e)
                             {
                                  e.printStackTrace();
                             } 
                        }
                        else
                        {
                              BOk.setEnabled(true);
                        }
                    }     
                    if(ae.getSource()==BCancel)
                    {
                        removeHelpFrame();
                    }     

            }
     }
     public void removeHelpFrame()
     {
          try
          {
               DeskTop.remove(this);
               DeskTop.repaint();
               DeskTop.updateUI();
          }
          catch(Exception ex) { }
     }
     public void insertGISDetails()
     {
          
          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          try
          {
               //Class.forName("oracle.jdbc.OracleDriver");
               //Connection theConnection = DriverManager.getConnection("oracle:jdbc:thin:@bulls:1521:amarml","inventory0405","stores0405");
               //Statement theStatement   = theConnection.createStatement();
               for(int i=0;i<FinalData.length;i++)
               {
                    String QS = "Insert Into GateInward (GINo,GIDate,Sup_Code,InvNo,InvDate,DcNo,DcDate,Item_Code,SupQty,GateQty,UnmeasuredQty) Values(";
                    QS = QS+"0"+TGateNo.getText()+",";
                    QS = QS+"'"+TGateDate.toNormal()+"',";
                    QS = QS+"'"+TSupCode.getText()+"',";
                    QS = QS+"'"+TInvNo.getText()+"',";
                    QS = QS+"'"+TInvDate.toNormal()+"',";
                    QS = QS+"'"+TDCNo.getText()+"',";
                    QS = QS+"'"+TDCDate.toNormal()+"',";
                    QS = QS+"'"+(String)FinalData[i][0]+"',";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][2]).trim()))+",";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][3]).trim()))+",";
                    QS = QS+"0"+String.valueOf(common.toDouble(((String)FinalData[i][4]).trim()))+")";
                    //theStatement.execute(QS);
                    stDomain.insertGISDetails(QS);
               }
               //theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }
     public boolean allOk()
     {
          String SDate = TGateDate.toNormal();
          String SSupCode = TSupCode.getText();

          if(SDate.length() < 8)
               return false;
          
          if(SSupCode.length() < 6)
               return false;

          Object FinalData[][] = MiddlePanel.MiddlePanel.getFromVector();
          for(int i=0;i<FinalData.length;i++)
          {
               String S1 = ((String)FinalData[i][2]).trim();
               String S2 = ((String)FinalData[i][3]).trim();
               String S3 = ((String)FinalData[i][4]).trim();

               try
               {
                    double d1=Double.parseDouble(S1);
                    double d2=Double.parseDouble(S2);
                    double d3=Double.parseDouble(S3);                    
               }
               catch(Exception ex)
               {
                    JOptionPane.showMessageDialog(null,"Plz enter Valid QTY ");
                    return false;
               }
          }
          return true;
     }
}
