package client.stores;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.DateField;
import util.Common;
import util.StatusPanel;
import utility.TabReport;

public class StoresInwardList extends JInternalFrame implements rndi.CodedNames
{
     JPanel    TopPanel;
     JPanel    BottomPanel;

     String    SStDate,SEnDate;
     JComboBox JCOrder,JCFilter;
     JButton   BApply;

//     Stores stDomain;

     DateField TStDate;
     DateField TEnDate;
    
     Object RowData[][];
     String ColumnData[] = {"G.I. No","Date","Supplier","Code","Name","Inv Qty","Invoice No","Inv Date","DC No","DC Date"};
     String ColumnType[] = {"N","S","S","S","S","N","S","S","S","S"};
     int iColumnWidth[]    = {100,100,200,150,250,120,120,120,120,120};
     JLayeredPane DeskTop;
     Vector VCode,VName;
     StatusPanel SPanel;

     Common common = new Common();

     Vector VGINo,VGIDate,VSupName,VGICode,VGIName,VInvQty,VInvNo,VInvDate,VDCNo,VDCDate;

     TabReport tabreport;

     public StoresInwardList(JLayeredPane DeskTop,Vector VCode,Vector VName,StatusPanel SPanel)
     {
         super("Stores Materials Received During a Period");
         this.DeskTop = DeskTop;
         this.SPanel  = SPanel;
         this.VCode   = VCode;
         this.VName   = VName;
         setDomain();
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
//               stDomain            = (Stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void createComponents()
     {
         TopPanel    = new JPanel();
         BottomPanel = new JPanel();

         TStDate  = new DateField();
         TEnDate  = new DateField();
         BApply   = new JButton("Apply");
         JCOrder  = new JComboBox();
         JCFilter = new JComboBox();

         TStDate.setTodayDate();
         TEnDate.setTodayDate();
     }

     public void setLayouts()
     {
         TopPanel.setLayout(new GridLayout(1,8));

         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,798,520);
     }

     public void addComponents()
     {
         JCOrder.addItem("G.I. No");
         JCOrder.addItem("Materialwise");
         JCOrder.addItem("Supplierwise");

         JCFilter.addItem("All");
         JCFilter.addItem("With Invoice/DC");
         JCFilter.addItem("Without Invoice/DC");

         TopPanel.add(new JLabel("Sorted On"));
         TopPanel.add(JCOrder);

         TopPanel.add(new JLabel("List Only"));
         TopPanel.add(JCFilter);

         TopPanel.add(new JLabel("Period"));
         TopPanel.add(TStDate);
         TopPanel.add(TEnDate);
         TopPanel.add(BApply);

         getContentPane().add(TopPanel,BorderLayout.NORTH);
         getContentPane().add(BottomPanel,BorderLayout.SOUTH);
     }
     public void addListeners()
     {
         BApply.addActionListener(new ApplyList());
     }
     public class ApplyList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               setDataIntoVector();
               setRowData();
               try
               {
                  getContentPane().remove(tabreport);
               }
               catch(Exception ex){}

               try
               {
                  tabreport = new TabReport(RowData,ColumnData,ColumnType);
                  tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                  for(int i=0;i<ColumnData.length;i++)
                  {
                    tabreport.ReportTable.getColumn(ColumnData[i]).setPreferredWidth(iColumnWidth[i]);
                  }  
                  getContentPane().add(tabreport,BorderLayout.CENTER);
                  setSelected(true);
                  DeskTop.repaint();
                  DeskTop.updateUI();
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
          }
     }
     public void setDataIntoVector()
     {

           VGINo     = new Vector();
           VGIDate   = new Vector();
           VSupName  = new Vector();
           VGICode   = new Vector();
           VGIName   = new Vector();
           VInvQty   = new Vector();
           VInvNo    = new Vector();
           VInvDate  = new Vector();
           VDCNo     = new Vector();
           VDCDate   = new Vector();

           SStDate = TStDate.toString();
           SEnDate = TEnDate.toString();
           String StDate  = TStDate.toNormal();
           String EnDate  = TEnDate.toNormal();
           try
           {
               //Class.forName("oracle.jdbc.OracleDriver");
               //Connection con = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               //Statement stat = con.createStatement();
               String QString = getQString(StDate,EnDate);
               //ResultSet res  = stat.executeQuery(QString);
               Vector InVector  = new Vector();
               Vector VRows     = new Vector();

//               VRows            = stDomain.getListVector(QString);

               int m=0;

               for(int j=0;j<(VRows.size())/10;j++)
               {
                    String str1 = (String)VRows.elementAt(6+m);
                    String str2 = (String)VRows.elementAt(8+m);


                    if(JCFilter.getSelectedIndex()==1)
                          if((((str1.trim()).length())==0) || (((str2.trim()).length())==0))
                           continue;

                    if(JCFilter.getSelectedIndex()==2)
                          if((((str1.trim()).length())>0) || (((str2.trim()).length())>0))
                              continue;

                     VGINo.addElement(VRows.elementAt(0+m));
                     VGIDate.addElement(common.parseDate((String)VRows.elementAt(1+m)));
                     VSupName.addElement(VRows.elementAt(2+m));
                     VGICode.addElement(VRows.elementAt(3+m));
                     VGIName.addElement(VRows.elementAt(4+m));
                     VInvQty.addElement(VRows.elementAt(5+m));
                     VInvNo.addElement(common.parseNull(str1));
                     VInvDate.addElement(common.parseDate((String)VRows.elementAt(7+m)));
                     VDCNo.addElement(common.parseNull(str2));
                     VDCDate.addElement(common.parseDate((String)VRows.elementAt(9+m)));
                     m=m+10;
               }
           }
           catch(Exception ex){System.out.println(ex);}
     }
     public void setRowData()
     {
         RowData     = new Object[VGIDate.size()][ColumnData.length];
         for(int i=0;i<VGIDate.size();i++)
         {
               RowData[i][0] = (String)VGINo.elementAt(i);
               RowData[i][1] = (String)VGIDate.elementAt(i);
               RowData[i][2] = (String)VSupName.elementAt(i);
               RowData[i][3] = (String)VGICode.elementAt(i);
               RowData[i][4] = (String)VGIName.elementAt(i);
               RowData[i][5] = (String)VInvQty.elementAt(i);
               RowData[i][6] = (String)VInvNo.elementAt(i);
               RowData[i][7] = (String)VInvDate.elementAt(i);
               RowData[i][8] = (String)VDCNo.elementAt(i);
               RowData[i][9] = (String)VDCDate.elementAt(i);
        }  
     }
     public String getQString(String StDate,String EnDate)
     {
          String QString = "";
          QString = "SELECT GateInward.GINo, GateInward.GIDate, Supplier.Name, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate "+
                    " FROM (GateInward INNER JOIN Supplier ON GateInward.Sup_Code = Supplier.Ac_Code) INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                     "Where GateInward.GIDate >= '"+StDate+"' and GateInward.GIDate <='"+EnDate+"'";

          if(JCOrder.getSelectedIndex() == 0)      
               QString = QString+" Order By GateInward.GINo,GateInward.GIDate";
          if(JCOrder.getSelectedIndex() == 1)      
               QString = QString+" Order By InvItems.Item_Name,GateInward.GIDate";
          if(JCOrder.getSelectedIndex() == 2)      
               QString = QString+" Order By Supplier.Name,GateInward.GIDate";

          return QString;
     }
}
