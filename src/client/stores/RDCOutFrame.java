package client.stores;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.Common;
import utility.TabReport;

public class RDCOutFrame extends JInternalFrame implements rndi.CodedNames
{

     Object RowData[][];
     String ColumnData[]={"RDC No","Work Shop Or Supplier","Sent Thro","Status"};
     String ColumnType[]={"N","S","S","S"};
 //    stores stDomain;
     TabReport tabreport;
     Vector VRdc,VSup,VSent;

     JLayeredPane Layer;
     Common common = new Common();

     public RDCOutFrame(JLayeredPane Layer)
     {
          this.Layer = Layer;
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
     }
     private void setDomain()
     {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
//               stDomain            = (stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void createComponents()
     {
          VRdc  = new Vector();
          VSup  = new Vector();
          VSent = new Vector();
     }
     public void setLayouts()
     {
         setClosable(true);
         setMaximizable(true);
         setIconifiable(true);
         setResizable(true);
         setBounds(0,0,798,520);

         setTitle("RDC for Machinery Spare Parts Pending @ Gate");

     }
     public void addComponents()
     {
             getData();
             setData();
             getContentPane().add("Center",tabreport=new TabReport(RowData,ColumnData,ColumnType));
             tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             tabreport.ReportTable.addMouseListener(new MouseList());
     }
     public class MouseList extends MouseAdapter
     {
            public void mouseClicked(MouseEvent me)
            {
                  if(me.getClickCount()==2)
                  {
                        int i = tabreport.ReportTable.getSelectedRow();
                        String SStatus = ((String)RowData[i][3]).trim();
                        if(SStatus.equals("Passed"))
                        {
                              JOptionPane.showMessageDialog(null,"Passed Already","Information",JOptionPane.INFORMATION_MESSAGE);                              
                              return;
                        }
                        String SRDCNo = (String)RowData[i][0];
                        RDCOutDetFrame rdcdf=new RDCOutDetFrame(Layer,SRDCNo,RowData,String.valueOf(i));

                        try
                        {
                              Layer.add(rdcdf);
                              rdcdf.show();
                              rdcdf.moveToFront();
                              Layer.repaint();
                              Layer.updateUI();
                        }
                        catch(Exception x)
                        {
                             try
                             {
                                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Nat.Prn")));
                                x.printStackTrace(out);
                                out.close();
                             }
                             catch(Exception e)
                             {
                             }
                        }


                        /*catch(Exception ex)
                        {
                              System.out.println(ex);
                        }*/

                  }
            }
     }
     public void getData()
     {

         /*String QS = "SELECT [RDC].[RDCNo], [Supplier].[Name], [RDC].[Thro] "+
                     " FROM RDC INNER JOIN Supplier ON [RDC].[Sup_Code]=[Supplier].[Ac_Code] "+
                     " WHERE [RDC].[Valid]=0";*/
         String QS = "SELECT RDC.RDCNo, Supplier.Name, RDC.Thro "+
                     " FROM RDC INNER JOIN Supplier ON RDC.Sup_Code = Supplier.Ac_Code "+
                     " WHERE RDC.Valid=0";

         System.out.println(QS);
         try
         {
                Vector VRows       = new Vector();
//                VRows              = stDomain.getRDCOutData(QS);
                int m=0;
                for(int i=0;i<(VRows.size())/3;i++)
                {
                        {
                             VRdc.addElement(VRows.elementAt(0+m));
                             VSup.addElement(VRows.elementAt(1+m));
                             VSent.addElement(VRows.elementAt(2+m));
                             m=m+3;
                        }
                }
         }
         catch(Exception ex)
         {
               ex.printStackTrace();
         }


     }
     public void setData()
     {
            RowData     = new Object[VRdc.size()][ColumnData.length];
            for(int i=0;i<VRdc.size();i++)
            {
                RowData[i][0]  = common.parseNull((String)VRdc.elementAt(i));
                RowData[i][1]  = common.parseNull((String)VSup.elementAt(i));
                RowData[i][2]  = common.parseNull((String)VSent.elementAt(i));
                RowData[i][3]  = "Pending";
            }
     }
}
