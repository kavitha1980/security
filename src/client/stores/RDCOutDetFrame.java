package client.stores;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.sql.*;

import util.Common;
import utility.TabReport;

public class RDCOutDetFrame extends JInternalFrame implements rndi.CodedNames
{
     JPanel TopPanel,BottomPanel,MiddlePanel;

//     stores stDomain;
     Object RowData[][];
     String SRDCNo,SId;

     Vector VId,VDesc,VDept,VUnit,VQty,VRemarks;
     JButton BOk;
     JButton BCancel;

     TabReport tabreport;

     JLayeredPane Layer; 
     Object RD[][];
     String ColumnData[] = {"Description","Department","Unit","Quantity","Remarks"};
     String ColumnType[] = {"S","S","S","N","S"};

     Common common = new Common();
     RDCOutDetFrame(JLayeredPane Layer,String SRDCNo,Object RowData[][],String SId)
     {
         this.Layer   = Layer;
         this.SRDCNo  = SRDCNo;
         this.RowData = RowData;
         this.SId     = SId;

         setDomain();
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         show();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
//               stDomain            = (stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void createComponents()
     {
         TopPanel    = new JPanel(true);
         MiddlePanel = new JPanel(true);
         BottomPanel = new JPanel(true);

         BOk     = new JButton("Pass");
         BCancel = new JButton("Don't Pass");

         getData();
         setData();
     }
     public void setLayouts()
     {
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,700,700);
         
          getContentPane()    .setLayout(new BorderLayout());
          TopPanel            .setLayout(new GridLayout(3,2));
          MiddlePanel         .setLayout(new BorderLayout());

     }
     public void addComponents()
     {
          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);

          BottomPanel.add(BOk);
          BottomPanel.add(BCancel);

          MiddlePanel.add("Center",tabreport=new TabReport(RD,ColumnData,ColumnType));
          tabreport.ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

          int iid = common.toInt(SId);

          TopPanel.add(new JLabel("RDC No"));
          TopPanel.add(new JLabel((String)RowData[iid][0]));
          TopPanel.add(new JLabel("Supplier/Work Shop"));
          TopPanel.add(new JLabel((String)RowData[iid][1]));
          TopPanel.add(new JLabel("Sent Through"));
          TopPanel.add(new JLabel((String)RowData[iid][2]));
     }
     public void addListeners()
     {
          BOk.addActionListener(new ActList());
          BCancel.addActionListener(new ActList());
     }
     public class ActList implements ActionListener
     {
         public void actionPerformed(ActionEvent ae)
         {
               if(ae.getSource()==BOk)
               {
                  BOk.setEnabled(false);
                  UpdateRDC();
               }
               removeHelpFrame();
         }
     }
     public void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     public void UpdateRDC()
     {
            try
            {
                //Class.forName("oracle.jdbc.OracleDriver");
                //Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
                //Statement theStatement   = theConnection.createStatement();
                for(int i=0;i<VId.size();i++)
                {
                     String QS = "Update RDC Set Valid=1 Where Id = "+(String)VId.elementAt(i);
                     //theStatement.execute(QS);
                   //  stDomain.updateRDC(QS);
                }
                //theConnection.close();
                RowData[common.toInt(SId)][3] = "Passed";
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
     }
     public void getData()
     {
            VId      = new Vector();
            VDesc    = new Vector();
            VDept    = new Vector();
            VUnit    = new Vector();
            VQty     = new Vector();
            VRemarks = new Vector();
            String QS = " SELECT RDC.Id,RDC.Descript,Dept.Dept_Name,Unit.Unit_Name,RDC.Qty,RDC.Remarks "+
                        " FROM (RDC INNER JOIN Dept ON RDC.Dept_Code=Dept.Dept_Code) INNER JOIN Unit ON RDC.Unit_Code=Unit.Unit_Code "+
                        " WHERE RDC.Valid=0 and RDC.RDCNo = "+SRDCNo ;
          try
          {
               Vector InVector    = new Vector();
               Vector VRows       = new Vector();
//               VRows              = stDomain.getRDCData(QS);
               int m=0;
               for(int j=0;j<(VRows.size())/6;j++)
               {
                    VId     .addElement(VRows.elementAt(0+m));
                    VDesc   .addElement(VRows.elementAt(2+m));
                    VDept   .addElement(VRows.elementAt(3+m));
                    VUnit   .addElement(VRows.elementAt(4+m));
                    VQty    .addElement(VRows.elementAt(5+m));
                    VRemarks.addElement(common.parseNull((String)VRows.elementAt(6+m)));
                    m=m+6;
               }

          }
          catch(Exception ex)
          {
             System.out.println(ex);           
          }
     }
     public void setData()
     {
               RD = new Object[VId.size()][ColumnData.length];

               for(int i=0;i<VId.size();i++)
               {
                     RD[i][0]=(String)VDesc.elementAt(i);
                     RD[i][1]=(String)VDept.elementAt(i);
                     RD[i][2]=(String)VUnit.elementAt(i);
                     RD[i][3]=(String)VQty.elementAt(i);
                     RD[i][4]=(String)VRemarks.elementAt(i);
               }
     }
}
