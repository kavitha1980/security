package client.stores;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.*;

public class GIMiddlePanel extends JPanel 
{
         JScrollPane TabScroll;
         InvMiddlePanel MiddlePanel;

         Object RowData[][];
         String ColumnData[] = {"Code","Name","DC/Inv Qty Mentioned By Supplier","Qty Measured @ Gate","Unmeasureable N� of packs"};
         String ColumnType[] = {"S","S","E","E","E"};

         JLayeredPane Layer;
         Vector VCode,VName;
         StatusPanel SPanel;

         Common common = new Common();
         public GIMiddlePanel(JLayeredPane Layer,StatusPanel SPanel)
         {
              this.Layer  = Layer;
              this.SPanel = SPanel;

              setLayout(new BorderLayout());
         }
         public void createComponents()
         {
               try
               {
                  MiddlePanel           = new InvMiddlePanel(Layer,RowData,ColumnData,ColumnType);
                  JScrollPane TabScroll = new JScrollPane(MiddlePanel);
                  add(TabScroll,BorderLayout.CENTER);
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
         }
         public void setRowData(Vector VSelectedCode,Vector VSelectedName)
         {
            RowData     = new Object[VSelectedCode.size()][ColumnData.length];
            for(int i=0;i<VSelectedCode.size();i++)
            {
                  RowData[i][0] = (String)VSelectedCode.elementAt(i);
                  RowData[i][1] = (String)VSelectedName.elementAt(i);
            }
         }
}         
