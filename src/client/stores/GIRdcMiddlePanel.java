package client.stores;


import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import util.Common;
import util.StatusPanel;

public class GIRdcMiddlePanel extends JPanel 
{
         JScrollPane TabScroll;
         InvMiddlePanel MiddlePanel;

         Object RowData[][];
         String ColumnData[] = {"Name","DC/Inv Qty Mentioned By Supplier","Qty Measured @ Gate","Unmeasureable N� of packs"};
         String ColumnType[] = {"S","E","E","E"};

         JLayeredPane Layer;
         Vector VCode,VName;
         StatusPanel SPanel;

         Common common = new Common();
         public GIRdcMiddlePanel(JLayeredPane Layer,StatusPanel SPanel)
         {
              this.Layer  = Layer;
              this.VCode  = VCode;
              this.VName  = VName;
              this.SPanel = SPanel;

              setLayout(new BorderLayout());
         }
         public void createComponents()
         {
               try
               {
                  MiddlePanel           = new InvMiddlePanel(Layer,RowData,ColumnData,ColumnType);
                  JScrollPane TabScroll = new JScrollPane(MiddlePanel);
                  add(TabScroll,BorderLayout.CENTER);
               }
               catch(Exception ex)
               {
                  System.out.println(ex);
               }
         }
         public void setRowData(Vector VSelectedName)
         {
            RowData     = new Object[VSelectedName.size()][ColumnData.length];
            for(int i=0;i<VSelectedName.size();i++)
            {
                  RowData[i][0] = (String)VSelectedName.elementAt(i);
            }
         }
}         
