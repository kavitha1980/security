package client.stores;

import java.rmi.*;
import java.rmi.registry.*;

import blf.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.sql.*;

import util.*;
import utility.*;

public class SupplierSearch implements ActionListener,rndi.CodedNames
{
      JLayeredPane   Layer;
      JTextField     TSupCode;
      JButton        BName;
      JButton        BOk;
      Vector         VSupName,VSupCode;
      JTextField     TIndicator;          
      JList          BrowList;
      JScrollPane    BrowScroll;
      JPanel         BottomPanel;
      JInternalFrame MediFrame;
      String str="";
      ActionEvent ae;
//      Stores stDomain;
      SupplierSearch(JLayeredPane Layer,JTextField TSupCode)
      {
          this.Layer       = Layer;
          this.TSupCode    = TSupCode;

          VSupName         = new Vector();
          VSupCode         = new Vector();

          setDomain();
          setDataIntoVector();

          TIndicator    = new JTextField();
          BOk           = new JButton("Selection Over");
          TIndicator.setEditable(false);

          BrowList         = new JList(VSupName);

          BrowScroll    = new JScrollPane(BrowList);

          BottomPanel   = new JPanel(true);
          BottomPanel.setLayout(new GridLayout(1,2));
          MediFrame     = new JInternalFrame("Supplier Selector");
          MediFrame.show();
          MediFrame.setBounds(80,100,550,350);
          MediFrame.setClosable(true);
          MediFrame.setResizable(true);
          BrowList.addKeyListener(new KeyList());
          MediFrame.getContentPane().setLayout(new BorderLayout());
          MediFrame.getContentPane().add("South",BottomPanel);
          MediFrame.getContentPane().add("Center",BrowScroll);
          BottomPanel.add(TIndicator);
          BottomPanel.add(BOk);
          BOk.addActionListener(new ActList());

      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
//               stDomain            = (Stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
      public void setDataIntoVector()
      {
               String QS = "Select Name,Ac_Code From Supplier Order By Name";
               try
               {
                    //Class.forName("oracle.jdbc.OracleDriver");
                    //Connection conn   = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
                    //Statement stat    = conn.createStatement();
                    //ResultSet res     = stat.executeQuery(QString);
                    Vector InVector     = new Vector();
                    Vector VRows        = new Vector(); 
//                    VRows               = stDomain.getSupplierData(QS);

                    int m=0;
                    for(int j=0;j<(VRows.size())/2;j++)
                    {
                         VSupName.addElement(VRows.elementAt(0+m));
                         VSupCode.addElement(VRows.elementAt(1+m));
                         m=m+2;
                    }
                    
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
      }
      public class ActList implements ActionListener
      {
          public void actionPerformed(ActionEvent e)
          {
               int index = BrowList.getSelectedIndex();
               setGrnDet(index);
               str="";
               removeHelpFrame();
          }
      }
      public void actionPerformed(ActionEvent ae)
      {
          this.ae = ae;  
          BName = (JButton)ae.getSource();
          removeHelpFrame();
          try
          {
               Layer.add(MediFrame);
               MediFrame.moveToFront();
               MediFrame.setSelected(true);
               MediFrame.show();
               BrowList.requestFocus();
               Layer.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
      }  
      public class KeyList extends KeyAdapter
      {
             public void keyReleased(KeyEvent ke)
             {
                  char lastchar=ke.getKeyChar();
                  lastchar=Character.toUpperCase(lastchar);
                  try
                  {
                     if(ke.getKeyCode()==8)
                     {
                        str=str.substring(0,(str.length()-1));
                        setCursor();
                     }
                     else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar>='0' && lastchar <= '9'))
                     {
                        str=str+lastchar;
                        setCursor();
                     }
                  }
                  catch(Exception ex){}
             }
             public void keyPressed(KeyEvent ke)
             {
                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                     int index = BrowList.getSelectedIndex();
                     setGrnDet(index);
                     str="";
                     removeHelpFrame();
                  }
             }
         }
         public void setCursor()
         {
            TIndicator.setText(str);            
            int index=0;
            for(index=0;index<VSupName.size();index++)
            {
                 String str1 = ((String)VSupName.elementAt(index)).toUpperCase();
                 if(str1.startsWith(str))
                 {
                      BrowList.setSelectedValue(str1,true);   
                      break;
                 }
            }
         }
         public void removeHelpFrame()
         {
            try
            {
               Layer.remove(MediFrame);
               Layer.repaint();
               Layer.updateUI();
               ((JButton)ae.getSource()).requestFocus();
            }
            catch(Exception ex) { }
         }
         public boolean setGrnDet(int index)
         {
               BName.setText((String)VSupName.elementAt(index));
               TSupCode.setText((String)VSupCode.elementAt(index));
               return true;
         } 
}
