package client.stores;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.sql.*;
public class InvMiddlePanel extends JPanel
{
   JTable         ReportTable;
   InvTableModel  dataModel;
     
   Object         RowData[][];
   String         ColumnData[],ColumnType[];

   JPanel         BottomPanel;

   JLayeredPane DeskTop;
   Vector VCode,VName;

   InvMiddlePanel(JLayeredPane DeskTop,Object RowData[][],String ColumnData[],String ColumnType[])
   {
         this.DeskTop     = DeskTop;
         //this.VCode       = VCode;
         //this.VName       = VName;
         this.RowData     = RowData;
         this.ColumnData  = ColumnData;
         this.ColumnType  = ColumnType;

         createComponents();
         setLayouts();
         setReportTable();
    }
    public void createComponents()
    {
         BottomPanel      = new JPanel();
     }
     public void setLayouts()
     {
         BottomPanel.setLayout(new BorderLayout());
     }
     public void setReportTable()
     {
         dataModel        = new InvTableModel(RowData,ColumnData,ColumnType);       
         ReportTable      = new JTable(dataModel);
         ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

         DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
         cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

         for (int col=0;col<ReportTable.getColumnCount();col++)
         {
               if(ColumnType[col]=="N" || ColumnType[col]=="B")
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);
         }
         ReportTable.setShowGrid(false);

         setLayout(new BorderLayout());
         add(ReportTable.getTableHeader(),BorderLayout.NORTH);
         add(ReportTable,BorderLayout.CENTER);
         add(BottomPanel,BorderLayout.SOUTH);
     }
     public Object[][] getFromVector()
     {
          return dataModel.getFromVector();     
     }
}

