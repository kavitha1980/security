/*

     The Owner Object is meant for
     allowing the User to select an owner of the rental vehicle
     belongs to.

     If the owner in ? is not available a provision
     to insert On line the new Owner.

*/
package client.master;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;

import util.Common;
import java.rmi.*;
import java.rmi.registry.*;

import blf.*;

public class OwnerModel extends JButton implements ActionListener,rndi.CodedNames
{

      JTextField     TCode;
      Vector         VCode,VName;
     
      Common common = new Common();
      RentVehicleMasterInfo ownerDomain;
      RentVehicleMasterFrame rentvehiclemasterframe;
      public OwnerModel(RentVehicleMasterFrame rentvehiclemasterframe)
      {
            this.rentvehiclemasterframe = rentvehiclemasterframe;
            setDomain();
            TCode = new JTextField();
            setText("Owner");
            setBorder(new javax.swing.border.BevelBorder(0));
            setBackground(new Color(128,128,255));
            setForeground(Color.white);

            setDataIntoVector();
           
            addActionListener(this);
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               ownerDomain         = (RentVehicleMasterInfo) registry.lookup(SECURITYDOMAIN);

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      public void actionPerformed(ActionEvent ae)
      {

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Owner Selector",true); 
            SearchPanel SP     = new SearchPanel(this,TCode,VCode,VName,theDialog);

            theDialog.getContentPane().add(SP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {
            String SName = getText();
            String SCode = TCode.getText();

            int index = VCode.indexOf(SCode);

            if(index == -1)
                  setData();
            else
            {
                  TCode.setText(SCode);
            }   
            setEnabled(false);
            rentvehiclemasterframe.JCPurpose.requestFocus();

      }

      private void setData()
      {
            String QS1 = "Insert Into RentVehicleOwner (Name) Values ("+
                  "'"+getText()+"')";


            try
            {
                  String SCode       = ownerDomain.getMaxOwnerCode();
                  int iCode          = common.toInt(SCode);
                  iCode  = iCode+1;
                  
                  SCode              = Integer.toString(iCode);
                  ownerDomain.insertOwnerName(getText(),SCode);
                  TCode.setText(SCode);
                  setDataIntoVector();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setDataIntoVector()
      {
            VCode      = new Vector();
            VName      = new Vector();

            try
            {
                  Vector VTemp     = new Vector();
                  VTemp            = ownerDomain.getOwnerInfo();
                  int m=0;
                  int size=(VTemp.size())/2;
                  for(int i=0;i<size;i++)
                  {
                        VCode.addElement(VTemp.elementAt(0+m));
                        VName.addElement(VTemp.elementAt(1+m));
                        m=m+2; 
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public String getCode()
      {
            return TCode.getText();
      }
     
}
