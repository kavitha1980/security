package client.vital;

import javax.swing.*;
import javax.swing.event.*;
import awt.*;
import awt.event.*;

import java.rmi.*;
import java.rmi.registry.*;

import blf.*;
import domain.jdbc.*;
public class SecurityManagementFrame extends JFrame implements rndi.CodedNames
{
     protected JLayeredPane Layer;

     JPanel TopPanel,CentrePanel,BottomPanel;

     DocNoField     TId;
     NoNumberField  TName;

     MyButton       BOk;
     JButton        BExit;

     SecurityManagementFrame(JLayeredPane Layer)
     {
          this.Layer     =    Layer;
          setVital();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void setVital()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               theData             = (Vital)registry.lookup(SECURITYDOMAIN);   
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"UNABLE TO LOAD VITALDATA.","Error",JOptionPane.ERROR_MESSAGE);
               System.out.println(ex);
          }
     }
     public void createComponents()
     {
          TopPanel    =  new JPanel();
          CentrePanel =  new JPanel();
          BottomPanel =  new JPanel();

          TId         =  new DocNoField();
          TName       =  new NoNumberField();

          BOk         =  new MyButton("Save");
          BExit       =  new MyButton("Exit");
     }
     public void setLayouts()
     {
          setTitle("Security Management Frame");
          setClosable(true);
          setMinimizable(true);
          setIconifiable(true);
          setBounds(0,0,400,500);

          BOk.setMnemonic('S');
          BExit.setMnemonic('E');

          TopPanel.setLayout(new GridLayout(2,2));
          CentrePanel.setLayout(new BorderLayout());
     }
     public void addComponents()
     {
          TopPanel.add(new JLabel("Id"));
          TopPanel.add(TId);
          TopPanel.add(new JLabel("Name"));
          TopPanel.add(TName);

          BottomPanel.add(BOk);
          BottomPanel.add(BExit);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);
     }
     public void addListeners()
     {
          BOk.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOk)
               {
                    JOptionPane.showMessageDialog(null,"Message");
               }
               if(ae.getSource()==BExit)
               {
                    removeHelpFrame();
               }
          }
     }
/*     private void getMessage()
     {
          String str =   "<html><body>";
          str = str+"A New SecurityName<br>";
          str = str+"Successfully Registerd<br>";
          str = str+"<\body></html>";
     }*/
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          { }
     }
}

