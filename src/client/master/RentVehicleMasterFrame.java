package client.master;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;
import domain.jdbc.*;

import util.*;
import util.MyComboBox;

import blf.*;

public class RentVehicleMasterFrame extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane Layer;

     JPanel  MasterPanel,InfoPanel,InfoLeftPanel,InfoRightPanel,ButtonPanel;

     MyLabel LCardNo,LVehicleRegNo,LVehicleName;

     MyComboBox JCPurpose;

     MyTextField MCardNo,MVehicleRegNo,MVehicleName;

     JButton BUpdate,BClose;

     OwnerModel   theOwner;

     RentVehicleMasterInfo InfoDomain;

     RentVehicleInfo       VDomain;

     Common common   = new Common();

     public RentVehicleMasterFrame(JLayeredPane Layer)
     {
          this.Layer = Layer;
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void setDomain()
     {
          try
          {
               Registry registry1  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               InfoDomain          = (RentVehicleMasterInfo)registry1.lookup(SECURITYDOMAIN);

               Registry registry2  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VDomain             = (RentVehicleInfo)registry2.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
	
     }
     

     public void createComponents()
     {
          try
          {
              MasterPanel         =    new JPanel();
              InfoPanel           =    new JPanel();
              InfoLeftPanel       =    new JPanel();
              InfoRightPanel      =    new JPanel();
              ButtonPanel         =    new JPanel();
    
              LCardNo             =    new MyLabel("CardNo");
              LVehicleRegNo       =    new MyLabel("RegistrationNumber");
              LVehicleName        =    new MyLabel("VehicleName");
              JCPurpose           =    new MyComboBox(InfoDomain.getPurpose());
    
              MCardNo             =    new MyTextField(7);
              MVehicleRegNo       =    new MyTextField(15);
              MVehicleName        =    new MyTextField(40);

              MCardNo.addFocusListener(new focusEvents());

    
              BUpdate             =    new JButton("Update");
              BClose              =    new JButton("Close");

              theOwner            =    new OwnerModel(this);
              theOwner.setMnemonic('O');    

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }


     }

     public void setLayouts()
     {
          setVisible(true);
          setSize(800,600);
          setTitle("Rental Vehicle Master Frame");
          setIconifiable(true);
          setMaximizable(true);
          setClosable(true);
          setResizable(true);

          BUpdate.setMnemonic('u');
          BClose.setMnemonic('c');

          MasterPanel.setLayout(new GridLayout(3,1));
          InfoPanel.setLayout(new GridLayout(2,3));
          InfoLeftPanel.setLayout(new GridLayout(3,2));
          InfoRightPanel.setLayout(new GridLayout(3,2));
          InfoPanel.setBorder(new TitledBorder("RentVehicleInfo"));
          ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
          ButtonPanel.setBorder(new TitledBorder("ControlPanel"));
     }

     public void addComponents()
     {
          InfoLeftPanel.add(LCardNo);
          InfoLeftPanel.add(MCardNo);
          InfoLeftPanel.add(LVehicleRegNo);
          InfoLeftPanel.add(MVehicleRegNo);
          InfoLeftPanel.add(LVehicleName);
          InfoLeftPanel.add(MVehicleName);
          InfoRightPanel.add(new MyLabel("Owner of the Vehicle"));
          InfoRightPanel.add(theOwner);
          InfoRightPanel.add(new JLabel(""));
          InfoRightPanel.add(new JLabel(""));
          InfoRightPanel.add(new MyLabel("Purpose"));
          InfoRightPanel.add(JCPurpose);

          InfoPanel.add(InfoLeftPanel);
          InfoPanel.add(new JLabel(""));
          InfoPanel.add(InfoRightPanel);
          InfoPanel.add(new JLabel(""));
          InfoPanel.add(new JLabel(""));
          InfoPanel.add(new JLabel(""));

          
          ButtonPanel.add(BUpdate);
          ButtonPanel.add(BClose);

          MasterPanel.add(InfoPanel);
          MasterPanel.add(ButtonPanel);
          MasterPanel.add(new JPanel());

          getContentPane().add(MasterPanel);
     }

     public void addListeners()
     {
          BUpdate.addActionListener(new action());
          BClose .addActionListener(new action());
     }

     private class focusEvents extends FocusAdapter
     {
          public void focusLost(FocusEvent fe)
          {

               try
               {

                    String SCardNo   =    common.parseNull(MCardNo.getText().trim());

                    if(checkCardAvailable())
                    {
                            JOptionPane.showMessageDialog(null,"The Card No already exists");
                            clearFields();
                    }
                    else if(SCardNo.equals("0"))
                    {
                            JOptionPane.showMessageDialog(null,"Not a Valid Card Number");
                            clearFields();
                    }
               }
               catch(Exception e)
               {
                    //e.printStackTrace();
               }

          }
     }
     private boolean checkCardAvailable()
     {
          boolean flag=true;
          try
          {
               String Scardno    = (String)MCardNo.getText();

               int iCount = VDomain.getRVCheckCardAvailable(Scardno);
               if(iCount==0)
               {                                             
                    flag=false;
               }
          }
          catch(Exception e)
          {
                  flag=false;
          }
          return flag;
     }



     public class action implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BUpdate)
               {
                    boolean ok = checkFields();

                    if(ok)
                    {
                         if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
                         {
                              insertValues();
                              clearFields();
                         }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null,"All fields must be filled");
                    }
               }
               if(ae.getSource()==BClose)
               {
                    setVisible(false);
               }
          }
     }

     private boolean checkFields()
     {

        boolean flag = true;

        try
        {
               String SCardNo   =    common.parseNull(MCardNo.getText().trim());

               if(SCardNo.equals("") || SCardNo.equals("0"))
               {
                    flag=false;
                    MCardNo.requestFocus();
                    return flag;
               }

               String SVehicleRegNo = common.parseNull(MVehicleRegNo.getText().trim());

               if(SVehicleRegNo.indexOf(" ") != -1)
               {
                    JOptionPane.showMessageDialog(null, "Spaces are not allowed in Truck No.", "Error", JOptionPane.ERROR_MESSAGE,null); 
                    return false;
               }

//                           SVehicleRegNo = SVehicleRegNo.replaceAll(" ",""); 

               if(SVehicleRegNo.equals("") || SVehicleRegNo.equals("0"))
               {
                    flag=false;
                    MVehicleRegNo.requestFocus();
                    return flag;
               }

               String SVehicleName = common.parseNull(MVehicleName.getText().trim());

               if(SVehicleName.equals("") || SVehicleName.equals("0"))
               {
                    flag=false;
                    MVehicleName.requestFocus();
                    return flag;
               }
               String SCode = (String)theOwner.TCode.getText();
               if(SCode.equals(""))
               {
                    flag=false;
                    theOwner.requestFocus();
                    return flag;
               }
        }
        catch(Exception e)
        {
               JOptionPane.showMessageDialog(null,"Fields cannot be empty");
        }

        return flag;
     }


     private void clearFields()
     {

            MCardNo.setText("");
            MVehicleRegNo.setText("");
            MVehicleName.setText("");
            JCPurpose.setSelectedIndex(0);
            theOwner.setEnabled(true);
            theOwner.setText("Owner");
            theOwner.TCode.setText("");
            BUpdate.setEnabled(true);
            MCardNo.requestFocus();
     }
     


     public void insertValues()
     {
          Vector infoVector = new Vector();

          try
          {
               String SCardNo           =    MCardNo.getText();
               String SVehicleRegNo     =    MVehicleRegNo.getText();
               String SVehicleName      =    MVehicleName.getText();
               String SPurpose          =    (String)JCPurpose.getSelectedItem();
               int    iCode             =    Integer.parseInt((String)theOwner.TCode.getText());
               String SStatus           =    "1";
               String SRentalStatus     =    "0";


               infoVector.addElement(String.valueOf(SCardNo));
               infoVector.addElement(String.valueOf(SVehicleRegNo));
               infoVector.addElement(String.valueOf(SVehicleName));
               infoVector.addElement(String.valueOf(SPurpose));
               infoVector.addElement(String.valueOf(iCode));
               infoVector.addElement(SStatus);
               infoVector.addElement(SRentalStatus);

               try
               {
                    String SMaxId = InfoDomain.getMaxRVId();
                    int iId       = (common.toInt(SMaxId))+1;
                    infoVector.addElement(String.valueOf(iId));
                    InfoDomain.insertRentVehicle(infoVector);
                    JOptionPane.showMessageDialog(null,"Record saved","Message",JOptionPane.NO_OPTION,null);
               }

               catch(Exception ex1)
               {
                    ex1.printStackTrace();
               }

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }



}
                     
