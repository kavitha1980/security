package client.WaterLorryIncentive;
/*

          A helper class to transport data
          to and from client and server.

          A combination of RowSet and HashMap pattern

*/

import util.*;
import java.util.Vector;

public class RowSet implements java.io.Serializable
{

     Vector VColumnName;
     Vector VColumnType;
     Vector VColumnSize;
     Vector VColumnLabel;
     Vector VRows;

     public RowSet()
     {
          VColumnName  = new Vector();
          VColumnType  = new Vector();
          VColumnSize  = new Vector();
          VColumnLabel = new Vector();
          VRows        = new Vector();
     }


     public void setColumnName(Vector vect)
     {

          for(int i=0;i<vect.size();i++)
          {
               VColumnName.addElement((String)vect.elementAt(i));
          }

     }

     public void setColumnName(String SArr[])
     {
          for(int i=0;i<SArr.length;i++)
          {
               VColumnName.addElement(SArr[i]);
          }
     }

     public void setColumnType(Vector vect)
     {
          for(int i=0;i<vect.size();i++)
          {
               VColumnType.addElement((String)vect.elementAt(i));
          }
     }



     public void setColumnType(String SArr[])
     {
          for(int i=0;i<SArr.length;i++)
          {
               if(SArr[i].equals("S"))
                    VColumnType.addElement(String.valueOf(java.sql.Types.VARCHAR));
               else if(SArr[i].equals("N"))
                    VColumnType.addElement(String.valueOf(java.sql.Types.INTEGER));
               else if(SArr[i].equals("D"))
                    VColumnType.addElement(String.valueOf(java.sql.Types.DOUBLE));
               else if(SArr[i].equals("T"))
                    VColumnType.addElement(String.valueOf(java.sql.Types.TIME));  
               else 
                    VColumnType.addElement(String.valueOf(java.sql.Types.OTHER));  
          }
     }


     public void appendRow(Vector vect)
     {
          VRows.addElement(vect);
     }

     public Vector getColumnName()
     {
          return VColumnName;
     }

     public Vector getColumnType()
     {
          return VColumnType;
     }

     public Vector getRows()
     {
          return VRows;
     }


     public String[] getMap(int iRow,int iCol) throws Exception
     {
          String SArr[] = new String[3];

          Vector row = (Vector)VRows.elementAt(iRow);

          SArr[0] = (String)VColumnName.elementAt(iCol);
          SArr[1] = (String)VColumnType.elementAt(iCol);
          SArr[2] = (String)row.elementAt(iCol);

          return SArr;
     }


     public void setMap(String[] SArr,int iRow,int iCol) throws Exception
     {
          Vector row = (Vector)VRows.elementAt(iRow);

          VColumnName.setElementAt(SArr[0],iCol);
          VColumnType.setElementAt(SArr[1],iCol);
          row.setElementAt(SArr[3],iCol);
     }

     public Vector getColumnValue(String SAttr)
     {
          Vector vect = new Vector();
          int index = getColumnIndex(SAttr);
          if(index == -1)
               return vect;
          for(int i=0;i<VRows.size();i++)
          {
               Vector row = (Vector)VRows.elementAt(i);
               vect.addElement((String)row.elementAt(index));
          }
          return vect;
     }

     public int getColumnIndex(String SAttr)
     {
          for(int i=0;i<VColumnName.size();i++)
          {
               if(SAttr.equals((String)VColumnName.elementAt(i)))
                    return i;
          }
          return -1;
     }
}
