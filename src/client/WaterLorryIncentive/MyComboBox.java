package client.WaterLorryIncentive;

/*
          Assuming the RowSet this ComboBox handles
          are Name in the Zeroth Column
          and Code in the 1st Column
*/
import util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class MyComboBox extends JComboBox
{
     protected RowSet rowSet;

	public MyComboBox()
	{
		super();
//          setKeySelectionManager(new ComboBoxListener());
	}

	public MyComboBox(Vector theVector)
	{
          super(theVector);
//          setKeySelectionManager(new ComboBoxListener());
	}

     public MyComboBox(RowSet rowSet)
     {
          this.rowSet = rowSet;

          Vector vect = getColumn(1);

          for(int i=0;i<vect.size();i++)
          {
               addItem((String)vect.elementAt(i));
          }

     }

     //-- The Accessor Methods ---//

     public RowSet getRowSet()
     {
          return rowSet;
     }

     public void setRowSet(RowSet rowSet)
     {

          if(rowSet == null)
               return;

          this.rowSet = rowSet;

          removeAllItems();

          Vector vect = getColumn(1);

          for(int i=0;i<vect.size();i++)
          {

               addItem((String)vect.elementAt(i));
          }
     }


     /*

          to Return the Column Values from the rowSet

     */


     public Vector getColumn(int iCol)
     {

          Vector vect = new Vector();

          Vector VRows = rowSet.getRows();


          try
          {
               for(int i=0;i<VRows.size();i++)
               {
                    Vector row = (Vector)VRows.elementAt(i);
     
                    vect.addElement((String)row.elementAt(iCol));
               }
          }
          catch(Exception ex){}

          return vect;
     }

     public void setValues(Vector theVect)
     {
          removeAllItems();

          for(int i=0;i<theVect.size();i++)
               addItem((String)theVect.elementAt(i));
     }

     public String getSelectedCode()
     {

          int index = getSelectedIndex();

          Vector vect = getColumn(0);

          return (String)vect.elementAt(index);

     }

     public void setSelectedCode(String SCode)
     {

          Vector vect = getColumn(0);

          int index = vect.indexOf(SCode);

          if (index == -1)
               return;

          setSelectedIndex(index);
     }

     //-- The Internal Methods ---//

     private class ComboBoxListener implements javax.swing.JComboBox.KeySelectionManager
     {
          private java.lang.String str = null;
          public ComboBoxListener()
          {
               super();
               str = new String();
          }
          public int selectionForKey(char arg1, javax.swing.ComboBoxModel arg2)
          {
               if (arg1 == '')
               {
                    str = new String();
                    return -1;
               }
               str += arg1;
               for (int i = 0; i < arg2.getSize(); i++)
               {
                    if (arg2.getElementAt(i).toString().startsWith(str.toUpperCase()))
                    {
                         return i;
                    }
               }
               return -1;
          }
     }
}
