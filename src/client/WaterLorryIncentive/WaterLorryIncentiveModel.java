package client.WaterLorryIncentive;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;
import java.sql.*;
import java.util.*;
import javax.swing.plaf.basic.*;
import util.*;
public class WaterLorryIncentiveModel extends DefaultTableModel
{
     String ColumnName[] = {"Date","Vehicle No","Opening Km", "Closing Km", "Tot No.Of Km/Day ","Place", "Std. Trips/Day on Std.Kms","Eligible Trips"  , "Actual Trips/Day on Actual Kms ","Extra Trip","Incentive Amt","Click"};
     String ColumnType[] = {"S","S","S","S","S","S","S","S","S","S","S","B"};
     int  iColumnWidth[] = {40 ,40 ,40 ,40 ,40 ,40 ,40 ,40 ,40 ,40 , 40, 40};

     public WaterLorryIncentiveModel()
     {
          setDataVector(getRowData(), ColumnName);
     }

    public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0; i<ColumnName.length; i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(), theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
     public void appendEmptyRow()
     {
          Vector curVector = new Vector();
          for(int i=0;i<ColumnName.length;i++) {
               if(i==ColumnName.length-1) {
                  curVector.addElement(new Boolean(false));
               }
               else {
                  if(i==0)
                     curVector.addElement(String.valueOf(getRows()+(i+1)));
                  else
                     curVector.addElement(" ");
               }
          }
          insertRow(getRows(),curVector);

     }

}

