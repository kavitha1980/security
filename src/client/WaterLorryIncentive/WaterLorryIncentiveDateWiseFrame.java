package client.WaterLorryIncentive;
import util.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class WaterLorryIncentiveDateWiseFrame extends JDialog
{
 
     protected    JLayeredPane Layer;

     Common common  = new Common();
 
     JPanel        TopPanel,BottomPanel;
          
     JButton       BExit;

     private   WaterLorryIncentiveDateWiseModel    theModel;

     private   JTable              theTable;
      
     Vector         theVector;

     boolean        bFlag=false;
    
     private        ArrayList                     theEmpList;
    
     private      ArrayList  AType;

     int iEmpCode;

     String SMonth;
	 
	 WaterLorryIncentiveClass theClass;
     
    public WaterLorryIncentiveDateWiseFrame(String SMonth,int iEmpCode)
    {
        this.iEmpCode = iEmpCode;
        this.SMonth   = SMonth;
		  
		try
        {
                       
            this.Layer   = Layer;
        
            createComponents();
            setLayouts();
            addComponents();
            setTableDetails();
            addListeners();
             
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.out.println(ex);
        }
    }
    public void createComponents()
    {
        TopPanel            = new JPanel();
        BottomPanel         = new JPanel();
         
        BExit               = new JButton("Exit");
          
        theModel            = new WaterLorryIncentiveDateWiseModel();
        theTable            = new JTable(theModel);
                                             
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
            (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
    }
    public void setLayouts()
    {
        setTitle("Incentive Driver Details");
        setModal(true) ;
          
        this.getContentPane().setLayout(new BorderLayout());
          
        TopPanel.setLayout(new BorderLayout());
        BottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
       
    }
    public void addComponents()
    {

		TopPanel.setBorder(new TitledBorder(" EMPLOYEE DETAILS"));
        TopPanel.add(new JScrollPane(theTable));
                    
        BottomPanel.setBorder(new TitledBorder("Controls"));
        BottomPanel.add(BExit);
        BExit.setMnemonic('E');

        this.getContentPane().add(TopPanel,BorderLayout.CENTER);
        this.getContentPane().add(BottomPanel,BorderLayout.SOUTH);
    }
    
    public void addListeners()
    {
		BExit.addActionListener(new ActList1());
        this.addWindowListener(new WinList());
  
    }

	private class ActList1 implements ActionListener
    {
          
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==BExit)
            {
                close1();      
            }
        }
    } 
    public void close1()
    {
        try
        {
            theTable = null;
            theModel = null;
            theVector    = null;
            theEmpList  = null;
            dispose();
		}
        catch(Exception e)
        {
			System.out.println(e);
        }
    }
	private class WinList extends WindowAdapter
	{
        public void windowClosing(WindowEvent we)
        {
			theTable = null;
            theModel = null;
            theVector    = null;
            theEmpList  = null;
        }
    }

    private void setTableDetails()
		{
			theModel                           . setNumRows(0);
			theVector = new Vector();
			setVectors();
			int iSlNo                          = 1;
          
		    double dTotalAmt = 0;
									
			for(int i=0;i<theVector.size();i++)
            {
                WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
		   
                HashMap theMap = (HashMap)theClass.theDriverList.get(0);
			
				double dIncentiveAmt = 0.0;
						   
				int iTotalNoOfKMRunPerDay = (common.toInt(String.valueOf(theMap.get("EndKm")))-common.toInt(String.valueOf(theMap.get("StartKm"))) );
				int iExtraTrip            = ( common.toInt(String.valueOf(theMap.get("ActualTrip")))-common.toInt(String.valueOf(theMap.get("StandardTrip"))) );
               
				if(iExtraTrip>0)
					dIncentiveAmt     = ( common.toDouble(String.valueOf(iExtraTrip)) * common.toDouble(String.valueOf(theMap.get("Amount")))  );
				else
				{
					dIncentiveAmt        = 0 ;
					iExtraTrip           = 0 ;
				}
					
			   
                ArrayList theList             = null;
                theList                       = new ArrayList();
                theList                       . clear();
                theList                       . add(common.parseDate((String)theMap.get("Date")));
                theList                       . add(String.valueOf(theMap.get("StartKm")));
                theList                       . add(String.valueOf(theMap.get("EndKm")));
                theList                       . add(String.valueOf(iTotalNoOfKMRunPerDay));
                theList                       . add(String.valueOf(theMap.get("StandardTrip")));
                theList                       . add(String.valueOf(theMap.get("ActualTrip")));
                theList                       . add(String.valueOf(theMap.get("ActualTrip")));
                theList                       . add(String.valueOf(theMap.get("StandardTrip")));
                theList                       . add(String.valueOf(iExtraTrip));
			    theList                       . add(String.valueOf(theMap.get("Amount")));
			    theList                       . add(String.valueOf(dIncentiveAmt));
                theList                       . add(new java.lang.Boolean(false));
               
                dTotalAmt = dTotalAmt + dIncentiveAmt;
			   
			    theModel                      . appendRow(new Vector(theList));
				
				
			}
			    
				ArrayList theList             = null;
                theList                       = new ArrayList();
                theList                       . clear();
				theList                       . clear();
				theList                       . add(null);
				theList                       . add("Total");
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(dTotalAmt);
				theList                       . add(null);
             
                theModel                      . appendRow(new Vector(theList));
				
				
				TOpening.setText(String.valueOf(theClass.dOpening));
				TCarriedOver.setText(String.valueOf(theClass.dCarriedOver));
				TRoundedNet.setText(String.valueOf(theClass.getRoundedNetPay()));
					
				
		    
		}
		private void setVectors()
		{
     
			//theVector = new Vector();
          
			try
			{
				ORAConnection1 oraConnection    =   ORAConnection1.getORAConnection();
				Connection connection           =   oraConnection.getConnection();
				Statement  theStatement         =   connection.createStatement();
				ResultSet  theResult            =   theStatement.executeQuery(getQS());
               
				while(theResult.next())
                {
					int iActualTrip    = theResult.getInt(1);
					String SDate       = theResult.getString(2);
					int iStartKm       = common.toInt(common.parseNull(theResult.getString(3)));
					int iEndKm         = common.toInt(common.parseNull(theResult.getString(4)));
					String SDriverName = common.parseNull(theResult.getString(5));
					int iStandardTrip  = theResult.getInt(6);
					double dAmount     = theResult.getDouble(7);
					int iEmpCode       = theResult.getInt(8);
					
					int iIndex=getIndexOf(iEmpCode,	SDate);	//	
					
					if(iIndex==-1)
                    {

                        WaterLorryIncentiveClass theClass   = new WaterLorryIncentiveClass(iEmpCode,SDate); //
                        theVector.addElement(theClass);
                        iIndex=theVector.size()-1;
                    }
					
                    theClass = (WaterLorryIncentiveClass)theVector.elementAt(iIndex);
                    theClass . appendDetails(iActualTrip,SDate,iStartKm,iEndKm,SDriverName,iStandardTrip,dAmount,iEmpCode);
                    theClass . appendData(iActualTrip,SDate,iStartKm,iEndKm,SDriverName,iStandardTrip,dAmount,iEmpCode);
				}
				
				theResult.close();
								
				theResult                = theStatement.executeQuery(getOpeningQS());
				while(theResult.next())
				{
                    int iEmpCode     = theResult.getInt(1);
                    double dOpening  = theResult.getDouble(2);

                    int iIndex = getIndexOf1(iEmpCode);

                    if(iIndex!=-1)
                    {
                         WaterLorryIncentiveClass theClass =(WaterLorryIncentiveClass)theVector.get(iIndex);
                         theClass.setOpening(dOpening);
                    }
                                      
                    
               }
               theResult.close();
               theStatement.close();
               
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		private String getOpeningQS()
		{
			StringBuffer sb = new StringBuffer();

			sb.append(" Select EmpCode,(-1)*CarriedOver from WaterLorryIncentive");
			sb.append(" where IncentiveMonth =(Select  Max(IncentiveMonth) from WaterLorryIncentive)  ");
     //     System.out.println("Carried OVer QS:"+sb.toString()); 
         
			return sb.toString();
		}
		private int getIndexOf(int iEmpCode,String SDate) //
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			    if((iEmpCode==theClass.iEmpCode) && (SDate.equals(theClass.SDate))   ) 
				//if((SDate.equals(theClass.SDate)))
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private int getIndexOf1(int iEmpCode)
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
				if(iEmpCode==theClass.iEmpCode)
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private  String getQS()
		{
			String QS = "";
							
			String SStDate       = SMonth+"01";
			String SEnDate       = SMonth+"31";
						        
			QS = " Select Count(*),OutDate,Min(STKM),Max(ENDKM),DriverName,StandardTrips,Amount,DriverUserCode from Vehicles "+
					    " Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo"+
						" where  OutDate>="+SStDate+" and OutDate<="+SEnDate+" "+
						" and DriverUserCode ="+iEmpCode+" and "+
						" (( OutTime>='08.00 AM'   or OutTime<='20.00 PM') or (OutTime>='20.00 PM'  or OutTime<'08.00 AM') ) "+
						" group by OutDate,DriverName,StandardTrips,Amount,DriverUserCode order by OutDate ";
			
			//System.out.println("QS:"+QS);
			return QS;
		}
      
     



     private void refresh()
     {
        try
        {
          theVector.removeAllElements();
          theModel.setNumRows(0);
                
        }
        catch(Exception e)
        {
          System.out.println(e);
          e.printStackTrace();
        }

     }
 


    private void removeFrame()
     {
	try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
        catch(Exception ex){}
     }
    
}

