
package client.WaterLorryIncentive;
import util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;
import util.*;

public class AttDateField extends JPanel{
 
    protected int iStDate,iEnDate;
    protected boolean bFlag=false;

    JTextField theDate;
    Common common = new Common();

    public AttDateField()
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(7);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        theDate.setBackground(new Color(255,222,206));    

    }

    public AttDateField(int iColumns)
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(iColumns);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        theDate.setBackground(new Color(255,222,206));    

    }
     
    public AttDateField(boolean isCurrent)
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(7);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate.setBackground(new Color(255,222,206));    

        setTodayDate();
        theDate.setEnabled(false);
    }

    public AttDateField(int iStDate,int iEnDate)
    {
        this.iStDate = iStDate;
        this.iEnDate = iEnDate;
        bFlag = true;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate.setBackground(new Color(255,222,206));

    }

    private boolean isBeyond()
    {

        int iDate = common.toInt(toNormal());

        if(iDate >= iStDate && iDate <= iEnDate)
            return false;

        return true;
    }

    //-- Data Accessor methods---//

    public void setEditable(boolean bFlag)
    {
        theDate.setEditable(bFlag);
    }

    public void setEnabled(boolean bFlag)
    {
        theDate.setEnabled(bFlag);
    }

    public void setTodayDate() 
    {
        String SDay,SMonth,SYear;
        try
        {
            String SDate = common.getServerDate();
    
            SDay         = SDate.substring(0,2);
            SMonth       = SDate.substring(3,5);
            SYear        = SDate.substring(6,10);
    
            theDate.setText(SDay+"."+SMonth+"."+SYear);
        }
        catch(Exception ex)
        {
        }
    }

    public String toNormal()
    {
        return common.pureDate(theDate.getText());
    }
    public String toString()
    {
        return theDate.getText();
    }
    public void fromString(String str)
    {
        theDate.setText(common.parseDate(str));
    }

    public void requestFocus()
    {
        theDate.requestFocus();
    }

}
