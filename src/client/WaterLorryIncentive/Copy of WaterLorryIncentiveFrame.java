package client.WaterLorryIncentive;

import util.*;
import java.sql.*;
import java.util.*;
import java.util.ArrayList;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.text.SimpleDateFormat;

    public class WaterLorryIncentiveFrame extends JInternalFrame
    {
      
        protected    JLayeredPane Layer;
          
        JPanel       TopPanel,TitlePanel,FilterPanel,MiddlePanel,BottomPanel,AmountPanel,ControlPanel,AttachmentPanel;
                         
        JButton      BSave,BExit,BApply,BPrint,BAttachment;
          
        MyLabel      LStDate,LEnDate,LOpening,LCarriedOver,LRoundedNet;
		
		JTextField   TOpening,TCarriedOver,TRoundedNet;
          
        AttDateField TStDate,TEnDate;
                              
        MyComboBox   JCDriversName;
                 
        private      JTable           theTable;
          
        private      WaterLorryIncentiveModel    theModel;
                                                         
        Vector       theVector;
                              
        private      ArrayList  ADriverList,ADriverName,ADriverEmpCode;
          
        int iEmpCode,iRow,iCol;
		
		String SAuthSysName;
		
		int iUSerCode,iFlowCode;
          
        Common common  = new Common();
		
		WaterLorryIncentiveClass theClass;
     		 
		int Date=0;
		int OpeningKm=1;
		int ClosingKm=2;
		int TotalNoOfKmPerDay=3;
		int StdTripsPerDayOnStdKms=4;
		int ActualTripsPerDayOnActualKms=5;
		int ActualWaterLoadTrips=6;
		int CeilingWaterLoadTrips=7;
		int ExtraTrip=8;
		int IncentiveRatePerExtraTrip=9;
		int TotalIncentive=10;
		int Click=11;
		
		public WaterLorryIncentiveFrame(JLayeredPane Layer)//int iUserCode,int iFlowCode,
		{
			try
			{
				this.Layer   = Layer;
                setDriversName();
				createComponents();
				setLayouts();
				addComponents();
				addListeners();
				
				try
				{
					SAuthSysName=InetAddress.getLocalHost().getHostName();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			catch(Exception ex)
			{
               ex.printStackTrace();
               System.out.println(ex);
			}
		}  

		public void createComponents()
		{
			TopPanel            = new JPanel();
			TitlePanel          = new JPanel();
			FilterPanel         = new JPanel();
			MiddlePanel         = new JPanel();
			BottomPanel         = new JPanel();
			ControlPanel        = new JPanel();
			AmountPanel         = new JPanel();
			AttachmentPanel     = new JPanel();
             
			BSave               = new JButton("Save");
			BExit               = new JButton("Exit");
			BApply              = new JButton("Apply");
			BAttachment         = new JButton("Attachment");
			BPrint              = new JButton("Print");

			LStDate             = new MyLabel("Start Date");
			LEnDate             = new MyLabel("End Date");
			LOpening            = new MyLabel("Opening");
			LCarriedOver        = new MyLabel("Carried OVer");
			LRoundedNet         = new MyLabel("RoundedNet");
			
			TOpening            = new JTextField(15);
			TOpening            . setEditable(false);
			TCarriedOver        = new JTextField(15);
			TCarriedOver        . setEditable(false);
			TRoundedNet         = new JTextField(15);
			TRoundedNet         . setEditable(false);
			                   
			TStDate             = new AttDateField();
			TEnDate             = new AttDateField();
            JCDriversName       = new MyComboBox(new Vector(ADriverName));
          
            theModel            = new WaterLorryIncentiveModel();
			theTable            = new JTable(theModel);
                 
			for(int i=0;i<theModel.ColumnName.length;i++)
			{
				(theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
			}
		}
     
		public void setLayouts()
		{
			setTitle("WATER LORRY INCENTIVE FRAME");
			setMaximizable(true);
			setClosable(true);
			setResizable(true);
			setIconifiable(true);
			setSize(950,600);
                    
            FilterPanel.setLayout(new GridLayout (1,7));
			TopPanel. setLayout(new BorderLayout());
			MiddlePanel.setLayout(new BorderLayout ());
			BottomPanel.setLayout(new GridLayout (1,3));
			ControlPanel.setLayout(new FlowLayout());
			AttachmentPanel.setLayout(new FlowLayout());
			AmountPanel.setLayout(new GridLayout (3,2));
               
		}

		public void addComponents()
		{
			TitlePanel.add(new MyLabel("WATER LORRY EXTRA TRIP INCENTIVE "));
			
            FilterPanel.setBorder(new TitledBorder("Filter"));
			FilterPanel.add(LStDate);
			FilterPanel.add(TStDate);
			FilterPanel.add(LEnDate);
			FilterPanel.add(TEnDate);
			FilterPanel.add(new MyLabel("Driver Name"));
			FilterPanel.add(JCDriversName);
			FilterPanel.add(BApply);
             
			TopPanel.add(TitlePanel,BorderLayout.NORTH);
			TopPanel.add(FilterPanel,BorderLayout.SOUTH);
			
			MiddlePanel.setBorder(new TitledBorder("Details")); 
            MiddlePanel.add(new JScrollPane(theTable));
                
				
			ControlPanel.setBorder(new TitledBorder("Authenticate"));
			ControlPanel.add(BSave);
			ControlPanel.add(BExit);
			ControlPanel.add(BPrint);
			
			AmountPanel.setBorder(new TitledBorder("Amount Data"));
			AmountPanel.add(LOpening);
			AmountPanel.add(TOpening);
			AmountPanel.add(LCarriedOver);
			AmountPanel.add(TCarriedOver);
			AmountPanel.add(LRoundedNet);
			AmountPanel.add(TRoundedNet);
			
			AttachmentPanel.setBorder(new TitledBorder("Approval"));
			AttachmentPanel.add(BAttachment);
			
			BottomPanel.setBorder(new TitledBorder("Controls"));
			BottomPanel.add(ControlPanel);
			BottomPanel.add(AmountPanel);
			BottomPanel.add(AttachmentPanel);
						
			getContentPane().add("North",TopPanel);
			getContentPane().add("Center",MiddlePanel);
			getContentPane().add("South",BottomPanel);
		}

		public void addListeners()
		{
			MyListener  Listener = new MyListener();
			BSave .addActionListener(Listener);
			BExit.addActionListener(Listener);
			BApply.addActionListener(Listener);
			BPrint. addActionListener(Listener);
		//	theTable . addKeyListener(new keyList());
		//	theTable . addKeyListener(new TableKeyList());
		} 
      
		private void setDriversName()
		{
            ADriverName     = null;
			ADriverEmpCode        = null;
          
			ADriverName     = new ArrayList();
			ADriverEmpCode        = new ArrayList();
         
		    ADriverName       .clear();       
			ADriverEmpCode        .clear();
                                        
			try
			{
                   
				ORAConnection oraConnection     =   ORAConnection.getORAConnection();
				Connection connection           =   oraConnection.getConnection();
				Statement  theStatement         =   connection.createStatement();
				ResultSet  theResult            =   theStatement.executeQuery("Select EmpCode,EmpName from Staff where DesignationCode=278 order by 2 ");
				while(theResult.next())
				{
                    ADriverEmpCode.add(theResult.getString(1));
                    ADriverName.add(theResult.getString(2));
                }
            }
			catch(Exception ex)
			{
				System.out.println(ex);
				ex.printStackTrace();
			}
                   
		}

		private class MyListener implements ActionListener, ItemListener
		{
		    public void actionPerformed(ActionEvent ae)
			{
				try
				{
					if(ae.getSource()==BApply)
					{
						setTableDetails();     
					}
		
					if(ae.getSource()==BPrint) 
					{
						try
						{
							String SFile = "D:\\WaterLorryIncentive.pdf";
							
							String SFromDate       = TStDate.toNormal();
							String SToDate       = TEnDate.toNormal();
							String SDriverName   = (String)JCDriversName.getSelectedItem() ;
						
							WaterLorryIncentivePDF createPDF = new WaterLorryIncentivePDF(SFile,SFromDate,SToDate,SDriverName,theVector);
																					
						}
						catch(Exception ex)
						{
						   System.out.println("PrnCreation"+ex);
						   ex.printStackTrace();
						}
					}
					if (ae.getSource() == BSave)
					{
						if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
						{
							SaveDetails();
						}							  
					} 
					if (ae.getSource() == BExit)
					{
						theTable    = null;
						theModel    = null;
						theVector   = null;
						ADriverList  = null;
						
						
						removeFrame();
						Layer.repaint();
						Layer.updateUI();
					}
				}
				catch(Exception e)
				{
				   System.out.println(e);
				   e.printStackTrace();
				}
			}
			public void itemStateChanged(ItemEvent ie)
			{
				try
				{
					theModel . setNumRows(0);                                                                       
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}
			}
		}
           
        private void setTableDetails()
		{
			theModel                           . setNumRows(0);
			theVector = new Vector();
			setVectors();
			int iSlNo                          = 1;
          
		    double dTotalAmt = 0;
									
			for(int i=0;i<theVector.size();i++)
            {
                WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
		   
                HashMap theMap = (HashMap)theClass.theDriverList.get(0);
			
				double dIncentiveAmt = 0.0;
						   
				int iTotalNoOfKMRunPerDay = (common.toInt(String.valueOf(theMap.get("EndKm")))-common.toInt(String.valueOf(theMap.get("StartKm"))) );
				int iExtraTrip            = ( common.toInt(String.valueOf(theMap.get("ActualTrip")))-common.toInt(String.valueOf(theMap.get("StandardTrip"))) );
               
				if(iExtraTrip>0)
					dIncentiveAmt     = ( common.toDouble(String.valueOf(iExtraTrip)) * common.toDouble(String.valueOf(theMap.get("Amount")))  );
				else
				{
					dIncentiveAmt        = 0 ;
					iExtraTrip           = 0 ;
				}
					
			   
                ArrayList theList             = null;
                theList                       = new ArrayList();
                theList                       . clear();
                theList                       . add(common.parseDate((String)theMap.get("Date")));
                theList                       . add(String.valueOf(theMap.get("StartKm")));
                theList                       . add(String.valueOf(theMap.get("EndKm")));
                theList                       . add(String.valueOf(iTotalNoOfKMRunPerDay));
                theList                       . add(String.valueOf(theMap.get("StandardTrip")));
                theList                       . add(String.valueOf(theMap.get("ActualTrip")));
                theList                       . add(String.valueOf(theMap.get("ActualTrip")));
                theList                       . add(String.valueOf(theMap.get("StandardTrip")));
                theList                       . add(String.valueOf(iExtraTrip));
			    theList                       . add(String.valueOf(theMap.get("Amount")));
			    theList                       . add(String.valueOf(dIncentiveAmt));
                theList                       . add(new java.lang.Boolean(false));
               
                dTotalAmt = dTotalAmt + dIncentiveAmt;
			   
			    theModel                      . appendRow(new Vector(theList));
				
				
			}
			    
				ArrayList theList             = null;
                theList                       = new ArrayList();
                theList                       . clear();
				theList                       . clear();
				theList                       . add(null);
				theList                       . add("Total");
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(null);
				theList                       . add(String.valueOf(dTotalAmt));
				theList                       . add(null);
             
                theModel                      . appendRow(new Vector(theList));
				
				
				TOpening.setText(String.valueOf(theClass.dOpening));
				TCarriedOver.setText(String.valueOf(theClass.dCarriedOver));
				TRoundedNet.setText(String.valueOf(theClass.getRoundedNetPay()));
					
				
		    
		}
		private void setVectors()
		{
     
			//theVector = new Vector();
          
			try
			{
				ORAConnection1 oraConnection    =   ORAConnection1.getORAConnection();
				Connection connection           =   oraConnection.getConnection();
				Statement  theStatement         =   connection.createStatement();
				ResultSet  theResult            =   theStatement.executeQuery(getQS());
               
				while(theResult.next())
                {
					int iActualTrip    = theResult.getInt(1);
					String SDate       = theResult.getString(2);
					int iStartKm       = common.toInt(common.parseNull(theResult.getString(3)));
					int iEndKm         = common.toInt(common.parseNull(theResult.getString(4)));
					String SDriverName = common.parseNull(theResult.getString(5));
					int iStandardTrip  = theResult.getInt(6);
					double dAmount     = theResult.getDouble(7);
					int iEmpCode       = theResult.getInt(8);
					
					int iIndex=getIndexOf(iEmpCode,	SDate);	//	
					
					if(iIndex==-1)
                    {

                        WaterLorryIncentiveClass theClass   = new WaterLorryIncentiveClass(iEmpCode,SDate); //
                        theVector.addElement(theClass);
                        iIndex=theVector.size()-1;
                    }
					
                    theClass = (WaterLorryIncentiveClass)theVector.elementAt(iIndex);
                    theClass . appendDetails(iActualTrip,SDate,iStartKm,iEndKm,SDriverName,iStandardTrip,dAmount,iEmpCode);
                    theClass . appendData(iActualTrip,SDate,iStartKm,iEndKm,SDriverName,iStandardTrip,dAmount,iEmpCode);
				}
				
				theResult.close();
								
				theResult                = theStatement.executeQuery(getOpeningQS());
				while(theResult.next())
				{
                    int iEmpCode     = theResult.getInt(1);
                    double dOpening  = theResult.getDouble(2);

                    int iIndex = getIndexOf1(iEmpCode);

                    if(iIndex!=-1)
                    {
                         WaterLorryIncentiveClass theClass =(WaterLorryIncentiveClass)theVector.get(iIndex);
                         theClass.setOpening(dOpening);
                    }
                                      
                    
               }
               theResult.close();
               theStatement.close();
               
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		private String getOpeningQS()
		{
			StringBuffer sb = new StringBuffer();

			sb.append(" Select EmpCode,(-1)*CarriedOver from WaterLorryIncentive");
			sb.append(" where IncentiveMonth =(Select  Max(IncentiveMonth) from WaterLorryIncentive)  ");
     //     System.out.println("Carried OVer QS:"+sb.toString()); 
         
			return sb.toString();
		}
		private int getIndexOf(int iEmpCode,String SDate) //
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			    if((iEmpCode==theClass.iEmpCode) && (SDate.equals(theClass.SDate))   ) 
				//if((SDate.equals(theClass.SDate)))
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private int getIndexOf1(int iEmpCode)
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
				if(iEmpCode==theClass.iEmpCode)
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private  String getQS()
		{
			String QS = "";
						
			String SStDate       = TStDate.toNormal();
			String SEnDate       = TEnDate.toNormal();
			
			int iIndex = JCDriversName.getSelectedIndex() ;
			
			String SEmpCode = (String)ADriverEmpCode.get(iIndex);
			

			        
					QS = " Select Count(*),OutDate,Min(STKM),Max(ENDKM),DriverName,StandardTrips,Amount,DriverUserCode from Vehicles "+
					     " Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo"+
						 " where  OutDate>="+SStDate+" and OutDate<="+SEnDate+" "+
						 " and DriverUserCode ="+SEmpCode+" and "+
						 " (( OutTime>='08.00 AM'   or OutTime<='20.00 PM') or (OutTime>='20.00 PM'  or OutTime<'08.00 AM') ) "+
						 /*" and (( to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')>='08.00'  "+
						 " or to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'20.00') or (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='20.00' "+
						 " or to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')<'08.00') ) "+*/
						 " group by OutDate,DriverName,StandardTrips,Amount,DriverUserCode order by OutDate ";
			
			//System.out.println("QS:"+QS);
			return QS;
		}
      
    private boolean CheckAnyOneSelected()
    {
        for(int i=0;i<theModel.getRowCount();i++)
        {
            java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,Click);

            if(bValue.booleanValue())
            {
                return true;
            }
        }
        JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Save","Information",JOptionPane.ERROR_MESSAGE);
        return false;
    }
            
    private int getIsAlreadyExist(int iEmpCode,String SMonth)
	{
        int iCount                    = 0;
          
        ORAConnection1 oraConnection   =   ORAConnection1.getORAConnection();
        Connection theConnection      =   oraConnection.getConnection();
          
        try
        {
			PreparedStatement thePS  = theConnection.prepareStatement("Select Count(*) from WaterLorryIncentive Where  EmpCode = "+iEmpCode+"  and IncentiveMonth="+SMonth+"  ");
            ResultSet rs             = thePS.executeQuery();
			 
            if(rs.next())
            {
                iCount              = rs.getInt(1);
            }
            rs                       . close();
            thePS                    . close();
        }                    
        catch(Exception ex)
		{
			ex.printStackTrace();
        }
        
		return iCount;       
    }
 
    private void SaveDetails()
    {
        Connection theConnection           = null;
              
        try
        {
            ORAConnection1 oraConnection     =   ORAConnection1.getORAConnection();
            theConnection                   =   oraConnection.getConnection();
               
            theConnection                 . setAutoCommit(false);
            int iExecuteCount=0,iExCount=0;

			int iExtraTrip=0;
			double dAmount=0.0;
									
			int iIndex = JCDriversName.getSelectedIndex() ;
				
			int iEmpCode    = common.toInt((String)ADriverEmpCode.get(iIndex));
			String SEmpName = (String)JCDriversName.getSelectedItem() ;	
            for(int i=0; i<theModel.getRowCount()-1; i++)
            {
                iExtraTrip  = iExtraTrip + common.toInt(String.valueOf(theModel.getValueAt(i,ExtraTrip)));
				dAmount     = dAmount+ common.toDouble(String.valueOf(theModel.getValueAt(i,10)));
			}
			int iFromDate            = common.toInt(TStDate.toNormal());
			int iToDate              = common.toInt(TEnDate.toNormal());
                     
			String SMonth = (TStDate.toNormal()).substring(0,6);                     
			int iOpening     = common.toInt(TOpening.getText() );
			int iCarriedOver = common.toInt(TCarriedOver.getText());
			double dRoundedNet = common.toDouble(TRoundedNet.getText());
						 
                        int iIsAlreadyExist = getIsAlreadyExist(iEmpCode,SMonth);
				        if(iIsAlreadyExist==0)
						{

                        PreparedStatement ps     = null;
                        ps                       = theConnection.prepareStatement(getSaveQS());
                              
                         ps    .    setInt(1,iEmpCode);
						 ps    .    setString(2,SEmpName);
                         ps    .    setInt(3,iExtraTrip); 
                         ps    .    setDouble(4,dAmount);
                         ps    .    setString(5,SMonth);
                         ps    .    setString(6,SAuthSysName);
                         ps    .    setInt(7,iOpening);
						 ps    .    setInt(8,iCarriedOver);
                         ps    .    setDouble(9,dRoundedNet);
						 ps    .    setInt(10,iFlowCode);
                                                          
                         iExCount =  ps               . executeUpdate();
                             
                         ps         . close();
                         ps         = null;
                         iExecuteCount     = iExecuteCount+iExCount;
                         }
                                                    
                                            
                
               
               if(iExecuteCount>0)
               {          
                    JOptionPane.showMessageDialog(null, "Data Saved Succesfully");
                 // setTableDetails();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"Data Already Processed");     
               }
               theConnection                 . commit();
               theConnection          = null;
               
          }
          catch (Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null, "Problem in  Saving Data", "Error", 
               JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
               try
               {
                    theConnection      . rollback();   
               } catch(Exception e){}
               
          }
     }
    private String getSaveQS()
    {
        StringBuffer SB = null;
    
		SB = new StringBuffer();
          
        SB.append(" Insert into WaterLorryIncentive(ID,EmpCode,DriverName,ExtraTrips,Amount,IncentiveMonth,EntryDate,EntryDateAndTime,EntrySystemName,Opening,CarriedOver,RoundedNet,FlowCode) ");
        SB.append(" values(WaterLorryIncentive_Seq.nextVal,?,?,?,?,?,to_Char(SysDate,'DD.MM.YYYY'),to_Char(SysDate,'DD.MM.YYYY HH24:MI:SS'),?,?,?,?,?) ");
         // System.out.println("QS:"+SB.toString());
        return SB.toString();
    }
	

    private void refresh()
    {
        try
        {
            theVector.removeAllElements();
            theModel.setNumRows(0);
        }
        catch(Exception e)
        {
			System.out.println(e);
            e.printStackTrace();
        }
    }
     
    private void removeFrame()
    {
        try
        {
            Layer.remove(this);
            Layer.updateUI();
        }
        catch(Exception ex){}
    }
  
    
}
