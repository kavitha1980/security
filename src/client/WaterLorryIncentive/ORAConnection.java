package client.WaterLorryIncentive;
import util.*;
import java.sql.*;
import java.io.*;
import java.util.*;

class ORAConnection
{
	static ORAConnection connect = null;
	Connection theConnection = null;

	private ORAConnection()
	{
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
               theConnection = 
                   DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");   

               
		}
		catch(Exception e)
		{
			System.out.println("ORAConnection : "+e);
		}
	}
     private ORAConnection(String SPropertyFile)
	{
          ResourceBundle res = ResourceBundle.getBundle(SPropertyFile);

          String SDsn        = res.getString("dsn");
          String SUserName   = res.getString("UserName");
          String SPassword   = res.getString("Password");

		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
               theConnection = 
                   DriverManager.getConnection("jdbc:oracle:thin:@"+SDsn,SUserName,SPassword);   

               
		}
		catch(Exception e)
		{
			System.out.println("ORAConnection : "+e);
		}
	}


	synchronized public static ORAConnection getORAConnection()
	{
		if (connect == null)
		{
			connect = new ORAConnection();
		}
		return connect;
	}
     synchronized public static ORAConnection getORAConnection(String SPropertyFile)
	{
		if (connect == null)
		{
               connect = new ORAConnection(SPropertyFile);
		}
		return connect;
	}


	public Connection getConnection()
	{
		return theConnection;
	}

}
