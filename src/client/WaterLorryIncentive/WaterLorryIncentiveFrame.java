package client.WaterLorryIncentive;

import java.sql.*;
import java.util.*;
import java.util.ArrayList;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import util.*;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class WaterLorryIncentiveFrame extends JInternalFrame
{
	protected    JLayeredPane Layer;
          
    JPanel       TopPanel,TitlePanel,FilterPanel,MiddlePanel,BottomPanel,AmountPanel,ControlPanel,AttachmentPanel;
                         
    JButton      BSave,BExit,BApply,BPrint,BAttachment;
          
    MyLabel      LStDate,LEnDate,LOpening,LCarriedOver,LRoundedNet;
		
	JTextField   TOpening,TCarriedOver,TRoundedNet;
          
    AttDateField TStDate,TEnDate;
                              
    MyComboBox   JCDriversName;
         
    private      JTable           theTable;
          
    private      WaterLorryIncentiveModel    theModel;
                                                         
    Vector       theVector;
                              
    private      ArrayList  ADriverList,ADriverName,ADriverEmpCode,AFireTankDataList;
	
	String		SFireTankSTKM  = "",SFireTankENDKM = "" ;
          
    int iEmpCode,iRow,iCol;
		
	String SAuthSysName;
		
	int iUSerCode,iFlowCode;
		
	ArrayList theDriverList;
          
    Common common  = new Common();
		
	WaterLorryIncentiveClass theClass;

	String SVehicleNo="";
     		 
	int Date=0;
	int OpeningKm=1;
	int ClosingKm=2;
	int TotalNoOfKmPerDay=3;
	int StdTripsPerDayOnStdKms=4;
	int ActualTripsPerDayOnActualKms=5;
	int ActualWaterLoadTrips=6;
	int CeilingWaterLoadTrips=7;
	int ExtraTrip=7;
	int IncentiveRatePerExtraTrip=9;
	int TotalIncentive=10;
	int Click=11;

	private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
    private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.RED);
    private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
    private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

    private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
    private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

    private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 14,Font.UNDERLINE);

	int iWidth[] = {8,10,15,15,15,25,15,15,15,10,10};

	Document document;
    PdfPTable table;
    PdfPCell c1;

	int iTotalColumns = 11;
		
	public WaterLorryIncentiveFrame(JLayeredPane Layer)
	{
		try
		{
			this.Layer   = Layer;
			setDriversName();
			createComponents();
			setLayouts();
			addComponents();
			addListeners();
				
			try
			{
				SAuthSysName=InetAddress.getLocalHost().getHostName();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		catch(Exception ex)
		{
               		ex.printStackTrace();
	                System.out.println(ex);
		}
	}  
	
	public void createComponents()
	{
		TopPanel            = new JPanel();
		TitlePanel          = new JPanel();
		FilterPanel         = new JPanel();
		MiddlePanel         = new JPanel();
		BottomPanel         = new JPanel();
		ControlPanel        = new JPanel();
		AmountPanel         = new JPanel();
		AttachmentPanel     = new JPanel();
            
		BSave               = new JButton("Save");
		BExit               = new JButton("Exit");
		BApply              = new JButton("Apply");
		BAttachment         = new JButton("Attachment");
		BPrint              = new JButton("Print");

		LStDate             = new MyLabel("Start Date");
		LEnDate             = new MyLabel("End Date");
		LOpening            = new MyLabel("Opening");
		LCarriedOver        = new MyLabel("Carried OVer");
		LRoundedNet         = new MyLabel("RoundedNet");
			
		TOpening            = new JTextField(15);
		TOpening            . setEditable(false);
		TCarriedOver        = new JTextField(15);
		TCarriedOver        . setEditable(false);
		TRoundedNet         = new JTextField(15);
		TRoundedNet         . setEditable(false);
		                   
		TStDate             = new AttDateField();
		TEnDate             = new AttDateField();
        JCDriversName       = new MyComboBox(new Vector(ADriverName));
          
        theModel            = new WaterLorryIncentiveModel();
		theTable            = new JTable(theModel);
                 
		for(int i=0;i<theModel.ColumnName.length;i++)
		{
			(theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
		}
	}
     
	public void setLayouts()
	{
		setTitle("WATER LORRY INCENTIVE FRAME");
		setMaximizable(true);
		setClosable(true);
		setResizable(true);
		setIconifiable(true);
		setSize(950,600);
                    
        FilterPanel.setLayout(new GridLayout (1,7));
		TopPanel. setLayout(new BorderLayout());
		MiddlePanel.setLayout(new BorderLayout ());
		BottomPanel.setLayout(new GridLayout (1,3));
		ControlPanel.setLayout(new FlowLayout());
		AttachmentPanel.setLayout(new FlowLayout());
		AmountPanel.setLayout(new GridLayout (3,2));
              
	}

	public void addComponents()
	{
		TitlePanel.add(new MyLabel("WATER LORRY EXTRA TRIP INCENTIVE "));
			
        FilterPanel.setBorder(new TitledBorder("Filter"));
		FilterPanel.add(LStDate);
		FilterPanel.add(TStDate);
		FilterPanel.add(LEnDate);
		FilterPanel.add(TEnDate);
		FilterPanel.add(new MyLabel("Driver Name"));
		FilterPanel.add(JCDriversName);
		FilterPanel.add(BApply);
             
		TopPanel.add(TitlePanel,BorderLayout.NORTH);
		TopPanel.add(FilterPanel,BorderLayout.SOUTH);
			
		MiddlePanel.setBorder(new TitledBorder("Details")); 
        MiddlePanel.add(new JScrollPane(theTable));
                
		ControlPanel.setBorder(new TitledBorder("Authenticate"));
		ControlPanel.add(BSave);
		ControlPanel.add(BExit);
		ControlPanel.add(BPrint);
			
		/*AmountPanel.setBorder(new TitledBorder("Amount Data"));
		AmountPanel.add(LOpening);
		AmountPanel.add(TOpening);
		AmountPanel.add(LCarriedOver);
		AmountPanel.add(TCarriedOver);
		AmountPanel.add(LRoundedNet);
		AmountPanel.add(TRoundedNet);*/
		
		AttachmentPanel.setBorder(new TitledBorder("Approval"));
		AttachmentPanel.add(BAttachment);
			
		BottomPanel.setBorder(new TitledBorder("Controls"));
		BottomPanel.add(ControlPanel);
		//BottomPanel.add(AmountPanel);
		BottomPanel.add(AttachmentPanel);
						
		getContentPane().add("North",TopPanel);
		getContentPane().add("Center",MiddlePanel);
		getContentPane().add("South",BottomPanel);
	}

	public void addListeners()
	{
		MyListener  Listener = new MyListener();
		BSave .addActionListener(Listener);
		BExit.addActionListener(Listener);
		BApply.addActionListener(Listener);
		BPrint. addActionListener(Listener);
	
	} 
      
	private void setDriversName()
	{
		ADriverName     = null;
		ADriverEmpCode  = null;
          
		ADriverName     = new ArrayList();
		ADriverEmpCode  = new ArrayList();
         
		ADriverName     .clear();       
		ADriverEmpCode  .clear();
                                        
		String QS = " Select EmpCode,EmpName||'('||(EmpCode)||')' from Staff where DesignationCode=160 "+
					" Union All "+
					" Select EmpCode,EmpName||'('||(EmpCode)||')' from ReliveEmp where EmpName like '%SIVAKUMAR%' and DesignationCode=160 "+
					" order by 2 ";


		try
		{
        	ORAConnection oraConnection     =   ORAConnection.getORAConnection();
			Connection connection           =   oraConnection.getConnection();
			Statement  theStatement         =   connection.createStatement();
			ResultSet  theResult            =   theStatement.executeQuery(QS);
			while(theResult.next())
			{
            	ADriverEmpCode.add(theResult.getString(1));
            	ADriverName.add(theResult.getString(2));
            }
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			ex.printStackTrace();
		}
                   
	}

	private class MyListener implements ActionListener, ItemListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			try
			{
				if(ae.getSource()==BApply)
				{
					setTableDetails();     
				}
		
				if(ae.getSource()==BPrint) 
				{
					try
					{
						/*String SFile = "D:\\WaterLorryIncentive.pdf";
							
						String SFromDate       = TStDate.toNormal();
						String SToDate       = TEnDate.toNormal();
						String SDriverName   = (String)JCDriversName.getSelectedItem() ;
						
						WaterLorryIncentivePDF createPDF = new WaterLorryIncentivePDF(SFile,SFromDate,SToDate,SDriverName,theVector,ADriverList);*/

						CreatePDF();
																					
					}
					catch(Exception ex)
					{
					   System.out.println("PrnCreation"+ex);
					   ex.printStackTrace();
					}
				}

				if (ae.getSource() == BSave)
				{
					if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
					{
						/*  if(CheckAnyOneSelected())
						{
							if(IsAlreadyExist())
							{*/
								SaveDetails();
							/*	}
							else
							{
								JOptionPane.showMessageDialog(null,"Data Already Processes For this Month ","Information",JOptionPane.ERROR_MESSAGE);
							} 
						}*/
					}
							  
				} 
				
				if (ae.getSource() == BExit)
				{
					theTable    = null;
					theModel    = null;
					theVector   = null;
					ADriverList  = null;
					
					removeFrame();
					Layer.repaint();
					Layer.updateUI();
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
		}

		public void itemStateChanged(ItemEvent ie)
		{
			try
			{
				theModel . setNumRows(0);                                                                       
			}
			catch(Exception e)
			{
				System.out.println(e);
				e.printStackTrace();
			}
		}
	}
           
    private void setTableDetails()
	{
		theModel                           . setNumRows(0);
		theVector = new Vector();
		setVectors();
		setFireTankData();
		int iSlNo                          = 1;
          
		ADriverList = new ArrayList();

		double dTotalAmt = 0;
		int iTotalTrip = 0;
							
		int iStDate       = common.toInt(TStDate.toNormal());
		int iEnDate       = common.toInt(TEnDate.toNormal());	
		int iNextDate     = common.getNextDate(iEnDate);
		int j = 0 ;						

		for(int i=iStDate;i<=iNextDate;i++)
		{
			int    iDayShiftExtraTrip       = 0,  iNightShiftExtraTrip      = 0;
			int    iDayShiftExtraTrip12     = 0,  iNightShiftExtraTrip12    = 0;
			double dIncentiveAmt            = 0.0,dDayShiftIncentiveAmt     = 0.0,dDayShiftIncentiveAmt12 = 0.0;
			double dNightShiftIncentiveAmt  = 0.0,dNightShiftIncentiveAmt12 = 0.0;
					
			int iDayShiftTrip   = getDayShiftTrips(iStDate);
			int iNightShiftTrip = getNightShiftTrips(iStDate);
													
			if(iStDate==iNextDate)
				break ;	

			System.out.println("theDriverList :"+theDriverList.size());

			if(theDriverList.size()<=j)
				break;
			
			
			HashMap theMap = (HashMap)theDriverList.get(j);

			double dAmt   = common.toDouble(String.valueOf(theMap.get("Amount")));
			
System.out.println("=================================================");
System.out.println("Date :"+iStDate);	
System.out.println("Day Shift Trip :"+iDayShiftTrip);	
System.out.println("iDayShiftExtraTrip :"+iDayShiftExtraTrip);
System.out.println("Night Shift Trip :"+iNightShiftTrip);	
System.out.println("iNightShiftExtraTrip :"+iNightShiftExtraTrip);
System.out.println("=================================================");
					
			int iExtraTrip12 		  = 0;
			dDayShiftIncentiveAmt12   = 0.0;
			dNightShiftIncentiveAmt12 = 0.0;

			int iActualTrip  = (iDayShiftTrip + iNightShiftTrip);
//			int iExtraTrip   = (iDayShiftExtraTrip + iNightShiftExtraTrip);

			int iExtraTrip   = (iDayShiftTrip + iNightShiftTrip)-6;;

			if(iExtraTrip>0)
				iExtraTrip   = iExtraTrip;
			else
				iExtraTrip = 0;

		//	dIncentiveAmt    = dDayShiftIncentiveAmt + dNightShiftIncentiveAmt ;
			dIncentiveAmt	= iExtraTrip * 20;
								
			int iEndKm   = 0;
			int iStartKm = getStartKM(iStDate);
			if(iStartKm>0)
				iEndKm   = getEndKM(iStDate);
				
			int iTotalNoOfKMRunPerDay = 0;	
			if(iEndKm>0)
				iTotalNoOfKMRunPerDay = (iEndKm-iStartKm);
					
			ArrayList theList             = null;
			theList                       = new ArrayList();
			theList                       . clear();
			theList                       . add(common.parseDate(String.valueOf(iStDate)));
			theList                       . add(SVehicleNo);
			theList                       . add(String.valueOf(iStartKm));
			theList                       . add(String.valueOf(iEndKm));
			theList                       . add(String.valueOf(iTotalNoOfKMRunPerDay));
			theList                       . add(String.valueOf("NADUPALAYAM"));
			theList                       . add("11");
			theList                       . add(String.valueOf(theMap.get("StandardTrip")));
			theList                       . add(String.valueOf(iActualTrip));
			theList                       . add(String.valueOf(iExtraTrip));
			theList                       . add(String.valueOf(dIncentiveAmt));
			theList                       . add(new java.lang.Boolean(true));

			theModel                      . appendRow(new Vector(theList));

			HashMap theMap1 = new HashMap() ;

            theMap1.put("StDate",String.valueOf(iStDate));
			theMap1.put("StartKm",String.valueOf(iStartKm));
			theMap1.put("EndKm",String.valueOf(iEndKm));
			theMap1.put("TotalNoOfKMRunPerDay",String.valueOf(iTotalNoOfKMRunPerDay));
			theMap1.put("StandardTrip",String.valueOf(String.valueOf(theMap.get("StandardTrip"))));
			theMap1.put("ActualTrip",String.valueOf(iActualTrip));
			theMap1.put("ExtraTrip8",String.valueOf(iExtraTrip+dNightShiftIncentiveAmt));
			theMap1.put("Amount8",String.valueOf(dDayShiftIncentiveAmt));
			theMap1.put("ExtraTrip12",String.valueOf(iExtraTrip12));
			theMap1.put("Amount12",String.valueOf(dDayShiftIncentiveAmt12+dNightShiftIncentiveAmt12));
			theMap1.put("IncentiveAmount",String.valueOf(dIncentiveAmt));
					
			ADriverList.add(theMap1);

			int iDayShiftFireTankTrip   = getDayShiftFireTankTrips(iStDate);
//			int iNightShiftFireTankTrip = getNightShiftFireTankTrips(iStDate);

			int iTotalFireTankTrips 	= iDayShiftFireTankTrip ;//+ iNightShiftFireTankTrip ;
			double dFireTankAmt			= iTotalFireTankTrips * 25 ;

			iTotalNoOfKMRunPerDay   	= common.toInt(SFireTankENDKM) - common.toInt(SFireTankSTKM);

			if(iTotalFireTankTrips>0)
			{
				
		
				theList                       . clear();
				theList                       . add(common.parseDate(String.valueOf(iStDate)));
				theList                       . add(SVehicleNo);
				theList                       . add(String.valueOf(SFireTankSTKM));
				theList                       . add(String.valueOf(SFireTankENDKM));
				theList                       . add(String.valueOf(iTotalNoOfKMRunPerDay));
				theList                       . add(String.valueOf("FIRE TANK"));
				theList                       . add("11");
				theList                       . add(String.valueOf(theMap.get("StandardTrip")));
				theList                       . add(String.valueOf(iTotalFireTankTrips));
				theList                       . add(String.valueOf(iTotalFireTankTrips));
				theList                       . add(String.valueOf(dFireTankAmt));
				theList                       . add(new java.lang.Boolean(true));

				theModel                      . appendRow(new Vector(theList));
			}
               
			dTotalAmt = dTotalAmt + dIncentiveAmt + dFireTankAmt;
			iTotalTrip = iTotalTrip + iExtraTrip + iTotalFireTankTrips ;

			j = j + 1 ;
					

			
			iStDate=common.getNextDate(iStDate);
		}
				
		ArrayList theList1	           = null;
        theList1                       = new ArrayList();
        theList1                       . clear();
		theList1                       . add(null);
		theList1                       . add("Total");
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(null);
		theList1                       . add(String.valueOf(iTotalTrip));
		theList1                       . add(String.valueOf(dTotalAmt));
		theList1                       . add(null);
     
        theModel                       . appendRow(new Vector(theList1));
				
		for(int i=0;i<theVector.size();i++)
		{
			WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			
			TOpening.setText(String.valueOf(theClass.dOpening));
			TCarriedOver.setText(String.valueOf(theClass.dCarriedOver));
			TRoundedNet.setText(String.valueOf(theClass.getRoundedNetPay()));
		}	
	}

	public int getNightShiftTrips(int iDate)
	{
		SVehicleNo = "";
 
		int iTotal = 0  ;
			
		for(int i=0;i<theDriverList.size();i++)
		{
			HashMap theMap = (HashMap)theDriverList.get(i);
			String SOutDate = ((String)theMap.get("OutDate"));
			String SOutTime = ((String)theMap.get("OutTime"));
			int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
			
			int iNextDate = common.getNextDate(iDate);
			String STime = SOutTime.substring(9,11);
				
			if((iDate==common.toInt(SOutDate)) && (STime.equals("PM")) && (iShift==2))
			{
				iTotal = iTotal + 1;
				SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
			}
			if((iNextDate==common.toInt(SOutDate)) && (STime.equals("AM")) && (iShift==2) )
			{
				iTotal = iTotal + 1;
			}
		}
		return iTotal;
	}
		
	public int getDayShiftTrips(int iDate)
	{
		int iTotal = 0  ;
		SVehicleNo = "";
			
		for(int i=0;i<theDriverList.size();i++)
		{
			HashMap theMap = (HashMap)theDriverList.get(i);
			String SOutDate = ((String)theMap.get("OutDate"));
			String SOutTime = ((String)theMap.get("OutTime"));
			int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
								
			String STime = SOutTime.substring(9,11);
				
			if((iDate==common.toInt(SOutDate)) &&  (iShift==1))
			{
				iTotal = iTotal + 1;
				SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
			}
		}
		return iTotal;
	}

	public int getDayShiftFireTankTrips(int iDate)
	{
		int iTotal = 0  ;
		SVehicleNo = "";
			//System.out.println("AFireTankDataList.size() :"+AFireTankDataList.size());

		for(int i=0;i<AFireTankDataList.size();i++)
		{
			HashMap theMap  = (HashMap)AFireTankDataList.get(i);

			String SOutDate = ((String)theMap.get("OutDate"));
			String SOutTime	= ((String)theMap.get("OutTime"));
			int    iShift	= common.toInt(String.valueOf(theMap.get("Shift")));
			String SSTKM	= ((String)theMap.get("StartKm"));	
			String SEndKM	= ((String)theMap.get("EndKm"));

			if(iDate==common.toInt(SOutDate))
			{
				iTotal = iTotal + 1;

				SFireTankSTKM  = SSTKM ;
				SFireTankENDKM = SEndKM ;
				SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
			}
		}
		return iTotal;
	}
	
	public int getStartKM(int iDate)
	{
		int iStKm = 0 ;
		SVehicleNo = "";			
		for(int i=0;i<theDriverList.size();i++)
		{
			HashMap theMap = (HashMap)theDriverList.get(i);
			String SOutDate = ((String)theMap.get("OutDate"));
			String SOutTime = ((String)theMap.get("OutTime"));
			int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
			
			int iNextDate = common.getNextDate(iDate);
			String STime = SOutTime.substring(9,11);
				
			if((iDate==common.toInt(SOutDate)) )
			{
				if(iShift==2)
				{
					if((iDate==common.toInt(SOutDate)) && (STime.equals("PM")) )
					{
						iStKm = common.toInt(String.valueOf(theMap.get("EndKm")));
						SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
						break;
					}
				}
				if(iShift==1)
				{
					iStKm = common.toInt(String.valueOf(theMap.get("EndKm")));
					SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
					break;
				}	
			}
		}
		return iStKm;
	}
		
	public int getEndKM(int iDate)
	{
		int iEndKm = 0;
		SVehicleNo = "";
			
		for(int i=0;i<theDriverList.size();i++)
		{
			HashMap theMap = (HashMap)theDriverList.get(i);
			String SOutDate = ((String)theMap.get("OutDate"));
			String SOutTime = ((String)theMap.get("OutTime"));
			int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
				
			int iNextDate = common.getNextDate(iDate);
			String STime = SOutTime.substring(9,11);
			
			if(iShift==1)
			{
				if(iDate==common.toInt(SOutDate))
					iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
					SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
			}
			if(iShift==2)
			{
				if((iDate==common.toInt(SOutDate)) && (STime.equals("PM")) )
				{
					iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
					SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
				}
			}
			if((iNextDate==common.toInt(SOutDate)) )
			{
				if(iShift==2)
				{
					if((iNextDate==common.toInt(SOutDate)) && (STime.equals("AM")) )
					{
						iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
						SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
					}
				}
				if(iShift==1)
				{
					if((common.toInt(SOutTime.substring(0,2))<8) && (STime.equals("AM")) )
					{
						iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
						SVehicleNo = common.parseNull((String)theMap.get("VehicleNo"));
					}
				}	
			}
		}
		return iEndKm;
	}
	
	private void setVectors()
	{
    	theDriverList = new ArrayList();
          
		try
		{
			ORAConnection1 oraConnection    =   ORAConnection1.getORAConnection();
			Connection connection           =   oraConnection.getConnection();
			Statement  theStatement         =   connection.createStatement();
			ResultSet  theResult            =   theStatement.executeQuery(getQS());
               
			while(theResult.next())
            {
				String SOutDate    = theResult.getString(1);
				String SOutTime    = theResult.getString(2);
				String SInDate     = theResult.getString(3);
				String SInTime     = theResult.getString(4);
				int iStartKm       = common.toInt(common.parseNull(theResult.getString(5)));
				int iEndKm         = common.toInt(common.parseNull(theResult.getString(6)));
				String SDriverName = common.parseNull(theResult.getString(7));
				int iEmpCode       = theResult.getInt(8);
				int iShift         = theResult.getInt(9);
				int iID            = theResult.getInt(10);
               	int iStandardTrip  = theResult.getInt(11);
				double dAmount     = theResult.getDouble(12);
				double dTripAmt12  = theResult.getDouble(13);
				
				HashMap theMap = new HashMap();
		
				theMap.put("OutDate",SOutDate);
				theMap.put("OutTime",SOutTime);
				theMap.put("InDate",SInDate);
				theMap.put("InTime",SInTime);
				theMap.put("DriverUserCode",String.valueOf(iEmpCode));
				theMap.put("DriverName",SDriverName);
				theMap.put("Amount",String.valueOf(dAmount));
				theMap.put("TripAmt12",String.valueOf(dTripAmt12));
				theMap.put("StartKm",String.valueOf(iStartKm));
				theMap.put("EndKm",String.valueOf(iEndKm));
				theMap.put("StandardTrip",String.valueOf(iStandardTrip));
				theMap.put("Shift",String.valueOf(iShift));
				theMap.put("ID",String.valueOf(iID));
				theMap.put("VehicleNo",theResult.getString(14));
								
				theDriverList . add(theMap);
			}
			theResult.close();
							
			theResult                = theStatement.executeQuery(getOpeningQS());
			while(theResult.next())
			{
            	int iEmpCode     = theResult.getInt(1);
                double dOpening  = theResult.getDouble(2);

                int iIndex = getIndexOf1(iEmpCode);

                if(iIndex!=-1)
                {
                    WaterLorryIncentiveClass theClass =(WaterLorryIncentiveClass)theVector.get(iIndex);
                	theClass.setOpening(dOpening);
                }
			}
            theResult.close();
            theStatement.close();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			ex.printStackTrace();
		}
	}
	private void setFireTankData()
	{
		AFireTankDataList = new ArrayList();

		String SStDate		= TStDate.toNormal();
		String SEnDate  	= TEnDate.toNormal();
		
		int iNextDate     	= common.getNextDate(common.toInt(SEnDate));
		int iIndex 			= JCDriversName.getSelectedIndex() ;
			
		String SEmpCode 	= (String)ADriverEmpCode.get(iIndex);

		String QS = " Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,DriverUserCode, '2' as Shift,ID,6 as StandardTrips,25 as Amount,0 as TripAmt12,Vehicles.VehicleNo from Vehicles  "+
					" where  DriverUserCode ="+SEmpCode+" and KINO = '0' and Purpose='TO WATER' and PLACE='FIRE TANK'   "+
					" and ( ( (OutDate >="+SStDate+" and outDate<="+SEnDate+") and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='20.00'  "+
					" and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'24.00')) or  ( outDate>"+SStDate+" and OutDate <= "+SEnDate+"  and (to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')>='00.00'  "+
					" and to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')<'08.00'))  )  "+
					" Union All  "+
					" Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,DriverUserCode, '1' as Shift,ID,6 as StandardTrips,25 Amount,0 as TripAmt12,Vehicles.VehicleNo from Vehicles  "+
					" where  DriverUserCode ="+SEmpCode+" and KINO = '0'  and Purpose='TO WATER'  and PLACE='FIRE TANK'  and ((OutDate >="+SStDate+" and outDate<="+SEnDate+") "+
					" and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='08.00'  and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'20.00'))  "+
					" Order by OutDate,ENDKM ";

System.out.println("Fire Tank QS :"+QS);
          
		try
		{
			ORAConnection1 oraConnection    =   ORAConnection1.getORAConnection();
			Connection connection           =   oraConnection.getConnection();
			Statement  theStatement         =   connection.createStatement();
			ResultSet  theResult            =   theStatement.executeQuery(QS);
               
			while(theResult.next())
            {
				HashMap theMap = new HashMap();
		
				theMap.put("OutDate",common.parseNull(theResult.getString(1)));
				theMap.put("OutTime",common.parseNull(theResult.getString(2)));
				theMap.put("InDate",common.parseNull(theResult.getString(3)));
				theMap.put("InTime",common.parseNull(theResult.getString(4)));
				theMap.put("StartKm",common.parseNull(theResult.getString(5)));
				theMap.put("EndKm",common.parseNull(theResult.getString(6)));
				theMap.put("DriverName",common.parseNull(theResult.getString(7)));
				theMap.put("DriverUserCode",common.parseNull(theResult.getString(8)));
				theMap.put("Shift",common.parseNull(theResult.getString(9)));
				theMap.put("ID",common.parseNull(theResult.getString(10)));
				theMap.put("StandardTrip",common.parseNull(theResult.getString(11)));
				theMap.put("Amount",common.parseNull(theResult.getString(12)));
				theMap.put("TripAmt12",common.parseNull(theResult.getString(13)));
				theMap.put("VehicleNo",common.parseNull(theResult.getString(14)));
												
				AFireTankDataList . add(theMap);
			}
			theResult.close();
		    theStatement.close();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			ex.printStackTrace();
		}
	}

	private String getOpeningQS()
	{
		StringBuffer sb = new StringBuffer();

		sb.append(" Select EmpCode,(-1)*CarriedOver from WaterLorryIncentive");
		sb.append(" where IncentiveMonth =(Select  Max(IncentiveMonth) from WaterLorryIncentive)  ");
     //     System.out.println("Carried OVer QS:"+sb.toString()); 
         
		return sb.toString();
	}

	private int getIndexOf(int iEmpCode) //,String SDate,int iShift
	{
		int iIndex               = -1;
          
		for(int i=0; i<theVector.size(); i++)
		{
			WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			    //if((iEmpCode==theClass.iEmpCode) && (SDate.equals(theClass.SOutDate))   && (iShift==theClass.iShift)) 
			if(iEmpCode==theClass.iEmpCode)
			{
    			iIndex         = i;
                break;
			}
		}
		return iIndex;
	}
	
	private int getIndexOf1(int iEmpCode)
	{
		int iIndex               = -1;
          
		for(int i=0; i<theVector.size(); i++)
		{
			WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			if(iEmpCode==theClass.iEmpCode)
			{
            	iIndex         = i;
                break;
			}
		}
		return iIndex;
	}
	
	private  String getQS()
	{
		String QS = "";
						
		String SStDate       = TStDate.toNormal();
		String SEnDate       = TEnDate.toNormal();
		
		int iNextDate     = common.getNextDate(common.toInt(SEnDate));
		int iIndex = JCDriversName.getSelectedIndex() ;
			
		String SEmpCode = (String)ADriverEmpCode.get(iIndex);
		
		QS = " Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,DriverUserCode, '2' as Shift,ID,StandardTrips,Amount,TripAmt12,Vehicles.VehicleNo from Vehicles "+ 
			 " Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo and "+SStDate+" between WithEffectFrom and WithEffectTo "+
			 " and "+SEnDate+" between WithEffectFrom and WithEffectTo"+
			 " where  DriverUserCode ="+SEmpCode+" and KINO = '0' and Purpose='TO WATER' and PLACE!='FIRE TANK'  "+
			 " and ( ( (OutDate >="+SStDate+" and outDate<="+SEnDate+") and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='20.00' "+
			 " and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'24.00')) or "+
			 " ( outDate>"+SStDate+" and OutDate <= "+iNextDate+"  and (to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')>='00.00' "+
			 " and to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')<'08.00'))  ) "+
			 " Union All "+
			 " Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,DriverUserCode, '1' as Shift,ID,StandardTrips,Amount,TripAmt12,Vehicles.VehicleNo from Vehicles "+
			 " Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo and "+SStDate+" between WithEffectFrom and WithEffectTo "+
			 " and "+SEnDate+" between WithEffectFrom and WithEffectTo"+
			 " where  DriverUserCode ="+SEmpCode+" and KINO = '0'  and Purpose='TO WATER'  and PLACE!='FIRE TANK' "+
			 " and ((OutDate >="+SStDate+" and outDate<="+SEnDate+") and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='08.00' "+
			 " and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'20.00')) "+
			 " Order by ENDKM ";
			
			System.out.println("QS:"+QS);
			return QS;
	}
      
    private boolean CheckAnyOneSelected()
    {
        for(int i=0;i<theModel.getRowCount();i++)
        {
            java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,Click);

            if(bValue.booleanValue())
            {
                return true;
            }
        }
        JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Save","Information",JOptionPane.ERROR_MESSAGE);
        return false;
    }
            
    private int getIsAlreadyExist(int iEmpCode,String SMonth)
	{
        int iCount                    = 0;
          
        ORAConnection1 oraConnection   =   ORAConnection1.getORAConnection();
        Connection theConnection      =   oraConnection.getConnection();
          
        try
        {
			PreparedStatement thePS  = theConnection.prepareStatement("Select Count(*) from WaterLorryIncentive Where  EmpCode = "+iEmpCode+"  and IncentiveMonth="+SMonth+"  ");
            ResultSet rs             = thePS.executeQuery();
			 
            if(rs.next())
            {
                iCount              = rs.getInt(1);
            }
            rs                       . close();
            thePS                    . close();
        }                    
        catch(Exception ex)
		{
			ex.printStackTrace();
        }
    	return iCount;       
    }
 
    private void SaveDetails()
    {
        Connection theConnection           = null;
              
        try
        {
            ORAConnection1 oraConnection     =   ORAConnection1.getORAConnection();
            theConnection                   =   oraConnection.getConnection();
               
            theConnection                 . setAutoCommit(false);
            int iExecuteCount=0,iExCount=0;

			int iExtraTrip=0;
			double dAmount=0.0;
									
			int iIndex = JCDriversName.getSelectedIndex() ;
				
			int iEmpCode    = common.toInt((String)ADriverEmpCode.get(iIndex));
			String SEmpName = (String)JCDriversName.getSelectedItem() ;	
            for(int i=0; i<theModel.getRowCount()-1; i++)
            {
                iExtraTrip  = iExtraTrip + common.toInt(String.valueOf(theModel.getValueAt(i,ExtraTrip)));
				dAmount     = dAmount+ common.toDouble(String.valueOf(theModel.getValueAt(i,8)));
			}
			int iFromDate            = common.toInt(TStDate.toNormal());
			int iToDate              = common.toInt(TEnDate.toNormal());
                     
			String SMonth = (TStDate.toNormal()).substring(0,6);                     
			/*int iOpening     = common.toInt(TOpening.getText() );
			int iCarriedOver = common.toInt(TCarriedOver.getText());
			double dRoundedNet = common.toDouble(TRoundedNet.getText());*/
						 
        	int iIsAlreadyExist = getIsAlreadyExist(iEmpCode,SMonth);
			if(iIsAlreadyExist==0)
			{
				PreparedStatement ps     = null;
                ps                       = theConnection.prepareStatement(getSaveQS());
                             
                ps    .    setInt(1,iEmpCode);
				ps    .    setString(2,SEmpName);
                ps    .    setInt(3,iExtraTrip); 
                ps    .    setDouble(4,dAmount);
                ps    .    setString(5,SMonth);
                ps    .    setString(6,SAuthSysName);
                ps    .    setInt(7,0);
				ps    .    setInt(8,0);
                ps    .    setDouble(9,0);
				ps    .    setInt(10,iFlowCode);
                                                          
                iExCount =  ps               . executeUpdate();
                             
                ps         . close();
                ps         = null;
                iExecuteCount     = iExecuteCount+iExCount;
			}
                                                    
            if(iExecuteCount>0)
            {          
            	JOptionPane.showMessageDialog(null, "Data Saved Succesfully");
			}
            else
            {
            	JOptionPane.showMessageDialog(null,"Data Already Processed");     
            }
            theConnection                 . commit();
            theConnection          = null;
		}
        catch (Exception ex)
        {
        	System.out.println(ex);
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Problem in  Saving Data", "Error", 
            JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
            try
            {
            	theConnection      . rollback();   
            } 
			catch(Exception e){}
		}
	}

    private String getSaveQS()
    {
		StringBuffer SB = null;
    
		SB = new StringBuffer();
          
        SB.append(" Insert into WaterLorryIncentive(ID,EmpCode,DriverName,ExtraTrips,Amount,IncentiveMonth,EntryDate,EntryDateAndTime,EntrySystemName,Opening,CarriedOver,RoundedNet,FlowCode) ");
        SB.append(" values(WaterLorryIncentive_Seq.nextVal,?,?,?,?,?,to_Char(SysDate,'DD.MM.YYYY'),to_Char(SysDate,'DD.MM.YYYY HH24:MI:SS'),?,?,?,?,?) ");
         // System.out.println("QS:"+SB.toString());
        return SB.toString();
    }
	
    private void refresh()
    {
        try
        {
            theVector.removeAllElements();
            theModel.setNumRows(0);
        }
        catch(Exception e)
        {
			System.out.println(e);
            e.printStackTrace();
        }
    }
     
    private void removeFrame()
    {
        try
        {
            Layer.remove(this);
            Layer.updateUI();
        }
        catch(Exception ex){}
    }
	public void CreatePDF()
	{
		try
		{
			String SFile = common.getPrintPath()+"WaterLorryPDF.pdf";
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFile));
            document.setPageSize(PageSize.A4.rotate());
			document.setMargins(15, 10, 15, 15);
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            table.setHeaderRows(5);
			SetPDF();       
			document.close();
			JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile+" ","Info",JOptionPane.INFORMATION_MESSAGE);   
		
				File pdfFile = new File(SFile);

				if (Desktop.isDesktopSupported()) 
				{
					Desktop.getDesktop().open(pdfFile);
				} 
				else 
				{
					System.out.println("Awt Desktop is not supported!");
				}
 
        }
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}
	private void SetPDF()
	{
		String SStDate       = TStDate.toNormal();
		String SEnDate       = TEnDate.toNormal();
		
		String SDriverName	= (String)JCDriversName.getSelectedItem() ;

		SDriverName = SDriverName.substring(0,SDriverName.length()-8);

		AddCellIntoTable("MONTHLY WATER LORRY EXTRA TRIP INCENTIVE FOR "+SDriverName+" ", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,0,0,0,0, bigBold);
		AddCellIntoTable("FROM "+common.parseDate(SStDate)+" TO "+common.parseDate(SEnDate)+"", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,0,0,0,0, bigNormal);
        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,0,0,0,0, bigBold);
        AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,0,0,0,0, bigBold);

		AddCellIntoTable("SNo",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Date",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Opening KM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Closing KM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Total NoOf KM",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Place",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Std Trips/Day",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Eligible Trips/Day",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Actual Trips/Day",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Extra Trips",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);
		AddCellIntoTable("Incentive Amt",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,40f,4,1,8,2,mediumBold);

		double	dTotalAmt	= 0.0;
		int		iTotalTrip 	= 0 ;

		for(int i=0;i<(theModel.getRowCount()-1);i++)
		{
			AddCellIntoTable(String.valueOf(i+1),table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,0),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,2),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,3),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,4),table,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,5),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,6),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,7),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,8),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,9),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);
			AddCellIntoTable((String)theModel.getValueAt(i,10),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,smallNormal);

			dTotalAmt = dTotalAmt + common.toDouble((String)theModel.getValueAt(i,10));
			iTotalTrip = iTotalTrip + common.toInt((String)theModel.getValueAt(i,9));
		}
		AddCellIntoTable("Total",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable("",table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable(String.valueOf(iTotalTrip),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		AddCellIntoTable(String.valueOf(dTotalAmt),table,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,1,30f,4,1,8,2,mediumBold);
		
		try
		{
			document.add(table);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex);
		}
	}
	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
}
