package client.WaterLorryIncentive;

import util.*;
import java.sql.*;
import java.io.*;
import java.util.*;

class ORAConnection1
{
	static ORAConnection1 connect = null;
	Connection theConnection = null;

	private ORAConnection1()
	{
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
               theConnection = 
                   DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");   

               
		}
		catch(Exception e)
		{
			System.out.println("ORAConnection : "+e);
		}
	}
     private ORAConnection1(String SPropertyFile)
	{
          ResourceBundle res = ResourceBundle.getBundle(SPropertyFile);

          String SDsn        = res.getString("dsn");
          String SUserName   = res.getString("UserName");
          String SPassword   = res.getString("Password");

		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
               theConnection = 
                   DriverManager.getConnection("jdbc:oracle:thin:@"+SDsn,SUserName,SPassword);   

               
		}
		catch(Exception e)
		{
			System.out.println("ORAConnection : "+e);
		}
	}


	synchronized public static ORAConnection1 getORAConnection()
	{
		if (connect == null)
		{
			connect = new ORAConnection1();
		}
		return connect;
	}
    synchronized public static ORAConnection1 getORAConnection(String SPropertyFile)
	{
		if (connect == null)
		{
               connect = new ORAConnection1(SPropertyFile);
		}
		return connect;
	}


	public Connection getConnection()
	{
		return theConnection;
	}

}
