package client.WaterLorryIncentive;

import java.sql.*;
import java.util.*;
import java.util.ArrayList;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import util.*;

    public class WaterLorryIncentiveFrame extends JInternalFrame
    {
      
        protected    JLayeredPane Layer;
          
        JPanel       TopPanel,TitlePanel,FilterPanel,MiddlePanel,BottomPanel,AmountPanel,ControlPanel,AttachmentPanel;
                         
        JButton      BSave,BExit,BApply,BPrint,BAttachment;
          
        MyLabel      LStDate,LEnDate,LOpening,LCarriedOver,LRoundedNet;
		
		JTextField   TOpening,TCarriedOver,TRoundedNet;
          
        AttDateField TStDate,TEnDate;
                              
        MyComboBox   JCDriversName;
                 
        private      JTable           theTable;
          
        private      WaterLorryIncentiveModel    theModel;
                                                         
        Vector       theVector;
                              
        private      ArrayList  ADriverList,ADriverName,ADriverEmpCode;
          
        int iEmpCode,iRow,iCol;
		
		String SAuthSysName;
		
		int iUSerCode,iFlowCode;
		
		ArrayList theDriverList;
          
        Common common  = new Common();
		
		WaterLorryIncentiveClass theClass;
     		 
		int Date=0;
		int OpeningKm=1;
		int ClosingKm=2;
		int TotalNoOfKmPerDay=3;
		int StdTripsPerDayOnStdKms=4;
		int ActualTripsPerDayOnActualKms=5;
		int ActualWaterLoadTrips=6;
		int CeilingWaterLoadTrips=7;
		int ExtraTrip=8;
		int IncentiveRatePerExtraTrip=9;
		int TotalIncentive=10;
		int Click=11;
		
		public WaterLorryIncentiveFrame(JLayeredPane Layer)
		{
			try
			{
				this.Layer   = Layer;
                setDriversName();
				createComponents();
				setLayouts();
				addComponents();
				addListeners();
				
				try
				{
					SAuthSysName=InetAddress.getLocalHost().getHostName();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			catch(Exception ex)
			{
               ex.printStackTrace();
               System.out.println(ex);
			}
		}  

		public void createComponents()
		{
			TopPanel            = new JPanel();
			TitlePanel          = new JPanel();
			FilterPanel         = new JPanel();
			MiddlePanel         = new JPanel();
			BottomPanel         = new JPanel();
			ControlPanel        = new JPanel();
			AmountPanel         = new JPanel();
			AttachmentPanel     = new JPanel();
             
			BSave               = new JButton("Save");
			BExit               = new JButton("Exit");
			BApply              = new JButton("Apply");
			BAttachment         = new JButton("Attachment");
			BPrint              = new JButton("Print");

			LStDate             = new MyLabel("Start Date");
			LEnDate             = new MyLabel("End Date");
			LOpening            = new MyLabel("Opening");
			LCarriedOver        = new MyLabel("Carried OVer");
			LRoundedNet         = new MyLabel("RoundedNet");
			
			TOpening            = new JTextField(15);
			TOpening            . setEditable(false);
			TCarriedOver        = new JTextField(15);
			TCarriedOver        . setEditable(false);
			TRoundedNet         = new JTextField(15);
			TRoundedNet         . setEditable(false);
			                   
			TStDate             = new AttDateField();
			TEnDate             = new AttDateField();
            JCDriversName       = new MyComboBox(new Vector(ADriverName));
          
            theModel            = new WaterLorryIncentiveModel();
			theTable            = new JTable(theModel);
                 
			for(int i=0;i<theModel.ColumnName.length;i++)
			{
				(theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
			}
		}
     
		public void setLayouts()
		{
			setTitle("WATER LORRY INCENTIVE FRAME");
			setMaximizable(true);
			setClosable(true);
			setResizable(true);
			setIconifiable(true);
			setSize(950,600);
                    
            FilterPanel.setLayout(new GridLayout (1,7));
			TopPanel. setLayout(new BorderLayout());
			MiddlePanel.setLayout(new BorderLayout ());
			BottomPanel.setLayout(new GridLayout (1,3));
			ControlPanel.setLayout(new FlowLayout());
			AttachmentPanel.setLayout(new FlowLayout());
			AmountPanel.setLayout(new GridLayout (3,2));
               
		}

		public void addComponents()
		{
			TitlePanel.add(new MyLabel("WATER LORRY EXTRA TRIP INCENTIVE "));
			
            FilterPanel.setBorder(new TitledBorder("Filter"));
			FilterPanel.add(LStDate);
			FilterPanel.add(TStDate);
			FilterPanel.add(LEnDate);
			FilterPanel.add(TEnDate);
			FilterPanel.add(new MyLabel("Driver Name"));
			FilterPanel.add(JCDriversName);
			FilterPanel.add(BApply);
             
			TopPanel.add(TitlePanel,BorderLayout.NORTH);
			TopPanel.add(FilterPanel,BorderLayout.SOUTH);
			
			MiddlePanel.setBorder(new TitledBorder("Details")); 
            MiddlePanel.add(new JScrollPane(theTable));
                
				
			ControlPanel.setBorder(new TitledBorder("Authenticate"));
			ControlPanel.add(BSave);
			ControlPanel.add(BExit);
			ControlPanel.add(BPrint);
			
			AmountPanel.setBorder(new TitledBorder("Amount Data"));
			AmountPanel.add(LOpening);
			AmountPanel.add(TOpening);
			AmountPanel.add(LCarriedOver);
			AmountPanel.add(TCarriedOver);
			AmountPanel.add(LRoundedNet);
			AmountPanel.add(TRoundedNet);
			
			AttachmentPanel.setBorder(new TitledBorder("Approval"));
			AttachmentPanel.add(BAttachment);
			
			BottomPanel.setBorder(new TitledBorder("Controls"));
			BottomPanel.add(ControlPanel);
			BottomPanel.add(AmountPanel);
			BottomPanel.add(AttachmentPanel);
						
			getContentPane().add("North",TopPanel);
			getContentPane().add("Center",MiddlePanel);
			getContentPane().add("South",BottomPanel);
		}

		public void addListeners()
		{
			MyListener  Listener = new MyListener();
			BSave .addActionListener(Listener);
			BExit.addActionListener(Listener);
			BApply.addActionListener(Listener);
			BPrint. addActionListener(Listener);
		
		} 
      
		private void setDriversName()
		{
            ADriverName     = null;
			ADriverEmpCode        = null;
          
			ADriverName     = new ArrayList();
			ADriverEmpCode        = new ArrayList();
         
		    ADriverName       .clear();       
			ADriverEmpCode        .clear();
                                        
			try
			{
                   
				ORAConnection oraConnection     =   ORAConnection.getORAConnection();
				Connection connection           =   oraConnection.getConnection();
				Statement  theStatement         =   connection.createStatement();
				ResultSet  theResult            =   theStatement.executeQuery("Select EmpCode,EmpName from Staff where DesignationCode=278 order by 2 ");
				while(theResult.next())
				{
                    ADriverEmpCode.add(theResult.getString(1));
                    ADriverName.add(theResult.getString(2));
                }
            }
			catch(Exception ex)
			{
				System.out.println(ex);
				ex.printStackTrace();
			}
                   
		}

		private class MyListener implements ActionListener, ItemListener
		{
		    public void actionPerformed(ActionEvent ae)
			{
				try
				{
					if(ae.getSource()==BApply)
					{
						setTableDetails();     
					}
		
					if(ae.getSource()==BPrint) 
					{
						try
						{
							String SFile = "D:\\WaterLorryIncentive.pdf";
							
							String SFromDate       = TStDate.toNormal();
							String SToDate       = TEnDate.toNormal();
							String SDriverName   = (String)JCDriversName.getSelectedItem() ;
						
							WaterLorryIncentivePDF createPDF = new WaterLorryIncentivePDF(SFile,SFromDate,SToDate,SDriverName,theVector,theDriverList);
																					
						}
						catch(Exception ex)
						{
						   System.out.println("PrnCreation"+ex);
						   ex.printStackTrace();
						}
					}
					if (ae.getSource() == BSave)
					{
						if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Information", JOptionPane.YES_NO_OPTION) == 0)
						{
							/*  if(CheckAnyOneSelected())
							  {
								if(IsAlreadyExist())
								{*/
									SaveDetails();
							/*	}
								else
								{
									JOptionPane.showMessageDialog(null,"Data Already Processes For this Month ","Information",JOptionPane.ERROR_MESSAGE);
								} 
							}*/
						}
							  
					} 
					if (ae.getSource() == BExit)
					{
						theTable    = null;
						theModel    = null;
						theVector   = null;
						ADriverList  = null;
						
						
						removeFrame();
						Layer.repaint();
						Layer.updateUI();
					}
				}
				catch(Exception e)
				{
				   System.out.println(e);
				   e.printStackTrace();
				}
			}
			public void itemStateChanged(ItemEvent ie)
			{
				try
				{
					theModel . setNumRows(0);                                                                       
				}
				catch(Exception e)
				{
					System.out.println(e);
					e.printStackTrace();
				}
			}
		}
           
        private void setTableDetails()
		{
			theModel                           . setNumRows(0);
			theVector = new Vector();
			setVectors();
			int iSlNo                          = 1;
          
		    double dTotalAmt = 0;
							
					
				int iStDate       = common.toInt(TStDate.toNormal());
				int iEnDate       = common.toInt(TEnDate.toNormal());	
				int iNextDate     = common.getNextDate(iEnDate);
				//System.out.println("iStDate:"+iStDate);
				//System.out.println("iNextDate:"+iNextDate);
				int j = 0 ;						
				for(int i=iStDate;i<=iNextDate;i++)
				{
					
					int iDayShiftExtraTrip = 0,iNightShiftExtraTrip =0;
					double dIncentiveAmt = 0.0 ;
					
					int iDayShiftTrip   = getDayShiftTrips(iStDate);
					int iNightShiftTrip = getNightShiftTrips(iStDate);
										
					if(iStDate==iNextDate)
						break ;		
					
										
					if((iDayShiftTrip>0) && (iDayShiftTrip>10))
					{
						iDayShiftExtraTrip = (iDayShiftTrip-10);
					}
					
					if(iNightShiftTrip>0 && iNightShiftTrip>10)
					{
						iNightShiftExtraTrip = (iNightShiftTrip-10);
					}
					
					int iActualTrip = (iDayShiftTrip + iNightShiftTrip);
					int iExtraTrip  = (iDayShiftExtraTrip + iNightShiftExtraTrip);
					
					
			//		WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theDriverList.get(j);
					//System.out.println("theDriverList.size():"+theDriverList.size());
					HashMap theMap = (HashMap)theDriverList.get(j);
					
										
					if(iExtraTrip>0)
					dIncentiveAmt     = ( iExtraTrip * common.toDouble(String.valueOf(theMap.get("Amount")))  );
					else
					{
					dIncentiveAmt    = 0 ;
					iExtraTrip       = 0 ;
					}
					
					int iEndKm   = 0;
					int iStartKm = getStartKM(iStDate);
					if(iStartKm>0)
					  iEndKm   = getEndKM(iStDate);
					
					int iTotalNoOfKMRunPerDay = 0;	
					if(iEndKm>0)
					iTotalNoOfKMRunPerDay = (iEndKm-iStartKm);
					
					ArrayList theList             = null;
					theList                       = new ArrayList();
					theList                       . clear();
					theList                       . add(common.parseDate(String.valueOf(iStDate)));
					theList                       . add(String.valueOf(iStartKm));
					theList                       . add(String.valueOf(iEndKm));
					theList                       . add(String.valueOf(iTotalNoOfKMRunPerDay));
					theList                       . add(String.valueOf(theMap.get("StandardTrip")));
					theList                       . add(String.valueOf(iActualTrip));
					theList                       . add(String.valueOf(iActualTrip));
					theList                       . add(String.valueOf(theMap.get("StandardTrip")));
					theList                       . add(String.valueOf(iExtraTrip));
					theList                       . add(String.valueOf(theMap.get("Amount")));
					theList                       . add(String.valueOf(dIncentiveAmt));
					theList                       . add(new java.lang.Boolean(true));
					theModel                      . appendRow(new Vector(theList));
				
               
					dTotalAmt = dTotalAmt + dIncentiveAmt;
					
					j = j + 1 ;
					
					iStDate=common.getNextDate(iStDate);
					
				}
				

				ArrayList theList1	           = null;
                theList1                       = new ArrayList();
                theList1                       . clear();
				theList1                       . clear();
				theList1                       . add(null);
				theList1                       . add("Total");
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(null);
				theList1                       . add(String.valueOf(dTotalAmt));
				theList1                       . add(null);
             
                theModel                       . appendRow(new Vector(theList1));
				
				for(int i=0;i<theVector.size();i++)
				{
				
					WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
					
					TOpening.setText(String.valueOf(theClass.dOpening));
					TCarriedOver.setText(String.valueOf(theClass.dCarriedOver));
					TRoundedNet.setText(String.valueOf(theClass.getRoundedNetPay()));
				}	
				
		    
		}
		public int getNightShiftTrips(int iDate)
		{
			int iTotal = 0  ;
			
			for(int i=0;i<theDriverList.size();i++)
			{
				HashMap theMap = (HashMap)theDriverList.get(i);
				String SOutDate = ((String)theMap.get("OutDate"));
				String SOutTime = ((String)theMap.get("OutTime"));
				int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
				
				int iNextDate = common.getNextDate(iDate);
				String STime = SOutTime.substring(9,11);
				
				if((iDate==common.toInt(SOutDate)) && (STime.equals("PM")) && (iShift==2))
				{
					iTotal = iTotal + 1;
				
				}
				if((iNextDate==common.toInt(SOutDate)) && (STime.equals("AM")) && (iShift==2) )
				{
					iTotal = iTotal + 1;
				}
			}
			return iTotal;
		}
		
		public int getDayShiftTrips(int iDate)
		{
			int iTotal = 0  ;
			
			for(int i=0;i<theDriverList.size();i++)
			{
				HashMap theMap = (HashMap)theDriverList.get(i);
				String SOutDate = ((String)theMap.get("OutDate"));
				String SOutTime = ((String)theMap.get("OutTime"));
				int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
								
				String STime = SOutTime.substring(9,11);
				
				if((iDate==common.toInt(SOutDate)) &&  (iShift==1))
				{
					iTotal = iTotal + 1;
				
				}
				
			}
			return iTotal;
		}
		public int getStartKM(int iDate)
		{
			int iStKm = 0 ;
					
			for(int i=0;i<theDriverList.size();i++)
			{
				HashMap theMap = (HashMap)theDriverList.get(i);
				String SOutDate = ((String)theMap.get("OutDate"));
				String SOutTime = ((String)theMap.get("OutTime"));
				int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
				
				int iNextDate = common.getNextDate(iDate);
				String STime = SOutTime.substring(9,11);
				
				if((iDate==common.toInt(SOutDate)) )
				{
					if(iShift==2)
					{
						if((iDate==common.toInt(SOutDate)) && (STime.equals("PM")) )
						{
							iStKm = common.toInt(String.valueOf(theMap.get("EndKm")));
							break;
						}
					}
					if(iShift==1)
					{
						iStKm = common.toInt(String.valueOf(theMap.get("EndKm")));
						break;
					}	
					
				}
							
					
			}
			return iStKm;
		}
		
		public int getEndKM(int iDate)
		{
			int iEndKm = 0;
					
			for(int i=0;i<theDriverList.size();i++)
			{
				HashMap theMap = (HashMap)theDriverList.get(i);
				String SOutDate = ((String)theMap.get("OutDate"));
				String SOutTime = ((String)theMap.get("OutTime"));
				int    iShift    = common.toInt(String.valueOf(theMap.get("Shift")));
				
				int iNextDate = common.getNextDate(iDate);
				String STime = SOutTime.substring(9,11);
				
				if(iShift==1)
				{
					if(iDate==common.toInt(SOutDate))
						iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
				}
				if((iNextDate==common.toInt(SOutDate)) )
				{
					if(iShift==2)
					{
						
						if((iNextDate==common.toInt(SOutDate)) && (STime.equals("AM")) )
						{
							iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
						}
					}
					if(iShift==1)
					{
						iEndKm = common.toInt(String.valueOf(theMap.get("EndKm")));
					}	
				}
								
			}
			return iEndKm;
		}
		private void setVectors()
		{
     
			theDriverList = new ArrayList();
          
			try
			{
				ORAConnection1 oraConnection    =   ORAConnection1.getORAConnection();
				Connection connection           =   oraConnection.getConnection();
				Statement  theStatement         =   connection.createStatement();
				ResultSet  theResult            =   theStatement.executeQuery(getQS());
               
				while(theResult.next())
                {
					String SOutDate    = theResult.getString(1);
					String SOutTime    = theResult.getString(2);
					String SInDate     = theResult.getString(3);
					String SInTime     = theResult.getString(4);
					int iStartKm       = common.toInt(common.parseNull(theResult.getString(5)));
					int iEndKm         = common.toInt(common.parseNull(theResult.getString(6)));
					String SDriverName = common.parseNull(theResult.getString(7));
					int iStandardTrip  = theResult.getInt(8);
					double dAmount     = theResult.getDouble(9);
					int iEmpCode       = theResult.getInt(10);
					int iShift         = theResult.getInt(11);
					int iID            = theResult.getInt(12);
					
					HashMap theMap = new HashMap();
		
					theMap.put("OutDate",SOutDate);
					theMap.put("OutTime",SOutTime);
					theMap.put("InDate",SInDate);
					theMap.put("InTime",SInTime);
					theMap.put("DriverUserCode",String.valueOf(iEmpCode));
					theMap.put("DriverName",SDriverName);
					theMap.put("Amount",String.valueOf(dAmount));
					theMap.put("StartKm",String.valueOf(iStartKm));
					theMap.put("EndKm",String.valueOf(iEndKm));
					theMap.put("StandardTrip",String.valueOf(iStandardTrip));
					theMap.put("Shift",String.valueOf(iShift));
					theMap.put("ID",String.valueOf(iID));
								
					theDriverList . add(theMap);
				}
				
				theResult.close();
								
				theResult                = theStatement.executeQuery(getOpeningQS());
				while(theResult.next())
				{
                    int iEmpCode     = theResult.getInt(1);
                    double dOpening  = theResult.getDouble(2);

                    int iIndex = getIndexOf1(iEmpCode);

                    if(iIndex!=-1)
                    {
                         WaterLorryIncentiveClass theClass =(WaterLorryIncentiveClass)theVector.get(iIndex);
                         theClass.setOpening(dOpening);
                    }
                                      
                    
               }
               theResult.close();
               theStatement.close();
               
			}
			catch(Exception ex)
			{
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		private String getOpeningQS()
		{
			StringBuffer sb = new StringBuffer();

			sb.append(" Select EmpCode,(-1)*CarriedOver from WaterLorryIncentive");
			sb.append(" where IncentiveMonth =(Select  Max(IncentiveMonth) from WaterLorryIncentive)  ");
     //     System.out.println("Carried OVer QS:"+sb.toString()); 
         
			return sb.toString();
		}
		private int getIndexOf(int iEmpCode) //,String SDate,int iShift
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
			    //if((iEmpCode==theClass.iEmpCode) && (SDate.equals(theClass.SOutDate))   && (iShift==theClass.iShift)) 
				if(iEmpCode==theClass.iEmpCode)
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private int getIndexOf1(int iEmpCode)
		{
			int iIndex               = -1;
          
			for(int i=0; i<theVector.size(); i++)
			{
				WaterLorryIncentiveClass theClass = (WaterLorryIncentiveClass)theVector.elementAt(i);
				if(iEmpCode==theClass.iEmpCode)
				{
                    iIndex         = i;
                    break;
				}
			}
			return iIndex;
		}
		private  String getQS()
		{
			String QS = "";
						
			String SStDate       = TStDate.toNormal();
			String SEnDate       = TEnDate.toNormal();
			
			int iNextDate     = common.getNextDate(common.toInt(SEnDate));
			
			int iIndex = JCDriversName.getSelectedIndex() ;
			
			String SEmpCode = (String)ADriverEmpCode.get(iIndex);
			
			QS = " Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,StandardTrips,Amount,DriverUserCode, '2' as Shift,ID from Vehicles "+
				" Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo "+
				" where  DriverUserCode ="+SEmpCode+" and KINO = 0 "+
				" and ( ( (OutDate >="+SStDate+" and outDate<="+SEnDate+") and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='20.00' "+
				" and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'24.00')) or "+
				" ( outDate>"+SStDate+" and OutDate <= "+iNextDate+"  and (to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')>='00.00' "+
				" and to_char( to_date(OutTime, 'hh:mi:ss AM'), 'hh24:mi')<'08.00'))  ) "+
				" Union All "+
				" Select OutDate,OutTime,InDate,InTime,STKM,ENDKM,DriverName,StandardTrips,Amount,DriverUserCode, '1' as Shift,ID from Vehicles "+
				" Inner join WaterLorryMaster on WaterLorryMaster.LorryNo = Vehicles.VehicleNo "+
				" where  DriverUserCode ="+SEmpCode+" and KINO = 0  "+
				" and ((OutDate >="+SStDate+" and outDate<="+SEnDate+") and (to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')>='08.00' "+
				" and to_char( to_date(OutTime, 'hh:mi:ss PM'), 'hh24:mi')<'20.00')) "+
				" Order by ENDKM " ;
			
			System.out.println("QS:"+QS);
			return QS;
		}
      
    private boolean CheckAnyOneSelected()
    {
        for(int i=0;i<theModel.getRowCount();i++)
        {
            java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,Click);

            if(bValue.booleanValue())
            {
                return true;
            }
        }
        JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Save","Information",JOptionPane.ERROR_MESSAGE);
        return false;
    }
            
    private int getIsAlreadyExist(int iEmpCode,String SMonth)
	{
        int iCount                    = 0;
          
        ORAConnection1 oraConnection   =   ORAConnection1.getORAConnection();
        Connection theConnection      =   oraConnection.getConnection();
          
        try
        {
			PreparedStatement thePS  = theConnection.prepareStatement("Select Count(*) from WaterLorryIncentive Where  EmpCode = "+iEmpCode+"  and IncentiveMonth="+SMonth+"  ");
            ResultSet rs             = thePS.executeQuery();
			 
            if(rs.next())
            {
                iCount              = rs.getInt(1);
            }
            rs                       . close();
            thePS                    . close();
        }                    
        catch(Exception ex)
		{
			ex.printStackTrace();
        }
        
		return iCount;       
    }
 
    private void SaveDetails()
    {
        Connection theConnection           = null;
              
        try
        {
            ORAConnection1 oraConnection     =   ORAConnection1.getORAConnection();
            theConnection                   =   oraConnection.getConnection();
               
            theConnection                 . setAutoCommit(false);
            int iExecuteCount=0,iExCount=0;

			int iExtraTrip=0;
			double dAmount=0.0;
									
			int iIndex = JCDriversName.getSelectedIndex() ;
				
			int iEmpCode    = common.toInt((String)ADriverEmpCode.get(iIndex));
			String SEmpName = (String)JCDriversName.getSelectedItem() ;	
            for(int i=0; i<theModel.getRowCount()-1; i++)
            {
                iExtraTrip  = iExtraTrip + common.toInt(String.valueOf(theModel.getValueAt(i,ExtraTrip)));
				dAmount     = dAmount+ common.toDouble(String.valueOf(theModel.getValueAt(i,10)));
			}
			int iFromDate            = common.toInt(TStDate.toNormal());
			int iToDate              = common.toInt(TEnDate.toNormal());
                     
			String SMonth = (TStDate.toNormal()).substring(0,6);                     
			int iOpening     = common.toInt(TOpening.getText() );
			int iCarriedOver = common.toInt(TCarriedOver.getText());
			double dRoundedNet = common.toDouble(TRoundedNet.getText());
						 
                        int iIsAlreadyExist = getIsAlreadyExist(iEmpCode,SMonth);
				        if(iIsAlreadyExist==0)
						{

                        PreparedStatement ps     = null;
                        ps                       = theConnection.prepareStatement(getSaveQS());
                              
                         ps    .    setInt(1,iEmpCode);
						 ps    .    setString(2,SEmpName);
                         ps    .    setInt(3,iExtraTrip); 
                         ps    .    setDouble(4,dAmount);
                         ps    .    setString(5,SMonth);
                         ps    .    setString(6,SAuthSysName);
                         ps    .    setInt(7,iOpening);
						 ps    .    setInt(8,iCarriedOver);
                         ps    .    setDouble(9,dRoundedNet);
						 ps    .    setInt(10,iFlowCode);
                                                          
                         iExCount =  ps               . executeUpdate();
                             
                         ps         . close();
                         ps         = null;
                         iExecuteCount     = iExecuteCount+iExCount;
                         }
                                                    
                                            
                
               
               if(iExecuteCount>0)
               {          
                    JOptionPane.showMessageDialog(null, "Data Saved Succesfully");
                 // setTableDetails();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"Data Already Processed");     
               }
               theConnection                 . commit();
               theConnection          = null;
               
          }
          catch (Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null, "Problem in  Saving Data", "Error", 
               JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
               try
               {
                    theConnection      . rollback();   
               } catch(Exception e){}
               
          }
     }
    private String getSaveQS()
    {
        StringBuffer SB = null;
    
		SB = new StringBuffer();
          
        SB.append(" Insert into WaterLorryIncentive(ID,EmpCode,DriverName,ExtraTrips,Amount,IncentiveMonth,EntryDate,EntryDateAndTime,EntrySystemName,Opening,CarriedOver,RoundedNet,FlowCode) ");
        SB.append(" values(WaterLorryIncentive_Seq.nextVal,?,?,?,?,?,to_Char(SysDate,'DD.MM.YYYY'),to_Char(SysDate,'DD.MM.YYYY HH24:MI:SS'),?,?,?,?,?) ");
         // System.out.println("QS:"+SB.toString());
        return SB.toString();
    }
	

    private void refresh()
    {
        try
        {
            theVector.removeAllElements();
            theModel.setNumRows(0);
        }
        catch(Exception e)
        {
			System.out.println(e);
            e.printStackTrace();
        }
    }
     
    private void removeFrame()
    {
        try
        {
            Layer.remove(this);
            Layer.updateUI();
        }
        catch(Exception ex){}
    }
  
    
}
