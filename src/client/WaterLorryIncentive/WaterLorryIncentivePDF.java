package client.WaterLorryIncentive;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;
import java.lang.String;
import util.*;        


import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class WaterLorryIncentivePDF
{

    String SFile="",SFromDate="",SToDate="",SDriverName="";
    Vector theVector;
	    
    Common common = new Common();

    int            Lctr      = 100;
    int            Pctr      = 0;

    private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
    private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.RED);
    private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
    private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);

    private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    private static Font tinyBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font underBold  = new Font(Font.FontFamily.TIMES_ROMAN, 14,Font.UNDERLINE);

    ArrayList theDriverList;

    String SHead[]  = {"SNO","DATE","OPENING KM","CLOSING KM","TOT NOOF KM RUN/DAY","STANDARD TRIPS/DAY ON STD.KM","ACTUAL TRIPS/DAY ON ACTUAL KMS","ACTUAL WATER LOAD TRIPS","CEILING WATER LOAD TRIPS","EXTRA TRIP<13","INCENTIVE AMT","EXTRA TRIP>13","INCENTIVE AMT","TOT INCENTIVE"};

    int    iWidth[] = {9,20,16,16,20,20,20,18,18,18,18,18,20,20};
     
    Document document;
    PdfPTable table;
    Connection theConnection = null;
    
   
    public WaterLorryIncentivePDF(String SFile,String SFromDate,String SToDate,String SDriverName,Vector theVector,ArrayList theDriverList)
    {
        this.SFile         = SFile;
        this.SFromDate     = SFromDate;
        this.SToDate       = SToDate;
        this.SDriverName   = SDriverName;

		this.theVector     = theVector ;
        this.theDriverList = theDriverList;  
		createPDFFile();
          
    }
    
	private void createPDFFile()
	{
		try
        {
            document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(SFile));
            document.open();

			document.newPage();
			
            table = new PdfPTable(14);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);

            addBody(document,table);
                        
            JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	
	private void addBody(Document document,PdfPTable table) throws BadElementException
	{
		try
		{
     
			table.flushContent();
			table = new PdfPTable(14);
			table.setWidths(iWidth);
			table.setWidthPercentage(100);
			table.setSpacingAfter(50);

			addIncentiveData(document,table);
		}
		catch(Exception ex)
		{
			System.out.println(" addIncentivedata PDF "+ex);
			ex.printStackTrace();
		}
	}
   
	private void addIncentiveData(Document document,PdfPTable table)
	{
		Paragraph paragraph;
        PdfPCell c1;
		
        try
		{
						
			table = new PdfPTable(14);
			table.setWidths(iWidth);
			table.setWidthPercentage(100);
			table.setSpacingAfter(10);
			table.setHeaderRows(4);
			
			c1 = new PdfPCell(new Phrase("MONTHLY WATER LORRY EXTRA TRIP INCENTIVE FOR "+SDriverName+" ",bigBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(0);
			c1.setColspan(14);
			table.addCell(c1);
		
			c1 = new PdfPCell(new Phrase("FROM "+common.parseDate(SFromDate)+" TO "+common.parseDate(SToDate)+" ",bigBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(0);
			c1.setColspan(14);
			table.addCell(c1);
		
			c1 = new PdfPCell(new Phrase("",tinyBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(0);
			c1.setColspan(14);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("S.No",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("DATE",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("OPENING KM",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("CLOSING KM",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("TOT NOOF KM RUN/DAY",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("STANDARD TRIPS/DAY ON STD.KMS",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("ACTUAL TRIPS/DAY ON ACTUAL KMS",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("ACTUAL WATER LOAD TRIPS",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			c1 = new PdfPCell(new Phrase("CEILING WATER LOAD TRIPS",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("EXTRA TRIP<13",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("INCENTIVE AMT",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
	
			
			c1 = new PdfPCell(new Phrase("EXTRA TRIP>13",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("INCENTIVE AMT",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("TOT INCENTIVE",mediumBold));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			table.addCell(c1);

           
			int iSlNo = 0 ;
			double dTotalAmt = 0.0;
			
			System.out.println("theDriverList : "+theDriverList.size());
				
			for(int i=0;i<theDriverList.size();i++)
            		{
				HashMap theMap = (HashMap)theDriverList.get(i);
										   
				int iTotalNoOfKMRunPerDay = common.toInt(String.valueOf(theMap.get("TotalNoOfKMRunPerDay")));
				int iExtraTrip            = common.toInt(String.valueOf(theMap.get("ExtraTrip")));
               
									
				iSlNo = iSlNo+1			
		;
				String SSno              	 = String.valueOf(iSlNo);
				String SDate   			 = ((String)theMap.get("StDate"));
				int    iStartKm			 = (common.toInt(String.valueOf(theMap.get("StartKm"))));
				int    iEndKm                    = (common.toInt(String.valueOf(theMap.get("EndKm"))));
                int    iStandardTrip 		 = (common.toInt(String.valueOf(theMap.get("StandardTrip"))));
		        int    iActualTrip   		 = (common.toInt(String.valueOf(theMap.get("ActualTrip"))));
				int    iExtraTrip8   		 = (common.toInt(String.valueOf(theMap.get("ExtraTrip8"))));
		        double dAmount8       		 = (common.toDouble(String.valueOf(theMap.get("Amount8"))));
				int    iExtraTrip12   		 = (common.toInt(String.valueOf(theMap.get("ExtraTrip12"))));
		        double dAmount12       		 = (common.toDouble(String.valueOf(theMap.get("Amount12"))));
				double dIncentiveAmt  		 = (common.toDouble(String.valueOf(theMap.get("IncentiveAmount"))));

                           
                dTotalAmt = dTotalAmt + dIncentiveAmt;
			   
				c1 = new PdfPCell(new Phrase(common.Pad(SSno,9),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);

				c1 = new PdfPCell(new Phrase(SDate,bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_LEFT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
				  
				c1 = new PdfPCell(new Phrase(String.valueOf(iStartKm),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
					 
				c1 = new PdfPCell(new Phrase(String.valueOf(iEndKm),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);

				c1 = new PdfPCell(new Phrase(String.valueOf(iTotalNoOfKMRunPerDay),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
					 
				c1 = new PdfPCell(new Phrase(String.valueOf(iStandardTrip),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
					 
				c1 = new PdfPCell(new Phrase(String.valueOf(iActualTrip),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iActualTrip),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iStandardTrip),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
				
				c1 = new PdfPCell(new Phrase(String.valueOf(iExtraTrip8),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
						 
				c1 = new PdfPCell(new Phrase(common.getRound(dAmount8,2),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);

				c1 = new PdfPCell(new Phrase(String.valueOf(iExtraTrip12),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
						 
				c1 = new PdfPCell(new Phrase(common.getRound(dAmount12,2),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
				table.addCell(c1);
				   
				c1 = new PdfPCell(new Phrase(common.getRound(dIncentiveAmt,2),bigNormal));
				c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);           
				table.addCell(c1);
							
				Lctr = Lctr+1;

            }
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
					 
			c1 = new PdfPCell(new Phrase(common.Pad(" TOTAL ",9),bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
			
			c1 = new PdfPCell(new Phrase("",bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
				 
			c1 = new PdfPCell(new Phrase(common.getRound(dTotalAmt,2),bigNormal));
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM);
			table.addCell(c1);
                                              
            document.add(table);
			
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex);
		}
		
		document.close();
	}
   
   }
