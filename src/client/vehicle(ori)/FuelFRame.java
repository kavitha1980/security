package client.vehicle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;

import util.*;

public class FuelFRame extends JFrame
{

     JLayeredPane Layer;
     int          iStKm;
     String       SVehicleNo;

     JPanel       TopPanel,BottomPanel;
     JPanel       ModiPanel;

     DateField1    DComplete;


     JTextField   TKm;


     MyButton     BOkay;
     
     Common common = new Common();

     public FuelFRame(int iStKm,String SVehicleNo )
     {
          //this.Layer          = Layer;
          this.iStKm          = iStKm;
          this.SVehicleNo     = SVehicleNo;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          ModiPanel      = new JPanel();

          DComplete      = new DateField1();
          DComplete.setTodayDate();      
          DComplete.setEditable(true);


          TKm            = new JTextField();


          BOkay          = new MyButton("Save");

     }

     private void setLayouts()
     {
          TopPanel   .setLayout(new GridLayout(1,2,10,10));

          BottomPanel.setLayout(new FlowLayout());
          ModiPanel  .setLayout(new BorderLayout());

          BOkay.setMnemonic('S');

          TopPanel.setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {


          TopPanel.add(new MyLabel("FUEL KM"));
          TopPanel.add(TKm);

          BottomPanel.add(BOkay);

          ModiPanel.add("North",TopPanel);
          ModiPanel.add("South",BottomPanel);
     }    

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    if(isValid(iStKm))
                    {
                         int iFuelKm         = Integer.parseInt((String)TKm.getText());

                         Class.forName("oracle.jdbc.OracleDriver");
                         Connection con      = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                         Statement stat      = con.createStatement();
                         String QS           = "Update Vehicles set FuelKms="+iFuelKm+" where VehicleNo='"+SVehicleNo+"' and Stkm="+iStKm+" " ;          
                         stat.executeUpdate(QS);
                         stat.close();
                         con.close();
                         JOptionPane.showMessageDialog(null,"Updated");
                         setVisible(false);
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Plz Enter Valid Fuel Km ");
                         TKm.setText("");
                         TKm.requestFocus();
                    }

               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     private boolean isValid(int iStartingKm)
     {
          boolean bOkFlag=true;
          try
          {
               int iFuelKm         = Integer.parseInt((String)TKm.getText());
               
               if(iFuelKm<iStartingKm)
               {
                    bOkFlag   = false;
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return bOkFlag;
     }

               

}
