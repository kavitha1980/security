package client.vehicle;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;
import util.Common;
import util.*;

import blf.*;


public class VehicleInFrame extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane layer;

     JPanel TotalPanel,EndKmPanel,ControlPanel,VInfo1Panel,VInfo2Panel,InfoPanel,TopPanel,CardPanel,InTimePanel,InDatePanel,TablePanel;

     JDialog theDialog;

     MyTextField TCardNo;

     WholeNumberField TEndKm;

     JDesktopPane desktop;

     MyLabel LVehicleNo,LVehicleName,LDriverName,LPurpose,LSecurityName,LInDate;

     MyLabel LText,LPlace;

     DateField1 DInDate;

     TimeField TInTime;

     JButton BSave,BClose,BCancel;

     Vector RetrieveVector;

     VehicleInfo InfoDomain;

     String SRightNow="";

     //int CardNo=0;

     String SCardNo="";

     int IInDate=0;

     int iTotal=0;

     String SOutTime="";

     Common common      = new Common();

     JTable infoTable;

     JScrollPane tableScroll;

     VehicleInModel vehicleinmodel;

     int iDate=0;

     public VehicleInFrame(JLayeredPane layer)
     {
          this.layer=layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);

               InfoDomain          = (VehicleInfo)registry.lookup(SECURITYDOMAIN);

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }


     public void createComponents()
     {
          CardPanel      =    new JPanel();
          InTimePanel    =    new JPanel();
          InDatePanel    =    new JPanel();
          TopPanel       =    new JPanel();
          TotalPanel     =    new JPanel();
          InfoPanel      =    new JPanel();
          ControlPanel   =    new JPanel();
          EndKmPanel     =    new JPanel();
          VInfo1Panel    =    new JPanel();
          VInfo2Panel    =    new JPanel();
          TablePanel     =    new JPanel();

          TCardNo        =    new MyTextField(8);
          TEndKm         =    new WholeNumberField(10);

          TEndKm.addFocusListener(new focusEvents());

          LVehicleNo     =    new MyLabel();
          LVehicleName   =    new MyLabel();
          LDriverName    =    new MyLabel();
          LPurpose       =    new MyLabel();
          LSecurityName  =    new MyLabel();
          LInDate        =    new MyLabel();
          LPlace         =    new MyLabel();

          TInTime        =    new TimeField();
          SRightNow      =    TInTime.getTimeNow();

          DInDate        =    new DateField1();

          DInDate.setTodayDate();

          LInDate.setText(common.parseDate(DInDate.toNormal()));

          BSave          =    new JButton("Update");
          BSave.setEnabled(false);
          BClose         =    new JButton("Exit");
          BCancel        =    new JButton("Cancel");

          String Date    = DInDate.toNormal();
          iDate          = common.toInt(common.pureDate(Date));

          try
          {
               Vector VInfo   = new Vector();
               VInfo          = InfoDomain.getCurrentInInfo(iDate);
               vehicleinmodel   = new VehicleInModel(VInfo);
               infoTable      = new JTable(vehicleinmodel);
               tableScroll    = new JScrollPane(infoTable);
           }
           catch(Exception e)
           {
               System.out.println("Exception"+e);
           }
     }

     public void setLayouts()
     {
          try
          {
               LVehicleNo   .setForeground(new Color(100,0,50));
               LVehicleName .setForeground(new Color(100,0,50));
               LDriverName  .setForeground(new Color(100,0,50));
               LPurpose     .setForeground(new Color(100,0,50));
               LSecurityName.setForeground(new Color(100,0,50));
               LPlace       .setForeground(new Color(100,0,50));


               TotalPanel.setLayout(new GridLayout(4,1));
               TopPanel.setLayout(new GridLayout(1,4));
               InfoPanel.setLayout(new GridLayout(1,2));
               InfoPanel.setBorder(new TitledBorder("VehicleInfo"));
               VInfo1Panel.setLayout(new GridLayout(3,2));
//               VInfo1Panel.setBorder(new TitledBorder("VehicleInfo"));
               VInfo2Panel.setLayout(new GridLayout(3,2));
//               VInfo2Panel.setBorder(new TitledBorder("VehicleInfo"));
               CardPanel.setBorder(new TitledBorder("Card_No"));
               InTimePanel.setBorder(new TitledBorder("In_Time"));
               InDatePanel.setBorder(new TitledBorder("In_Date"));
               EndKmPanel.setBorder(new TitledBorder("Ending_Km"));
               TablePanel.setLayout(new BorderLayout());
               TablePanel.setBorder(new TitledBorder("Details about Vehicles Currently In"));

               setTitle("Vehicle In Frame(12.0)");
               setSize(798,520);
               setVisible(true);
     
               setIconifiable(true);
               setMaximizable(true);
               setClosable(true);
               setResizable(true);

               BSave.setMnemonic('U');
               BClose.setMnemonic('X');
               BCancel.setMnemonic('C');

               BSave.addActionListener(new action());
               BClose.addActionListener(new action());
               BCancel.addActionListener(new action());

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void addComponents()
     {
          CardPanel.add(TCardNo);
          TCardNo.addKeyListener(new PressedKey());

          InTimePanel.add(new MyLabel(SRightNow));

          InDatePanel.add(LInDate);

          EndKmPanel.add(TEndKm);

          TopPanel.add(CardPanel);
          TopPanel.add(InTimePanel);
          TopPanel.add(InDatePanel);
          TopPanel.add(EndKmPanel);

          VInfo1Panel.add(new MyLabel("VehicleNo"));
          VInfo1Panel.add(LVehicleNo);

          VInfo1Panel.add(new MyLabel("VehicleName"));
          VInfo1Panel.add(LVehicleName);

          VInfo1Panel.add(new MyLabel("DriverName"));
          VInfo1Panel.add(LDriverName);

          VInfo2Panel.add(new MyLabel("Purpose"));
          VInfo2Panel.add(LPurpose);

          VInfo2Panel.add(new MyLabel("SecurityName"));
          VInfo2Panel.add(LSecurityName);

          VInfo2Panel.add(new MyLabel("Place"));
          VInfo2Panel.add(LPlace);

          ControlPanel.add(BSave);
          ControlPanel.add(BClose);
          ControlPanel.add(BCancel);

          InfoPanel.add(VInfo1Panel);
          InfoPanel.add(VInfo2Panel);

          TablePanel.add(tableScroll);

          TotalPanel.add(TopPanel);
          TotalPanel.add(InfoPanel);
          TotalPanel.add(TablePanel);
          TotalPanel.add(ControlPanel);

          getContentPane().add(TotalPanel);
     }
     private class focusEvents extends FocusAdapter
     {
          public void focusLost(FocusEvent fe)
          {

               try
               {
                    String SNo = (TCardNo.getText()).trim();
     
                    Vector v2       =    InfoDomain.getKm(SNo);
                    String Sskm     =    (String)v2.elementAt(1);
                    int ikm         =    Integer.parseInt(Sskm);
     
                    String SSEndKm  =    (TEndKm.getText()).trim();
                    int iIEndKm     =    Integer.parseInt(SSEndKm);

                    if(ikm > iIEndKm)
                    {
                         JOptionPane.showMessageDialog(null,"Cannot be lesser than Starting");
                         TEndKm.requestFocus();
                         TEndKm.setText("");
                    }
                    else
                    {
                         int totalkm     =    iIEndKm - ikm;
                         //JOptionPane.showMessageDialog(null,"Total kms Travelled :"  +totalkm);
                         JOptionPane.showMessageDialog(null,getKmMessage(totalkm),"Info",JOptionPane.INFORMATION_MESSAGE);
                         BSave.requestFocus();
                         BSave.setEnabled(true);
                    }
               }
               catch(Exception e)
               {
                    //e.printStackTrace();
               }

          }
     }
     private class action implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                String sEndKm="";

                if(ae.getSource()==BSave)
                {
                       sEndKm    = common.parseNull((TEndKm.getText()).trim());

                       if(sEndKm=="")
                       {
                            JOptionPane.showMessageDialog(null,"Plz Fill Closing Km")  ;
                       }     
                       else
                       {
                              String SNo = (TCardNo.getText()).trim();
                              //int iCard  = Integer.parseInt(SNo);  
                              UpdateRecords(SNo);
                       }
                }
                if(ae.getSource()==BClose)
                {
                       setVisible(false);
                }
                if(ae.getSource()==BCancel)
                {
                       clearFields();
                }

          }
     }        
     private void clearFields()
     {
          TEndKm.setEditable(true);
          TEndKm.setText("");
          TCardNo.setText("");
          LVehicleNo.setText("");
          LVehicleName.setText("");
          LDriverName.setText("");
          LPurpose.setText("");
          LSecurityName.setText("");
          LPlace.setText("");
          TCardNo.requestFocus();
          BSave.setEnabled(false);
     }
     private String getKmMessage(int Ikm)
     {
          String str = "<html><body>";
          str = str + "<font size='30'><b>Total KMs Travelled :"+Ikm+"</b></font><br>";
          str = str + "</body></html>";
          return str;
     }


     private class PressedKey extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               String cardNo="";
               try
               {
                    if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                    {
                         SCardNo = (TCardNo.getText()).trim();
                         
                         TEndKm.requestFocus();
                         //cardNo  = Integer.parseInt(SCardNo);
                         cardNo  = SCardNo;
                  
                         if(cardNo=="")
                         {
                              JOptionPane.showMessageDialog(null,"Not a number");
                         }
                         else
                         {
                         int status = InfoDomain.getVehicleCardNo(cardNo,0);
                         RetrieveValues(SCardNo);
                         }
                           

                    }
               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }
          }
     }


     public void RetrieveValues(String CardNo)
     {
          try
          {       

               RetrieveVector =    InfoDomain.getVehicleInInfo(CardNo);
               LVehicleNo.setText((String)RetrieveVector.elementAt(0));
               LVehicleName.setText((String)RetrieveVector.elementAt(1));
               LDriverName.setText((String)RetrieveVector.elementAt(2));
               LPurpose.setText((String)RetrieveVector.elementAt(3));
               LSecurityName.setText((String)RetrieveVector.elementAt(4));
               LPlace.setText((String)RetrieveVector.elementAt(5));
               SOutTime = (String)RetrieveVector.elementAt(6);     

               String SVehicleNo        = (String)RetrieveVector.elementAt(0);
               String SVehiclePurpose   = (String)RetrieveVector.elementAt(3);
               System.out.println("Purpose"+SVehiclePurpose);
               int iStkm                = Integer.parseInt((String)RetrieveVector.elementAt(7));

               int iOk=0;

               if(SVehicleNo.equals("TN39J 7994"))
                    iOk=1;
               if(SVehiclePurpose.equals("TO FILL FUEL") && iOk==0)
               {
                         Frame dummy = new Frame();
                         FuelFRame mframe = new FuelFRame(iStkm,SVehicleNo);
                         theDialog = new JDialog(mframe,"Fuel Dialog",true);
                         theDialog.getContentPane().add(mframe.ModiPanel);
                         theDialog.setBounds(80,100,300,250);
                         theDialog.setVisible(true);
               }
          }
          catch(Exception e)
          {
               TCardNo.setText("");
               TCardNo.requestFocus();
               JOptionPane.showMessageDialog(null,"This Vehicle already Inside");
               //e.printStackTrace();
          }
     }



     public void UpdateRecords(String CNo)
     {
          Vector updateVector = new Vector();

          try
          {
               DInDate.setTodayDate();
               String SInDate =    DInDate.toNormal();
               int IInDate    =    Integer.parseInt(SInDate);

               Vector v1      =    InfoDomain.getKm(CNo);
               String skm     =    (String)v1.elementAt(1);
               int km         =    Integer.parseInt(skm);

               String SEndKm  =    (TEndKm.getText()).trim();
               int IEndKm     =    Integer.parseInt(SEndKm);
               SRightNow      =    TInTime.getTimeNow();

               if(km > IEndKm)
               {
                    JOptionPane.showMessageDialog(null,"Cannot be lesser than Starting");
                    TEndKm.setText("");
                    TEndKm.requestFocus();
               }
               else
               {
                    iTotal = IEndKm-km;

                    InfoDomain.getVehicleUpdate(CNo,SRightNow,IInDate,IEndKm,SOutTime);
                    InfoDomain.updateVehicleKm(CNo,IEndKm);
                    JOptionPane.showMessageDialog(null,"Record Updated","Message",JOptionPane.YES_OPTION,null);
                    //setVisible(false);
                    BSave.setEnabled(false);
                    TEndKm.setEditable(true);
                    clearFields();
               }
          }
          catch(Exception e)
          {     
               JOptionPane.showMessageDialog(null,"All the Fields Must be Filled");
               //e.printStackTrace();
          }
     }   
}
