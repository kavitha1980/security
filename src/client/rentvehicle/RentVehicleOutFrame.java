package client.rentvehicle;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;
import util.Common;
import util.*;

import blf.*;


public class RentVehicleOutFrame extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane layer;

     JPanel TotalPanel,EndKmPanel,TerminatePanel,ControlPanel,VInfo1Panel,VInfo2Panel,InfoPanel,TopPanel,CardPanel,OutTimePanel,OutDatePanel,TablePanel;

     JDialog theDialog;

     MyTextField TCardNo;

     WholeNumberField TEndKm,TLoad;

     MyComboBox JCTerminate;

     JDesktopPane desktop;

     MyLabel LVehicleNo,LVehicleName,LDriverName,LPurpose,LInDate,LPlace,LOutDate,LInTime,LStartingKm,LOutTime;

     DateField1 DOutDate;

     TimeField TOutTime;

     JButton BSave,BClose,BCancel;

     Vector RetrieveVector;

     RentVehicleInfo InfoDomain;

     String SRightNow="";

     String SCardNo="";

     int iStKm=0;

     String SId="";

     String STripNo="";

     Common common      = new Common();

     JTable infoTable;

     JScrollPane tableScroll;

     RentVehicleOutModel rentvehicleoutmodel;

     int iDate=0;

     public RentVehicleOutFrame(JLayeredPane layer)
     {
          this.layer=layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);

               InfoDomain          = (RentVehicleInfo)registry.lookup(SECURITYDOMAIN);

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }


     public void createComponents()
     {
          CardPanel      =    new JPanel();
          OutTimePanel   =    new JPanel();
          OutDatePanel   =    new JPanel();
          TopPanel       =    new JPanel();
          TotalPanel     =    new JPanel();
          InfoPanel      =    new JPanel();
          ControlPanel   =    new JPanel();
          EndKmPanel     =    new JPanel();
          TerminatePanel =    new JPanel();
          VInfo1Panel    =    new JPanel();
          VInfo2Panel    =    new JPanel();
          TablePanel     =    new JPanel();

          TCardNo        =    new MyTextField(8);
          TEndKm         =    new WholeNumberField(10);
          TLoad          =    new WholeNumberField(10);
          JCTerminate    =    new MyComboBox();

          LVehicleNo     =    new MyLabel();
          LVehicleName   =    new MyLabel();
          LDriverName    =    new MyLabel();
          LPurpose       =    new MyLabel();
          LOutDate       =    new MyLabel();
          LPlace         =    new MyLabel();
          LInDate        =    new MyLabel();
          LInTime        =    new MyLabel();
          LStartingKm    =    new MyLabel();
          LOutTime       =    new MyLabel();

          TOutTime       =    new TimeField();

          DOutDate       =    new DateField1();

          DOutDate.setTodayDate();

          LOutDate.setText(common.parseDate(DOutDate.toNormal()));


          SRightNow      =    TOutTime.getTimeNow();
          LOutTime.setText(SRightNow); 

          BSave          =    new JButton("Update");
          BSave.setEnabled(false);
          BClose         =    new JButton("Exit");
          BCancel        =    new JButton("Cancel");

          String Date    = DOutDate.toNormal();
          iDate          = common.toInt(common.pureDate(Date));

          try
          {
               Vector VActiveId    = new Vector();
               VActiveId           = InfoDomain.getRVActiveID();
               Vector VInfo        = new Vector();
//               VInfo               = InfoDomain.getRVCurrentOutInfo(VActiveId);
               rentvehicleoutmodel = new RentVehicleOutModel(VInfo);
               infoTable           = new JTable(rentvehicleoutmodel);
               tableScroll         = new JScrollPane(infoTable);
           }
           catch(Exception e)
           {
               System.out.println("Exception"+e);
               e.printStackTrace();
           }   
     }
     public void setLayouts()
     {
          try
          {
               LVehicleNo   .setForeground(new Color(100,0,50));
               LVehicleName .setForeground(new Color(100,0,50));
               LDriverName  .setForeground(new Color(100,0,50));
               LPurpose     .setForeground(new Color(100,0,50));
               LPlace       .setForeground(new Color(100,0,50));
               LInDate      .setForeground(new Color(100,0,50));
               LInTime      .setForeground(new Color(100,0,50));
               LStartingKm  .setForeground(new Color(100,0,50));


               TotalPanel.setLayout(new GridLayout(4,1));
               TopPanel.setLayout(new GridLayout(1,5));
               InfoPanel.setLayout(new GridLayout(1,2));
               VInfo1Panel.setLayout(new GridLayout(5,2,8,8));
               VInfo1Panel.setBorder(new TitledBorder("VehicleInfo"));
               VInfo2Panel.setLayout(new GridLayout(5,2,8,8));
               VInfo2Panel.setBorder(new TitledBorder("VehicleInfo"));
               CardPanel.setBorder(new TitledBorder("Card_No"));
               OutTimePanel.setBorder(new TitledBorder("Out_Time"));
               OutDatePanel.setBorder(new TitledBorder("Out_Date"));
               EndKmPanel.setBorder(new TitledBorder("Ending_Km"));
               TerminatePanel.setBorder(new TitledBorder("Rental Status"));
               TablePanel.setLayout(new BorderLayout());
               TablePanel.setBorder(new TitledBorder("Details about Rental Vehicles Currently Out"));

               setTitle("Rent Vehicle Out Frame(16.0)");
               setSize(800,580);
               setVisible(true);
     
               setIconifiable(true);
               setMaximizable(true);
               setClosable(true);
               setResizable(true);

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void addComponents()
     {
          CardPanel.add(TCardNo);

          OutTimePanel.add(LOutTime);
          OutDatePanel.add(LOutDate);

          EndKmPanel.add(TEndKm);

          JCTerminate.addItem("Continue");
          JCTerminate.addItem("Terminate");

          TerminatePanel.add(JCTerminate);

          TopPanel.add(CardPanel);
          TopPanel.add(OutTimePanel);
          TopPanel.add(OutDatePanel);
          TopPanel.add(EndKmPanel);
          TopPanel.add(TerminatePanel);

          VInfo1Panel.add(new MyLabel("VehicleNo"));
          VInfo1Panel.add(LVehicleNo);

          VInfo1Panel.add(new MyLabel("VehicleName"));
          VInfo1Panel.add(LVehicleName);

          VInfo1Panel.add(new MyLabel("DriverName"));
          VInfo1Panel.add(LDriverName);

          VInfo1Panel.add(new MyLabel("Purpose"));
          VInfo1Panel.add(LPurpose);

          VInfo1Panel.add(new MyLabel("LoadNo"));
          VInfo1Panel.add(TLoad);


          VInfo2Panel.add(new MyLabel("Place"));
          VInfo2Panel.add(LPlace);

          VInfo2Panel.add(new MyLabel("InDate"));
          VInfo2Panel.add(LInDate);

          VInfo2Panel.add(new MyLabel("InTime"));
          VInfo2Panel.add(LInTime);

          VInfo2Panel.add(new MyLabel("Starting Km"));
          VInfo2Panel.add(LStartingKm);

          VInfo2Panel.add(new MyLabel(""));
          VInfo2Panel.add(new MyLabel(""));


          ControlPanel.add(BSave);
          ControlPanel.add(BClose);
          ControlPanel.add(BCancel);

          BSave.setMnemonic('U');
          BClose.setMnemonic('X');
          BCancel.setMnemonic('C');


          InfoPanel.add(VInfo1Panel);
          InfoPanel.add(VInfo2Panel);

          TablePanel.add(tableScroll);

          TotalPanel.add(TopPanel);
          TotalPanel.add(InfoPanel);
          TotalPanel.add(TablePanel);
          TotalPanel.add(ControlPanel);

          getContentPane().add(TotalPanel);
          TEndKm.setEditable(false);

          TCardNo.addKeyListener(new PressedKey());

     }
     public void addListeners()
     {
          BSave.addActionListener(new action());
          BClose.addActionListener(new action());
          BCancel.addActionListener(new action());
     }
  
     private class keyEvents extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    try
                    {
                         String SEndKm  =    (TEndKm.getText()).trim();
                         int iEndKm     =    common.toInt(SEndKm);
    
                         if(iStKm > iEndKm)
                         {
                              JOptionPane.showMessageDialog(null,"Cannot be lesser than Starting");
                              TEndKm.requestFocus();
                              TEndKm.setText("");
                              BSave.setEnabled(false);
                         }
                         else
                         {
                              int iTotalkm = iEndKm - iStKm;
                              JOptionPane.showMessageDialog(null,getKmMessage(iTotalkm),"Info",JOptionPane.INFORMATION_MESSAGE);
                              BSave.requestFocus();
                              BSave.setEnabled(true);
                         }
    
    
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }


               }
          }

     }


     private class action implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
                String sEndKm="";

                if(ae.getSource()==BSave)
                {
                       if(iStKm>0)
                       {
                           sEndKm    = common.parseNull((TEndKm.getText()).trim());
    
                           if(sEndKm=="")
                           {
                                JOptionPane.showMessageDialog(null,"Plz Fill Closing Km")  ;
                           }     
                           else
                           {
                                  String SNo = (TCardNo.getText()).trim();
                                  UpdateRecords(SNo);
                           }
                       }
                       else
                       {
                           String SNo = (TCardNo.getText()).trim();
                           UpdateRecords(SNo);
                           clearFields();
                       }
                }
                if(ae.getSource()==BClose)
                {
                       setVisible(false);
                }
                if(ae.getSource()==BCancel)
                {
                       clearFields();
                }

          }
     }        
 
     private void clearFields()
     {
          TEndKm.setText("");
          iStKm=0;
          SId="";
          STripNo="";
          TCardNo.setText("");
          LVehicleNo.setText("");
          LVehicleName.setText("");
          LDriverName.setText("");
          LPurpose.setText("");
          LPlace.setText("");
          LInDate.setText("");
          LInTime.setText("");
          LStartingKm.setText("");
          TCardNo.requestFocus();
          TCardNo.setEditable(true);
          JCTerminate.setSelectedIndex(0);
          TEndKm.setEditable(false);
          BSave.setEnabled(false);
          //SRightNow      =    TOutTime.getTimeNow();
          //System.out.println("TimeValue:"+TOutTime.getTimeNow());

          //LOutTime.setText(SRightNow);
     }
     private String getKmMessage(int Ikm)
     {
          String str = "<html><body>";
          str = str + "<font size='30'><b>Total KMs Travelled :"+Ikm+"</b></font><br>";
          str = str + "</body></html>";
          return str;
     }


     private class PressedKey extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               try
               {
                    if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                    {
                         SCardNo = (TCardNo.getText()).trim();

                         TCardNo.setEditable(false);
                         
                         TEndKm.requestFocus();
                  

                         if(checkCardAvailable())
                         {
                             if(checkCardStatus())
                             {

                                  RetrieveValues(SCardNo);
    
                                  if(iStKm>0)
                                  {
                                        TEndKm.requestFocus();
                                        TEndKm.setEditable(true);
                                        BSave.setEnabled(false);
                                        TEndKm.addKeyListener(new keyEvents());
                                  }
                                  else
                                  {
                                        BSave.requestFocus();
                                        BSave.setEnabled(true);
                                        TEndKm.setEditable(false);
                                  }
                             }
                             else
                             {

                                  JOptionPane.showMessageDialog(null,"Vehicle In Entry does not exist");
                                  clearFields();
                             }
                             
                         }
                         else
                         {

                              JOptionPane.showMessageDialog(null,"No Such Card Number in Master");
                              clearFields();
                         }

                    }
               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }
          }
     }
     private boolean checkCardAvailable()
     {
          boolean flag=true;
          try
          {
               String Scardno    = (String)TCardNo.getText();

               int iCount = InfoDomain.getRVCheckCardAvailable(Scardno);
               if(iCount==0)
               {                                             
                    flag=false;
               }
          }
          catch(Exception e)
          {
               flag=false;
          }
          return flag;
     }
     private boolean checkCardStatus()
     {
          boolean flag=true;
          try
          {
               String Scardno    = (String)TCardNo.getText();

               int status = InfoDomain.getRVCheckCardStatus(Scardno);
               if(status==1)
               {                                             
                    flag=false;
               }
          }
          catch(Exception e)
          {
                    flag=false;
          }
          return flag;
     }

     public void RetrieveValues(String CardNo)
     {
          try
          {       

               RetrieveVector =    InfoDomain.getRVVehicleOutInfo(CardNo);
               LVehicleNo.setText((String)RetrieveVector.elementAt(0));
               LVehicleName.setText((String)RetrieveVector.elementAt(1));
               LDriverName.setText((String)RetrieveVector.elementAt(2));
               LPurpose.setText((String)RetrieveVector.elementAt(3));
               LPlace.setText((String)RetrieveVector.elementAt(4));
               LInTime.setText((String)RetrieveVector.elementAt(5));     

               iStKm   = common.toInt((String)RetrieveVector.elementAt(6));
               LStartingKm.setText(String.valueOf(iStKm));

               LInDate.setText(common.parseDate((String)RetrieveVector.elementAt(7)));

               STripNo = (String)RetrieveVector.elementAt(8);
               SId     = (String)RetrieveVector.elementAt(9);

               SRightNow      =    TOutTime.getTimeNow();
               LOutTime.setText(SRightNow);
          }
          catch(Exception e)
          {
               clearFields();
               JOptionPane.showMessageDialog(null,"This Vehicle already Outside");
               e.printStackTrace();
          }
     }
     public void UpdateRecords(String CNo)
     {
          Vector updateVector = new Vector();

          try
          {
               int iRunKm=0; 
               DOutDate.setTodayDate();
               String SOutDate =    DOutDate.toNormal();
               int iOutDate    =    Integer.parseInt(SOutDate);

               String SEndKm   =    (TEndKm.getText()).trim();
               int iEndKm      =    common.toInt(SEndKm);
               int iRentStatus =    JCTerminate.getSelectedIndex();
               String SLoad    =    TLoad.getText();

               if(iStKm > iEndKm)
               {
                    JOptionPane.showMessageDialog(null,"Cannot be lesser than Starting");
                    TEndKm.setText("");
                    TEndKm.requestFocus();
               }
               else
               {
                    iRunKm = iEndKm-iStKm;

                    InfoDomain.getRVVehicleUpdate(CNo,SRightNow,iOutDate,iEndKm,iRunKm,STripNo,SId,iRentStatus,SLoad);
                    JOptionPane.showMessageDialog(null,"Record Updated","Message",JOptionPane.YES_OPTION,null);
                    BSave.setEnabled(false);
                    TEndKm.setEditable(true);
                    clearFields();
                    Vector VActiveId   = new Vector();
                    VActiveId          = InfoDomain.getRVActiveID();
                    Vector VInfo   = new Vector();
//                    VInfo          = InfoDomain.getRVCurrentOutInfo(VActiveId);
                    rentvehicleoutmodel.setVector1(VInfo);
               }
          }
          catch(Exception e)
          {     
               JOptionPane.showMessageDialog(null,"All the Fields Must be Filled");
               e.printStackTrace();
          }
     }
  
}
