package client.rentvehicle;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import java.lang.*;

public class RentVehicleModel extends DefaultTableModel
{  
     String columnname[]={"S.No.","Vehicle Name","VehicleRegNo","OuTTime","DriverName","Initial Km","Purpose"};
     String ColumnType[]={"N","N","N","N","N","N","N"};     
     Object values[][];
     Vector v1;
     int sNo;

     RentVehicleModel(Vector v1)
     {
               this.v1 = v1;
               setDataVector(getdata(),columnname);
     }

     private Object[][] getdata()
     {
               int sno=0;
               int size = (v1.size()/6);
               Object values[][]=new Object[size][columnname.length];
               int m=0;
               try
               {
                    for(int i=0;i<size;i++)
                    {
                         sno=sno+1;                    
                         values[i][0]   =    String.valueOf(sno);
                         values[i][1]   =    v1.elementAt(0+m);
                         values[i][2]   =    v1.elementAt(1+m);
                         values[i][3]   =    v1.elementAt(2+m);
                         values[i][4]   =    v1.elementAt(3+m);
                         values[i][5]   =    v1.elementAt(4+m);
                         values[i][6]   =    v1.elementAt(5+m);
                         m=m+6;
                    }
                }
                catch(Exception e)
                {
                      System.out.println("Exception in vector");
                }
                return values;
     }

     public int getSno(Vector vect1)
     {
               
               int i;
               for(i=0;i<vect1.size();i++)
               {
               }
               return i;
      }
//     public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }

     public void setVector(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }

     public  boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="N")
               return false;
          return true;
     }

     public void setVector1(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }

    



}
