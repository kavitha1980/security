package client.rentvehicle;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;

import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;

import blf.*;
import java.util.*;
import util.DateField;
import util.TimeField;
import util.MyLabel;
import util.MyComboBox;
import util.MyTextField;
import util.NameField;
import util.WholeNumberField;
import util.DateField1;
import util.Common;

public class RentVehicleInFrame extends JInternalFrame implements rndi.CodedNames
{
        JPanel leftPanel,topPanel,rightPanel,bottomPanel,middlePanel,totalPanel;
        JPanel buttonPanel,controlPanel;
        DateField1 DInDate,DOutDate;
        MyTextField TCardNo,TVehicleCode;
        WholeNumberField TStartingKm,TPersons;
        NameField NDriverName,NName,NPlace;
        MyLabel LInTime,LOutTime,LVehicleName,LOutDate,LVehicleNo,LInDate;
        JTabbedPane tab;
        MyComboBox JCPurpose,JCPlace;
        JTable infoTable;
        JScrollPane tableScroll;
        JButton BSave,BCancel,BExit;
        TimeField time = new TimeField();
        RentVehicleInModel rentvehiclemodel;
        JLayeredPane layer;
        Vector VValues ;
        RentVehicleInfo VDomain;
        Common common   = new Common();
        JDialog theDialog;

        Vector VInfo = null;
        int iDate=0;
        String SId="";

        String SVehicleNo="";
        public RentVehicleInFrame(JLayeredPane layer)
        {
                this.layer = layer;
                setDomain();
                updateLookAndFeel();
                createComponents();
                setLayouts();
                addComponents();
                addListeners();
        }
        private void setDomain()
        {
                try
                {
                        Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                        VDomain           = (RentVehicleInfo)registry.lookup(SECURITYDOMAIN);
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
        }

        private void updateLookAndFeel()
        {
                String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
                try
                {
                        UIManager.setLookAndFeel(win);
                }
                catch(Exception e)
                {
			e.printStackTrace();
                }
	}
	private void createComponents()
	{
                try
                {

                        DInDate         = new DateField1();
                        DOutDate        = new DateField1();
                        DInDate.setTodayDate();
                        NDriverName     = new NameField();
                        TVehicleCode    = new MyTextField();
                        TStartingKm     = new WholeNumberField(10);
                        TPersons        = new WholeNumberField(4);
                        NName           = new NameField();
                        NPlace          = new NameField();
        
                        tab             = new JTabbedPane();
                        TCardNo         = new MyTextField(7);
                       
                        VValues         = new Vector();

                        LInTime         = new MyLabel("");
                        LVehicleNo      = new MyLabel("");
                        LVehicleName    = new MyLabel("");
                        LInDate         = new MyLabel("");
                        LOutDate        = new MyLabel("");
                        LOutTime        = new MyLabel("");

                        LInTime.setText(time.getTimeNow());
                       
                        BSave           = new JButton("Save");
                        BCancel         = new JButton("Cancel");
                        BExit           = new JButton("Exit");

                        controlPanel    = new JPanel();
                        buttonPanel     = new JPanel();
                        leftPanel       = new JPanel();
                        topPanel        = new JPanel();
                        rightPanel      = new JPanel();
                        middlePanel     = new JPanel();
                        bottomPanel     = new JPanel();
                        totalPanel      = new JPanel();
                                        
                        JCPurpose       = new MyComboBox(VDomain.getPurpose());
                        JCPlace         = new MyComboBox(VDomain.getPlaces());

                        String Date     = DInDate.toNormal();
                        iDate           = common.toInt(common.pureDate(Date));

                        VInfo           = new Vector();
                        VInfo           = VDomain.getRVCurrentInfo(iDate);
                        rentvehiclemodel= new RentVehicleInModel(VInfo);
                        infoTable       = new JTable(rentvehiclemodel);
                        tableScroll     = new JScrollPane(infoTable);

                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
        }
        private void setLayouts()
        {
                leftPanel.setLayout(new GridLayout(5,2,1,1));
                leftPanel.setBorder(new TitledBorder("Rental Vechicle Info"));
                rightPanel.setLayout(new GridLayout(5,2,1,1));
                rightPanel.setBorder(new TitledBorder("General Info"));
                controlPanel.setLayout(new GridLayout(2,1));
                //buttonPanel.setBorder(new TitledBorder("Control"));
                bottomPanel.setLayout(new BorderLayout());
                bottomPanel.setBorder(new TitledBorder("Details about Vehicles Currently In"));
                topPanel.setLayout(new GridLayout(1,2));
                totalPanel.setLayout(new GridLayout(2,1));
        }
        
        private void addComponents()
        {

                buttonPanel.add(BSave);
                buttonPanel.add(BCancel);
                buttonPanel.add(BExit);

                BSave.setMnemonic('S');
                BExit.setMnemonic('X');
                BCancel.setMnemonic('C');

                leftPanel.add(new MyLabel("CardNo"));
                leftPanel.add(TCardNo);
                leftPanel.add(new MyLabel("Date"));
                leftPanel.add(DInDate);

                TCardNo.addKeyListener(new keyEvents());

                leftPanel.add(new MyLabel("VehicleNO"));
                leftPanel.add(LVehicleNo);
                leftPanel.add(new MyLabel("VehicleName"));
                leftPanel.add(LVehicleName);
                leftPanel.add(new MyLabel("InTime"));
                leftPanel.add(LInTime);

                rightPanel.add(new MyLabel("DriverName"));
                rightPanel.add(NDriverName);

                rightPanel.add(new MyLabel("Purpose"));
                rightPanel.add(JCPurpose);

                rightPanel.add(new MyLabel("Place"));
                rightPanel.add(JCPlace);

                rightPanel.add(new MyLabel("No. of Persons"));
                rightPanel.add(TPersons);

                rightPanel.add(new MyLabel("Starting Km"));
                rightPanel.add(TStartingKm);

                bottomPanel.add(tableScroll);

                topPanel.add(leftPanel);
                topPanel.add(rightPanel);
                totalPanel.add(topPanel);
                controlPanel.add(bottomPanel);
                controlPanel.add(buttonPanel);

                setTitle("Rent Vehicle In Frame (15.0)");
                setSize(800,520);
                setMaximizable(true);
                setIconifiable(true);
                setClosable(true);

                getContentPane().add(totalPanel,"North");
                getContentPane().add(controlPanel);
                setVisible(true);
                
                DInDate.setEditable(false);
                BSave.setEnabled(false);

        }

        private void addListeners()
        {
                BSave.addActionListener(new actionEvents());
                BCancel.addActionListener(new actionEvents());
                BExit.addActionListener(new actionEvents());
        }
        private class keyEvents extends KeyAdapter
        {
                public void keyPressed(KeyEvent ke)
                {
                        if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                        {

                                if(checkCardAvailable())
                                {
                                    if(checkCardStatus())
                                    {
                                            setPresets();
                                            BSave.setEnabled(true);
                                    }
                                    else
                                    {
    
                                            JOptionPane.showMessageDialog(null,"This Vehicle Already In");
                                            clearFields();
                                    }
                                }
                                else
                                {

                                        JOptionPane.showMessageDialog(null,"No Such Card Number in Master");
                                        clearFields();
                                }

                        }
                }

        }

        private boolean checkCardAvailable()
        {
                boolean flag=true;
                try
                {
                        String Scardno    = (String)TCardNo.getText();

                        int iCount = VDomain.getRVCheckCardAvailable(Scardno);
                        if(iCount==0)
                        {                                             
                                flag=false;
                        }
                }
                catch(Exception e)
                {
                        flag=false;
                }
                return flag;
        }



        private boolean checkCardStatus()
        {
                boolean flag=true;
                try
                {
                        String Scardno    = (String)TCardNo.getText();

                        int status = VDomain.getRVCheckCardStatus(Scardno);
                        if(status ==0)
                        {                                             
                                flag=false;
                        }
                }
                catch(Exception e)
                {
                        flag=false;
                }
                return flag;
        }

        private class actionEvents implements ActionListener
        {
                public void actionPerformed(ActionEvent ae)
                {
                        if(ae.getSource()==BSave)
                        {
                                boolean ok = checkFields();
                                if(ok)
                                {
                                        insertValues(VValues);
                                        VValues.removeAllElements();
                                }
                                else
                                {

                                       JOptionPane.showMessageDialog(null,"All fields must be filled");
                                }

                        }

                        if(ae.getSource()==BCancel)
                        {
                                clearFields();
                        }                                
                        if(ae.getSource()==BExit)
                        {
                                setVisible(false);
                        }
                }
        }

        private void clearFields()
        {
                TCardNo.setText("");
                LVehicleNo.setText("");
                LVehicleName.setText("");
                LInTime.setText("");
                NDriverName.setText("");
                TPersons.setText("");
                TStartingKm.setText("");
                JCPurpose.setSelectedIndex(0);
                JCPlace.setSelectedIndex(0);
                TCardNo.requestFocus();
                BSave.setEnabled(false);
                SId="";

        }
                                       


        private void setPresets()
        {
                Vector v1       = new Vector();
                try
                {
                        String scard    = (String)TCardNo.getText();
                   
                        v1              = VDomain.getRentVehicleInfo(scard);

                        LVehicleNo.setText((String)v1.elementAt(0));
                        LVehicleName.setText((String) v1.elementAt(1));
                        String SPurpose = (String)v1.elementAt(2);
                        SId = (String)v1.elementAt(3);
                        JCPurpose.setSelectedItem(SPurpose);
                        LInTime.setText(time.getTimeNow());

                        SVehicleNo       = (String)v1.elementAt(0);

                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
        }
        private boolean checkFields()
        {
                boolean flag=true;
                try
                {

                        String SCardNo = common.parseNull(TCardNo.getText().trim());

                        if(SCardNo.equals(""))
                        {
                                flag= false;
                                return flag;
                        }
        
                        String SVno     = common.parseNull((String)LVehicleNo.getText());
                        String SVname   = common.parseNull((String)LVehicleName.getText());
                        String SInTime  = common.parseNull((String)LInTime.getText());
                        int    iPersons = common.toInt(common.parseNull((String)TPersons.getText()));
                        String SStKm    = common.parseNull(((String)TStartingKm.getText()).trim());
                        int    iStKm    = common.toInt(SStKm);
                        String SPlace   = (String)JCPlace.getSelectedItem();
                        String SPurpose = (String)JCPurpose.getSelectedItem();
                        String SStatus  = "0";

                        String sdate    = DInDate.toNormal();
                        if(common.parseNull(sdate).equals(""))
                        {
                              flag = false;
                              return flag;
                        }
                        int iDate       = common.toInt(sdate);

                        String Sdriver   = common.parseNull(NDriverName.getText().trim());

                        if(Sdriver.equals(""))
                        {
                                flag= false;
                                return flag;
                        }

                        String STripNo = VDomain.getMaxRVTripNo(common.toInt(SId));
                        int iTripNo    = common.toInt(STripNo)+1;


                        VValues.addElement(SCardNo);
                        VValues.addElement(SVno);
                        VValues.addElement(SVname);
                        VValues.addElement(SInTime);
                        VValues.addElement(String.valueOf(iStKm));
                        VValues.addElement(SPlace);
                        VValues.addElement(SPurpose);
                        VValues.addElement(String.valueOf(iDate));
                        VValues.addElement(Sdriver);
                        VValues.addElement(SStatus);
                        VValues.addElement(SId);
                        VValues.addElement(String.valueOf(iTripNo));
                        VValues.addElement(String.valueOf(iPersons));
                        
                }
                catch(Exception e)
                {                                 
                        flag=false;
                        JOptionPane.showMessageDialog(null,"Incorrect Values");

                }
                return flag;
        }                

        public void insertValues(Vector vect)
        {
                try
                {
                        VDomain.insertRVData(VValues);
                        clearFields();
                        Vector Vinfo    = new Vector();
                        Vinfo           = VDomain.getRVCurrentInfo(iDate);
                        rentvehiclemodel.setVector1(Vinfo);
                }
                catch(Exception e)        
                {
                        e.printStackTrace();
                }
        }

}
