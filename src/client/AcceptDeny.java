package client;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import util.*;
import domain.jdbc.*;


public class AcceptDeny extends JFrame  
{

     JButton        BAccept,BDeny;
     JLabel         theLabel;
     MyComboBox     JCUsers,JCUnit,JCDivision;

     JPasswordField TA;
     JPanel         TopPanel,MiddlePanel,BottomPanel;
	 
     Vector         VUserCode;
     Vector         VUserName;
     Vector         VPassword;
     Vector         VDivisionCode;

     int            iUserCode,iUnitCode,iDivisionCode;

     Common common = new Common();

     AcceptDeny()
     {
          try
          {

               setUsers();

               TopPanel       = new JPanel(true);
               MiddlePanel    = new JPanel(true);
               BottomPanel    = new JPanel(true);
     
               BAccept        = new JButton("Accept");
               BDeny          = new JButton("Deny");
     
               theLabel       = new JLabel();

               JCUsers        = new MyComboBox(VUserName);

               TA             = new JPasswordField();
     
               MiddlePanel    .setLayout(new GridLayout(2,3,5,5));
     
               BottomPanel    .add(BAccept);
               BottomPanel    .add(BDeny);

               MiddlePanel    .add(new MyLabel("User Name"));
               MiddlePanel    .add(JCUsers);
               
               MiddlePanel    .add(new MyLabel("Password"));
               MiddlePanel    .add(TA);
     
               MiddlePanel    .setBorder(new TitledBorder("Login"));
     
               theLabel       .setBackground(Color.cyan);
               theLabel       .setForeground(Color.red);
     
               BAccept        .setBackground(Color.cyan);
               BAccept        .setForeground(Color.blue);
               BDeny          .setBackground(Color.red);
               BDeny          .setForeground(Color.white);
               TA             .setFont(new Font("monospaced",Font.PLAIN,12));
               TA             .setForeground(new Color(130,30,60));
               TA             .setBackground(Color.pink);
     
               getContentPane().add("North",theLabel);
               getContentPane().add("Center",MiddlePanel);
               getContentPane().add("South",BottomPanel);
     
               Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
               double x             = screenSize.getWidth();
               double y             = screenSize.getHeight();
               x=x/2;
               y=y/2;
               setSize(340,160);
               setLocation(new Double(x-150).intValue(),new Double(y-90).intValue());

               setResizable(false);

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }


     private void setUsers()
     {
          VUserCode =    new Vector();
          VUserName =    new Vector();
          VPassword =    new Vector();


          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");
               Statement stat   = theConnection.createStatement();
               ResultSet rs     = stat.executeQuery(" Select UserCode,UserName,password from RawUser order by username ");
               while(rs.next())
               {
                    VUserCode          . addElement(common.parseNull(rs.getString(1)));
                    VUserName          . addElement(common.parseNull(rs.getString(2)));
                    VPassword          . addElement(common.parseNull(rs.getString(3)));
               }
               rs                       . close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public boolean isPassValid()
     {
          boolean bFlag = false;

          try
          {
               String SPass     = (String)VPassword.elementAt(JCUsers.getSelectedIndex());
               iUserCode        = common.toInt((String)VUserCode.elementAt(JCUsers.getSelectedIndex()));

               if(SPass.equalsIgnoreCase(TA.getText()))
                    return true;
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
               ex.printStackTrace();
          }
          return false;
     }
}






