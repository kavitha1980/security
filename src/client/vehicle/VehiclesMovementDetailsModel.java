package client.vehicle;

import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import java.sql.*;
import util.*;

public class VehiclesMovementDetailsModel extends DefaultTableModel 
{
     protected String SWhere;
     protected String SIndentNo;

     Vector theVector;

     String ColumnName[]   = {"S.NO", "VehicleName",      "Purpose", "Place", "DriverName", "OutDateTime", "InDateTime",  "Click"};
     String ColumnType[]   = {"S"	,     "S"	   ,       "S"	   ,    "S" ,    "S"	  ,  "S"		 ,   "S"	   , "B"};
	  int iColumnWidth[]   = {8 ,20,20,20,20,20,20,10};


     Common common = new Common();

     boolean bIndent=false;

     VehiclesMovementDetailsModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol]=="B" || ColumnType[iCol]=="E")
               return true;
          return false;
     }


     public int getRows()
     {
          return super.dataVector.size();
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          RowData[0][0]= "";
          RowData[0][1]= "";
          RowData[0][2]= "";
          RowData[0][3]= "";
          RowData[0][4]= "";
          RowData[0][5]= "";
          RowData[0][6]= "";
          RowData[0][7]= new Boolean(false);

          return RowData;
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public String getFibreCode(int iRow)
     {
          return (String)getValueAt(iRow,0);
     }

     public String getFibreName(int iRow)
     {
          return (String)getValueAt(iRow,1);
     }
     public String getValueFrom(int iRow,int iCol)
     {
          return (String)getValueAt(iRow,iCol);
     }
     public void setMasterDetails(String SValue,int iRow,int iCol)
     {
          setValueAt(SValue,iRow,iCol);                        
     }

}