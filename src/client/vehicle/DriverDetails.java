package client.vehicle;

import java.sql.*;
import domain.jdbc.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.sql.Date.*;
import java.util.*;
import java.io.*;
import util.*;

public class DriverDetails extends JInternalFrame
{
    protected       JLayeredPane Layer;
    protected       boolean bFlag = false;
    JPanel          TopPanel,BottomPanel;
    JPanel          TopLeft,TopRight;
    
    JTabbedPane     jTabPane;
    
    MyComboBox      JCDriver;
    Vector          VDriver;
    boolean okflag = true;
    
    DateField       DFromDate,DToDate;
    
    JTextField      TDriverName;
    JTextField      TWorkType;
    
    String STime;
    String SDriver="";
    String SWork="";
    String SInformTime="";
    
    String          InformedTime;
    TimeFieldNew    CTime;
    
    String          MeetedTime;
    TimeFieldNew    CMTime;
    
    JButton         BSubmit,BExit;
    
    Vector          VPartyName,VPartyCode;
    
    JTable          theTable;
    
    Common common ;
    
    public DriverDetails(JLayeredPane Layer)
    {
        this.Layer = Layer;
        
        try
        {
            common  = new Common();
            getParty();
            createComponents();
            setLayouts();
            addComponents();
            addListener();
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }
    
    private void createComponents()
    {                     
        TopPanel         = new JPanel();
        BottomPanel      = new JPanel();
    //  MiddlePanel      = new JPanel();
        
        TopLeft          = new JPanel();
        TopRight         = new JPanel();     
        
        DFromDate        = new DateField();
        DFromDate.setTodayDate();
        
        JCDriver         = new MyComboBox(VPartyName);
        
        TDriverName      = new JTextField();
        TWorkType        = new JTextField();
        
        CTime            = new TimeFieldNew(true);
        InformedTime     = CTime.toNormalNew();
        
        CMTime           = new TimeFieldNew(true);
        MeetedTime       = CMTime.toNormalNew();
        
        BSubmit          = new JButton("Save");
        BExit            = new JButton("Exit");
    }
    
    private void setLayouts()
    {
        setTitle("DRIVERS REGISTER");
        setMaximizable(true);
        setClosable(true);
        setResizable(true);
        setIconifiable(true);
        setBounds(0,0,620,500);
//      setSize(300,300);
        show();
        
        TopPanel.setLayout(new GridLayout(6,2,2,2));
        BottomPanel.setBorder(new TitledBorder("Save And Exit"));
        TopPanel.setBorder(new TitledBorder(" Driver Details"));
    }
    
    private void addComponents()
    {
    
        TopPanel.setSize(6,7);
        TopPanel.add(new MyLabel("Date"));
        TopPanel.add(DFromDate);
        
/*      TopPanel.add(new MyLabel("To Date"));
        TopPanel.add(DToDate);
*/
        
        TopPanel.add(new JLabel("Driver Name"));
        TopPanel.add(JCDriver);
        
        TopPanel.add(new JLabel("New Employee Name"));
        TopPanel.add(TDriverName);
        
        TopPanel.add(new JLabel("Work Type"));
        TopPanel.add(TWorkType);
        
        TopPanel.add(new MyLabel("In Time"));
        TopPanel.add(CTime);
        
        TopPanel.add(new MyLabel("Out Time"));
        TopPanel.add(CMTime);
        
        BottomPanel.add(BSubmit);
        BottomPanel.add(BExit);
        
        getContentPane().add(TopPanel,"North");
        getContentPane().add(BottomPanel,"South");
    }
    
    private void addListener()
    {
        BSubmit.addActionListener(new ActList());
        BExit.addActionListener(new ActList());
        TDriverName.addFocusListener(new cardFocus());
        JCDriver.addItemListener(new ComboEvents());
    }
    
    private class cardFocus extends FocusAdapter
    {
        public void focusLost(FocusEvent fe)
        {
            okflag=true;
            SInformTime="";
            SWork="";
            
            try
            {
                String SDriverName  = TDriverName.getText().trim();
                String QS           = "Select outtime,intime,worktype from Driver where DriverName='"+TDriverName.getText()+"' and InDate="+DFromDate.toNormal()+" ";
                
                int st         =  getGuestInitialCheck(QS);
                if(st==0)
                {
                    okflag=false;
                    JOptionPane.showMessageDialog(null,"Already a person having This Card");
                    TDriverName.setText("");
                    TWorkType.setText("");
                    JCDriver.requestFocus();
                }
                
                else
                {
                if(!SInformTime.equals(""))
                CTime.fromStringNew(SInformTime);
                
                if(!SWork.equals(""))
                TWorkType.setText(SWork);
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            //NGuestName.requestFocus();
        }
    }
    
    private class ComboEvents implements ItemListener
    {
        public void itemStateChanged(ItemEvent a)
        {
            if(a.getSource()==JCDriver)
            {
                try
                {
                    int index      = JCDriver.getSelectedIndex();
                    if(index==0)
                    {
                        TDriverName.setText("");
                    }
                
                    else
                    {
                        TDriverName.setText((String)JCDriver.getSelectedItem());
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            
            }
        }
    }
    
    private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource()==BSubmit)
            {
                if(!SInformTime.equals(""))
                updateDeluxOrder();

                else
                insertIntoTable();
            }
            if (ae.getSource() == BExit)
            {
                removeFrame();
            }
        }  
    }
    
    private void removeFrame()
    {
        try
        {
            Layer.remove(this);
            Layer.repaint();
            Layer.updateUI();
        }
        catch(Exception ex)
        {
        
        }
    }
    
    public void getParty()
    {
        VPartyCode = new Vector();
        VPartyName = new Vector();
        
        String SQry = " Select id,name from vehicledrivers ";
        
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection theConnection = 
            DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
            
            Statement theStatement = theConnection.createStatement();
            ResultSet theResult = theStatement.executeQuery(SQry);
            
            VPartyName.add("New Driver");
            VPartyCode.add("999");
            
            while(theResult.next())
            {
                VPartyName.add(theResult.getString(2));
                VPartyCode.add(theResult.getString(1));
            }
            theStatement.close();
        }
        catch(Exception e)
        {
            System.out.println("3"+e);
            e.printStackTrace();                                                                                                                                                                                                    
        }
    }
    
    public void insertIntoTable()
    {
        try
        {
            String SOutTime = CMTime.toNormalNew();
            
            if(SOutTime.length()>1)
            SOutTime = SOutTime;
            else
            SOutTime = "";
            
            String QS = " insert into driver (Id,DriverName,worktype,intime,outtime,inDate) values( DriverDetails_seq.nextval,'"+(String)TDriverName.getText()+"', '"+(String)TWorkType.getText()+"', '"+CTime.toNormalNew()+"','"+SOutTime+"' ,'"+DFromDate.toNormal()+"') ";
            
            Class.forName("oracle.jdbc.OracleDriver");
            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
            Statement theStatement = theConnection.createStatement();
            
            JOptionPane.showMessageDialog(null,"Record saved","Message",JOptionPane.NO_OPTION,null);
            
            System.out.println(QS);
            
            theStatement.executeUpdate(QS);
            theStatement.close();
            
            TDriverName.setText("");
            TWorkType.setText("");
            CTime.toClear();
            CMTime.toClear();
        }
        catch(Exception ex)
        {
            System.out.println("Created Party name : "+ex);
        }                                       
    }
    
    public void updateDeluxOrder()
    {
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
            Statement theStatement = theConnection.createStatement();
            theStatement.executeUpdate(" Update driver  set outtime='"+CMTime.toNormalNew()+"' WHere indate='"+DFromDate.toNormal()+"' and DriverName='"+TDriverName.getText()+"' ");
            
            JOptionPane.showMessageDialog(null,"Record Updated","Message",JOptionPane.NO_OPTION,null);
            
            TDriverName.setText("");
            TWorkType.setText("");
            
            CTime.toClear();
            CMTime.toClear();
            
            theStatement.close();
        }
        catch(Exception ex)
        {
            System.out.println("Update Party name : "+ex);
        }                                       
    }
    
    private String getPartyCode(String SName)
    {
        int iIndex=-1;
        String SPartyCode="";
        String SPartyName="";
        iIndex = VPartyName.indexOf(SName);
        if(iIndex!=-1)
        {
            SPartyCode = (String)VPartyCode.elementAt(iIndex);
        }
        else
        {
            SPartyCode = "";
        }
        return SPartyCode;
    }
    
    public int getGuestInitialCheck(String QS)
    {
        int card=-1;
        
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
            Statement stat   = theConnection.createStatement();
            ResultSet result = stat.executeQuery(QS);
            
            while(result.next())
            {
                String SMeetedTime   = common.parseNull(result.getString(1));
                SInformTime          = common.parseNull(result.getString(2));
                SWork                = common.parseNull(result.getString(3));
                
                if(!SMeetedTime.equals(""))
                card           = 0;
            }
            result.close();
            stat.close();
        }
        catch(Exception ex)
        {
            System.out.println("Secondary RowSet in DataManager(setGuestInitialCheck()) : "+ex);
            ex.printStackTrace();
        }
    
        return card;
    
    }
}
