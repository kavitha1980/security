package client.vehicle;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import domain.jdbc.*;

import blf.*;

public class FuelFRame extends JFrame implements rndi.CodedNames
{

     JLayeredPane Layer;
     int          iStKm;
     String       SVehicleNo;

     JPanel       TopPanel,BottomPanel;
     JPanel       ModiPanel;

     DateField1    DComplete;

     VehicleInfo vehicleData;

     JTextField   TKm;


     MyButton     BOkay;
	 JButton 		BExit;
     
     Common common = new Common();

     public FuelFRame(int iStKm,String SVehicleNo )
     {
          //this.Layer          = Layer;
          this.iStKm            = iStKm;
          this.SVehicleNo       = SVehicleNo;

          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);

               vehicleData         = (VehicleInfo)registry.lookup(SECURITYDOMAIN);

          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }


     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          ModiPanel      = new JPanel();

          DComplete      = new DateField1();
          DComplete.setTodayDate();      
          DComplete.setEditable(true);


          TKm            = new JTextField();

          BOkay          = new MyButton("Save");
  		  BExit          = new JButton("Exit");
     }

     private void setLayouts()
     {
          TopPanel   .setLayout(new GridLayout(1,2,10,10));

          BottomPanel.setLayout(new FlowLayout());
          ModiPanel  .setLayout(new BorderLayout());

          BOkay.setMnemonic('S');

          TopPanel.setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {

          TopPanel.add(new MyLabel("FUEL KM"));
          TopPanel.add(TKm);

          BottomPanel.add(BOkay);
  		  //BottomPanel.add(BExit);

          ModiPanel.add("North",TopPanel);
          ModiPanel.add("South",BottomPanel);


     }

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
		   //BExit.addActionListener(new ActList());
     }
     
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               try
               {
                    if(isValid(iStKm))
                    {

                         int iFuelKm         = Integer.parseInt((String)TKm.getText());
                         try
                         {

                         /*Class.forName("oracle.jdbc.OracleDriver");
                         Connection con      = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                         Statement stat      = con.createStatement();
                         String QS           = "Update Vehicles set FuelKms="+iFuelKm+" where VehicleNo='"+SVehicleNo+"' and Stkm="+iStKm+" " ;          
                         stat.executeUpdate(QS);
                         stat.close();
                         con.close();*/

                         vehicleData.updateFuelData(SVehicleNo,iFuelKm,iStKm);

                         JOptionPane.showMessageDialog(null,"Updated");
                         setVisible(false);
                         }
                         catch(Exception e)
                         {
                              JOptionPane.showMessageDialog(null,"Error:"+e);
                         }
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Error:Check the Fuel Km !!! ");
                    }
					if(ae.getSource()==BExit)
               		{
                    		if((JOptionPane.showConfirmDialog(null,"Do U want to Exit? ","Warning",JOptionPane.YES_NO_OPTION))== 0)
                        	 exitFrame();
                    		else
                         	return;
               		}
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     private boolean isValid(int iStartingKm)
     {
          boolean bOkFlag=true;
          try
          {
               int iFuelKm         = Integer.parseInt((String)TKm.getText());
               
               if(iFuelKm<iStartingKm)
               {
                    bOkFlag   = false;
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return bOkFlag;
     }

	 public void exitFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ed)
          {
               System.out.println(ed);
          }
     }               

}
