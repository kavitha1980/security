/*
     The Subclass of the VehicleEntryFrame to capture the Vehicle Code
*/

package client.vehicle;
import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import java.awt.Font;
import java.awt.Color;
import domain.jdbc.*;
import java.sql.*;

import ADSDK.Communication.Quote;
import ADSDK.RFID.P30.P30Comm;

import util.*;
public class VehicleSerialReader implements SerialPortEventListener,Runnable
{
		protected JLabel LCardNo,LVehicleNo,LVehicleName,LTransType;
		protected JLabel LDriverCode,LDriverName,LMasterCard;
		protected JButton BOkay;
		protected JList KeyCodeList;
		protected DefaultListModel Items;
		
		static CommPortIdentifier portId;
		static Enumeration portList;
		private P30Comm PassiveComm;
		
		SerialPort serialPort;
		InputStream inputStream;
		OutputStream outputStream;
		Connection con;	
		Thread  readThread;
		Common common = new Common();
		String SPort     = "COM1";
		String strBuffer = "";
		String sKeyCode = "";
		String sTransType = "";
		String SVehicleCard = "";
		String SDriverCard = "";
		int    ctr=0;
		int    iMultiply = 1;
		boolean heartAttack = false;
		Font KeyCodeFont =   new Font("Times New Roman", Font.BOLD, 20);
		Font KeyNameFont =   new Font("Times New Roman", Font.BOLD, 18);
		StringBuffer inputBuffer = new StringBuffer();
		private byte[] _rcvByte2 = new byte[1];
		
		VehicleEntryFrame VehicleEntry;
		VehicleEntryModel   VEntryModel;
		String CardNO = "";
		String VehicleNo = "";
		String VehicleName = "";
		String SMasterCard = "";
		String Vehicleinfo = "";
		String strRcvDataTemp ="";
		String PrevRcvDataTempOut = "";
		String PrevRcvDataTempIn = "";
		String PrevRcvDataDriver = "";
		String SDriver = "";
		String SType = "";
	          				
     public VehicleSerialReader(JLabel LCardNo,JLabel LVehicleNo,JLabel LVehicleName,JLabel LTransType,JLabel LDriverCode,JLabel LDriverName,JLabel LMasterCard,JButton BOkay,VehicleEntryFrame VehicleEntry,VehicleEntryModel   VEntryModel)
     {
          this.LCardNo   	= 	LCardNo;
		  this.LVehicleNo  	= 	LVehicleNo;
		  this.LVehicleName	=	LVehicleName;
		  this.LTransType	=	LTransType;
		  this.LDriverCode 	= 	LDriverCode;
		  this.LDriverName 	= 	LDriverName;
		  this.LMasterCard  =   LMasterCard;
          this.BOkay      	= 	BOkay;
		  this.VehicleEntry	=	VehicleEntry;
		  this.VEntryModel  = 	VEntryModel;
		  PassiveComm = new P30Comm();
          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          //System.out.println("createComponents 1");
		  
		  LCardNo.setForeground(new Color(255,94,94));
		  LVehicleNo.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();  
			   System.out.println("After portList "+portList);        
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
					
//					System.out.println("After portId --> "+portId.getName()); 
					
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println("Comm Error : "+ex);
          }
     }     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;
		  String SCardData = "";
		  int CardType = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                     try
                    {
							String CardNO = "";
     			  		 _rcvByte2 = new byte[inputStream.available()];
						   while(newData!=-1)
					      {                             
							  if (inputStream.available() > 0)
							  {
							        
									inputStream.read(_rcvByte2, 0, _rcvByte2.length);
							  }
								newData = inputStream.read();
								CardNO	= CardNO+ByteArrayToHexString(_rcvByte2);
                         }
//						 String CardNO =  ByteArrayToHexString(_rcvByte2);
						 System.out.println("CardNO  --->1 "+CardNO);
//						 setVehicleInfo(CardNO);
						 int intstart = CardNO.indexOf("CCFFFF10");
						 if(CardNO.length()-intstart >= 40)
						 {
							SCardData = CardNO.substring(intstart+14, intstart+40-2);
							CardType = CheckCardType(SCardData);
						 }
						 if(CardType == 1)
						 {						 
							 	setVehicleCard(SCardData);
						  }
						  else
						  {
							  	setDriverCard(SCardData);
						  }	
						 setVehicleInfo();
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     private void setVehicleInfo()
     {	 	  
		  try
          {		  			
					Vehicleinfo		=	getVehicleInfo(SVehicleCard);
					StringTokenizer ST  = new StringTokenizer(Vehicleinfo,"#");
					VehicleNo  	= ST.nextToken();
					VehicleName = ST.nextToken();
					SType  		= ST.nextToken();
					SMasterCard	= ST.nextToken();
									
					System.out.println("Vehicle In / Out  1 OR 0 ---> "+SType);
					System.out.println("Vehicle Carno No ---> "+SVehicleCard);
					System.out.println("Driver Carno No ---> "+SDriverCard);
					
					if(SType.equals("0"))
					{
//						if(!SDriverCard.equals(PrevRcvDataDriver))
//						{
							SDriver = getDriverName(SDriverCard);
							LDriverName		.	setText(SDriver);
							if(!SVehicleCard.equals(PrevRcvDataTempOut))
							{
//									SDriver = getDriverName(SDriverCard);
									if(SType.equals("0"))
									{
										LCardNo			.	setText(SVehicleCard);
										LVehicleName	.	setText(VehicleName);
										LVehicleNo		.	setText(VehicleNo);
										LTransType		.	setText(SType);
										LMasterCard		.	setText(SMasterCard);
										LDriverName		.	setText(SDriver);
										VehicleEntry.showList();   // For Vehicle Out
									}
									PrevRcvDataTempOut = SVehicleCard;
									PrevRcvDataTempIn = "";
							}
//							PrevRcvDataDriver = SDriverCard;
//						}
					}
					if(SType.equals("1"))
					{
						SDriverCard ="";
//						if(!SDriverCard.equals(PrevRcvDataDriver))
//						{
							SDriver = getDriverName(SDriverCard);
							LDriverName		.	setText(SDriver);
							if(!SVehicleCard.equals(PrevRcvDataTempIn))
							{
//							    SDriver = getDriverName(SDriverCard);
								System.out.println("Before Update List");
								LDriverName		.	setText(SDriver);
								VehicleEntry.UpdateList(VehicleNo,SType,SVehicleCard); // For Vehicle In
								PrevRcvDataTempIn = SVehicleCard;
								PrevRcvDataTempOut = "";
							}
//							PrevRcvDataDriver = SDriverCard;
//						}
					}
          }
          catch(Exception ex){freeze();}
     }
	 
   public String getVehicleInfo(String CardNO)
	{
//			System.out.println("Get Vehicle Info Card No ----> "+CardNO);
			String	SVName="",SVNo="",SType="",Scard="";
			StringBuffer sb = new StringBuffer();
			sb.append(" Select VEHICLEREGNO,VEHICLENAME,0 as Vehicleout,CardNo  from vehicleinfo where VEHICLECARDOUT =? ");
			sb.append(" Union All ");
			sb.append(" Select VEHICLEREGNO,VEHICLENAME, 1 as Vehiclein,CardNo from vehicleinfo where VEHICLECARDIN =? ");
			
			try {
				if(con==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    con       			= jdbc.getConnection();
               }
			   java.sql.PreparedStatement ps = con.prepareStatement(sb.toString());
										  ps . setString(1,CardNO.trim());
										  ps . setString(2,CardNO.trim());
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){
											SVNo=common.parseNull(rs1.getString(1));
											SVName=common.parseNull(rs1.getString(2));
											SType = common.parseNull(rs1.getString(3));
											Scard = common.parseNull(rs1.getString(4));
										  }
										  rs1.close();
										  ps.close();
			}catch(Exception ex){
				ex.printStackTrace();
				freeze();
			}
//			System.out.println("SVNo-->"+SVNo);
//			System.out.println("SVName-->"+SVName);
		    return SVNo+"#"+SVName+"#"+SType+"#"+Scard;
	  }
	  public String getDriverName(String sKeyCode){
	  
			String	SKeyName="";
			StringBuffer sb = new StringBuffer();
			sb.append(" Select EMPNAME from Staff where IDCARDNO = ? ");
			
			try {
				if(con==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    con       			= jdbc.getConnection();
               }			   
			   java.sql.PreparedStatement ps = con.prepareStatement(sb.toString());
										  ps . setString(1,sKeyCode.trim());										  
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){										  	
											SKeyName=common.parseNull(rs1.getString(1));
										  }
										  rs1.close();
										  ps.close();										  
				
			}catch(Exception ex){
				ex.printStackTrace();
			}	  
		  return SKeyName;
	  }	 
	public static String asciiStringToHexString(String s) 
	{
			String str="";
			for (int i=0;i<s.length();i++)
			{
			int ch = (int)s.charAt(i);
			String s4 = Integer.toHexString(ch);
			str = str + s4;
			}
			return str;
	} 
	public static String hexStringtoAsciiString(String s) 
	{ 
			byte[] baKeyword = new byte[s.length()/2]; 
			for(int i = 0; i < baKeyword.length; i++) 
			{ 
			try 
			{ 
			baKeyword[i] = (byte)(0xff & Integer.parseInt(s.substring(i*2, i*2+2),16)); 
			} 
			catch(Exception e) 
			{ 
			e.printStackTrace(); 
			} 
			} 
			try 
			{ 
			s = new String(baKeyword, "utf-8");//UTF-16le:Not 
			} 
			catch (Exception e1) 
			{ 
			e1.printStackTrace(); 
			} 
			return s; 
	} 
	public static byte[] hexStringToByteArray(String s) 
	{
    	int len = s.length();
	    byte[] data = new byte[len / 2];
    	for (int i = 0; i < len; i += 2) 
		{
        	data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                             + Character.digit(s.charAt(i+1), 16));
    	}
    	return data;
	}
	public String ByteArrayToHexString(byte[] b) { 
 		String ret = ""; 
 		for (int i = 0; i < b.length; i++) { 
	 		String hex = Integer.toHexString(b[i] & 0xFF); 
	 		if (hex.length() == 1) { 
	 			hex = '0' + hex; 
	 		} 
	 		ret += hex.toUpperCase(); 
 		} 
 		return ret; 
 	}
	public void setVehicleCard(String strRcvData)
	{			
		SVehicleCard = strRcvData;
		PrevRcvDataDriver = "";
	}
	public void setDriverCard(String CardNo)
	{
		 SDriverCard = CardNo;
	}
	public int CheckCardType(String CardNO)
	{
//	  	 System.out.println("Check Card No ----> "+CardNO);
		 int icount = 0;
		 StringBuffer sb = new StringBuffer();
			//sb.append(" select count(*) from staff where IDCARDNO = ? ");
			sb.append(" Select Count(*) from vehicleinfo where VEHICLECARDIN = ? or VEHICLECARDOUT = ? ");
			try {
				if(con==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    con       			= jdbc.getConnection();
               }
			   java.sql.PreparedStatement ps = con.prepareStatement(sb.toString());
										  ps . setString(1,CardNO.trim());
										  ps . setString(2,CardNO.trim());
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){
											icount = rs1.getInt(1);
										  }
										  rs1.close();
										  ps.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		    return icount;
	  }
     public void freeze()
     {
          serialPort.close();
     }
}
// dRIVER  CCFFFF10320D01E23016661302242420AA14661302242420 
// vEHICLE CCFFFF10320D01E22019440C01250660021D440C01250660   // E22019440C01250660021D44

// E22019440C02340670B74744  Vehicle In

// id 410922

// CCFFFF10320D01
//E22019440C00470810E97444

//E22019440C00610750BB4144
//CCFFFF10320D01
//E23016661301512490151466
//1301512490