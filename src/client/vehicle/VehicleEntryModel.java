package client.vehicle;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import java.lang.*;


public class VehicleEntryModel extends DefaultTableModel
{

     //String ColumnName[] = {"CardNo","VehicleRegNo","VehicleName","OutDate","OutTime","InDate","InTime","StartingKm","EndingKm","TotalKms","Place","Purpose","DriverName","SecurityName","DINo","KiNo","DIDate","KIDate","DQty","KQty","BunkName","Fuel Km","Remarks","Status","Card","Authetication"};
//     String ColumnType[] = { "S",  "S",  "E", "E", "E", "E", "E", "E", "E","S", "E", "E", "E", "E", "E", "E", "E", "E",  "E", "E", "E", "E", "E","S","S","B"};

String ColumnName[] = {"Authetication","VehicleRegNo","VehicleName","OutDate","OutTime","InDate","InTime","StartingKm","EndingKm","TotalKms","Place","Purpose","DriverName","SecurityName","DINo","KiNo","DIDate","KIDate","DQty","KQty","BunkName","Fuel Km","Remarks","Status","IdNo","Card","InCard"};
     String ColumnType[] = { "B",  "S",  "E", "E", "E", "E", "E", "E", "E","S", "E", "E", "E", "E", "E", "E", "E", "E",  "E", "E", "E", "E", "E","S","S","S","S"};

     Vector vect;

     public VehicleEntryModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Object[][] getRowData()
     {
          Object RowData[][] = new Object[0][ColumnName.length];
          return RowData;
     }
     public boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="B" || ColumnType[col]=="E")
               return true;
               return false;
     }
     public Class getColumnClass(int col)
     { return getValueAt(0,col).getClass(); }
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public int getRows()
     {
          return super.dataVector.size();
     }

     public void appendEmptyRow()
     {
          Vector temp = new Vector();

          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");
          temp.addElement("");

          temp.addElement(new Boolean(false));
          insertRow(getRows(),temp);
     }
     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }
     public void setSNo()
     {
          for(int i=0; i<getRows(); i++)
          {
               setValueAt(String.valueOf(i+1),i,0);
          }
     }
}