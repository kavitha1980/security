
/*
     The Subclass of the KeyTransactons to capture the Key Code
*/

package client.vehicle;
import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import java.awt.Font;
import java.awt.Color;
import domain.jdbc.*;
import java.sql.*;

import util.*;
public class DriverSerialReader implements SerialPortEventListener,Runnable
{
     protected JLabel LDriverCode,LDriverName;
     protected JButton BOkay;
	 protected JList KeyCodeList;
	 protected DefaultListModel Items;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;
	 Connection con;	
     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
	 String sKeyCode = "";
	 String sTransType = "";
     int    ctr=0;
     int    iMultiply = 1;
     boolean heartAttack = false;
	 Font KeyCodeFont =   new Font("Times New Roman", Font.BOLD, 20);
	 Font KeyNameFont =   new Font("Times New Roman", Font.BOLD, 18);
     StringBuffer inputBuffer = new StringBuffer();
	 VehicleEntryFrame VehicleEntry;

     public DriverSerialReader(JLabel LDriverCode,JLabel LDriverName,JButton BOkay,VehicleEntryFrame VehicleEntry)
     {
          this.LDriverCode   	= 	LDriverCode;
		  this.LDriverName   	= 	LDriverName;
          this.BOkay      	= 	BOkay;
          this.VehicleEntry		=	VehicleEntry;

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          //System.out.println("createComponents 1");
		  
		  LDriverCode.setForeground(new Color(255,94,94));
		  LDriverName.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();  
			   System.out.println("After portList "+portList);        
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
					
					System.out.println("After portId --> "+portId.getName()); 
					
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println("Comm Error : "+ex);
          }
     }     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
        				  //System.out.println("KeyCode  --->1 "+(char)newData);

                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              //System.out.println("KeyCode "+(char)newData);

                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setKeyCode(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                //if(ctr < 10)
                                  outputStream.write((int)'a');
                                //else
//                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     private void setKeyCode(String str)
     {	 	  
          String sKeyName = "";
		  try
          {
		  			System.out.println("SCardNo-->"+str);
					sKeyCode		=	str.substring(1,str.length() - 1);
					sKeyName		=	getDriverName(sKeyCode);
					LDriverName		.	setText(sKeyName);
//					VehicleEntry.showList();
          }
          catch(Exception ex){}
     }
	 
      public String getDriverName(String sKeyCode){
	  
			String	SKeyName="";
			StringBuffer sb = new StringBuffer();
			sb.append(" Select EMPNAME from Staff where IDCARDNO = ?");
			
			try {
				if(con==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    con       			= jdbc.getConnection();
               }			   
			   java.sql.PreparedStatement ps = con.prepareStatement(sb.toString());
										  ps . setString(1,sKeyCode.trim());										  
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){										  	
											SKeyName=common.parseNull(rs1.getString(1));
										  }
										  rs1.close();
										  ps.close();										  
				
			}catch(Exception ex){
				ex.printStackTrace();
			}	  
		  return SKeyName;
	  }	 
     public void freeze()
     {
          serialPort.close();
          //LDriverCode.setForeground(new Color(255,94,94));
		  LDriverCode	.	setFont(KeyCodeFont);
		  LDriverName	.	setFont(KeyNameFont);
          //BOkay.setEnabled(true);
     }
}
//FDSFESARES