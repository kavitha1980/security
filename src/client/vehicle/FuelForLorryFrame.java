package client.vehicle;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.border.*;
import java.io.*;
import java.sql.*;
import domain.jdbc.*;

import util.*;

public class FuelForLorryFrame extends JFrame
{

     JLayeredPane Layer;
     int          iStKm;
     int          iDiDate;      
     String       SVehicleNo;

     JPanel       TopPanel,BottomPanel;
     JPanel       ModiPanel;

     DateField1   DComplete;

     JRadioButton JRYes,JRNo;   

     JTextField   TKm,TKQty,TKIndentNo,TDIndentNo,TDQty;

     Connection   theConnection;

     MyButton     BOkay;
     
     Common common = new Common();

     String MSG="";
     public FuelForLorryFrame(int iStKm,String SVehicleNo ,int iDiDate)
     {
          //this.Layer          = Layer;
          this.iStKm          = iStKm;
          this.SVehicleNo     = SVehicleNo;
          this.iDiDate        = iDiDate;

          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
          ModiPanel      = new JPanel();

          DComplete      = new DateField1();
          DComplete.setTodayDate();      
          DComplete.setEditable(true);


          TKm            = new JTextField();
          TKQty          = new JTextField();
          TKIndentNo     = new JTextField();
          TDQty          = new JTextField();
          TDIndentNo     = new JTextField();

          JRYes          = new JRadioButton("Yes",false);
          JRNo           = new JRadioButton("No",true);


          BOkay          = new MyButton("Save");
     }

     private void setLayouts()
     {
          TopPanel   .setLayout(new GridLayout(5,2,10,10));

          BottomPanel.setLayout(new FlowLayout());
          ModiPanel  .setLayout(new BorderLayout());

          BOkay.setMnemonic('S');

          TopPanel.setBorder(new TitledBorder(""));
     }

     private void addComponents()
     {


          TopPanel.add(new MyLabel("FUEL KM"));
          TopPanel.add(TKm);

          TopPanel.add(new MyLabel(" Kero IndentNo"));
          TopPanel.add(TKIndentNo);

          TopPanel.add(new MyLabel(" Kero Qty"));
          TopPanel.add(TKQty);

          TopPanel.add(new MyLabel(" Diesel IndentNo"));
          TopPanel.add(TDIndentNo);

          TopPanel.add(new MyLabel(" Diesel Qty"));
          TopPanel.add(TDQty);


          BottomPanel.add(BOkay);

          ModiPanel.add("North",TopPanel);
          ModiPanel.add("South",BottomPanel);
     }    

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
     }
     
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {

               try
               {
                         if(isValid(iStKm))
                         {
     
                              int iFuelKm         = Integer.parseInt((String)TKm.getText());
                              int iKQty           = Integer.parseInt((String)TKQty.getText());   
                              int iDQty           = Integer.parseInt((String)TDQty.getText());   
     
                              String SKiNo        = (String)TKIndentNo.getText();   
                              String SDiNo        = (String)TDIndentNo.getText();   
     
                              //Class.forName("oracle.jdbc.OracleDriver");
                              //Connection con      = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:arun","gate","gatepass");
                              JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                              theConnection       = jdbc.getConnection();      

                              Statement stat      = theConnection.createStatement();
                              String QS           = " Update Vehicles set FuelKms="+iFuelKm+",Purpose='TO FILL FUEL', "+
                                                    " DQty= "+iDQty+",KQty= "+iKQty+", KINO='"+SKiNo+"',DINo='"+SDiNo+"', "+
                                                    " DiDate="+iDiDate+",KiDate="+iDiDate+" "+
                                                    " where VehicleNo='"+SVehicleNo+"' and Stkm="+iStKm+" " ;
                              stat.executeUpdate(QS);
                              stat.close();                                                                                                        
                              JOptionPane.showMessageDialog(null,"Updated");
                              setVisible(false);
                         }
                         else
                         {
                              JOptionPane.showMessageDialog(null,"+MSG+","Error",JOptionPane.ERROR_MESSAGE);
                         }
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     private boolean isValid(int iStartingKm)
     {
          boolean bOkFlag=true;
          try
          {
               int iFuelKm            = Integer.parseInt((String)TKm.getText());
               
               String SFuelKm         = common.parseNull((String)TKm.getText());
               String SKQty           = common.parseNull((String)TKQty.getText());   
               String SDQty           = common.parseNull((String)TDQty.getText());   

               String SKiNo           = common.parseNull((String)TKIndentNo.getText());
               String SDiNo           = common.parseNull((String)TDIndentNo.getText());   

               if( SFuelKm.equals("") || SKQty.equals("") || SDQty.equals("") || SKiNo.equals("") ||  SDiNo.equals(""))
               {
                    MSG       = "All Fields Must Be Filled";
                    bOkFlag   = false;
                    return bOkFlag;
               }


               if(iFuelKm<iStartingKm)
               {
                   bOkFlag   = false;
                   MSG       = "Cannot be Lesser than Starting Km";
               }
          }
          catch(Exception e)
          {
          }
          return bOkFlag;
     }

               

}
