package client.vehicle;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Font;
import java.awt.Color;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.ClockField;
import util.*;

import domain.jdbc.*;
import blf.*;


public class VehicleEntryFrame extends JInternalFrame
{
     protected           JLayeredPane Layer;
     JPanel              TopPanel,TopLeft,TopRight,MiddlePanel,MidLeft,BottomPanel;
     JButton             BSave,BExit,BApply;
     JButton             BUpdate,BDelete;
	 MyLabel			 LOutTime;
	 MyLabel 			 LDriverName;
	 MyLabel 			 LDriverCode,LCardNo,LVehicleNo,LVehicleName,LMasterCard,LVehicleCardIn;
	 MyLabel			 LTransType;
     AddressField        ARackName;
     FractionNumberField dRate;
     DateField           dateField;
	 DateField1 		 DInDate,DOutDate,DDIndentDate,DKIndentDate;
	 TimeField time 	 = new TimeField();
	 

     String STDate      ="";
     String SVechicleNo ="";
     String STDate1     ="";

     JComboBox           JCVechicleNo,JCSecurityName,JCDriverName,JCPurpose,JCPlace,JCBunkName;
     
     JTable              theTable;
     VehicleEntryModel   VEntryModel;
     Vector theVector,VVechicleNo,VSecName,VDriverName,VDriverCode,VPurpose,VPlaceName;
     Vector VBunkName,VBunkCode;
	 Vector VInsertData;
     Common common = new Common();
     JDBCConnection connect;
     Connection theconnect;

     Connection theConnection=null;
     Statement theStatement=null;
 	 DriverSerialReader DriverReader;
	 VehicleSerialReader VehicleReader;
	 
	 Font PurposeFont = 	new Font("Times New Roman", Font.BOLD, 18);

     public VehicleEntryFrame(JLayeredPane Layer)
     {
          this.Layer = Layer;
          try
          {
			   selectVechicleNo();
               selectVechicleNo1();
               createCompenents();
               setLayouts();
               addCompenents();
               addListeners();
//			   DriverReader = new DriverSerialReader(LDriverCode,LDriverName,BUpdate,this);
               VehicleReader = new VehicleSerialReader(LCardNo,LVehicleNo,LVehicleName,LTransType,LDriverCode,LDriverName,LMasterCard,BUpdate,this,VEntryModel);
				String ss = (String)LDriverName.getText();
//				showList();
          }
          catch(Exception e)
          {
               System.out.println(e);
               e.printStackTrace();
          }
     }
    public void createCompenents()
    {
          TopPanel       = new JPanel();
          TopLeft        = new JPanel();
          TopRight       = new JPanel();
          MiddlePanel    = new JPanel();
          MidLeft        = new JPanel();
          BottomPanel    = new JPanel();
          dateField      = new DateField();
		  DOutDate       = new DateField1();
		  DOutDate.setTodayDate();
		  DOutDate.setEditable(false);

          VEntryModel    = new VehicleEntryModel();
          theTable       = new JTable(VEntryModel);
          theTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		  theTable.setRowHeight(theTable.getRowHeight() + 10);

          ARackName      = new AddressField(25);
          dRate          = new FractionNumberField(10,4);
          JCVechicleNo   = new JComboBox(VVechicleNo);
          JCSecurityName = new JComboBox();
          JCDriverName   = new JComboBox();
          JCPurpose      = new JComboBox();
          JCPlace        = new JComboBox();
		  JCBunkName     = new JComboBox();
		  
		  LOutTime        = new MyLabel("");
		  LOutTime		  .setText(time.getTimeNow());
		  
		  LDriverName     = 	new MyLabel("");
		  LDriverCode	  = 	new MyLabel("");
		  LCardNo		  =     new MyLabel("");
		  LVehicleNo	  =     new MyLabel("");
		  LVehicleName	  =     new MyLabel("");
		  LTransType	  =     new MyLabel("");
		  LMasterCard     = 	new MyLabel("");
		  LVehicleCardIn  =     new MyLabel("");

          BSave          = new JButton("Save");
          BApply         = new JButton("Apply");
          BUpdate        = new JButton("Update");
          BDelete        = new JButton("Delete");
          BExit          = new JButton("Exit");
		  
		  JCSecurityName   	. setFont(PurposeFont);
		  JCDriverName	   	. setFont(PurposeFont);
		  JCPurpose	   		. setFont(PurposeFont);
		  JCPlace	   		. setFont(PurposeFont);
		  JCBunkName	   	. setFont(PurposeFont);
     }
     private void setLayouts()
     {
          setTitle("Vechicle Entry Frame");
          setMaximizable(true);
          setClosable(false);
          setResizable(true);
          setIconifiable(true);		  
          setSize(800,400);
          show();

          TopPanel.setLayout(new GridLayout(1,2));
          TopPanel.setBorder(new TitledBorder("Info"));
          MiddlePanel.setBorder(new TitledBorder("Vechicle List"));
          BottomPanel.setBorder(new TitledBorder(""));
          MiddlePanel.setLayout(new BorderLayout());
     }
     private void addCompenents()
     {
          TopPanel.add(new MyLabel("Time"));
          TopPanel.add(LOutTime);

          TopPanel.add(new MyLabel("Date"));
          TopPanel.add(DOutDate);
		  
//		  TopPanel.add(new MyLabel("CardNo"));
//          TopPanel.add(LCardNo);
		  
		  TopPanel.add(new MyLabel("VehicleNo"));
          TopPanel.add(LVehicleNo);
		  
		  TopPanel.add(new MyLabel("VehicleName"));
          TopPanel.add(LVehicleName);
		  
		  TopPanel.add(new MyLabel("DriverName"));
          TopPanel.add(LDriverName);
		  
//		  TopPanel.add(new MyLabel("Type"));
//          TopPanel.add(LTransType);
		  
//		  TopPanel.add(new MyLabel("Card"));
//          TopPanel.add(LMasterCard);	
		  	  		  				  
          //TopPanel.add(new MyLabel("Apply"));
          //TopPanel.add(BApply);

          MiddlePanel.add(new JScrollPane(theTable));

          BottomPanel.add(BSave);
//          BottomPanel.add(BDelete);
          BottomPanel.add(BExit);

          TopPanel.add(TopLeft);
          TopPanel.add(TopRight);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }
     private void addListeners()
     {
          BExit.addActionListener(new ActList());
          BSave.addActionListener(new ActList());
          BDelete.addActionListener(new ActList());
          BApply.addActionListener(new ActList());
		  theTable. addMouseListener(new MouseList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
//                    showList();
               }
               if(ae.getSource()==BExit)
               {
                    if((JOptionPane.showConfirmDialog(null,"Do U want to Exit? ","Warning",JOptionPane.YES_NO_OPTION))== 0)
					{	
						VehicleReader.freeze();
					    exitFrame();
					}
                    else
					{
                         return;
					}
               }
               if(ae.getSource()==BSave)
               {
//                    System.out.println("1st Coming");
                    setParticulars();
               }
               if(ae.getSource()==BDelete)
               {
                    setDelete();
               }
          }
     }
	 private class MouseList extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
				String VehicleRegNo  ="";
				String OutDate       ="";
				String OutTime       ="";
				String InDate        ="";
				String InTime        ="";
				String StartingKm    ="";
				String EndingKm      ="";
				String TotalKms      ="";
				String Place         ="";
				String Purpose       ="";
				String sDriver       ="";
				String SecurityName  ="";
				String DINo          ="";
				String KiNo          ="";
				String DIDate        ="";
				String KIDate        ="";
				String DQty          ="";
				String KQty          ="";
				String BunkName      ="";
				String FuelKm        ="";
				String CardNo        ="";
		  		if( me.getClickCount()==1 ) 
				{
   
						int   iRow	= 	theTable.getSelectedRow   ();
						int   iCol  = 	theTable.getSelectedColumn();
						Boolean isClick = (Boolean)theTable.getValueAt(iRow,0);
						if(isClick.booleanValue())
						{
							CardNo 		=	(String)VEntryModel.getValueAt(iRow,24);
							ValueIsNull(CardNo,iRow,"Enter CARDNO");
							VehicleRegNo  =(String)VEntryModel.getValueAt(iRow,1);
							ValueIsNull(VehicleRegNo,iRow,"Enter RegNo");
							OutDate       =(String)VEntryModel.getValueAt(iRow,3);
							OutTime       =(String)VEntryModel.getValueAt(iRow,4);
							InDate        =(String)VEntryModel.getValueAt(iRow,5);
							InTime        =(String)VEntryModel.getValueAt(iRow,6);
							StartingKm    =(String)VEntryModel.getValueAt(iRow,7);
							ValueIsNull(StartingKm,iRow,"Enter Starting Km");
							EndingKm      =(String)VEntryModel.getValueAt(iRow,8);
//							ValueIsNull(StartingKm,iRow,"Enter Ending Km");
							TotalKms      =(String)VEntryModel.getValueAt(iRow,9);
							ValueIsNull(TotalKms,iRow,"TotalKms Is Empty");
							Place         =(String)VEntryModel.getValueAt(iRow,10);
							ValueIsNull(Place,iRow,"Select Place");
							Purpose       =(String)VEntryModel.getValueAt(iRow,11);
							ValueIsNull(Place,iRow,"Selet Purpose");
							sDriver       =(String)VEntryModel.getValueAt(iRow,12);
							ValueIsNull(sDriver,iRow,"Enter Driver Name ");
							SecurityName  =(String)VEntryModel.getValueAt(iRow,13);
							ValueIsNull(SecurityName,iRow," Select Security Name ");
							if(JCPurpose.getSelectedIndex()==3)
							{
								BunkName      		=	(String)VEntryModel.getValueAt(iRow,20);
								String SBunkCode 	= 	getBunkCode(BunkName);
								if(!SBunkCode.equals("4"))
								{
									DINo          		=	(String)VEntryModel.getValueAt(iRow,14);
									ValueIsNull(DINo,iRow," Enter DINo ");
									DIDate        		=	(String)VEntryModel.getValueAt(iRow,16);
									ValueIsNull(DIDate,iRow," Enter DIDate ");
									DQty          		=	(String)VEntryModel.getValueAt(iRow,18);
									ValueIsNull(DQty,iRow," Enter DQty ");
								}
								if(SBunkCode.equals("4"))
								{
										KiNo          =(String)VEntryModel.getValueAt(iRow,15);
										ValueIsNull(KiNo,iRow," Enter KINo ");					
										KIDate        =(String)VEntryModel.getValueAt(iRow,17);							
										ValueIsNull(KIDate,iRow," Enter KIDate ");
										KQty          =(String)VEntryModel.getValueAt(iRow,19);	
										ValueIsNull(KQty,iRow," Enter KQty ");
								}
							}
							FuelKm        =(String)VEntryModel.getValueAt(iRow,21);
						}
				}
		  }
	 }
     public void showList() throws Exception
     {
				String CardNo="",VehicleRegNo="",VehicleName="",OutDate="",OutTime="",InDate="0", InTime="", StartingKm="0", EndingKm="0";
				String TotalKms="0", Place="", Purpose="", DriverName="", SecurityName="", DINo="0", KiNo="0", DIDate="0";
				String KIDate="0", DQty="0", KQty="0", BunkName="", FuelKm="",SRemarks="",SType="";	 
				
				theVector       = new Vector();
				System.out.println("DriverName-->"+(String)LDriverName.getText());
			try
			{
				
				TableColumn SColumnSecurtiy   = theTable.getColumn("SecurityName");
				JCSecurityName                = new MyComboBox(VSecName);
				SColumnSecurtiy.setCellEditor(new DefaultCellEditor(JCSecurityName));
								
				TableColumn SColumnPurpose    = theTable.getColumn("Purpose");
				JCPurpose                     = new MyComboBox(VPurpose);
				SColumnPurpose.setCellEditor(new DefaultCellEditor(JCPurpose));
				
				TableColumn SColumnPlace      = theTable.getColumn("Place");
				JCPlace                       = new MyComboBox(VPlaceName);
				SColumnPlace.setCellEditor(new DefaultCellEditor(JCPlace));
				
				TableColumn SColumnDriver     = theTable.getColumn("DriverName");
          		JCDriverName                  = new MyComboBox(VDriverName);
		        SColumnDriver.setCellEditor(new DefaultCellEditor(JCDriverName));
				
				TableColumn SColumnBunkName      = theTable.getColumn("BunkName");
				JCBunkName                       = new MyComboBox(VBunkName);
				SColumnBunkName.setCellEditor(new DefaultCellEditor(JCBunkName));
				
				StartingKm = getKm((String)LMasterCard.getText());
				
				theVector.addElement(new Boolean(false));

			  	theVector.addElement((String)LVehicleNo.getText());
				theVector.addElement((String)LVehicleName.getText());
			  	theVector.addElement(DOutDate.toString());
			  	theVector.addElement(LOutTime.getText());
				theVector.addElement(InDate);
				theVector.addElement(InTime);
//			  	theVector.addElement(DOutDate.toString());
//			  	theVector.addElement(LOutTime.getText());
			  	theVector.addElement(StartingKm);
			  	theVector.addElement(EndingKm);
			  	theVector.addElement(TotalKms);
			  	theVector.addElement(Place);
			  	theVector.addElement(Purpose);
			  	theVector.addElement((String)LDriverName.getText());
			  	theVector.addElement(SecurityName);
			  	theVector.addElement(DINo);
			  	theVector.addElement(KiNo);
				theVector.addElement(DIDate);
				theVector.addElement(KIDate);
//			  	theVector.addElement(DOutDate.toString());
//			  	theVector.addElement(DOutDate.toString());
			  	theVector.addElement(DQty);
			  	theVector.addElement(KQty);
			  	theVector.addElement(BunkName);
				theVector.addElement(FuelKm);
			  	theVector.addElement(SRemarks);
				theVector.addElement(LTransType.getText());
			  	theVector.addElement((String)LCardNo.getText());
				theVector.addElement(LMasterCard.getText());
				theVector.addElement("");
			  	
			  	VEntryModel.appendRow(theVector);
				VEntryModel.setValueAt((String)LDriverName.getText(),0,12);
				insertVehicleValues();
			}
			catch (Exception ex)
			{
				
			}
     }
	 public void UpdateList(String SVehicleNo,String SType,String SVehicleCard)
     {
          try
          {
               System.out.println("In Vehivcle In Update");
			   JDBCConnection connect        =    JDBCConnection.getJDBCConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(getBreakDownQuery(SVehicleNo,SType,SVehicleCard));

               while(theResult.next())
                    organizeBreakDown(theResult);

               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
	 private String getBreakDownQuery(String SVehicleNo,String SType,String SVehicleCard)
     {
          String QS = " select VEHICLENO,OUTDATE,OUTTIME,INDATE,INTIME,STKM,"+
                      " ENDKM,(ENDKM-STKM) as difkm,PLACE,PURPOSE,DriverName,SECURITYNAME,"+
                      " DINO,KINO,DIDATE,KIDATE,DQTY,KQTY,"+
                      " BUNKNAME,nvl(FUELKMS,0),nvl(ID,0),"+SType+" as type,VEHICLENAME,CardNo, '"+SVehicleCard+"' AS InCard "+
                      " from VEHICLES where Status=0 and VEHICLENO='"+SVehicleNo+"'";
//                      " from VEHICLES where Status=0 and VEHICLENO='"+SVehicleNo+"' and VEHICLECARDOUT = '"+SVehicleCard+"' ";
		  			System.out.println(QS);
          return QS;
     }
	 private void organizeBreakDown(ResultSet theResult) throws Exception
     {
          theVector       = new Vector();

          TableColumn SColumnSecurtiy   = theTable.getColumn("SecurityName");
          JCSecurityName                = new MyComboBox(VSecName);
          SColumnSecurtiy.setCellEditor(new DefaultCellEditor(JCSecurityName));

          TableColumn SColumnDriver     = theTable.getColumn("DriverName");
          JCDriverName                  = new MyComboBox(VDriverName);
          SColumnDriver.setCellEditor(new DefaultCellEditor(JCDriverName));

          TableColumn SColumnPurpose    = theTable.getColumn("Purpose");
          JCPurpose                     = new MyComboBox(VPurpose);
          SColumnPurpose.setCellEditor(new DefaultCellEditor(JCPurpose));

          TableColumn SColumnPlace      = theTable.getColumn("Place");
          JCPlace                       = new MyComboBox(VPlaceName);
          SColumnPlace.setCellEditor(new DefaultCellEditor(JCPlace));
		  
		  TableColumn SColumnBunkName      = theTable.getColumn("BunkName");
		  JCBunkName                       = new MyComboBox(VBunkName);
		  SColumnBunkName.setCellEditor(new DefaultCellEditor(JCBunkName));

          String S1       = common.parseNull(theResult.getString(1));
          String S2       = common.parseNull(theResult.getString(2));
          String S3       = common.parseNull(theResult.getString(3));
          String S4       = common.parseNull(theResult.getString(4));
          String S5       = common.parseNull(theResult.getString(5));
          String S6       = common.parseNull(theResult.getString(6));
          String S7       = common.parseNull(theResult.getString(7));
          String S8       = common.parseNull(theResult.getString(8));
          String S9       = common.parseNull(theResult.getString(9));
          String S10      = common.parseNull(theResult.getString(10));
          String S11      = common.parseNull(theResult.getString(11));
          String S12      = common.parseNull(theResult.getString(12));
          String S13      = common.parseNull(theResult.getString(13));
          String S14      = common.parseNull(theResult.getString(14));

          String S15      = common.parseNull(theResult.getString(15));
          String S16      = common.parseNull(theResult.getString(16));
          String S17      = common.parseNull(theResult.getString(17));
          String S18      = common.parseNull(theResult.getString(18));
          String S19      = common.parseNull(theResult.getString(19));
          String S20      = common.parseNull(theResult.getString(20));
          String S21      = common.parseNull(theResult.getString(21));
		  String S22      = common.parseNull(theResult.getString(22));
		  String S23      = common.parseNull(theResult.getString(23));
		  String S24      = common.parseNull(theResult.getString(24));
		  String SCard    = common.parseNull(theResult.getString(25));
		  
		  theVector.addElement(new Boolean(false));		// Authetication          
          theVector.addElement(S1);							// VehicleNo
		  theVector.addElement(S23);						// VehicleName
          theVector.addElement(common.parseDate(S2));		// OutDate
          theVector.addElement(S3);							// OutTime
//          theVector.addElement(common.parseDate(S4));  		// InDate
//          theVector.addElement(S5);					   		// InTime
		  theVector.addElement(DOutDate.toString());		// InDate
		  theVector.addElement(LOutTime.getText());			// InTime
          theVector.addElement(S6);							//StartingKm
          theVector.addElement(S7);							//EndingKm
          theVector.addElement(S8);							//TotalKms
          theVector.addElement(S9);							//Place
          theVector.addElement(S10);						//Purpose
//          theVector.addElement(S11);						//DriverName
		  theVector.addElement((String)LDriverName.getText()); //DriverName
          theVector.addElement(S12);						//SecurityName
          theVector.addElement(S13);						//DINo
          theVector.addElement(S14);						//KiNo
          theVector.addElement(common.parseDate(S15));		//DIDate
          theVector.addElement(common.parseDate(S16));		//KIDate
          theVector.addElement(S17);						//DQty
          theVector.addElement(S18);						//KQty
          theVector.addElement(S19);						//BunkName
          theVector.addElement(S20);						//Fuel Km
		  theVector.addElement("");							//Remarks
		  theVector.addElement(S22);						//Status
		  theVector.addElement(S21);						// ID / CardNo
		  theVector.addElement(S24);						// Card
		  theVector.addElement(SCard);						// InCard
         
          VEntryModel.appendRow(theVector);
     }
     private void setParticulars()
     {
          Boolean bFlag;

          String VehicleRegNo  ="";
		  String VehicleName   ="";
          String OutDate       ="";
          String OutTime       ="";
          String InDate        ="";
          String InTime        ="";
          String StartingKm    ="";
          String EndingKm      ="";
          String TotalKms      ="";
          String Place         ="";
          String Purpose       ="";
          String sDriver       ="";
          String SecurityName  ="";
          String DINo          ="";
          String KiNo          ="";
          String DIDate        ="";
          String KIDate        ="";
          String DQty          ="";
          String KQty          ="";
          String BunkName      ="";
          String FuelKm        ="";
		  String Remarks       = "";
          String ID            ="";
		  String SType		   = "";
		  String SCard		   = "";
		  String SInCard	   = "";
		  String SBunkCode		= "";
		  String SDriverCode	= "";

          try
          {
               for(int index=0; index<VEntryModel.getRows(); index++)
               {
                    VehicleRegNo  ="";
					VehicleName   ="";
                    OutDate       ="";
                    OutTime       ="";
                    InDate        ="";
                    InTime        ="";
                    StartingKm    ="";
                    EndingKm      ="";
                    TotalKms      ="";
                    Place         ="";
                    Purpose       ="";
                    sDriver       ="";
                    SecurityName  ="";
                    DINo          ="";
                    KiNo          ="";
                    DIDate        ="";
                    KIDate        ="";
                    DQty          ="";
                    KQty          ="";
                    BunkName      ="";
                    FuelKm        ="";
					Remarks       ="";
                    ID            ="";
					SType		  = "";
					SCard         = "";
					SBunkCode     = "";
					SDriverCode   = "";
     
//                    bFlag     = (Boolean)VEntryModel.getValueAt(index,25);

					 bFlag     = (Boolean)VEntryModel.getValueAt(index,0);

                    if(bFlag.booleanValue())
                    {
                         ID            =(String)VEntryModel.getValueAt(index,24);
                         VehicleRegNo  =(String)VEntryModel.getValueAt(index,1);
						 VehicleName   =(String)VEntryModel.getValueAt(index,2);
                         OutDate       =(String)VEntryModel.getValueAt(index,3);
                         OutTime       =(String)VEntryModel.getValueAt(index,4);
                         InDate        =(String)VEntryModel.getValueAt(index,5);
                         InTime        =(String)VEntryModel.getValueAt(index,6);
                         StartingKm    =(String)VEntryModel.getValueAt(index,7);
                         EndingKm      =(String)VEntryModel.getValueAt(index,8);
                         TotalKms      =(String)VEntryModel.getValueAt(index,9);
                         Place         =(String)VEntryModel.getValueAt(index,10);
                         Purpose       =(String)VEntryModel.getValueAt(index,11);
                         sDriver       =(String)VEntryModel.getValueAt(index,12);
                         SecurityName  =(String)VEntryModel.getValueAt(index,13);
                         DINo          =(String)VEntryModel.getValueAt(index,14);
                         KiNo          =(String)VEntryModel.getValueAt(index,14);
                         DIDate        =(String)VEntryModel.getValueAt(index,16);
                         KIDate        =(String)VEntryModel.getValueAt(index,17);
                         DQty          =(String)VEntryModel.getValueAt(index,18);
                         KQty          =(String)VEntryModel.getValueAt(index,19);
                         BunkName      =(String)VEntryModel.getValueAt(index,20);
                         FuelKm        =(String)VEntryModel.getValueAt(index,21);
						 Remarks       =(String)VEntryModel.getValueAt(index,22);
						 SType         =(String)VEntryModel.getValueAt(index,23);
						 SCard		   =(String)VEntryModel.getValueAt(index,25);
						 SInCard       =(String)VEntryModel.getValueAt(index,26);
						 SBunkCode 	   = getBunkCode(BunkName);
						 SDriverCode   = getDriverCode(sDriver);

                         UpdateDate(VehicleRegNo,OutDate,OutTime,InDate,InTime,StartingKm,EndingKm,TotalKms,Place,Purpose,sDriver,SecurityName,DINo,KiNo,DIDate,KIDate,DQty,KQty,BunkName,FuelKm,ID,SBunkCode,Remarks,SType,SCard,SInCard,SDriverCode);
						 VEntryModel.removeRow(index);
                    }
                    VEntryModel.setValueAt(new Boolean(false),index,0);
               }
          }
          catch(Exception ex)
          {
                System.out.println(ex);
                ex.printStackTrace();
          }
     }
     private void setDelete()
     {
          Boolean bFlag;
          String ID            ="";

          try
          {
               for(int index=0; index<VEntryModel.getRows(); index++)
               {
                    ID            ="";
     
                    bFlag     = (Boolean)VEntryModel.getValueAt(index,21);

                    if(bFlag.booleanValue())
                    {
                         ID            =(String)VEntryModel.getValueAt(index,0);

                         DeleteData(ID);
                    }
                    VEntryModel.setValueAt(new Boolean(false),index,21);
               }
          }
          catch(Exception ex)
          {
                System.out.println(ex);
                ex.printStackTrace();
          }
     }
     public void DeleteData(String SID)
     {
          try
          {   
               String Qry=" delete from VEHICLES where ID="+SID+" ";
               if(theconnect==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               theStatement.executeUpdate(Qry);
               theStatement.close();
          }
          catch(Exception e1)
          {
               System.out.println(e1);
          }    
     }

     private void Check()
     {
          Boolean bFlag;
          int iStatus=0;
          String SStatus="";
          int i=0;

          for(int index=0;index<VEntryModel.getRows();index++)
          {
               bFlag     = (Boolean)VEntryModel.getValueAt(index,21);

               if(bFlag.booleanValue())
               {
                    VEntryModel.setValueAt(new Boolean(false),index,21);
               }  
          }
     }

     protected void setUpdateStatus(String QS)
     {
          showConfirmMsg();

          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               theStatement.executeUpdate(QS);
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
	 protected void setUpdateKm(String QS)
     {
//          showConfirmMsg();

          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               theStatement.executeUpdate(QS);
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     public void UpdateDate(String SVehicleRegNo,String SOutDate,String SOutTime,String SInDate,String SInTime,String SStartingKm,String SEndingKm,String STotalKms,String SPlace,String SPurpose,String SDriver,String SSecurityName,String SDINo,String SKiNo,String SDIDate,String SKIDate,String SDQty,String SKQty,String SBunkName,String SFuelKm,String SID, String SBunkCode,String Remarks,String SType,String SCard,String SInCard,String SDriverCode)
     {
          String QS ="";
		  String KmUpdate = "";
		  if(SType.equals("0"))  // Vehicle Out
		  {

          QS = " update VEHICLES set "+
               " VEHICLENO       = '"+SVehicleRegNo+"' ,"+
               " OUTDATE         = '"+common.pureDate(SOutDate)+"' ,"+
               " OUTTIME         = '"+SOutTime+"' ,"+
               " INDATE          = '"+common.pureDate(SInDate)+"',"+
               " INTIME          = '"+SInTime+"',"+
               " STKM            = '"+SStartingKm+"',"+
               " ENDKM           = '"+SEndingKm+"',"+
               " PLACE           = '"+SPlace+"',"+
               " PURPOSE         = '"+SPurpose+"',"+
               " SECURITYNAME    = '"+SSecurityName+"',"+
               " DINO            = '"+SDINo+"',"+
               " KINO            = '"+SKiNo+"',"+
               " DIDATE          = '"+common.pureDate(SDIDate)+"',"+
               " KIDATE          = '"+common.pureDate(SKIDate)+"',"+
               " DQTY            = '"+SDQty+"',"+
               " KQTY            = '"+SKQty+"',"+
               " BUNKNAME        = '"+SBunkName+"',"+
               " FUELKMS         = '"+SFuelKm+"',"+
			   " BUNKCODE        = '"+SBunkCode+"',"+
			   " REMARKS         = '"+Remarks+"',"+
			   " DRIVERUSERCODE  = '"+SDriverCode+"',"+
               " DriverName      = '"+SDriver+"' "+
			   " where VEHICLENO='"+SVehicleRegNo+"' and status=0 and id=(select max(id) from Vehicles where VEHICLECARDOUT='"+SID+"') ";
//             " where VEHICLENO='"+SVehicleRegNo+"' and VEHICLECARDOUT='"+SID+"' and status=0 and id=(select max(id) from Vehicles where VEHICLECARDOUT='"+SID+"') ";
			   setUpdateStatus(QS);
		}
		if(SType.equals("1")) // Vehicle In
		{
			QS = " update VEHICLES set ENDKM="+SEndingKm+" ,Status=1,VEHICLECARDIN ='"+SInCard+"' where VEHICLENO='"+SVehicleRegNo+"' and ID='"+SID+"' ";
			KmUpdate = " Update VehicleKm set EndKm = "+SEndingKm+" Where CardNo='"+SCard+"' ";
			setUpdateStatus(QS);
			setUpdateKm(KmUpdate);
			System.out.println(KmUpdate);
		}

         System.out.println(QS);
//         setUpdateStatus(QS);
     }
	 
     private void showConfirmMsg()
     {
          if((JOptionPane.showConfirmDialog(null,"Do U Want To Update Now? ","Warning",JOptionPane.YES_NO_OPTION)) == 1)
          {
//               DriverReader.freeze();
			   exitFrame();
          }
          else
          {
                JOptionPane.showMessageDialog(null," Ok Sucessfully Update","Confirm",JOptionPane.INFORMATION_MESSAGE);
//				DriverReader.freeze();
          }
     }
     public void exitFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.repaint();
               Layer.updateUI();
          }
          catch(Exception ed)
          {
               System.out.println(ed);
          }
     }
     private void selectVechicleNo()
     {
          VVechicleNo = new Vector();
          VDriverName = new Vector();
		  VDriverCode = new Vector(); 
          VPurpose    = new Vector();
          VPlaceName  = new Vector();
		  VBunkName = new Vector();
          VBunkCode = new Vector();

          try
          {
               JDBCConnection connect        =    JDBCConnection.getJDBCConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(getVechicleNoQS());

//               VDriverName.addElement("");
               VPurpose   .addElement("");
               VPlaceName .addElement("");

               while(theResult.next())
               {
                    VVechicleNo.addElement(theResult.getString(1));
               }
               theResult.close();

               theResult      =    theStatement.executeQuery(" Select EMPCODE,EMPNAME From staff where deptcode=48 order by EMPCODE ");
//               theResult      =    theStatement.executeQuery(" Select Name  from VehicleDrivers order by 1 ");

               while(theResult.next())
               {
                    VDriverCode.addElement(theResult.getString(1));
					VDriverName.addElement(theResult.getString(2));
               }
               theResult.close();

               theResult      =    theStatement.executeQuery(" Select Purpose  from VehiclePurpose Order By ID ");
               while(theResult.next())
               {
                    VPurpose.addElement(theResult.getString(1));
               }
               theResult.close();

               theResult      =    theStatement.executeQuery(" Select Place from VehiclePlaces order by Place ");
               while(theResult.next())
               {
                    VPlaceName.addElement(theResult.getString(1));
               }
               theResult.close();
			   
			   theResult      =    theStatement.executeQuery(" Select BunkName,BunkCode  From BunkMaster Order By 1 ");
			   while(theResult.next())
               {
                    VBunkName.addElement((theResult.getString(1)).toUpperCase());
                    VBunkCode.addElement(theResult.getString(2));
               }
			   theResult.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getVechicleNoQS()
     {
          String QS = " select distinct VEHICLENO from VEHICLEs order by 1";

          return QS;
     }
     private void selectVechicleNo1()
     {
          VSecName    = new Vector();

          try
          {
               JDBCConnection connect        =    JDBCConnection.getJDBCConnection();
               Connection     theConnect     =    connect.getConnection();
               Statement      theStatement   =    theConnect.createStatement();
               ResultSet      theResult      =    theStatement.executeQuery(getVechicleNoQS1());
               VSecName.addElement("");

               while(theResult.next())
               {
                    VSecName.addElement(theResult.getString(1));
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getVechicleNoQS1()
     {
          String QS = " select name from SecurityNames order by 1 ";

          return QS;
     }
	public int getVehicleStatus(String CardNO)
	  {
	  	 int icount = 0;
		 StringBuffer sb = new StringBuffer();
			sb.append(" Select count(*) from vehicleinfo where VEHICLECARDOUT = ? ");
			try {
				if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
			   java.sql.PreparedStatement ps = theConnection.prepareStatement(sb.toString());
										  ps . setString(1,CardNO.trim());
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){
											icount = rs1.getInt(1);
										  }
										  rs1.close();
										  ps.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		    return icount;
	  }
	 public String getKm(String card)
     {
          String Skm = "";
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =   theConnection.createStatement();
               String QS           = " Select EndKm from VehicleKm where CardNo = '"+card+"' ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    Skm = rs.getString(1);
               }
               st.close();
          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return Skm;
     }
	 public void insertVehicleValues()
     {
          Boolean bFlag;

          String VehicleRegNo  ="";
		  String VehicleName   ="";
          String OutDate       ="";
          String OutTime       ="";
          String InDate        ="";
          String InTime        ="";
          String StartingKm    ="";
          String EndingKm      ="";
          String TotalKms      ="";
          String Place         ="";
          String Purpose       ="";
          String sDriver       ="";
          String SecurityName  ="";
          String DINo          ="";
          String KiNo          ="";
          String DIDate        ="";
          String KIDate        ="";
          String DQty          ="";
          String KQty          ="";
          String BunkName      ="";
          String FuelKm        ="";
		  String Remarks       ="";
          String ID            ="";
		  String SCard 		   ="";
		  String SBunkCode     = "";
		  String SDriverCode   = "";

          try
          {
               for(int index=0; index<VEntryModel.getRows(); index++)
               {
                    VehicleRegNo  ="";
                    OutDate       ="";
                    OutTime       ="";
                    InDate        ="";
                    InTime        ="";
                    StartingKm    ="";
                    EndingKm      ="";
                    TotalKms      ="";
                    Place         ="";
                    Purpose       ="";
                    sDriver       ="";
                    SecurityName  ="";
                    DINo          ="";
                    KiNo          ="";
                    DIDate        ="";
                    KIDate        ="";
                    DQty          ="";
                    KQty          ="";
                    BunkName      ="";
                    FuelKm        ="";
                    ID            ="";
					Remarks       = "";
					VehicleName   = "";
					SCard         = "";
					SBunkCode     = "";
		  			SDriverCode   = "";
     
//                    bFlag     = (Boolean)VEntryModel.getValueAt(index,21);

//                    if(bFlag.booleanValue())
//                    {
                          ID           =(String)VEntryModel.getValueAt(index,24);
                         VehicleRegNo  =(String)VEntryModel.getValueAt(index,1);
						 VehicleName   =(String)VEntryModel.getValueAt(index,2);
                         OutDate       =(String)VEntryModel.getValueAt(index,3);
                         OutTime       =(String)VEntryModel.getValueAt(index,4);
                         InDate        =(String)VEntryModel.getValueAt(index,5);
                         InTime        =(String)VEntryModel.getValueAt(index,6);
                         StartingKm    =(String)VEntryModel.getValueAt(index,7);
                         EndingKm      =(String)VEntryModel.getValueAt(index,8);
                         TotalKms      =(String)VEntryModel.getValueAt(index,9);
                         Place         =(String)VEntryModel.getValueAt(index,10);
                         Purpose       =(String)VEntryModel.getValueAt(index,11);
                         sDriver       =(String)VEntryModel.getValueAt(index,12);
                         SecurityName  =(String)VEntryModel.getValueAt(index,13);
                         DINo          =(String)VEntryModel.getValueAt(index,14);
                         KiNo          =(String)VEntryModel.getValueAt(index,14);
                         DIDate        =(String)VEntryModel.getValueAt(index,16);
                         KIDate        =(String)VEntryModel.getValueAt(index,17);
                         DQty          =(String)VEntryModel.getValueAt(index,18);
                         KQty          =(String)VEntryModel.getValueAt(index,19);
                         BunkName      =(String)VEntryModel.getValueAt(index,20);
                         FuelKm        =(String)VEntryModel.getValueAt(index,21);
						 Remarks       =(String)VEntryModel.getValueAt(index,22);
						 SCard         =(String)VEntryModel.getValueAt(index,25);
						 SBunkCode 	   = getBunkCode(BunkName);
						 SDriverCode   = getDriverCode(sDriver);

                         InsertData(VehicleRegNo,VehicleName,OutDate,OutTime,InDate,InTime,StartingKm,EndingKm,TotalKms,Place,Purpose,sDriver,SecurityName,DINo,KiNo,DIDate,KIDate,DQty,KQty,BunkName,FuelKm,ID,SBunkCode,Remarks,SCard,SDriverCode);

//                    }
//                    VEntryModel.setValueAt(new Boolean(false),index,21);
               }
          }
          catch(Exception ex)
          {
                System.out.println(ex);
                ex.printStackTrace();
          }
     }
	 
     public void InsertData(String SVehicleRegNo,String VehicleName,String SOutDate,String SOutTime,String SInDate,String SInTime,String SStartingKm,String SEndingKm,String STotalKms,String SPlace,String SPurpose,String SDriver,String SSecurityName,String SDINo,String SKiNo,String SDIDate,String SKIDate,String SDQty,String SKQty,String SBunkName,String SFuelKm,String SID, String SBunkCode,String Remarks,String SCard,String SDriverCode)
     {
          String QS ="";
		  
		    QS  =  " Insert into Vehicles(CARDNO,VEHICLECARDOUT,VehicleNo,VehicleName,OutTime,Stkm,EndKm,place,Purpose,OutDate,InDate,"+
                   " InTime,DIno,Kino,Didate,kidate,Dqty,kqty,DriverName,SecurityName,Status,VCheckCard,BunkName,BunkCode, Remarks,  id,DRIVERUSERCODE) "+
                   " Values('"+SCard+"','"+SID+"','"+SVehicleRegNo+"','"+VehicleName+"','"+SOutTime+"', "+SStartingKm+", "+
                   " "+SEndingKm+",'"+SPlace+"','"+SPurpose+"',"+common.pureDate(SOutDate)+","+common.pureDate(SInDate)+", "+
                   " '"+SInTime+"','"+SDINo+"','"+SKiNo+"',"+common.pureDate(SDIDate)+","+common.pureDate(SKIDate)+","+
                   " "+SDQty+","+SKQty+",'"+SDriver+"','"+SSecurityName+"',0,1,'"+SBunkName+"','"+SBunkCode+"','"+Remarks+"',vehicles_seq.nextval,"+SDriverCode+" )";
         System.out.println(QS);
         setInsertStatus(QS);
     }
	protected void setInsertStatus(String QS)
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               theStatement.executeUpdate(QS);
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
	 private String getBunkCode(String SBunk)
     { 
          int iIndex=-1;
          iIndex = VBunkName.indexOf(SBunk);
          if(iIndex!=-1)
               return common.parseNull((String)VBunkCode.elementAt(iIndex));
          else
               return "";
     }
	private String getDriverCode(String SDriver)
    {
          int iIndex=-1;
          iIndex = VDriverName.indexOf(SDriver);
          if(iIndex!=-1)
               return common.parseNull((String)VDriverCode.elementAt(iIndex));
          else
               return "0";
     }
	 private boolean ValueIsNull(String SValue,int iRow,String SName) 
	 {
			if(SValue.equals(""))	 
			{
	 			JOptionPane.showMessageDialog(null,SName+ "");	
                theTable.setValueAt(new java.lang.Boolean(false),iRow,0);
				return false;
			}	
			else
			{
				return true;
			}
	 }
     private String getWarningMessage()
     {
          String str = "<html><body>";
          str = str + "<b>Pleas Check the Data.</b>"+"<br>";
          str = str + "Current Date and Prevoius Date Only<br>";
          str = str + "Entry<br>";
          str = str + "</body></html>";
          return str;
     }
}
// int currentValue = ((Integer) model.getValueAt(row, column)).intValue();
//  model.setValueAt(new Integer(currentValue + 1), row, column);