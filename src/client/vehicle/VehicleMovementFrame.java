package client.vehicle;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.*;

import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;

import blf.*;
import java.util.*;
import util.DateField;
import util.TimeField;
import util.MyLabel;
import util.MyComboBox;
import util.MyTextField;
import util.NameField;
import util.WholeNumberField;
import util.DateField1;
import util.Common;

public class VehicleMovementFrame extends JInternalFrame implements rndi.CodedNames
{
        JPanel leftPanel,topPanel,rightPanel,bottomPanel,middlePanel,totalPanel,indentPanel;
        JPanel buttonPanel,controlPanel;
        DateField1 DInDate,DOutDate,DDIndentDate,DKIndentDate;
        MyTextField TInTime,TOutTime,TTotalKm,TSecurityName,TDieselIndentNo,TKeroIndentNo,TCardNo,TVehicleCode,TRemarks;
        MyTextField TDQty,TKQty,TBunk;
        NameField NName,NPlace;
        WholeNumberField WAge;
        MyLabel LInTime,LVehicleName,LOutDate,LVehicleNo,LInDate,LStartingKm;
        JTabbedPane tab;
        MyComboBox JCDriverName,JCPurpose,JCSecurity,JCPlace,JCBunk;
        JTable infoTable;
        JScrollPane tableScroll;
        JButton BSave,BCancel,BExit;
        TimeField time = new TimeField();
        VehicleModel vehiclemodel;
        JLayeredPane layer;
        Vector VValues ;
        VehicleInfo VDomain;
        Common common   = new Common();
        JDialog theDialog;

        Vector VInfo = null;
        int iDate=0;

        String SVehicleNo="";
        int iStkm=0;
        Vector VBunkName,VBunkCode;

        public VehicleMovementFrame(JLayeredPane layer)
        {
                this.layer = layer;
                setDomain();
                setVectors();
                updateLookAndFeel();
                createComponents();
                setLayouts();
                addComponents();
                addListeners();
                setFalse();
        }
        private void setDomain()
        {
                try
                {
                        Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                        VDomain           = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
        }
        private void setFalse()
        {
                JCSecurity.setEnabled(false);
                JCBunk.setEnabled(false);

                TDieselIndentNo.setEditable(false);
                TKeroIndentNo.setEditable(false);
                DDIndentDate.setEditable(false);
                DKIndentDate.setEditable(false);
                TDQty.setEditable(false);
                TKQty.setEditable(false);
                TBunk.setEditable(false);
        }
        private void setTrue()
        {
                JCSecurity.setEnabled(true);
                JCBunk.setEnabled(true);
                TDieselIndentNo.setEditable(true);
                TKeroIndentNo.setEditable(true);
                DDIndentDate.setEditable(true);
                DKIndentDate.setEditable(true);
                TDQty.setEditable(true);
                TKQty.setEditable(true);
                TBunk.setEditable(true);
                
        }

        private void updateLookAndFeel()
        {
                String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
                try
                {
                        UIManager.setLookAndFeel(win);
                }
                catch(Exception e)
                {
			e.printStackTrace();
                }
	}
	private void createComponents()
	{
           try
           {

                   DInDate         = new DateField1();
                   DOutDate        = new DateField1();
                   DOutDate.setTodayDate();
                   DDIndentDate    = new DateField1();
                   DKIndentDate    = new DateField1();
                   TInTime         = new MyTextField();
                   TOutTime        = new MyTextField();
                   TVehicleCode    = new MyTextField();
                   NName           = new NameField();
                   NPlace          = new NameField();
               
                   tab             = new JTabbedPane();
                   TCardNo         = new MyTextField(7);
                   TTotalKm        = new MyTextField(20);
                   TDQty           = new MyTextField(5);
                   TKQty           = new MyTextField(5);
                   TSecurityName   = new MyTextField(30);
                   TDieselIndentNo = new MyTextField(20);
                   TKeroIndentNo   = new MyTextField(20);
                   TBunk           = new MyTextField(30);
                   TRemarks        = new MyTextField(30);
               
                   VValues         = new Vector();
               
                   LInTime         = new MyLabel("");
                   LVehicleNo      = new MyLabel("");
                   LVehicleName    = new MyLabel("");
                   LInDate         = new MyLabel("");
                   LOutDate        = new MyLabel("");
                   LStartingKm     = new MyLabel("");
               
                   BSave           = new JButton("Save");
                   BCancel         = new JButton("Cancel");
                   BExit           = new JButton("Exit");
               
                   controlPanel    = new JPanel();
                   buttonPanel     = new JPanel();
                   leftPanel       = new JPanel();
                   topPanel        = new JPanel();
                   rightPanel      = new JPanel();
                   middlePanel     = new JPanel();
                   bottomPanel     = new JPanel();
                   totalPanel      = new JPanel();
                   indentPanel     = new JPanel();
                              
                   JCDriverName    = new MyComboBox(VDomain.getDriverInfo());
                   JCPurpose       = new MyComboBox(VDomain.getPurpose());
                   JCSecurity      = new MyComboBox(VDomain.getSecurityInfo());
                   JCPlace         = new MyComboBox(VDomain.getPlaces());
                   JCBunk          = new MyComboBox(VBunkName);
               
                   String Date     = DOutDate.toNormal();
                   iDate       = common.toInt(common.pureDate(Date));
               
                   VInfo    = new Vector();
                   VInfo           = VDomain.getCurrentInfo(iDate);
                   vehiclemodel    = new VehicleModel(VInfo);
                   infoTable       = new JTable(vehiclemodel);
                   tableScroll     = new JScrollPane(infoTable);

           }
           catch(Exception e)
           {
                   e.printStackTrace();
           }
        }
        private void setLayouts()
        {
                leftPanel.setLayout(new GridLayout(5,2,1,1));
                leftPanel.setBorder(new TitledBorder("Vechicle Info"));
                rightPanel.setLayout(new GridLayout(6,2,1,1));
                rightPanel.setBorder(new TitledBorder("General Info"));
                controlPanel.setLayout(new GridLayout(2,1));
                //buttonPanel.setBorder(new TitledBorder("Control"));
                bottomPanel.setLayout(new BorderLayout());
                bottomPanel.setBorder(new TitledBorder("Details about Vehicles Currently went out"));
                topPanel.setLayout(new GridLayout(1,2));
                totalPanel.setLayout(new GridLayout(2,1));
                indentPanel.setLayout(new GridLayout(3,3,10,30));
                indentPanel.setBorder(new TitledBorder("Going for fill,diesel"));

        }
        
        private void addComponents()
        {

                buttonPanel.add(BSave);
                buttonPanel.add(BCancel);
                buttonPanel.add(BExit);

                BSave.setMnemonic('S');
                BExit.setMnemonic('X');
                BCancel.setMnemonic('C');

                leftPanel.add(new MyLabel("CardNo"));
                leftPanel.add(TCardNo);
                leftPanel.add(new MyLabel("Date"));
                leftPanel.add(DOutDate);

                TCardNo.addKeyListener(new keyEvents());

                leftPanel.add(new MyLabel("VehicleNO"));
                leftPanel.add(LVehicleNo);
                leftPanel.add(new MyLabel("VehicleName"));
                leftPanel.add(LVehicleName);
                leftPanel.add(new MyLabel(""));
                leftPanel.add(new MyLabel(""));

                rightPanel.add(new MyLabel(""));
                rightPanel.add(new MyLabel(""));

                //rightPanel.add(new MyLabel("CardNo"));
                //rightPanel.add(TCardNo);

                rightPanel.add(new MyLabel("DriverName"));
                rightPanel.add(JCDriverName);

                rightPanel.add(new MyLabel("Purpose"));
                rightPanel.add(JCPurpose);

                rightPanel.add(new MyLabel("Place"));
                rightPanel.add(JCPlace);


                rightPanel.add(new MyLabel("Total Km"));
                rightPanel.add(TTotalKm);

                rightPanel.add(new MyLabel("Remarks"));
                rightPanel.add(TRemarks);




                indentPanel.add(new MyLabel("SecurityName:"));
                //indentPanel.add(TSecurityName);
                indentPanel.add(JCSecurity);
                indentPanel.add(new MyLabel("Bunk Name:"));
//                indentPanel.add(TBunk);
                indentPanel.add(JCBunk);
                //indentPanel.add(new MyLabel(" "));
                //indentPanel.add(new MyLabel(" "));
                indentPanel.add(new MyLabel(" "));
                indentPanel.add(new MyLabel(" "));
                indentPanel.add(new MyLabel("DieselIndentNo:"));
                indentPanel.add(TDieselIndentNo);
                indentPanel.add(new MyLabel("DieselIndentDate:"));
                indentPanel.add(DDIndentDate);
                indentPanel.add(new MyLabel("Diesel QTY:"));
                indentPanel.add(TDQty);
                indentPanel.add(new MyLabel("KeroIndentNo:"));
                indentPanel.add(TKeroIndentNo);
                indentPanel.add(new MyLabel("KeroIndentDate:"));
                indentPanel.add(DKIndentDate);
                indentPanel.add(new MyLabel("Kero QTY:"));
                indentPanel.add(TKQty);


                bottomPanel.add(tableScroll);

                topPanel.add(leftPanel);
                topPanel.add(rightPanel);
                totalPanel.add(topPanel);
                totalPanel.add(indentPanel);
//                controlPanel.add(new JPanel());
                controlPanel.add(bottomPanel);
                controlPanel.add(buttonPanel);

                setTitle("Vehicle Movement Frame");
                setSize(800,520);
                setMaximizable(true);
                setIconifiable(true);
                setClosable(true);



                getContentPane().add(totalPanel,"North");
                getContentPane().add(controlPanel);
                setVisible(true);
                
                DOutDate.setEditable(false);
//                TCardNo.requestFocus();


        }

        private void addListeners()
        {
                BSave.addActionListener(new actionEvents());
                BCancel.addActionListener(new actionEvents());
                BExit.addActionListener(new actionEvents());

                JCPurpose.addActionListener(new actionEvents());
                //JCSecurity.addActionListener(new actionEvents());  
        }
        private class keyEvents extends KeyAdapter
        {
                public void keyPressed(KeyEvent ke)
                {
                        if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                        {

                                if(checkCardStatus())
                                {
                                        setPresets();
                                }
                                else
                                {

                                        JOptionPane.showMessageDialog(null,"This Vehicle Already Out");
                                        clearFields();
                                }
                        }
                }

        }

        private boolean checkCardStatus()
        {
                boolean flag=true;
                try
                {
                        String Scardno    = (String)TCardNo.getText();

                        int status = VDomain.getCheckCardStatus(Scardno);
                        if(status ==0)
                        {                                             
                                flag=false;
                        }
                }
                catch(Exception e)
                {
                        flag=false;
                }
                return flag;
        }

        private class actionEvents implements ActionListener
        {
                public void actionPerformed(ActionEvent ae)
                {
                        if(ae.getSource()==BSave)
                        {
                                boolean ok = checkFields();
                                if(ok)
                                {
//                                      TCardNo.requestFocus();
                                        insertValues(VValues);
                                        VValues.removeAllElements();
                                }
                                else
                                {

                                       JOptionPane.showMessageDialog(null,"All fields must be filled");
                                }

                        }

                        if(ae.getSource()==JCPurpose)
                        {
                         //System.out.println(JCPurpose.getSelectedItem());
                                if(JCPurpose.getSelectedIndex()==2)
                                {
                                        setTrue();
                                }
                                else
                                {
                                        setFalse();
                                }
                        }
                        if(ae.getSource()==BCancel)
                        {
                                clearFields();
                        }                                
                        if(ae.getSource()==BExit)
                        {
                                setVisible(false);
                        }
                }
        }

        private void  setCardFocus()
        {

               TCardNo.setText("");
               TCardNo.requestFocus();

        }
        private void clearFields()
        {
                setTrue();

                TCardNo.setText("");
                LVehicleNo.setText("");
                LVehicleName.setText("");
                JCDriverName.setSelectedIndex(0);
                JCPurpose.setSelectedIndex(0);
                TDieselIndentNo.setText("");
                TKeroIndentNo.setText("");
                DDIndentDate.setString("");
                DKIndentDate.setString("");
                TDQty.setText("");
                TKQty.setText("");
                LStartingKm.setText("");
                TBunk.setText("");
                TCardNo.requestFocus();
        }
                                       


        private void setPresets()
        {
                Vector v1       = new Vector();
                Vector v2       = new Vector();        
                try
                {
                        String scard    = (String)TCardNo.getText();
                   
                        v1              = VDomain.getInfo(scard,0);
                        v2              = VDomain.getKm(scard);

                        LVehicleNo.setText((String)v1.elementAt(0));
                        LVehicleName.setText((String) v1.elementAt(1));
                        //ikm= Integer.parseInt((String)v2.elementAt(1));
                        LStartingKm.setText((String)v2.elementAt(1));

                        SVehicleNo       = (String)v1.elementAt(0);
                        iStkm               = Integer.parseInt((String)v2.elementAt(1));
                }
                catch(Exception e)
                {
                        //JOptionPane.showMessageDialog(null,"This Vehicle Already Went Out");
                        e.printStackTrace();
                }
        }
        private boolean checkFields()
        {
                boolean flag=true;
                try
                {


                        String SCardNo = (String)TCardNo.getText();
        
                        if(SCardNo=="")
                                flag= false;
                        else
                        {
                                VValues.addElement(SCardNo);
                        }
        
                        String SVno     = common.parseNull((String)LVehicleNo.getText());
                        String SVname   = common.parseNull((String)LVehicleName.getText());
			String STotalKm = common.parseNull((String)TTotalKm.getText());
                        int ikm         = Integer.parseInt(STotalKm);
                        String SPurpose = (String)JCPurpose.getSelectedItem();
                        String SPlace   = (String)JCPlace.getSelectedItem();
			String SMoveDate= common.getCurrentDate();
 
			VValues.addElement( SVno);
                        VValues.addElement( SVname);
                        VValues.addElement( String.valueOf(ikm));
                        VValues.addElement( SPlace);
                        VValues.addElement( SPurpose);
			VValues.addElement( SMoveDate);

                        String sdate    = DOutDate.toNormal();
                        if(common.parseNull(sdate).equals(""))
                        {
                              flag = false;
                              return flag;
                        }
                        int iDate       = common.toInt(sdate);

                        String SBunk="";

                        if(JCPurpose.getSelectedIndex()==2)
                        {  
                                try
                                {
                                        setTrue();
                                        String SDINo=(String) TDieselIndentNo.getText();
                                        String SKINo=(String) TKeroIndentNo.getText();
                
                                        VValues.addElement(SDINo);
                                        VValues.addElement(SKINo);
                
                                        String SDiDate = (String) DDIndentDate.toNormal();
                                        String SKiDate = (String) DKIndentDate.toNormal();
                                       
//                                      SBunk   = TBunk.getText().trim();
                                        SBunk   = ((String)JCBunk.getSelectedItem()).trim();

                                        int iDidate    = common.toInt(common.pureDate(SDiDate));
                                        int iKidate    = common.toInt(common.pureDate(SKiDate));
                
                                        VValues.addElement(String.valueOf(iDidate));
                                        VValues.addElement(String.valueOf(iKidate));
                
                
                                        String Skqty   = (String) TKQty.getText();
	                                String Sdqty   = (String) TDQty.getText();
                
                                        double ikqty      = common.toDouble(Skqty);
	                                double idqty      = common.toDouble(Sdqty);

                                        VValues.addElement(String.valueOf(ikqty));
                                        VValues.addElement(String.valueOf(idqty));

                                        setFalse();
                                }
                                catch(Exception e)
                                {
                                        flag=false;
                                }
                        }
                        else
                        {
        		        VValues.addElement(" ");
                                VValues.addElement(" ");
                                SBunk   =" ";
                                VValues.addElement(String.valueOf(0));
                                VValues.addElement(String.valueOf(0));
                                VValues.addElement(String.valueOf(0));
				VValues.addElement(String.valueOf(0));
                        }        
        

/*
              String QS       =  " INSERT INTO VEHICLEDETAILS (CARDNO, VEHICLENO, VEHICLENAME, TOTALKMS, PLACE, PURPOSE, MOVEMENTDATE, "+
                                 " DINO, KINO, DIDATE, KIDATE, KQTY, DQTY, DRIVERNAME, SECURITYNAME, TRAVELLER, "+
                                 " STATUS, VCHECKCARD, SLIPNO, BUNKNAME, TANKFILLED, FUELKMS, ONCHECK, "+
                                 " ID, BUNKCODE, ACCSTATUS, ACCODE, REMARKS) "+
*/
                                String Sdriver   = (String) JCDriverName.getSelectedItem();
                                String Ssecurity = (String) JCSecurity.getSelectedItem();


                                //String SOutCardNo= (String) TCardNo.getText();
                                VValues.addElement(Sdriver);
                                VValues.addElement(Ssecurity);
                                VValues.addElement(" "); //Traveller
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                VValues.addElement(SBunk);
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                String SBunkCode = getBunkCode(SBunk);
                                VValues.addElement(SBunkCode);
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                VValues.addElement(common.parseNull(getNarration((String)TRemarks.getText())));

                }
                catch(Exception e)
                {
                        flag=false;
                        JOptionPane.showMessageDialog(null,"Incorrect Values");

                }
                return flag;
        }

     public String getNarration(String Str)
     {
          String SNarr="";

          StringTokenizer ST = new StringTokenizer(Str,"'");
          while(ST.hasMoreElements())
               SNarr =SNarr+ST.nextToken()+"''";

          if(SNarr.length()==0)
               return "";

          return SNarr.substring(0,SNarr.length()-2);
     }


        public void insertValues(Vector vect)
        {
                try
                {
                        VDomain.insertVehicleDetailsData(VValues);
			
                        if(SVehicleNo.equals("TN39J 7994"))
                        {
                           if((JOptionPane.showConfirmDialog(null,"Filled Kerosene?","Info",JOptionPane.YES_NO_OPTION))==0)
                           {
                                   FuelForLorryFrame mframe = new FuelForLorryFrame(iStkm,SVehicleNo,iDate);
                                   theDialog = new JDialog(mframe,"Fuel Dialog",true);
                                   theDialog.getContentPane().add(mframe.ModiPanel);
                                   theDialog.setBounds(80,100,350,250);
                                   theDialog.setVisible(true);
                           }     

                        }
			
                        JOptionPane.showMessageDialog(null,"Details Saved");
                        
                        clearFields();
                        Vector Vinfo    = new Vector();
                        Vinfo           = VDomain.getCurrentInfo(iDate);
                        //vehiclemodel    = new VehicleModel(Vinfo);                      
                        vehiclemodel.setVector1(Vinfo);

                        //setVisible(false);
                }
                catch(Exception e)        
                {
                        e.printStackTrace();
                }
        }
     private void setVectors()
     {
          VBunkName = new Vector();
          VBunkCode = new Vector();

          String QS1 = " Select BunkName,BunkCode  From BunkMaster Order By 1 ";
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VBunkName.addElement((theResult.getString(1)).toUpperCase());
                    VBunkCode.addElement(theResult.getString(2));
               }
               theResult.close();
           }catch(Exception ee){}
      }
     private String getBunkCode(String SBunk)
     {
          int iIndex=-1;
          iIndex = VBunkName.indexOf(SBunk);
          if(iIndex!=-1)
               return common.parseNull((String)VBunkCode.elementAt(iIndex));
          else
               return "";
     }

}
