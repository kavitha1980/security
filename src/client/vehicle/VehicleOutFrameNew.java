package client.vehicle;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.*;
import java.awt.Font;
import java.awt.Color;

import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;

import blf.*;
import java.util.*;
import util.DateField;
import util.TimeField;
import util.MyLabel;
import util.MyComboBox;
import util.MyTextField;
import util.NameField;
import util.WholeNumberField;
import util.DateField1;
import util.Common;
import guiutil.*;

public class VehicleOutFrameNew extends JInternalFrame implements rndi.CodedNames
{
        JPanel leftPanel,topPanel,rightPanel,bottomPanel,totalPanel,indentPanel;
        JPanel buttonPanel,controlPanel,placePanel,entryPanel,purposePanel;
		JTabbedPane theTab,PlaceTab;
        DateField1 DInDate,DOutDate,DDIndentDate,DKIndentDate;
        MyTextField TInTime,TOutTime,TStartingKm,TSecurityName,TDieselIndentNo,TKeroIndentNo,TCardNo,TVehicleCode,TRemarks;
        MyTextField TDQty,TKQty,TBunk;
        NameField NName,NPlace;
        WholeNumberField WAge;
        MyLabel LInTime,LOutTime,LVehicleName,LOutDate,LVehicleNo,LInDate,LStartingKm;
		MyLabel L1,L2,L3,L4,L5,L6,L7,L8,L9,L10,L11,L12 ;
		MyLabel LPP1,LPP2,LPP3,LPP4,LPP5,LPP6,LPP7,LPP8,LPP9,LPP10,LPP11,LPP12;
        JTabbedPane tab;
        MyComboBox JCDriverName,JCPurpose,JCSecurity,JCPlace,JCBunk;
        JTable infoTable;
        JScrollPane tableScroll;
        JButton BSave,BCancel,BExit;
        TimeField time = new TimeField();
        VehicleModel vehiclemodel;
        JLayeredPane layer;
        Vector VValues ;
        VehicleInfo VDomain;
        Common common   = new Common();
        JDialog theDialog;
		Font font = new Font("Times New Roman", Font.BOLD, 20);
		Font PurposeFont = 	new Font("Times New Roman", Font.BOLD, 18);
		Font PlaceFont =   new Font("Times New Roman", Font.BOLD, 18);

        Vector VInfo = null;
        int iDate=0;

        String SVehicleNo="";
        int iStkm=0;
        Vector VBunkName,VBunkCode;
        public VehicleOutFrameNew(JLayeredPane layer)
        {
                this.layer = layer;
                setDomain();
                setVectors();
                updateLookAndFeel();
                createComponents();
                setLayouts();
                addComponents();
                addListeners();
                setFalse();
        }
        private void setDomain()
        {
                try
                {
                        Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                        VDomain           = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                }
        }
        private void setFalse()
        {
                JCSecurity.setEnabled(false);
                JCBunk.setEnabled(false);

                TDieselIndentNo.setEditable(false);
                TKeroIndentNo.setEditable(false);
                DDIndentDate.setEditable(false);
                DKIndentDate.setEditable(false);
                TDQty.setEditable(false);
                TKQty.setEditable(false);
                TBunk.setEditable(false);
        }
        private void setTrue()
        {
                JCSecurity.setEnabled(true);
                JCBunk.setEnabled(true);
                TDieselIndentNo.setEditable(true);
                TKeroIndentNo.setEditable(true);
                DDIndentDate.setEditable(true);
                DKIndentDate.setEditable(true);
                TDQty.setEditable(true);
                TKQty.setEditable(true);
                TBunk.setEditable(true);
        }
        private void updateLookAndFeel()
        {
                String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
                try
                {
                        UIManager.setLookAndFeel(win);
                }
                catch(Exception e)
                {
			e.printStackTrace();
                }
	}
	private void createComponents()
	{
           try
           {
                   DInDate         = new DateField1();
                   DOutDate        = new DateField1();
                   DOutDate.setTodayDate();
                   DDIndentDate    = new DateField1();
                   DKIndentDate    = new DateField1();
                   TInTime         = new MyTextField();
                   TOutTime        = new MyTextField();
                   TVehicleCode    = new MyTextField();
                   NName           = new NameField();
                   NPlace          = new NameField();
               
                   tab             = new JTabbedPane();
                   TCardNo         = new MyTextField(7);
                   TStartingKm     = new MyTextField(20);
                   TDQty           = new MyTextField(5);
                   TKQty           = new MyTextField(5);
                   TSecurityName   = new MyTextField(30);
                   TDieselIndentNo = new MyTextField(20);
                   TKeroIndentNo   = new MyTextField(20);
                   TBunk           = new MyTextField(30);
                   TRemarks        = new MyTextField(30);
               
                   VValues         = new Vector();
				   theTab          =  new JTabbedPane();
				   PlaceTab		   =  new JTabbedPane();
               
                   LInTime         = new MyLabel("");
                   LVehicleNo      = new MyLabel("");
                   LVehicleName    = new MyLabel("");
                   LInDate         = new MyLabel("");
                   LOutDate        = new MyLabel("");
                   LOutTime        = new MyLabel("");
                   LStartingKm     = new MyLabel("");
				   
				   L1 			   = new MyLabel("NAMBIYUR");
				   L2			   = new MyLabel("NADUPALAYAM");
				   L3			   = new MyLabel("INSIDE");
				   L4			   = new MyLabel("ARASUR");
				   L5			   = new MyLabel("KOODAKARAI");
				   L6			   = new MyLabel("KOOKALORE");
				   L7			   = new MyLabel("KONNANMODAI");
				   L8			   = new MyLabel("KOLAPPLURE");
				   L9			   = new MyLabel("GOBI");
				   L10			   = new MyLabel("UPPUKARAPALLAM");
				   L11			   = new MyLabel("OTTARKARATTUPALAYAM");
				   L12			   = new MyLabel("ERODE");
				   
				   
				   LPP1 		   = new MyLabel("TO DROP WORKERS");
				   LPP2			   = new MyLabel("TO PICK UP WORKERS");
				   LPP3			   = new MyLabel("TO FILL FUEL");
				   LPP4			   = new MyLabel("TO WATER");
				   LPP5			   = new MyLabel("TO PURCHASE GOODS");
				   LPP6			   = new MyLabel("TO BANK");
				   LPP7			   = new MyLabel("TO DELIEVER YARNS");
				   LPP8			   = new MyLabel("TO PURCHASE YARNS");
				   LPP9			   = new MyLabel("FOR DYEING");
				   LPP10		   = new MyLabel("TO DYEING UNIT");
				   LPP11		   = new MyLabel("TO SHOPPING");
				   LPP12		   = new MyLabel("TO HEAD OFFICE");
               
                   LOutTime.setText(time.getTimeNow());
               
                   BSave           = new JButton("Save");
                   BCancel         = new JButton("Cancel");
                   BExit           = new JButton("Exit");
               
                   controlPanel    = new JPanel();
                   buttonPanel     = new JPanel();
                   leftPanel       = new JPanel();
                   topPanel        = new JPanel();
                   rightPanel      = new JPanel();                   
                   bottomPanel     = new JPanel();
                   totalPanel      = new JPanel();
                   indentPanel     = new JPanel();				   
				   placePanel      = new JPanel();
				   entryPanel	   = new JPanel();
				   purposePanel	   = new JPanel();
                              
                   JCDriverName    = new MyComboBox(VDomain.getDriverInfo());
                   JCPurpose       = new MyComboBox(VDomain.getPurpose());
                   JCSecurity      = new MyComboBox(VDomain.getSecurityInfo());
                   JCPlace         = new MyComboBox(VDomain.getPlaces());
                   JCBunk          = new MyComboBox(VBunkName);
				   
				   JCDriverName	   . setFont(font);
				   JCPurpose	   . setFont(PurposeFont);
				   JCPlace		   . setFont(font);
               
                   String Date     = DOutDate.toNormal();
                   iDate       = common.toInt(common.pureDate(Date));
               
                   VInfo    = new Vector();
                   VInfo           = VDomain.getCurrentInfo(iDate);
                   vehiclemodel    = new VehicleModel(VInfo);
                   infoTable       = new JTable(vehiclemodel);
                   tableScroll     = new JScrollPane(infoTable);
           }
           catch(Exception e)
           {
                   e.printStackTrace();
           }
        }
        private void setLayouts()
        {
                leftPanel.setLayout(new GridLayout(5,2));
                leftPanel.setBorder(new TitledBorder("Vechicle Info"));
                rightPanel.setLayout(new GridLayout(6,2));
                rightPanel.setBorder(new TitledBorder("General Info"));
				placePanel.setLayout(new GridLayout(6,2));
				placePanel.setBorder(new TitledBorder("Select Place"));
				purposePanel.setLayout(new GridLayout(6,2));
				purposePanel.setBorder(new TitledBorder("Select Place"));
                controlPanel.setLayout(new GridLayout(2,1));     
                bottomPanel.setLayout(new BorderLayout());
                bottomPanel.setBorder(new TitledBorder("Details about Vehicles Currently went out"));
                topPanel.setLayout(new GridLayout(1,3));
                totalPanel.setLayout(new GridLayout(3,1));				
                indentPanel.setLayout(new GridLayout(4,4,10,30));
                indentPanel.setBorder(new TitledBorder("Going for fill,diesel"));
				entryPanel.setLayout(new GridLayout(3,1));	
				/*SCLayout          midlayout   =  new guiutil.SCLayout(2, guiutil.SCLayout.FILL, guiutil.SCLayout.FILL,2);
                midlayout   	.  setScale(0,0.90);
                midlayout   	.  setScale(1,0.10);
                totalPanel  	.  setLayout(midlayout);
                controlPanel	.  setLayout(midlayout);

				 setBounds      (10,20,800,800);
				 setVisible     (true);
				 setClosable    (true);
				 setMaximizable (true);
				 setIconifiable (true);
				 setResizable   (true);		*/
        }        
        private void addComponents()
        {
                //buttonPanel.add(BSave);
                //buttonPanel.add(BCancel);
                //buttonPanel.add(BExit);

                BSave.setMnemonic('S');
                BExit.setMnemonic('X');
                BCancel.setMnemonic('C');

                leftPanel.add(new MyLabel("CardNo"));
                leftPanel.add(TCardNo);
                leftPanel.add(new MyLabel("Date"));
                leftPanel.add(DOutDate);

                TCardNo.addKeyListener(new keyEvents());

                leftPanel.add(new MyLabel("VehicleNO"));
                leftPanel.add(LVehicleNo);
                leftPanel.add(new MyLabel("VehicleName"));
                leftPanel.add(LVehicleName);
                leftPanel.add(new MyLabel(""));
                leftPanel.add(new MyLabel(""));
                
                rightPanel.add(new MyLabel("OutTime"));
                rightPanel.add(LOutTime);
                
                rightPanel.add(new MyLabel("DriverName"));
                rightPanel.add(JCDriverName);

                rightPanel.add(new MyLabel("Purpose"));
                rightPanel.add(JCPurpose);

                rightPanel.add(new MyLabel("Place"));
                rightPanel.add(JCPlace);


                rightPanel.add(new MyLabel("Starting Km"));
                rightPanel.add(LStartingKm);

                rightPanel.add(new MyLabel("Remarks"));
                rightPanel.add(TRemarks);
								
				 L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L1	.	setFont(PlaceFont);
				 placePanel.add(L1);
				 L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L2	.	setFont(PlaceFont);
				 placePanel.add(L2);
				 L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L3	.	setFont(PlaceFont);
				 placePanel.add(L3);
				 L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L4	.	setFont(PlaceFont);
				 placePanel.add(L4);
				 L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L5	.	setFont(PlaceFont);
				 placePanel.add(L5);
				 L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L6	.	setFont(PlaceFont);
				 placePanel.add(L6);
				 L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L7	.	setFont(PlaceFont);
				 placePanel.add(L7);
				 L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L8	.	setFont(PlaceFont);
				 placePanel.add(L8);
				 L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L9	.	setFont(PlaceFont);
				 placePanel.add(L9);
				 L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L10	.	setFont(PlaceFont);
				 placePanel.add(L10);
				 L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L11	.	setFont(PlaceFont);
				 placePanel.add(L11);
				 L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				 L12	.	setFont(PlaceFont);
				 placePanel.add(L12);
				 
				 
				 // Start To add Purpose lables 
				 LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP1	.	setFont(PlaceFont);
				 purposePanel.add(LPP1);
				 LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP2	.	setFont(PlaceFont);
				 purposePanel.add(LPP2);
				 LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP3	.	setFont(PlaceFont);
				 purposePanel.add(LPP3);
				 LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP4	.	setFont(PlaceFont);
				 purposePanel.add(LPP4);
				 LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP5	.	setFont(PlaceFont);
				 purposePanel.add(LPP5);
				 LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP6	.	setFont(PlaceFont);
				 purposePanel.add(LPP6);
				 LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP7	.	setFont(PlaceFont);
				 purposePanel.add(LPP7);
				 LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP8	.	setFont(PlaceFont);
				 purposePanel.add(LPP8);
				 LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP9	.	setFont(PlaceFont);
				 purposePanel.add(LPP9);
				 LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP10	.	setFont(PlaceFont);
				 purposePanel.add(LPP10);
				 LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP11	.	setFont(PlaceFont);
				 purposePanel.add(LPP11);
				 LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				 LPP12	.	setFont(PlaceFont);
				 purposePanel.add(LPP12);
				 //End To add Purpose lables
				 			
				

                indentPanel.add(new MyLabel("SecurityName:"));
                indentPanel.add(JCSecurity);
                indentPanel.add(new MyLabel("Bunk Name:"));
                indentPanel.add(JCBunk);
                indentPanel.add(new MyLabel(" "));
                indentPanel.add(new MyLabel(" "));
                indentPanel.add(new MyLabel("DieselIndentNo:"));
                indentPanel.add(TDieselIndentNo);
                indentPanel.add(new MyLabel("DieselIndentDate:"));
                indentPanel.add(DDIndentDate);
                indentPanel.add(new MyLabel("Diesel QTY:"));
                indentPanel.add(TDQty);
                indentPanel.add(new MyLabel("KeroIndentNo:"));
                indentPanel.add(TKeroIndentNo);
                indentPanel.add(new MyLabel("KeroIndentDate:"));
                indentPanel.add(DKIndentDate);
                indentPanel.add(new MyLabel("Kero QTY:"));
                indentPanel.add(TKQty);
				
				/*indentPanel.add(new MyLabel(" "));
				indentPanel.add(new MyLabel(" "));
				indentPanel.add(new MyLabel(" "));
				indentPanel.add(new MyLabel(" "));
				indentPanel.add(new MyLabel(" "));
				indentPanel.add(new MyLabel(" "));*/
				indentPanel.add(new MyLabel(" "));
				indentPanel.add(BSave);
                indentPanel.add(BCancel);
                indentPanel.add(BExit);


                bottomPanel.add(tableScroll);

                topPanel.add(leftPanel);
				topPanel.add(PlaceTab);
                topPanel.add(rightPanel);
				
                totalPanel.add(topPanel);				
                totalPanel.add(indentPanel);
				//totalPanel.add(buttonPanel);
                controlPanel.add(bottomPanel);
                				
				
				theTab.addTab("Vehicles Entry",totalPanel);
      			theTab.addTab("Vehicles Currently Went Out" ,controlPanel);
				entryPanel.add(theTab);
				
				PlaceTab.addTab("Place",placePanel);
      			PlaceTab.addTab("Purpose" ,purposePanel);
				
				
                setTitle("Vehicle Out Frame (13.0)");
                setSize(1200,680);
                setMaximizable(true);
                setIconifiable(true);
                setClosable(true);
				setResizable(false);

			    getContentPane().add("North",entryPanel);
                setVisible(true);
                
                DOutDate.setEditable(false);
        }
        private void addListeners()
        {
                L1.addMouseListener(new MouseList());
				L2.addMouseListener(new MouseList());
				L3.addMouseListener(new MouseList());
				L4.addMouseListener(new MouseList());
				L5.addMouseListener(new MouseList());
				L6.addMouseListener(new MouseList());
				L7.addMouseListener(new MouseList());
				L8.addMouseListener(new MouseList());
				L9.addMouseListener(new MouseList());
				L10.addMouseListener(new MouseList());
				L11.addMouseListener(new MouseList());
				L12.addMouseListener(new MouseList());
				
				LPP1.addMouseListener(new MouseListPurpose());
				LPP2.addMouseListener(new MouseListPurpose());
				LPP3.addMouseListener(new MouseListPurpose());
				LPP4.addMouseListener(new MouseListPurpose());
				LPP5.addMouseListener(new MouseListPurpose());
				LPP6.addMouseListener(new MouseListPurpose());
				LPP7.addMouseListener(new MouseListPurpose());
				LPP8.addMouseListener(new MouseListPurpose());
				LPP9.addMouseListener(new MouseListPurpose());
				LPP10.addMouseListener(new MouseListPurpose());
				LPP11.addMouseListener(new MouseListPurpose());
				LPP12.addMouseListener(new MouseListPurpose());
				
				
				BSave.addActionListener(new actionEvents());
                BCancel.addActionListener(new actionEvents());
                BExit.addActionListener(new actionEvents());

                JCPurpose.addActionListener(new actionEvents());
        }
        private class keyEvents extends KeyAdapter
        {
                public void keyPressed(KeyEvent ke)
                {
                        if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                        {

                                if(checkCardStatus())
                                {
                                        setPresets();
                                }
                                else
                                {
                                        JOptionPane.showMessageDialog(null,"This Vehicle Already Out");
                                        clearFields();
                                }
                        }
                }
        }
        private boolean checkCardStatus()
        {
                boolean flag=true;
                try
                {
                        String Scardno    = (String)TCardNo.getText();

                        int status = VDomain.getCheckCardStatus(Scardno);
                        if(status ==0)
                        {                                             
                                flag=false;
                        }
                }
                catch(Exception e)
                {
                        flag=false;
                }
                return flag;
        }
        private class actionEvents implements ActionListener
        {
                public void actionPerformed(ActionEvent ae)
                {
                        if(ae.getSource()==BSave)
                        {
                                boolean ok = checkFields();
                                if(ok)
                                {
//                                      TCardNo.requestFocus();
                                        insertValues(VValues);
                                        VValues.removeAllElements();
                                }
                                else
                                {

                                       JOptionPane.showMessageDialog(null,"All fields must be filled");
                                }
                        }

                        if(ae.getSource()==JCPurpose)
                        {
                         //System.out.println(JCPurpose.getSelectedItem());
                                if(JCPurpose.getSelectedIndex()==2)
                                {
                                        setTrue();
                                }
                                else
                                {
                                        setFalse();
                                }
                        }
                        if(ae.getSource()==BCancel)
                        {
                                clearFields();
                        }                                
                        if(ae.getSource()==BExit)
                        {
                                setVisible(false);
                        }
                }
        }
        private void  setCardFocus()
        {

               TCardNo.setText("");
               TCardNo.requestFocus();

        }
        private void clearFields()
        {
                setTrue();

                TCardNo.setText("");
                LVehicleNo.setText("");
                LVehicleName.setText("");
                JCDriverName.setSelectedIndex(0);
                JCPurpose.setSelectedIndex(0);
                TDieselIndentNo.setText("");
                TKeroIndentNo.setText("");
                DDIndentDate.setString("");
                DKIndentDate.setString("");
                TDQty.setText("");
                TKQty.setText("");
                LStartingKm.setText("");
                TBunk.setText("");
                TCardNo.requestFocus();
        }              
        private void setPresets()
        {
                Vector v1       = new Vector();
                Vector v2       = new Vector();        
                try
                {
                        String scard    = (String)TCardNo.getText();
                   
                        v1              = VDomain.getInfo(scard,0);
                        v2              = VDomain.getKm(scard);

                        LVehicleNo.setText((String)v1.elementAt(0));
                        LVehicleName.setText((String) v1.elementAt(1));
                        LOutTime.setText(time.getTimeNow());
                        //ikm= Integer.parseInt((String)v2.elementAt(1));
                        LStartingKm.setText((String)v2.elementAt(1));

                        SVehicleNo       = (String)v1.elementAt(0);
                        iStkm               = Integer.parseInt((String)v2.elementAt(1));
						
						System.out.println("==>Stkm==>"+iStkm);


                }
                catch(Exception e)
                {
                        //JOptionPane.showMessageDialog(null,"This Vehicle Already Went Out");
                        e.printStackTrace();
                }
        }
        private boolean checkFields()
        {
                boolean flag=true;
                try
                {

                        String SCardNo = (String)TCardNo.getText();
        
                        if(SCardNo=="")
                                flag= false;
                        else
                        {
                                VValues.addElement(SCardNo);
                        }
        
                        String SVno     = common.parseNull((String)LVehicleNo.getText());
                        String SVname   = common.parseNull((String)LVehicleName.getText());
                        String SOutTime = common.parseNull((String)LOutTime.getText());
                        String Skm      = common.parseNull((String)LStartingKm.getText());
                        int ikm         = Integer.parseInt(Skm);
                        String SPurpose = (String)JCPurpose.getSelectedItem();
                        String SPlace   = (String)JCPlace.getSelectedItem();						


                        VValues.addElement( SVno);
                        VValues.addElement( SVname);
                        VValues.addElement( SOutTime);
                        VValues.addElement( String.valueOf(ikm));
                        VValues.addElement(String.valueOf(0));
                        VValues.addElement(SPlace);
                        VValues.addElement( SPurpose);
                        String sdate    = DOutDate.toNormal();
                        if(common.parseNull(sdate).equals(""))
                        {
                              flag = false;
                              return flag;
                        }
                        int iDate       = common.toInt(sdate);

                        VValues.addElement(String.valueOf(iDate));
                        VValues.addElement(String.valueOf(iDate));
        
                        VValues.addElement(common.parseNull((String)LOutTime.getText()));
                        String SBunk="";
                        if(JCPurpose.getSelectedIndex()==2)
                        {  
                                try
                                {
                                        setTrue();
                                        String SDINo=(String) TDieselIndentNo.getText();
                                        String SKINo=(String) TKeroIndentNo.getText();
                
                                        VValues.addElement(SDINo);
                                        VValues.addElement(SKINo);
                
                                        String SDiDate = (String) DDIndentDate.toNormal();
                                        String SKiDate = (String) DKIndentDate.toNormal();
                                       
//                                      SBunk   = TBunk.getText().trim();
                                        SBunk   = ((String)JCBunk.getSelectedItem()).trim();

                                        int iDidate    = common.toInt(common.pureDate(SDiDate));
                                        int iKidate    = common.toInt(common.pureDate(SKiDate));
                
                                        VValues.addElement(String.valueOf(iDidate));
                                        VValues.addElement(String.valueOf(iKidate));
                
                                        String Sdqty   = (String) TDQty.getText();
                                        String Skqty   = (String) TKQty.getText();
                
                                        double idqty      = common.toDouble(Sdqty);
                                        double ikqty      = common.toDouble(Skqty);
                
                                        VValues.addElement(String.valueOf(idqty));
                                        VValues.addElement(String.valueOf(ikqty));
                                        setFalse();
                                }
                                catch(Exception e)
                                {
                                        flag=false;
                                }
                        }
                        else
                        {
        
                                VValues.addElement(" ");
                                VValues.addElement(" ");

                                SBunk   =" ";
                                VValues.addElement(String.valueOf(0));
                                VValues.addElement(String.valueOf(0));
                                VValues.addElement(String.valueOf(0));
                                VValues.addElement(String.valueOf(0));
                        }                                                
                                String Sdriver   = (String) JCDriverName.getSelectedItem();
                                String Ssecurity = (String) JCSecurity.getSelectedItem();
                                //String SOutCardNo= (String) TCardNo.getText();
                                VValues.addElement(Sdriver);
                                VValues.addElement(Ssecurity);
                                VValues.addElement(" ");
                                VValues.addElement(" ");
                                VValues.addElement(SBunk);
                                String SBunkCode = getBunkCode(SBunk);
                                VValues.addElement(SBunkCode);
//                                VValues.addElement(common.parseNull((String)TRemarks.getText()));
                                VValues.addElement(common.parseNull(getNarration((String)TRemarks.getText())));

                }
                catch(Exception e)
                {
                        flag=false;
                        JOptionPane.showMessageDialog(null,"Incorrect Values");

                }
                return flag;
        }

     public String getNarration(String Str)
     {
          String SNarr="";

          StringTokenizer ST = new StringTokenizer(Str,"'");
          while(ST.hasMoreElements())
               SNarr =SNarr+ST.nextToken()+"''";

          if(SNarr.length()==0)
               return "";

          return SNarr.substring(0,SNarr.length()-2);
     }


        public void insertValues(Vector vect)
        {
                try
                {
                        VDomain.insertData(VValues);

                        if(SVehicleNo.equals("TN39J 7994"))
                        {
                           if((JOptionPane.showConfirmDialog(null,"Filled Kerosene?","Info",JOptionPane.YES_NO_OPTION))==0)
                           {
                                   FuelForLorryFrame mframe = new FuelForLorryFrame(iStkm,SVehicleNo,iDate);
                                   theDialog = new JDialog(mframe,"Fuel Dialog",true);
                                   theDialog.getContentPane().add(mframe.ModiPanel);
                                   theDialog.setBounds(80,100,350,250);
                                   theDialog.setVisible(true);
                           }     

                        }
                        JOptionPane.showMessageDialog(null,"Details Saved");
                        
                        clearFields();
                        Vector Vinfo    = new Vector();
                        Vinfo           = VDomain.getCurrentInfo(iDate);
                        //vehiclemodel    = new VehicleModel(Vinfo);
                        vehiclemodel.setVector1(Vinfo);

                        //setVisible(false);
                }
                catch(Exception e)        
                {
                        e.printStackTrace();
                }
        }
     private void setVectors()
     {
          VBunkName = new Vector();
          VBunkCode = new Vector();

          String QS1 = " Select BunkName,BunkCode  From BunkMaster Order By 1 ";
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection  = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VBunkName.addElement((theResult.getString(1)).toUpperCase());
                    VBunkCode.addElement(theResult.getString(2));
               }
               theResult.close();
           }catch(Exception ee){}
      }
     private String getBunkCode(String SBunk)
     {
          int iIndex=-1;
          iIndex = VBunkName.indexOf(SBunk);
          if(iIndex!=-1)
               return common.parseNull((String)VBunkCode.elementAt(iIndex));
          else
               return "";
     }
	 private class MouseList extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
		  	String ss= "";
		  	if (me.getSource() == L1)
			{
				ss	= (String)L1.getText();
				L1	. 	setForeground(new java.awt.Color(86,239,20));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);
				String SPlace   = (String)JCPlace.getSelectedItem();						
				//System.out.println("==Select Place==="+SPlace);
				//System.out.println("sssssssssss"+ss);
			}
			if (me.getSource() == L2)
			{
				ss	= (String)L2.getText();
				L2	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L3)
			{
				ss	= (String)L3.getText();
				L3	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L4)
			{
				ss	= (String)L4.getText();
				L4	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));				
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L5)
			{
				ss	= (String)L5.getText();
				L5	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));				
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L6)
			{
				ss	= (String)L6.getText();
				L6	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L7)
			{
				ss	= (String)L7.getText();
				L7	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L8)
			{
				ss	= (String)L8.getText();
				L8	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L9)
			{
				ss	= (String)L9.getText();
				L9	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L10)
			{
				ss	= (String)L10.getText();
				L10	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L11)
			{
				ss	= (String)L11.getText();
				L11	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
			if (me.getSource() == L12)
			{
				ss	= (String)L12.getText();
				L12	. 	setForeground(new java.awt.Color(86,239,20));
				L1	. 	setForeground(new java.awt.Color(51,50,250,250));
				L2	. 	setForeground(new java.awt.Color(51,50,250,250));
				L3	. 	setForeground(new java.awt.Color(51,50,250,250));
				L4	. 	setForeground(new java.awt.Color(51,50,250,250));
				L5	. 	setForeground(new java.awt.Color(51,50,250,250));
				L6	. 	setForeground(new java.awt.Color(51,50,250,250));
				L7	. 	setForeground(new java.awt.Color(51,50,250,250));
				L8	. 	setForeground(new java.awt.Color(51,50,250,250));
				L9	. 	setForeground(new java.awt.Color(51,50,250,250));
				L10	. 	setForeground(new java.awt.Color(51,50,250,250));
				L11	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPlace . setSelectedItem(ss);				
			}
		  }
	 }	  
	 /*  Mouse Listner For The Purpose Lable Selection */
	 private class MouseListPurpose extends MouseAdapter
     {
          public void mouseReleased(MouseEvent me)
          {
		  	String ss= "";
		  	if (me.getSource() == LPP1)
			{
				ss	= (String)LPP1.getText();
				LPP1	. 	setForeground(new java.awt.Color(86,239,20));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);
				String SPlace   = (String)JCPurpose.getSelectedItem();						
				//System.out.println("==Select Place==="+SPlace);
				//System.out.println("sssssssssss"+ss);
			}
			if (me.getSource() == LPP2)
			{
				ss	= (String)LPP2.getText();
				LPP2	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP3)
			{
				ss	= (String)LPP3.getText();
				LPP3	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP4)
			{
				ss	= (String)LPP4.getText();
				LPP4	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));				
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP5)
			{
				ss	= (String)LPP5.getText();
				LPP5	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));				
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP6)
			{
				ss	= (String)LPP6.getText();
				LPP6	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP7)
			{
				ss	= (String)LPP7.getText();
				LPP7	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP8)
			{
				ss	= (String)LPP8.getText();
				LPP8	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP9)
			{
				ss	= (String)LPP9.getText();
				LPP9	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP10)
			{
				ss	= (String)LPP10.getText();
				LPP10	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP11)
			{
				ss	= (String)LPP11.getText();
				LPP11	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP12	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
			if (me.getSource() == LPP12)
			{
				ss	= (String)LPP12.getText();
				LPP12	. 	setForeground(new java.awt.Color(86,239,20));
				LPP1	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP2	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP3	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP4	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP5	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP6	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP7	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP8	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP9	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP10	. 	setForeground(new java.awt.Color(51,50,250,250));
				LPP11	. 	setForeground(new java.awt.Color(51,50,250,250));
				JCPurpose . setSelectedItem(ss);				
			}
		  }
	 }

}