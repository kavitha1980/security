package client.vehicle;

import java.sql.*;
import domain.jdbc.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.sql.*;
import java.sql.Date.*;
import java.util.*;
import java.io.*;
import util.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTableEvent;

import java.io.FileOutputStream;



public class VehiclesMovementDetails extends JInternalFrame
{
    protected       				JLayeredPane Layer;
	JPanel          				TopPanel,BottomPanel,MiddlePanel;
	DateField       				TFromDate,TToDate;
	JButton         				BApply,BExit,BPdf;
	JTable          				theTable;
    Common 							common ;
	VehiclesMovementDetailsModel 	theModel;
	Connection 						theConnection = null;
	Connection 						theDisConnection = null;
	ArrayList						ADataList;
	byte[] imgbytes=null;
	int iCnt=1;
	int iImageCount;
	
	String   SFileName, SFileOpenPath, SFileWritePath;
    Document document;
    PdfPTable table;
    PdfPCell c1;
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN",8, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);
    
    int iWidth[] = {12,20,12,20};

    int iTotalColumns = 4;
	
	 public VehiclesMovementDetails(JLayeredPane Layer)
    {
        this.Layer = Layer;
        
        try
        {
			
            common  = new Common();
            createComponents();
            setLayouts();
            addComponents();
            addListener();
			setTableData();
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
    }
	
	private void createComponents()
	{
		TopPanel 	= new JPanel();
		BottomPanel	= new JPanel();
		MiddlePanel	= new JPanel();
		
		//TFromDate = new DateField();
		//TToDate	  = new DateField();
		
		//BApply	  = new JButton("Apply");
		BExit	  = new JButton("Exit");
		BPdf	  = new JButton("PDF");
		
		theModel            = new VehiclesMovementDetailsModel();
        theTable            = new JTable(theModel);
                 
        for(int i=0;i<theModel.ColumnName.length;i++)
        {
           
            (theTable.getColumnModel()) . getColumn(i) . setPreferredWidth(theModel.iColumnWidth[i]);
        }
		
	}
	
	private void setLayouts()
	{
		setTitle("DRIVERS REGISTER");
		setMaximizable(true);
		setClosable(true);
		setResizable(true);
		setIconifiable(true);
		setBounds(0,0,620,500);
		show();

		TopPanel.setLayout(new GridLayout(3,1,2,2));
		MiddlePanel.setLayout(new BorderLayout());
		BottomPanel.setLayout(new FlowLayout());
		
		TopPanel.setBorder(new TitledBorder("Filter"));
		MiddlePanel.setBorder(new TitledBorder("Data"));
		BottomPanel.setBorder(new TitledBorder("Controls"));
	}
	
	 private void addComponents()
    {
    
      /*  TopPanel.add(new MyLabel("From Date"));
        TopPanel.add(TFromDate);
        TopPanel.add(new JLabel("To Date"));
        TopPanel.add(TToDate);
        TopPanel.add(BApply);*/
        
        MiddlePanel.add(new JScrollPane(theTable));
        
		BottomPanel.add(BPdf);
        BottomPanel.add(BExit);
        
        getContentPane().add(TopPanel,"North");
		getContentPane().add(MiddlePanel,"Center");
        getContentPane().add(BottomPanel,"South");
    }
	
	 private void addListener()
    {
        //BApply.addActionListener(new ActList());
        BExit.addActionListener(new ActList());
        BPdf.addActionListener(new ActList());
    }
	
	 private class ActList implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            
            if (ae.getSource() == BExit)
            {
                removeFrame();
            }
			
			if (ae.getSource() == BPdf)
            {
				
                createPDFFile();
				SetPrintData();
				try
				{
					File theFile   = new File(SFileName);
					Desktop        . getDesktop() . open(theFile);
				}
				catch(Exception ex){}
            }
        }  
    }
	
	
	private void getImage(int iRow)
{
	 imgbytes=null;
	 iImageCount=0;
	 iCnt=0;	

	try
	{
		HashMap theMap      = (HashMap)ADataList.get(iRow);
		String SId	  		= common.parseNull((String)theMap.get("ID"));
		
		System.out.println("SId-->"+SId);
		
		String QS = " select QRCODE from vehicles_movement"+
					" where vehicles_movement.ID="+SId;
					
		//String QS = " Select IRNQRCODE from Dispatch.Invoice where InvoiceNo = 'PIS213261'";
					

		iImageCount	=0;			
					
		System.out.println("Img QS-->"+QS);
				
			if(theConnection == null)
			{
                JDBCConnection jdbc   = JDBCConnection.getJDBCConnection();
                theConnection         = jdbc.getConnection();
            }
			PreparedStatement thePre    = theConnection.prepareStatement(QS);
			ResultSet theResult         = thePre.executeQuery();
			while(theResult.next())
			{
				imgbytes  =  getOracleBlob(theResult,"QRCODE");
				if(imgbytes!=null)
				{
					Blob b = theResult.getBlob(1);
					byte barr[] = b.getBytes(1, (int) b.length());
					System.out.println("barr-->"+barr);
					iCnt++;
					
					//String sFileName = "d:\\Vehicle"+iCnt+".png";
					String sFileName = "d:\\Vehicle"+SId+".jpg";

					System.out.println("file Name====>"+sFileName);

					FileOutputStream fout = new FileOutputStream(sFileName);
					fout.write(barr);
					fout.close();
					iImageCount++;
				}
		
				
			}
			System.out.println("iImageCount-->"+iImageCount);
		
	theResult.close();
	thePre.close();
			
	}
	 
	catch(Exception ex)  
	{
		ex.printStackTrace();
		System.out.println("ex:"+ex);
	}
		
		

}

private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
      {
    
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);
            if(blob!=null)
            {
                 inputStream = blob.getBinaryStream();        
                 int bytesRead = 0;        
                 
                 while((bytesRead = inputStream.read()) != -1) {        
                     outputStream.write(bytesRead);    
                 }
                 
                 bytes = outputStream.toByteArray();
            
             }
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
        return bytes;
     
     }
	
	
	private void createPDFFile()
    {
        try 
        {
			SFileName      = "d://VehicleMovementDetails.pdf";
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileName));
            document.setPageSize(PageSize.A4);
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(50);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
           
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }
	
	public void SetPrintData()
	{
		try
		{
			
				String SDate = common.getServerDate();
		
			 for(int i=0;i<theModel.getRowCount();i++)
            { 
                HashMap theMap = (HashMap)ADataList.get(i);
				String SId	   = common.parseNull((String)theMap.get("ID"));
                
                boolean bselect = (Boolean)(theModel.getValueAt(i,7));
				
				if(bselect)
                {
				
				AddCellIntoTable("Vehicle Movement Details" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Planned Date" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseDate(SDate) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				
				getImage(i);
				
				System.out.println("iImageCount-->"+iImageCount);
				
				if(iImageCount>0)
				{
					Image theImage      = Image.getInstance("d://Vehicle"+SId+".jpg");
					System.out.println("theImage-->"+theImage);
					
					
					PdfPCell c1         = new PdfPCell(theImage, true);
					c1                  . setHorizontalAlignment(Element.ALIGN_CENTER);
					c1                  . setVerticalAlignment(Element.ALIGN_MIDDLE);
					c1                  . setRowspan(2);
					c1                  . setColspan(2);
					c1                  . setBorder(Rectangle.TOP | Rectangle.BOTTOM | Rectangle.RIGHT) ;
					c1                  . hasMinimumHeight();
					table               . addCell(c1);
				}
				
				AddCellIntoTable("VehicleNo" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("VEHICLENAME")) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Planned OutTime" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("OUTDATE"))  , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("Return Time Std" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("INDATE")) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Purpose" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("PURPOSE")) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Location" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("PLACE")) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("DriverName" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable(common.parseNull((String)theMap.get("DRIVERNAME")) , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 3,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Approval for Trips Bus" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("T.K/H.R.O" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Approval for Trips Dost" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("Ramasamy/S.K/SO" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Approval for Trips RM Lorry" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("RMI/SMB" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, mediumbold);
				
				AddCellIntoTable("Approval for Service" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("Balan/Vishnu/AE" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
				AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, mediumbold);
				
				document.add(table);
				table.flushContent();
				document.newPage();
				
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("ex:"+ex);
		}
		
			
			try
			{
				
				document.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				System.out.println("ex:"+ex);
			}
	}
	
	private void setTableData()
	{
			theModel.setNumRows(0);
			setData();
			
			System.out.println("ADataList-->"+ADataList.size());
			
			for(int i=0;i<ADataList.size();i++)
			{
				HashMap theMap = (HashMap)ADataList.get(i);

				ArrayList theList;

				theList                     = new ArrayList();
				theList                     . clear();
				theList                     . add(i+1);
				theList                     . add(common.parseNull((String)theMap.get("VEHICLENAME")));
				theList                     . add(common.parseNull((String)theMap.get("PURPOSE")));
				theList                     . add(common.parseNull((String)theMap.get("PLACE")));
				theList                     . add(common.parseNull((String)theMap.get("DRIVERNAME")));
				theList                     . add(common.parseNull((String)theMap.get("OUTDATE")));
				theList                     . add(common.parseNull((String)theMap.get("INDATE")));
				theList.add(new Boolean(false));
				theModel                      . appendRow(new Vector(theList));
				theList                         = null;
			}
	}
	
	private void setData()
	{
		
		ADataList = new ArrayList();
		
		try
		{
			if(theConnection==null)
			{
				 JDBCConnection jdbc 	= JDBCConnection.getJDBCConnection();
				 theConnection 			= jdbc.getConnection();
			}
			
			PreparedStatement thePS  = theConnection.prepareStatement(getQS());
		    ResultSet theResult      = thePS.executeQuery();
			
			while((theResult.next()))
			{
				HashMap theMap = new HashMap();
				
				theMap.put("CARDNO",common.parseNull(theResult.getString(1)));
				theMap.put("VEHICLENAME",common.parseNull(theResult.getString(2)));
				theMap.put("PURPOSE",common.parseNull(theResult.getString(3)));
				theMap.put("PLACE",common.parseNull(theResult.getString(4)));
				theMap.put("DRIVERNAME",common.parseNull(theResult.getString(5)));
				theMap.put("OUTDATE",common.parseNull(theResult.getString(6)));
				theMap.put("INDATE",common.parseNull(theResult.getString(7)));
				theMap.put("ID",common.parseNull(theResult.getString(8)));
				
				ADataList.add(theMap);
			}
			theResult.close();
			thePS.close();
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("ex:"+ex);
		}
	}
	
	private String getQS()
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(" select vehicles_movement.cardno,vehicleName,purpose,place,empname,to_char(outdate,'dd-mm-yyyy hh24:mi:ss') as Outdate,");
		sb.append(" to_char(Indate,'dd-mm-yyyy hh24:mi:ss') as Indate,vehicles_movement.id from vehicles_movement");
		sb.append(" Left join staff on staff.empcode = vehicles_movement.drivercode");
		sb.append(" Inner join vehiclepurpose on vehiclepurpose.id= vehicles_movement.purposecode");
		sb.append(" Inner join vehicleplaces on vehicleplaces.id =  vehicles_movement.PLACECODE");
		sb.append(" Inner join vehicleinfo on vehicleinfo.cardno = vehicles_movement.cardno ");
		
		return sb.toString();
	}
	
	private void removeFrame()
    {
        try
        {
            Layer.remove(this);
            Layer.repaint();
            Layer.updateUI();
        }
        catch(Exception ex)
        {
        
        }
    }
	public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }  
}