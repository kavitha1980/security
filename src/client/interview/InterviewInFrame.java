package client.interview;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;

public class InterviewInFrame extends JInternalFrame implements rndi.CodedNames
{

     Info infoDomain;
     protected JLayeredPane layer;
     DateField1 DInDate,DOutDate;
     JTextField TInTime,TOutTime,TSpendTime;
     MyLabel LName,LInTime,LInDate,LSex,LQuali,LPlace,LCat;
     MyLabel LAgentName,LHod,LOutDate,LSpendTime,LOutTime,LAge,LSlip;
     NameField NName,NPlace;

     boolean okflag=true;
     MyTextField MCardNo;

     WholeNumberField WAge;

     MyComboBox JCAgentName,JCHod,JCCategory,JCSex,JCQuali;
     JTable infoTable;
     JScrollPane tableScroll;
     JButton BSave,BCancel,BExit;
     JPanel LeftPanel,RightPanel,MiddlePanel,BottomPanel,TotalPanel,TopPanel,FullPanel;
     MessageBean messageBean;
     RowSet rowset;
     TimeField time;
     Vector VAgentCode,VHodCode,VQualiCode,VCategoryCode;
     String QS=" ";
     int slipno=0;
     InterviewInModel interviewModel;
     
     Common common = new Common();
     public InterviewInFrame(JLayeredPane layer)
     {
          this.layer = layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          //setSlipNo();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void createComponents()
     {
          try
          {
               rowset         = new RowSet();
     
               LeftPanel      = new JPanel();
               RightPanel     = new JPanel();
               LeftPanel      = new JPanel();
               TopPanel       = new JPanel();
               TotalPanel     = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               FullPanel      = new JPanel();
     
               MCardNo        = new MyTextField();
               MCardNo.addKeyListener(new keys());

               LInTime        = new MyLabel("InTime");
               LOutTime       = new MyLabel("OutTime");
               LName          = new MyLabel("Name");
               LAgentName     = new MyLabel("AgentName");
               LCat           = new MyLabel("Category");
               LQuali         = new MyLabel("Qualification");
               LHod           = new MyLabel("Hod");
               LSex           = new MyLabel("Sex");
               LPlace         = new MyLabel("Place");
               LInDate        = new MyLabel("Date");
               LOutDate       = new MyLabel("OutDate");
               LSpendTime     = new MyLabel("SpendTime");
               LAge           = new MyLabel("Age");
               LSlip = new MyLabel(" ");
     
               VAgentCode     = new Vector();
               VQualiCode     = new Vector();
               VHodCode       = new Vector();
               VCategoryCode  = new Vector();
     
               VAgentCode     = infoDomain.getAgentCode();
               VQualiCode     = infoDomain.getQualiCode();
               VHodCode       = infoDomain.getHodCode();
               VCategoryCode  = infoDomain.getCategoryCode();

               JCCategory     = new MyComboBox(infoDomain.getCategory());
               JCCategory.addItem("STAFF");
               JCCategory.addItem("DC WORKER");
               JCCategory.addItem("2YEARS CONTRACT ");
               JCCategory.addItem("3YEARS CONTRACT");


               JCAgentName    = new MyComboBox(infoDomain.getAgent());
               JCAgentName.addItem("BY CALL LETTER");
               JCAgentName.addItem("BY AGENT");
               JCAgentName.addItem("LOCAL AREA");


/*               JCAgentName.addItem("SRIDHAR");
               JCAgentName.addItem("LOCAL AREA"); */

               JCHod          = new MyComboBox(infoDomain.getHod());
               JCHod.addItem("DYEING MASTER");

               JCSex          = new MyComboBox();

               JCQuali        = new MyComboBox(infoDomain.getQuali());
               JCQuali.addItem("Gradutae");
               JCQuali.addItem("Intermidate");
               JCQuali.addItem("10th");
               JCQuali.addItem("Under Metric");


     
                         time = new TimeField();
               String STime   = time.getTimeNow();

               WAge           = new WholeNumberField(2);
     
               NPlace         = new NameField(30);
               NName          = new NameField(40);
               NPlace.addFocusListener(new keyfocus());

               DInDate        = new DateField1();
               DOutDate       = new DateField1();
     
               /*TInTime        = new TimeField();
               TOutTime       = new TimeField();
               TSpendTime     = new TimeField();*/
     
               TInTime        = new JTextField(20);
               TOutTime       = new JTextField(20);
               TSpendTime     = new JTextField(20);
     
               TInTime.setText(STime);
               TInTime.setEditable(false);

               DInDate.setTodayDate();
               String SinDate = DInDate.toNormal();
               int iinDate    = Integer.parseInt(SinDate);
               DInDate.setEditable(false);
               Vector initVector = infoDomain.initValues(0);

               interviewModel = new InterviewInModel(initVector);
               infoTable      = new JTable(interviewModel);

               tableScroll    = new JScrollPane(infoTable);
               infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (infoTable.getColumn("Name")).setPreferredWidth(200);
               (infoTable.getColumn("Sex")).setPreferredWidth(50);
               (infoTable.getColumn("InTime")).setPreferredWidth(100);
               (infoTable.getColumn("Place")).setPreferredWidth(180);
               (infoTable.getColumn("Agent")).setPreferredWidth(180);
               (infoTable.getColumn("Category")).setPreferredWidth(180);
               (infoTable.getColumn("Hod")).setPreferredWidth(180);


               BSave          = new JButton("Save");
               BCancel        = new JButton("Cancel");
               BExit          = new JButton("Exit");

               BSave.setMnemonic('S');
               BCancel.setMnemonic('C');
               BExit.setMnemonic('X');
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }
     private class keyfocus extends FocusAdapter
     {
          public void focusLost(FocusEvent fe)
          {
               JCCategory.requestFocus();
          }
     }

     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    okflag=true;
                    try
                    {
                         String SSCard  = MCardNo.getText().trim();
                         String QS      = "Select CheckCard from InterviewNames where SlipNo='"+SSCard+"' And Status=0 ";
                         int st         = infoDomain.getInterviewInitialCheck(QS);
                         if(st==0)
                         {
                              okflag    = false;
                              JOptionPane.showMessageDialog(null,"Already a Person having this Card");
                              MCardNo.setText("");
                              MCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
               }
          }
     }

     private void setLayouts()
     {
          FullPanel.setLayout(new GridLayout(2,1));
          LeftPanel.setLayout(new GridLayout(5,2,5,5));
          LeftPanel.setBorder(new TitledBorder("Personal Info"));
          RightPanel.setLayout(new GridLayout(5,2,5,5));
          RightPanel.setBorder(new TitledBorder("General Info"));
          TotalPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new FlowLayout());
          BottomPanel.setLayout(new BorderLayout());
          BottomPanel.setBorder(new TitledBorder(" Details about Persons" ));
          TopPanel.setLayout(new GridLayout(2,1));
          setTitle("InterviewInFrame(1.0)");
     }
     /*private void setSlipNo()
     {
          try
          {

               slipno         = (infoDomain.getSlipNo())+1;
               LSlip.setText(""+slipno);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }*/

     private void addComponents()
     {
          try
          {
               JCSex.addItem("Male");
               JCSex.addItem("Female");
     
               LeftPanel.add(new MyLabel("Card No: "));
               LeftPanel.add(MCardNo);
               LeftPanel.add(LName);
               LeftPanel.add(NName);
               LeftPanel.add(LAge);
               LeftPanel.add(WAge);
               LeftPanel.add(LSex);
               LeftPanel.add(JCSex);
               LeftPanel.add(LQuali);
               LeftPanel.add(JCQuali);
               RightPanel.add(LPlace);
               RightPanel.add(NPlace);
     
               RightPanel.add(LInDate);
               RightPanel.add(DInDate);
               RightPanel.add(LInTime);
               RightPanel.add(TInTime);
               RightPanel.add(LCat);
               RightPanel.add(JCCategory);
               //RightPanel.add(LHod);
               //RightPanel.add(JCHod);
               RightPanel.add(LAgentName);
               RightPanel.add(JCAgentName);
               //RightPanel.add(new JLabel(" "));
               //RightPanel.add(new JLabel(" "));

               //RightPanel.add(LOutTime);
               //RightPanel.add(TOutTime);
               //RightPanel.add(LOutDate);
               //RightPanel.add(DOutDate);
               //RightPanel.add(LSpendTime);
               //RightPanel.add(TSpendTime);
     
               TotalPanel.add(LeftPanel);
               TotalPanel.add(RightPanel);
     
               MiddlePanel.add(BSave);
               MiddlePanel.add(BCancel);
               MiddlePanel.add(BExit);
     
               TopPanel.add(TotalPanel);
               TopPanel.add(MiddlePanel);
     
     
               BottomPanel.add("North",infoTable.getTableHeader());
               BottomPanel.add(tableScroll);
     
               FullPanel.add(TotalPanel);
               FullPanel.add(MiddlePanel);
     
               TOutTime.setEditable(false);
               TSpendTime.setEditable(false);
               DOutDate.setEditable(false);
               DInDate.requestFocus();
               setSize(800,520);
               setVisible(true);
               setClosable(true);
               setMaximizable(true);
               setIconifiable(true);
     
               getContentPane().setLayout(new BorderLayout());
               getContentPane().add(TotalPanel,"North");
               getContentPane().add(BottomPanel);
               getContentPane().add(MiddlePanel,"South");
               
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void addListeners()
     {
          BSave.addActionListener(new Insert());
          BCancel.addActionListener(new Insert());
          BExit.addActionListener(new Insert());
          
     }

     private class Insert implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BSave)
               {

                    if(okflag==true)
                    {
                         Insertvalues();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"Plz Enter Another Card No");
                    }
               }

               if(ae.getSource()==BCancel)
               {
                    clearFields();
                    MCardNo.requestFocus();    
               }

               if(ae.getSource()==BExit)
               {
                    setVisible(false);
               }
               
          }
          
     }
     private void clearFields()
     {
          //DInDate.setText("");
          MCardNo.setText("");
          NName.setText("");
          WAge.setText("");
          JCSex.setSelectedIndex(0);
          JCQuali.setSelectedIndex(0);
          NPlace.setText("");
          //TInTime.setText("");
          JCAgentName.setSelectedIndex(0);
          JCHod.setSelectedIndex(0);
          JCCategory.setSelectedIndex(0);
     }



     private void Insertvalues()
     {
         

          Vector infoVector = new Vector();
          try
          {
               if(checkValues())
               {


                    String SDate             = DInDate.toNormal();
                    int iInDate              = common.toInt(common.pureDate(SDate));
                    String SName             = NName.getText();
                    String SPlace            = NPlace.getText();
                    String SInTime           = (String)TInTime.getText();
          
                    int iAge                 = Integer.parseInt(WAge.getText());
                    String SQuali            = (String)JCQuali.getSelectedItem();
                    String SSex              = (String)JCSex.getSelectedItem();
                    String SAgent            = (String)JCAgentName.getSelectedItem();
                    String SCategory         = (String)JCCategory.getSelectedItem();

                    //String SHod            = (String)JCHod.getSelectedItem();
                    String SHod              = " ";

                    int Quali                = JCQuali.getSelectedIndex();
                    int Sex                  = JCSex.getSelectedIndex();
                    int Agent                = JCAgentName.getSelectedIndex();
                    int Category             = JCCategory.getSelectedIndex();
                    int Hod                  = JCHod.getSelectedIndex();
          
          
          
                    int iId                  = infoDomain.getId();
                  /*int iQualiCode           = infoDomain.setCode(Quali,VQualiCode);
                    int iAgentCode           = infoDomain.setCode(Agent,VAgentCode);
                    int iCategoryCode        = infoDomain.setCode(Category,VCategoryCode);
                    int iHodCode             = infoDomain.setCode(Hod,VHodCode);*/
     
                    infoVector.addElement(String.valueOf(iInDate));
                    infoVector.addElement(SName);
                    infoVector.addElement(String.valueOf(iAge));
                    infoVector.addElement(SSex);
                    infoVector.addElement(SQuali);
                    infoVector.addElement(SInTime);
                    infoVector.addElement(SCategory);
                    infoVector.addElement(SHod);
                    infoVector.addElement(SAgent);
                    infoVector.addElement("0");
                    infoVector.addElement(String.valueOf(iId));
                    infoVector.addElement(String.valueOf(0));
                    infoVector.addElement(SPlace);
                    infoVector.addElement(String.valueOf(0));

                    //int slipno  = infoDomain.getSlipNo();
                    String Scard  = (String)MCardNo.getText();
                    //int slipno    = Integer.parseInt(Scard); 

                    infoVector.addElement(Scard);
                    //infoVector.addElement(String.valueOf(slipno));
                    try
                    {
                             infoDomain.insertValues(infoVector);
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
     
                    }
     
     
     
                    Vector iVector = new Vector();
                    iVector.addElement(" ");
                    iVector.addElement(common.parseNull(SName));
                    iVector.addElement(common.parseNull(SSex));
                    iVector.addElement(common.parseNull(SInTime));
                    iVector.addElement(common.parseNull(SPlace));
                    iVector.addElement(common.parseNull(SAgent));
                    iVector.addElement(common.parseNull(SCategory));
                    iVector.addElement(common.parseNull(SHod));
     
     
     
                    interviewModel.insertData(iVector);
                    TInTime.setText(time.getTimeNow());
                    clearFields();
                    LSlip.setText(" ");
                    MCardNo.requestFocus();
                    //setSlipNo();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"All fields must be filled");

               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private boolean checkValues()
     {
          boolean flag=true;
          try
          {
               String name = "";
               String place = "",SCardNo="";
               int age =0,cardno=0;
     
               SCardNo                 = common.parseNull(MCardNo.getText().trim()); 
               if(SCardNo.equals("")) 
               {
                     flag=false;
                     MCardNo.requestFocus();
                     return flag;
               }      
               //cardno                  = Integer.parseInt(SCardNo);

               name                    = common.parseNull(NName.getText().trim());
               if(name.equals(""))
               {
                     flag=false;
                     NName.requestFocus();
                     return flag;
               } 

               String sage             = common.parseNull(WAge.getText().trim());
               if(sage.equals(""))
               { 
                     flag=false;
                     WAge.requestFocus();
                     return flag;
               }

               age                     = Integer.parseInt(sage);

               place                   = common.parseNull(NPlace.getText().trim());

               if(place.equals(""))
               {
                     flag=false;
                     NPlace.requestFocus();
                     return flag;
               }


          }
          catch(Exception e)
          {
               JOptionPane.showMessageDialog(null,"All Fields Must be Filled");
          }
     return flag;                                       
     }






}
