package client.interview;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.util.*;
import java.sql.*;

public class InterviewUpdationModel extends DefaultTableModel
{  
     String columnname[]    = {"Sno","Name","Age","Sex","Qualification","Place","InDate","OutDate","InTime","OutTime","Category","AgentName","SpentTime","SelectedFor","Result"};
     String columnname1[]   = {"Sno","Name","Age","Sex","Qualification","Place","InDate","OutDate","InTime","OutTime","Category","AgentName","SpentTime","SelectedFor","Result"};
     String ColumnType[]    = {"N"  , "E"  , "E" , "E" ,      "E"      ,  "E"  ,  "E"   ,  "E"    ,  "E"   ,  "E"    ,   "E"    ,    "E"    ,    "S"    ,     "E"     ,   "E"  };     

   //  String ColumnType[]    = {"N"  , "S"  ,"N"  , "E" ,      "E"      , "S"   ,  "D"   ,  "D"    ,  "S"   ,  "S"    ,   "E"    ,    "E"    ,    "S"    ,     "S"     ,   "E"  };     

     Object values[][];

     Vector v1;
     int sNo;

     InterviewUpdationModel(Vector v1)
     {
               this.v1 = v1;
               setDataVector(getdata(),columnname);
     }
     private Object[][] getdata()
     {
               int sno=1;
               int m=0;
               int size = (v1.size()/13);

               System.out.println("Model Vect"+v1);

               System.out.println("size"+size);

               Object values[][]=new Object[size][columnname.length];
               try
               {
                    for(int i=0;i<size;i++)
                    {
                         System.out.println("size"+size);
                         System.out.println("inside"+(String)v1.elementAt(m+0));

                         values[i][0]   =    String.valueOf(sno+i);
                         values[i][1]   =    v1.elementAt(m+0);
                         values[i][2]   =    v1.elementAt(m+1);
                         values[i][3]   =    v1.elementAt(m+2);
                         values[i][4]   =    v1.elementAt(m+3);
                         values[i][5]   =    v1.elementAt(m+4);
                         values[i][6]   =    v1.elementAt(m+5);
                         values[i][7]   =    v1.elementAt(m+6);
                         values[i][8]   =    v1.elementAt(m+7);
                         values[i][9]   =    v1.elementAt(m+7);
                         values[i][10]  =    v1.elementAt(m+8);
                         values[i][11]  =    v1.elementAt(m+10);
                         values[i][12]  =    v1.elementAt(m+11);
                         values[i][13]  =    v1.elementAt(m+12);
                         values[i][14]  =    v1.elementAt(m+13);

                         m=m+14;
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Exception in vector");

                    e.printStackTrace();
                }
               return values;
     }
     public int getSno(Vector vect1)
     {
               int i;
               for(i=0;i<vect1.size();i++)
               {
               }
               return i;
     }
     public Class getColumnClass(int col){ return getValueAt(0,col).getClass();}
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }
     public void setVector(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }
     public  boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="N")
               return false;
          return true;
     }
     public void setVector1(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }
     public int getRows()
     {
          return super.dataVector.size();
     }   
     public void insertData(Vector vect)
     {
         insertRow(getRows(),vect);
         setRowId();
     }
     private void setRowId()
     {
         for(int i=0;i<getRows();i++)
         {
                  setValueAt(String.valueOf(i+1),i,0);
         }
     }
}
