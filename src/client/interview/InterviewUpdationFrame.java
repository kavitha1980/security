package client.interview;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.sql.*;
import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;

import domain.jdbc.info.*;

public class InterviewUpdationFrame extends JInternalFrame implements rndi.CodedNames
{
     Info infoDomain;
     SecurityData   security;
     protected JLayeredPane layer;
     DateField1 DInDate,DOutDate;
     JTextField TInTime,TOutTime,TSpendTime;
     MyLabel LName,LInTime,LInDate,LSex,LQuali,LPlace,LCat;
     MyLabel LAgentName,LHod,LOutDate,LSpendTime,LOutTime,LAge,LSlip;
     NameField NName,NPlace;

     boolean okflag=true;
     MyTextField MCardNo;

     WholeNumberField WAge;

     MyComboBox JCAgentName,JCHod,JCCategory,JCSex,JCQuali;
     JTable infoTable;
     JScrollPane tableScroll;
     JButton BApply,BSave,BCancel,BExit;
     JPanel LeftPanel,RightPanel,MiddlePanel,BottomPanel,TotalPanel,TopPanel,FullPanel;
     MessageBean messageBean;
     RowSet rowset;
     TimeField time;
     Vector VAgentCode,VHodCode,VQualiCode,VCategoryCode;
     String QS=" ";
     int slipno=0;
     int iinDate=0;
     int ioutDate=0;
     int iFromDate=0;
     int iToDate=0;
     InterviewUpdationModel interviewModel;
     
     Common common = new Common();
     public InterviewUpdationFrame(JLayeredPane layer)
     {
          this.layer = layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
//               security            = (SecurityData)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void createComponents()
     {
          try
          {
               rowset         = new RowSet();
     
               LeftPanel      = new JPanel();
               RightPanel     = new JPanel();
               LeftPanel      = new JPanel();
               TopPanel       = new JPanel();
               TotalPanel     = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               FullPanel      = new JPanel();
     
               LInDate        = new MyLabel("Date");
               LOutDate       = new MyLabel("OutDate");
     
               VAgentCode     = new Vector();
               VQualiCode     = new Vector();
               VHodCode       = new Vector();
               VCategoryCode  = new Vector();
     
               VAgentCode     = infoDomain.getAgentCode();
               VQualiCode     = infoDomain.getQualiCode();
               VHodCode       = infoDomain.getHodCode();
               VCategoryCode  = infoDomain.getCategoryCode();

               JCCategory     = new MyComboBox(infoDomain.getCategory());
               JCCategory.addItem("DC WORKER");
               JCCategory.addItem("2YEARS CONTRACT ");
               JCCategory.addItem("3YEARS CONTRACT");

               JCAgentName    = new MyComboBox(infoDomain.getAgent());
               JCAgentName.addItem("BY CALL LETTER");
               JCAgentName.addItem("SRIDHAR");
               JCAgentName.addItem("LOCAL AREA");

               JCHod          = new MyComboBox(infoDomain.getHod());
               JCHod.addItem("DYEING MASTER");

               JCSex          = new MyComboBox();

               JCQuali        = new MyComboBox(infoDomain.getQuali());
               JCQuali.addItem("B.SC");
     
               DInDate        = new DateField1();
               DOutDate       = new DateField1();

            /*
               DInDate.setTodayDate();
               DOutDate.setTodayDate();
               String SinDate = DInDate.toNormal();
               iinDate    = Integer.parseInt(SinDate);
               String SoutDate= DOutDate.toNormal();
               ioutDate   = Integer.parseInt(SoutDate);
           */


            //   Vector initVector = infoDomain.initValues(0);

               Vector initVector = infoDomain.initValues(iinDate,ioutDate);

               System.out.println("Initial INITVECTOR size: "+initVector.size());
               interviewModel = new InterviewUpdationModel(initVector);
               infoTable      = new JTable(interviewModel);

               tableScroll    = new JScrollPane(infoTable);
               infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (infoTable.getColumn("Name")).setPreferredWidth(200);
               (infoTable.getColumn("Age")).setPreferredWidth(50);
               (infoTable.getColumn("Sex")).setPreferredWidth(50);
               (infoTable.getColumn("Qualification")).setPreferredWidth(80);
               (infoTable.getColumn("Place")).setPreferredWidth(180);
               (infoTable.getColumn("InDate")).setPreferredWidth(100);
               (infoTable.getColumn("OutDate")).setPreferredWidth(100);
               (infoTable.getColumn("InTime")).setPreferredWidth(100);
               (infoTable.getColumn("OutTime")).setPreferredWidth(100);
               (infoTable.getColumn("Category")).setPreferredWidth(180);
               (infoTable.getColumn("AgentName")).setPreferredWidth(180);
               (infoTable.getColumn("SpentTime")).setPreferredWidth(100);
               (infoTable.getColumn("SelectedFor")).setPreferredWidth(100);
               (infoTable.getColumn("Result")).setPreferredWidth(180);

               BApply         = new JButton("Apply");
               BSave          = new JButton("Save");
               BCancel        = new JButton("Cancel");
               BExit          = new JButton("Exit");

               BApply.setMnemonic('A');
               BSave.setMnemonic('S');
               BCancel.setMnemonic('C');
               BExit.setMnemonic('X');
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private class keyfocus extends FocusAdapter
     {
          public void focusLost(FocusEvent fe)
          {
               JCCategory.requestFocus();
          }
     }
     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    okflag=true;
                    try
                    {
                         String SSCard  = MCardNo.getText().trim();
                         String QS      = "Select CheckCard from InterviewNames where SlipNo='"+SSCard+"' And Status=0 ";
                         int st         = infoDomain.getInterviewInitialCheck(QS);
                         if(st==0)
                         {
                              okflag    = false;
                              JOptionPane.showMessageDialog(null,"Already a Person having this Card");
                              MCardNo.setText("");
                              MCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
               }
          }
     }
     private void setLayouts()
     {
          FullPanel.setLayout(new GridLayout(2,1));
          LeftPanel.setBorder(new TitledBorder("Date Info"));
          MiddlePanel.setLayout(new FlowLayout());
          BottomPanel.setLayout(new BorderLayout());
          BottomPanel.setBorder(new TitledBorder(" Details about Persons" ));
          TopPanel.setLayout(new GridLayout(2,1));
          setTitle("InterviewInFrame(1.0)");
     }
     private void addComponents()
     {
          try
          {
               LeftPanel.add(new MyLabel("From "));
               LeftPanel.add(DInDate);
               LeftPanel.add(new MyLabel("To "));
               LeftPanel.add(DOutDate);
               LeftPanel.add(BApply);
     
               TotalPanel.add(LeftPanel);
     
               MiddlePanel.add(BSave);
               MiddlePanel.add(BCancel);
               MiddlePanel.add(BExit);
     
               TopPanel.add(TotalPanel);
               TopPanel.add(MiddlePanel);
     
               BottomPanel.add("North",infoTable.getTableHeader());
               BottomPanel.add(tableScroll);
     
               FullPanel.add(TotalPanel);
               FullPanel.add(MiddlePanel);
     
               DOutDate.setEditable(true);
               DInDate.requestFocus();
               setSize(800,520);
               setVisible(true);
               setClosable(true);
               setMaximizable(true);
               setIconifiable(true);
     
               getContentPane().setLayout(new BorderLayout());
               getContentPane().add(TotalPanel,"North");
               getContentPane().add(BottomPanel);
               getContentPane().add(MiddlePanel,"South");
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void addListeners()
     {
          BApply.addActionListener(new Insert());
          BSave.addActionListener(new Insert());
          BCancel.addActionListener(new Insert());
          BExit.addActionListener(new Insert());
     }
     private class Insert implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BApply)
               {
                    System.out.println("Apply button is clicked");

                    try
                    {
                         Vector initVector = infoDomain.initValues(iinDate,ioutDate);
                         initVector = infoDomain.initValues(iinDate,ioutDate);

                         String SinDate = DInDate.toNormal();
                         iinDate    = Integer.parseInt(SinDate);
                         String SoutDate= DOutDate.toNormal();
                         ioutDate   = Integer.parseInt(SoutDate);

                         System.out.println("in Apply From: "+iinDate);
                         System.out.println("in Appy To: "+ioutDate);
                         System.out.println("InitVector size: "+initVector.size());

                         infoDomain.getInterviewValues(iinDate,ioutDate);

                         System.out.println("InitVector size1: "+initVector.size());

                         interviewModel = new InterviewUpdationModel(initVector);
                         infoTable      = new JTable(interviewModel);
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }
               }
               if(ae.getSource()==BSave)
               {
                    System.out.println("In Save From: "+iinDate);
                    System.out.println("in Save To: "+ioutDate);
                    try
                    {
                         System.out.println("Save button is clicked");
                       //  infoDomain.getInterviewValues(iinDate,ioutDate);
                         infoDomain.getInterviewValues(20061010,20061025);

                         System.out.println("Test");
                    }
                    catch(Exception e)
                    {
                         System.out.println(e);
                         e.printStackTrace();
                    }

               }


             /*
               if(ae.getSource()==BSave)
               {
                    if(okflag==true)
                       //  Insertvalues();
                       System.out.println("Insert values");
                    else
                         JOptionPane.showMessageDialog(null,"Plz Enter Another Card No");
               }
               if(ae.getSource()==BCancel)
               {
                  //  clearFields();
                    DInDate.requestFocus();    
               }
               if(ae.getSource()==BExit)
                    setVisible(false);
              */

          }
     }

   /*
     private void clearFields()
     {
          DInDate.setText("");
          DOutDate.setText("");
     }

     private void Insertvalues()
     {
          Vector infoVector = new Vector();
          try
          {
               if(checkValues())
               {
                    String SDate             = DInDate.toNormal();
                    int iInDate              = common.toInt(common.pureDate(SDate));
                    String SName             = NName.getText();
                    String SPlace            = NPlace.getText();
                    String SInTime           = (String)TInTime.getText();
          
                    int iAge                 = Integer.parseInt(WAge.getText());
                    String SQuali            = (String)JCQuali.getSelectedItem();
                    String SSex              = (String)JCSex.getSelectedItem();
                    String SAgent            = (String)JCAgentName.getSelectedItem();
                    String SCategory         = (String)JCCategory.getSelectedItem();
                    String SHod              = " ";

                    int Quali                = JCQuali.getSelectedIndex();
                    int Sex                  = JCSex.getSelectedIndex();
                    int Agent                = JCAgentName.getSelectedIndex();
                    int Category             = JCCategory.getSelectedIndex();
                    int Hod                  = JCHod.getSelectedIndex();
                    int iId                  = infoDomain.getId();
     
                    infoVector.addElement(String.valueOf(iInDate));
                    infoVector.addElement(SName);
                    infoVector.addElement(String.valueOf(iAge));
                    infoVector.addElement(SSex);
                    infoVector.addElement(SQuali);
                    infoVector.addElement(SInTime);
                    infoVector.addElement(SCategory);
                    infoVector.addElement(SHod);
                    infoVector.addElement(SAgent);
                    infoVector.addElement("0");
                    infoVector.addElement(String.valueOf(iId));
                    infoVector.addElement(String.valueOf(0));
                    infoVector.addElement(SPlace);
                    infoVector.addElement(String.valueOf(0));

                    String Scard  = (String)MCardNo.getText();

                    infoVector.addElement(Scard);
                    try
                    {
                             infoDomain.insertValues(infoVector);
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
     
                    }
                    Vector iVector = new Vector();
                    iVector.addElement(" ");
                    iVector.addElement(common.parseNull(SName));
                    iVector.addElement(common.parseNull(SSex));
                    iVector.addElement(common.parseNull(SInTime));
                    iVector.addElement(common.parseNull(SPlace));
                    iVector.addElement(common.parseNull(SAgent));
                    iVector.addElement(common.parseNull(SCategory));
                    iVector.addElement(common.parseNull(SHod));
     
                    interviewModel.insertData(iVector);
                    TInTime.setText(time.getTimeNow());
                    clearFields();
                    LSlip.setText(" ");
                    MCardNo.requestFocus();
                    //setSlipNo();
               }
               else
               {
                    JOptionPane.showMessageDialog(null,"All fields must be filled");

               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private boolean checkValues()
     {
          boolean flag=true;
          try
          {
               String name = "";
               String place = "",SCardNo="";
               int age =0,cardno=0;
     
               SCardNo                 = common.parseNull(MCardNo.getText().trim()); 
               if(SCardNo.equals("")) 
               {
                     flag=false;
                     MCardNo.requestFocus();
                     return flag;
               }      
               //cardno                  = Integer.parseInt(SCardNo);

               name                    = common.parseNull(NName.getText().trim());
               if(name.equals(""))
               {
                     flag=false;
                     NName.requestFocus();
                     return flag;
               } 

               String sage             = common.parseNull(WAge.getText().trim());
               if(sage.equals(""))
               { 
                     flag=false;
                     WAge.requestFocus();
                     return flag;
               }

               age                     = Integer.parseInt(sage);

               place                   = common.parseNull(NPlace.getText().trim());

               if(place.equals(""))
               {
                     flag=false;
                     NPlace.requestFocus();
                     return flag;
               }
          }
          catch(Exception e)
          {
               JOptionPane.showMessageDialog(null,"All Fields Must be Filled");
          }
     return flag;                                       
     }
  */


}
