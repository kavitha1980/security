package client.interview;

import java.io.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;


public class InterviewOutFrame extends JInternalFrame implements rndi.CodedNames
{

     Info infoDomain;
     protected JLayeredPane layer;
     DateField1 DInDate,DOutDate;
     MyTextField TInTime,TOutTime,TSpendTime,TCardNo;
     MyLabel LName,LInTime,LInDate,LSex,LQuali,LPlace,LCat,LSelectName;
     MyLabel LAgentName,LHod,LOutDate,LSpendTime,LOutTime,LAge;
     NameField NName,NPlace;
     WholeNumberField WAge;
     MyComboBox JCAgentName,JCHod,JCCategory,JCSex,JCQuali,JCNames;
     MyComboBox JCResult;
     JTable infoTable;
     JScrollPane tableScroll;
     JButton BSave,BCancel,BExit;
     JPanel LeftPanel,RightPanel,MiddlePanel,BottomPanel,TotalPanel,topPanel,FullPanel;
     JPanel namePanel;
     MessageBean messageBean;
     RowSet rowset;

     Vector VAgentCode,VHodCode,VQualiCode,VCategoryCode,VHodName;
     String QS=" ";
     //int sl=0;
     String sl="";
     InterviewOutModel interviewModel;
     
     Common common = new Common();
     public InterviewOutFrame(JLayeredPane layer)
     {
          this.layer = layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void createComponents()
     {
          try
          {
               rowset         = new RowSet();
               namePanel      = new JPanel();
               LeftPanel      = new JPanel();
               RightPanel     = new JPanel();
               LeftPanel      = new JPanel();
               topPanel       = new JPanel();
               TotalPanel     = new JPanel();
               MiddlePanel    = new JPanel();
               BottomPanel    = new JPanel();
               FullPanel      = new JPanel();
     
               LSelectName    = new MyLabel("Select Name:");
               LInTime        = new MyLabel("      ");
               LOutTime       = new MyLabel("      ");
               LName          = new MyLabel("      ");
               LAgentName     = new MyLabel("      ");
               LCat           = new MyLabel("      ");
               LQuali         = new MyLabel("      ");
               LHod           = new MyLabel("      ");
               LSex           = new MyLabel("      ");
               LPlace         = new MyLabel("      ");
               LInDate        = new MyLabel("      ");
               LOutDate       = new MyLabel("      ");
               LSpendTime     = new MyLabel("      ");
               LAge           = new MyLabel("      ");

               VHodName       = new Vector();
               VHodCode       = new Vector();

               JCHod          = new MyComboBox(infoDomain.getHod());
               JCHod.addItem("DYEING MASTER");

               TimeField time = new TimeField();
               String STime   = time.getTimeNow();
     
               WAge           = new WholeNumberField();
     
               NPlace         = new NameField(30);
               NName          = new NameField(40);
     
               DInDate        = new DateField1();
               DOutDate       = new DateField1();
     
               JCResult       = new MyComboBox();

               TInTime        = new MyTextField(20);
               TOutTime       = new MyTextField(20);
               TSpendTime     = new MyTextField(20);
               TCardNo        = new MyTextField(10);
               TOutTime.setText(STime);

               DOutDate.setTodayDate();

               String Sdate   = DOutDate.toNormal();
               int iinDate    =Integer.parseInt(Sdate);

               LOutDate.setText(common.parseDate(Sdate));
               LOutTime.setText(STime);

               Vector initVector = infoDomain.getOutValues(iinDate);

               interviewModel = new InterviewOutModel(initVector);
               infoTable      = new JTable(interviewModel);
               infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (infoTable.getColumn("Name")).setPreferredWidth(200);
               (infoTable.getColumn("Sex")).setPreferredWidth(50);
               (infoTable.getColumn("InTime")).setPreferredWidth(100);
               (infoTable.getColumn("OutTime")).setPreferredWidth(100);

               (infoTable.getColumn("Place")).setPreferredWidth(180);
               (infoTable.getColumn("Agent")).setPreferredWidth(180);
               (infoTable.getColumn("Category")).setPreferredWidth(180);
               (infoTable.getColumn("Hod")).setPreferredWidth(180);

               tableScroll    = new JScrollPane(infoTable);


               BSave          = new JButton("Save");
               BCancel        = new JButton("Cancel");
               BExit          = new JButton("Exit");
               
               BSave.setMnemonic('S');
               BCancel.setMnemonic('C');
               BExit.setMnemonic('X');
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }
     private void setLayouts()
     {
          FullPanel.setLayout(new GridLayout(2,1));
          LeftPanel.setLayout(new GridLayout(7,2,3,3));
          LeftPanel.setBorder(new TitledBorder("Personal Info"));
          RightPanel.setLayout(new GridLayout(7,2,3,3));
          RightPanel.setBorder(new TitledBorder("General Info"));
          TotalPanel.setLayout(new GridLayout(1,2));
          MiddlePanel.setLayout(new FlowLayout());
          BottomPanel.setLayout(new BorderLayout());
          BottomPanel.setBorder(new TitledBorder(" Information of Today Candidates(Interview Attended)"));
          topPanel.setLayout(new GridLayout(2,1));
          namePanel.setLayout(new GridLayout(1,2));

          JCResult.addItem("UnKnown");
          JCResult.addItem("Selected");
          JCResult.addItem("NotSelected");


     }

     private void addComponents()
     {

          LeftPanel.add(new MyLabel("Card No:"));
          LeftPanel.add(TCardNo);

          LeftPanel.add(new MyLabel("InDate"));
          LeftPanel.add(LInDate);

          LeftPanel.add(new MyLabel("Name"));
          LeftPanel.add(LName);

          LeftPanel.add(new MyLabel("Age"));
          LeftPanel.add(LAge);

          LeftPanel.add(new MyLabel("Sex"));
          LeftPanel.add(LSex);

          LeftPanel.add(new MyLabel("Qualification"));
          LeftPanel.add(LQuali);

          LeftPanel.add(new MyLabel("Place"));
          LeftPanel.add(LPlace);

//        LeftPanel.add(new MyLabel(""));
//        LeftPanel.add(new MyLabel(""));

          RightPanel.add(new MyLabel("InTime"));
          RightPanel.add(LInTime);

          RightPanel.add(new MyLabel("Category"));
          RightPanel.add(LCat);

          RightPanel.add(new MyLabel("AgentName"));
          RightPanel.add(LAgentName);

          RightPanel.add(new MyLabel("OutTime"));
          RightPanel.add(LOutTime);

          RightPanel.add(new MyLabel("OutDate"));
          RightPanel.add(LOutDate);

          RightPanel.add(new MyLabel("Hod"));
          RightPanel.add(JCHod);

          RightPanel.add(new MyLabel("Result"));
          RightPanel.add(JCResult);

          TotalPanel.add(LeftPanel);
          TotalPanel.add(RightPanel);

          MiddlePanel.add(BSave);
          MiddlePanel.add(BCancel);
          MiddlePanel.add(BExit);

          //TopPanel.add(TotalPanel);
          //TopPanel.add(MiddlePanel);


          BottomPanel.add("North",infoTable.getTableHeader());
          BottomPanel.add(tableScroll);

          FullPanel.add(TotalPanel);
          FullPanel.add(MiddlePanel);

          setSize(800,520);
          setVisible(true);

          setTitle("Interview Out Information(2.0)");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);


          getContentPane().setLayout(new BorderLayout());
          getContentPane().add(TotalPanel,"North");
          getContentPane().add(BottomPanel);
          getContentPane().add(MiddlePanel,"South");
          
          
     }

     private void addListeners()
     {
          BSave.addActionListener(new Insert());
          BCancel.addActionListener(new Insert());
          BExit.addActionListener(new Insert());
          TCardNo.addKeyListener(new keys());
     }
     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {

                    String SString = common.parseNull((TCardNo.getText()).trim());

                    sl   = SString;
                    //sl = Integer.parseInt(SString);

                    if(SString.equals(""))
                    {
                         JOptionPane.showMessageDialog(null,"Enetr Valid Slip Number");
                    }
                    else
                    {

                         if(checkCard(SString))
                         {
                              setValues(SString);

                         }
                         else
                         {
                              JOptionPane.showMessageDialog(null,"This Card No Already Out");
                         }
                    }
               }
          }
     }

     private boolean checkCard(String Sno)
     {
          boolean ok=false;
          try
          {
               int status = infoDomain.getInterviewCard(Sno);

               if(status==1)
                    ok=true;
          }                
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return ok;
     }

     private void setValues(String SSelected)
     {
          try
          {
               //int slno    = Integer.parseInt(SSelected);

               Vector  vVector = infoDomain.getValues(SSelected);
               

               LInDate.setText(common.parseDate((String)vVector.elementAt(0)));

               LName.setText((String)vVector.elementAt(1));
                               
               LAge.setText((String)vVector.elementAt(2));

               LSex.setText((String)vVector.elementAt(3));

               LQuali.setText((String)vVector.elementAt(4));

               LPlace.setText((String)vVector.elementAt(5));

               LInTime.setText((String)vVector.elementAt(6));

               LCat.setText((String)vVector.elementAt(7));

               LHod.setText((String)vVector.elementAt(8));

               LAgentName.setText((String)vVector.elementAt(9));

          }
          catch(Exception e)
          {
               JOptionPane.showMessageDialog(null,"There is No Matching Records");
               TCardNo.setText("");
               TCardNo.requestFocus();
               //e.printStackTrace();
          } 
     }

     private class Insert implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BSave)
               {
                    Insertvalues();
                    TCardNo.requestFocus();         

               }

               if(ae.getSource()==BCancel)
               {
                    clearFields();
                    TCardNo.requestFocus();
               }

               if(ae.getSource()==BExit)
               {
                    setVisible(false);
               }

          }
          
     }

     private void clearFields()
     {
          TCardNo.setText("");
          LInDate.setText("");
          LName.setText("");
          LAge.setText("");
          LSex.setText("");
          LQuali.setText("");
          LPlace.setText("");
          LInTime.setText("");
          LAgentName.setText("");
          LHod.setText("");
          LCat.setText("");
          TimeField t    = new TimeField();
          LOutTime.setText(t.getTimeNow());

          //LOutDate.setString("");
     }

     private void Insertvalues()
     {
          Vector infoVector   = new Vector();
          try
          {

               int iOutDate=0;
               String SDate        = LInDate.getText();
               int iDate           = common.toInt(common.pureDate(SDate));
               //int iDate           = Integer.parseInt(SDate);
               String SOutTime     = (String) LOutTime.getText();
               String SOutDate     = LOutDate.getText();
               iOutDate            = common.toInt(common.pureDate(SOutDate));

               //iOutDate            = Integer.parseInt(SOutDate);
               String SInTime      = LInTime.getText();
               int iResult=0;
               if(JCResult.getSelectedItem().equals("UnKnown"))
               {
                    iResult=0;
               }
               if(JCResult.getSelectedItem().equals("Selected"))
               {
                    iResult=1;
               }
               if(JCResult.getSelectedItem().equals("NotSelected"))
               {
                    iResult=2;
               }

               String SHod         = (String)JCHod.getSelectedItem();

               int id = infoDomain.setId(sl);


               infoDomain.getUpdateTimeAndDate(SOutTime,iOutDate,id,iResult,SHod);

               Vector iVector = new Vector();
               Vector VVector = new Vector();

               iVector = infoDomain.getOutValues(iOutDate);

               interviewModel.RemoveRow();

               int n=0;
               int isize= (iVector.size())/8;

               for(int i=0;i<isize;i++)
               {
                     //VVector.addElement(" ");
                     interviewModel.appendEmptyRow();
                     VVector.addElement(iVector.elementAt(0+n));
                     VVector.addElement(iVector.elementAt(1+n));
                     VVector.addElement(iVector.elementAt(2+n));
                     VVector.addElement(iVector.elementAt(3+n));
                     VVector.addElement(iVector.elementAt(4+n));
                     VVector.addElement(iVector.elementAt(5+n));
                     VVector.addElement(iVector.elementAt(6+n));
                     VVector.addElement(iVector.elementAt(7+n));
                     //interviewModel.insertData(VVector);
                     VVector.removeAllElements();
                     interviewModel.setValueAt(iVector.elementAt(0+n),i,1);
                     interviewModel.setValueAt(iVector.elementAt(1+n),i,2);
                     interviewModel.setValueAt(iVector.elementAt(2+n),i,3);
                     interviewModel.setValueAt(iVector.elementAt(3+n),i,4);
                     interviewModel.setValueAt(iVector.elementAt(4+n),i,5);
                     interviewModel.setValueAt(iVector.elementAt(5+n),i,6);
                     interviewModel.setValueAt(iVector.elementAt(6+n),i,7);
                     interviewModel.setValueAt(iVector.elementAt(7+n),i,8);
                     n=n+8;
               }

               clearFields(); 
               TCardNo.requestFocus();
                
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}
