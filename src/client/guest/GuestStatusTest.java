package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import util.ClockField;
import util.SerialReader;
import domain.jdbc.*;

import blf.*;

public class GuestStatusTest extends JInternalFrame  {

      protected            JLayeredPane layer;
      JButton              BSave,BCancel,BApply;
      int                  iUserCode;
      JPanel               TopPanel    ,MiddlePanel    ,BottomPanel;

      Connection           theConnection;
      Common               common   =   new Common();

      GuestStatusModelTest theModel;
      JTable               theTable;
      DateField            tdate;

      public GuestStatusTest(JLayeredPane layer,int iUserCode)   {

         this  . layer     = layer;
         this  . iUserCode = iUserCode;

         updateLookAndFeel();
         createComponents();
         setLayouts();
         addComponents();
         addListeners();
         theModel.setNumRows(0);
      }

      private void updateLookAndFeel() {

         String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

         try   {

              UIManager.setLookAndFeel(win);
         }  catch(Exception e) {

              e.printStackTrace();
         }
      }

      private void createComponents()  {

         try   {

               BottomPanel = new JPanel();
               MiddlePanel = new JPanel();
               TopPanel    = new JPanel();

               BSave       = new JButton("Save");
               BCancel     = new JButton("Exit");
               BApply      = new JButton("Apply");
               tdate       = new DateField();
               tdate       . setTodayDate();

               theModel    = new GuestStatusModelTest();
               theTable    = new JTable(theModel);

               TableColumnModel TC =  theTable.getColumnModel();
               for(int i=0;i<theModel.ColumnName.length;i++)   {

                 TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
               }
          }
          catch(Exception ex) {

            ex.printStackTrace();
          }
      }
      private void setLayouts()  {

         TopPanel    . setLayout(new FlowLayout());
         MiddlePanel . setLayout(new BorderLayout());
         MiddlePanel . setBorder(new TitledBorder("Token Data"));
         BottomPanel . setLayout(new FlowLayout());

         setTitle("Guest Token update(15.0)");
         setSize(340,330);
         setVisible(true);

         BSave    . setMnemonic('S');
         BCancel  . setMnemonic('C');

         setIconifiable (true);
         setMaximizable (true);
         setClosable    (true);
         setResizable   (true);
      }

      private void addComponents()  {

         TopPanel    .   add(tdate);
         TopPanel    .   add(BApply);
   
         MiddlePanel .   add("Center",new JScrollPane(theTable));

         BottomPanel .   add(BSave);
         BottomPanel .   add(BCancel);

//         getContentPane().setLayout(new BorderLayout());
   
         getContentPane().add(TopPanel,"North");
         getContentPane().add(MiddlePanel,"Center");
         getContentPane().add(BottomPanel,"South");
   
      }
      private void addListeners()   {

//          BSave   .   addActionListener(new ActList());
//          BCancel .   addActionListener(new ActList());
      }
}
