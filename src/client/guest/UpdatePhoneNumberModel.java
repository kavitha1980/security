import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;

public class UpdatePhoneNumberModel extends DefaultTableModel
{

     String ColumnName[] = {"HrdTicketNo"    ,"EmpName"   ,"Sex"  ,"Address1"   ,"Address2"    ,"Address3"    ,"Address4"  ,"PhoneNo"    ,"Tick"   };
     String ColumnType[] = {"S"              ,"S"         ,"S"        ,"S"           ,"S"          ,"S"       ,"S"         ,"E"          ,"B"      };
     int  iColumnWidth[] = {80               ,140         ,30        ,115            ,115          ,115       ,115          ,100         ,35       };

     public UpdatePhoneNumberModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
