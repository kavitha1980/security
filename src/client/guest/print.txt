     InputStream in = new FileInputStream("d:\\barcodes.pdf");
        DocFlavor flavor = DocFlavor.INPUT_STREAM.PDF;

        // find the printing service
        AttributeSet attributeSet = new HashAttributeSet();
        attributeSet.add(new PrinterName(defaultPrinter, null));
        attributeSet.add(new Copies(1));

        PrintService[] services = PrintServiceLookup.lookupPrintServices(
                DocFlavor.INPUT_STREAM.PDF, attributeSet);

        //create document
        Doc doc = new SimpleDoc(in, flavor, null);

        // create the print job
        PrintService service = services[0];
        DocPrintJob job = service.createPrintJob();

        // monitor print job events
        PrintJobWatcher watcher = new PrintJobWatcher(job);

        System.out.println("Printing...");
        job.print(doc, null);

        // wait for the job to be done
        watcher.waitForDone();
        System.out.println("Job Completed!!");
