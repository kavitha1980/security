package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;


import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import util.ClockField;
import util.SerialReader;
import domain.jdbc.*;


import blf.*;


import java.awt.Color;
import java.io.FileOutputStream;

import com.lowagie.text.*;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;

import com.lowagie.text.Element;   
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.BarcodeEAN;
import com.lowagie.text.pdf.BarcodeEANSUPP;
import com.lowagie.text.pdf.BarcodeInter25;
import com.lowagie.text.pdf.BarcodePostnet;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPCell;

import com.lowagie.text.pdf.PdfAction;

import com.lowagie.text.pdf.BaseFont;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;
import java.awt.image.*;
import javax.imageio.ImageIO;



public class GuestInFrameNew extends JInternalFrame implements rndi.CodedNames
{

    PdfContentByte contentByte;

     protected JLayeredPane layer;

     protected boolean bFlag=false;

     DateField1 DInDate,DOutDate;

     MyTextField TNoPersons;

     NameField NGuestName,NGuestPlace;

     MyComboBox JCCategory,JCSex,JCDepartment;

     MyComboBox JCToMeet;

     MyTextField EmpName;

     MyComboBox JCTea,JCTiffen,JCLunch;

     JButton BSave,BCancel;

     JPanel LeftPanel,RightPanel,BottomPanel,TablePanel,TotalPanel,TopPanel,FullPanel;

     JTable InfoTable;
     MyLabel LHostel,LFatherName,LMotherName,LDistrict,LDepartment,LNoPersons;
     JScrollPane InfoPane;
     Common common = new Common();
     GuestInModel gInModel;
     int iInDate=0;
     Info infoDomain;
	 
	 int iFeedCount=0;

     String STime;
//     ClockField CInTime;

     TimeField time;

     Connection theConnection;

     Vector VCategoryCode,VToMeetCode,VDepartmentCode,VDeptCode,VDeptName,VAllDept,VEmpCode,VMeet;

     //MyLabel LSlipNo;

     MyTextField LCardNo;

     int slno=0;

     Vector VFather,VMother,VDistrict,VHostel,VHostelName,VHostelCode;

     boolean okflag=true;

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser     = "gate";
     String SPassword = "gatepass";

     String SPort = "LPT2";

//     String SPort = "USB001";


     FileWriter      FW;
     //PrinterComm   Printer;
     File            file;
     JCheckBox       CToken;
     int             iUserCode;
     
     public GuestInFrameNew(JLayeredPane layer,int iUserCode)
     {
          this.layer       =layer;
          this.iUserCode   =iUserCode;

          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     /*private void setSlip()
     {
          try
          {
               slno = infoDomain.getGuestSlipNo();
               slno =slno+1;
               //LSlipNo.setText(""+slno);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }*/

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void createComponents()
     {
          try
          {
               VEmpCode =   new Vector();
               VMeet    =   new Vector();

/*               VHostelName          = new Vector();
               VHostelCode          = new Vector();
               
               Vector VVector        = infoDomain.getHostelCode();
               int m=0;

               for(int i=0;i<VVector.size();i++)
               {
                    VHostelCode.addElement(VVector.elementAt(0+m));
                    VHostelName.addElement(VVector.elementAt(1+m));
                    m=m+2;
               }
*/ 
               LeftPanel      = new JPanel();
               RightPanel     = new JPanel();
               TopPanel       = new JPanel();
               TotalPanel     = new JPanel();
               BottomPanel    = new JPanel();
               TablePanel    = new JPanel();
               //FullPanel      = new JPanel();


               //LSlipNo      = new MyLabel();
               LCardNo        = new MyTextField(10);
               LCardNo.addKeyListener(new keys());
               LCardNo.addFocusListener(new cardFocus());

               JCCategory     = new MyComboBox(infoDomain.getCategory());
               JCCategory.addItem("WORKER");

               JCSex          = new MyComboBox();

               Vector VTotal  = infoDomain.getToMeetCode();
               VMeet          = (Vector)VTotal.elementAt(0);
               VFather        = (Vector)VTotal.elementAt(1);
               VMother        = (Vector)VTotal.elementAt(2);
               VDistrict      = (Vector)VTotal.elementAt(3);
               VDeptCode      = (Vector)VTotal.elementAt(4);
               VHostel        = (Vector)VTotal.elementAt(5);
               VEmpCode       = (Vector)VTotal.elementAt(6);

               System.out.println("VMeet size--"+VMeet.size());
               System.out.println("VCode  size--"+VEmpCode.size());


               VAllDept       = infoDomain.getHodCode();
               VDeptName      = infoDomain.getHod();

               JCToMeet       = new MyComboBox(VMeet);
               JCDepartment   = new MyComboBox(VDeptName);

               LFatherName    = new MyLabel();
               LMotherName    = new MyLabel();
               LDistrict      = new MyLabel();
               LDepartment    = new MyLabel();
               LHostel        = new MyLabel();

               LNoPersons     = new MyLabel("No.of Persons");
               
               time = new TimeField();
               STime = time.getTimeNow();
               
               //CInTime = new ClockField();

               NGuestPlace    = new NameField(30);
               NGuestName     = new NameField(40);
     
               DInDate        = new DateField1();
               DOutDate       = new DateField1();

               TNoPersons     = new MyTextField(4);
     
               DInDate.setTodayDate();
               String SinDate = DInDate.toNormal();
               iInDate    = Integer.parseInt(SinDate);

               DOutDate.setTodayDate();
               String SoutDate = DOutDate.toNormal();
               int iOutDate   = Integer.parseInt(SoutDate);

               BSave          = new JButton("Save");
               BCancel        = new JButton("Close");

               CToken         =    new JCheckBox("Token");

               VCategoryCode     = new Vector();
               VToMeetCode       = new Vector();
               EmpName           = new MyTextField(20);

               VDepartmentCode = new Vector();

               Vector initVector = infoDomain.getGuestInInitValues(iInDate);
               gInModel   = new GuestInModel(initVector);

               InfoTable      = new JTable(gInModel);  
               InfoPane       = new JScrollPane(InfoTable);

				JCTea         = new MyComboBox(); 
               JCTiffen       = new MyComboBox();
               JCLunch        = new MyComboBox();
			   
			   
			   JCTea       . addItem("1");
               JCTea       . addItem("2");
               JCTea       . addItem("3");

               JCTiffen       . addItem("");
               JCTiffen       . addItem("1");
               JCTiffen       . addItem("2");

               JCLunch        . addItem("");
               JCLunch        . addItem("1");
               JCLunch        . addItem("2");

          }

          catch(Exception e)
          {
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("vector.txt")));
                    e.printStackTrace(out);
                    out.close();
               }
               catch(Exception ex)
               {
                    System.out.println("Error");
               }
          }

     }

     private class cardFocus extends FocusAdapter
     {
               public void focusLost(FocusEvent fe)
               {
                    okflag=true;
                    try
                    {
                         String SSCard  = LCardNo.getText().trim();
                         String QS      = "Select CheckCard from GuestTable where CardNo='"+SSCard+"' And Status=0 ";
                         int st         = infoDomain.getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              LCardNo.setText("");
                              LCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
                    
                    //NGuestName.requestFocus();
               }
     }

     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    okflag=true;
                    try
                    {
                         String SSCard = LCardNo.getText().trim();
                         String QS     = "Select CheckCard from GuestTable where CardNo='"+SSCard+"' And Status=0 ";
                         int st  =infoDomain.getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              LCardNo.setText("");
                              LCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
               }
          }
     }

     private boolean CheckFields()
     {
          try
          {
               bFlag=true;
     
               String SLCardNo   =    common.parseNull(LCardNo.getText().trim());
     
               if(SLCardNo.equals(""))
               {
                    bFlag=false;
                    LCardNo.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
     
               String SNGuestName  =    common.parseNull(NGuestName.getText().trim());          
               if(SNGuestName.equals(""))
               {
                    bFlag=false;
                    NGuestName.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
     
               String SNGuestPlace =    common.parseNull(NGuestPlace.getText().trim());

               if(SNGuestPlace.equals(""))
               {
                    bFlag=false;
                    NGuestPlace.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }

               String SNoPersons =    common.parseNull(TNoPersons.getText().trim());

               if(SNoPersons.equals(""))
               {
                    bFlag=false;
                    TNoPersons.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
               if(JCToMeet.getSelectedItem().equals(""))
               {
                    bFlag=false;
                    JCToMeet.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
          }
          catch(Exception e)
          {
               JOptionPane.showMessageDialog(null,"Fields cannot be empty");
               if(LCardNo.equals("")) LCardNo.requestFocus();
               if(NGuestName.equals("")) NGuestName.requestFocus();
               if(NGuestPlace.equals("")) NGuestPlace.requestFocus();
               if(TNoPersons.equals("")) TNoPersons.requestFocus();
               
          }
          return bFlag;
     }

     private void setLayouts()
     {
          //FullPanel  .setLayout(new BorderLayout());
          TopPanel   .setLayout(new GridLayout(2,1));
          TotalPanel .setLayout(new GridLayout(1,2));
          LeftPanel  .setLayout(new GridLayout(11,2,2,2));
          RightPanel .setLayout(new GridLayout(12,2,2,2));
          TablePanel.setLayout(new BorderLayout());  

          LeftPanel  .setBorder(new TitledBorder("Personal Info"));
          RightPanel .setBorder(new TitledBorder("General Info"));
          TablePanel.setBorder(new TitledBorder("Today's Guest Info" ));
//          BottomPanel.setBorder(new TitledBorder("Control Panel"));

          setTitle("GuestInFrame(14.0)");
          setSize(795,520);
          setVisible(true);

          BSave.setMnemonic('S');
          BCancel.setMnemonic('c');

          setIconifiable(true);
          setMaximizable(true);
          setClosable(true);
          setResizable(true);

          JCToMeet.addItemListener(new ComboEvents());

          JCCategory.setEditable(false);
          JCSex.setEditable(false);
          JCToMeet.setEditable(false);
          JCDepartment.setEditable(false);


     }

     private void addComponents()
     {
          JCSex.addItem("Male");
          JCSex.addItem("Female");

          LeftPanel.add(new MyLabel("CardNo"));
          LeftPanel.add(LCardNo);
          LeftPanel.add(new MyLabel("InDate"));
          LeftPanel.add(DInDate);
          LeftPanel.add(new MyLabel("Guest Name"));
          LeftPanel.add(NGuestName);
          LeftPanel.add(new MyLabel("Sex"));
          LeftPanel.add(JCSex);
          LeftPanel.add(new MyLabel("Guest Place"));
          LeftPanel.add(NGuestPlace);
          LeftPanel.add(LNoPersons);
          LeftPanel.add(TNoPersons);
          LeftPanel.add(new MyLabel("InTime"));
          LeftPanel.add(new MyLabel(STime));
          LeftPanel.add(new MyLabel(""));
          LeftPanel.add(new MyLabel(""));
          LeftPanel.add(new MyLabel(""));
          LeftPanel.add(new MyLabel(""));
		  LeftPanel.add(new MyLabel(""));
          LeftPanel.add(new MyLabel(""));

          

          RightPanel.add(new MyLabel("Category"));
          RightPanel.add(JCCategory);

          RightPanel.add(new MyLabel("To Meet"));
          RightPanel.add(JCToMeet);

          RightPanel.add(new MyLabel("Emp Name"));
          RightPanel.add(EmpName);

          RightPanel.add(new MyLabel("FatherName:"));
          RightPanel.add(LFatherName);

          RightPanel.add(new MyLabel("MotherName"));
          RightPanel.add(LMotherName);

          RightPanel.add(new MyLabel("District"));
          RightPanel.add(LDistrict);

          RightPanel.add(new MyLabel("Department"));
          RightPanel.add(LDepartment);

          RightPanel.add(new MyLabel("Hostel"));
          RightPanel.add(LHostel);

//          RightPanel.add(new MyLabel(""));
//          RightPanel.add(new MyLabel(""));

		  RightPanel.add(new MyLabel("Tea"));
          RightPanel.add(JCTea);
		  
          RightPanel.add(new MyLabel("Tiffen"));
          RightPanel.add(JCTiffen);

          RightPanel.add(new MyLabel("Lunch"));
          RightPanel.add(JCLunch);        


          TotalPanel.add(LeftPanel);
          TotalPanel.add(RightPanel);

          BottomPanel.add(CToken);
          BottomPanel.add(BSave);
          BottomPanel.add(BCancel);


          TopPanel.add(TotalPanel);
//          TopPanel.add(BottomPanel);

          TablePanel.add(InfoTable.getTableHeader());
          TablePanel.add(InfoPane);

          getContentPane().setLayout(new BorderLayout());
          getContentPane().add(TotalPanel,"North");
          getContentPane().add(TablePanel,"Center");
          getContentPane().add(BottomPanel,"South");

          DOutDate.setEditable(false);
          DInDate.requestFocus();

          //getContentPane().add(FullPanel);


	}

	public void addListeners()
	{
		BSave.addActionListener(new addAction());
          BCancel.addActionListener(new addAction());
	}
     private class ComboEvents implements ItemListener
     {
          public void itemStateChanged(ItemEvent a)
          {
               if(a.getSource()==JCToMeet)
               {
                    try
                    {
                         int index      = JCToMeet.getSelectedIndex();

                         if(index==0)
                         {
                              LFatherName.setText("");
                              LMotherName.setText("");
                              LDistrict.setText("");
                              LDepartment.setText("");
                              LHostel.setText("");
                              EmpName.setText("");


                         }
                         else
                         {
                              LFatherName.setText((String)VFather.elementAt(index-1));
                              LMotherName.setText((String)VMother.elementAt(index-1));
                              LDistrict.setText((String)VDistrict.elementAt(index-1));
                              int iDIndex    = getIndex(VDeptCode,index);
     
                              LDepartment.setText((String)VDeptName.elementAt(iDIndex));

//                              int iHIndex    = getHostelIndex(VHostelCode,index);
                              LHostel.setText((String)VHostel.elementAt(index-1));

                              if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
                              {
                                 EmpName.setText((String)JCToMeet.getSelectedItem());
                              }

                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }

               }
          }
     }
     private int getIndex(Vector v,int index)
     {
          int code       = Integer.parseInt((String)v.elementAt(index-1));

          int iDeptIndex = VAllDept.indexOf(String.valueOf(code));

          return iDeptIndex;
     }

/*     private int getHostelIndex(Vector v1,int index)
     {
          int code1       = Integer.parseInt((String)v1.elementAt(index-1));

          int iHIndex = VHostelName.indexOf(String.valueOf(code1));

          return iHIndex;
     }
*/

     public class addAction implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			if(ae.getSource()==BSave)
			{
                    bFlag = true;

                    if(okflag)
                    {
                         if(CheckFields())
                         {
                              BSave.setEnabled(true);
                              Insertvalues();
                              LCardNo.requestFocus();


                              if(CToken.isSelected())
                              {

                                   String SEmpCode=    getEmpCode((String)JCToMeet.getSelectedItem());
                                   String SStatus =    getEmpStatus(SEmpCode);

                                   if(SEmpCode.equals("0"))
                                       SStatus = "";

                                   if(SStatus.equals(""))
                                   {
                                       if(((String)JCCategory.getSelectedItem()).equals("WORKER")) {

                                              System.out.println("commong print");
                                              //setPrintData();

                                              //setPrintDataNew();
											  setPrintDataNew2();

                                      }
                                   }
                                   else {
                                     JOptionPane.showMessageDialog(null,"This Emp Already Allocated ","Message",JOptionPane.ERROR_MESSAGE,null);
                                   } 
                              }
                         }
                         else
                         {
                              //BSave.setEnabled(false);
                              JOptionPane.showMessageDialog(null,"Fields cannot be empty","Message",JOptionPane.ERROR_MESSAGE,null);
                         }
                    }
                    else
                    {
                         LCardNo.requestFocus();
                         JOptionPane.showMessageDialog(null,"Plz Change the CardNo","Message",JOptionPane.ERROR_MESSAGE,null);
                    }  

               }

               if(ae.getSource()==BCancel)
               {                    
                    setVisible(false);
               }
          }
	}

    private String getEmpCode(String SEmp)
    {
        int iIndex  =   VMeet.indexOf(SEmp);

        if(iIndex!=-1)
            return (String)VEmpCode.elementAt(iIndex);
        return "";
    }

     public void Insertvalues()
     {
          Vector infoVector = new Vector();

          try
          {
               DInDate.setTodayDate();
               String SDate             = DInDate.toNormal();
               int    IInDate           = common.toInt(common.pureDate(SDate));
               String SODate            = DOutDate.toNormal();
               int    iOutDate          = common.toInt(common.pureDate(SODate));
               String SGuestName        = NGuestName.getText();
               String SGuestPlace       = NGuestPlace.getText();
               String SNoPersons        = TNoPersons.getText();
//               String SInTime           = CInTime.getText();
//               String SOutTime          = CInTime.getText();
               String SInTime           = STime;
               String SOutTime          = STime;
               String SToMeet           = (String)JCToMeet.getSelectedItem();
               String SSex              = (String)JCSex.getSelectedItem();
               String SDepartment       = common.parseNull((String)LDepartment.getText());   
               String SCategory         = (String)JCCategory.getSelectedItem();

               String sCard             = LCardNo.getText();
               //int    slipno               = Integer.parseInt(sCard);

               infoVector.addElement(String.valueOf(IInDate));
               infoVector.addElement(String.valueOf(sCard));
               infoVector.addElement(String.valueOf(SGuestName));
               infoVector.addElement(String.valueOf(SSex));
//               infoVector.addElement(String.valueOf(CInTime));
//               infoVector.addElement(String.valueOf(CInTime));
               infoVector.addElement(String.valueOf(STime));
               infoVector.addElement(String.valueOf(STime));
               infoVector.addElement(String.valueOf(SGuestPlace));
               infoVector.addElement(String.valueOf(SNoPersons));
               infoVector.addElement(String.valueOf(SToMeet));
               infoVector.addElement(String.valueOf(SDepartment));
               infoVector.addElement(String.valueOf(SCategory));


               try
               {
                    String QS =  " insert into GuestTable fields(InDate,CardNo,GuestName,Sex,GuestPlace,NoPersons,ToMeet,InTime,Category,Department,OutDate,OutTime,status,SlipNo,checkcard) values("+iInDate+",'"+sCard+"','"+SGuestName+"','"+SSex+"','"+SGuestPlace+"',"+SNoPersons+",'"+SToMeet+"','"+STime+"','"+SCategory+"','"+SDepartment+"',"+iOutDate+",'"+STime+"',0,"+slno+",0) ";
                    Class.forName(SDriver);                         
                    theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
                    Statement stat     = theConnection.createStatement();
                    stat.executeUpdate(QS);
                    theConnection.close();

                    //infoDomain.getGuestInsertData(QS);
                    JOptionPane.showMessageDialog(null,"Record saved","Message",JOptionPane.NO_OPTION,null);

                    LCardNo.setText("");
                    NGuestName.setText("");
                    NGuestPlace.setText("");
                    TNoPersons.setText("");
                    LCardNo.requestFocus();
                }

               catch(Exception ex)
               {
                    try
                    {
                         PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("temp.txt")));
                         out.println(ex);
                         out.close();
                    }
                    catch(Exception ex1)
                    {
                         ex1.printStackTrace();
                    }
               }

               infoVector = infoDomain.getGuestInInitValues(iInDate);
               gInModel.setVector1(infoVector);

               //setSlip();

               STime = time.getTimeNow();
          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void setPrintDataNew()
     {
          //int iLimit     =3;
		  int iLimit     =0;
          try
          {
               int  iTiffen   =    common.toInt((String)JCTiffen.getSelectedItem());
               int  iLunch    =    common.toInt((String)JCLunch.getSelectedItem());
			   iLimit		  =    common.toInt((String)JCTea.getSelectedItem());		
               int  iTot      =    iTiffen + iLunch;

               System.out.println("iTot      ->"+iTot);
               System.out.println("iTiffen   ->"+iTiffen);
               System.out.println("iLunch    ->"+iLunch);
			   
			   iFeedCount=0;


               Document document = new Document(PageSize.A4, 60, 100, 0, 22); //leftMargin, rightMargin, topMargin, bottomMargin


               /* Step 2: Create PDF Writer*/
               PdfWriter writer = PdfWriter.getInstance(document,
                         new FileOutputStream("d:\\BarCode1.pdf"));

               /* Step 3: Open the document so that we can write over it.*/
               document.open();

               /* Step 4: We have to create a set of contents.*/
               contentByte = writer.getDirectContent();

               writer.setOpenAction(new PdfAction(PdfAction.PRINTDIALOG));
               //int iPos=800;
			   //int iPos;
               int icnt=0;
               if(iTot<=4)    
			   {

                   // System.out.println("comming to Print"+iLimit);

                    for(int i=0;i<iLimit;i++)
                    {
                         String SId     = getMaxFoodId();
                         String SName   = "";

                         String SType = "Tea";

                         SId            = getId(SId);
                         String SId1    = SId;
						 int iPos = 1000;

                        // System.out.println("comming to Tea");

                         if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
                         {
                           SName = "Name:"+(String)JCToMeet.getSelectedItem();
                         }
                         else
                         {
                           SName = "Name:(NEW)"+(String)EmpName.getText()+"\n";
                         }
						 
						 
						 
                         document.add(new Paragraph("GUEST TOKEN \n"));
                         document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
                         document.add(new Paragraph(SName));

                         java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage("d:\\token\\tea.png");

                         PdfPTable table = new PdfPTable(1);
						 

                      //   System.out.println("Id--->"+SId1);

                         table.setWidthPercentage(100);
                         table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                         table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                         table.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
                         table.getDefaultCell().setFixedHeight(80);
                         table.addCell(new Phrase(new Chunk(createBarCode39(SId1.toUpperCase()), 0, 0)));
                         document.add(table);
						 document.add(new Paragraph(" "));
						 if(iLimit==2)
						 {
						 	document.add(new Paragraph(" "));
						 }
                         int iLess;
                         if(i==1) 
						 {
                              iLess=360;
							  iPos= iPos-iLess;
                         }
						 else if(i==2)
						 {
                         	iLess=510;
							iPos= iPos-iLess;
						 }	
						 else
						 {
						 	iPos= iPos-210;
						 }
						//  System.out.println("Iposition == : "+iPos);
                         Image image = Image.getInstance("d:\\token\\tea.png");
                         image.setAbsolutePosition(155,iPos);
                         image.setAlignment(Image.LEFT);
                         contentByte.addImage(image);

                        // insertData(SId1,SType); 
                    }
                         document.newPage();
	     				 document.add(new Paragraph(" "));
		     			 document.add(new Paragraph(" "));


                   // iPos=1000;
                    if(iTiffen>0)  
					{
                         for(int j=0;j<iTiffen;j++)    
						 {                        
							 int iPos=1000;
							 int iLess; 	
							 icnt++;	
							 String SId     = getMaxFoodId();
							 String SName   = "";	
							 SId            = getId(SId);
							 String SId1    = SId;		
							 String SType   ="Tiffen";

							 if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
							 {
							   SName = "Name:"+(String)JCToMeet.getSelectedItem();
							 }
							 else
							 {
							   SName = "Name:(NEW)"+(String)EmpName.getText()+"\n";
							 }
	
							 document.add(new Paragraph("GUEST TOKEN \n"));
							 document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
							 document.add(new Paragraph(SName));
	
							 java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage("d:\\token\\tiffen.png");	
							 PdfPTable table = new PdfPTable(1);	
							 table.setWidthPercentage(102);
							 table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
							 table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
							 table.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
							 table.getDefaultCell().setFixedHeight(80);
	
							 table.addCell(new Phrase(new Chunk(createBarCode39(SId1.toUpperCase()), 0, 0)));               
							 document.add(table);
							 document.add(new Paragraph(" "));
							 if(j==2)
						 	 {
						 		document.add(new Paragraph(" "));
						 	 }
							 Image image = Image.getInstance("d:\\token\\tiffen.png");

							 if(j==1) 
							 {
								  iLess=410;
								  iPos= iPos-iLess;
							 }
							 else if(j==2)
							 {
								iLess=570;
								iPos= iPos-iLess;
							 }	
							 else
							 {
								iPos= iPos-250;								
							 }
							 image.setAbsolutePosition(155, iPos);	
							 image.setAlignment(Image.LEFT);
							 contentByte.addImage(image); 
	                         //insertData(SId1,SType);

                              if(icnt>3) 
							  {
                               document.newPage();
                              }
                         }
                    }
                    if(iLunch>0)
					{
                         for(int k=0;k<iLunch;k++)    
						 {                             							
							 String SId     = getMaxFoodId();
	                         String SType   ="Lunch";

							 String SName   = "";	
							 SId            = getId(SId);
							 String SId1    = SId;
							 int iLess;
							 int iPos=1000;
                              icnt++;
                              if(icnt>3) 
							  {
									document.newPage();									
			  						 document.add(new Paragraph(" "));
									 document.add(new Paragraph(" "));

									icnt=0;
									if(k==0)
						 			{
                              			iLess=270;
							  			iPos= iPos-iLess;
										System.out.println("Iposition in Lunch NewPage K==0: "+iPos);
                         			}
						 			if(k==1)
						 			{
                         				iPos= iPos-270;
										System.out.println("Iposition in Lunch NewPage K==1: "+iPos);
						 			}	
						 			if(k==2)
						 			{
						 				iPos= iPos-580;
										System.out.println("Iposition in Lunch NewPage K==2: "+iPos);
						 			}
                              }
							  else
							  {
							  	if(iTiffen==1 && k==0)
							  	{							  		
									iPos= iPos-370;
							  	}
								else if(iTiffen==1 && k==1)
								{
									iPos= iPos-570;
								}
							  	else if(iTiffen==2 && k==0)
							  	{
							  		iPos= iPos-570;
							 	}							  
							  	else
							  	{
									if(k==0)
						 			 {
                         				iPos= iPos-210;
										System.out.println("Iposition in Lunch Not new page k==0 : "+iPos);
                         			 }
									if(k==1)
									 {
            			             	iPos= iPos-360;
										System.out.println("Iposition in Lunch Not new page K==1: "+iPos);
                        			 }
									 if(k==2)
									 {
									 		iPos= iPos-510;
											System.out.println("Iposition in Lunch Not new page K==2: "+iPos);
									 }						 			 
							  	}
							}	
                         if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
                         {
                           SName = "Name:"+(String)JCToMeet.getSelectedItem();
                         }
                         else
                         {
                           SName = "Name:(NEW)"+(String)EmpName.getText()+"\n";
                         }
						 

                         document.add(new Paragraph("GUEST TOKEN \n"));
                         document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
                         document.add(new Paragraph(SName));
                         java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage("d:\\token\\lunch.png");
                         PdfPTable table = new PdfPTable(1);
                         table.setWidthPercentage(102);
                         table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                         table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                         table.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
                         table.getDefaultCell().setFixedHeight(80);
                         table.addCell(new Phrase(new Chunk(createBarCode39(SId1.toUpperCase()), 0, 0)));               
                         document.add(table);
						 document.add(new Paragraph(" "));
						 if(k==2)
						 {
						 		document.add(new Paragraph(" "));
						 }
                         Image image = Image.getInstance("d:\\token\\lunch.png");
						 System.out.println("Iposition in :: "+iPos);
                         image.setAbsolutePosition(155, iPos);
                         image.setAlignment(Image.LEFT);
                         contentByte.addImage(image);
//                         insertData(SId1,SType);              
                         }
                    }
               }
	// step 5: we close the document
	document.close();

          Process p = Runtime
			   .getRuntime()
                  .exec("rundll32 url.dll,FileProtocolHandler  d:\\barcode1.pdf");
               p.waitFor(); 
          }
          catch(Exception ex)
          {
		  	ex.printStackTrace();
          }
     } 
	 private void setPrintDataNew2()
     {
          //int iLimit     =3;
		  int iLimit     =0;
          try
          {
               int  iTiffen   =    common.toInt((String)JCTiffen.getSelectedItem());
               int  iLunch    =    common.toInt((String)JCLunch.getSelectedItem());
			   iLimit		  =    common.toInt((String)JCTea.getSelectedItem());		
               int  iTot      =    iTiffen + iLunch;

               System.out.println("iTot      ->"+iTot);
               System.out.println("iTiffen   ->"+iTiffen);
               System.out.println("iLunch    ->"+iLunch);


               //Document document = new Document(PageSize.A4, 60, 100, 0, 22); //leftMargin, rightMargin, topMargin, bottomMargin
			   Rectangle rect = new Rectangle(195f,120f);
			   Document document = new Document(rect,1,1,0,0);   //Document(pgSize, leftMargin, rightMargin, topMargin, bottomMargin);


               /* Step 2: Create PDF Writer*/
               PdfWriter writer = PdfWriter.getInstance(document,
                         new FileOutputStream("d:\\BarCode1.pdf"));

               /* Step 3: Open the document so that we can write over it.*/
               document.open();

               /* Step 4: We have to create a set of contents.*/
               contentByte = writer.getDirectContent();

               writer.setOpenAction(new PdfAction(PdfAction.PRINTDIALOG));
               //int iPos=800;
			   //int iPos;
               int icnt=0;
               if(iTot<=4)    
			   {
                    for(int i=0;i<iLimit;i++)
                    {
                         String SId     = getMaxFoodId();
                         String SName   = "";
                         String SType = "Tea";

                         SId            = getId(SId);
                         String SId1    = SId;
                         if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
                         {
                           SName = "Name:"+(String)JCToMeet.getSelectedItem();
                         }
                         else
                         {
                           SName = "Name:(NEW)"+(String)EmpName.getText();
                         }
						 Image image = Image.getInstance("d:\\token\\tea.png");
						 Image imgbar;
                         document.add(new Paragraph("GUEST TOKEN \n"));
                         document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
                         document.add(new Paragraph(SName));
						 document.add(new Chunk(image, 100f, 30f));
						 imgbar = createBarCode39(SId1.toUpperCase());	 
						 document.add(new Chunk(imgbar, 0f, -25f));
						 //document.add(new Chunk(imgbar, 0f, -10f));						 						 
						 document.newPage(); 
						  insertData(SId1,SType); 						
                    }
					if(iTiffen>0)  
					{
                         for(int j=0;j<iTiffen;j++)    
						 {                        
							 String SId     = getMaxFoodId();
							 String SName   = "";	
							 SId            = getId(SId);
							 String SId1    = SId;	
							 String SType   ="Tiffen";	
	
							 if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
							 {
							   SName = "Name:"+(String)JCToMeet.getSelectedItem();
							 }
							 else
							 {
							   SName = "Name:(NEW)"+(String)EmpName.getText()+"\n";
							 }
							 Image image = Image.getInstance("d:\\token\\tiffen.png");
							 Image imgbar;
							 document.add(new Paragraph("GUEST TOKEN \n"));
							 document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
							 document.add(new Paragraph(SName));
							 document.add(new Chunk(image, 100f, 30f));
							 imgbar = createBarCode39(SId1.toUpperCase());	 
							 document.add(new Chunk(imgbar, 0f, -25f));						 						 
							 document.newPage(); 
							  insertData(SId1,SType); 						
						}
					}	
					if(iLunch>0)  
					{
							for(int k=0;k<iLunch;k++)    
							{
								 String SId     = getMaxFoodId();
								 String SType   ="Lunch";
								 String SName   = "";
								 SId            = getId(SId);
								 String SId1    = SId;
								 if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
								 {
								   SName = "Name:"+(String)JCToMeet.getSelectedItem();
								 }
								 else
								 {
								   SName = "Name:(NEW)"+(String)EmpName.getText()+"\n";
								 }
								 Image image = Image.getInstance("d:\\token\\lunch.png");
								 Image imgbar;
								 document.add(new Paragraph("GUEST TOKEN \n"));
								 document.add(new Paragraph("Dt  :"+common.parseDate(common.getCurrentDate())+"\n"));
								 document.add(new Paragraph(SName));
								 document.add(new Chunk(image, 100f, 30f));
								 imgbar = createBarCode39(SId1.toUpperCase());			 
								 document.add(new Chunk(imgbar, 10f, -10f));						 						 
								 document.newPage(); 
								  insertData(SId1,SType); 						
							}
					}
               }
				document.close();
				Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler  d:\\barcode1.pdf");
				p.waitFor(); 
          }
          catch(Exception ex)
          {
		  	ex.printStackTrace();
          }
     }
/*     private void setPrintData()
     {
          int iLimit     =3;
          try
          {
               int  iTiffen   =    common.toInt((String)JCTiffen.getSelectedItem());
               int  iLunch    =    common.toInt((String)JCLunch.getSelectedItem());

               int  iTot      =    iTiffen + iLunch;

               System.out.println("iTot      ->"+iTot);
               System.out.println("iTiffen   ->"+iTiffen);
               System.out.println("iLunch    ->"+iLunch);

               if(iTot<=2)    {

                    for(int i=0;i<iLimit;i++)
                    {

                         String SType = "Tea";
                         doPrint(SType);
                    }
                    if(iTiffen>0)  {
                         for(int j=0;j<iTiffen;j++)    {
                              doPrint("Tiffen");
                         }
                    }
                    if(iLunch>0)  {
                         for(int k=0;k<iLunch;k++)    {

                              doPrint("Lunch");
                         }
                    }
               }
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     } */

     private void  doPrint(String SType)
     {
          try
          {
              ParallelPort serialPort = getSerialPort();
              OutputStream output = serialPort.getOutputStream();

              String SId     = getMaxFoodId();

              SId            = getId(SId);

              String SId1    = SId.substring(1,SId.length());

              String str1 = "@ G     AMARJOTHI SPINNING MILLS LTD\n\n";
              String str2 = "!H fh-wkF08080400000"+SId1+"p\n";
              String str3 = "@ GId  :"+SId+"\n";
              String str0 = "@ GDt  :"+common.parseDate(common.getCurrentDate())+"\n";

              String str4 = "";

              if(!((String)JCToMeet.getSelectedItem()).equals("UNKNOWN"))
              {
                 str4 = "@ GName:"+(String)JCToMeet.getSelectedItem()+"\n";
              }
              else
              {
                 str4 = "@ GName:(NEW)"+(String)EmpName.getText()+"\n";
              }

              String str5 = "! G       "+SType+"!\n\n\n\n\n";
              String str6 = "m";

              byte buf[] =null;
                   
              int bytesread;

/*              File f;

              if(SType.equals("Tiffen")) {

                    f   = new File("D:\\Tiffen.gif");

               } else {

                    f   = new File("D:\\Tea.gif");
               }

               FileInputStream fis = new FileInputStream(f);
               ByteArrayOutputStream out  = new ByteArrayOutputStream();
               while((bytesread= fis.read())!=-1)
               {
                  out.write(bytesread);
               }
               buf = out.toByteArray();
               fis . close(); */
          

              output.write(str1.getBytes());
              output.write(str2.getBytes());
              output.write(str3.getBytes());
              output.write(str0.getBytes());
              output.write(str4.getBytes());
              output.write(str5.getBytes());

//              output.write(buf);
              output.write(str6.getBytes());
              insertData(SId,SType); 
              serialPort.close();
         }
         catch(Exception ex)
         {ex.printStackTrace();
         }
    }
     private ParallelPort getSerialPort()
     {
          ParallelPort serialPort=null;
          try
          {
               Enumeration portList = CommPortIdentifier.getPortIdentifiers();
               System.out.println("Port List-->"+portList);

               while(portList.hasMoreElements())
               {
                    System.out.println("Inside");

                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    System.out.println("  Check --,"+portId.getPortType()+","+portId.getName());
                    
                    if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL && portId.getName().equals(SPort))
                    {
                         serialPort = (ParallelPort)portId.open("comapp",2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return serialPort;
    }
    public String getMaxFoodId()
    {
          int       iMaxNo=0;

          String    QS    = " select Max(to_char(substr(Id,2,length(id)))) from Guest_Food_Details ";

          //System.out.println(QS);

          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    iMaxNo = rs.getInt(1);
               }
               rs.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return String.valueOf(iMaxNo+1);
    }

    private String getEmpStatus(String SEmpCode)
    {
        String SStatus="";
     
        String QS   =   " Select Id from Guest_Food_Details Where Emp='"+SEmpCode+"' and TDate='"+common.getCurrentDate()+"' ";

          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    SStatus = rs.getString(1);
               }
               rs.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
        return SStatus;
    }

    private String getId(String SId)
    {
        String SVal ="";
        if(SId.length()==1)
            return  SVal   =    "C000000"+SId;
        if(SId.length()==2)
            return  SVal   =    "C00000"+SId;
        if(SId.length()==3)
            return  SVal   =    "C0000"+SId;
        if(SId.length()==4)
            return  SVal   =    "C000"+SId;
        if(SId.length()==5)
            return  SVal   =    "C00"+SId;
        if(SId.length()==6)
            return  SVal   =    "C0"+SId;

        return "";
    }

    private void insertData(String SId,String SType)
    {

        try {

            String SMonth   = common.getCurrentDate();
            String SEmp     = getEmpCode((String)JCToMeet.getSelectedItem());
            String SDateTime= common.getServerDateTime();

            String QS     = " Insert into Guest_Food_Details (Id,TDate,Emp,CreationDateTime,CreateUser) values ( '"+SId+"' , '"+SMonth+"' , '"+SEmp+"' ,'"+SDateTime+"',"+iUserCode+" ) ";

            JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
            theConnection        = jdbc.getConnection();
            Statement st        = theConnection.createStatement();
            st.executeQuery(QS);
        }
        catch(Exception ex)
        {ex.printStackTrace();
        }
    }

/**
 * Method to create barcode image of type Barcode39 for mytext
 */
public Image createBarCode39(String myText) 
{
	/**
	 * Code 39 character set consists of barcode symbols representing
	 * characters 0-9, A-Z, the space character and the following symbols:
	 * - . $ / + %
	 */
	Barcode39 myBarCode39 = new Barcode39();
	myBarCode39.setCode(myText);
	myBarCode39.setStartStopText(false);
    myBarCode39.setBarHeight(30f);

	Image myBarCodeImage39 = myBarCode39.createImageWithBarcode(contentByte, null, null);
	//myBarCodeImage39.scaleAbsolute(100f,100f);
	return myBarCodeImage39;
}
}

// ES key 1513993847