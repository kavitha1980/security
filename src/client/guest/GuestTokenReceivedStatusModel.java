package client.guest;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import java.lang.*;


public class GuestTokenReceivedStatusModel extends DefaultTableModel
{
     String ColumnName[] = {"ID" ,"SlipNo"  ,"VistorDate"  ,"RepName"  ,"UserName" ,"Type"	,"TokenId" ,"FoodName"  ,"Save" };
     String ColumnType[] = {"S"  ,"S"       ,"N"          ,"S"         ,"S"         ,"S"    	,"S"		,"S"		,"B"};
     int  iColumnWidth[] = {80  ,80        ,50           ,80          ,50            ,60      ,100		,60			,60 };

     public GuestTokenReceivedStatusModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}
