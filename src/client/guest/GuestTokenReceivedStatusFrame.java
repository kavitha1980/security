package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.ClockField;
import util.*;

import domain.jdbc.*;
import blf.*;

public class GuestTokenReceivedStatusFrame extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane layer;

     protected boolean bFlag=false;   

     JButton BSave,BCancel;

     JPanel LeftPanel,RightPanel,BottomPanel,TablePanel,TotalPanel,TopPanel,FullPanel;

     JTable theUpdateTable;

     JScrollPane InfoPane;

     Common common = new Common();

     GuestTokenReceivedStatusModel GTokenModell;
	 private  Vector                        vData;

     boolean okflag=true;

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser     = "gate";
     String SPassword = "gatepass";
	 String STime = "";
     Connection theConnection;    
     
     public GuestTokenReceivedStatusFrame(JLayeredPane layer)
     {
          this.layer=layer;
		  STime = common.getServerDateTime();
          updateLookAndFeel();
         // setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
		  setData();
		  setTabReport();
     }
      
     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void createComponents()
     {
          try
          {
               
               TopPanel       = new JPanel();
               TotalPanel     = new JPanel();
               BottomPanel    = new JPanel();
               TablePanel    = new JPanel();
			                  
               BSave          = new JButton("Save");
               BCancel        = new JButton("Close");              
			   
			   
			   GTokenModell            =  new GuestTokenReceivedStatusModel();
               theUpdateTable       =  new JTable(GTokenModell);
   
            TableColumnModel TC  =  theUpdateTable.getColumnModel();
   
            for(int i=0;i<GTokenModell.ColumnName.length;i++)   
			{   
               TC .getColumn(i).setPreferredWidth(GTokenModell.iColumnWidth[i]);
            }
			   

               theUpdateTable      = new JTable(GTokenModell);  
               InfoPane       = new JScrollPane(theUpdateTable);
          }
          catch(Exception e)
          {
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("vector.txt")));
                    e.printStackTrace(out);
                    out.close();
               }
               catch(Exception ex)
               {
                    System.out.println("Error");
               }
          }

     }   
     private void setLayouts()
     {
          
          TopPanel   .setLayout(new GridLayout(2,1));
          TotalPanel .setLayout(new GridLayout(1,2));         
          TablePanel.setLayout(new BorderLayout());  
                    
          TablePanel.setBorder(new TitledBorder("Update Token Status" ));

          setTitle("Guest Token Received Status Frame");
          setSize(795,520);
          setVisible(true);

          BSave.setMnemonic('S');
          BCancel.setMnemonic('c');

          setIconifiable(true);
          setMaximizable(true);
          setClosable(true);
          setResizable(true);
     }

     private void addComponents()
     {          
          BottomPanel.add(BSave);
          BottomPanel.add(BCancel);

          TablePanel.add(theUpdateTable.getTableHeader());
          TablePanel.add(InfoPane);

          getContentPane().setLayout(new BorderLayout());
          getContentPane().add(TablePanel,"Center");
          getContentPane().add(BottomPanel,"South");          
	}

	public void addListeners()
	{
		BSave.addActionListener(new addAction());
        BCancel.addActionListener(new addAction());
	}   
     public class addAction implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			if(ae.getSource()==BSave)			
			{	                         
                                if(validCheck())
								{
									if(UpdateStatus())
									{
									  JOptionPane.showMessageDialog(null," Ok Sucessfully Updated","Confirm",JOptionPane.INFORMATION_MESSAGE);
									  setData();
									  setTabReport();
									}
									else
									{
										JOptionPane.showMessageDialog(null,"Not Updated","Error",JOptionPane.ERROR_MESSAGE,null);  
									}  
								}
            }

               if(ae.getSource()==BCancel)
               {                    
                    setVisible(false);
               }
          }
	}
	private void setTabReport()
	{
		GTokenModell  .  setNumRows(0);
	  	HashMap   theMap = new HashMap();
		String Stype = "";
      try
	  {
         for(int i=0;i<vData.size();i++)
		 {      
               theMap     =  (HashMap)vData.elementAt(i);

               Vector    Vect       =  new Vector();
			   Stype = common.parseNull((String)theMap.get("TYPE"));
				
               Vect                 .  addElement(common.parseNull((String)theMap.get("ID")));
               Vect                 .  addElement(common.parseNull((String)theMap.get("SLIPNUMBER")));
               Vect                 .  addElement(common.parseDate(common.parseNull((String)theMap.get("VISITORDATE"))));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("NAME"))); 
               Vect                 .  addElement(common.parseNull((String)theMap.get("USERNAME")));
              // Vect                 .  addElement(common.parseDate(common.parseNull((String)theMap.get("ENTRYDATETIME"))));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("TYPENAME")));			   
               Vect                 .  addElement(common.parseNull((String)theMap.get("TOKENID")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("FOODNAME")));
               Vect                 .  addElement(new Boolean(false));
               GTokenModell     .  appendRow(Vect);	
         }
      }  
	  catch(Exception ex)  
	  {
         ex.printStackTrace();
      }
	}	
	private void setData() 
	{
		vData          =  new Vector();
		StringBuffer   sStoredProcedure = new StringBuffer();
		try
		{
			if(theConnection == null)
			{
							JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
							theConnection        = jdbc.getConnection();
			}
			
				sStoredProcedure . append(" select canteentokenallotment.ID,canteentokenallotment.SlipNumber,canteentokenallotment.VisitorDate, ");
				sStoredProcedure . append(" canteentokenallotment.Repcode,canteentokenallotment.RawUser,canteentokenallotment.EntryDateTime, ");
				sStoredProcedure . append(" canteentokenallotment.Type,canteentokenallotment.TokenId,canteentokenallotment.FoodCode, ");
				sStoredProcedure . append(" FoodMaster.FOODNAME,VisitorType.TypeName, ");
				sStoredProcedure . append(" Rawuser.UserName,REPRESENTATIVE.Name from canteentokenallotment ");
				sStoredProcedure . append(" inner join Rawuser on RawUser.USERCODE=canteentokenallotment.RAWUSER ");
				sStoredProcedure . append(" inner join REPRESENTATIVE on REPRESENTATIVE.code=canteentokenallotment.REPCODE ");
				sStoredProcedure . append(" inner join FoodMaster on FoodMaster.FOODCODE = canteentokenallotment.FOODCODE ");
				sStoredProcedure . append(" Inner join VisitorType on VisitorType.Code = canteentokenallotment.TYPE ");
				sStoredProcedure . append(" where canteentokenallotment.RECEIVESTATUS=0 ");
			
			/*Class.forName(SDriver);
			theConnection  = DriverManager.getConnection(SDSN,SUser,SPassword);*/
			Statement theStatement    = theConnection.createStatement();
            ResultSet result          = theStatement.executeQuery(sStoredProcedure.toString());
            ResultSetMetaData rsmd    = result.getMetaData();
			while(result.next())
            {
				 HashMap row = new HashMap();
                 for(int i=0;i<rsmd.getColumnCount();i++)
                 {
                      row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                 }                         
                 vData.addElement(row);
			}
			result      .close();
			theStatement.close();
			//theConnection.close();
		}
		catch(Exception ex)  
	  	{
         	ex.printStackTrace();
      	}
	}    
   private boolean UpdateStatus() 
   {
   			PreparedStatement preparedStatement = null;
			ResultSet theResult = null;
			boolean flag = false;
			try
			{            	  
						if(theConnection == null)
						{
							JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
							theConnection        = jdbc.getConnection();
						}
						for(int j=0;j<GTokenModell.getRowCount();j++)
						{
							  Boolean isSave = (Boolean)GTokenModell.getValueAt(j,8);
			
							  if(isSave.booleanValue())
							  {
									HashMap  theMap		= 	(HashMap)vData.get(j);
									String sId         	= 	common.parseNull((String)theMap.get("ID"));					
									String sUpdateQry   =  	" Update canteentokenallotment set RECEIVESTATUS = ?,ENTRYDATETIME = ? where ID = ? "; 
									preparedStatement   = 	theConnection.prepareStatement(sUpdateQry);								
									preparedStatement 	. 	setInt(1,1);
									preparedStatement 	.	setString(2,STime);
									preparedStatement 	.	setString(3,sId);
									preparedStatement 	. 	executeUpdate();
								}	
						}
						preparedStatement.close();
						theConnection = null;
						flag          =   true;
         	}
			catch(Exception ex)
			{
            	ex.printStackTrace();
				flag          =   false;
         	}
			return flag;   
   }
   private boolean validCheck() 
   {
      for(int j=0;j<GTokenModell.getRowCount();j++)
         {
            Boolean isSave = (Boolean)GTokenModell.getValueAt(j,8);
            if(isSave.booleanValue())
            {
               return true;
            }
         }
         JOptionPane.showMessageDialog(null,"Select Atleast One Check Box","Error",JOptionPane.ERROR_MESSAGE);
         return false;
   }  
}