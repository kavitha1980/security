package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import util.ClockField;
import util.SerialReader;
import domain.jdbc.*;

import blf.*;

public class GuestStatusFrameNew extends JInternalFrame implements rndi.CodedNames
{
    protected   JLayeredPane layer;
    JButton     BSave,BCancel;
    int         iUserCode;
    JPanel      TopPanel    ,MiddlePanel    ,BottomPanel;

    Connection  theConnection;
    Common common   =   new Common();
    Vector V;
    GuestStatusModel     theModel;
    JTable               theTable;
    JComboBox            JCFood;
    MyTextField           TId; 


    public GuestStatusFrameNew(JLayeredPane layer,int iUserCode)
    {
        this.layer    =layer;
        this.iUserCode=iUserCode;

        updateLookAndFeel();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
        theModel.setNumRows(0);
    }
    private void updateLookAndFeel()
    {
         String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

         try
         {
              UIManager.setLookAndFeel(win);
         }
         catch(Exception e)
         {
              e.printStackTrace();
         }
    }
    private void createComponents()
    {
         try
         {
              V           =  new Vector();
              BottomPanel =  new JPanel();
              MiddlePanel =  new JPanel();
              TopPanel    =  new JPanel();
              BSave       =  new JButton("Save");
              BCancel     =  new JButton("Exit");

              JCFood      =  new JComboBox();
              JCFood      .  addItem("Tea");
              JCFood      .  addItem("Tiffen");
              JCFood      .  addItem("Lunch");

              JCFood      .  addFocusListener(new cardFocus1());

              theModel       =    new GuestStatusModel();
              theTable       =    new JTable(theModel);


//              theTable       .    addFocusListener(new cardFocus());

              TId            =    new MyTextField(10);
              TId            .    addFocusListener(new cardFocus());

              TableColumnModel TC =  theTable.getColumnModel();
              for(int i=0;i<theModel.ColumnName.length;i++)
              {
                 TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
              }
/*              TableColumn SColumnPurpose;
              SColumnPurpose    = theTable.getColumn("TYPE");
              SColumnPurpose    . setCellEditor(new DefaultCellEditor(JCFood)); */
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
    }

    private void setLayouts()
    {
         TopPanel   .setLayout(new GridLayout(2,2));
         MiddlePanel.setLayout(new BorderLayout());  
         MiddlePanel.setBorder(new TitledBorder("Token Data"));
         BottomPanel.setLayout(new FlowLayout());  

         setTitle("Guest Token update(15.0)");
         setSize(340,330);
         setVisible(true);

         BSave.setMnemonic('P');
         BCancel.setMnemonic('c');

         setIconifiable(true);
         setMaximizable(true);
         setClosable(true);
         setResizable(true);
//         BSave.setEnabled(false);
    }
    private void addComponents()
    {
        TopPanel    .   add(new JLabel(" Token ID"));
        TopPanel    .   add(TId);
        TopPanel    .   add(new JLabel(" FoodType"));
        TopPanel    .   add(JCFood);

        MiddlePanel .   add("Center",new JScrollPane(theTable));
        BottomPanel .   add(BSave);
        BottomPanel .   add(BCancel);
        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(TopPanel,"North");
        getContentPane().add(MiddlePanel,"Center");
        getContentPane().add(BottomPanel,"South");

    }
    private class cardFocus extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            try
            {

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        public void focusLost(FocusEvent fe)
        {
            try
            {
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
   }

    private class cardFocus1 extends FocusAdapter
    {
        public void focusLost(FocusEvent fe)
        {
            try
            {
                if(TId.getText().length()>0)    {

                    Vector V    =   new Vector();
                           V    .   addElement(TId.getText());
                           V    .   addElement(JCFood.getSelectedItem());
                    theModel    .   appendRow(V);
                    TId         .   setText("");
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
   }

   private void addListeners()
   {
       BSave   .   addActionListener(new ActList());
       BCancel .   addActionListener(new ActList());
   }

   private class ActList implements ActionListener
   {
        public void actionPerformed(ActionEvent ae)
        {
           if(ae.getSource()==BSave)
           {
               try {

                  for(int i=0;i<theModel.getRowCount();i++)
                  {

                     String SId  =  common.parseNull((String)theModel.getValueAt(i,0));

                     if(SId.length()>8)
                     {
                            SId       =   SId.replaceAll("08080400000","C").trim();
                     }

                     String SType=  common.parseNull((String)theModel.getValueAt(i,1));

                     updateStatus(SId,SType);
                   }
                   JOptionPane.showMessageDialog(null,"Data updated sucessfully","Message",JOptionPane.INFORMATION_MESSAGE,null);
                   theModel.setNumRows(0);
               }
               catch(Exception ex)
               {ex.printStackTrace();
               }
           }
           if(ae.getSource()==BCancel)
           {
                   removeFrame();
           }
        }
    }

    private void updateStatus(String SId,String SType)
    {
		System.out.println("In  updateStatus");
        try {
            String SDateTime= common.getServerDateTime();

            String QS     = " update Guest_Food_Details set FoodStatus=1 ,CloseDateTime='"+SDateTime+"' , StatusUser='"+iUserCode+"' , FoodType='"+SType+"' Where Id='"+SId+"'   ";

            System.out.println(QS);

            if(theConnection == null)
            {
                 JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                 theConnection        = jdbc.getConnection();
            }
            Statement st        = theConnection.createStatement();
            st.executeQuery(QS);
            st.close(); 
        }
        catch(Exception ex)
        {ex.printStackTrace();
        }
    }

    private void removeFrame()
    {
        try
        {
             layer.remove(this);
             layer.updateUI();
        }
        catch(Exception ex)
        {
             System.out.println(ex);
        }
     }



}



                

