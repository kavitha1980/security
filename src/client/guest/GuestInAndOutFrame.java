package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.ClockField;
import util.*;

import blf.*;

//GuestInFrame
public class GuestInAndOutFrame extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane layer;

     protected boolean bFlag=false;

     DateField1 DInDate,DOutDate;

     MyTextField TNoPersons;

     NameField NGuestName,NGuestPlace;

     MyComboBox JCCategory,JCSex,JCDepartment;

     MyComboBox JCToMeet;

     MyTextField TName;

     JButton BSave,BCancel;

     JPanel LeftPanel,RightPanel,BottomPanel,TablePanel,TotalPanel,TopPanel,FullPanel;

     JTable InfoTable;

     MyLabel LHostel,LFatherName,LMotherName,LDistrict,LDepartment,LNoPersons;

     JScrollPane InfoPane;

     Common common = new Common();

     GuestInModel gInModel;

     int iInDate=0;

     Info infoDomain;

     String STime;
//     ClockField CInTime;

     TimeField time;

     Vector VCategoryCode,VToMeetCode,VDepartmentCode,VDeptCode,VDeptName,VAllDept;

     //MyLabel LSlipNo;

     MyTextField LCardNo;

     int slno=0;

     Vector VFather,VMother,VDistrict,VHostel,VHostelName,VHostelCode;

     boolean okflag=true;

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser     = "gate";
     String SPassword = "gatepass";
     Connection theConnection;

     String SPort = "LPT1";
     
     
     public GuestInAndOutFrame(JLayeredPane layer)
     {
          this.layer=layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     /*private void setSlip()
     {
          try
          {
               slno = infoDomain.getGuestSlipNo();
               slno =slno+1;
               //LSlipNo.setText(""+slno);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }*/

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void createComponents()
     {
          try
          {

/*               VHostelName          = new Vector();
               VHostelCode          = new Vector();
               
               Vector VVector        = infoDomain.getHostelCode();
               int m=0;

               for(int i=0;i<VVector.size();i++)
               {
                    VHostelCode.addElement(VVector.elementAt(0+m));
                    VHostelName.addElement(VVector.elementAt(1+m));
                    m=m+2;
               }
*/ 
               LeftPanel      = new JPanel();
               RightPanel     = new JPanel();
               TopPanel       = new JPanel();
               TotalPanel     = new JPanel();
               BottomPanel    = new JPanel();
               TablePanel    = new JPanel();
               //FullPanel      = new JPanel();


               //LSlipNo      = new MyLabel();
               LCardNo        = new MyTextField(10);
               LCardNo.addKeyListener(new keys());
               LCardNo.addFocusListener(new cardFocus());

               JCCategory     = new MyComboBox(infoDomain.getCategory());
               JCCategory.addItem("WORKER");

               JCSex          = new MyComboBox();

               Vector VTotal  = infoDomain.getToMeetCode();
               Vector VMeet   = (Vector)VTotal.elementAt(0);
               VFather        = (Vector)VTotal.elementAt(1);
               VMother        = (Vector)VTotal.elementAt(2);
               VDistrict      = (Vector)VTotal.elementAt(3);
               VDeptCode      = (Vector)VTotal.elementAt(4);
               VHostel        = (Vector)VTotal.elementAt(5);

               VAllDept       = infoDomain.getHodCode();
               VDeptName      = infoDomain.getHod();

               JCToMeet       = new MyComboBox(VMeet);
               JCDepartment   = new MyComboBox(VDeptName);

               LFatherName    = new MyLabel();
               LMotherName    = new MyLabel();
               LDistrict      = new MyLabel();
               LDepartment    = new MyLabel();
               LHostel        = new MyLabel();

               LNoPersons     = new MyLabel("No.of Persons");
               
               time = new TimeField();
               STime = time.getTimeNow();
               
               //CInTime = new ClockField();

               NGuestPlace       = new NameField(30);
               NGuestName        = new NameField(40);
     
               DInDate           = new DateField1();
               DOutDate          = new DateField1();

               TNoPersons        = new MyTextField(4);
     
               DInDate.setTodayDate();
               String SinDate    = DInDate.toNormal();
               iInDate           = Integer.parseInt(SinDate);

               DOutDate.setTodayDate();
               String SoutDate   = DOutDate.toNormal();
               int iOutDate      = Integer.parseInt(SoutDate);

               BSave             = new JButton("Save");
               BCancel           = new JButton("Close");

               VCategoryCode     = new Vector();
               VToMeetCode       = new Vector();

               TName             = new MyTextField(20);

               VDepartmentCode   = new Vector();

               Vector initVector = infoDomain.getGuestInInitValues(iInDate);
               gInModel          = new GuestInModel(initVector);

               InfoTable         = new JTable(gInModel);  
               InfoPane          = new JScrollPane(InfoTable);
          }
          catch(Exception e)
          {
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("vector.txt")));
                    e.printStackTrace(out);
                    out.close();
               }
               catch(Exception ex)
               {
                    System.out.println("Error");
               }
          }

     }

     private class cardFocus extends FocusAdapter
     {
               public void focusLost(FocusEvent fe)
               {
                    okflag=true;
                    try
                    {
                         String SSCard  = LCardNo.getText().trim();
                         String QS      = "Select CheckCard from GuestTable where CardNo='"+SSCard+"' And Status=0 ";
                         int st         = infoDomain.getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              LCardNo.setText("");
                              LCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
                    
                    //NGuestName.requestFocus();
               }
     }

     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    okflag=true;
                    try
                    {
                         String SSCard = LCardNo.getText().trim();
                         String QS     = "Select CheckCard from GuestTable where CardNo='"+SSCard+"' And Status=0 ";
                         int st  =infoDomain.getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              LCardNo.setText("");
                              LCardNo.requestFocus();
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }
               }
          }
     }

     private boolean CheckFields()
     {
          try
          {
               bFlag=true;
     
               String SLCardNo   =    common.parseNull(LCardNo.getText().trim());
     
               if(SLCardNo.equals(""))
               {
                    bFlag=false;
                    LCardNo.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
     
               String SNGuestName  =    common.parseNull(NGuestName.getText().trim());          
               if(SNGuestName.equals(""))
               {
                    bFlag=false;
                    NGuestName.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
     
               String SNGuestPlace =    common.parseNull(NGuestPlace.getText().trim());

               if(SNGuestPlace.equals(""))
               {
                    bFlag=false;
                    NGuestPlace.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }

               String SNoPersons =    common.parseNull(TNoPersons.getText().trim());

               if(SNoPersons.equals(""))
               {
                    bFlag=false;
                    TNoPersons.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
               if(JCToMeet.getSelectedItem().equals(""))
               {
                    bFlag=false;
                    JCToMeet.requestFocus();
                    //BSave.setEnabled(false);
                    return bFlag;
               }
          }
          catch(Exception e)
          {
               JOptionPane.showMessageDialog(null,"Fields cannot be empty");
               if(LCardNo.equals("")) LCardNo.requestFocus();
               if(NGuestName.equals("")) NGuestName.requestFocus();
               if(NGuestPlace.equals("")) NGuestPlace.requestFocus();
               if(TNoPersons.equals("")) TNoPersons.requestFocus();
               
          }
          return bFlag;
     }

     private void setLayouts()
     {
          //FullPanel  .setLayout(new BorderLayout());
          TopPanel   .setLayout(new GridLayout(2,1));
          TotalPanel .setLayout(new GridLayout(1,2));
          LeftPanel  .setLayout(new GridLayout(7,2,2,2));
          RightPanel .setLayout(new GridLayout(8,2,2,2));
          TablePanel.setLayout(new BorderLayout());  

          LeftPanel  .setBorder(new TitledBorder("Personal Info"));
          RightPanel .setBorder(new TitledBorder("General Info"));
          TablePanel.setBorder(new TitledBorder("Today's Guest Info" ));
//          BottomPanel.setBorder(new TitledBorder("Control Panel"));

          setTitle("GuestInAndOutFrame(14.0)");
          setSize(795,520);
          setVisible(true);

          BSave.setMnemonic('S');
          BCancel.setMnemonic('c');

          setIconifiable(true);
          setMaximizable(true);
          setClosable(true);
          setResizable(true);

          JCToMeet.addItemListener(new ComboEvents());

          JCCategory.setEditable(false);
          JCSex.setEditable(false);
          JCToMeet.setEditable(false);
          JCDepartment.setEditable(false);


     }

     private void addComponents()
     {
          JCSex.addItem("Male");
          JCSex.addItem("Female");

          LeftPanel.add(new MyLabel("CardNo"));
          LeftPanel.add(LCardNo);

          LeftPanel.add(new MyLabel("InDate"));
          LeftPanel.add(DInDate);

          LeftPanel.add(new MyLabel("Guest Name"));
          LeftPanel.add(NGuestName);

          LeftPanel.add(new MyLabel("Sex"));
          LeftPanel.add(JCSex);

          LeftPanel.add(new MyLabel("Guest Place"));
          LeftPanel.add(NGuestPlace);
          
          LeftPanel.add(LNoPersons);
          LeftPanel.add(TNoPersons);

          LeftPanel.add(new MyLabel("InTime"));
          LeftPanel.add(new MyLabel(STime));

          RightPanel.add(new MyLabel("Category"));
          RightPanel.add(JCCategory);

          RightPanel.add(new MyLabel("To Meet"));
          RightPanel.add(JCToMeet);

          RightPanel.add(new MyLabel("New"));
          RightPanel.add(TName);

          RightPanel.add(new MyLabel("FatherName:"));
          RightPanel.add(LFatherName);

          RightPanel.add(new MyLabel("MotherName"));
          RightPanel.add(LMotherName);

          RightPanel.add(new MyLabel("District"));
          RightPanel.add(LDistrict);

          RightPanel.add(new MyLabel("Department"));
          RightPanel.add(LDepartment);

          RightPanel.add(new MyLabel("Hostel"));
          RightPanel.add(LHostel);

//          RightPanel.add(new MyLabel(""));
//          RightPanel.add(new MyLabel(""));

          TotalPanel.add(LeftPanel);
          TotalPanel.add(RightPanel);

          BottomPanel.add(BSave);
          BottomPanel.add(BCancel);

          TopPanel.add(TotalPanel);
//          TopPanel.add(BottomPanel);

          TablePanel.add(InfoTable.getTableHeader());
          TablePanel.add(InfoPane);

          getContentPane().setLayout(new BorderLayout());
          getContentPane().add(TotalPanel,"North");
          getContentPane().add(TablePanel,"Center");
          getContentPane().add(BottomPanel,"South");

          DOutDate.setEditable(false);
          DInDate.requestFocus();

          //getContentPane().add(FullPanel);


	}

	public void addListeners()
	{
		BSave.addActionListener(new addAction());
          BCancel.addActionListener(new addAction());
	}
     private class ComboEvents implements ItemListener
     {
          public void itemStateChanged(ItemEvent a)
          {
               if(a.getSource()==JCToMeet)
               {
                    try
                    {
     
                         int index      = JCToMeet.getSelectedIndex();

                         if(index==0)
                         {
                              LFatherName.setText("");
                              LMotherName.setText("");
                              LDistrict.setText("");
                              LDepartment.setText("");
                              LHostel.setText("");
                         }
                         else
                         {

                              LFatherName.setText((String)VFather.elementAt(index-1));
                              LMotherName.setText((String)VMother.elementAt(index-1));
                              LDistrict.setText((String)VDistrict.elementAt(index-1));
                              int iDIndex    = getIndex(VDeptCode,index);
     
                              LDepartment.setText((String)VDeptName.elementAt(iDIndex));

//                              int iHIndex    = getHostelIndex(VHostelCode,index);
                              LHostel.setText((String)VHostel.elementAt(index-1));
                         }
                    }
                    catch(Exception e)
                    {
                         e.printStackTrace();
                    }

               }
          }
     }


     private int getIndex(Vector v,int index)
     {
          int code       = Integer.parseInt((String)v.elementAt(index-1));

          int iDeptIndex = VAllDept.indexOf(String.valueOf(code));

          return iDeptIndex;
     }

/*     private int getHostelIndex(Vector v1,int index)
     {
          int code1       = Integer.parseInt((String)v1.elementAt(index-1));

          int iHIndex = VHostelName.indexOf(String.valueOf(code1));

          return iHIndex;
     }
*/
     public class addAction implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			if(ae.getSource()==BSave)
			{
                    bFlag = true;

                    if(okflag)
                    {
                         if(CheckFields())
                         {
                              BSave.setEnabled(true);
                              Insertvalues();
                              LCardNo.requestFocus();
                         }
                         else
                         {
                              //BSave.setEnabled(false);
                              JOptionPane.showMessageDialog(null,"Fields cannot be empty","Message",JOptionPane.ERROR_MESSAGE,null);
                         }
                    }
                    else
                    {
                         LCardNo.requestFocus();
                         JOptionPane.showMessageDialog(null,"Plz Change the CardNo","Message",JOptionPane.ERROR_MESSAGE,null);
                    } 

//                    setprintData();
               }

               if(ae.getSource()==BCancel)
               {
                    
                    setVisible(false);
               }
          }
	}

     public void Insertvalues()
     {
          Vector infoVector = new Vector();

          try
          {
               DInDate.setTodayDate();
               String SDate             = DInDate.toNormal();
               int    IInDate           = common.toInt(common.pureDate(SDate));
               String SODate            = DOutDate.toNormal();
               int    iOutDate          = common.toInt(common.pureDate(SODate));
               String SGuestName        = NGuestName.getText();
               String SGuestPlace       = NGuestPlace.getText();
               String SNoPersons        = TNoPersons.getText();
//               String SInTime           = CInTime.getText();
//               String SOutTime          = CInTime.getText();
               String SInTime           = STime;
               String SOutTime          = STime;
               String SToMeet           = (String)JCToMeet.getSelectedItem();
               String SSex              = (String)JCSex.getSelectedItem();
               String SDepartment       = common.parseNull((String)LDepartment.getText());   
               String SCategory         = (String)JCCategory.getSelectedItem();

               String sCard             = LCardNo.getText();
               //int    slipno               = Integer.parseInt(sCard);

               infoVector.addElement(String.valueOf(IInDate));
               infoVector.addElement(String.valueOf(sCard));
               infoVector.addElement(String.valueOf(SGuestName));
               infoVector.addElement(String.valueOf(SSex));
//               infoVector.addElement(String.valueOf(CInTime));
//               infoVector.addElement(String.valueOf(CInTime));
               infoVector.addElement(String.valueOf(STime));
               infoVector.addElement(String.valueOf(STime));
               infoVector.addElement(String.valueOf(SGuestPlace));
               infoVector.addElement(String.valueOf(SNoPersons));
               infoVector.addElement(String.valueOf(SToMeet));
               infoVector.addElement(String.valueOf(SDepartment));
               infoVector.addElement(String.valueOf(SCategory));


               try
               {
                    String QS =  " insert into GuestTable fields(InDate,CardNo,GuestName,Sex,GuestPlace,NoPersons,ToMeet,InTime,Category,Department,OutDate,OutTime,status,SlipNo,checkcard) values("+iInDate+",'"+sCard+"','"+SGuestName+"','"+SSex+"','"+SGuestPlace+"',"+SNoPersons+",'"+SToMeet+"','"+STime+"','"+SCategory+"','"+SDepartment+"',"+iOutDate+",'"+STime+"',0,"+slno+",0) ";
                    Class.forName(SDriver);                         
                    theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
                    Statement stat     = theConnection.createStatement();
                    stat.executeUpdate(QS);
                    theConnection.close();

                    //infoDomain.getGuestInsertData(QS);
                    JOptionPane.showMessageDialog(null,"Record saved","Message",JOptionPane.NO_OPTION,null);

                    LCardNo.setText("");
                    NGuestName.setText("");
                    NGuestPlace.setText("");
                    TNoPersons.setText("");
                    LCardNo.requestFocus();
                }

               catch(Exception ex)
               {
                    try
                    {
                         PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("temp.txt")));
                         out.println(ex);
                         out.close();
                    }
                    catch(Exception ex1)
                    {
                         ex1.printStackTrace();
                    }
               }

               infoVector = infoDomain.getGuestInInitValues(iInDate);
               gInModel.setVector1(infoVector);

               //setSlip();

               STime = time.getTimeNow();
          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void setprintData()
     {
          int iLimit     =1;
          try
          {
               for(int i=0;i<iLimit;i++)
               {
                    String SType   =    "";
                    if(i==0 || i==1)   {
                         SType = "Tiffen / Lunch";
                    }
                    else {
                         SType = "Tea";
                    }
                    doPrint(SType);
               }
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }
     private void doPrint(String SType)
     {
          try
          {
               ParallelPort serialPort = getSerialPort();
               OutputStream output = serialPort.getOutputStream();

               String xtr1 = "!H fh-wkF080804000002181708"+1232231111+"p\n";
               String str2 = "p !OrderNo:TEst\n";

//               output.write(xtr1.getBytes());
               output.write(str2.getBytes());

               output.write("E\r".getBytes());

               serialPort.close();
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
     }

     private ParallelPort getSerialPort()
     {
          ParallelPort serialPort=null;
          try
          {
               Enumeration portList = CommPortIdentifier.getPortIdentifiers();
               System.out.println(portList);
               while(portList.hasMoreElements())
               {
                    System.out.println("Inside");

                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();
                    System.out.println(portId.getName());
                    
                    if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL && portId.getName().equals(SPort))
                    {
                         serialPort = (ParallelPort)portId.open("comapp",2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return serialPort;
     }

}      

