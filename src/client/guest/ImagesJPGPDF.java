package client.guest;

import java.io.FileOutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

public class ImagesJPGPDF {
  public static void main(String[] args) {
    Document document = new Document();
    try {
      PdfWriter.getInstance(document, new FileOutputStream("e:\\ImagesJPGPDF.pdf"));
      document.open();

      document.add(new Paragraph("load a jpg image file"));
      Image jpg = Image.getInstance("d:\\images.jpeg");
      document.add(jpg);
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    document.close();
  }
}
