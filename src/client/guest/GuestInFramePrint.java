package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;
import java.awt.image.*;
import javax.imageio.ImageIO;


//import javax.print.*;
//import javax.print.attribute.*;


import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;


import util.*;
import util.ClockField;
import util.SerialReader;
import domain.jdbc.*;

import blf.*;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;



import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;


import javax.print.event.PrintJobAdapter.*;
import javax.print.event.PrintJobEvent.*;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;



import jxl.CellReferenceHelper;
import jxl.CellView;
import jxl.HeaderFooter;
import jxl.Range;
import jxl.WorkbookSettings;
import jxl.SheetSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.format.CellFormat;
import jxl.write.Blank;
import jxl.write.Boolean;
import jxl.write.DateFormat;
import jxl.write.DateFormats;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableCell;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;



import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

public class GuestInFramePrint extends JInternalFrame implements rndi.CodedNames
{
     protected JLayeredPane layer;
     protected boolean bFlag=false;

     DateField1 DInDate;
     MyComboBox JCToMeet;
     JButton    BPrint,BCancel;
     JPanel     TopPanel,MiddlePanel,BottomPanel;

     Common common = new Common();
     Connection theConnection;

     String SPort     = "LPT1";
     Vector VMeet;
     Info infoDomain;
     JTextArea  TA;
     Vector theVect,VEmpCode,theVect1;
     int    iUserCode;

     JLabel b;

     int iRow=1;

     Label     label;



     public GuestInFramePrint(JLayeredPane layer,int iUserCode)
     {
          this.layer    =layer;
          this.iUserCode=iUserCode;

          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }
     private void setDomain()
     {

          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void createComponents()
     {
          try
          {

                 b=new JLabel();
                VMeet       =   new Vector();
                VEmpCode    =   new Vector();

                TopPanel    =   new JPanel();
                BottomPanel =   new JPanel();
                MiddlePanel =   new JPanel();

                Vector VTotal  = infoDomain.getToMeetCode();
                VMeet          = (Vector)VTotal.elementAt(0);
                VEmpCode       = (Vector)VTotal.elementAt(6);
                JCToMeet       = new MyComboBox(VMeet);
                DInDate        = new DateField1();
                DInDate        . setTodayDate();

                BPrint         = new JButton("Print");
                BCancel        = new JButton("Exit");


                TA            = new JTextArea(10,20);
//                TA.setFont(new Font("monospaced",Font.BOLD,12));

                Font font = new Font("monospaced",Font.BOLD,12);
                TA.setFont(font);
                TA.setForeground(Color.blue);

          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
    }

    private void setLayouts()
    {

         TopPanel   .setLayout(new GridLayout(3,2,2,2));
         TopPanel   .setBorder(new TitledBorder("Input Data"));

         MiddlePanel.setLayout(new BorderLayout());  
         MiddlePanel.setBorder(new TitledBorder("Print Data"));
         BottomPanel.setLayout(new FlowLayout());  

         setTitle("GuestInFrameRePrint(15.0)");
         setSize(320,380);
         setVisible(true);

         BPrint.setMnemonic('P');
         BCancel.setMnemonic('c');

         setIconifiable(true);
         setMaximizable(true);
         setClosable(true);
         setResizable(true);
         TA.setEditable(false);
         BPrint.setEnabled(false);
    }
    private void addComponents()
    {
        MyLabel Label1 =   new MyLabel("InDate");
        MyLabel Label2 =   new MyLabel("Name");


/*        Border border = BorderFactory.createLineBorder(Color.BLACK);
            Label1    . setBorder(border);
            Label2    . setBorder(border);
            DInDate   . setBorder(border);
            JCToMeet  . setBorder(border); */

        TopPanel    .   add(Label1);
        TopPanel    .   add(DInDate);
        TopPanel    .   add(Label2);
        TopPanel    .   add(JCToMeet);

        String rt="\u0B9F\u0BBF\u0BAA\u0BA9\u0BCD";
        b.setFont(new Font("Latha",Font.PLAIN,12));
        b.setText(rt);


        TopPanel    .   add(new JLabel("font"));
        TopPanel    .   add(b);



        


        MiddlePanel .   add("Center",new JScrollPane(TA));

        BottomPanel .   add(BPrint);
        BottomPanel .   add(BCancel);

        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(TopPanel,"North");
        getContentPane().add(MiddlePanel,"Center");
        getContentPane().add(BottomPanel,"South");
    }
    private void addListeners()
    {
        BPrint  .   addActionListener(new ActList());
        BCancel .   addActionListener(new ActList());
        JCToMeet.   addItemListener(new OnChange());
    }

    private class ActList implements ActionListener
    {
         public void actionPerformed(ActionEvent ae)
         {
            if(ae.getSource()==BPrint)
            {
                try {

                doPrint2();

//                    doPrint1();
//                    doPrint();
                    BPrint.setEnabled(false);
                }
                catch(Exception ex)
                {ex.printStackTrace();
                }
            }
            if(ae.getSource()==BCancel)
            {

            }
         }
    }
     private class OnChange implements ItemListener
     {
          public void itemStateChanged(ItemEvent ie)
          {
              if(ie.getSource()==JCToMeet)
              {
                try
                {
                    String SDate             = DInDate.toNormal();
                    String SEmpCode          = getEmpCode((String)JCToMeet.getSelectedItem());
                    getBarCodeNos(SEmpCode,SDate);
                    setCodes();
                    BPrint.setEnabled(true);
                }
                catch(Exception ex)
                {ex.printStackTrace();
                }
              }
          }
     }

    private void getBarCodeNos(String SEmpCode,String SDate)
    {
        theVect     =   new Vector();
        theVect1    =   new Vector();


        String QS   =   " Select Id,FoodType from Guest_Food_Details Where  Emp='"+SEmpCode+"' and TDate='"+SDate+"'  Order by 1";

        try
        {
            if(theConnection == null)
            {
                JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                theConnection        = jdbc.getConnection();
            }
            Statement st        = theConnection.createStatement();
            ResultSet rs        = st.executeQuery(QS);
            while(rs.next())
            {
                    theVect . addElement(rs.getString(1));
                    theVect1. addElement(rs.getString(2));
            }
            rs.close();
            st.close();
        }
        catch(Exception e)
        {
              e.printStackTrace();
        }
    }

    private String getEmpCode(String SEmp)
    {
        int iIndex  =   VMeet.indexOf(SEmp);

        if(iIndex!=-1)
            return (String)VEmpCode.elementAt(iIndex);
        return "";
    }
    private void setCodes()
    {
        TA.setText("");

        for(int i=0;i<theVect.size();i++)
        {
             TA.append((String)theVect.elementAt(i)+"\n");
        }
    }

    private void doPrint2()
    {
        try
	        {
//         Document document = new Document(PageSize.A4, 30, 30, 30, 30);

         Document document = new Document();
 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("d:\\Barcodes.pdf"));
            document.open();

            PdfContentByte cb = writer.getDirectContent();

     java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage("d:\\tiffen.png");


            Barcode39 code39 = new Barcode39();
            code39.setCode("CODE39-1234567890");
            code39.setStartStopText(false);
            Image image39 = code39.createImageWithBarcode(cb, null, null);

            document.add(new Phrase(new Chunk(image39,0,0)));

/*            Image image = Image.getInstance(awtImage, new Color(0x00, 0x00, 0xFF), true);
            image.setAbsolutePosition(50, 700);
            cb.addImage(image);  */

            document.close();


    String defaultPrinter =
      PrintServiceLookup.lookupDefaultPrintService().getName();

    System.out.println("Default printer: " + defaultPrinter);


/*

    PrintService service = PrintServiceLookup.lookupDefaultPrintService();

    FileInputStream in = new FileInputStream("d:\\barcodes.pdf");


    PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
    pras.add(new Copies(1));

    DocFlavor flavor = DocFlavor.INPUT_STREAM.PDF;
    Doc doc = new SimpleDoc(in, flavor, null);

    DocPrintJob job = service.createPrintJob();
    PrintJobWatcher pjw = new PrintJobWatcher(job);
    job.print(doc, pras);
    pjw.waitForDone();
    in.close();

    // send FF to eject the page

    InputStream ff = new ByteArrayInputStream("\f".getBytes());
    Doc docff = new SimpleDoc(ff, flavor, null);
    DocPrintJob jobff = service.createPrintJob();
    pjw = new PrintJobWatcher(jobff);
    jobff.print(docff, null);
    pjw.waitForDone();    */



        }
        catch(Exception ex) {

          ex.printStackTrace();
       }
    }  




    private void doPrint1()
    {
        try
        {

          try {

    WritableWorkbook workbook = Workbook.createWorkbook(new File("d:\\AddImage.xls"));


    WritableSheet sheet = workbook.createSheet("My Sheet", 0);
    
    WritableImage image = new WritableImage(
        1, 1,   //column, row
        1, 1,   //width, height in terms of number of cells
        new File("d:\\tiffen.png")); //Supports only 'png' images
    
    sheet.addImage(image);

    iRow          += 1;

    WritableFont   wfHead              = new WritableFont(WritableFont.TIMES,12,WritableFont.BOLD);

    WritableCellFormat AllBordersHead  = new WritableCellFormat(wfHead);
    AllBordersHead                     . setBorder(Border.NONE, BorderLineStyle.THIN);
    AllBordersHead                     . setAlignment(Alignment.LEFT);

    String SId1="111";


    String str1 = "@ G AMARJOTHI SPINNING MILLS LTD";
    String str2 = "!H fh-wkF08080400000"+SId1+"p";
    String str0 = "@ GDt  :07.11.2012";
    String str6 = "m";

        ByteArrayInputStream in = new ByteArrayInputStream(str2.getBytes());           
        BufferedImage bi = new BufferedImage(50, 50, BufferedImage.TYPE_BYTE_BINARY);       
        File outputfile = new File("d:\\1.png");  
        ImageIO.write(bi, "png", outputfile);  
  
          

    label          = new Label(0,iRow,str1,AllBordersHead);
    sheet          . addCell(label);
    sheet          . mergeCells(0,iRow,4,iRow);
    iRow          += 1;

//label          = new Label(0,iRow,getUnicodeString(str2.getBytes(),15,0),AllBordersHead);
//    sheet          . addCell(label);
//    sheet          . mergeCells(0,iRow,3,iRow);  


  WritableImage image1 = new WritableImage(
        0, iRow,   //column, row
        1, 1,   //width, height in terms of number of cells
        new File("d:\\1.png"));

    sheet.addImage(image1);

    iRow          += 1; 

    label          = new Label(0,iRow,str0,AllBordersHead);
    sheet          . addCell(label);
    sheet          . mergeCells(0,iRow,3,iRow);
    iRow          += 1;


    label          = new Label(0,iRow,"Name:Gokul",AllBordersHead);
    sheet          . addCell(label);
    sheet          . mergeCells(0,iRow,3,iRow);
    iRow          += 1;

    label          = new Label(0,iRow,str6,AllBordersHead);
    sheet          . addCell(label);
    sheet          . mergeCells(0,iRow,3,iRow);
    iRow          += 1;


    //Writes out the data held in this workbook in Excel format
    workbook.write(); 
    
    //Close and free allocated memory 
    workbook.close();

    } catch(Exception ex) {

     ex.printStackTrace();
    }




    String defaultPrinter =
      PrintServiceLookup.lookupDefaultPrintService().getName();
    System.out.println("Default printer: " + defaultPrinter);

    PrintService service = PrintServiceLookup.lookupDefaultPrintService();

//    FileInputStream in = new FileInputStream(new File("d:\\AddImage.xls"));

    FileInputStream in = new FileInputStream(new File("d:\\g.txt"));


    PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
    pras.add(new Copies(1));


    DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
    Doc doc = new SimpleDoc(in, flavor, null);

    DocPrintJob job = service.createPrintJob();
    PrintJobWatcher pjw = new PrintJobWatcher(job);
    job.print(doc, pras);
    pjw.waitForDone();
    in.close();

    // send FF to eject the page
    InputStream ff = new ByteArrayInputStream("\f".getBytes());
    Doc docff = new SimpleDoc(ff, flavor, null);
    DocPrintJob jobff = service.createPrintJob();
    pjw = new PrintJobWatcher(jobff);
    jobff.print(docff, null);
    pjw.waitForDone(); 



/*            ParallelPort serialPort = getSerialPort();
            OutputStream output = serialPort.getOutputStream();

           byte buf[] =null;
                   
           int bytesread;
           File f;

          PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
          pras.add(new Copies(1));

          PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);

          if (pss.length == 0)
          throw new RuntimeException("No printer services available.");

          System.out.println("Print size length-->"+pss.length);

          for(int i=0;i<pss.length;i++) {

               System.out.println("Printer Name-->"+pss[i].getName());
          }

          PrintService ps = pss[0];
          System.out.println("Printing to " + ps);

          DocPrintJob job = ps.createPrintJob();

          FileInputStream fin = new FileInputStream("d:\\AddImage.xls");

          Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.GIF, null);

          job.print(doc, pras);

          fin.close();  
                     
          serialPort.close(); */



} catch (IOException ie) {
ie.printStackTrace();
} catch (PrintException pe) {
pe.printStackTrace();
}
    }


    private void doPrint()
    {
        try
        {
            String SType    ="";
            String SId      ="";

            ParallelPort serialPort = getSerialPort();
            OutputStream output = serialPort.getOutputStream();

            for(int i=0;i<theVect.size();i++)
            {
                   SId            = (String)theVect.elementAt(i);
                   String SId1    = SId.substring(1,SId.length());
                   SType          = (String)theVect1.elementAt(i);

                   String str1 = "@ G     AMARJOTHI SPINNING MILLS LTD\n\n";
                   String str2 = "!H fh-wkF08080400000"+SId1+"p\n";
                   String str3 = "@ GId  :"+SId+"\n";
                   String str0 = "@ GDt  :"+common.parseDate(common.getCurrentDate())+"\n";
                   String str4 = "@ GName:"+(String)JCToMeet.getSelectedItem()+"\n";
                   String str5 = "! G       "+SType+"!\n\n\n\n\n";
                   String str6 = "m";


              byte buf[] =null;
                   
              int bytesread;

              File f;

//              if(SType.equals("Tiffen")) {

                    f   = new File("D:\\Tiffen.gif");

  //             } else {

                    f   = new File("D:\\Tea.gif");
    //           }

               FileInputStream fis = new FileInputStream(f);
               ByteArrayOutputStream out  = new ByteArrayOutputStream();
               while((bytesread= fis.read())!=-1)
               {
                  out.write(bytesread);
               }
               buf = out.toByteArray();
               fis . close(); 


                   output.write(str1.getBytes());
                   output.write(str2.getBytes());
                   output.write(str3.getBytes());
                   output.write(str0.getBytes());
                   output.write(str4.getBytes());
                   output.write(str5.getBytes());

                   output.write(buf);


//                   if(i==theVect.size()-1)  {
                        output.write(str6.getBytes());
//                   } 
              }
              serialPort.close();
         }
         catch(Exception ex)
         {ex.printStackTrace();
         }
    }

     private ParallelPort getSerialPort()
     {
          ParallelPort serialPort=null;
          try
          {
               Enumeration portList = CommPortIdentifier.getPortIdentifiers();

               System.out.println("Port Litst-->"+portList);

               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    System.out.println("  Check --,"+portId.getPortType()+","+portId.getName());

                    if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL && portId.getName().equals(SPort))
                    {
                         serialPort = (ParallelPort)portId.open("comapp",2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return serialPort;
    }
         public static String getUnicodeString(byte[] d, int length, int pos)
         {
            try
            {
   	          byte[] b = new byte[length * 2];
                  System.arraycopy(d, pos, b, 0, length * 2);
              return b+"";
            }
            catch (Exception e)
            {
                // Fail silently
                 return "";
            }
        }

}

class PrintJobWatcher {
  boolean done = false;

  PrintJobWatcher(DocPrintJob job) {
    job.addPrintJobListener(new PrintJobAdapter() {
      public void printJobCanceled(PrintJobEvent pje) {
        allDone();
      }
      public void printJobCompleted(PrintJobEvent pje) {
        allDone();
      }
      public void printJobFailed(PrintJobEvent pje) {
        allDone();
      }
      public void printJobNoMoreEvents(PrintJobEvent pje) {
        allDone();
      }
      void allDone() {
        synchronized (PrintJobWatcher.this) {
          done = true;
          System.out.println("Printing done ...");
          PrintJobWatcher.this.notify();
        }
      }
    });
  }
  public synchronized void waitForDone() 
  {
    try {
      while (!done) {
        wait();
      }
    } catch (InterruptedException e) {
    }
  }
}