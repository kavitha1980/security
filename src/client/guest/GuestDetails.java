
// Program to insert Warden name, Informed Time , Meeted Time


package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import util.ClockField;
import util.*;
import blf.*;
import javax.swing.event.*;
import java.sql.Date.*;

public class GuestDetails extends JInternalFrame
{
     protected    JLayeredPane Layer;
     protected    boolean bFlag = false;
     JPanel       TopPanel,BottomPanel;
     JPanel       TopLeft,TopRight;
                 
     JTabbedPane  jTabPane;

     MyComboBox   JCWarden;

     String STime;

     String InformedTime;
     TimeFieldNew CTime;

     String MeetedTime;
     TimeFieldNew CMTime;

     Vector VPartyCode,VPartyName;
     boolean okflag = true;
   
     JButton      BSubmit,BExit;

     JTable           theTable;
     Common common ;

     JTextField TCardNO;

     String SInformTime="";
     String SWarden="";

     public GuestDetails(JLayeredPane Layer)
     {
          this.Layer = Layer;

          try
          {
               common  = new Common();
               getParty();
               createComponents();
               setLayouts();
               addComponents();
               addListener();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private void createComponents()
     {                     
          TopPanel       = new JPanel();
          BottomPanel    = new JPanel();
       // MiddlePanel    = new JPanel();

          TopLeft        = new JPanel();
          TopRight       = new JPanel();     

          TCardNO        = new JTextField();
          TCardNO.addKeyListener(new keys());
          TCardNO.addFocusListener(new cardFocus());

          JCWarden       = new MyComboBox(VPartyName);
         
         CTime = new TimeFieldNew(true);
         InformedTime   = CTime.toNormalNew();

         CMTime         = new TimeFieldNew(true);
         MeetedTime     = CMTime.toNormalNew();

         BSubmit        = new JButton("Save");
         BExit          = new JButton("Exit");

     }


     private void setLayouts()
     {
          setTitle("Guest Meeting Frame");
          setMaximizable(true);
          setClosable(true);
          setResizable(true);
          setIconifiable(true);
          setBounds(0,0,300,200);
         // setSize(300,300);
          show();
  
          TopPanel.setLayout(new GridLayout(4,2,2,2));
          BottomPanel.setBorder(new TitledBorder("Save And Exit "));
          TopPanel.setBorder(new TitledBorder("Guest Meet Time Details"));


     }
     private void addComponents()
     {
              
               TopPanel.setSize(3,4);

               TopPanel.add(new JLabel("CardNo"));
               TopPanel.add(TCardNO);

               TopPanel.add(new JLabel("Warden Name"));
               TopPanel.add(JCWarden);

               TopPanel.add(new MyLabel("Informed Time"));
               TopPanel.add(CTime);
      
               TopPanel.add(new MyLabel("Meeted Time"));
               TopPanel.add(CMTime);

               BottomPanel.add(BSubmit);
               BottomPanel.add(BExit);

               getContentPane().add(TopPanel,"North");
           //  getContentPane().add(MiddlePanel,"Center");

               getContentPane().add(BottomPanel,"South");


     
          }


     private class cardFocus extends FocusAdapter
     {
               public void focusLost(FocusEvent fe)
               {
                    okflag=true;
                    try
                    {
                         String SSCard  = TCardNO.getText().trim();
                    //   String QS      = "Select Warden from GuestTable where CardNo='"+SSCard+"' And Status=0 ";
                         String QS      = "Select meetedtime ,informedtime,warden from GuestTable where CardNo='"+SSCard+"'  ";


                         int st         =  getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              TCardNO.setText("");
                              TCardNO.requestFocus();
                         }
                         else
                         {
                              if(!SInformTime.equals(""))
                                    CTime.fromStringNew(SInformTime);
                              if(!SWarden.equals(""))
                                    JCWarden.setSelectedItem(SWarden);

                         }
                    }
                    catch(Exception e)
                    {

                         e.printStackTrace();
                    }
                    
                    //NGuestName.requestFocus();
               }
     }

     private class keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    okflag=true;
                    try
                    {
                         String SSCard = TCardNO.getText().trim();
                    //   String QS     = "Select Warden from GuestTable where CardNo='"+SSCard+"' And Status=0 ";

                         String QS     = "Select meetedtime,informedtime,warden from GuestTable where CardNo='"+SSCard+"' ";

                         int st        = getGuestInitialCheck(QS);
                         if(st==0)
                         {
                              okflag=false;
                              JOptionPane.showMessageDialog(null,"Already a person having This Card");
                              TCardNO.setText("");
                              TCardNO.requestFocus();
                         }
                         else
                         {
                              if(!SInformTime.equals(""))
                                    CTime.fromStringNew(SInformTime);
                              if(!SWarden.equals(""))
                                    JCWarden.setSelectedItem(SWarden);

                         }

                    }
                    catch(Exception e)
                    {
                         JOptionPane.showMessageDialog(null,"Fields cannot be empty");
                         e.printStackTrace();
                    }
               }
          }
     }


     private void addListener()
          {
               BSubmit.addActionListener(new ActList());
               BExit.addActionListener(new ActList());
               TCardNO.addKeyListener(new keys());
               TCardNO.addFocusListener(new cardFocus());

          }

     private class ActList implements ActionListener
          {
               public void actionPerformed(ActionEvent ae)
                    {
                         if(ae.getSource()==BSubmit)
                              {

                                 updateDeluxOrder();
                              }
                         if (ae.getSource() == BExit)
                              {
                                   removeFrame();
                              }
                    }  
          }
     private void removeFrame()
          {
               try
               {
                    Layer.remove(this);
                    Layer.repaint();
                    Layer.updateUI();

               }
               catch(Exception ex)
               {

               }
          }


   public void getParty()
	{
          VPartyCode = new Vector();
          VPartyName = new Vector();


          String SQry = " Select EmpName,EmpCode from Staff where DesignationCode = 117 ";

		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
         Connection theConnection = 
               DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","scm","rawmat");

			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
			while(theResult.next())
			{
                    VPartyName.add(theResult.getString(1));
                    VPartyCode.add(theResult.getString(2));
			}
			theStatement.close();
		}
		catch(Exception e)
		{
               System.out.println("3"+e);
               e.printStackTrace();                                                                                                                                                                                                    
		}
	}




   public void updateDeluxOrder()
     {
          try
          {

               String SInformedTime = CTime.toNormalNew();
               String SMeetedTime   = CMTime.toNormalNew();


               if(SMeetedTime.length()>1)
                     SMeetedTime = SMeetedTime;
               else
                     SMeetedTime = "";

               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
               Statement theStatement = theConnection.createStatement();
               theStatement.executeUpdate(" Update GuestTable  set Warden='"+(String)JCWarden.getSelectedItem()+"',InformedTime='"+SInformedTime+"',MeetedTime='"+SMeetedTime+"' WHere CardNO='"+TCardNO.getText()+"' "); 
               theStatement.close();
               JOptionPane.showMessageDialog(null,"Record saved","Message",JOptionPane.NO_OPTION,null);
               TCardNO.setText("");


          }
          catch(Exception ex)
          {
               System.out.println("Update Party name : "+ex);
          }                                       
     }


     private String getPartyCode(String SName)
     {
          int iIndex=-1;
          String SPartyCode="";
          iIndex = VPartyName.indexOf(SName);
          if(iIndex!=-1)
          {
               SPartyCode = (String)VPartyCode.elementAt(iIndex);
          }
          else
          {
               SPartyCode = "";
          }
          return SPartyCode;
     }

     public int getGuestInitialCheck(String QS)
     {
          int card=-1;
          
          try
          {

                Class.forName("oracle.jdbc.OracleDriver");
                Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
                Statement stat   = theConnection.createStatement();
                ResultSet result = stat.executeQuery(QS);
                while(result.next())
                {
                     String SCard   = common.parseNull(result.getString(1));
                     SInformTime    = common.parseNull(result.getString(2));
                     SWarden        = common.parseNull(result.getString(3));

                     if(!SCard.equals(""))
                           card           = 0;
                }
                result.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setGuestInitialCheck()) : "+ex);
               ex.printStackTrace();
          }

          return card;

     }


}

