/*
    This Class Is Used to Print Data Directlly to Printer
*/
package client.guest;


import javax.comm.*;

import java.util.*;
import java.io.*;
import util.*;
import util.SerialReader;

public class PrinterComm 
{
     String SPort = "LPT1";

     PrinterComm()
     {
     }

     public void CallPrinter(File fileName)
     {

          try
          {
               ParallelPort parallelPort = getParallelPort();
               OutputStream output = parallelPort.getOutputStream();
               FileInputStream fis= new FileInputStream(fileName);
               byte b[] = new byte[fis.available()];
               fis.read(b,0,fis.available());
               output.write(b);
               parallelPort.close();
               
          }
          catch(Exception ex)
          {
               System.out.println("doPrint : "+ex);
               ex.printStackTrace();
          }
     }

     private ParallelPort getParallelPort() 
     {
          ParallelPort parallelPort= null;
          try
          {
               Enumeration portList = CommPortIdentifier.getPortIdentifiers();

               System.out.println("port list "+portList);

               while(portList.hasMoreElements())
               {
                    CommPortIdentifier portId = (CommPortIdentifier)portList.nextElement();

                    System.out.println(" 2 -- Check"+portId.getPortType()+","+portId.getName()+SPort);

                    if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL && portId.getName().equals(SPort))
                    {
                         System.out.println(" 3 -- Check --,"+portId.getPortType()+","+portId.getName());

                         parallelPort = (ParallelPort)portId.open("Par",2000);
                    }
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          return parallelPort;
     }
}
