package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import blf.*;

public class GuestOutFrame extends JInternalFrame implements rndi.CodedNames
{
     protected      JLayeredPane layer;

     DateField1     DInDate,DOutDate;

     MyTextField    TSlipNo;

     NameField      NGuestPlace;

     MyLabel        TInTime,TOutTime,LInTime,LRightNow;

     MyLabel        LGuestName,LSex,LDepartment,LCategory,LHostel,LToMeet;

     MyLabel        LGuestPlace,LNoPersons;

     MyComboBox     JCCategory,JCSex,JCToMeet,JCDepartment,JCListNames;

     JButton        BSave,BExit;

     JPanel         TotalPanel,TopPanel,MiddlePanel,TablePanel,ButtonPanel,NamePanel,InDatePanel;

     JPanel         InTimePanel,PersonalPanel,GeneralPanel,OutPanel,TopGeneralPanel;

     JTable         infoTable;

     JScrollPane    infoPane;

     Info           infoDomain;

     String         CurrentTime="";

     TimeField      time;

     Vector         VCategoryCode,VToMeetCode,VDepartmentCode;

     GuestOutModel     gModel;

     int            iInDate=0;

     String         SInTime="";

     String         SOutTime ="";

     //int            slipno=0;
     String slipno="";

     boolean bFlag=false;

     Common common = new Common();

     public GuestOutFrame(JLayeredPane layer)
     {
          this.layer=layer;
          updateLookAndFeel();
          setDomain();
          createComponents();
          setLayouts();
          addComp();
          addListeners();
     }

     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
	
     }

     private void updateLookAndFeel()
     {
          String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void createComponents()
     {
          try
          {
               TSlipNo        = new MyTextField(10);
               TotalPanel     = new JPanel();
               TopPanel       = new JPanel();
               MiddlePanel    = new JPanel();
               TablePanel     = new JPanel();
               ButtonPanel    = new JPanel();
               NamePanel      = new JPanel();
               InDatePanel    = new JPanel();
               InTimePanel    = new JPanel();
               PersonalPanel  = new JPanel();
               GeneralPanel   = new JPanel();
               OutPanel       = new JPanel();
               TopGeneralPanel= new JPanel();

               LCategory      = new MyLabel(" ");
               LGuestName     = new MyLabel(" ");
               LSex           = new MyLabel(" ");
               LToMeet        = new MyLabel(" ");     
//               LHostel        = new MyLabel(" ");     
               LGuestPlace    = new MyLabel(" ");
               LNoPersons     = new MyLabel(" ");
               LDepartment    = new MyLabel(" ");
               LRightNow      = new MyLabel(" ");

               //JCListNames    = new MyComboBox(infoDomain.getGuestNames());
               //JCListNames.setPreferredSize(new Dimension(200,20));
               time           = new TimeField();
               CurrentTime    = time.getTimeNow();                        
     
               NGuestPlace    = new NameField(30);
     
               DInDate        = new DateField1();
               DOutDate       = new DateField1();

               TInTime        = new MyLabel();
               TOutTime       = new MyLabel();

               DInDate.setTodayDate();
               String SinDate = DInDate.toNormal();
               iInDate        = Integer.parseInt(SinDate);

               DOutDate.setTodayDate();
               String SoutDate = DOutDate.toNormal();
               int iOutDate   = Integer.parseInt(SoutDate);

               BSave          = new JButton("Update");
               BSave.setMnemonic('U');
               BExit          = new JButton("Exit");

               VCategoryCode  = new Vector();
               VToMeetCode    = new Vector();
               VDepartmentCode = new Vector();

               //System.out.println("InDate:"+ iInDate);

               Vector initVector = infoDomain.getGuestInitValues(iInDate);

               gModel   = new GuestOutModel(initVector);

               infoTable      = new JTable(gModel);  
               infoPane       = new JScrollPane(infoTable);
               infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (infoTable.getColumn("S.No")).setPreferredWidth(40);
               (infoTable.getColumn("CardNo")).setPreferredWidth(150);
               (infoTable.getColumn("Name")).setPreferredWidth(200);
               (infoTable.getColumn("Sex")).setPreferredWidth(50);
               (infoTable.getColumn("InTime")).setPreferredWidth(150);
               (infoTable.getColumn("Place")).setPreferredWidth(150);
               (infoTable.getColumn("Department")).setPreferredWidth(150);
               (infoTable.getColumn("To Meet")).setPreferredWidth(180);
               (infoTable.getColumn("InDate")).setPreferredWidth(100);
               (infoTable.getColumn("OutDate")).setPreferredWidth(100);

          }

          catch(Exception e)
          {
               try
               {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("vector.txt")));
                    e.printStackTrace(out);
                    out.close();
               }
               catch(Exception ex)
               {
                    System.out.println("Error");
               }
          }

     }

     private void setLayouts()
     {

          TotalPanel.setLayout(new GridLayout(1,1));

          TopPanel.setLayout(new GridLayout(1,3));

          TopGeneralPanel.setLayout(new GridLayout(2,1));
          TopGeneralPanel.setBorder(new TitledBorder("Display"));

          MiddlePanel.setLayout(new GridLayout(1,3));

          TablePanel.setLayout(new BorderLayout());
          TablePanel.setBorder(new TitledBorder("Today's GuestInfo"));

          ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
          ButtonPanel.setBorder(new TitledBorder("ButtonPanel"));

          NamePanel.setBorder(new TitledBorder("Card_No"));

          InDatePanel.setBorder(new TitledBorder("In_Date"));

          InTimePanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
          InTimePanel.setBorder(new TitledBorder("In_Time"));

          PersonalPanel.setLayout(new GridLayout(4,2));
          PersonalPanel.setBorder(new TitledBorder("PersonalInfo"));

          GeneralPanel.setLayout(new GridLayout(4,2));
          GeneralPanel.setBorder(new TitledBorder("GeneralInfo"));

          OutPanel.setLayout(new GridLayout(3,2));
          OutPanel.setBorder(new TitledBorder("OutInfo"));

          setTitle("GuestOutInfo(15.0)");
          setSize(800,520);
          setVisible(true);
          setIconifiable(true);
          setMaximizable(true);
          setClosable(true);
          setResizable(true);
     }

     private void addComp()
     {
          NamePanel.add(TSlipNo);
          TSlipNo.addKeyListener(new Keys());
          
          String SIn =(String) DInDate.toString();
          InDatePanel.add(new MyLabel(SIn));
          
          LInTime   = new MyLabel();
          InTimePanel.add(LRightNow);
          
          TopPanel.add(NamePanel);
          TopPanel.add(InDatePanel);
          TopPanel.add(InTimePanel);
          
          PersonalPanel.add(new MyLabel("GuestName"));
          PersonalPanel.add(LGuestName);
          PersonalPanel.add(new MyLabel("Sex"));
          PersonalPanel.add(LSex);
          PersonalPanel.add(new MyLabel("GuestPlace"));
          PersonalPanel.add(LGuestPlace);
          PersonalPanel.add(new MyLabel("No.of.Persons"));
          PersonalPanel.add(LNoPersons);
          
          GeneralPanel.add(new MyLabel("ToMeet"));
          GeneralPanel.add(LToMeet);
//          GeneralPanel.add(new MyLabel("Hostel"));
//          GeneralPanel.add(LHostel);
          GeneralPanel.add(new MyLabel("Category"));
          GeneralPanel.add(LCategory);
          GeneralPanel.add(new MyLabel("Department"));
          GeneralPanel.add(LDepartment);
          
          String SOut=(String) DOutDate.toString();
          
          OutPanel.add(new MyLabel("OutTime"));
          OutPanel.add(new MyLabel(CurrentTime));
          OutPanel.add(new MyLabel("OutDate"));
          OutPanel.add(new MyLabel(SOut));
          OutPanel.add(new MyLabel(""));
          OutPanel.add(new MyLabel(""));
          
          MiddlePanel.add(PersonalPanel);
          MiddlePanel.add(GeneralPanel);
          MiddlePanel.add(OutPanel);
          
          TopGeneralPanel.add(TopPanel);
          TopGeneralPanel.add(MiddlePanel);

          ButtonPanel.add(BSave);
          ButtonPanel.add(BExit);
          BSave.setMnemonic('U');
          BExit.setMnemonic('E');

          TotalPanel.add(TopGeneralPanel);
          
          TablePanel.add("North",infoTable.getTableHeader());
          TablePanel.add(infoPane);
          
          DOutDate.setEditable(false);
          DInDate.requestFocus();
          
          getContentPane().setLayout(new BorderLayout());
          getContentPane().add(TotalPanel,"North");
          getContentPane().add(TablePanel);
          getContentPane().add(ButtonPanel,"South");
     }

	public void addListeners()
	{
          BSave.addActionListener(new addAction());
          BExit.addActionListener(new addAction());
	}

     private class Keys extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               try
               {
                    if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                    {
                         String Slip = (String)TSlipNo.getText();
                         //slipno  = Integer.parseInt(Slip);
                         slipno=Slip;
                         if(slipno.equals(""))
                         {
                              JOptionPane.showMessageDialog(null,"Not a number");
                              TSlipNo.setText("");
                              TSlipNo.requestFocus();
                         }
                         else
                         {
                              int status = infoDomain.getCheckCard(slipno,0);
                              //System.out.println("Status:"+status);
                              if(status==-1)
                              {
                                   JOptionPane.showMessageDialog(null,"already Out");
                                   TSlipNo.setText("");
                                   TSlipNo.requestFocus();
                              }
                              else
                              {
                                   setValues(slipno);
                              }
                         }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    //System.out.println("SlipNo cannot be Zero");

                }
          }
     }

     private void setValues(String slno)
     {
          try
          {
               //System.out.println("Selected:"+slno);
               Vector  vVector = infoDomain.getGuestValues(slno);

               LGuestName.setText((String)vVector.elementAt(0));
               LSex.setText(""+(String)vVector.elementAt(1));
               LGuestPlace.setText((String)vVector.elementAt(2));
               LNoPersons.setText((String)vVector.elementAt(3));
               LToMeet.setText((String)vVector.elementAt(4));     
//               LHostel.setText((String)vVector.elementAt(5));     
               LCategory.setText((String)vVector.elementAt(5));
               LDepartment.setText((String)vVector.elementAt(6));
               LRightNow.setText((String)vVector.elementAt(7));
          }
          catch(Exception e)
          {
               e.printStackTrace();
          } 
     }

	public class addAction implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
               if(ae.getSource()==BSave)
			{
                    bFlag=true;

                    if(CheckFields())
                    {
                         Insertvalues();
                         TSlipNo.setText("");
                         TSlipNo.requestFocus();
                    }
                    else
                    {
                         JOptionPane.showMessageDialog(null,"CardNo cannot be empty","Message Dialog",JOptionPane.ERROR_MESSAGE,null);
                    }
			}

               if(ae.getSource()==BExit)
               {
                    setVisible(false);
			}
          }
	}

     private boolean CheckFields()
     {
          bFlag=true;

          if(TSlipNo.getText().equals(""))
          {
               bFlag=false;
          }
          return bFlag;
     }

     public void Insertvalues()
     {
          Vector infoVector = new Vector();
          
          try
          {
               SInTime            = LInTime.getText();
               
               String SOutDate          = DOutDate.toNormal();
               int IOutDate             = common.toInt(common.pureDate(SOutDate));

               SOutTime = time.getTimeNow();
               
//               int s = Integer.parseInt(slipno);
               try
               {
                    int s = infoDomain.getGuestUpdateSlipNo(slipno);
                   
                    infoDomain.setGuestUpdateTime(s,SOutTime,IOutDate,slipno);
                    infoVector = infoDomain.getGuestInitValues(iInDate);
                    gModel.setVector1(infoVector);
                    JOptionPane.showMessageDialog(null,"Record Updated","Message",JOptionPane.NO_OPTION,null);
               }
          
               catch(Exception ex)
               {
                    ex.printStackTrace();
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}      

