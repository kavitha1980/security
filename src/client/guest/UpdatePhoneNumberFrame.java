
/*   Updation of Orissa Workers Phone Number in HRD.java     */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.io.*;
                             

public class UpdatePhoneNumberFrame  extends JInternalFrame
{
     protected                JLayeredPane Layer;

     JPanel                   TopPanel       ,MiddlePanel        ,BottomPanel;
     JPanel                   TopLeft        ;

     MyTextField              THrdTicketNo;
     JButton                  BSave          ,BExit;

     Common                   common;
     Connection               theConnection;
     Statement                theStatement;
     Vector                   theVector;
     UpdatePhoneNumberModel   theModel;
     JTable                   theTable;
     CanteenChooseFrame       canteenFrame;

     public UpdatePhoneNumberFrame(JLayeredPane Layer)
     {
          this.Layer     =    Layer;

          common              = new Common();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
     }
     private void createComponents()
     {

          TopPanel       =    new JPanel();
          MiddlePanel    =    new JPanel();
          BottomPanel    =    new JPanel();
          TopLeft        =    new JPanel();

          THrdTicketNo   =    new MyTextField(10);

          BSave          =    new JButton("SAVE");
          BExit          =    new JButton("EXIT");

          theModel       =    new UpdatePhoneNumberModel();
          theTable       =    new JTable(theModel);

          TableColumnModel TC =  theTable.getColumnModel();
          for(int i=0;i<theModel.ColumnName.length;i++)
          {
             TC .getColumn(i).setPreferredWidth(theModel.iColumnWidth[i]);
          }
     }

     private void setLayouts()
     {
          setTitle("Orissa Workers Phone Number");
          setIconifiable(true);
          setBounds(20,20,780,480);
          setClosable(true);
          setResizable(true);
          setMaximizable(true);

          TopPanel       .    setLayout(new GridLayout(1,13));
          TopLeft        .    setLayout(new GridLayout(2,13));
          MiddlePanel    .    setLayout(new BorderLayout());
          BottomPanel    .    setLayout(new FlowLayout());
          BottomPanel    .    setBorder(new TitledBorder("Save & Exit"));
     }

     private void addComponents()
     {
          TopLeft        .    add(new MyLabel(" HRD Ticket Number"));
          TopLeft        .    add(THrdTicketNo);
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));
          TopLeft        .    add(new MyLabel(" "));

          TopPanel       .    add(TopLeft);

          MiddlePanel    .    add(new JScrollPane(theTable));

          BottomPanel    .    add(BSave);
          BottomPanel    .    add(BExit);

          TopPanel       .    setBorder(new TitledBorder("SEARCH"));
          TopLeft        .    setBorder(new TitledBorder(""));
          MiddlePanel    .    setBorder(new TitledBorder("DETAILS"));
          BottomPanel    .    setBorder(new TitledBorder("Save & Exit"));

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     private void addListeners()
     {
          BExit          .    addActionListener(new ActList());
          BSave          .    addActionListener(new ActList());
          THrdTicketNo   .    addKeyListener(new KeyList());
     }

   public class KeyList extends KeyAdapter   {

       public void keyReleased(KeyEvent ke)  {

            String SFind = THrdTicketNo.getText();

            int i=indexOf(SFind);

            if (i != -1)
            {
                    theTable.setRowSelectionInterval(i,i);
                    Rectangle cellRect = theTable.getCellRect(i,0,true);
                    theTable.scrollRectToVisible(cellRect);
            }
       }
   }
   public int indexOf(String SFind) {

       SFind = SFind.toUpperCase();

       for(int i=0;i<theModel.getRowCount();i++)
       {
               String str = (String)theModel.getValueAt(i,0);
               str=str.toUpperCase();
               if(str.startsWith(SFind))
                       return i;
       }
       return -1;
   }


     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BSave)
               {
                     if(checkAnyOneSelected())
                         {
                                  updateData();
                                  setTabReport();
                                  JOptionPane.showMessageDialog(null,"Data Saved","Information",JOptionPane.INFORMATION_MESSAGE);
                         }
               
               }
               if(ae.getSource()==BExit)
               {
                    removeFrame();
               }
          }
     }


     private boolean checkAnyOneSelected()
     {
          int iCount = 0;

          for(int i=0;i<theModel.getRowCount();i++)
          {
               java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,8);

              if(bValue.booleanValue())
               {
                    String SPhoneNo     = (String)theModel.getValueAt(i,7);

                    if(!SPhoneNo.equals(""))
                    {
                         return true;
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null,"Data Must Be Fill","Warning",JOptionPane.ERROR_MESSAGE);
                         return false;
                    }
               }
          } 
          JOptionPane.showMessageDialog(null,"Please Select Any one Check Box for Save","Information",JOptionPane.ERROR_MESSAGE);
          return false;
     }   
         
     private void  updateData()
     {
          for(int i=0;i<theModel.getRowCount();i++)
          {
               java.lang.Boolean bValue      = (java.lang.Boolean)theModel.getValueAt(i,8);

               if(bValue.booleanValue())
               {
                    String SHrdTicket   =    common.parseNull((String)theModel.getValueAt(i,0));
                    String SPhoneNo     =    common.parseNull((String)theModel.getValueAt(i,7));

                   updatePhone(SHrdTicket,SPhoneNo);    
               }
          }

     }

     private boolean  updatePhone(String SHrdTicket, String SPhoneNo)
     {
               
               String QS =" Update ReliveEmp set Phoneno ='"+SPhoneNo+"' where HRDTicketNo ='"+SHrdTicket+"' ";

          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
               theStatement             = theConnection.createStatement();
               theStatement             . executeQuery(QS);
               theStatement             . close();
               return  true;
          }
          catch(Exception ex)
          {
               ex.printStackTrace();

               return false;
          }

     }
        
     private Vector setVector()
     {
          theConnection                 =    null;
          theStatement                  =    null;


          try
          {
               Class                    . forName("oracle.jdbc.OracleDriver");
               theConnection            = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
               theStatement             = theConnection.createStatement();
               ResultSet rs             = null;
               rs                       = theStatement.executeQuery(getMainQS());
               ResultSetMetaData rsmd   = rs.getMetaData();

               while(rs.next())
               {
                    HashMap row = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),rs.getString(i+1));
                    }                         
                    theVector.addElement(row);
               }
               rs.close();
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
          return theVector;
     }

     private String getMainQS()
     {
          StringBuffer   sB   =    new StringBuffer();

          sB.append(" select HrdTicketNo,EmpName,Sex,Address1,Address2,Address3,Address4,PhoneNo from ReliveEmp Where shiftduration = 12 order by 1");


          return sB.toString();
     }

     private void setTabReport()
     {
          try  {
               theModel.setNumRows(0);

               theVector                =    new Vector();
               theVector                =    setVector();

               System.out.println("the Vector"+theVector.size());

               for(int i=0;i<theVector.size();i++)
               {
                    HashMap   theMap    =    (HashMap)theVector.elementAt(i);
                    Vector Vect         =    new Vector();

                    Vect                .    addElement(common.parseNull((String)theMap.get("HRDTICKETNO")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("EMPNAME")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("SEX")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("ADDRESS1")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("ADDRESS2")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("ADDRESS3")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("ADDRESS4")));
                    Vect                .    addElement(common.parseNull((String)theMap.get("PHONENO")));
                    Vect                .    addElement(new Boolean(false));

                    theModel            .    appendRow(Vect);
               }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

}

