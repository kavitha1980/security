package client.guest;

import java.awt.Color;
import java.io.FileOutputStream;

import com.lowagie.text.*;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;

import com.lowagie.text.Element;   
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.BarcodeEAN;
import com.lowagie.text.pdf.BarcodeEANSUPP;
import com.lowagie.text.pdf.BarcodeInter25;
import com.lowagie.text.pdf.BarcodePostnet;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import com.lowagie.text.pdf.PdfAction;



 import com.lowagie.text.pdf.BaseFont;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;
import java.awt.image.*;
import javax.imageio.ImageIO;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;



import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;


import javax.print.event.PrintJobAdapter.*;
import javax.print.event.PrintJobEvent.*;


public class BarCodeGenerator {
PdfContentByte contentByte;

public void generateBarCode() {
	/* Step 1: Create a Document*/

     Document document = new Document(PageSize.A4, 50, 50, 50, 50);

	try {

		/* Step 2: Create PDF Writer*/
		PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream("d:\\BarCode1.pdf"));

		/* Step 3: Open the document so that we can write over it.*/
		document.open();

		/* Step 4: We have to create a set of contents.*/
		contentByte = writer.getDirectContent();

          writer.setOpenAction(new PdfAction(PdfAction.PRINTDIALOG)); 

          document.add(new Paragraph("CANTEEN TOKEN"));
          document.add(new Paragraph(""));
          document.add(new Paragraph("20.11.2012"));
          document.add(new Paragraph(""));
          document.add(new Paragraph("RAJESH"));


          java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage("d:\\images.jpeg");

          PdfPTable table = new PdfPTable(1);

          table.setWidthPercentage(102);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
          table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
          table.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
          table.getDefaultCell().setFixedHeight(80);

          String myText = "gokul"; // Text to encode


		table.addCell(new Phrase(new Chunk(createBarCode39(myText
				.toUpperCase()), 0, 0)));

          document.add(table);

          String str6 = "m"; 
          
//          Image image = Image.getInstance(awtImage, new Color(0x00, 0x00, 0xFF), true);
          Image image = Image.getInstance("d:\\tea.png");

          image.setAbsolutePosition(44, 655);
          image.setAlignment(Image.LEFT);
          contentByte.addImage(image);

          document.newPage();


          document.add(new Paragraph("CANTEEN TOKEN"));
          document.add(new Paragraph(""));
          document.add(new Paragraph("20.11.2012"));
          document.add(new Paragraph(""));
          document.add(new Paragraph("RAJESH"));


          java.awt.Image awtImage1 = Toolkit.getDefaultToolkit().createImage("d:\\images.jpeg");

          PdfPTable table1 = new PdfPTable(1);

          table1.setWidthPercentage(102);
          table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
          table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
          table1.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
          table1.getDefaultCell().setFixedHeight(70);

          String myText1 = "gokul"; // Text to encode


               table1.addCell(new Phrase(new Chunk(createBarCode39(myText1
				.toUpperCase()), 0, 0)));

          document.add(table);

//          Image image1 = Image.getInstance(awtImage, new Color(0x00, 0x00, 0xFF), true);

          Image image1 = Image.getInstance("d:\\tiffen.png");

          image1.setAbsolutePosition(44, 655);
          image1.setAlignment(Image.LEFT);
          contentByte.addImage(image1);


	// step 5: we close the document
	document.close();


          Process p = Runtime
			   .getRuntime()
                  .exec("rundll32 url.dll,FileProtocolHandler  d:\\barcode1.pdf");
               p.waitFor(); 


/*          String[] command = {"acrord32.exe","d:\\barcode1.pdf"};

          Process process = Runtime.getRuntime().exec(command); */


//                  String[] command = {"C:\\Program Files\\Adobe\\Reader 9.0\\Reader\\","d:\\barcode1.prn"};
//          Process process = Runtime.getRuntime().exec(command);




/*          Process process = Runtime.getRuntime().exec("cmd.exc /C start accrod32 /P /h" + s);

                                   Runtime r = Runtime.getRuntime();

                                   String[] command = {"acrord32.exe","d:\\barcode1.prn"}; 
                                   r.exec(command);  */




/*    String defaultPrinter =
      PrintServiceLookup.lookupDefaultPrintService().getName();

    System.out.println("Default printer: " + defaultPrinter);


    PrintService service = PrintServiceLookup.lookupDefaultPrintService();

    FileInputStream in = new FileInputStream(new File("d:\\BarCode1.pdf"));


    PrintRequestAttributeSet  pras = new HashPrintRequestAttributeSet();
    pras.add(new Copies(1));

     DocFlavor flavor = DocFlavor.INPUT_STREAM.PDF;    PrintService defaultService =  PrintServiceLookup.lookupDefaultPrintService();
     PrintService printServices[] = PrintServiceLookup.lookupPrintServices(flavor, pras);

    Doc doc = new SimpleDoc(in, flavor, null);

    DocPrintJob job = service.createPrintJob();
    PrintJobWatcher pjw = new PrintJobWatcher(job);
    job.print(doc, pras);
    pjw.waitForDone();
    in.close();

    // send FF to eject the page

    InputStream ff = new ByteArrayInputStream("\f".getBytes());
    Doc docff = new SimpleDoc(ff, flavor, null);
    DocPrintJob jobff = service.createPrintJob();
    pjw = new PrintJobWatcher(jobff);
    jobff.print(docff, null);
    pjw.waitForDone();     */




	} catch (Exception de) {
		de.printStackTrace();
	}

}

public static void main(String args[]) {
	new BarCodeGenerator().generateBarCode();
}

/**
 * Method to create barcode image of type Barcode39 for mytext
 */
public Image createBarCode39(String myText) {
	/**
	 * Code 39 character set consists of barcode symbols representing
	 * characters 0-9, A-Z, the space character and the following symbols:
	 * - . $ / + %
	 */

	Barcode39 myBarCode39 = new Barcode39();
	myBarCode39.setCode(myText);
	myBarCode39.setStartStopText(false);
	Image myBarCodeImage39 = myBarCode39.createImageWithBarcode(
			contentByte, null, null);
	return myBarCodeImage39;
}

/*Creating a barcode image using Barcode39 extended type for myText*/
public Image createBarcode39Extended(String myText) {
	Barcode39 myBarCode39extended = new Barcode39();
	myBarCode39extended.setCode(myText);
	myBarCode39extended.setStartStopText(false);
	myBarCode39extended.setExtended(true);
	Image myBarCodeImage39Extended = myBarCode39extended
			.createImageWithBarcode(contentByte, null, null);
	return myBarCodeImage39Extended;
}

/* Creating a barcode image using Barcode 128 for myText*/
public Image createBarcode128(String myText) {
	Barcode128 code128 = new Barcode128();
	code128.setCode(myText);
	Image myBarCodeImage128 = code128.createImageWithBarcode(contentByte,
			null, null);
	return myBarCodeImage128;
}

/* Creating a barcode image using BarcodeEAN for myText*/
public Image createBarcodeEAN(String myText) {
	BarcodeEAN myBarcodeEAN = new BarcodeEAN();
	myBarcodeEAN.setCodeType(Barcode.EAN13); // 13 characters.
	myBarcodeEAN.setCode(myText);
	Image myBarCodeImageEAN = myBarcodeEAN.createImageWithBarcode(
			contentByte, null, null);
	return myBarCodeImageEAN;
}

/* creating a barcode image using BarCodeInter25 for myText*/
public Image createBarcodeInter25(String myText) {
	BarcodeInter25 myBarcode25 = new BarcodeInter25();
	myBarcode25.setGenerateChecksum(true);
	myBarcode25.setCode(myText);
	Image myBarCodeImageInter25 = myBarcode25.createImageWithBarcode(
			contentByte, null, null);
	return myBarCodeImageInter25;
}

/*creating a barcode image using BarcodePostnet for myText*/
public Image createBarcodePostnet(String myText) {
	BarcodePostnet myBarcodePostnet = new BarcodePostnet();
	myBarcodePostnet.setCode(myText);
	Image myBarcodeImagePostnet = myBarcodePostnet.createImageWithBarcode(
			contentByte, null, null);
	return myBarcodeImagePostnet;
}

/* creating a barcode image using BarCodeInter25 */
public Image createBarcodePostnetPlanet(String myText) {
	BarcodePostnet myBarCodePostnetPlanet = new BarcodePostnet();
	myBarCodePostnetPlanet.setCode(myText);
	myBarCodePostnetPlanet.setCodeType(Barcode.PLANET);
	Image myBarCodeImagePostntPlanet = myBarCodePostnetPlanet
			.createImageWithBarcode(contentByte, null, null);
	return myBarCodeImagePostntPlanet;
}

public Image createBARCodeEANSUPP(String myTextPrimary,
		String myTextSupplementary5) {
	PdfTemplate pdfTemplate = contentByte.createTemplate(0, 0);
	BarcodeEAN myBarcodeEAN = new BarcodeEAN();
	myBarcodeEAN.setCodeType(Barcode.EAN13);
	myBarcodeEAN.setCode(myTextPrimary);
	PdfTemplate ean = myBarcodeEAN.createTemplateWithBarcode(contentByte,
			null, Color.blue);
	BarcodeEAN codeSUPP = new BarcodeEAN();
	codeSUPP.setCodeType(Barcode.SUPP5);
	codeSUPP.setCode(myTextSupplementary5);
	codeSUPP.setBaseline(-2);
	BarcodeEANSUPP eanSupp = new BarcodeEANSUPP(myBarcodeEAN, codeSUPP);
	Image imageEANSUPP = eanSupp.createImageWithBarcode(contentByte, null,
			Color.blue);
	return imageEANSUPP;
}
}




class PrintJobWatcher {
  boolean done = false;

  PrintJobWatcher(DocPrintJob job) {
    job.addPrintJobListener(new PrintJobAdapter() {
      public void printJobCanceled(PrintJobEvent pje) {
        allDone();
      }
      public void printJobCompleted(PrintJobEvent pje) {
        allDone();
      }
      public void printJobFailed(PrintJobEvent pje) {
        allDone();
      }
      public void printJobNoMoreEvents(PrintJobEvent pje) {
        allDone();
      }
      void allDone() {
        synchronized (PrintJobWatcher.this) {
          done = true;
          System.out.println("Printing done ...");
          PrintJobWatcher.this.notify();
        }
      }
    });
  }
  public synchronized void waitForDone() {
    try {
      while (!done) {
        wait();
      }
    } catch (InterruptedException e) {
    }
  }
}

