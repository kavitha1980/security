package client.guest;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.comm.*;

import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import util.ClockField;
import util.SerialReader;
import domain.jdbc.*;

import blf.*;

public class GuestStatusFrame extends JInternalFrame implements rndi.CodedNames
{
    protected   JLayeredPane layer;
    JTextArea   TA;
    JButton     BSave,BCancel;
    int         iUserCode;
    JPanel      TopPanel    ,MiddlePanel    ,BottomPanel;

    Connection  theConnection;
    Common common   =   new Common();
    Vector V;

    public GuestStatusFrame(JLayeredPane layer,int iUserCode)
    {
        this.layer    =layer;
        this.iUserCode=iUserCode;

        updateLookAndFeel();
        createComponents();
        setLayouts();
        addComponents();
        addListeners();
    }
    private void updateLookAndFeel()
    {
         String win="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

         try
         {
              UIManager.setLookAndFeel(win);
         }
         catch(Exception e)
         {
              e.printStackTrace();
         }
    }
    private void createComponents()
    {
         try
         {
              V           =  new Vector();
              BottomPanel =  new JPanel();
              MiddlePanel =  new JPanel();
              BSave       =  new JButton("Save");
              BCancel     =  new JButton("Exit");

              TA        = new JTextArea(20,20);
              Font font = new Font("monospaced",Font.BOLD,12);
              TA.setFont(font);
              TA.setForeground(Color.blue);
              TA.addFocusListener(new cardFocus());
//              TA.append("Toekn Id \n");
//              TA.setEditable(false); 
          }
          catch(Exception ex)
          {ex.printStackTrace();
          }
    }

    private void setLayouts()
    {
         MiddlePanel.setLayout(new BorderLayout());  
         MiddlePanel.setBorder(new TitledBorder("Token Data"));
         BottomPanel.setLayout(new FlowLayout());  

         setTitle("Guest Token update(15.0)");
         setSize(280,340);
         setVisible(true);

         BSave.setMnemonic('P');
         BCancel.setMnemonic('c');

         setIconifiable(true);
         setMaximizable(true);
         setClosable(true);
         setResizable(true);
//         BSave.setEnabled(false);
    }
    private void addComponents()
    {
        MiddlePanel .   add("Center",new JScrollPane(TA));
        BottomPanel .   add(BSave);
        BottomPanel .   add(BCancel);
        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(MiddlePanel,"Center");
        getContentPane().add(BottomPanel,"South");
    }
    private class cardFocus extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            try
            {
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        public void focusLost(FocusEvent fe)
        {
            try
            {
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
   }
   private void addListeners()
   {
       BSave   .   addActionListener(new ActList());
       BCancel .   addActionListener(new ActList());
   }

   private class ActList implements ActionListener
   {
        public void actionPerformed(ActionEvent ae)
        {
           if(ae.getSource()==BSave)
           {
               try {
                   String STA   =   TA.getText().trim();
                   int iTotLines=   TA.getLineCount();

                   System.out.println("iTot Lines-------->"+iTotLines);

                   iTotLines   =   iTotLines-1;

                   for(int i=0;i<iTotLines;i++) {

                       int iStart  =   TA.getLineStartOffset(i);
                       int iEnd    =   TA.getLineEndOffset(i);

                       System.out.println("iStart-->"+iStart);
                       System.out.println("iEnd  -->"+iEnd);

                       iEnd = iEnd-1;

                       String SLine=   STA.substring(iStart,iEnd).trim();

                       System.out.println("Sline after---->'"+SLine+"'");
                       System.out.println("Sline Length---->"+SLine.length());

                        if(SLine.length()==8) {
                            updateStatus(SLine);
                        }
                        else    {
                            SLine       =   SLine.replaceAll("08080400000","C").trim();
                            updateStatus(SLine);
                        }
                   }
                   JOptionPane.showMessageDialog(null,"Data updated sucessfully","Message",JOptionPane.INFORMATION_MESSAGE,null);
                   TA.setText("");
               }
               catch(Exception ex)
               {ex.printStackTrace();
               }
           }
           if(ae.getSource()==BCancel)
           {
                   removeFrame();
           }
        }
    }

    private void updateStatus(String SId)
    {
        try {
            String SDateTime= common.getServerDateTime();

            String QS     = " update Guest_Food_Details set FoodStatus=1 ,CloseDateTime='"+SDateTime+"' , StatusUser='"+iUserCode+"' Where Id='"+SId+"'   ";

            System.out.println(QS);

            if(theConnection == null)
            {
                 JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                 theConnection        = jdbc.getConnection();
            }
            Statement st        = theConnection.createStatement();
            st.executeQuery(QS);
            st.close(); 
        }
        catch(Exception ex)
        {ex.printStackTrace();
        }
    }

    private void removeFrame()
    {
        try
        {
             layer.remove(this);
             layer.updateUI();
        }
        catch(Exception ex)
        {
             System.out.println(ex);
        }
     }



}



                

