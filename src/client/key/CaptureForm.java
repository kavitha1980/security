package client.key;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;
import client.key.*;

import domain.jdbc.*;

import javax.imageio.*;
import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;


public class CaptureForm
{
	
	public CaptureForm()
	{	
	 init();
	 start();
	 stop();
	 } 
	
	protected void init()
	{
		capturer.addDataListener(new DPFPDataAdapter() {
			@Override public void dataAcquired(final DPFPDataEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint sample was captured.");
					setPrompt("Scan the same fingerprint again.");
                    process(e.getSample());
				}});
                    System.out.println(" Comming Here");
			}
		});
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
			@Override public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
			@Override public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
			@Override public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
			@Override public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
			@Override public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.
		drawPicture(convertSampleToBitmap(sample));

		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good

		if (features != null)
		{
			// Compare the feature set with our template

			DPFPVerificationResult result = 
                verificator.verify(features,template);
			updateStatus(result.getFalseAcceptRate());
			if (result.isVerified())
               {
                    bVerifyStatus =true;
                    JOptionPane.showMessageDialog(null," Verified...and Found Matched");
                    setVisible(false);
               }
               else
               {
                    bVerifyStatus =false;
                    JOptionPane.showMessageDialog(null," Verified...and Found NOT Matched");
//                    makeReport("The fingerprint wan NOT VERIFIED.");
               }
          }


	}
     public boolean getVerified()
     {
          return bVerifyStatus ;
     }

	protected void start()
	{
		capturer.startCapture();
		setPrompt("Using the fingerprint reader, scan your fingerprint.");
	}

	protected void stop()
	{
		capturer.stopCapture();
	}
	public void setPrompt(String string) {
		prompt.setText(string);
	}
	public void makeReport(String string) {
          log.append(string + "\n");
	}
	public void drawPicture(Image image) {
                ImageIcon imageicon= new ImageIcon(
                        image.getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_DEFAULT));

                picture.setIcon(imageicon);

	
               Image image1 = new ImageIcon(
			image.getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_DEFAULT)).getImage();  
               RenderedImage rendered = null;  
               if (image1 instanceof RenderedImage)  
               {  
                    rendered = (RenderedImage)image1;  
               }  
            else  
            {  
                BufferedImage buffered = new BufferedImage(  
                    imageicon.getIconWidth(),  
                    imageicon.getIconHeight(),  
                    BufferedImage.TYPE_INT_RGB  
                );  
                Graphics2D g = buffered.createGraphics();  
                g.drawImage(image1, 0, 0, null);  
                g.dispose();  
                rendered = buffered;  
            }
            try
            {
                    ImageIO.write(rendered, "GIF", new File("d:\\image.gif"));  
            }
            catch(Exception ex)
            {
                    System.out.println(ex);
            }

        }
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
}	
