package client.key;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;
import client.key.*;

import domain.jdbc.*;


public class KeyWDFrame extends JInternalFrame implements rndi.CodedNames
{
     Key keyData;

     protected JLayeredPane Layer;

     JPanel    TopPanel,BottomPanel,ListPanel;
     JTable listTable;
     KeyModel keyModel;
     Vector VName,VCode;
     Vector VKeyName,VPerson,VTime,VInfo;

     TimeField TWDTime;
     DateField1 DToday;
     JButton BOkay;
     MyTextField TKeyCardNo,TKeyName;
     MyLabel LName;
     MyComboBox JCStaff;
     Common common  = new Common();
     Connection theConnection;
     Vector VKeyId,VWDDate;
     int iInit=0;
     int iColumnWidth[]= {50,250,600,600,700};

     public KeyWDFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;
          setDomain();
          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVector1();
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               keyData             = (Key)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void createComponents()
     {
          TKeyCardNo          = new MyTextField();
          TKeyName            = new MyTextField();
          JCStaff             = new MyComboBox(VName);
          TWDTime              = new TimeField();

          DToday              = new DateField1();
          DToday.setTodayDate();
          DToday.setEditable(false);

          BOkay               = new JButton("Okay");
          TopPanel            = new JPanel();
          ListPanel           = new JPanel();
          BottomPanel         = new JPanel();

          LName               = new MyLabel("");
          keyModel            = new KeyModel();
          listTable           = new JTable(keyModel);
          listTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          for(int i=0;i<5;i++)
               (listTable.getColumn(keyModel.columnname[i])).setPreferredWidth(iColumnWidth[i]);    

     }
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(5,2));
          TopPanel.setBorder(new TitledBorder("Key Information"));
          BottomPanel.setBorder(new TitledBorder("Control"));
          ListPanel.setLayout(new BorderLayout());
          ListPanel.setBorder(new TitledBorder("Key Movements"));
     }
     private void addComponents()
     {
          setTitle("Key WithDrawn ");
          setBounds(0,0,400,500);
          setMaximizable(true);
          setClosable(true);

          TopPanel.add(new MyLabel("Date "));
          TopPanel.add(DToday);
          TopPanel.add(new MyLabel("Time"));
          TopPanel.add(new MyLabel(TWDTime.getTimeNow()));
          TopPanel.add(new MyLabel("KeyCode"));
          TopPanel.add(TKeyCardNo);
          TopPanel.add(new MyLabel("KeyName"));
          TopPanel.add(LName);
          TopPanel.add(new MyLabel("Person"));
          TopPanel.add(JCStaff);

          ListPanel.add("North",listTable.getTableHeader());
          ListPanel.add(new JScrollPane(listTable));

          BottomPanel.add(BOkay);
          BOkay.setEnabled(false);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",ListPanel);
          getContentPane().add("South",BottomPanel);

          //TKeyCardNo.requestFocus();

          BOkay.setMnemonic('O');


     }

     private void addListeners()
     {
          TKeyCardNo.addKeyListener(new ActList());
          BOkay.addActionListener(new OnOkay());
     }
     private class ActList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {

                    if(checkData())
                    {
                         setData();
                         BOkay.setEnabled(true);
                    }
                    else
                         JOptionPane.showMessageDialog(null,"Key Not Available","Information",JOptionPane.ERROR_MESSAGE,null);
               }
          }
     }

     private class OnOkay implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               saveData();
               setDefault();
               setVisible(false);
          }
     }
     private void setDataIntoVector()
     {
          try
          {
               Vector VInfo   = keyData.getUserInformation();

               VCode          = (Vector)VInfo.elementAt(0);
               VName          = (Vector)VInfo.elementAt(1);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void setDefault()
     {

          TKeyCardNo.setText("");
          //TKeyName.setText("");
          LName.setText("");
          BOkay.setEnabled(false);
          //TKeyCardNo.requestFocus();

     }
     private void saveData()
     {
          String  QS          = " Select KeySeq.nextVal from Dual ";

          int iKeyCode        = common.toInt((String)VKeyId.elementAt(0));

          int iIndex          = JCStaff.getSelectedIndex();
          int iStaffCode      = common.toInt((String)VCode.elementAt(iIndex));

          System.out.println("StaffCode"+iStaffCode);
          int iId             = 0;

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    iId            = common.toInt((String)theResult.getString(1));
               }
               theStatement.close();
               String QS1 = " Insert into KeyTrans(Id,KeyId,WDTime,StaffCode,Status) "+
                            " Values("+iId+","+iKeyCode+",sysdate,"+iStaffCode+",0) ";
               Statement theStatement1 = theConnection.createStatement();
               theStatement1.executeUpdate(QS1);

               JOptionPane.showMessageDialog(null,"Details Saved");
               Vector VShowInfo    = new Vector();
               VShowInfo.addElement(LName.getText());
               VShowInfo.addElement(DToday.toNormal());
               VShowInfo.addElement(JCStaff.getSelectedItem());
               keyModel.insertData(VShowInfo);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void setData()
     {

          VKeyId       = new Vector();
          Vector VKeyName     = new Vector();

          String SCardNo      = common.parseNull((String)TKeyCardNo.getText());

          String QS           = " select KeyId,KeyName from KeyInfo where KeyCode = '"+SCardNo+"' ";

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VKeyId.addElement(theResult.getString(1));
                    VKeyName.addElement(theResult.getString(2));

                    //TKeyName.setText((String)VKeyName.elementAt(0));
                    LName.setText((String)VKeyName.elementAt(0));

               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     private void setVector1()
     {

          VKeyName       = new Vector();
          VTime          = new Vector();
          VPerson        = new Vector();
          VInfo          = new Vector();
          VWDDate        = new Vector();

          String QS = " Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as Time, "+
                      " EmpName,To_Char(WdTime,'yyyymmdd') as WDDate from KeyTrans Inner join KeyInfo on "+
                      " KeyInfo.KeyId   = KeyTrans.keyId "+
                      " inner join VisitorStaff on VisitorStaff.EmpCode = KeyTrans.StaffCode "+
                      " where Status=0 and To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ";
					  
					/*	QS = " Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as Time, "+
						" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate from KeyTrans Inner join KeyInfo on "+
						" KeyInfo.KeyId   = KeyTrans.keyId "+
						" inner join staff on staff.EmpCode = KeyTrans.StaffCode "+
						" where Status=0 and To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') "+
						" Union All "+
						" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as Time, "+
						" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate from KeyTrans Inner join KeyInfo on "+
						" KeyInfo.KeyId   = KeyTrans.keyId "+
						" inner join schemeapprentice on schemeapprentice.EmpCode = KeyTrans.StaffCode "+
						" where Status=0 and To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') "+
						" Union All "+
						" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as Time, "+
						" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate from KeyTrans Inner join KeyInfo on "+
						" KeyInfo.KeyId   = KeyTrans.keyId "+
						" inner join contractapprentice on contractapprentice.EmpCode = KeyTrans.StaffCode "+
						" where Status=0 and To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ";*/
          try
          {

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VKeyName.addElement(theResult.getString(1));
                    VTime.addElement(theResult.getString(2));
                    VPerson.addElement(theResult.getString(3));
                    VWDDate.addElement(common.parseDate((String)theResult.getString(4)));
               }

               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          VInfo.addElement(VKeyName);
          VInfo.addElement(VWDDate);
          VInfo.addElement(VTime);
          VInfo.addElement(VPerson);

          if(iInit==0)
          {
               keyModel.setVector1(VInfo);
               iInit=1;
          }
          else
               keyModel.insertData(VInfo);
               

     }
     public boolean checkData()
     {
          String SCardNo      = common.parseNull((String)TKeyCardNo.getText());
          String QS           =    " Select Count(*) from KeyTrans inner join "+
                                   " KeyInfo on KeyInfo.KeyId = KeyTrans.KeyId "+
                                   " Where KeyInfo.KeyCode='"+SCardNo+"' and Status=0 ";
          int iCount=0;
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    iCount              = common.toInt((String)theResult.getString(1));
               }
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          if(iCount==0)
               return true;
          else
               return false;
     }
}
