
/*
     The Subclass of the Balance Bean to capture the weight of the content
     placed on a balance
*/

package client.key;
import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import java.awt.Font;
import java.awt.Color;
import domain.jdbc.*;
import java.sql.*;

import util.*;
public class KeySerialReader implements SerialPortEventListener,Runnable
{
     protected JLabel LKeyCode,LKeyName;
     protected JButton BOkay;    

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;
	 Connection con;	
     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
	 String sKeyCode = "";
     int    ctr=0;
     int    iMultiply = 1;
     boolean heartAttack = false;
	 Font KeyCodeFont =   new Font("Times New Roman", Font.BOLD, 20);
	 Font KeyNameFont =   new Font("Times New Roman", Font.BOLD, 18);
     StringBuffer inputBuffer = new StringBuffer();

     public KeySerialReader(JLabel LKeyCode,JLabel LKeyName,JButton BOkay)
     {
          this.LKeyCode   = LKeyCode;
		  this.LKeyName   = LKeyName;
          this.BOkay      = BOkay;          

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          LKeyCode.setForeground(new Color(255,94,94));
		  LKeyName.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
					System.out.println("After portId --> "+portId.getName());
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println("Comm Error : "+ex);
          }
     }     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
        				  System.out.println("KeyCode  --->1 "+(char)newData);

                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              System.out.println("KeyCode "+(char)newData);

                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setKeyCode(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                //if(ctr < 10)
                                  outputStream.write((int)'a');
                                //else
//                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     private void setKeyCode(String str)
     {	 	  
          String sKeyName = "";
		  try
          {
               sKeyCode		=	str.substring(1,str.length() - 1);
			   LKeyCode		.	setText(sKeyCode);
			   sKeyName		=	getKeyName(sKeyCode);
			   
			 
			   LKeyName		.	setText(sKeyName);		   
          }
          catch(Exception ex){}
     }
	 
      public String getKeyName(String sKeyCode){
	  
			String	SKeyName="";
			StringBuffer sb = new StringBuffer();
			sb.append(" select KeyName from KeyInfo where KEYCODENEW = ?");
			
			try {			
                //Class.forName("oracle.jdbc.OracleDriver");
				//java.sql.Connection con = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
				if(con==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    con       			= jdbc.getConnection();
               }			   
			   java.sql.PreparedStatement ps = con.prepareStatement(sb.toString());
										  ps . setString(1,sKeyCode.trim());										  
										  java.sql.ResultSet rs1 = ps.executeQuery();
										  int its = 0;
										  while(rs1.next()){										  	
											SKeyName=common.parseNull(rs1.getString(1));
										  }
										  rs1.close();
										  ps.close();										  
				
			}catch(Exception ex){
				ex.printStackTrace();
			}	  
		  return SKeyName;
	  }	 
     public void freeze()
     {
          serialPort.close();
          //LKeyCode.setForeground(new Color(255,94,94));
		  LKeyCode	.	setFont(KeyCodeFont);
		  LKeyName	.	setFont(KeyNameFont);
          //BOkay.setEnabled(true);
     }   
}