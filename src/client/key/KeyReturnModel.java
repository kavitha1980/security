package client.key;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.util.*;
import java.sql.*;

import util.*;
public class KeyReturnModel extends DefaultTableModel
{  

     public String columnname[]={"Sno","KeyName","WithDrawn Date","Person WithDrawn","PersonReturned","ReturnedDate","ReturnedTime"};
     String ColumnType[]={"N","S","S","S","S","S","S"};     

     Object values[][];

     Vector v1;
     int sNo;
     Common common = new Common();
     public KeyReturnModel()
     {
               setDataVector(getdata(),columnname);
     }
     private Object[][] getdata()
     {

               int sno=1;
               Object values[][]=new Object[1][columnname.length];
     
               
               int m=0;
               try
               {
                    for(int i=0;i<1;i++)
                    {
                                             
                         values[i][0]   =    "";
                         values[i][1]   =    "";
                         values[i][2]   =    "";
                         values[i][3]   =    "";
                         values[i][4]   =    "";
                         values[i][5]   =    "";
                         values[i][6]   =    "";

                    }
                }

                     catch(Exception e)
                         {
                              System.out.println("Exception in vector");
                         }
               return values;
     }
     private Object[][] getdata(Vector VInfo)
     {

               int sno=1;
               Vector VKey         = (Vector)VInfo.elementAt(0);
               Vector VWDDate      = (Vector)VInfo.elementAt(1);
               Vector VTime        = (Vector)VInfo.elementAt(2);
               Vector VPerson      = (Vector)VInfo.elementAt(3);
               Vector VRetDate            = (Vector)VInfo.elementAt(4);
               Vector VWDPerson    = (Vector)VInfo.elementAt(5);
               Object values[][]=new Object[VKey.size()][columnname.length];
     
               
               int m=0;
               try
               {

                    for(int i=0;i<VKey.size();i++)
                    {
                                             
                         values[i][0]   =    String.valueOf(i+1);
                         values[i][1]   =    common.parseNull((String)VKey.elementAt(i));
                         values[i][2]   =    common.parseNull((String)VWDDate.elementAt(i));
                         values[i][3]   =    common.parseNull((String)VWDPerson.elementAt(i));
                         values[i][4]   =    common.parseNull((String)VPerson.elementAt(i));
                         values[i][5]   =    common.parseNull((String)VRetDate.elementAt(i));
                         values[i][6]   =    common.parseNull((String)VTime.elementAt(i));
                    }
                }

                     catch(Exception e)
                         {
                              System.out.println("Exception in vector");
                         }
               return values;
     }


     public int getSno(Vector vect1)
     {
               
               int i;
               for(i=0;i<vect1.size();i++)
               {
               }
               return i;
      }
     public Class getColumnClass(int col){ return getValueAt(0,col).getClass();}
     
     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column,0));
          }
          catch(Exception ex)
          {
               System.out.println("4 : "+ex);
          }
     }

     public void setVector(Vector theVector)
     {
          v1 = theVector;
          setDataVector(getdata(),columnname);
     }

     public  boolean isCellEditable(int row,int col)
     {
          if(ColumnType[col]=="N"||ColumnType[col]=="S")
               return false;
          return true;
     }

     public void setVector1(Vector theVector)
     {
          setDataVector(getdata(theVector),columnname);
     }
     public int getRows()
     {
          return super.dataVector.size();
     }   
     public void insertData(Vector vect)
     {
         insertRow(getRows(),vect);
         setRowId();
     }
     private void setRowId()
     {
         for(int i=0;i<getRows();i++)
         {
                  setValueAt(String.valueOf(i+1),i,0);
         }
     }
     
          
}
