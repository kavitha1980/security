package client.key;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;
import client.key.*;
import domain.jdbc.*;

import javax.imageio.*;
import com.digitalpersona.onetouch.*;
import com.digitalpersona.onetouch.capture.*;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.*;
import com.digitalpersona.onetouch.verification.*;


public class KeyTransactionsFrame extends JInternalFrame implements rndi.CodedNames
{
     Key keyData;

     protected JLayeredPane Layer;
     private DPFPCapture capturer = DPFPGlobal.getCaptureFactory().createCapture();
     private DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();

     JPanel    TopPanel,BottomPanel,ListPanel;
     JTable listTable;
	 
     KeyTransactionModel keyModel;
	 
     Vector VName,VCode;
     Vector VKeyName,VPerson,VTime,VInfo;

     TimeField TWDTime;
     DateField1 DToday;
     JButton BOkay,BExit;

     JLabel  LPerson,LSecurity;
     MyTextField TKeyCardNo;
	 
     MyLabel LKeyName;
	 MyLabel LKeyCode;
	 MyComboBox JCTransType;
     Common common  = new Common();
     Connection theConnection;
     Vector vData ;
	 Vector VKeyTrans,VTransTypeCode;
	 Font NameFont =   new Font("Times New Roman", Font.BOLD, 18);
     int iInit=0;
     int iColumnWidth[]= {50,250,600,600,700};
	 boolean PersonFlag;
     boolean SecFlag;
	 String sTransType	=	"";
	 String SToday = "";
	 String sKeyCode = "";
	 String SPersonEmpCode = "";
	 String SSecurityEmpCode= "";
     ArrayList theTemplateList;
	 KeySerialReader keyreader;
     public KeyTransactionsFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;
		  SToday=common.pureDate(common.getServerDate());
          setDomain();
          setData();
		  setTransactionType();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setTabReport();
          onLoad();
		  init();
	      start();
     	  keyreader = new KeySerialReader(LKeyCode,LKeyName,BOkay);		  
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               keyData             = (Key)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
			   keyreader.freeze();
          }
     }
     public void createComponents()
     {
          TKeyCardNo          = new MyTextField();
		  JCTransType		  =	new MyComboBox(VKeyTrans);
          TWDTime             = new TimeField();

          DToday              = new DateField1();
          DToday.setTodayDate();
          DToday.setEditable(false);

          BOkay               = 	new JButton("Okay");
		  BExit				  =		new JButton("Exit");
          LPerson             = 	new JLabel("Person Punch");		  
          LSecurity           = 	new JLabel("Security Punch");
          TopPanel            = 	new JPanel();
          ListPanel           = 	new JPanel();
          BottomPanel         = 	new JPanel();

          LKeyName            = new MyLabel("");
		  LKeyCode			  = new MyLabel(""); 	
          keyModel            = new KeyTransactionModel();
          listTable           = new JTable(keyModel);
          TableColumnModel TC1 =  listTable.getColumnModel();
			   
            for(int i=0;i<keyModel.ColumnName.length;i++)   
			{   
               TC1 .getColumn(i).setPreferredWidth(keyModel.iColumnWidth[i]);
            }
     }
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(7,2));
          TopPanel.setBorder(new TitledBorder("Key Information"));
          BottomPanel.setBorder(new TitledBorder("Control"));
          ListPanel.setLayout(new BorderLayout());
          ListPanel.setBorder(new TitledBorder("Key Movements"));
     }
     private void addComponents()
     {
          setTitle("Key Transactions ");
          setBounds(0,0,500,500);
          setMaximizable(true);
          setClosable(false);

          TopPanel.add(new MyLabel("Date "));
          TopPanel.add(DToday);
          TopPanel.add(new MyLabel("Time"));
          TopPanel.add(new MyLabel(TWDTime.getTimeNow()));
          TopPanel.add(new MyLabel("KeyCode"));
          //TopPanel.add(TKeyCardNo);
		  TopPanel.add(LKeyCode);
          TopPanel.add(new MyLabel("KeyName"));
          TopPanel.add(LKeyName);
		  TopPanel.add(new MyLabel("Transaction Type"));
          TopPanel.add(JCTransType);
          TopPanel.add(new MyLabel("Person (WithDrawn / Return)"));
          TopPanel.add(LPerson);          
		  TopPanel.add(new MyLabel("Security"));
          TopPanel.add(LSecurity);

          ListPanel.add("North",listTable.getTableHeader());
          ListPanel.add(new JScrollPane(listTable));

          BottomPanel.add(BOkay);
		  BottomPanel.add(BExit);
          BOkay.setEnabled(false);

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",ListPanel);
          getContentPane().add("South",BottomPanel);

          //TKeyCardNo.requestFocus();

          BOkay.setMnemonic('O');
     }

    private void addListeners()
    {          
         BOkay.addActionListener(new OnOkay());
         BExit.addActionListener(new ActList());
	//	this.addWindowListener(new WindowList());
		

    }
	private class WindowList extends WindowAdapter
	{
		public void windowClosing(WindowEvent we)
		{
			stop();		
		}	
	}
	private class ActList implements ActionListener
	{
	     public void actionPerformed(ActionEvent ae)
    	{
			removeFrame();	
		}
	}
	
	private void removeFrame()
	{	
			try
			{
				keyreader.freeze();
				stop();
				Layer.remove(this);
				Layer.updateUI();
			}catch(Exception e){keyreader.freeze();}
	}
     private class OnOkay implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               saveData();
               setDefault();
			   setData();
			   setTabReport();
               //setVisible(false);
          }
     }
     private void setDefault()
     {
          //BOkay.setEnabled(false);
		  LPerson     	.	setText("Person Punch");
		  LPerson		.	setForeground(new Color(0,0,0));
          LSecurity		.	setText("Security Punch");
		  LSecurity		.	setForeground(new Color(0,0,0));
		  LKeyCode  	.	setText("");
		  LKeyName		.	setText("");
     }
     private void saveData()
     {
          StringBuffer InsertSb = new StringBuffer();
		  String  QS          = " Select KeySeq.nextVal from Dual ";
		  sTransType =  (String)JCTransType.getSelectedItem();		  
          sKeyCode   	  = LKeyCode.getText();
		  String SKeyId= "",SRowId="";
          String sId             = "";
		  StringBuffer sb = new StringBuffer();
		  StringBuffer sb1 = new StringBuffer();										
		  sb.append(" select KEYID from KeyInfo where KEYCODENEW = ?");
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }			   
					java.sql.PreparedStatement ps = theConnection.prepareStatement(sb.toString());
										  ps . setString(1,sKeyCode.trim());										  
										  java.sql.ResultSet rs = ps.executeQuery();										  
										  while(rs.next()){										  	
											SKeyId=common.parseNull(rs.getString(1));
										  }
										  rs.close();
										  ps.close();
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    sId            = common.parseNull(theResult.getString(1));
               }
               theStatement.close();
			   if(sTransType.trim().equals("KeyWithDrawn"))
			   {
               		if(sKeyCode.length()>0 && SPersonEmpCode.length()>0 && SSecurityEmpCode.length()>0)
					{						
						if(KeyCheck(sKeyCode.trim())){
					
						InsertSb.append(" Insert into KeyTrans(Id,KeyId,WDTime,StaffCode,Status,SecurityCode) ");
						InsertSb.append(" Values(?,?,sysdate,?,?,?) ");
               			java.sql.PreparedStatement 	psd 	= 	theConnection.prepareStatement(InsertSb.toString());
               									psd 	. 	setInt(1,common.toInt(sId));
												psd 	. 	setInt(2,common.toInt(SKeyId));
												psd 	. 	setInt(3,common.toInt(SPersonEmpCode));
												psd 	. 	setString(4,"0");
												psd 	. 	setInt(5,common.toInt(SSecurityEmpCode));
												psd		.	executeUpdate();
               			JOptionPane.showMessageDialog(null,"Details Saved");
						}
						else
						{
							JOptionPane.showMessageDialog(null,"Key Already In USe.. You May Return This Now");						
							//LPerson     .	setText("Person Punch");
          					//LSecurity	.	setText("Security Punch");
						}
				   }
				   else
				   {
						JOptionPane.showMessageDialog(null,"KeyCode Is Empty","Error",JOptionPane.ERROR_MESSAGE);
				   }
			  }
			  else if(sTransType.trim().equals("KeyReturn"))
			  {
							if(sKeyCode.length()>0 && SPersonEmpCode.length()>0 && SSecurityEmpCode.length()>0){
								//if(KeyCheckReturn(SKeyId)){		  
									sb1.append(" select KeyTrans.Id from KeyTrans ");
									sb1.append(" inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId ");
									sb1.append(" inner join VisitorStaff on VisitorStaff.EmpCode = KeyTrans.StaffCode ");
									sb1.append(" where KEYCODENEW = ? and Status=0 ");
									sb1.append(" Union All ");
									sb1.append(" select KeyTrans.Id from KeyTrans  ");
									sb1.append(" inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId ");
									sb1.append(" inner join staff on staff.EmpCode    = KeyTrans.StaffCode ");
									sb1.append(" where KEYCODENEW = ? and Status=0 ");
									sb1.append(" Union All ");
									sb1.append(" select KeyTrans.Id from KeyTrans  ");
									sb1.append(" inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId ");
									sb1.append(" inner join schemeapprentice on schemeapprentice.EmpCode = KeyTrans.StaffCode ");
									sb1.append(" where KEYCODENEW = ? and Status=0 ");
									sb1.append(" Union All ");
									sb1.append(" select KeyTrans.Id from KeyTrans  ");
									sb1.append(" inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId ");
									sb1.append(" inner join contractapprentice on contractapprentice.EmpCode = KeyTrans.StaffCode ");
									sb1.append(" where KEYCODENEW = ? and Status=0 ");
									
									java.sql.PreparedStatement ps1 = theConnection.prepareStatement(sb1.toString());
										  ps1 . setString(1,sKeyCode.trim());
										  ps1 . setString(2,sKeyCode.trim());
										  ps1 . setString(3,sKeyCode.trim());
										  ps1 . setString(4,sKeyCode.trim());
										  java.sql.ResultSet rs1 = ps1.executeQuery();
										  while(rs1.next()){										  	
											SRowId=common.parseNull(rs1.getString(1));
										  }
										  rs1.close();
										  ps.close();
										  
									updateData(SKeyId,SRowId,SPersonEmpCode,SSecurityEmpCode);
								//}
								//else
								//{
								//	JOptionPane.showMessageDialog(null,"Key Already Returned.. You May WithDrawn This Now");
								//}
						}						
						else{
						JOptionPane.showMessageDialog(null,"KeyCode Is Empty","Error",JOptionPane.ERROR_MESSAGE);
				   		}
			  }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void setData()
     {
          vData = new Vector();
		  StringBuffer Sb = new StringBuffer();
		  
				Sb.append(" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as WTime,to_char(RETURNTIME,'HH:mi:ss AM') as RTime, ");
				Sb.append(" getEmpName(STAFFCODE) as WdName,To_Char(WdTime,'yyyymmdd') as WDDate,nvl(To_Char(RETURNTIME,'yyyymmdd'),'0') as RtDate, ");
				Sb.append(" getEmpName(SECURITYCODE) as WdFrom, getEmpName(RETURNEDBY) as ReturnName,getEmpName(RETURNEDTO) as RETURNEDTO  from KeyTrans ");
				Sb.append(" Inner join KeyInfo on KeyInfo.KeyId   = KeyTrans.keyId ");
				Sb.append(" where To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ");
					  					
				/*	Sb.append(" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as WTime,to_char(RETURNTIME,'HH:mi:ss AM') as RTime, ");
					Sb.append(" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate,nvl(To_Char(RETURNTIME,'yyyymmdd'),'0') as RtDate  from KeyTrans Inner join KeyInfo on ");
					Sb.append(" KeyInfo.KeyId   = KeyTrans.keyId ");
					Sb.append(" inner join staff on staff.EmpCode = KeyTrans.StaffCode ");
					//Sb.append(" Left join staff on staff.EmpCode = KeyTrans.RETURNEDBY ");
					Sb.append(" where To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ");
					Sb.append(" Union All ");
					Sb.append(" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as WTime,to_char(RETURNTIME,'HH:mi:ss AM') as RTime, ");
					Sb.append(" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate,nvl(To_Char(RETURNTIME,'yyyymmdd'),'0') as RtDate from KeyTrans Inner join KeyInfo on ");
					Sb.append(" KeyInfo.KeyId   = KeyTrans.keyId ");
					Sb.append(" inner join schemeapprentice on schemeapprentice.EmpCode = KeyTrans.StaffCode ");
					//Sb.append(" Left join schemeapprentice on schemeapprentice.EmpCode = KeyTrans.RETURNEDBY ");
					Sb.append(" where To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ");
					Sb.append(" Union All ");
					Sb.append(" Select KeyInfo.KeyName,to_char(WDTime,'HH:mi:ss AM') as WTime,to_char(RETURNTIME,'HH:mi:ss AM') as RTime, ");
					Sb.append(" EmpName,To_Char(WdTime,'yyyymmdd') as WDDate,nvl(To_Char(RETURNTIME,'yyyymmdd'),'0') as RtDate from KeyTrans Inner join KeyInfo on ");
					Sb.append(" KeyInfo.KeyId   = KeyTrans.keyId ");
					Sb.append(" inner join contractapprentice on contractapprentice.EmpCode = KeyTrans.StaffCode ");
					//Sb.append(" Left join contractapprentice on contractapprentice.EmpCode = KeyTrans.RETURNEDBY ");
					Sb.append(" where To_Char(WDTime,'yyyymmdd')=To_char(SysDate,'yyyymmdd') ");*/
          try
          {

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               java.sql.PreparedStatement ps   = theConnection.prepareStatement(Sb.toString());
               java.sql.ResultSet rs = ps.executeQuery();
			   ResultSetMetaData rsmd    = rs.getMetaData();
               while(rs.next())
               {
                    	HashMap themap = new HashMap();
						for(int i=0;i<rsmd.getColumnCount();i++)
						{
							 themap.put(rsmd.getColumnName(i+1),rs.getString(i+1));
						}
						vData.addElement(themap);
               }
               ps.close();
			   rs.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
	private void setTabReport() 
   	{
      	keyModel       .  setNumRows(0);
	  	HashMap   theMap = new HashMap();
      try{
         	for(int i=0;i<vData.size();i++)
		 	{
               theMap     =  (HashMap)vData.elementAt(i);
               Vector    Vect       =  new Vector();
			   Vect                 .  addElement(String.valueOf(i+1));
               Vect                 .  addElement(common.parseNull((String)theMap.get("KEYNAME")));
               Vect                 .  addElement(common.parseNull((String)theMap.get("WTIME")));
			   Vect                 .  addElement(common.parseDate((String)theMap.get("WDDATE")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("WDNAME")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("WDFROM")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("RTIME")));
			   Vect                 .  addElement(common.parseDate((String)theMap.get("RTDATE")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("RETURNNAME")));
			   Vect                 .  addElement(common.parseNull((String)theMap.get("RETURNEDTO")));
               keyModel             .  appendRow(Vect);
         	}
      }
	  catch(Exception ex)
	  {
         ex.printStackTrace();
      }
   }
   private void updateData(String SKeyId,String SRowId, String sStaffCode,String SSecurityCode)
     {
	 	System.out.println("Row Id=="+SRowId);
          String QS           = " Update KeyTrans set Status=1,ReturnTime=sysdate,ReturnedBy="+sStaffCode+",RETURNEDTO="+SSecurityCode+
                                " where KeyId="+SKeyId+" and Id ="+SRowId+" ";
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               theStatement.executeUpdate(QS);
               JOptionPane.showMessageDialog(null,"Details Saved");
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
	 private boolean KeyCheck(String SKeyCodeNew)
	 {
	 	StringBuffer sb = new StringBuffer();
		java.sql.ResultSet rs = null;
		int icount = 0;
		boolean flag = true;		
		sb.append(" Select Count(*) from KeyTrans inner join ");
        sb.append(" KeyInfo on KeyInfo.KeyId = KeyTrans.KeyId ");
        sb.append(" Where KeyInfo.KEYCODENEW='"+SKeyCodeNew+"' and Status=0 ");
		System.out.println("Sqry=" + sb.toString());
		try
		{
				if(theConnection==null)
                {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
			   java.sql.PreparedStatement  ps  = theConnection.prepareStatement(sb.toString());
               rs  = ps.executeQuery();
			   while(rs.next())
               {
			   		icount = common.toInt(common.parseNull(rs.getString(1)));
			   }
			   if(icount >0)
			   {
			   		flag = false;
			   }
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return flag;
		
	 }
	 private boolean KeyCheckReturn(String SKeyId)
	 {
	 	StringBuffer sb = new StringBuffer();
		java.sql.ResultSet rs = null;
		int icount = 0;
		boolean flag = false;
		sb.append(" Select count(*) from KeyTrans where keyid="+SKeyId+" ");
		sb.append(" and Status=1 and to_Char(RETURNTIME, 'YYYYMMDD')=sysdate ");
		try
		{
				if(theConnection==null)
                {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
			   java.sql.PreparedStatement  ps  = theConnection.prepareStatement(sb.toString());
               rs  = ps.executeQuery();
			   while(rs.next())
               {
			   		icount = common.toInt(common.parseNull(rs.getString(1)));
			   }
			   if(icount >0)
			   {
			   		flag = true;
			   }
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return flag;
		
	 }
	 public void setTransactionType()
     {
		  VKeyTrans                    = new Vector();
          VTransTypeCode               = new Vector();
		  StringBuffer sb = new StringBuffer();
		  java.sql.ResultSet rs = null;
		  sb.append(" select TRANSACTION,TYPECODE from KeyTransType ");
          try
          {
              if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
			   java.sql.PreparedStatement  ps  = theConnection.prepareStatement(sb.toString());
               rs  = ps.executeQuery();
               while(rs.next())
               {
					VKeyTrans         . addElement(common.parseNull(rs.getString(1)));
                    VTransTypeCode    . addElement(common.parseNull(rs.getString(2)));
               }
               rs				. 	close();
               ps              	.    close();               
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

	protected void init()			
	{
		PersonFlag = false;
		SecFlag = false;
		System.out.println("First Init");
		capturer.addDataListener(new DPFPDataAdapter() {
                public void dataAcquired(final DPFPDataEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint sample was captured.");
                    process(e.getSample());
				}});
                    System.out.println(" Comming Here");
			}
		});
		capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                public void readerConnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
		 			makeReport("The fingerprint reader was connected.");
				}});
			}
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was disconnected.");
				}});
			}
		});
		capturer.addSensorListener(new DPFPSensorAdapter() {
                public void fingerTouched(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The fingerprint reader was touched.");
				}});
			}
                public void fingerGone(final DPFPSensorEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					makeReport("The finger was removed from the fingerprint reader.");
				}});
			}
		});
		capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                public void onImageQuality(final DPFPImageQualityEvent e) {
				SwingUtilities.invokeLater(new Runnable() {	public void run() {
					if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD))
						makeReport("The quality of the fingerprint sample is good.");
					else
						makeReport("The quality of the fingerprint sample is poor.");
				}});
			}
		});
	}

	protected void process(DPFPSample sample)
	{
		// Draw fingerprint sample image.

		DPFPFeatureSet features = extractFeatures(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

		// Check quality of the sample and start verification if it's good
		int iStatus=0;
   	    String SName  = "";
		String SCode  = "";
		if (features != null)
		{
			// Compare the feature set with our template

               for(int i=0; i<theTemplateList.size(); i++)
               {

                    HashMap theMap = (HashMap) theTemplateList.get(i);

                    SCode = (String)theMap.get("EMPCODE");
                    SName    = (String)theMap.get("DISPLAYNAME");
                    
                    DPFPTemplate template =(DPFPTemplate)theMap.get("TEMPLATE");
     
                    DPFPVerificationResult result = 
                    verificator.verify(features,template);
                    if (result.isVerified())
                    {
                              

//                         JOptionPane.showMessageDialog(null," Verified...and Found Matched");
//                         setVisible(false);
                          	iStatus=1;
	           				break;
                    }
                    else
                    {
//                         	JOptionPane.showMessageDialog(null," Verified...and Found NOT Matched");
     //                    	makeReport("The fingerprint wan NOT VERIFIED.");
                    }
               }
               if(iStatus==0)
               JOptionPane.showMessageDialog(null," No Match Found Punch Again");
			   if(SPersonEmpCode.equals(SCode) || SSecurityEmpCode.equals(SCode))
			   {
			   		 JOptionPane.showMessageDialog(null," Already Punched ");
					 iStatus=0;					
			   }
			   if(iStatus==1 && LPerson.getText().equals("Person Punch") && LSecurity.getText().equals("Security Punch"))
			   {
			   		LPerson.setText(SName);
					LPerson			  .		setForeground(new Color(255,94,94));
		  			LPerson			  .		setFont(NameFont);
			        SPersonEmpCode = SCode;
					iStatus=0;
			   }
			   if(iStatus==1 && LSecurity.getText().equals("Security Punch") && !LPerson.getText().equals("Person Punch"))
			   {
			   		LSecurity.setText(SName);
					LSecurity			  .		setForeground(new Color(255,94,94));
		  			LSecurity			  .		setFont(NameFont);
	 				SSecurityEmpCode= SCode;
                    iStatus=0;
					BOkay.setEnabled(true);
			   }	           
               
          }
	}
	protected void start()
	{
		capturer.startCapture();
	}

	protected void stop()
	{
		capturer.stopCapture();
	}
	public void setPrompt(String string) {
//          prompt.setText(string);
	}
	public void makeReport(String string) {
//          log.append(string + "\n");
	}
	
	protected Image convertSampleToBitmap(DPFPSample sample) {
		return DPFPGlobal.getSampleConversionFactory().createImage(sample);
	}

	protected DPFPFeatureSet extractFeatures(DPFPSample sample, DPFPDataPurpose purpose)
	{
		DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		try {
			return extractor.createFeatureSet(sample, purpose);
		} catch (DPFPImageQualityException e)
        {
            System.out.println(" Returning Null");

        	return null;
		}
	}
     public void onLoad()
     {
			try
               {

//                form          =  null;
                theTemplateList = new ArrayList();     
                Class.forName("oracle.jdbc.OracleDriver");
                Connection theConnection1 = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","essl","essl");

                String sqlStmt=" Select EmpCode,DisplayName,FingerPrint from OneTouchEmployee ";

                Statement st=theConnection1.createStatement();
                ResultSet rs = st.executeQuery(sqlStmt);
                while(rs.next())
                {
                      byte[] data =null ;
                      data  = getOracleBlob(rs,"FINGERPRINT");
                      DPFPTemplate t = DPFPGlobal.getTemplateFactory().createTemplate();
                      t.deserialize(data);
                      HashMap theMap = new HashMap();

                      theMap.put("EMPCODE",rs.getString(1));
                      theMap.put("DISPLAYNAME",rs.getString(2));
                      theMap.put("TEMPLATE",t);
                    
                      theTemplateList.add(theMap);  

                }
			} catch (Exception ex) {

                    System.out.println(ex);
                    ex.printStackTrace();
				JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Fingerprint loading", JOptionPane.ERROR_MESSAGE);
			}
    } 

    private byte[] getOracleBlob(ResultSet result, String columnName) throws SQLException
    {   
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        byte[] bytes = null;
        
        try {    
            oracle.sql.BLOB blob = ((oracle.jdbc.OracleResultSet)result).getBLOB(columnName);        
            inputStream = blob.getBinaryStream();        
            int bytesRead = 0;        
            
            while((bytesRead = inputStream.read()) != -1) {        
                outputStream.write(bytesRead);    
            }
            
            bytes = outputStream.toByteArray();
            
        } catch(IOException e) {
            throw new SQLException(e.getMessage());
        } finally 
        {
        }
    
        return bytes;
    }
}