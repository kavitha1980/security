package client.key;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import java.sql.*;

import util.*;
import blf.*;
import util.MyLabel.*;
import util.TimeField.*;
import client.key.*;

import domain.jdbc.*;


public class KeyReturnFrame extends JInternalFrame implements rndi.CodedNames
{
     Key keyData;

     protected JLayeredPane Layer;

     JPanel    TopPanel,BottomPanel,ListPanel;
     JTable listTable;
     KeyReturnModel keyModel;
     Vector VName,VCode;
     Vector VKeyName,VPerson,VTime,VInfo,VRowId,VWDPerson;
     MyLabel LKeyName,LWDTime,LWDDate,LPerson;
     

     TimeField TWDTime;
     DateField DToday;
     JButton BOkay;
     MyTextField TKeyCardNo,TKeyName;
     MyComboBox JCStaff;
     Common common  = new Common();
     Connection theConnection;
     Vector VKeyId,VSelected,VRetDate,VWDDate;
     int iColumnWidth[]= {50,250,600,600,700,500,400};
                                          

     public KeyReturnFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;
          setDomain();
          setDataIntoVector();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVector1();
     }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               keyData             = (Key)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void createComponents()
     {
          TKeyCardNo          = new MyTextField();
          TKeyName            = new MyTextField();
          JCStaff             = new MyComboBox(VName);

          DToday              = new DateField();
          DToday.setTodayDate();

          LWDTime             = new MyLabel("");
          LKeyName            = new MyLabel("");
          LWDDate             = new MyLabel("");
          LPerson             = new MyLabel("");



          BOkay               = new JButton("Okay");
          TopPanel            = new JPanel();
          ListPanel           = new JPanel();
          BottomPanel         = new JPanel();


          keyModel            = new KeyReturnModel();
          listTable           = new JTable(keyModel);
          listTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          for(int i=0;i<5;i++)
               (listTable.getColumn(keyModel.columnname[i])).setPreferredWidth(iColumnWidth[i]);    
                                                                           
     }
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(6,2));
          TopPanel.setBorder(new TitledBorder("Key Information"));
          BottomPanel.setBorder(new TitledBorder("Control"));
          ListPanel.setLayout(new BorderLayout());
          ListPanel.setBorder(new TitledBorder("Key Movements"));
     }
     private void addComponents()
     {
          setTitle("Key Return");
          setBounds(0,0,400,500);
          setMaximizable(true);
          setClosable(true);
          setIconifiable(true);

          TopPanel.add(new MyLabel("KeyCode"));
          TopPanel.add(TKeyCardNo);
          TopPanel.add(new MyLabel("Date"));
          TopPanel.add(LWDDate);
          TopPanel.add(new MyLabel("Time"));
          TopPanel.add(LWDTime); 
          TopPanel.add(new MyLabel("KeyName"));
          TopPanel.add(LKeyName);
          TopPanel.add(new MyLabel("Person"));
          TopPanel.add(LPerson);
          TopPanel.add(new MyLabel("Return By"));
          TopPanel.add(JCStaff);

          ListPanel.add("North",listTable.getTableHeader());
          ListPanel.add(new JScrollPane(listTable));

          BottomPanel.add(BOkay);
          BOkay.setEnabled(false);

          BOkay.setMnemonic('O');

          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",ListPanel);
          getContentPane().add("South",BottomPanel);
     }

     private void addListeners()
     {
          TKeyCardNo.addKeyListener(new ActList());
          BOkay.addActionListener(new OnOkay());
     }
     private class ActList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    if(isAvailable())
                    {
                         setData();
                         showData();
                         BOkay.setEnabled(true);
                    }
                    else
                         JOptionPane.showMessageDialog(null,"Key Not Available","Information",JOptionPane.ERROR_MESSAGE,null);
               }
          }
     }

     private class OnOkay implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               updateData();
               setDefault();
          }
     }
     private void setDataIntoVector()
     {
          try
          {
               Vector VInfo   = keyData.getUserInformation();

               VCode          = (Vector)VInfo.elementAt(0);
               VName          = (Vector)VInfo.elementAt(1);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     private void setDefault()
     {
          Layer.remove(1);
          setVisible(false);
     }
     private void updateData()
     {

          int iId             = common.toInt((String)VKeyId.elementAt(0));
          int iStaffCode      = common.toInt((String)VCode.elementAt(JCStaff.getSelectedIndex()));
          String QS           = " Update KeyTrans set Status=1,ReturnTime=sysdate,ReturnedBy="+iStaffCode+"  "+
                                " where KeyId="+iId+" and Id ="+common.toInt((String)VRowId.elementAt(0))+" ";

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               theStatement.executeUpdate(QS);
               JOptionPane.showMessageDialog(null,"Details Saved");
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void showData()
     {
          LKeyName.setText((String)VSelected.elementAt(0));
          LPerson.setText((String)VSelected.elementAt(1));
          LWDTime.setText((String)VSelected.elementAt(2));
          LWDDate.setText(common.parseDate((String)VSelected.elementAt(3)));
     }
     private void setData()
     {

          VKeyId              = new Vector();
          VSelected           = new Vector();
          VRowId              = new Vector();         
		  String QS 		  = "";
          String SCardNo      = common.parseNull((String)TKeyCardNo.getText());

         /* String QS           = " select KeyTrans.KeyId,KeyName,EmpName,to_Char(WDTime,'HH:mi:ss AM') as WDTime, "+
                                " to_Char(WDTime,'yyyymmdd') as WDDate,KeyTrans.Id "+
                                " from KeyTrans inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId "+
                                " inner join VisitorStaff on VisitorStaff.EmpCode    = KeyTrans.StaffCode "+
                                " where KeyCode = '"+SCardNo+"' and Status=0 "; */
								QS = " select KeyTrans.KeyId,KeyName,EmpName,to_Char(WDTime,'HH:mi:ss AM') as WDTime, "+
								" to_Char(WDTime,'yyyymmdd') as WDDate,KeyTrans.Id,KeyTrans.StaffCode "+
								" from KeyTrans inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId "+
								" inner join Staff on Staff.empcode=KeyTrans.StaffCode  "+
								" where KeyCode = '"+SCardNo+"' and Status=0 "+
								" Union All "+
								" select KeyTrans.KeyId,KeyName,EmpName,to_Char(WDTime,'HH:mi:ss AM') as WDTime, "+
								" to_Char(WDTime,'yyyymmdd') as WDDate,KeyTrans.Id,KeyTrans.StaffCode "+
								" from KeyTrans inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId "+
								" inner join schemeapprentice on schemeapprentice.empcode=KeyTrans.StaffCode "+
								" where KeyCode = '"+SCardNo+"' and Status=0 "+
								" Union All "+
								" select KeyTrans.KeyId,KeyName,EmpName,to_Char(WDTime,'HH:mi:ss AM') as WDTime, "+
								" to_Char(WDTime,'yyyymmdd') as WDDate,KeyTrans.Id,KeyTrans.StaffCode "+
								" from KeyTrans inner join KeyInfo on KeyTrans.KeyId = KeyInfo.KeyId "+
								" inner join contractapprentice on contractapprentice.empcode=KeyTrans.StaffCode "+
								" where KeyCode = '"+SCardNo+"' and Status=0 ";
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VKeyId.addElement(theResult.getString(1));
                    VSelected.addElement(common.parseNull((String)theResult.getString("KEYNAME")));
                    VSelected.addElement(common.parseNull((String)theResult.getString("EMPNAME")));
                    VSelected.addElement(common.parseNull((String)theResult.getString("WDTIME")));
                    VSelected.addElement(common.parseNull((String)theResult.getString("WDDATE")));
                    VRowId.addElement(common.parseNull((String)theResult.getString("ID")));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          /*try
          {
               Vector VInfo   = keyData.getKeyInformation(SCardNo);

               Vector VKeyId       = (Vector)VInfo.elementAt(0);
               Vector VKeyName     = (Vector)VInfo.elementAt(1);
     
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }*/

     }

     private void setVector1()
     {

          VKeyName       = new Vector();
          VTime          = new Vector();
          VPerson        = new Vector();
          VInfo          = new Vector();
          VWDPerson      = new Vector();
          VWDDate        = new Vector();
          VRetDate       = new Vector();


          String QS = " select KeyName,time,returnname,empname as WDname,WDDate,RetDate from "+
                      " ( "+    
                      " Select KeyInfo.KeyName,to_char(ReturnTime,'HH24:mi:ss PM ') as Time, "+
                      " EmpName as returnname,staffcode,To_Char(WDTime,'yyyymmdd') as WDDate,"+
                      " To_char(ReturnTime,'yyyymmdd')  as RetDate "+
                      " from KeyTrans Inner join KeyInfo on "+
                      " KeyInfo.KeyId   = KeyTrans.keyId "+
                      " inner join VisitorStaff on VisitorStaff.EmpCode = KeyTrans.ReturnedBy  "+
                      " where Status=1 and To_Char(ReturnTime,'yyyymmdd')=To_char(Sysdate,'yyyymmdd')) temp  "+
                      " inner join VisitorStaff on VisitorStaff.EmpCode = temp.staffcode ";

          try
          {

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VKeyName.addElement(theResult.getString(1));
                    VTime.addElement(theResult.getString(2));
                    VPerson.addElement(theResult.getString(3));
                    VWDPerson.addElement(theResult.getString(4));
                    VWDDate.addElement(common.parseDate((String)theResult.getString(5)));
                    VRetDate.addElement(common.parseDate((String)theResult.getString(6)));

               }

               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          VInfo.addElement(VKeyName);
          VInfo.addElement(VWDDate);
          VInfo.addElement(VTime);
          VInfo.addElement(VPerson);
          VInfo.addElement(VRetDate);
          VInfo.addElement(VWDPerson);
          keyModel.setVector1(VInfo);
          ListPanel.updateUI();
          ListPanel.repaint();
     }
     public boolean isAvailable()
     {
          String SCardNo      = common.parseNull((String)TKeyCardNo.getText());

          String QS           =    " Select Count(KeyTrans.KeyId) from KeyTrans inner join "+
                                   " KeyInfo on KeyInfo.KeyId = KeyTrans.KeyId "+
                                   " Where KeyInfo.KeyCode='"+SCardNo+"' and Status=0 ";
          int iCount=0;
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    iCount              = common.toInt((String)theResult.getString(1));
               }
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          if(iCount==0)
               return false;
          else
               return true;
     }
}
