package client.key;

import java.awt.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;
import javax.swing.*;
import util.*;
import guiutil.*;

public class KeyTransactionModel extends DefaultTableModel
{    
     String ColumnName[] = {"S.NO","KeyName","WdTime","WdDate","WdName","WdFrom","RetTime","RetDate","ReturnName","ReturnTo"};
     String ColumnType[] = { "S", "S", "S","S","S","S","S","S","S" ,"S","S"};
	 int  iColumnWidth[] = {30,110  ,80   ,40  ,80  ,50  ,80  ,55 ,55,55};

     Common common = new Common();
     Vector vect;

     public KeyTransactionModel()
     {
          setDataVector(getRowData(),ColumnName);
     }
     public Class getColumnClass(int iCol)
     {
          return getValueAt(0,iCol).getClass();
     }

     public boolean isCellEditable(int iRow,int iCol)
     {
          if(ColumnType[iCol] == "E" || ColumnType[iCol] == "B")
               return true;

          return false;
     }

     private Object[][] getRowData()
     {
          Object RowData[][] = new Object[1][ColumnName.length];

          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i] = "";

          return RowData;
     }

     public void appendRow(Vector theVect)
     {
          insertRow(getRows(),theVect);
     }

     public int getRows()
     {
          return super.dataVector.size();
     }
}