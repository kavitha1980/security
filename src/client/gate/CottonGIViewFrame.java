/*


*/

package client.gate;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.io.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import guiutil.*;
import blf.*;
import rndi.*;



public class CottonGIViewFrame extends JInternalFrame implements CodedNames
{
     protected JLayeredPane Layer;

     RowSet    rowSet;
     Vital     theData;
     CottonGI  cottonGI;

     JPanel   TopPanel,BottomPanel;
     ReceiptPanel receiptPanel;

     Vector      ReceiptVector ;

     Common common = new Common();


     public CottonGIViewFrame(JLayeredPane Layer)
     {
          this.Layer = Layer;

          try
          {
               setVital();
               createComponents();
               setLayouts();
               addComponents();
               setData();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void createComponents()
     {
          try
          {
               TopPanel       = new JPanel(true);
               receiptPanel    = new ReceiptPanel();
               BottomPanel    = new JPanel(true);
     
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

     }

     private void setLayouts()
     {

          setTitle("List of Gate Inwards Entered Today (**)");
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,500);

          TopPanel.setLayout(new BorderLayout());

     }

     private void addComponents()
     {
          getContentPane().add("North",TopPanel);
          getContentPane().add("Center",receiptPanel);
          getContentPane().add("South",BottomPanel);

     }


     private void setVital()
     {
          try
          {
               Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               theData           = (Vital)registry.lookup(DOMAIN);
               cottonGI          = (CottonGI)registry.lookup(DOMAIN);

          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Unable to Load Data from Server.","Error",JOptionPane.ERROR_MESSAGE);
          }
     }


     private void setData()
     {

          try
          {

               ReceiptVector   = cottonGI.getGateInwards(common.getCurrentDate());
     
               receiptPanel .setDetails(ReceiptVector);

          }
          catch(Exception ex)
          {
               System.out.println("Unable to get Receipt Details");
          }
     }


     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }



/****Table Panel starts ****/
public class ReceiptPanel extends JPanel
{
     ReceiptModel  theReceiptModel;
     JTable theReceiptTable;

     int iMaterial = 0;

     public ReceiptPanel()
     {
          theReceiptModel = new ReceiptModel();
          theReceiptTable = new JTable(theReceiptModel);
          theReceiptTable.setAutoResizeMode(theReceiptTable.AUTO_RESIZE_OFF);

          setColumnWidths();

          setBorder(new TitledBorder(" List Of Gate Inwards"));
          setLayout(new BorderLayout());

          add("North",theReceiptTable.getTableHeader());
          add("Center",new JScrollPane(theReceiptTable));

     }


     public void setDetails(Vector vect)
     {
          theReceiptModel.zap();
          theReceiptModel.appendDetails(vect);
     }


     public void setColumnWidths()
     {
          for (int col=0;col<theReceiptTable.getColumnCount();col++)
          {
               theReceiptTable.getColumn(theReceiptModel.ColumnName[col]).setPreferredWidth(theReceiptModel.ColumnWidth[col]);
          }
     }

}

/****Table Model starts ****/

public class ReceiptModel extends DefaultTableModel
{

     int GINO          = 0;
     int SUPPLIER      = 1;
     int DCNO          = 2;
     int DCDATE        = 3;
     int INVNO         = 4;
     int INVDATE       = 5;
     int TRUCKNO       = 6;
     int TIMEIN        = 7;
     int SECURITYCODE  = 8;
     int LOTNO         = 9;
     int BALES         = 10;


     String ColumnName[]   = {"GI No","Supplier" ,"DC No"  ,"DC Date"  ,"Inv No" ,"Inv Date" ,"Truck No" ,"Time In" ,"Security Code","Lot No","Bales"};
     String ColumnType[]   = {"S"    ,"S"        ,"S"      ,"S"        ,"S"      ,"S"        ,"S"        ,"S"       ,"S"            ,"S"     ,"S"    };
     int    ColumnWidth[]  = {50     ,150        ,60       ,60         ,60       ,60         ,50         ,70        ,50             ,100     ,50     };


     public ReceiptModel()
     {
          setTableData();

     }

     public void setTableData()
     {
          setDataVector(getRowData(),ColumnName);     

     }

     public Class getColumnClass(int col)
     {
          return getValueAt(0,col).getClass();
     }

     public boolean isCellEditable(int row,int col)
     {
          return false;
     }

     public void setValueAt(Object aValue, int row, int column)
     {
          try
          {
               Vector rowVector = (Vector)super.dataVector.elementAt(row);
               rowVector.setElementAt(aValue, column);
               fireTableChanged(new TableModelEvent(this, row, row, column));
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public int getRows()
     {
          return super.dataVector.size();
     }


     private Object[][] getRowData()
     {
          Object RowData[][];
          RowData = new Object[1][ColumnName.length];
          for(int i=0;i<ColumnName.length;i++)
               RowData[0][i]="";
          return RowData;
     }


     public void appendEmptyRow()
     {
          Vector vect = new Vector();
          for(int i=0;i<getColumnCount();i++)
               vect.addElement("");
          insertRow(getRows(),vect);

     }

     public void appendDetails(Vector vect)
     {

          if(!(vect.size()>0))
               fireTableChanged(new TableModelEvent(this));

          for(int i=0;i<vect.size();i++)
          {
               appendEmptyRow();

               int iRow   = getRows()-1;
               HashMap row = (HashMap)vect.elementAt(i);

               String SGINo    = common.parseNull((String)row.get("GINO"));
               String SName    = common.parseNull((String)row.get("SUPPLIER"));
               String SDCNo    = common.parseNull((String)row.get("DCNO"));
               String SDCDate  = common.parseDate((String)row.get("DCDATE"));
               String SInvNo   = common.parseNull((String)row.get("INVNO"));
               String SInvDate = common.parseDate((String)row.get("INVDATE"));
               String STruckNo = common.parseNull((String)row.get("TRUCKNO"));
               String STimeIn  = common.parseNull((String)row.get("TIMEIN"));
               String SCode    = common.parseNull((String)row.get("SECCODE"));
               String SLotNo   = common.parseNull((String)row.get("PARTYLOTNO"));
               String SBales   = common.parseNull((String)row.get("BALES"));

               setValueAt(SGINo    ,iRow,GINO    );
               setValueAt(SName    ,iRow,SUPPLIER);
               setValueAt(SDCNo    ,iRow,DCNO    );
               setValueAt(SDCDate  ,iRow,DCDATE  );
               setValueAt(SInvNo   ,iRow,INVNO   );
               setValueAt(SInvDate ,iRow,INVDATE );
               setValueAt(STruckNo ,iRow,TRUCKNO );
               setValueAt(STimeIn  ,iRow,TIMEIN  );
               setValueAt(SCode    ,iRow,SECURITYCODE);
               setValueAt(SLotNo   ,iRow,LOTNO   );
               setValueAt(SBales   ,iRow,BALES   );


          }
     }

     public void zap()
     {
          super.dataVector.removeAllElements();
     }



}



}
