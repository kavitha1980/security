/*

     A class to manage arrival of the rawmaterials
     Received From Dyer @ Gate

     This class can also act like a modifier as
     signled by the Primary Key
*/

package client.gate;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import guiutil.*;
import blf.*;
import rndi.*;

public class GateInwardFrame extends JInternalFrame implements CodedNames
{
     protected JLayeredPane Layer;
     protected int iPK = -1;

     Vital theData;
     Purchase  purchase;
     RowSet    rowSet;

     JPanel MiddlePanel,MidTop,MidBot,BottomPanel;

     DateField        TGIDate,TDCDate,TInvoiceDate;
     FractionNumberField     TWeight;
     WholeNumberField TGINo,TBales;
     DocNoField       TDCNo,TInvoiceNo;
     NoNumberField    TTruckNo;
//     JTextField       TTimeIn;
     ClockField       TTimeIn;
     MyComboBox       JCDyer,JCFibreType;
     JTextArea        TA;
     TimeField        theTime; 
     
     MyButton         BOkay;
     PrintButton      BPrint;
     JButton          BExit;
     int iMaterialType = 0;
     String STitle;

     Common common = new Common();

     public GateInwardFrame(JLayeredPane Layer)
     {
          this.Layer = Layer;
          setScreenTitle(iMaterialType);
          setVital();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public GateInwardFrame(JLayeredPane Layer,int iPK)
     {
          this.Layer = Layer;
          this.iPK   = iPK;
          setScreenTitle(iMaterialType);
          setVital();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     public GateInwardFrame(JLayeredPane Layer,int iPK, int iMaterialType)
     {
          this.Layer = Layer;
          this.iPK   = iPK;
          this.iMaterialType   = iMaterialType;
          setScreenTitle(iMaterialType);
          setVital();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
     }

     private void createComponents()
     {
          try
          {
               MiddlePanel     = new JPanel();
               MidTop          = new JPanel();
               MidBot          = new JPanel();
               BottomPanel     = new JPanel();
     
               TGIDate         = new DateField(theData.getStartDate(),theData.getEndDate());                
               TGINo           = new WholeNumberField(10);
              
               JCDyer          = new MyComboBox(theData.getData("DYER"));

               if(iMaterialType==0)
                    JCFibreType     = new MyComboBox(theData.getData("SFTYPE"));
               else if(iMaterialType==1)
               {
                    JCFibreType     = new MyComboBox();
                    JCFibreType.addItem("COTTON");
               }
               TDCNo           = new DocNoField(20);
               TDCDate         = new DateField();

               TTruckNo        = new NoNumberField(15);
//               TTimeIn         = new JTextField();
               TTimeIn         = new ClockField();

               TInvoiceNo      = new DocNoField(20);
               TInvoiceDate    = new DateField();

               TBales          = new WholeNumberField(6);
               TWeight         = new FractionNumberField(12,1);
               theTime         = new TimeField();
     
               TA              = new JTextArea(10,20);

               BOkay           = new MyButton("Okay");
               BPrint          = new PrintButton();
               BExit           = new JButton("Exit");
          }
          catch(Exception ex)
          {
          }
     }

     private void setLayouts()
     {
          setTitle(STitle);
          setClosable(true);
          setMaximizable(true);
          setIconifiable(true);
          setResizable(true);
          setBounds(0,0,800,500);

          MidTop.setLayout(new GridLayout(12,3));
          MidBot.setLayout(new BorderLayout());
          MiddlePanel.setLayout(new BorderLayout());

          MidTop.setBorder(new TitledBorder("Fibre Gate Inward Details"));
          MidBot.setBorder(new TitledBorder("Description & Narration"));

          BottomPanel.setBorder(new TitledBorder(""));
          TTimeIn.setEditable(false);
          BOkay.setMnemonic('O');
          BExit.setMnemonic('E');
          setPresets();
     }

     private void addComponents()
     {
          MidTop.add(new JLabel("Inward Date"));
          MidTop.add(TGIDate);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Inward No"));
          MidTop.add(TGINo);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Supplier Name"));
          MidTop.add(JCDyer);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("DC No"));
          MidTop.add(TDCNo);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("DC Date"));
          MidTop.add(TDCDate);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Invoice No"));
          MidTop.add(TInvoiceNo);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Invoice Date"));
          MidTop.add(TInvoiceDate);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Truck No"));
          MidTop.add(TTruckNo);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Time In"));
          MidTop.add(TTimeIn);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Fibre Type"));
          MidTop.add(JCFibreType);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("No of Bales"));
          MidTop.add(TBales);
          MidTop.add(new JLabel(" "));
          MidTop.add(new JLabel("Load Weight"));
          MidTop.add(TWeight);
          MidTop.add(new JLabel(" "));

          MidBot.add("Center",new JScrollPane(TA));

          MiddlePanel.add("North",MidTop);
          MiddlePanel.add("Center",MidBot);

          BottomPanel.add(BOkay);
          BottomPanel.add(BPrint);
          BottomPanel.add(BExit);
          getContentPane().add("Center",MiddlePanel);
          getContentPane().add("South",BottomPanel);
     }

     private void addListeners()
     {
          BOkay.addActionListener(new ActList());
          BExit.addActionListener(new ActList());
     }
     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BOkay)
               {
                    saveData();
               }
               if(ae.getSource()==BExit)
               {
                    showConfirmMsg();
               }
          }
     }

     private void saveData()
     {
          if(!isValidData())
               return;
          setDomainRowSet();
          persist();
          refresh();
     }

     private void refresh()
     {
          if(iPK != -1)
               return ;
           TGINo.setText("");
           TDCNo.setText("");
           TInvoiceNo.setText("");
           TTruckNo.setText("");
           TTimeIn.setText("");
           TBales.setText("");
           TWeight.setText("");
           TA.setText("");
           TGIDate.requestFocus();
           setPresets();
     }
      private void setPresets()
      {
            TTimeIn.setText(getCurrentTime());
      }


     private void setDomainRowSet()
     {
          String ColumnName[] = {"GINO","GIDATE","ACCODE",
                                 "DCNO","DCDATE","INVOICENO",
                                 "INVOICEDATE","TRUCKNO","TIMEIN",
                                 "FIBRETYPECODE","BALES","WEIGHT",
                                 "NARRATION","STATUS"};

          
          String ColumnType[] = {"N","N","S",
                                 "S","N","S",
                                 "N","S","S",
                                 "N","N","D",
                                 "S","N"};


          Vector theVector = new Vector();

          try
          {        
               theVector.addElement(TGINo.getText());
               theVector.addElement(String.valueOf(TGIDate.toNormal()));
               theVector.addElement(theData.findByIndex("DYERCODE",JCDyer.getSelectedIndex()));

               theVector.addElement(TDCNo.getText());
               theVector.addElement(String.valueOf(TDCDate.toNormal()));
               theVector.addElement(TInvoiceNo.getText());

               theVector.addElement(String.valueOf(TInvoiceDate.toNormal()));
               theVector.addElement((TTruckNo.getText()).toUpperCase());
               theVector.addElement(TTimeIn.getText());

               theVector.addElement(String.valueOf(JCFibreType.getSelectedIndex()+2));
               theVector.addElement(TBales.getText());
               theVector.addElement(TWeight.getText());

               theVector.addElement(common.parseNull(TA.getText().trim()));
               theVector.addElement("0");

               rowSet = new RowSet();

               rowSet.setColumnName(ColumnName);
               rowSet.setColumnType(ColumnType);
               rowSet.appendRow(theVector);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               JOptionPane.showMessageDialog(null,getWarningMessage(),"Error",JOptionPane.ERROR_MESSAGE);
          }
     }
     private void persist()
     {
          try
          {
               Vector VId = new Vector();

               Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               purchase  = (Purchase)registry.lookup(DOMAIN);
               if(iPK==-1)
               {
                    VId = purchase.insertPurchaseInwardData(rowSet);
                    //showGIMessage(VId);
                    //removeHelpFrame();
               }
               else
                    purchase.updatePurchaseInwardData(rowSet,iPK);
          }
          catch(Exception ex)
          {
          }    
     }

     private void setVital()
     {
          try
          {
               Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               theData           = (Vital)registry.lookup(DOMAIN);
          }
          catch(Exception ex)
          {
               JOptionPane.showMessageDialog(null,"Unable to Load Vital Data.","Error",JOptionPane.ERROR_MESSAGE);
          }

     }
     private boolean isValidData()
     {
          if(common.toInt(TGINo.getText())==0)
          {
               JOptionPane.showMessageDialog(null,"Please Fill Gate Inward No","Dear user,",JOptionPane.ERROR_MESSAGE);
               TGINo.requestFocus();
               return false;
          }
          if(isDuplicateGI())
          {
               JOptionPane.showMessageDialog(null,"Gate Inward No is Duplicated","Dear user,",JOptionPane.ERROR_MESSAGE);
               TGINo.requestFocus();
               return false;
          }

          if(TGIDate.toNormal().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Inward Date not Filled","Error",JOptionPane.ERROR_MESSAGE);
               TGIDate.requestFocus();
               return false;
          }

          if(TDCNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"DC No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TDCNo.requestFocus();
               return false;
          }

          if(TInvoiceNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Invoice No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TInvoiceNo.requestFocus();
               return false;
          }

          if(common.toInt(TDCDate.toNormal())==0)
          {
               JOptionPane.showMessageDialog(null,"DC Date Should Be Filled","Dear user,",JOptionPane.ERROR_MESSAGE);
               TDCDate.requestFocus();
               return false;
          }

          if(common.toInt(TInvoiceDate.toNormal())==0)
          {
               JOptionPane.showMessageDialog(null,"Invoice Date Should Be Filled","Dear user,",JOptionPane.ERROR_MESSAGE);
               TInvoiceDate.requestFocus();
               return false;
          }

          if(common.toInt(TDCDate.toNormal()) > common.toInt(TGIDate.toNormal()))
          {
               JOptionPane.showMessageDialog(null,"DC Date & GI Date Mis-Matches","Dear user,",JOptionPane.ERROR_MESSAGE);
               TDCDate.requestFocus();
               return false;  
          }

          if(common.toInt(TInvoiceDate.toNormal()) > common.toInt(TGIDate.toNormal()))
          {
               JOptionPane.showMessageDialog(null,"Invoice Date & GI Date Mis-Matches","Dear user,",JOptionPane.ERROR_MESSAGE);
               TInvoiceDate.requestFocus();
               return false;  
          }

          if(TTruckNo.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Truck No Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TTruckNo.requestFocus();
               return false;
          }

          if(TTimeIn.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null,"Time In Must be filled","Error",JOptionPane.ERROR_MESSAGE);
               TTimeIn.requestFocus();
               return false;
          }

          if(common.toInt(TBales.getText())<=0)
          {
               JOptionPane.showMessageDialog(null,"Invalid No Bales","Dear user,",JOptionPane.ERROR_MESSAGE);
               TBales.requestFocus();
               return false;
          }

          if(common.toDouble(TWeight.getText())<=0)
          {
               JOptionPane.showMessageDialog(null,"Invalid Pay Load Weight","Dear user,",JOptionPane.ERROR_MESSAGE);
               TWeight.requestFocus();
               return false;
          }

          return true;
     }

     private String getWarningMessage()
     {
          String str = "<html><body>";
          str = str + "<b>Problem in persisting the Data.</b>"+"<br>";
          str = str + "It may be either due to a poor network<br>";
          str = str + "or<br>";          
          str = str + "violation of data management principles.<br>";
          str = str + "Contact your system administrator.<br>";          
          str = str + "</body></html>";
          return str;
     }

     private boolean isDuplicateGI()
     {
          int iGINo = common.toInt(TGINo.getText());

          try
          {
               Registry registry = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               purchase   = (Purchase)registry.lookup(DOMAIN);
               HashMap theMap = purchase.findByGatePurchaseId(iGINo);
               int iXGINo     = common.toInt((String)theMap.get("GINO"));
               if(iGINo == iXGINo)
                    return true;
               return false;
          }
          catch(Exception ex)
          {
          }
          return false;
     }

     private String getGIMessage(String SId)
     {
          String str = "<html><body>";
          str = str + "<b>Id For this Transaction : "+SId+"</b><br>";
          str = str + "A New Gate Inward Transaction was<br>";
          str = str + "successfully registered<br>";          
          str = str + "</body></html>";
          return str;
     }
     private void showGIMessage(Vector VId)
     {
          if(VId.size()==0)
               return;
          String SId = (String)VId.elementAt(0);
          JOptionPane.showMessageDialog(null,getGIMessage(SId),"Dear User",JOptionPane.INFORMATION_MESSAGE);
     }
     private void removeHelpFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex)
          {
          }
     }
     private String getCurrentTime()
     {
           return theTime.getTimeNow();
          
     }
     private void showConfirmMsg()
     {
          if((JOptionPane.showConfirmDialog(null,"Exit without saving...? ","Warning",JOptionPane.YES_NO_OPTION)) == 0)
          {
               removeHelpFrame();
          }               
          else
          {
               return;
          }               
     }

     private void setScreenTitle(int iMaterialType)
     {
          if(iMaterialType == 0)
               STitle = "Gate Inward Entry (Purchased Fibres) (34a)";
          else if(iMaterialType == 1)
               STitle = "Gate Inward Entry (Cotton) (**)";
     }


}
