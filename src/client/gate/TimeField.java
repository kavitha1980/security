/*

     To make use of the System Time in UIs

*/
package client.gate;

import java.util.*;
import java.io.*;
import java.text.*;

public class TimeField
{
      Calendar rightnow;
      Date     myTime;
      int      myHour;
      int      myMinute;
      int      mySecond;
      int      myAM_PM;
      int      myNumber;
     
      String   mySHour   = " " ;
      String   mySMinute = " " ;
      String   mySSecond = " " ;
      String   mySTime   = " " ;

      TimeField()
      {
      }
      public String getTimeNow()
      {
            rightnow=Calendar.getInstance();
            myTime=rightnow.getTime();
            myHour=rightnow.get(Calendar.HOUR);
            myMinute=rightnow.get(Calendar.MINUTE);
            mySecond=rightnow.get(Calendar.SECOND);
            myAM_PM=rightnow.get(Calendar.AM_PM);

            if (myHour==0) 
            {
                  myHour  = myHour + 12;
                  mySHour = Integer.toString(myHour);
            }

            if (myHour<10)
            {
                  mySHour = Integer.toString(myHour);
                  mySHour = "0"+mySHour;
            }
            else
            {
                  mySHour = Integer.toString(myHour);
            }

            if (myMinute<10)
            {
                  mySMinute = Integer.toString(myMinute);
                  mySMinute = "0"+mySMinute;
            }
            else
            {
                  mySMinute = Integer.toString(myMinute);
            }


            if (mySecond<10)
            {
                  mySSecond = Integer.toString(mySecond);
                  mySSecond = "0"+mySSecond;
            }
            else                      
            {
                  mySSecond = Integer.toString(mySecond);
            }

            mySTime =mySHour+":"+mySMinute+":"+mySSecond+" "+(myAM_PM==0?"AM":"PM");
            return mySTime;                                                       
      }
}

