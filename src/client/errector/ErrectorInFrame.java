 /*

     To acquire details of visitor(s) inward

     The First Class to instantiated for a visitor

     Refer the helpers

          CompanyModel 
          VisitorModel

*/
package client.errector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.util.Date;
import java.util.GregorianCalendar;

import util.DateField1;
import util.DateField;
import util.TimeField;
import util.ClockField;
import util.Common;
import domain.jdbc.*;

import utility.Control;
import java.sql.*;
import blf.*;

import java.rmi.*;
import java.rmi.registry.*;


public class ErrectorInFrame extends JInternalFrame implements rndi.CodedNames
{

      protected JLayeredPane Layer;

	  java.sql.Connection theConnection = null;
      JTabbedPane    InTabPane;
      JPanel         TopPanel,MiddlePanel,BottomPanel;
      JTextField     TSlipNo;
      DateField1      TDate;
      JTextField     TTime;
      JComboBox      JCDeptName,JCUnitName; 
      CompanyModel   theCompany;
      ErrectorInModel   theModel;
      JTable         theTable;
      TimeField      theTime;
      JButton        BOkay,BExit;
      Control        control;
      Common         common;
      String         maxslipno;
      VisitorIn      visitorDomain;

	  Vector vDeptCode,vDeptName,vUnitCode,vUnitName;

      public ErrectorInFrame(JLayeredPane Layer)
      {
            this.Layer = Layer;
            setDomain();
			//setUnit();
            createComponents();
            setLayouts();
            addComponents();
            addListeners();
      }

      private void setDomain()
      {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       =(VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private void createComponents()
      {
            InTabPane      = new JTabbedPane();   

            TopPanel       = new JPanel(true);
            MiddlePanel    = new JPanel(true);
            BottomPanel    = new JPanel(true);

            TSlipNo        = new JTextField();
            TDate          = new DateField1();
            TTime          = new JTextField();

			//JCDeptName	   = new JComboBox(vDeptName);
			//JCUnitName	   = new JComboBox(vUnitName);

            theCompany     = new CompanyModel();
            theCompany.setMnemonic('C');

            theModel       = new ErrectorInModel(theCompany);
            theTable       = new JTable(theModel);

            theTime        = new TimeField();

            BExit          = new JButton("Exit");
            BOkay          = new JButton("Okay");
            BOkay.setMnemonic('O');
            BExit.setMnemonic('X');

            common         = new Common();
            control        = new Control();

      }

      private void setLayouts()
      {

            setTitle("Errctor Information Feeder - Inward(3.0)");
            setClosable(true);
            setMaximizable(true);
            setIconifiable(true);
            setResizable(true);
            setBounds(0,0,798,520);

            TopPanel.setLayout(new GridLayout(1,4));
            MiddlePanel.setLayout(new BorderLayout());

            TopPanel.setBorder(new TitledBorder("Company"));
            MiddlePanel.setBorder(new TitledBorder("Errector Information"));

            TSlipNo.setBorder(new TitledBorder("Slip No"));
            TDate.setBorder(new TitledBorder("Date"));
            TTime.setBorder(new TitledBorder("In-Time"));
		    //JCUnitName.setBorder(new TitledBorder("Unit"));
			//JCDeptName.setBorder(new TitledBorder("Dept"));
          
            TSlipNo.setEditable(false);
            TTime.setEditable(false);

            setPresets();
      }


      private void addComponents()
      {
            TopPanel.add(TSlipNo);
            TopPanel.add(TDate);
            TopPanel.add(TTime);
			//TopPanel.add(JCUnitName);
			//TopPanel.add(JCDeptName);
            TopPanel.add(theCompany);

            MiddlePanel.add("North",theTable.getTableHeader());
            MiddlePanel.add("Center",new JScrollPane(theTable));

            setTitle("Errector In Frame(3.0)");

            InTabPane.setFont(new Font("Copperplate Gothic Bold",Font.BOLD,11));   
            InTabPane.addTab("ErrectorInfo",MiddlePanel);
            InTabPane.addTab("Today's Errector Info",new ErrectorTabInModel());

            BottomPanel.add(BOkay);
            BottomPanel.add(BExit);
            getContentPane().add("North",TopPanel);
            getContentPane().add("Center",InTabPane);
            getContentPane().add("South",BottomPanel);
      }
     
     public class ErrectorTabInModel extends JPanel
     {
          JTable todaytable;
          
          public ErrectorTabInModel()
          {
               try
               {
                    DateField1 da    =    new DateField1();
                    da.setTodayDate();
                    int iDate = Integer.parseInt(da.toNormal());
                    setLayout(new BorderLayout());

                    Vector EVector =    visitorDomain.getErrectorInInfo(iDate,iDate);
                    ErrectorsTodayInModel EModel  =    new ErrectorsTodayInModel(EVector);     
                    JTable ETable  =    new JTable(EModel);
                    JScrollPane EPane   =    new JScrollPane(ETable);
                    add(EPane);
               }

               catch(Exception e)
               {
                    e.printStackTrace();
               }
          }
          private void setPreferredWidth()
          {
               todaytable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
               (todaytable.getColumn("CompanyName")).setPreferredWidth(200);
          }
     }
      private void addListeners()
      {
            theTable.addKeyListener(new TableKeyList());
            BOkay.addActionListener(new SaveList());
            BExit.addActionListener(new exitList());
      }
      private class exitList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  setVisible(false);
            }
      }
      private class TableKeyList extends KeyAdapter
      {

            public void keyPressed(KeyEvent ke)
            {
                  if(ke.getKeyCode()==KeyEvent.VK_INSERT)
                  {
                        appendRow();
                  }

                  if(ke.getKeyCode()==KeyEvent.VK_DELETE)
                  {
                        deleteRow(theTable.getSelectedRow());
                  }

                  if(ke.getKeyCode()==KeyEvent.VK_ENTER)
                  {
                        showHelpFrame(theTable.getSelectedRow(),theTable.getSelectedColumn());
                  }
            }
      }
      private void appendRow()
      {

            theModel.appendRow();
      }

      private void deleteRow(int iRow)
      {
            theModel.deleteRow(iRow);
      }

      private void showHelpFrame(int iRow,int iCol)
      {
			theModel.showHelpFrame(iRow,iCol);
      }

      private class SaveList implements ActionListener
      {
            public void actionPerformed(ActionEvent ae)
            {
                  boolean OkFlag = checkFields();
                  if(!OkFlag)
                  {
                        JOptionPane.showMessageDialog(null,"All Fields must be filled");
                        boolean  EFlag =   theCompany.isEnabled();
                        if(!EFlag)
                        {
                              theTable.requestFocus();
                        }
                        else
                        {
                              theCompany.requestFocus();
                        }
                  }
                  else
                  {
                        BOkay.setEnabled(false);
                        saveData(maxslipno);
                        setVisible(false);
                  }
            }
      }

      private boolean checkFields()
      {
            boolean flag=true;

            int count = theModel.getRows();


            for(int i=0;i<count;i++)
            {

                 theTable.changeSelection(i,0,false,false);


                 System.out.println("Staff Code:"+theModel.getStaff(theTable.getSelectedRow()));
     
     
                 if(theModel.getCard(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getRep(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getStaff(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
                 if(theModel.getPurpose(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
				 if(theModel.getDept(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   
				 if(theModel.getUnit(theTable.getSelectedRow())=="")
                 {
                      flag=false;
                      return flag;
                 }   

            }
            return flag;
      }

      private void saveData(String maxslipno)
      {
            try
            {
                 maxslipno = visitorDomain.getNextSlipNo(1);
                 //theModel.saveData(maxslipno,getUnitCode(),getDeptCode());
				 theModel.saveData(maxslipno);
                 JOptionPane.showMessageDialog(null,"Details Saved,SlipNo:  "+maxslipno); 
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }

      private void refresh()
      {
            theModel.setNumRows(0);
            theModel.appendRow();
            theCompany.setEnabled(true);
            theCompany.setText("Company");
            setPresets();
            BOkay.setEnabled(true);
      }

      private void setPresets()
      {
          try
          {
               TSlipNo.setText("To Be Determined");              
               TDate.setTodayDate();
               TDate.setEditable(false);
               TTime.setText(getCurrentTime());
               

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      
      private String getCurrentTime()
      {
            return theTime.getTimeNow();
      }
	  private void setUnit()
      {
			vUnitCode = new Vector();
			vUnitName = new Vector();
			vDeptCode = new Vector();
			vDeptName = new Vector();			
			StringBuffer sbUnit = new StringBuffer();
			StringBuffer sbDept = new StringBuffer();
			java.sql.PreparedStatement pst = null;
		    java.sql.ResultSet   rst = null;
			sbUnit . append(" SELECT UnitCode,UnitName FROM Scm.Unit ORDER BY UnitName ");
			sbDept . append(" SELECT DEPTCODE,DEPTNAME FROM HrdNew.department ORDER BY DEPTNAME ");
		  try{
		  
		    	if(theConnection ==null){
                	JDBCConnection  jdbc = JDBCConnection.getJDBCConnection();
                	theConnection = jdbc . getConnection();
            	}
				pst = theConnection.prepareStatement(sbUnit.toString());
    	        rst = pst . executeQuery();

            	while(rst.next()) {
					vUnitCode .addElement(rst.getString(1));
					vUnitName .addElement(rst.getString(2));
            	}
	            rst.close();
				pst = theConnection.prepareStatement(sbDept.toString());
				rst = pst . executeQuery();
				while(rst.next()) {
					vDeptCode .addElement(rst.getString(1));
					vDeptName .addElement(rst.getString(2));
            	}
    	        pst.close();
				rst.close();
			}
			catch(Exception ex){
            ex.printStackTrace();
        }
      }
	private String getUnitCode(){
        return common.parseNull((String)vUnitCode.elementAt(JCUnitName.getSelectedIndex()));
   	}
   	private String getDeptCode(){
        return common.parseNull((String)vDeptCode.elementAt(JCDeptName.getSelectedIndex()));
   	}
}
