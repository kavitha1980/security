/*                             
      The Model for handling Visitor informations
      Name
      CardNo
      Purpose
      To Meet whom
*/
package client.errector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;
import java.sql.*;

import util.*;

import blf.*;

import java.rmi.*;
import java.rmi.registry.*;

public class ErrectorInModel extends DefaultTableModel implements rndi.CodedNames
{

      protected CompanyModel theCompany;


      // Column Ids
      int SLNO   = 0;
      int REP    = 1;
      int PURPOSE= 2;
      int STAFF  = 4;
      int CARDNO = 3;  
      int DEPT   = 5;
      int UNIT   = 6;



      ErRepModel     theRep;
      ErPurposeModel thePurpose;
      ErStaffModel   theStaff;
      TimeField    theTime;
      DateField1    theDate;
      ErDepartmentModel theDept;
	  ErUnitModel   theUnit;
      VisitorIn visitorDomain;

      Vector VRepCode,VPurposeCode,VStaffCode,VDeptCode,VUnitCode;

      String ColumnName[] = {"Sl No","Representative","Purpose","Card No","Staff","Dept","Unit"};

      String ColumnType[] = {  "S"  ,      "S"       ,   "S"   , "E" ,"S","S","S"};
     
      Common common = new Common();


      //-- The Constructor Method as referenced by the VisitorFrame


      public ErrectorInModel(CompanyModel theCompany)
      {
            this.theCompany = theCompany;

            VRepCode        = new Vector();
            VPurposeCode    = new Vector();
            VStaffCode      = new Vector();
            VDeptCode       = new Vector();
			VUnitCode       = new Vector();

            theRep          = new ErRepModel(this);
            thePurpose      = new ErPurposeModel(this);
            theStaff        = new ErStaffModel(this);
            theDept         = new ErDepartmentModel(this);  
			theUnit			= new ErUnitModel(this);

            theDate         = new DateField1();

            setDomain();
            setDataVector(getRowData(),ColumnName);

            removeRow(0);
            appendRow();
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

      }
      private Object[][] getRowData()
      {
            Object RowData[][] = new Object[1][ColumnName.length];
            for(int i=0;i<ColumnName.length;i++)
            {
                  RowData[0][i] = "";

            }
            return RowData;
      }

      //-- Overriding the methods of Default Table Model for additional features


      public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }


      public boolean isCellEditable(int row,int col)
      {
            if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
            return false;
      }

      boolean okflag=true;
      public void setValueAt(Object aValue, int row, int column)
      {
            try
            {
                  Vector rowVector = (Vector)super.dataVector.elementAt(row);
                  rowVector.setElementAt(aValue, column);

                  if (column==5)
                  {
                        try
                        {
                              String Scol4 = common.parseNull((String)rowVector.elementAt(4));
                              if(Scol4.equals(""))
                              {
                                   JOptionPane.showMessageDialog(null,"CardNo must be filled");
                              }
                        }
                        catch(Exception ne)
                        {
                              rowVector.set(3,"");
                        }
                        fireTableChanged(new TableModelEvent(this, row, row, column));
                  }
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      public int getRows()
      {

            return super.dataVector.size();
      }
      public void refresh()
      {
          setNumRows(0);
      }

      public void appendRow()
      {
            Vector vect = new Vector();
            for(int i=0;i<ColumnName.length;i++)                     
            {
                  vect.addElement("");

                  VRepCode.addElement("-1");
                  VPurposeCode.addElement("-1");
                  VStaffCode.addElement("-1");
                  VDeptCode.addElement("-1");
				  VUnitCode.addElement("-1");
            }               
            insertRow(getRows(),vect);
            setRowId();
      }

      public void deleteRow(int iRow)
      {
            if(getRows()==1)
                  return;
            removeRow(iRow);

            VRepCode.removeElementAt(iRow);
            VPurposeCode.removeElementAt(iRow);
            VStaffCode.removeElementAt(iRow);
            VDeptCode.removeElementAt(iRow);
			VUnitCode.removeElementAt(iRow);
            setRowId();
      }

      private void setRowId()
      {
            for(int i=0;i<getRows();i++)
            {
                  setValueAt(String.valueOf(i+1),i,SLNO);
            }
      }


      public void showHelpFrame(int iRow,int iCol)
      {
            if(iCol == REP)
            {
				System.out.println(" Before Rep ");
				theRep.showRepFrame(iRow,iCol);
            }
            if(iCol == PURPOSE)
            {
                  thePurpose.showPurposeFrame(iRow,iCol);
            }
            if(iCol == STAFF)
            {
                  theStaff.showStaffFrame(iRow,iCol);               
            }
            if(iCol == DEPT)
            {
                  theDept.showDeptFrame(iRow,iCol);
            }
			if(iCol == UNIT)
			{
				  theUnit.showUnitFrame(iRow,iCol);
			}
      }

      public String getCompanyCode()
      {
            return theCompany.getCode();
      }

    
      public void setRep(String SCode,String SName,int iRow)
      {
            try
            {
                  VRepCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,REP);
            }
            catch(Exception ex){}
      }

      public void setDept(String SCode,String SName,int iRow)
      {
            try
            {
                  VDeptCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,DEPT);
            }
            catch(Exception ex){}
      }
	  public void setUnit(String SCode,String SName,int iRow)
      {
            try
            {
                  VUnitCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,UNIT);
            }
            catch(Exception ex){}
      }

      public void setPurpose(String SCode,String SName,int iRow)
      {
            try
            {
                  VPurposeCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,PURPOSE);
            }
            catch(Exception ex){}
      }

      public void setStaff(String SCode,String SName,int iRow)
      {
            try
            {
                  VStaffCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,STAFF);
            }
            catch(Exception ex){}
      }


      public String getRep(int iRow)
      {
            return (String)VRepCode.elementAt(iRow);
      }

      public String getPurpose(int iRow)
      {
            return (String)VPurposeCode.elementAt(iRow);
      }

      public String getStaff(int iRow)
      {
            return (String)VStaffCode.elementAt(iRow);
      }

      public String getDept(int iRow)
      {
            return (String)VDeptCode.elementAt(iRow);
      }
	  public String getUnit(int iRow)
      {
            return (String)VUnitCode.elementAt(iRow);
      }
      public String getCard(int iRow)
      { 
            return ((String)getValueAt(iRow,CARDNO)).toUpperCase();
      }

      public boolean isRepInList(String SCode)
      {
            int index = VRepCode.indexOf(SCode);
            if(index == -1)
                  return false;
            return true;
      }

      public void saveData(String slipno)
      {
            try
            {
                  //int ccode = Integer.parseInt((String)theCompany.TCode.getText());
		  String ccode = (String)theCompany.TCode.getText();

                  theTime = new TimeField();
                  String theSTime =theTime.getTimeNow();

                  theDate.setTodayDate();
				                    
                       int vslipno = Integer.parseInt(slipno);
                    
                       for(int i=0;i<theRep.VSelectedRepCode.size();i++)
                       {
							int rpcode = Integer.parseInt((String)theRep.VSelectedRepCode.elementAt(i));
							int ppcode = Integer.parseInt((String)thePurpose.VSelectedPurposeCode.elementAt(i));
							int stcode = Integer.parseInt((String)theStaff.VSelectedStaffCode.elementAt(i));
							int unitcode = Integer.parseInt((String)theUnit.VSelectedUnitCode.elementAt(i));
							int deptcode = Integer.parseInt((String)theDept.VSelectedDeptCode.elementAt(i));
																					
							int slno    = visitorDomain.getSlNo("visitor_seq");
							Vector vect = new Vector();
							vect.addElement(String.valueOf(slno));
							vect.addElement(String.valueOf(vslipno));
							vect.addElement(ccode);
							vect.addElement(String.valueOf(rpcode));
							vect.addElement(String.valueOf(ppcode));
							vect.addElement(String.valueOf(stcode));
							vect.addElement(getCard(i));
							vect.addElement(theSTime);
							int iDate = common.toInt(common.pureDate(theDate.toNormal()));
							vect.addElement(String.valueOf(iDate));
							vect.addElement(String.valueOf(unitcode));
							vect.addElement(String.valueOf(deptcode));
							
							visitorDomain.saveErrectorData(slipno,vect);                            
                       }                                            
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
                  ex.printStackTrace();
            }
      }
      
      public boolean checkDate(String Date)
      {
            boolean flag=false;
            /*try
            {

                  Vector VDate      = visitorDomain.getFinanceYear();
                  int stDate        = Integer.parseInt((String)VDate.elementAt(0));
                  int eDate         = Integer.parseInt((String)VDate.elementAt(1));

                  int idate         = common.toInt(common.pureDate(Date));


                  if( (idate >=stDate ) && (idate<=eDate) )
                        flag =  true;
                  
            }
            catch(Exception e)
            {
                  e.printStackTrace();
            }*/
            return flag;
      }

}


                          


