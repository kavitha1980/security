/*


*/
package client.errector;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import util.Common;

public class ErDepartmentModel implements rndi.CodedNames 
{

      protected ErrectorInModel theModel;
      protected int iRow,iCol;
      JTextField     TName,TCode;
      Vector         VCode,VName;
      VisitorIn visitorDomain;
      Common common = new Common();

      Vector VSelectedDeptCode  = new Vector();

      public ErDepartmentModel(ErrectorInModel theModel)
      {
          this.theModel = theModel;
          setDomain();
          TName = new JTextField();
          TCode = new JTextField();
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      public void showDeptFrame(int iRow,int iCol)
      {
            this.iRow = iRow;
            this.iCol = iCol;

            setDataIntoVector();
            //TCode.setText(theModel.getStaff(iRow));

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Staff Selector",true);
            ErDeptSearchPanel PSP = new ErDeptSearchPanel(this,TCode,TName,VCode,VName,theDialog);

            theDialog.getContentPane().add(PSP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }

      public void setDetails()
      {
            String SCode = TCode.getText();
            int index    = VCode.indexOf(SCode);
            if(index == -1)
                  setData();
            else
                  TCode.setText(SCode);

            theModel.setDept(TCode.getText(),TName.getText(),iRow);
            VSelectedDeptCode.addElement(TCode.getText());
      }

      private void setData()
      {
            int iStaffCode = 0;
            try
            {
                        String SStaffCode = visitorDomain.getMaxStaffCode();
                        iStaffCode  = Integer.parseInt(SStaffCode)+1;
                        SStaffCode  = Integer.toString(iStaffCode);
                        TCode.setText(SStaffCode);
	                String name = (String) TName.getText();
			setDataIntoVector();
            }
            catch(Exception ex)
            {
               System.out.println("ErDepartmentModel : "+ex);
            }
      }

      private void setDataIntoVector()
      {
            VCode        = new Vector();
            VName        = new Vector();
            Vector VTemp = new Vector();

            String QString = " SELECT DEPTCODE,DEPTNAME FROM department ORDER BY DEPTNAME ";

            try
            {
		Class.forName("oracle.jdbc.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","HrdNew","HrdNew");
		Statement stat   = conn.createStatement();
		ResultSet res = stat.executeQuery(QString);

                  while(res.next())
                  {
                        VCode.addElement(res.getString(1));
                        VName.addElement(res.getString(2));
                  }
		  conn.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }
}
