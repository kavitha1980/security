/*                  
      The Model for handling Visitor Outward information

      Out-time
      Card Number
*/
package client.errector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;
import java.sql.*;

import blf.*;
import java.rmi.*;
import java.rmi.registry.*;

import util.*;

public class ErrectorOutModel extends DefaultTableModel implements rndi.CodedNames
{

     // Column Ids

      int          SLNO   = 0;
      int          REP    = 1;
      int          PURPOSE= 2;
      //int          STAFF  = 3;
      int          CARDNO = 3;  
    
      TimeField    theTime;
      DateField    theDate;
      JTextField   TSlipNo;
     
      String          CardNo="";
      VisitorIn visitorDomain;

      Vector       VRepCode,VPurposeCode,VStaffCode;
      Vector       vRepCode,vRepName,vPurpose,vStaff,vCardNo;

      String ColumnName[] = {"Sl No","Representative","Purpose","Card No", "Selection"};

      String ColumnType[] = {  "S"  ,       "S"      ,    "S"  ,   "S"   ,     "B"    };
     
      Common common = new Common();
 

      //-- The Constructor Method as referenced by the VisitorOutwardFrame


      public ErrectorOutModel()
      {

            theDate         = new DateField();

            VRepCode        = new Vector();
            VPurposeCode    = new Vector();
            VStaffCode      = new Vector();
          
            setDomain();
            setDataVector(getRowData(),ColumnName);

            removeRow(0);
            appendRow();
      }
      private void setDomain()
      {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
      private Object[][] getRowData()
      {
            Object RowData[][] = new Object[1][ColumnName.length];
            for(int i=0;i<ColumnName.length;i++)
            {
                  RowData[0][i] = "";

            }
            return RowData;
      }


      //-- Overriding the methods of Default Table Model for additional features



      public Class getColumnClass(int col){ return getValueAt(0,col).getClass(); }


      public boolean isCellEditable(int row,int col)
      {
            if(ColumnType[col]=="B" || ColumnType[col]=="E")
                  return true;
            return false;
      }

      public void setCardNo(String sCardNo,Vector vRepCode,Vector vRepName,Vector vPurpose,Vector vStaff,Vector vCardNo)
      {
            this.CardNo = sCardNo;
            this.vRepCode =  vRepCode;
            this.vRepName =  vRepName;
            this.vPurpose =  vPurpose;
            //this.vStaff   =  vStaff; 
            this.vCardNo  =  vCardNo;
	    System.out.println("CardNo-->"+CardNo);
	    System.out.println("vRepCode-->"+vRepCode);
	    System.out.println("vRepName-->"+vRepName);
	    System.out.println("vPurpose-->"+vPurpose);
	    System.out.println("vCardNo-->"+vCardNo);

            for(int i=0;i<vRepName.size();i++)
            {

                  setValueAt(vRepName.elementAt(i),i,1);
                  setValueAt(vPurpose.elementAt(i),i,2);
                  //setValueAt(vStaff.elementAt(i),i,3);
                  setValueAt(vCardNo.elementAt(i),i,3);
                  appendRow();
                       

            }
            
      }

      public void setValueAt(Object aValue, int row, int column)
      {
            try
            {

                  Vector rowVector = (Vector)super.dataVector.elementAt(row);
                  rowVector.setElementAt(aValue, column);
                  fireTableChanged(new TableModelEvent(this, row, row, column));
            }
            catch(Exception ex)
            {
                  System.out.println(ex+ "132");
            }
      }
     
      public int getRows()
      {
            return super.dataVector.size();
      }

      public void appendRow()
      {
            Vector vect = new Vector();
            for(int i=0;i<ColumnName.length;i++)                     
            {
                  vect.addElement("");

                  VRepCode.addElement("-1");
                  VPurposeCode.addElement("-1");
                  //VStaffCode.addElement("-1");
            }               
            insertRow(getRows(),vect);
            setRowId();
      }

      public void deleteRow(int iRow)
      {
            if(getRows()==1)
                  return;
            removeRow(iRow);
            setRowId();
      }

      private void setRowId()
      {
            for(int i=0;i<getRows();i++)
            {
                  setValueAt(String.valueOf(i+1),i,SLNO);
                  setValueAt(new Boolean(false),i,4);
            }
      }

      public void setRep(String SCode,String SName,int iRow)
      {
            try
            {
                  VRepCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,REP);
            }
            catch(Exception ex){}
      }

      public void setPurpose(String SCode,String SName,int iRow)
      {
            try
            {
                  VPurposeCode.setElementAt(SCode,iRow);
                  setValueAt(SName,iRow,PURPOSE);
            }
            catch(Exception ex){}
      }

      public void setStaff(String SCode,String SName,int iRow)
      {
            try
            {
                  VStaffCode.setElementAt(SCode,iRow);
                  //setValueAt(SName,iRow,STAFF);
            }
            catch(Exception ex){}
      }

      public String getRep(int iRow)
      {

            return (String)VRepCode.elementAt(iRow);
      }

      public String getPurpose(int iRow)
      {

            return (String)VPurposeCode.elementAt(iRow);
      }

      public String getStaff(int iRow)
      {

            return (String)VStaffCode.elementAt(iRow);
      }

      public String getCard(int iRow)
      { 

          return (String)getValueAt(iRow,CARDNO);
      }

      public Boolean getSelection(int iRow)
      { 

          return (Boolean)getValueAt(iRow,4);
      }


      public void saveData()
      {
            try
            {                   
                  //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                  //Connection conn  = DriverManager.getConnection(common.getDSN(),"","");
                  //Statement stat   = conn.createStatement();

                  theTime = new TimeField();
                  String theSTime =theTime.getTimeNow();

                  String Srepcode="";
                  int iRepCode=0;
                  for(int i=0;i<vRepCode.size();i++)
                  {
                        if (getSelection(i).booleanValue())
                        {
                              Srepcode  = (String)vRepCode.elementAt(i);
                              iRepCode  = Integer.parseInt(Srepcode);
                              visitorDomain.setErrectorOutTime(theSTime,iRepCode);

                              //stat.execute("update visitor set outtime='"+theSTime+"',Out=yes where visitor.repcode = "+Integer.parseInt((String)vRepCode.elementAt(i))+" and Visitor.Out= no");
                         }
                  }
                  //conn.close();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }

      }

}
                     


