/*

     The Representative Object is meant for
     allowing the User to select a Representative
     belongs to a particular Company.

     If the Representative in ? is not available a provision
     to insert On line the new Representative.

*/
package client.errector;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.util.Vector;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import util.Common;

public class ErRepModel  implements rndi.CodedNames
{

      protected ErrectorInModel theModel;
      protected   int          iRow,iCol;

      JTextField   TName,TCode;
      Vector       VCode,VName;
      Vector       VSelectedRepCode;

      Common      common = new Common();
      VisitorIn visitorDomain;
     

      public ErRepModel(ErrectorInModel theModel)
      {
            this.theModel      = theModel;
            setDomain();
            TName              = new JTextField();
            TCode              = new JTextField();
            VSelectedRepCode   = new Vector();
      }
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               visitorDomain       = (VisitorIn) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void showRepFrame(int iRow,int iCol)
     {
            this.iRow          = iRow;
            this.iCol          = iCol;
	    System.out.println(" In Rep Model ");
            setDataIntoVector();
	    System.out.println(" In Rep Model Afet setDataIntoVector ");
            TCode.setText(theModel.getRep(iRow));

            Frame dummy        = new Frame();
            JDialog theDialog  = new JDialog(dummy,"Representative Selector",true); 
            ErRepSearchPanel RSP = new ErRepSearchPanel(this,TCode,TName,VCode,VName,theDialog);

            theDialog.getContentPane().add(RSP);
            theDialog.setBounds(190,90,400,350);
            theDialog.setVisible(true);
      }
          
      public void setDetails()
      {

            if(theModel.isRepInList(TCode.getText()))
                  return;

            String SCode = TCode.getText();

            int index    = VCode.indexOf(SCode);

            if(index == -1)
            {				
                  setData();    // 27-02-2016
	    }
            else
	    {
            	TCode.setText(SCode);
            	theModel.setRep(TCode.getText(),TName.getText(),iRow);
            	VSelectedRepCode.addElement((String)TCode.getText());
	    }
      }

      private void setData()
      {           
            try
            {
                  //visitorDomain.insertRep((String)TName.getText(),theModel.getCompanyCode());

                  //TCode.setText((String)visitorDomain.getMaxRepCode());

                  setDataIntoVector();
            }
            catch(Exception ex)
            {
                  System.out.println(ex);
            }
      }

      private void setDataIntoVector()
      {
            VCode        = new Vector();
            VName        = new Vector();             
            Vector VTemp = new Vector();

            //String QString = " Select Code,Name From Representative  "+
            //      " Where CompanyCode="+ Integer.parseInt(theModel.getCompanyCode())+
            //      " Order By 2";
            try
            {
                  //VTemp  = visitorDomain.getRepInfo(Integer.parseInt(theModel.getCompanyCode()));
				  VTemp  = visitorDomain.getRepInfo(theModel.getCompanyCode());
                  int m=0;
                  int size=(VTemp.size())/2;
                  for(int i=0;i<size;i++)
                  {
                        VCode.addElement(VTemp.elementAt(0+m));
                        VName.addElement(VTemp.elementAt(1+m));
                        m=m+2; 
                  }
            }
            catch(Exception ex)
            {
                  ex.printStackTrace();
				  //System.out.println(" Error In set Data : "+ex);
            }
      }

}
