package client.vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import util.*;
import domain.jdbc.*;
import java.sql.*;

public class RepresentativeFrame extends JInternalFrame
{
     // Global Declarations...
	 
		java.sql.Connection theConnection = null;
		protected      JLayeredPane                       Layer;

		private        JPanel                             MiddlePanel, BottomPanel;

		private        JTextField                         TRepName, TNw2;
		private        FractionNumberField                TRate, TVATPer;

		private        JButton                            BSave, BExit;
		private        JComboBox                          JCPartyName; 

		private        Common                             common;

		private 	    Vector 							   VPartyName,VPartyCode;

     // create Construtor..

     public RepresentativeFrame(JLayeredPane Layer)
     {
					this	.	Layer   	= Layer;
          try
          {
               common                   = null;
               common                   = new Common();

               setWheelType();
               createComponents();
               setLayouts();
               addComponents();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // Create Components, set Layouts and Add Components..

     private void createComponents()
     {
				MiddlePanel	=	new JPanel();
				BottomPanel	=	new JPanel();

				TRepName	=	new JTextField();

				JCPartyName	=	new JComboBox(VPartyName);

				BSave	=	new JButton("Save");
				BExit	=	new JButton("Exit");

				// Add Listeners..

				BSave. addActionListener(new ActList());
				BExit. addActionListener(new ActList());
     }

     private void setLayouts()
     {

          setTitle("Representative Entry");
          setBounds(5, 10, 530, 280);
          setResizable(true);
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);

          MiddlePanel                   . setLayout(new GridLayout(4,2,5,5));
          BottomPanel                   . setLayout(new FlowLayout());

          MiddlePanel                   . setBorder(new TitledBorder("Company Name"));
          BottomPanel                   . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {

				MiddlePanel              . add(new JLabel("Select Company Name"));
				MiddlePanel              . add(JCPartyName);

				MiddlePanel              . add(new JLabel("Representative Name"));
				MiddlePanel              . add(TRepName);
								                             
				BottomPanel              . add(BSave);
				BottomPanel              . add(BExit);

				getContentPane()         . add("Center", MiddlePanel);
				getContentPane()         . add("South", BottomPanel);
     }

     // Listener Classes...

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BSave)
               {
									if(validations())
									{
										 if(JOptionPane.showConfirmDialog(null, "Are you Sure you want to Save the Data?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
										 {
												int iSaved =   saveDetails();
												if(iSaved == 1){
												JOptionPane.showMessageDialog(null, "Data Saved.", "Information!", JOptionPane.INFORMATION_MESSAGE);
												refreshFields();
												setWheelType();
												}
												else
												{
													JOptionPane.showMessageDialog(null, "Not Saved", "Information", JOptionPane.ERROR_MESSAGE);
												}
    	              }
                 }
               }

               if(ae.getSource() == BExit)
               {
                    removeHelpFrame();
               }
          }
     }

     // Validations..

     private boolean validations()
     {
				String SError       = "";
				int iSlNo           = 1;

				String SRep="";

				SRep   = common.parseNull(TRepName.getText()).trim();
				if(SRep.length() <= 0)
				{
					 SError   += (iSlNo++)+").Rep Name should be filled.\n";
				}
				if(SError.length() > 0)
				{
					 JOptionPane.showMessageDialog(null, SError, "Information", JOptionPane.ERROR_MESSAGE);
					 return false;
				}
				return true;
     }

     // General Methods..

     private void removeHelpFrame()
     {
          try
          {
               this.dispose();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private void refreshFields()
     {
          TRepName  . setText("");
     }
     private void setWheelType()
     {
			VPartyName = new Vector();
			VPartyCode = new Vector();
			StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT PARTYCODE,PARTYNAME FROM SCM.PARTYMASTER ORDER BY PARTYNAME ");
		  try{
		  
		    	if(theConnection ==null){
                	JDBCConnection  jdbc = JDBCConnection.getJDBCConnection();
                	theConnection = jdbc . getConnection();
            	}
							java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
    	        java.sql.ResultSet   rst = pst . executeQuery();

            	while(rst.next()) {
								VPartyCode .addElement(rst.getString(1));
								VPartyName .addElement(rst.getString(2));
            	}
	            rst.close();
    	        pst.close();
			}
			catch(Exception ex){
            ex.printStackTrace();
        }
     }

     private int saveDetails()
     {
		  int iSaved = 1;
			StringBuffer sb  = new StringBuffer();
			sb.append(" INSERT INTO Representative (CODE,NAME,COMPANYCODE,DISPLAYSTATUS,ENTRYDATE) ");
			sb.append(" VALUES(Rep_seq.nextVal,?,?,?,to_Char(sysdate,'DD-MM-YYYY:hh24:mi:ss')) ");
			String SCode = getPartyCode();
			String SRepName  = common.parseNull(TRepName.getText()).trim();
		  try
			{
					if(theConnection ==null){
						JDBCConnection  jdbc = JDBCConnection.getJDBCConnection();
						theConnection = jdbc . getConnection();
					}
					java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
					pst . setString(1,SRepName);
					pst . setString(2,SCode);
					pst . setString(3,"1");
					pst . executeUpdate();
					pst.close();
					pst = null;
			}
			catch(Exception ex){
				ex.printStackTrace();
				iSaved = 2;
			}
		 return iSaved;
     }
	 private String getPartyCode(){
        return common.parseNull((String)VPartyCode.elementAt(JCPartyName.getSelectedIndex()));
   }
}
