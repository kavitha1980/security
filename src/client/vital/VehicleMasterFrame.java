package client.vital;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.awt.event.*;
import domain.jdbc.*;
import java.util.*;
import javax.swing.border.*;
import util.*;
public class VehicleMasterFrame extends JInternalFrame
{
     JPanel TopRightPanel,TopLeftPanel,BottomPanel,TopPanel;
     JTextField     TCardNo,TName,TRegNo,TStKm,TStdKm,TBudKm;
     JButton        BSave,BModify,BExit;
     JLayeredPane   Layer;
     JList LVehicles;
     Connection     theConnection;
     Vector VName,VRegNo,VStKm,VStdKm,VBudKm,VCardNo,VId;
     Common common  = new Common();
     String SMsg;
     int iIndex=-1;
     public VehicleMasterFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVectors();
     }
     private void createComponents()
     {
          TopPanel            =    new JPanel();
          TopLeftPanel        =    new JPanel();
          TopRightPanel       =    new JPanel();
          BottomPanel         =    new JPanel();

          LVehicles           =    new JList();

          TCardNo             =    new JTextField();
          TName               =    new JTextField();
          TRegNo              =    new JTextField();
          TStKm               =    new JTextField();
          TStdKm              =    new JTextField();
          TBudKm              =    new JTextField();


          BSave               =    new JButton("Save");
          BExit               =    new JButton("Exit");
          BModify             =    new JButton("Modify");
     }
     private void setLayouts()
     {
          TopPanel.setLayout(new GridLayout(1,2));
          TopPanel.setBorder(new TitledBorder("Info"));
          TopLeftPanel.setLayout(new GridLayout(6,2));
          TopLeftPanel.setBorder(new TitledBorder("Vehicle Information"));
          TopRightPanel.setLayout(new BorderLayout());
          TopRightPanel.setBorder(new TitledBorder("Available Vehicles"));
          BottomPanel.setBorder(new TitledBorder("Control"));
     }
     private void addComponents()
     {
          setTitle("Vehicle Information");
          setBounds(0,0,400,500);
          setClosable(true);
          setMaximizable(true);

          TopLeftPanel.add(new JLabel("Vehicle Name "));
          TopLeftPanel.add(TName);
          TopLeftPanel.add(new JLabel("Reg No "));
          TopLeftPanel.add(TRegNo);
          TopLeftPanel.add(new JLabel("Card No"));
          TopLeftPanel.add(TCardNo);

          TopLeftPanel.add(new JLabel("StandardKm"));
          TopLeftPanel.add(TStdKm);
          TopLeftPanel.add(new JLabel("Budget Km"));
          TopLeftPanel.add(TBudKm);
          TopLeftPanel.add(new JLabel("Starting Km"));
          TopLeftPanel.add(TStKm);

          TopRightPanel.add(new JScrollPane(LVehicles));
          BottomPanel.add(BSave);
          BottomPanel.add(BModify);
          BottomPanel.add(BExit);

          BModify.setEnabled(false);

          TopPanel   .add(TopLeftPanel);
          TopPanel   .add(TopRightPanel);

          getContentPane().add("North",TopPanel);
          getContentPane().add("South",BottomPanel);

     }
     private void addListeners()
     {
          BSave.addActionListener(new OnSave());
          BModify.addActionListener(new OnModify());
          LVehicles.addMouseListener(new OnClick());
          BExit.addActionListener(new OnExit());
     }
     private void setVectors()
     {
          VId            = new Vector();
          VName          = new Vector();
          VRegNo         = new Vector();
          VStKm          = new Vector();
          VStdKm         = new Vector();
          VBudKm         = new Vector();
          VCardNo        = new Vector();

          String QS      = " Select 0 as Id,CardNo,VehicleRegNo,VehicleName,StdKmPl,"+
                           " BudgetPerKm,StartKm from VehicleInfo ";
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VId.addElement(theResult.getString(1));
                    VCardNo.addElement(theResult.getString(2));
                    VRegNo.addElement(theResult.getString(3));
                    VName.addElement(theResult.getString(4));
                    VStdKm.addElement(theResult.getString(5));
                    VBudKm.addElement(theResult.getString(6));
                    VStKm.addElement(theResult.getString(7));
               }
               theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          LVehicles.setListData(VName);
     }
     private class OnSave implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(checkData())
               {
                    if(JOptionPane.showConfirmDialog(null, "Confirm Save the Data?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
                    {
                         insertData();
                    }
               }
          }
     }

     private class OnModify implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               {
                    updateData();     
               }
          }
     }
     private class OnClick extends MouseAdapter
     {
          public void mouseClicked(MouseEvent me)
          {
               if(me.getClickCount()==2)
               {
                    iIndex    = LVehicles.getSelectedIndex();
                    BModify.setEnabled(true);
                    setData();
               }
          }
     }
     private class OnExit implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    setVisible(false);
               }
               catch(Exception ex){}
               }
     }
     private boolean checkData()
     {
          int iflag      = 0;
          String SRegNo  = common.parseNull((String)TRegNo.getText());
          String SCardNo = common.parseNull((String)TCardNo.getText());
          for(int i=0;i<VRegNo.size();i++)
          {
               String STempRegNo     = common.parseNull((String)VRegNo.elementAt(i));
               String STempCardNo    = common.parseNull((String)VCardNo.elementAt(i));
               if(STempRegNo.equals(SRegNo))
               {
                    iflag = 1;
                    SMsg = " This Vehicle AlreadyExists";
                    break;
               }
               if(STempCardNo.equals(SCardNo))
               {
                    iflag=1;
                    SMsg = " A Vehicle Already Using This Card " ;
                    break;
               }
          }

          if(SRegNo.indexOf(" ") != -1)
          {
               JOptionPane.showMessageDialog(null, "Spaces are not allowed in Truck No.", "Error", JOptionPane.ERROR_MESSAGE,null); 
               return false;
          }

          if(iflag==1)
          {
               JOptionPane.showMessageDialog(null,"Error:"+SMsg,"Error ",JOptionPane.ERROR_MESSAGE,null); 
               return false;
          }
          else
               return true;
     }
     private void insertData()
     {
          int iID        = 0;        
          String SRegNo  = common.parseNull((String)TRegNo.getText()).toUpperCase();
          String SName   = common.parseNull((String)TName.getText()).toUpperCase();
          String SCardNo = common.parseNull((String)TCardNo.getText()).toUpperCase();
          double dStdKm  = common.toDouble(common.parseNull((String)TStdKm.getText()));
          double dBudKm  = common.toDouble(common.parseNull((String)TBudKm.getText()));
          int iStKm      = common.toInt((String)TStKm.getText());

          String QS1     = " Select Max(ID)+1 from VehicleInfo ";
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    iID                 = common.toInt((String)theResult.getString(1));
               }
               theStatement.close();

               String QS      = " Insert into VehicleInfo(Id,CardNo,VehicleRegNo,VehicleName, "+
                                " StdKmpl,BudgetPerKm,StartKm) values ( "+
                                " "+iID+",'"+SCardNo+"','"+SRegNo+"','"+SName+"',"+dStdKm+", "+
                                " "+dBudKm+","+iStKm+" ) " ;

               Statement theStatement1  = theConnection.createStatement();
               theStatement1.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          setVectors();
          setDefaults();
     }
     private void updateData()
     {
          String SRegNo  = common.parseNull((String)TRegNo.getText()).toUpperCase();
          String SName   = common.parseNull((String)TName.getText()).toUpperCase();
          String SCardNo = common.parseNull((String)TCardNo.getText()).toUpperCase();
          double dStdKm  = common.toDouble(common.parseNull((String)TStdKm.getText()));
          double dBudKm  = common.toDouble(common.parseNull((String)TBudKm.getText()));
          int iStKm      = common.toInt((String)TStKm.getText());
          int iId        = common.toInt((String)VId.elementAt(iIndex));

          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               String QS      = " update  VehicleInfo set CardNo = '"+SCardNo+"', "+
                                "          VehicleRegNo = '"+SRegNo+"' , "+
                                "          VehicleName  = '"+SName+"'  , "+
                                "          StdKmpl      = "+dStdKm+"   , "+
                                "          BudgetPerKm  = "+dBudKm+"   , "+
                                "          StartKm      = "+iStKm+"    "+
                                "          where Id     = "+iId+"       ";

               Statement theStatement1  = theConnection.createStatement();
               theStatement1.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          setVectors();
          setDefaults();
     }
     private void setData()
     {
          String SName="",SRegNo="",SCardNo="",SStdKmpl="",SBudKm="",SStartKm="";
          int iId        = common.toInt((String)VId.elementAt(iIndex));
          String QS1     = " Select CardNo,VehicleRegNo,VehicleName,StdKmPl,"+
                           " BudgetPerKm,StartKm "+   
                           " from VehicleInfo where Id = "+iId+" ";

          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    SCardNo             = (String)theResult.getString(1);
                    SRegNo              = (String)theResult.getString(2);
                    SName               = (String)theResult.getString(3);
                    SStdKmpl            = (String)theResult.getString(4);
                    SBudKm              = (String)theResult.getString(5);
                    SStartKm            = (String)theResult.getString(6);
               }
               theStatement.close();
               TCardNo.setText(SCardNo);
               TRegNo.setText(SRegNo);
               TName.setText(SName);
               TStdKm.setText(SStdKmpl);
               TBudKm.setText(SBudKm);
               TStKm.setText(SStartKm);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     

     private void setDefaults()
     {
          TRegNo.setText("");
          TName.setText("");
          TCardNo.setText("");
          TStKm.setText("");
          TStdKm.setText("");
          TBudKm.setText("");
          BModify.setEnabled(false);
     }
}


     

