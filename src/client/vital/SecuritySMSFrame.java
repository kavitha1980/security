package client.vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import util.*;
import domain.jdbc.*;
import java.sql.*;

public class SecuritySMSFrame extends JInternalFrame
{
     // Global Declarations...
	 
		java.sql.Connection theConnection = null;
		protected      JLayeredPane    Layer;

		private        JPanel       MiddlePanel, BottomPanel;


		private        JButton     BSave, BExit;
		private        JComboBox   JCSMS; 

		private        Common      common;
                 MyLabel       LLastDateTime;
		private         Vector  	   VSms,VId,VLastDateTime ;
                java.util.List AUserCodeList = new java.util.ArrayList();
              
     // create Construtor..

     public SecuritySMSFrame(JLayeredPane Layer)
     {
					this	.	Layer   	= Layer;
          try
          {
               common                   = null;
               common                   = new Common();

               setusercode();
               setRawusercode();
               createComponents();
               setLayouts();
               addComponents();
               setLabel(); 

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     // Create Components, set Layouts and Add Components..

     private void createComponents()
     {
		MiddlePanel	=	new JPanel();
		BottomPanel	=	new JPanel();
                JCSMS	        =       new JComboBox(VSms);
                LLastDateTime   =       new MyLabel(""); 
              
    
		BSave	=	new JButton("Send SMS");
		BExit	=	new JButton("Exit");

		// Add Listeners..
                JCSMS        . addItemListener(new ItemList());
		BSave. addActionListener(new ActList());
		BExit. addActionListener(new ActList());
     }

     private void setLayouts()
     {

          setTitle("Security SMS Frame");
          setBounds(5, 10, 530, 280);
          setResizable(true);
          setClosable(true);
          setIconifiable(true);
          setMaximizable(true);

          MiddlePanel                   . setLayout(new GridLayout(4,2,5,5));
          BottomPanel                   . setLayout(new FlowLayout());

          MiddlePanel                   . setBorder(new TitledBorder("Security SMS Frame"));
          BottomPanel                   . setBorder(new TitledBorder("Controls"));
     }

     private void addComponents()
     {

		MiddlePanel              . add(new JLabel("Select SMS	: "));
		MiddlePanel              . add(JCSMS);

		
                MiddlePanel              . add(new JLabel("Last Sms Sending Date & Time :"));
                MiddlePanel              . add(LLastDateTime);
						                             
		BottomPanel              . add(BSave);
		BottomPanel              . add(BExit);

		getContentPane()         . add("Center", MiddlePanel);
		getContentPane()         . add("South", BottomPanel);
     }

     // Listener Classes...

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource() == BSave)
               {
			
		 if(JOptionPane.showConfirmDialog(null, "Are you Sure you want to send  the SMS?", "Confirm?", JOptionPane.YES_NO_OPTION) == 0)
		 {
				int iSaved =   saveDetails();
				if(iSaved == 1){
				JOptionPane.showMessageDialog(null, "SMS has been send sucessfully...", "Information!", JOptionPane.INFORMATION_MESSAGE);
			 
				setusercode();
                                setLabel();
                                //String slasttime  = LLastDateTime . getText() . trim(); 
                               // UpdateLastDateTime(slasttime);
                               
				}
				else
				{
					JOptionPane.showMessageDialog(null, "SMS has not send", "Information", JOptionPane.ERROR_MESSAGE);
				}

                 }
               }

               if(ae.getSource() == BExit)
               {
                    removeHelpFrame();
               }
          }

  }
     
  private class ItemList implements ItemListener

     {
 
	 public void itemStateChanged(ItemEvent ie)
	  {
		       if(ie.getSource() == JCSMS)
		       {
		            

		            setLabel();

		       }
		  
	     
	  }

   }

   
     // General Methods..

     private void removeHelpFrame()
     {
          try
          {
               this.dispose();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

  
     private void setusercode()
     {
	
		VId = new Vector();
		VSms = new Vector();
                VLastDateTime = new Vector();

		StringBuffer sb = new StringBuffer();
	        sb . append(" Select Id, Sms, to_Char(SentDateTime, 'DD.MM.YYYY HH24:MI:SS') as SentDateTime  from SecuritySmsMaster  ");
	  try{
		  
	    	if(theConnection ==null)
                 {
        	JDBCConnection2  jdbc = JDBCConnection2.getJDBCConnection2();
        	theConnection = jdbc . getConnection();
            	}
		java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
    	        java.sql.ResultSet   rst = pst . executeQuery();

            	while(rst.next())
                {
			VId  .addElement(rst.getString(1));
                        VSms .addElement(rst.getString(2));
                        VLastDateTime.addElement(rst.getString(3));
			
            	}
	        rst.close();
    	        pst.close();
	   }catch(Exception ex){
            ex.printStackTrace();
        }
     }

     private int saveDetails()
     {
		        int iSaved = 1;
			StringBuffer sb  = new StringBuffer();
					sb.append(" Insert into TBL_ALERT_COLLECTION  ");
					sb.append(" ( ");
					sb.append(" COLLECTIONID, STATUSID, ALERTMESSAGE, CUSTOMERCODE, STATUS  ");
					sb.append(" )  ");
					sb.append(" Values  ");
					sb.append(" (  ");
					sb.append(" AlertCollection_Seq.nextval, 'STATUS283',?, ?, 0 ");
					sb.append(" ) ");

			
			String SSMSText = (String)JCSMS.getSelectedItem() ;

                        String QS2 = "  Update SecuritySmsMaster set SentDateTime = SysDate where Id = ? ";
			String SCode = common.parseNull((String)VId.elementAt(JCSMS.getSelectedIndex())); 

		  try
			{
					if(theConnection ==null)
                                        {
						JDBCConnection2  jdbc = JDBCConnection2.getJDBCConnection2();
						theConnection = jdbc . getConnection();
					}
					java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
                                        for(int i=0;i<AUserCodeList.size();i++)
                                        {
                                           HashMap theMap = (HashMap) AUserCodeList.get(i);
                                           int  iUserCode =common.toInt((String) theMap.get("USERCODE"));

					   pst . setString(1,SSMSText);
					   pst . setInt(2,iUserCode);    
                			   pst . executeUpdate();
                                        }
					
					
 
                                        pst = theConnection.prepareStatement(QS2 );
                                        pst . setString(1,SCode);
 					pst . executeUpdate();
                                        pst = null;
			}
			catch(Exception ex){
				ex.printStackTrace();
				
			}
		 return iSaved;
     }

  private void setLabel()

     {

                   String slasttime = common.parseNull((String)JCSMS.getSelectedItem());
                   String slastdatetime = common.parseNull((String)(VLastDateTime.elementAt(VSms.indexOf(slasttime))));
                   LLastDateTime      . setText(slastdatetime);
                  
                   

     }
	

     public void setRawusercode()
     {
	   AUserCodeList = new ArrayList();
           AUserCodeList.clear();
		
		StringBuffer sb = new StringBuffer();
	        sb . append(" Select UserCode from RawUser where SecuritySMSStatus = 1 ");
	  try{
		  
	    	if(theConnection ==null)
                 {
        	JDBCConnection2  jdbc = JDBCConnection2.getJDBCConnection2();
        	theConnection = jdbc . getConnection();
            	}
		java.sql.PreparedStatement pst = theConnection.prepareStatement(sb.toString());
    	        java.sql.ResultSet   rst = pst . executeQuery();
                ResultSetMetaData rsmd = rst.getMetaData();
            	while(rst.next())
                {
		 HashMap themap = new HashMap();
                 for (int i = 0; i < rsmd.getColumnCount(); i++)
                 {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                 }
		       
                AUserCodeList.add(themap);
            	}
	        rst.close();
    	        pst.close();
	   }catch(Exception ex){
            ex.printStackTrace();
        }
     }
 /*private void UpdateLastDateTime(String slasttime)
    {    
            String UpdateDataQS;   
              try
            
             {    
             UpdateDataQS  = " Update SecuritySmsMaster set SentDateTime = SysDate where Id = ?  ";
            
             String SCode = common.parseNull((String)VId.elementAt(JCSMS.getSelectedIndex()));
             System.out.println("SCode -------------->"+SCode);

			
			if(theConnection ==null)
                                        {
						JDBCConnection2  jdbc = JDBCConnection2.getJDBCConnection2();
						theConnection = jdbc . getConnection();
					}
					java.sql.PreparedStatement pst = theConnection.prepareStatement(UpdateDataQS);
                                       
                                          

					   pst . setString(1,SCode);
					   
                			   pst . executeUpdate();
                                        
					pst.close();
					pst = null;
			}
			catch(Exception ex){
				ex.printStackTrace();
				
			}
   } */

 
}
