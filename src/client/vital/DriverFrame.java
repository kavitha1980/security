package client.vital;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import util.*;
import blf.*;
import domain.jdbc.*;

public class DriverFrame extends JInternalFrame implements rndi.CodedNames
{
     protected  JLayeredPane Layer;
     protected  String       tableName="";
     protected  String       fieldName="";
     protected  String       fieldCode="";

     JPanel         LeftPanel,RightTopPanel,RightPanel;
     JList          subList;
     JButton        BAdd,BModify,BClose;
     NameField      TName;
     Vector         VName,VCode;
     Common         common = new Common();
     String         SCode;

     //public ManagementFrame(JLayeredPane Layer,String tableName,String fieldCode,String fieldName)
     public DriverFrame(JLayeredPane Layer)
     {
          this.Layer     = Layer;
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setVectors();
     }

     private void createComponents()
     {
          LeftPanel         = new JPanel(true);
          RightTopPanel     = new JPanel(true);
          RightPanel        = new JPanel(true);
          TName             = new NameField();
          BAdd              = new JButton("Add");
          BModify           = new JButton("Modify");
          BClose            = new JButton("Close");
          subList           = new JList();
          BModify.setEnabled(false);
     }

     private void setLayouts()
     {
          setTitle( " Driver Names ");
          setClosable(true);
          setIconifiable(false);
          setBounds(0,0,400,500);
          show();

          getContentPane().setLayout(new GridLayout(1,2));
          LeftPanel .setLayout(new BorderLayout());
          RightTopPanel.setLayout(new GridLayout(4,1,5,5));
          RightPanel.setLayout(new BorderLayout());
          LeftPanel .setBorder(new TitledBorder(" List"));
          RightPanel.setBorder(new TitledBorder("Control Panel"));

          subList.setFont(new Font("ArialBlack",Font.BOLD,11));
     }

     private void addComponents()
     {
          LeftPanel.add("Center",new JScrollPane(subList));
          RightTopPanel.add(TName);
          RightTopPanel.add(BAdd);
          RightTopPanel.add(BModify);
          RightTopPanel.add(BClose);
          RightPanel.add("North",RightTopPanel);
          getContentPane().add(LeftPanel);
          getContentPane().add(RightPanel);

     }

     private void setVectors()
     {

          VName = new Vector();
          VCode = new Vector();

          String QS1 = " Select Name,Id  From VehicleDrivers Order By 1 ";
          try
          {
               Connection theConnection  = getConnection();
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult       = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    VName.addElement((theResult.getString(1)).toUpperCase());
                    VCode.addElement(theResult.getString(2));
               }
               theResult.close();
           }catch(Exception ee){}
          subList.setListData(VName);
      }

     private void addListeners()
     {
          BAdd      .addActionListener(new NameBean());
          BModify   .addActionListener(new NameBean());
          BClose    .addActionListener(new NameBean());
          subList   .addKeyListener(new NameKeyList());
          subList   .addMouseListener(new NameMouseList());
          TName     .addKeyListener(new TextKeyList());
     }
                          
     private class NameBean implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==BAdd && checkData())
                     onAdd();
               if(ae.getSource()==BModify && checkData())                    
                     onModify();
               if(ae.getSource()==BClose)
                     removeFrame();
          }
     }

     private void onAdd()
     {
          try
          {
                Connection subConnection  = getConnection();
                String    QS1             = "Select Max(Id) From VehicleDrivers ";
                Statement addStatement    = subConnection.createStatement();
                ResultSet addResult       = addStatement.executeQuery(QS1);
                addResult.next();
                int  max  = addResult.getInt(1) + 1;
                String  QS2  = "Insert into VehicleDrivers(Id,Name) values(?,?) ";
                PreparedStatement addPrepStatement = subConnection.prepareStatement(QS2);
                addPrepStatement.setInt(1,max);
                addPrepStatement.setString(2,TName.getText());
                addPrepStatement.executeUpdate();
                setDefaults();
                setVectors();

          }catch(Exception ee)
          {
                System.out.println("In ADD -> " + ee);
          }
     }

     private void onModify()
     {
          try
          {
                Connection subConnection  = getConnection();
                int iCode = Integer.parseInt(SCode);
                String QS2 = " Update VehicleDrivers set Name =? where Id=? ";
                PreparedStatement addPrepStatement = subConnection.prepareStatement(QS2);
                addPrepStatement.setString(1,TName.getText());
                addPrepStatement.setInt(2,iCode);
                addPrepStatement.executeUpdate();
                setDefaults();
                setVectors();
          }catch(Exception ee){ System.out.println("In modify -> " + ee);}
     }
     private void setDefaults()
     {
          BModify.setEnabled(false);
          BAdd.setEnabled(true);
          int j = subList.getSelectedIndex();
          TName.setText("");
          SCode = ""; 
     }

     private class NameMouseList extends MouseAdapter
     {  
          public void mouseClicked(MouseEvent me)
          {
               setSelectionDet();
          }
     }

     private class TextKeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               setCursor(); 
          }
     }
     private class NameKeyList extends KeyAdapter
     {
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ENTER)
               {
                    setSelectionDet();
                    return;
               }
               else if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
               {
                    setDefaults();
                    return;
               }
               else
               {
                    setCursor();
               }
           }
     }

     private void setCursor()
     {
          TName.setText((TName.getText()).toUpperCase());
          for(int index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index));
               if(str1.startsWith(TName.getText()))
               {
                    subList.setSelectedValue(str1,true);
                    subList.ensureIndexIsVisible(index+10);
                    break;
               }
          }
     }

     protected void setSelectionDet()
     {
          int index = subList.getSelectedIndex();
          String SName = (String)VName.elementAt(index);
          SCode  = (String)VCode.elementAt(index);
          TName.setText(SName);
          BModify.setEnabled(true);
          BAdd.setEnabled(false);
     }

     private void removeFrame()
     {
          try
          {
               Layer.remove(this);
               Layer.updateUI();
          }
          catch(Exception ex){}
     }
     private Connection getConnection()
     {
          Connection subConnection=null;
          try
          {
               JDBCConnection connect = JDBCConnection.getJDBCConnection();
               subConnection = connect.getConnection();
          }
          catch(Exception ex){}
          return subConnection;                
     }


     private boolean checkData()
     {
          if(TName.getText().equals(""))
          {
               JOptionPane.showMessageDialog(null, tableName +" Name is Empty", "Warning", 
               JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
               TName.requestFocus();
               return false;
          }
          for(int index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index)).trim();
               if(str1.equalsIgnoreCase(TName.getText()))
               {
                    JOptionPane.showMessageDialog(null, fieldName +"  already Exists", "Warning", 
                    JOptionPane.ERROR_MESSAGE,new ImageIcon("D:/HRDGUI/Warning.gif"));
                    return false;
               }
          }
          return true;
     }
}

