package client;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.*;
import javax.swing.border.*;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;
import java.sql.*;

import java.rmi.*;
import java.rmi.registry.*;

import blf.*;
import util.*;

import client.interview.*;
import client.visitor.*;
import client.errector.*;
import client.guest.*;
import client.vehicle.*;
//import client.stores.*;
import client.vital.*;
import client.key.*;
import client.rentvehicle.*;
import client.thabal.*;
import client.master.*;
import client.WaterLorryIncentive.*;

public class Security extends JFrame implements rndi.CodedNames
{
     JMenuBar  JMBSecurity;

     JMenu     JMMaster,JMInterview,JMVisitor,JMErrector,JMGuest,JMVehicle,JMRentVehicle,JMThabal,JMGateInward,JMKey,JMTest,JMIncentive,JMSMS,JVehiclePrint;

     JMenuItem JMIKeyWD,JMIKeyRet,JMITest,JMITestNew,JMITestOutNew,JMIVisitorKeyEntry,JMIVisitorInfo;

     JMenuItem JMISecurityName,JMIPlace,JMIDriver,JMIVehicles,JMIRentVehicle,JMIBunk,JMIRepName;

     JMenuItem JMIInterviewIn,JMIInterviewOut,JMIInterviewUpdate,JMIVisitorIn,JMIVisitorOut,JMIPartyVisitor;

    JMenuItem JMIErrectorIn,JMIErrectorOut,JMIGuestIn,JMIGuestOut,JMIGuestInNew,JMIGuestPrint,JMIGuestStatus,JMIGuestStatusNew,JMIGuestStatusTest,JMIGuestVisitingDetails,JMIGuestDetails,JMIGuestFrame;

     JMenuItem JMIVehicleIn,JMIVehicleOut,JMIStores,JMIStoreswpo,JMIStoresInList,JMIVehicleUpdate,JMIDriverDetails, JMIVehicleMovement,JMIVehicleEntry;

     JMenuItem JMIRdcOut,JMIRdcIn;

     JMenuItem JMIRentVehicleIn,JMIRentVehicleOut;
	 
	 JMenuItem JMIWaterLorryIncentive;
         
     JMenuItem JMIThabalIn,JMIThabalOut;
	 JMenuItem JMIGuestTokenReceivedStatus;
	 JMenuItem JMIKeyNew;
     JMenuItem JMISecuritysms; 
	 JMenuItem  mVehiclePrint;
     JDesktopPane desktop;
  
     StatusPanel SPanel;

   //Stores gDomain;

     Vector         VCode=null,VName=null;
     boolean        bflag;
     util.Common         common= new util.Common();

     public Security()
     {
          desktop = new JDesktopPane();
          updateLookAndFeel();
        //setDomain();
          createComponents();
          setLayouts();
          addComponents();
          addListeners();
          setDataIntoVector();
     }
     /*private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               gDomain             = (Stores) registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }*/
     private void updateLookAndFeel()
     {
          String win = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
          try
          {
               UIManager.setLookAndFeel(win);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void createComponents()
     {
          SPanel              =  new StatusPanel();

          JMBSecurity         =  new JMenuBar();

          JMMaster            =  new JMenu("VitalInfo");
          JMInterview         =  new JMenu("Interview");
          JMVisitor           =  new JMenu("Visitor");
          JMErrector          =  new JMenu("Errector");    
          JMGuest             =  new JMenu("Guest");
          JMVehicle           =  new JMenu("Vehicle");
          JMRentVehicle       =  new JMenu("Rental Vehicle");
          JMThabal            =  new JMenu("Thabal");
          JMGateInward        =  new JMenu("GateInward");
          JMTest              =  new JMenu("Test");
          JMIncentive         = new JMenu("Incentive");
          JMSMS               = new JMenu("SMS");
		  JVehiclePrint		  = new JMenu("VechiclePrint");

          JMKey               =  new JMenu("Keys");

          JMISecurityName     =  new JMenuItem("SecurityName");
          JMIPlace            =  new JMenuItem("Places");
          JMIDriver           =  new JMenuItem("Drivers");
          JMIVehicles         =  new JMenuItem("Vehicles");
          JMIRentVehicle      =  new JMenuItem("Rental Vehicle");
        //JMITest             =  new JMenuItem("TestVisitor");
          JMITest             =  new JMenuItem("VisitorIn");
          JMITestNew          =  new JMenuItem("VisitorIn (Under Testing)");
          JMITestOutNew       =  new JMenuItem("VisitorOut (Under Testing)");
		  JMIVisitorKeyEntry  =  new JMenuItem("Visitor Key Entry");
		 // JMIVisitorInfo 	  =  new JMenuItem("Visitor Information Frame Entry");
          JMIBunk             =  new JMenuItem("Bunk");
					JMIRepName					=  new JMenuItem("Representative");


          JMIKeyWD            =  new JMenuItem("KeyWithDrawn");
          JMIKeyRet           =  new JMenuItem("KeyReturn");
		  JMIKeyNew			  =  new JMenuItem("KeyTransactions");

          JMIInterviewIn      =  new JMenuItem("Check In");
          JMIInterviewOut     =  new JMenuItem("Check Out");
          JMIInterviewUpdate  =  new JMenuItem("Interview Updation");

          JMIVisitorIn        =  new JMenuItem("Visitor In");
          JMIVisitorOut       =  new JMenuItem("Visitor Out");
          JMIPartyVisitor     =  new JMenuItem("Party Visitor Frame");

          JMIErrectorIn       =  new JMenuItem("Errector In");
          JMIErrectorOut      =  new JMenuItem("Errector Out");

//        JMIGuestIn          =  new JMenuItem("GuestIn");
          JMIGuestOut         =  new JMenuItem("GuestOut");
          JMIGuestInNew       =  new JMenuItem("GuestIn");
          JMIGuestPrint       =  new JMenuItem("Guest Token RePrint");
//        JMIGuestStatus      =  new JMenuItem("Guest Token Collect");
          JMIGuestStatusNew   =  new JMenuItem("Guest Token Collect");
          JMIGuestStatusTest  =  new JMenuItem("Guest Test");
//          JMIGuestVisitingDetails   =  new JMenuItem("Guest Visiting Details");
          JMIGuestDetails     =  new JMenuItem("Guest Meeting Time Details");
		  JMIGuestTokenReceivedStatus = new JMenuItem("Guest Token Received Status");
//          JMIGuestFrame       =  new JMenuItem("Guest Frame");



          JMIVehicleIn        =  new JMenuItem("VehicleIn");
          JMIVehicleOut       =  new JMenuItem("VehicleOut");
          JMIVehicleUpdate    =  new JMenuItem("VehicleUpdation");
          JMIDriverDetails    =  new JMenuItem("Driver Details");
	  	  JMIVehicleMovement  =  new JMenuItem("Vehicle Movement");
    	  JMIVehicleEntry	  =  new JMenuItem("Vehicle Entry");	


          JMIRentVehicleIn    =  new JMenuItem("VehicleIn");
          JMIRentVehicleOut   =  new JMenuItem("VehicleOut");

          JMIThabalIn         =  new JMenuItem("Thabal In");
          JMIThabalOut        =  new JMenuItem("Thabal Out");

          JMIStores           =  new JMenuItem("Stores & Spares");
          JMIStoreswpo        =  new JMenuItem("Stores & Spares without P.O");
          JMIStoresInList     =  new JMenuItem("Stores Inward List");
          JMIRdcOut           =  new JMenuItem("RDC Out");
          JMIRdcIn            =  new JMenuItem("RDC In");
		  
		  JMIWaterLorryIncentive = new JMenuItem("Water Lorry Authentication");

       JMISecuritysms      = new JMenuItem("Security SMS");
	   
	   mVehiclePrint		= new JMenuItem("Vehicle Print");

     }

     public void setLayouts()
     {
          Dimension d       = Toolkit.getDefaultToolkit().getScreenSize();

//          setSize(800,600);
          setSize(d);
          setVisible(true);
          setExtendedState(JFrame.MAXIMIZED_BOTH);

          setJMenuBar(JMBSecurity);
          setTitle("Security 1.0");

          JMMaster.setMnemonic('V');
          JMInterview.setMnemonic('I');
          JMVisitor.setMnemonic('V');
          JMErrector.setMnemonic('E');
          JMGuest.setMnemonic('U');
          JMIGuestDetails.setMnemonic('G');
          JMVehicle.setMnemonic('H');
          JMIVehicleIn.setMnemonic('I');
          JMRentVehicle.setMnemonic('R');
          JMThabal.setMnemonic('T');
          JMGateInward.setMnemonic('G');
     }

     public void addComponents()
     {
          JMBSecurity   .add(JMMaster);
          JMBSecurity   .add(JMInterview);
          JMBSecurity   .add(JMVisitor);
          JMBSecurity   .add(JMErrector);
      //    JMBSecurity   .add(JMGuest);  //comment on 04022019
//          JMBSecurity   .add(JMVehicle); comment on 20140902
 //         JMBSecurity   .add(JMKey); // 04.02.2019
          JMBSecurity   .add(JMRentVehicle);
          JMBSecurity   .add(JMThabal);
		  JMBSecurity   .add(JMIncentive);
         
          JMBSecurity  .add(JMSMS);
		   JMBSecurity  .add(JVehiclePrint);
          //JMBSecurity   .add(JMTest); 

//          JMBSecurity   .add(JMGateInward);

          JMMaster.add(JMISecurityName);
          JMMaster.add(JMIPlace);
          JMMaster.add(JMIDriver);
          JMMaster.add(JMIVehicles);  
          JMMaster.add(JMIRentVehicle);
          JMMaster.add(JMIBunk);
					JMMaster.add(JMIRepName);

          JMInterview.add(JMIInterviewIn);
          JMInterview.add(JMIInterviewOut);
          JMInterview.add(JMIInterviewUpdate);

          //JMVisitor.add(JMIVisitorIn);
          JMVisitor.add(JMITest);
          JMVisitor.add(JMIVisitorOut);
          JMVisitor.add(JMITestNew);
          JMVisitor.add(JMITestOutNew);
		  JMVisitor.add(JMIVisitorKeyEntry);
		  //JMVisitor.add(JMIVisitorInfo);
          JMVisitor.add(new JSeparator());
          JMVisitor.add(JMIPartyVisitor);

          JMErrector.add(JMIErrectorIn);
          JMErrector.add(JMIErrectorOut);


//          JMGuest.add(JMIGuestIn);
          JMGuest.add(JMIGuestInNew);
          JMGuest.add(JMIGuestOut);
          JMGuest.add(JMIGuestPrint);
//          JMGuest.add(JMIGuestStatus);
          JMGuest.add(JMIGuestStatusNew);
          JMGuest.add(JMIGuestStatusTest);
//          JMGuest.add(JMIGuestVisitingDetails);
          JMGuest.add(JMIGuestDetails);
//          JMGuest.add(JMIGuestFrame);
          JMGuest.add(JMIGuestTokenReceivedStatus);


          JMVehicle.add(JMIVehicleIn);
          JMVehicle.add(JMIVehicleOut);
          JMVehicle.add(JMIVehicleUpdate);
          JMVehicle.add(JMIDriverDetails);
	  JMVehicle.add(JMIVehicleMovement);
	  JMVehicle.add(JMIVehicleEntry);

          JMRentVehicle.add(JMIRentVehicleIn);
          JMRentVehicle.add(JMIRentVehicleOut);

          JMThabal.add(JMIThabalIn);
          JMThabal.add(JMIThabalOut);

          JMGateInward.add(JMIStores);
          JMGateInward.add(JMIStoreswpo);
          JMGateInward.add(JMIStoresInList);
          JMGateInward.add(JMIRdcOut);
          JMGateInward.add(JMIRdcIn);

          JMKey.add(JMIKeyWD);
          JMKey.add(JMIKeyRet);
		  JMKey.add(JMIKeyNew);

		  JMIncentive.add(JMIWaterLorryIncentive);
          JMSMS.add(JMISecuritysms);
		  
		  JVehiclePrint.add(mVehiclePrint);

		  
          //JMTest.add(JMITest);

          Container con=getContentPane();
          con.setLayout(new BorderLayout());
          con.add(desktop);
          con.add(SPanel,"South");
     }

     public void addListeners()
     {
          JMISecurityName.addActionListener(new addAction());
          JMIPlace.addActionListener(new addAction());
          JMIDriver.addActionListener(new addAction());
          JMIVehicles.addActionListener(new addAction());
          JMIRentVehicle.addActionListener(new addAction());
          JMIBunk.addActionListener(new addAction());
					JMIRepName.addActionListener(new addAction());


          JMIInterviewIn        .addActionListener(new addAction());
          JMIInterviewOut       .addActionListener(new addAction());
          JMIInterviewUpdate    .addActionListener(new addAction());

          JMIVisitorIn.addActionListener(new addAction());
          JMIVisitorOut.addActionListener(new addAction());
          JMIPartyVisitor.addActionListener(new addAction());

          JMIErrectorIn.addActionListener(new addAction());
          JMIErrectorOut.addActionListener(new addAction());

//          JMIGuestIn.addActionListener(new addAction());
          JMIGuestOut.addActionListener(new addAction());
          JMIGuestInNew.addActionListener(new addAction());
          JMIGuestPrint.addActionListener(new addAction());
//          JMIGuestStatus.addActionListener(new addAction());
          JMIGuestStatusNew.addActionListener(new addAction());
          JMIGuestStatusTest.addActionListener(new addAction());
//          JMIGuestVisitingDetails.addActionListener(new addAction());
          JMIGuestDetails.addActionListener(new addAction());
		  JMIGuestTokenReceivedStatus.addActionListener(new addAction());
//          JMIGuestFrame.addActionListener(new addAction());



          JMIVehicleIn.addActionListener(new addAction());
          JMIVehicleOut.addActionListener(new addAction());
          JMIDriverDetails.addActionListener(new addAction());
	  JMIVehicleMovement.addActionListener(new addAction());
	  JMIVehicleEntry.addActionListener(new addAction());

          JMIRentVehicleIn.addActionListener(new addAction());
          JMIRentVehicleOut.addActionListener(new addAction());

          JMIThabalIn.addActionListener(new addAction());
          JMIThabalOut.addActionListener(new addAction());

          JMIStores.addActionListener(new addAction());
          JMIStoreswpo.addActionListener(new addAction());
          JMIStoresInList.addActionListener(new addAction());
          JMIRdcOut.addActionListener(new addAction());
          JMIRdcIn.addActionListener(new addAction());

          JMIKeyWD.addActionListener(new addAction());
          JMIKeyRet.addActionListener(new addAction());
		  
		  JMIKeyNew.addActionListener(new addAction());

          JMITest.addActionListener(new addAction());
		  JMITestNew.addActionListener(new addAction());
		  JMITestOutNew.addActionListener(new addAction());
		  JMIVisitorKeyEntry.addActionListener(new addAction());
		 // JMIVisitorInfo.addActionListener(new addAction());
		  
		  JMIWaterLorryIncentive.addActionListener(new addAction());
           JMISecuritysms.addActionListener(new addAction());
		   mVehiclePrint.addActionListener(new addAction());

     }
     class addAction implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               try
               {
                    /*if(ae.getSource()==JMISecurityName)
                    {
                         SecurityManagementFrame secFrame   =    new SecurityManagementFrame(desktop);
                         desktop.add(secFrame);
                         desktop.repaint();
                         secFrame.setSelected(true);
                         desktop.updateUI();
                         secFrame.show();
                    }*/
                    if(ae.getSource()==JMISecurityName)
                    {
                         ManagementFrame secFrame   =    new ManagementFrame(desktop);
                         desktop.add(secFrame);
                         desktop.repaint();
                         secFrame.setSelected(true);
                         desktop.updateUI();
                         secFrame.show();
                    }
                    if(ae.getSource()==JMIPlace)
                    {
                         PlaceFrame placeFrame   =    new PlaceFrame(desktop);
                         desktop.add(placeFrame);
                         desktop.repaint();
                         placeFrame.setSelected(true);
                         desktop.updateUI();
                         placeFrame.show();
                    }

                    if(ae.getSource()==JMIDriver)
                    {
                         DriverFrame driverFrame   =    new DriverFrame(desktop);
                         desktop.add(driverFrame);
                         desktop.repaint();
                         driverFrame.setSelected(true);
                         desktop.updateUI();
                         driverFrame.show();
                    }
                    if(ae.getSource()==JMIVehicles)
                    {
                         VehicleMasterFrame  vehicleFrame  = new VehicleMasterFrame(desktop);
                         desktop.add(vehicleFrame);
                         desktop.repaint();
                         vehicleFrame.setSelected(true);
                         desktop.updateUI();
                         vehicleFrame.show();
                    }

                    if(ae.getSource()==JMIKeyWD)
                    {
                         KeyWDFrame wdframe       = new KeyWDFrame(desktop);
                         desktop.add(wdframe);
                         desktop.repaint();
                         wdframe.setSelected(true);
                         desktop.updateUI();
                         wdframe.show();
                    }

                    if(ae.getSource()==JMIKeyRet)
                    {
                         KeyReturnFrame retframe       = new KeyReturnFrame(desktop);
                         desktop.add(retframe);
                         desktop.repaint();
                         retframe.setSelected(true);
                         desktop.updateUI();
                         retframe.show();
                    }
					
					if(ae.getSource()==JMIKeyNew)
                    {
                         KeyTransactionsFrame keyTransactionsFrame       = new KeyTransactionsFrame(desktop);
                         desktop.add(keyTransactionsFrame);
                         desktop.repaint();
                         keyTransactionsFrame.setSelected(true);
                         desktop.updateUI();
                         keyTransactionsFrame.show();
                    }

                    if(ae.getSource()==JMIRentVehicle)
                    {
                         RentVehicleMasterFrame rentvehiclemasterframe = new RentVehicleMasterFrame(desktop);
                         desktop.add(rentvehiclemasterframe);
                         desktop.repaint();
                         rentvehiclemasterframe.setSelected(true);
                         desktop.updateUI();
                         rentvehiclemasterframe.show();
                    }

                    if(ae.getSource()==JMIBunk)
                    {
                         BunkFrame bunkFrame   =    new BunkFrame(desktop);
                         desktop.add(bunkFrame);
                         desktop.repaint();
                         bunkFrame.setSelected(true);
                         desktop.updateUI();
                         bunkFrame.show();
                    }

										
										if(ae.getSource()==JMIRepName)
                    {
                         RepresentativeFrame repFrame   =    new RepresentativeFrame(desktop);
                         desktop.add(repFrame);
                         desktop.repaint();
                         repFrame.setSelected(true);
                         desktop.updateUI();
                         repFrame.show();
                    }


                    if(ae.getSource()==JMIInterviewIn)
                    {
                         InterviewInFrame interviewinframe = new InterviewInFrame(desktop);
                         desktop.add(interviewinframe);
                         desktop.repaint();
                         interviewinframe.setSelected(true);
                         desktop.updateUI();
                         interviewinframe.show();
                    }
                    if(ae.getSource()==JMIInterviewOut)
                    {
                         InterviewOutFrame interviewoutframe = new InterviewOutFrame(desktop);
                         desktop.add(interviewoutframe);
                         desktop.repaint();
                         interviewoutframe.setSelected(true);
                         desktop.updateUI();
                         interviewoutframe.show();
                    }
                    if(ae.getSource()==JMIInterviewUpdate)
                    {
                         InterviewUpdationFrame interviewupdationframe = new InterviewUpdationFrame(desktop);
                         desktop.add(interviewupdationframe);
                         desktop.repaint();
                         interviewupdationframe.setSelected(true);
                         desktop.updateUI();
                         interviewupdationframe.show();
                    }
                    if(ae.getSource()==JMIVisitorIn)
                    {
                         VisitorInFrame visitorinframe = new VisitorInFrame(desktop);
                         desktop.add(visitorinframe);
                         desktop.repaint();
                         visitorinframe.setSelected(true);
                         desktop.updateUI();
                         visitorinframe.show();
                    }
                    if(ae.getSource()==JMIVisitorOut)
                    {
                         VisitorOutFrame visitoroutframe = new VisitorOutFrame(desktop);
                         desktop.add(visitoroutframe);
                         desktop.repaint();
                         visitoroutframe.setSelected(true);
                         desktop.updateUI();
                         visitoroutframe.show();
                    }

                    if(ae.getSource()==JMIPartyVisitor)
                    {
                         PartyVisitorFrame partyvisitorframe = new PartyVisitorFrame(desktop);
                         desktop.add(partyvisitorframe);
                         desktop.repaint();
                         partyvisitorframe.setSelected(true);
                         desktop.updateUI();
                         partyvisitorframe.show();
                    }

                    if(ae.getSource()==JMIErrectorIn)
                    {
                         ErrectorInFrame errectorinframe = new ErrectorInFrame(desktop);
                         try
                         {
                              desktop.add(errectorinframe);
                              desktop.repaint();
                              errectorinframe.setSelected(true);
                              desktop.updateUI();
                              errectorinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIErrectorOut)
                    {
                         ErrectorOutFrame errectoroutframe= new ErrectorOutFrame(desktop);
                         try
                         {
                              desktop.add(errectoroutframe);
                              desktop.repaint();
                              errectoroutframe.setSelected(true);
                              desktop.updateUI();
                              errectoroutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

/*                    if(ae.getSource()==JMIGuestIn)
                    {
                         GuestInFrame guestinframe= new GuestInFrame(desktop);
                         try
                         {
                              desktop.add(guestinframe);
                              desktop.repaint();
                              guestinframe.setSelected(true);
                              desktop.updateUI();
                              guestinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    } */

                    if(ae.getSource()==JMIGuestInNew)
                    {
//                         GuestInFrameNew guestinframenew= new GuestInFrameNew(desktop);
                         try
                         {
                             new GuestAuth(desktop,1);

/*                              desktop.add(guestinframenew);
                              desktop.repaint();
                              guestinframenew.setSelected(true);
                              desktop.updateUI();
                              guestinframenew.show(); */
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }



                    if(ae.getSource()==JMIGuestPrint)
                    {
//                         GuestInFramePrint guestinprint= new GuestInFramePrint(desktop);
                         try
                         {
/*                              desktop.add(guestinprint);
                              desktop.repaint();
                              guestinprint.setSelected(true);
                              desktop.updateUI();
                              guestinprint.show(); */
                              new GuestAuth(desktop,2);
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIGuestStatus)
                    {
                         try
                         {
                              new GuestAuth(desktop,3);
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIGuestStatusNew)
                    {
                         try
                         {
                              new GuestAuth(desktop,4);
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

/*                  if(ae.getSource()==JMIGuestVisitingDetails)
                    {
                         GuestVisitingDetails guestvisitingdetails= new GuestVisitingDetails(desktop);
                         try
                         {

                              desktop.add(guestvisitingdetails);
                              desktop.repaint();
                              guestvisitingdetails.setSelected(true);
                              desktop.updateUI();
                              guestvisitingdetails.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }

*/

                    if(ae.getSource()==JMIGuestDetails)
                    {
                         GuestDetails guestdetails= new GuestDetails(desktop);
                         try
                         {

                              desktop.add(guestdetails);
                              desktop.repaint();
                              guestdetails.setSelected(true);
                              desktop.updateUI();
                              guestdetails.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }
					
					if(ae.getSource()==JMIGuestTokenReceivedStatus)
                    {
                         GuestTokenReceivedStatusFrame GuestTokenReceivedStatusFrame= new GuestTokenReceivedStatusFrame(desktop);
                         try
                         {

                              desktop.add(GuestTokenReceivedStatusFrame);
                              desktop.repaint();
                              GuestTokenReceivedStatusFrame.setSelected(true);
                              desktop.updateUI();
                              GuestTokenReceivedStatusFrame.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }


/*                    if(ae.getSource()==JMIGuestFrame)
                    {
                         GuestFrame guestframe= new GuestFrame(desktop);
                         try
                         {

                              desktop.add(guestframe);
                              desktop.repaint();
                              guestframe.setSelected(true);
                              desktop.updateUI();
                              guestframe.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }

*/
                    if(ae.getSource()==JMIGuestOut)
                    {
                         GuestOutFrame guestoutframe= new GuestOutFrame(desktop);
                         try
                         {
                              desktop.add(guestoutframe);
                              desktop.repaint();
                              guestoutframe.setSelected(true);
                              desktop.updateUI();
                              guestoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIVehicleIn)
                    {
                         VehicleInFrame vehicleinframe= new VehicleInFrame(desktop);
                         try
                         {
                              desktop.add(vehicleinframe);
                              desktop.repaint();
                              vehicleinframe.setSelected(true);
                              desktop.updateUI();
                              vehicleinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIVehicleOut)
                    {
                         VehicleOutFrameNew vehicleoutframe= new VehicleOutFrameNew(desktop);
                         try
                         {
                              desktop.add(vehicleoutframe);
                              desktop.repaint();
                              vehicleoutframe.setSelected(true);
                              desktop.updateUI();
                              vehicleoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }
					/*if(ae.getSource()==JMIVehicleOut)
                    {
                         VehicleOutFrame vehicleoutframe= new VehicleOutFrame(desktop);
                         try
                         {
                              desktop.add(vehicleoutframe);
                              desktop.repaint();
                              vehicleoutframe.setSelected(true);
                              desktop.updateUI();
                              vehicleoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }*/
//JMIVehicleMovement
                    if(ae.getSource()==JMIVehicleMovement)
                    {
                         VehicleMovementFrame vehiclemovementframe= new VehicleMovementFrame(desktop);
                         try
                         {
                              desktop.add(vehiclemovementframe);
                              desktop.repaint();
                              vehiclemovementframe.setSelected(true);
                              desktop.updateUI();
                              vehiclemovementframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }
					
//JMIVehicleEntry			
					if(ae.getSource()==JMIVehicleEntry)
                    {
                         VehicleEntryFrame vehicleEntryFrame= new VehicleEntryFrame(desktop);
                         try
                         {

                              desktop.add(vehicleEntryFrame);
                              desktop.repaint();
                              vehicleEntryFrame.setSelected(true);
                              desktop.updateUI();
                              vehicleEntryFrame.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }		

                    if(ae.getSource()==JMIRentVehicleIn)
                    {
                         RentVehicleInFrame rentvehicleinframe= new RentVehicleInFrame(desktop);
                         try
                         {
                              desktop.add(rentvehicleinframe);
                              desktop.repaint();
                              rentvehicleinframe.setSelected(true);
                              desktop.updateUI();
                              rentvehicleinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIRentVehicleOut)
                    {
                         RentVehicleOutFrame rentvehicleoutframe= new RentVehicleOutFrame(desktop);
                         try
                         {
                              desktop.add(rentvehicleoutframe);
                              desktop.repaint();
                              rentvehicleoutframe.setSelected(true);
                              desktop.updateUI();
                              rentvehicleoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }


                    if(ae.getSource()==JMIDriverDetails)
                    {
                         DriverDetails driverdetails= new DriverDetails(desktop);
                         try
                         {

                              desktop.add(driverdetails);
                              desktop.repaint();
                              driverdetails.setSelected(true);
                              desktop.updateUI();
                              driverdetails.show(); 
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         } 
                    }





                    if(ae.getSource()==JMIThabalIn)
                    {
                         ThabalInFrame thabalinframe= new ThabalInFrame(desktop);
                         try
                         {
                              desktop.add(thabalinframe);
                              desktop.repaint();
                              thabalinframe.setSelected(true);
                              desktop.updateUI();
                              thabalinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIThabalOut)
                    {
                         ThabalOutFrame thabaloutframe= new ThabalOutFrame(desktop);
                         try
                         {
                              desktop.add(thabaloutframe);
                              desktop.repaint();
                              thabaloutframe.setSelected(true);
                              desktop.updateUI();
                              thabaloutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMITest)
                    {
                         //VisitorInFrameCopy visitframe = new VisitorInFrameCopy(desktop);
			 VisitorInFrameNew visitframe = new VisitorInFrameNew(desktop);
                         try
                         {
                              desktop.add(visitframe);
                              desktop.repaint();
                              visitframe.setSelected(true);
                              desktop.updateUI();
                              visitframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

					if(ae.getSource()==JMITestNew)
                    {
			 			VisitorFrame visitframe = new VisitorFrame(desktop);
                         try
                         {
                              desktop.add(visitframe);
                              desktop.repaint();
                              visitframe.setSelected(true);
                              desktop.updateUI();
                              visitframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

					if(ae.getSource()==JMITestOutNew)
                    {
			 			VisitorOutFrameNew visitframe = new VisitorOutFrameNew(desktop);
                         try
                         {
                              desktop.add(visitframe);
                              desktop.repaint();
                              visitframe.setSelected(true);
                              desktop.updateUI();
                              visitframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }
					if(ae.getSource()==JMIVisitorKeyEntry)
					{
						VisitorKeyEntryFrame keyentryframe = new VisitorKeyEntryFrame(desktop);
                        try
                        {
                              desktop.add(keyentryframe);
                              desktop.repaint();
                              keyentryframe.setVisible(true);
                              desktop.updateUI();
                              keyentryframe.show();
                        }
                        catch(Exception ex)
                        {
                        	ex.printStackTrace();     
                        }
					}
					/*if(ae.getSource()==JMIVisitorInfo)
					{
						VisitorInfoFrame visitframe = new VisitorInfoFrame(desktop);
                         try
                         {
                              desktop.add(visitframe);
                              desktop.repaint();
                              visitframe.setSelected(true);
                              desktop.updateUI();
                              visitframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
					}*/
		if(ae.getSource()==JMIWaterLorryIncentive)
                    {
                         //WaterLorryIncentiveFrameNew wframe       = new WaterLorryIncentiveFrameNew(desktop);
						 WaterLorryIncentiveFrame wframe       = new WaterLorryIncentiveFrame(desktop);
                         desktop.add(wframe);
                         desktop.repaint();
                         wframe.setSelected(true);
                         desktop.updateUI();
                         wframe.show();
                    }

                         if(ae.getSource()==JMISecuritysms)
                    {
                         //VisitorInFrameCopy visitframe = new VisitorInFrameCopy(desktop);
			 SecuritySMSFrame smsframe = new SecuritySMSFrame(desktop);
                         try
                         {
                              desktop.add(smsframe);
                              desktop.repaint();
                              smsframe.setSelected(true);
                              desktop.updateUI();
                              smsframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }
					
					if(ae.getSource()==mVehiclePrint)
                    {
						VehiclesMovementDetails theFrame = new VehiclesMovementDetails(desktop);
                         try
                         {
                              desktop.add(theFrame);
                              desktop.repaint();
                              theFrame.setSelected(true);
                              desktop.updateUI();
                              theFrame.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }


                    /*if(ae.getSource()==JMIStores)
                    {
                         GISFrame gisframe= new GISFrame(desktop,SPanel);
                         try
                         {
                              desktop.add(gisframe);
                              desktop.repaint();
                              gisframe.setSelected(true);
                              desktop.updateUI();
                              gisframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIStoreswpo)
                    {
                         GIDSFrame gidsframe= new GIDSFrame(desktop,VCode,VName,SPanel);
                         try
                         {
                              desktop.add(gidsframe);
                              desktop.repaint();
                              gidsframe.setSelected(true);
                              desktop.updateUI();
                              gidsframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIStoresInList)
                    {
                         StoresInwardList silist= new StoresInwardList(desktop,VCode,VName,SPanel);
                         try
                         {
                              desktop.add(silist);
                              desktop.repaint();
                              silist.setSelected(true);
                              desktop.updateUI();
                              silist.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIRdcOut)
                    {
                         RDCOutFrame rdcoutframe= new RDCOutFrame(desktop);
                         try
                         {
                              desktop.add(rdcoutframe);
                              desktop.repaint();
                              rdcoutframe.setSelected(true);
                              desktop.updateUI();
                              rdcoutframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }

                    if(ae.getSource()==JMIRdcIn)
                    {
                         RDCInFrame rdcinframe= new RDCInFrame(desktop,SPanel);
                         try
                         {
                              desktop.add(rdcinframe);
                              desktop.repaint();
                              rdcinframe.setSelected(true);
                              desktop.updateUI();
                              rdcinframe.show();
                         }
                         catch(Exception ex)
                         {
                              ex.printStackTrace();     
                         }
                    }*/

               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }

          }
     }   

     public void setDataIntoVector()
     {
         /* SPanel.LLabel.setText("Collecting Data From Material Master");

          try
          {
               Vector NameVector = gDomain.getInvName();
               Vector CodeVector = gDomain.getInvCode();
               VCode          = new Vector();
               VName          = new Vector();

               int m=0;
               int size = NameVector.size()/2;
                    for(int j=0;j<size;j++)
                    {
                         VName.addElement((String)NameVector.elementAt(0+m));
                         VCode.addElement((String)CodeVector.elementAt(1+m));
                         m=m+2;
                    }
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
               JOptionPane.showMessageDialog(null,"Unable to Establish a Connection with Server","Information",JOptionPane.INFORMATION_MESSAGE);
               System.exit(0);
          }
          SPanel.LLabel.setText(" ");
          */
     }

     public static void main(String args[])
     {
          Security ff = new Security();
          ff.show();
     }
}

