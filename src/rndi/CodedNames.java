/*
     This interface defines names in code used as
     argument for lookup().

     author : Rajasubramaniam.K
*/

package rndi;
import java.awt.Color;
public interface CodedNames
{
     public static final String RMIHOST            = "172.16.2.20";
     public static final int    RMIPORT            = 4445;
     public static final String SECURITYDOMAIN     = "SecurityDomain";
     public static final String SCRIPTHOST         = "172.16.2.20";
     public static final String SCRIPTFOLDER       = "SECURITY1.1";

     public static final Color  FOREGROUND     = new Color(128,0,64);
     public static final Color  PANEL_NORMAL_BACKGROUND = new Color(192,192,192);
     public static final Color  PANEL_ENHANCED_BACKGROUND = new Color(193,193,255);
     public static final Color  BUTTON_NORMAL_FOREGROUND   = new Color(255,255,255);
     public static final Color  BUTTON_NORMAL_BACKGROUND   = new Color(79,157,157);
     public static final Color  BUTTON_ENHANCED_FOREGROUND = new Color(255,255,255);
     public static final Color  BUTTON_ENHANCED_BACKGROUND = new Color(139,197,197);
     public static final Color  BALEID_BACKGROUND = new Color(128,128,192);

     public static int ALL       = -1;

     public static int NORMAL    = 0;
     public static int REJECTION = 1;
     public static int TRANSFER  = 2;
     public static int PURCHASE  = 4;

     public static int ORDER    = 0;
     public static int MATCHING = 1;
     public static int DYER     = 2;
     
}


