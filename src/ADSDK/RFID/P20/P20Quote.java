package ADSDK.RFID.P20;

import ADSDK.Communication.Quote;

public class P20Quote extends Quote{

	/// ---Public---
    /// <summary>
    /// ADDR PUB:HEX FFFF Decimal 65535
    /// </summary>
    public static final int ADDR_PUB = 65535;
    /// <summary>
    /// LEN SIX:HEX 06 Decimal 6
    /// </summary>
    public static final int LEN_SIX = 6;
    /// <summary>
    /// LEN SEVEN:HEX 07 Decimal 7
    /// </summary>
    public static final int LEN_SEVEN = 7;
    /// <summary>
    /// SOI:HEX 7C Decimal 124
    /// </summary>
    public static final int SOI_SEND = 124;
    /// <summary>
    /// SOI:HEX CC Decimal 204
    /// </summary>
    public static final int SOI_RECIVE = 204;

    /// ---6B---
    /// <summary>
    /// IDEN CARD HEX:0701 Decimal 1793
    /// </summary>
    public static final int IDEN_6B = 1793;
    /// <summary>
    /// GET CARD HEX:0702 Decimal 1794
    /// </summary>
    public static final int GET_6B = 1794;
    /// <summary>
    /// SET CARD HEX:0703 Decimal 1795
    /// </summary>
    public static final int SET_6B = 1795;

    /// ---6C---
    /// <summary>
    /// IDEN CARD HEX:0801 Decimal 2049
    /// </summary>
    public static final int IDEN_6C = 2049;
    /// <summary>
    /// GET CARD HEX:0802 Decimal 2050
    /// </summary>
    public static final int GET_6C = 2050;
    /// <summary>
    /// SET CARD HEX:0803 Decimal 2051
    /// </summary>
    public static final int SET_6C = 2051;

    /// ---Parameter---
    /// <summary>
    /// SET PARA HEX:3081 Decimal 12417
    /// </summary>
    public static final int SET_PARA = 12417;
    /// <summary>
    /// GET PARA HEX:3082 Decimal 12418
    /// </summary>
    public static final int GET_PARA = 12418;

    /// ---Other---
    /// <summary>
    /// RCV SOI INFO HEX:0A Decimal 10
    /// </summary>
    public static final int SOI_RCV_INFO = 10;
    /// <summary>
    /// INFO ADDR:HEX BBAA Decimal 48042
    /// </summary>
    public static final int ADDR_INFO = 48042;
    /// <summary>
    /// SET ADDR��HEX:708C Decimal 28812
    /// </summary>
    public static final int SET_ADDR = 28812;
    /// <summary>
    /// GET INFO HEX:2080 Decimal 8320
    /// </summary>
    public static final int GET_INFO = 8320;

}
