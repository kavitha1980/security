package ADSDK.RFID.P20;

import ADSDK.Communication.CommBase;

public class P20Comm extends CommBase {
	
	private boolean _isConnectedReader = false;
    public boolean getReaderConnected(){
    	return this._isConnectedReader;
    }
    public void setReaderConnected(boolean state){
    	this._isConnectedReader = state;
    }

    private int _Address = 65535;
    public int getAddress(){
    	return this._Address;
    }
    public void setAddress(int address){
    	this._Address= address;
    }
    
    private P20Command pcCommand = new P20Command();
    
    public P20Comm()
    {
    	
    }
    
    /**
   	 * 获取联机信息
   	 */ 
    public int getInfomation()
    {
        byte[] bytSend = pcCommand.GetInformation();
        int FCount = Communication(bytSend, true);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length == 112) return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }

    /**
   	 * 获取基本参数
   	 */ 
    public int getPara()
    {
        byte[] bytSend = pcCommand.GetParameters(_Address);
        int FCount = Communication(bytSend, true);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length >= 6 && 
        		this.getReceiveData_Hex()[3] ==  (byte)(P20Quote.GET_PARA % 256))
            return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }
    
    /**
	 * 写入基本参数
	 */ 
    public int setPara(byte[] sndData)
    {
        byte[] bytSend = pcCommand.SetParameters(_Address,sndData);

        int FCount = Communication(bytSend, true);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length >= 6 && 
        		this.getReceiveData_Hex()[0] == (byte)0xCC && 
        		this.getReceiveData_Hex()[5] == (byte)0x01 && 
        		this.getReceiveData_Hex()[3] == (byte)(P20Quote.SET_PARA % 256))
            return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }
    
    /**
	 * 识别标签
	 */ 
    public int identify6C()
    {
        byte[] bytSend = pcCommand.Identify6C(_Address);
        int FCount = Communication(bytSend, false);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length >= 6 && 
        		this.getReceiveData_Hex()[5] > 0 && 
        		this.getReceiveData_Hex()[3] == (byte)(P20Quote.IDEN_6C % 256))
            return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }

    /**
	 * 读取标签数据
	 */ 
    public int read6C(byte mem, byte state, byte len)
    {
        byte[] bytSend = pcCommand.Read6C(_Address,mem,state,len);
        int FCount = Communication(bytSend, false);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length >= 6 && 
        		this.getReceiveData_Hex()[5] > 0 && 
        		this.getReceiveData_Hex()[3] == (byte)(P20Quote.GET_6C % 256))
            return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }

    /**
   	 * 写入标签数据
   	 */ 
    public int write6C(byte mem, byte state, byte len, byte[] sndData)
    {
        byte[] bytSend = pcCommand.Write6C(_Address, mem, state, len,sndData);
        int FCount = Communication(bytSend, false);
        if (FCount != P20Quote.SUCCEED) return FCount;

        if (this.getReceiveData_Hex().length >= 6 && 
        		this.getReceiveData_Hex()[5] == (byte)0x01 && 
        		this.getReceiveData_Hex()[3] == (byte)(P20Quote.SET_6C % 256))
            return P20Quote.SUCCEED;
        return P20Quote.FAIL;
    }
}
