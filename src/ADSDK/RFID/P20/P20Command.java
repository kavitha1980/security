package ADSDK.RFID.P20;

public class P20Command extends P20Quote{
	 
    /// ---获取联机信息过程---
    public byte[] GetInformation()
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(ADDR_INFO % 256);
        bytSend[intCount++] = (byte)(ADDR_INFO / 256);
        bytSend[intCount++] = (byte)(GET_INFO % 256);
        bytSend[intCount++] = (byte)(GET_INFO / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }
    
    /// ---获取6B卡号过程---
    public byte[] Identify6B(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(IDEN_6B % 256);
        bytSend[intCount++] = (byte)(IDEN_6B / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---读取6B数据过程---
    public byte[] Read6B(int address, byte state, byte size)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX + 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(GET_6B % 256);
        bytSend[intCount++] = (byte)(GET_6B / 256);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)size;
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---写入6B数据过程---
    public byte[] Write6B(int address, byte state, byte size, byte[] data)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX + 2 + size];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(SET_6B % 256);
        bytSend[intCount++] = (byte)(SET_6B / 256);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)size;
        for (int m = 0 ; m < size ; m++)
        {
            bytSend[intCount++] = data[m];
        }
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---获取6C卡号过程---
    public byte[] Identify6C(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(IDEN_6C % 256);
        bytSend[intCount++] = (byte)(IDEN_6C / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---读取6C数据过程---
    public byte[] Read6C(int address, byte mem, byte state, byte size)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX + 3];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(GET_6C % 256);
        bytSend[intCount++] = (byte)(GET_6C / 256);
        bytSend[intCount++] = (byte)(mem);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)((size % 2 > 0) ? (size / 2 + 1) : (size / 2));
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---写入6C数据过程---
    public byte[] Write6C(int address, byte mem, byte state, byte size, byte[] data)
    {
        int intLengthTemp = (size % 2 > 0) ? (size / 2 + 1) : (size / 2);
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX + 3 + intLengthTemp * 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(SET_6C % 256);
        bytSend[intCount++] = (byte)(SET_6C / 256);
        bytSend[intCount++] = (byte)(mem);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)intLengthTemp;
        for (int m = 0 ; m < size ; m++)
        {
            bytSend[intCount++] = data[m];
        }
        if (size % 2 > 0) bytSend[intCount++] = (byte)(0);

        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---获取读卡器参数过程---
    public byte[] GetParameters(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(GET_PARA % 256);
        bytSend[intCount++] = (byte)(GET_PARA / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---设置读卡器参数过程---
    public byte[] SetParameters(int address, byte[] para)
    {
        int intCount = 0;
        int intLength = para.length + LEN_SIX;
        byte[] bytSend = new byte[intLength];

        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(SET_PARA % 256);
        bytSend[intCount++] = (byte)(SET_PARA / 256);

        for (int m = 0 ; m < para.length ; m++)
        {
            bytSend[intCount++] = para[m];
        }
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /// ---设置通讯地址过程---
    public byte[] SetAddress(int address, int paraAddr)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SIX + 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(SET_ADDR % 256);
        bytSend[intCount++] = (byte)(SET_ADDR / 256);
        bytSend[intCount++] = (byte)(paraAddr % 256);
        bytSend[intCount++] = (byte)(paraAddr / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

}
