package ADSDK.RFID.P30;

import ADSDK.RFID.P30.P30Quote;

/**
 * 3.0版读卡器通讯命令数据包
 * @author ENDY
 *
 */
public class P30Command extends P30Quote{
	 
	 /**
	  * 获取联机信息命令数据包
	  * @return
	  */
    public byte[] GetInformation()
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(ADDR_PUB);
        bytSend[intCount++] = (byte)(ADDR_PUB);
        bytSend[intCount++] = (byte)(INFO);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(0);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }
    
    /**
     * 识别ISO180000-6B标签数据命令数据包
     * @param address	通讯地址 ,默认65535
     * @return
     */
    public byte[] Identify6B(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(ISO6B_IDEN);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(0);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 读取ISO180000-6B标签数据命令数据包
     * @param address 通讯地址 ,默认65535
     * @param state 数据地址
     * @param size 数据长度
     * @return
     */
    public byte[] Read6B(int address, byte state, byte size)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN + 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(ISO6B_RW);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(2);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)size;
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 写入ISO180000-6B标签数据命令数据包
     * @param address 通讯地址 ,默认65535
     * @param state 数据地址
     * @param size 数据长度
     * @param data 数据集
     * @return
     */
    public byte[] Write6B(int address, byte state, byte size, byte[] data)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN + 2 + size];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(ISO6B_RW);
        bytSend[intCount++] = (byte)(COMM_SET);
        bytSend[intCount++] = (byte)(2 + size);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)size;
        for (int m = 0 ; m < size ; m++)
        {
            bytSend[intCount++] = data[m];
        }
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 识别EPC标签命令数据包
     * @param address 通讯地址 ,默认65535
     * @return
     */
    public byte[] Identify6C(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(EPC_IDEN);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(0);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 读取EPC标签数据命令数据包
     * @param address 通讯地址 ,默认65535
     * @param mem	块地址	;0-保留区,1-EPC区,2-TID区,3-用户区
     * @param state	数据地址;EPC区地址必须从2开始
     * @param size	数据长度;			
     * @return
     */
    public byte[] Read6C(int address, byte mem, byte state, byte size)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN + 3];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(EPC_RW);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(3);
        bytSend[intCount++] = (byte)(mem);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)((size % 2 > 0) ? (size / 2 + 1) : (size / 2));
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 写入EPC标签数据命令数据包
     * @param address 通讯地址 ,默认65535
     * @param mem		块地址	;0-保留区,1-EPC区,2-TID区,3-用户区
     * @param state		数据地址;EPC区地址必须从2开始
     * @param len		数据长度;		
     * @param sndData	数据集;
     * @return
     */
    public byte[] Write6C(int address, byte mem, byte state, byte size, byte[] data)
    {
        int intLengthTemp = (size % 2 > 0) ? (size / 2 + 1) : (size / 2);
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN + 3 + intLengthTemp * 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(EPC_RW);
        bytSend[intCount++] = (byte)(COMM_SET);
        bytSend[intCount++] = (byte)(3 + intLengthTemp*2);
        bytSend[intCount++] = (byte)(mem);
        bytSend[intCount++] = (byte)state;
        bytSend[intCount++] = (byte)intLengthTemp;
        for (int m = 0 ; m < size ; m++)
        {
            bytSend[intCount++] = data[m];
        }
        if (size % 2 > 0) bytSend[intCount++] = (byte)(0);

        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
     * 获取基本参数命令数据包
     * @param address 通讯地址 ,默认65535
     * @return
     */
    public byte[] GetParameters(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(PARA);
        bytSend[intCount++] = (byte)(COMM_GET);
        bytSend[intCount++] = (byte)(0);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }
    
    /**
     * 设置基本参数命令数据包
     * @param address 通讯地址 ,默认65535
     * @param para 参数集
     * @return
     */
    public byte[] SetParameters(int address, byte[] para)
    {
        int intCount = 0;
        int intLength = para.length + LEN_SEVEN;
        byte[] bytSend = new byte[intLength];

        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(PARA);
        bytSend[intCount++] = (byte)(COMM_SET);
        bytSend[intCount++] = (byte)(para.length);

        for (int m = 0 ; m < para.length ; m++)
        {
            bytSend[intCount++] = para[m];
        }
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
   	 * 设置通讯地址命令数据包
   	 */ 
    public byte[] SetAddress(int address, int paraAddr)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN + 2];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(INFO);
        bytSend[intCount++] = (byte)(COMM_SET);
        bytSend[intCount++] = (byte)(2);
        bytSend[intCount++] = (byte)(paraAddr % 256);
        bytSend[intCount++] = (byte)(paraAddr / 256);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }


    /**
   	 * 获取TCPIP参数命令数据包
   	 */ 
    public byte[] GetTcpip(int address)
    {
        int intCount = 0;
        byte[] bytSend = new byte[LEN_SEVEN];
        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(TCPIP);
        bytSend[intCount++] = (byte)(SENIOR_GET);
        bytSend[intCount++] = (byte)(0);
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

    /**
   	 * 涉资TCPIP参数命令数据包
   	 */ 
    public byte[] SetTcpip(int address, byte[] para)
    {
        int intCount = 0;
        int intLength = para.length + LEN_SEVEN;
        byte[] bytSend = new byte[intLength];

        bytSend[intCount++] = SOI_SEND;
        bytSend[intCount++] = (byte)(address % 256);
        bytSend[intCount++] = (byte)(address / 256);
        bytSend[intCount++] = (byte)(TCPIP);
        bytSend[intCount++] = (byte)(SENIOR_SET);
        bytSend[intCount++] = (byte)(para.length);

        for (int m = 0 ; m < para.length ; m++)
        {
            bytSend[intCount++] = para[m];
        }
        bytSend[intCount++] = (byte)Checksum(bytSend, 0, intCount);
        return bytSend;
    }

}
