package ADSDK.RFID.P30;

import ADSDK.Communication.Quote;
/**
 * 3.0�������ͨѶ����
 * @author ENDY
 *
 */
public class P30Quote extends Quote{

	/// ---Public---
    /// <summary>
    /// ADDR PUB:HEX FF Decimal 255
    /// </summary>
    public static final int ADDR_PUB = 255;
    /// <summary>
    /// LEN SIX:HEX 06 Decimal 6
    /// </summary>
    public static final int LEN_SIX = 6;
    /// <summary>
    /// LEN SEVEN:HEX 07 Decimal 7
    /// </summary>
    public static final int LEN_SEVEN = 7;
    /// <summary>
    /// SOI:HEX 7C Decimal 124
    /// </summary>
    public static final int SOI_SEND = 124;
    /// <summary>
    /// SOI:HEX CC Decimal 204
    /// </summary>
    public static final int SOI_RECIVE = 204;

    /// ---6B---
    /// <summary>
    ///  ISO6B_IDEN HEX:0x01
    /// </summary>
    public static final int ISO6B_IDEN = 1;
    /// <summary>
    /// ISO6B_RW HEX:0x02
    /// </summary>
    public static final int ISO6B_RW = 2;

    /// ---6C---
    /// <summary>
    /// EPC_IDEN HEX:0x10
    /// </summary>
    public static final int EPC_IDEN = 16;
    /// <summary>
    /// EPC_MULT HEX:0x11
    /// </summary>
    public static final int EPC_MULT = 17;
    /// <summary>
    ///EPC_RW HEX:0x12
    /// </summary>
    public static final int EPC_RW = 18;

    /// ---Parameter---
    /// <summary>
    ///PARA HEX:0x81
    /// </summary>
    public static final int PARA = 129;
    /// <summary>
    /// TCPIP HEX:0xB9
    /// </summary>
    public static final int TCPIP = 185;
    /// <summary>
    /// GET INFO HEX:0x82
    /// </summary>
    public static final int INFO = 130;
    /// <summary>
    /// SECRET HEX:0x30
    /// </summary>
    public static final int SECRET = 48;
    /// <summary>
    /// SECRET HEX:0x31
    /// </summary>
    public static final int OUTCARD = 49;
    /// <summary>
    /// Set Syris Address HEX:0x8D
    /// </summary>
    public static final int INITSYRIS = 141;
    /// <summary>
    /// SOFTRESET HEX:0x8F
    /// </summary>
    public static final int SOFTRESET = 143;

    /// ---Other---
  /// <summary>
    /// Control identification code :get
    /// </summary>
    public static final int COMM_GET = 50;
    /// <summary>
    /// Control identification code :set
    /// </summary>
    public static final int COMM_SET = 49;
    /// <summary>
    /// Control identification code :get
    /// </summary>
    public static final int SENIOR_GET = 34;
    /// <summary>
    /// Control identification code :set
    /// </summary>
    public static final int SENIOR_SET = 33;

}
