package ADSDK.RFID.P30;

import ADSDK.Communication.CommBase;
import ADSDK.Communication.Quote;

/**
 * 
 * @author ENDY
 *
 */
public class P30Comm extends CommBase {
	
	private boolean _isConnectedReader = false;
    public boolean getReaderConnected(){
    	return this._isConnectedReader;
    }
    public void setReaderConnected(boolean state){
    	this._isConnectedReader = state;
    }

    private int _Address = 65535;
    ///通讯地址
    public int getAddress(){
    	return this._Address;
    }
    public void setAddress(int address){
    	this._Address= address;
    }
    
    private P30Command pcCommand = new P30Command();
    
    public P30Comm()
    {
    	
    }
        
    /**
     * 校验返回数据完整性
     * @param fCount
     * @param rcvData
     * @param cid
     * @param isSet
     * @return
     */
    private int CheckRecvData(int fCount, byte[] rcvData, int cid, int isSet)
    {
        if (fCount != Quote.SUCCEED) return fCount;
        if (rcvData.length >= 6 && rcvData[4] == (byte)0 && rcvData[3] == (byte)cid) return Quote.SUCCEED;

        if (isSet == P30Quote.COMM_SET) return Quote.ERROR_SET;
        return Quote.ERROR_GET;
    }
    
    /**
     * 获取联机信息
     * @return
     */
    public int getInfomation()
    {
        byte[] bytSend = pcCommand.GetInformation();
        int FCount = Communication(bytSend, true);
        
        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.INFO, P30Quote.COMM_GET);
     
        return FCount;
    }
    
    /**
     * 获取基本参数
     * @return
     */
    public int getPara()
    {
        byte[] bytSend = pcCommand.GetParameters(_Address);
        int FCount = Communication(bytSend, true);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.PARA, P30Quote.COMM_GET);
     
        return FCount;
    }

    /**
     * 设置基本参数
     * @param sndData
     * @return
     */
    public int setPara(byte[] sndData)
    {
        byte[] bytSend = pcCommand.SetParameters(_Address,sndData);

        int FCount = Communication(bytSend, true);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.PARA, P30Quote.COMM_SET);
     
        return FCount;
    }
      

    /**
     * Get tcpip parameters from reader
     * @return
     */
    public int getTcpip()
    {
        byte[] bytSend = pcCommand.GetTcpip(_Address);
        int FCount = Communication(bytSend, true);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.TCPIP, P30Quote.SENIOR_GET);
     
        return FCount;
    }

    /**
     * Set tcpip parameters to reader
     * @param sndData
     * @return
     */
    public int setTcpip(byte[] sndData)
    {
        byte[] bytSend = pcCommand.SetTcpip(_Address,sndData);

        int FCount = Communication(bytSend, true);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.TCPIP, P30Quote.SENIOR_SET);
     
        return FCount;
    }
    
    /**
     * 识别EPC标签
     * @return
     */
    public int identify6C()
    {
        byte[] bytSend = pcCommand.Identify6C(_Address);
        int FCount = Communication(bytSend, false);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.EPC_IDEN, P30Quote.COMM_GET);
     
        return FCount;
    }


    /**
     * 获取EPC标签数据
     * @param mem	块地址	;0-保留区,1-EPC区,2-TID区,3-用户区
     * @param state	数据地址;EPC区地址必须从2开始
     * @param len	数据长度;				
     * @return
     */
    public int read6C(byte mem, byte state, byte len)
    {
        byte[] bytSend = pcCommand.Read6C(_Address,mem,state,len);
        int FCount = Communication(bytSend, false);
        if (FCount != P30Quote.SUCCEED) return FCount;

        FCount = CheckRecvData(FCount,  this.getReceiveData_Hex(), P30Quote.EPC_RW, P30Quote.COMM_GET);
        
        return FCount;
    }
    
    /**
     * 写入EPC标签数据
     * @param mem		块地址	;0-保留区,1-EPC区,2-TID区,3-用户区
     * @param state		数据地址;EPC区地址必须从2开始
     * @param len		数据长度;		
     * @param sndData	数据;
     * @return
     */
    public int write6C(byte mem, byte state, byte len, byte[] sndData)
    {
        byte[] bytSend = pcCommand.Write6C(_Address, mem, state, len,sndData);
        int FCount = Communication(bytSend, false);

        FCount = CheckRecvData(FCount, this.getReceiveData_Hex(), P30Quote.EPC_RW, P30Quote.COMM_SET);
        return FCount;
    }
}
