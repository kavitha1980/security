package ADSDK.Communication;

public class CommandInfo {

     //Communication Name
     private String _Name= "";
     
     public String getName(){
    	 return this._Name;
     }
     public void setName(String name){
    	 this._Name = name;
     }
     
     /// Access Address
     private int _Address =65535;
     
     public int getAddress(){
    	 return this._Address;
     }
     public void setAddress(int address){
    	 this._Address = address;
     }

     /// Access Command mark Sum
     private int _CID =0;
     
     public int getCID(){
    	 return this._CID;
     }
     public void setCID(int CID){
    	 this._CID = CID;
     }

     /// Access Command mark 1 
     private int _CID1 =0;
     
     public int getCID1(){
    	 return this._CID1;
     }
     public void setCID1(int CID1){
    	 this._CID1 = CID1;
     }
    
     /// Access Command mark 2
     private int _CID2 =0;
     
     public int getCID2(){
    	 return this._CID2;
     }
     public void setCID2(int CID2){
    	 this._CID2 = CID2;
     }

     /// Access Return mark
     private int _RTN =0;
     
     public int getRTN(){
    	 return this._RTN;
     }
     public void setRTN(int RTN){
    	 this._RTN = RTN;
     }

     /// Access Data Count
     private int _Count =0;
     
     public int getCount(){
    	 return this._Count;
     }
     public void setCount(int Count){
    	 this._Count = Count;
     }

     /// Access Data Long
     private int _Long =0;
     
     public int getLong(){
    	 return this._Long;
     }
     public void setLong(int Long){
    	 this._Long = Long;
     }

     /// Access Data Length
     private int _Length =0;
     
     public int getLength(){
    	 return this._Length;
     }
     public void setLength(int Length){
    	 this._Length = Length;
     }

     /// Access byte data
     private byte[] _DataBytes =new byte[1];
     
     public byte[] getDataBytes(){
    	 return this._DataBytes;
     }
     public void setDataBytes(byte[] DataBytes){
    	 this._DataBytes = DataBytes;
     }

     /// Access string data
     public String getDataString(){
    	 return  Quote.ByteArrayToHexString(this._DataBytes);
     }
     public void setDataString(String DataString){
    	 this._DataBytes = Quote.HexStringToByteArray(DataString);
     }
     
     //返回session对象的特征 
     public String toString(){
    	 return this._Name;
     }
     
     public CommandInfo(){
    	 this._Name = "";
     }
     public CommandInfo(String name){
    	 this._Name = name;
     }
}
