package ADSDK.Communication;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

public class CommBase {

    
	private OutputStream os;
	private InputStream is;
	private CommPortIdentifier portId;
	private SerialPort sp;
    private Socket sk;

    private int _intDelay = 0;

    private int _TimeOut = 1000;
    
    public int getTimeOut(){
    	return this._TimeOut;
    }
    
    public void setTimeOut(int timeOut){
    	this._TimeOut = timeOut;
    }
    
    private boolean _isConnected = false;
    /**
    Gets Connect state as an <code>boolean</code>.
    @return Current Connect state.
    */
    public boolean IsConnected() {
	return _isConnected;
    }

	private String _ip = "172.16.2.20";
    /**
    Sets ip name.
    @param ip New port name.
    */
    public void setIP(String ip) {
	this._ip = ip;
    }
    /**
    Gets ip name.
    @return Current ip name.
    */
    public String getIP() {
	return this._ip;
    }
    
    private int _ipPort = 49152;
    /**
    Sets ip port name.
    @param ipport New port name.
    */
    public void setIPPort(int ipport) {
	this._ipPort = ipport;
    }
    /**
    Gets ipport name.
    @return Current ipport name.
    */
    public int getIPPort() {
	return this._ipPort;
    }
    
	private String _portName = "COM1";
    /**
    Sets port name.
    @param portName New port name.
    */
    public void setPortName(String portName) {
	this._portName = portName;
    }

    /**
    Gets port name.
    @return Current port name.
    */
    public String getPortName() {
	return this._portName;
    }

    private int _baudRate = 9600;
    /**
    Sets baud rate.
    @param baudRate New baud rate.
    */
    public void setBaudRate(int baudRate) {
	this._baudRate = baudRate;
    }

    /**
    Sets baud rate.
    @param baudRate New baud rate.
    */
    public void setBaudRate(String baudRate) {
	this._baudRate = Integer.parseInt(baudRate);
    }

    /**
    Gets baud rate as an <code>dint</code>.
    @return Current baud rate.
    */
    public int getBaudRate() {
	return this._baudRate;
    }

    /**
    Gets baud rate as a <code>String</code>.
    @return Current baud rate.
    */
    public String getBaudRateString() {
	return Integer.toString(this._baudRate);
    }

    private boolean _IsActive = false;
    /**
    Sets Mode
    */
    public void setMode(boolean isActive) {
    	this._IsActive = isActive;
    }
    
    private boolean _IsNet = false;
    /**
    Sets net communication.
    */
    public void setNet() {
    	if(this._IsNet)this.DisConnect();
    	this._IsNet = true;
    }
    /**
    Sets serialport communication.
    */
    public void setSerialPort() {
    	if(this._IsNet)this.DisConnect();
	this._IsNet = false;
    }
    
    private byte[] _rcvByte2 = new byte[1];
    /**
    Gets Receive data Buffer <code>byte[]</code>.
    @return Current baud rate.
    */
    public byte[] getReceiveData_Hex() {
	return this._rcvByte2;
    }
    /**
    Gets Receive data Buffer <code>String</code>.
    @return Current baud rate.
    */
    public String getReceiveData_String() {
	return this._rcvByte2.toString();
    }
    /**
    Gets Receive data Buffer <code>String</code>.
    @return Current baud rate.
    */
    public String getReceiveData_HexString() {
	return Quote.ByteArrayToHexString(this._rcvByte2);
    }
	public CommBase()
	{
		
	}
	public int Connect()
	{
		// Open the input and output streams for the connection. If they won't
		// open, close the port before throwing an exception.

		try 
		{
           // DisConnect();
			if(this._IsNet)
			{
				try {
					sk = new Socket(this._ip,this._ipPort);
					System.out.println("IP Address：" + this._ip+" IP Port：" + this._ipPort+" is connected!");
					
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
	    			System.err.println(e);
	                return Quote.FAIL;
				} catch (IOException e) {
	    			System.err.println(e);
	                return Quote.FAIL;
				}
				//Use InputStream in to read from the serial port, and OutputStream
				//out to write to the serial port.
				try
				{
					is = sk.getInputStream();
					os = sk.getOutputStream();
				} 
				catch (IOException e)
				{
	    			System.err.println(e);
					return Quote.FAIL;
				}
			}
			else
			{
				portId = CommPortIdentifier.getPortIdentifier(this._portName);
				
				if (portId.isCurrentlyOwned())
				{
					DisConnect();
					return Quote.FAIL;
				}
				
				try 
				{
					sp = (SerialPort)portId.open("Serial_Communication", 2000);
					System.out.println("ComPort：" + sp.getName()+" is opened!");
	
				}
				catch (PortInUseException e)
				{
	    			System.err.println(e);
					return Quote.FAIL;
				}
	
				//Initialize the communication parameters to 9600, 8, 1, none.
				try 
				{
					sp.setSerialPortParams(this._baudRate,
							SerialPort.DATABITS_8,
							SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE);
	
				}
				catch (UnsupportedCommOperationException e)
				{
	    			System.err.println(e);
					return Quote.FAIL;
				}
				
				//Use InputStream in to read from the serial port, and OutputStream
				//out to write to the serial port.
				try
				{
					is = sp.getInputStream();
					os = sp.getOutputStream();
				} 
				catch (IOException e)
				{
	    			System.err.println(e);
					return Quote.FAIL;
				}
			}
		} 
		catch (NoSuchPortException e)
		{
			System.err.println(e);
			return Quote.FAIL;
		}
		// Open the port represented by the CommPortIdentifier object. Give
		// the open call a relatively long timeout of 30 seconds to allow
		// a different application to relinquish the port if the user
		// wants to.
		// 打开需要的延迟，并获取commPort对象
		// 可以再打开sPort后多次得到发送与解析的数据

        _isConnected = true;
		return Quote.SUCCEED;
	}
	
	public int Connect(String portName,int baudRate)
	{
		if(this._IsNet)
		{
			this._ip = portName;
			this._ipPort = baudRate;
		}
		else
		{
			this._portName = portName;
			this._baudRate = baudRate;
		}
		return Connect();
	}
	
	/// <summary>
    /// disconnect
    /// </summary>
    /// <returns></returns>
    public int DisConnect()
    {
    	// If port is already closed just return.
        if (!_isConnected) return Quote.SUCCEED;
        if(this._IsNet)
        { 
        	if (sk != null) 
	        {
	    		try 
	    		{
	    			// close the i/o streams.
	    			os.close();
	    			is.close();
	    		} 
	    		catch (IOException e)
	    		{
	    			System.err.println(e);
	                return Quote.ERROR_DISCONNECT;
	    		}
		    	// Close the port.
	    		try {
					sk.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
	    			System.err.println(e);
	                return Quote.ERROR_DISCONNECT;
				}
		    	// Remove the ownership listener.
		    	// portId.removePortOwnershipListener(this);
		        _isConnected = false;
				System.out.println("Network has been broken!");
	        }
        }
        else
        {
	    	// Check to make sure sPort has reference to avoid a NPE.
	        if (sp != null) 
	        {
	    		try 
	    		{
	    			// close the i/o streams.
	    			os.close();
	    			is.close();
	    		} 
	    		catch (IOException e)
	    		{
	    			System.err.println(e);
	                return Quote.ERROR_DISCONNECT;
	    		}
		    	// Close the port.
		    	sp.close();
		    	// Remove the ownership listener.
		    	// portId.removePortOwnershipListener(this);
		        _isConnected = false;
				System.out.println("ComPort has been closed!");
	        }
        }
        if (!_isConnected) return Quote.SUCCEED;
        else return Quote.FAIL;
    }
    
    public int Communication(byte[] sndData, boolean flag)
    {
        _intDelay = 0;
        int FCount = Quote.FAIL;
        boolean commEnabled = _IsActive;
    	if(!_isConnected) return Quote.ERROR_CONNECT;
        _IsActive = false;
        try
        {
            _IsActive = false;
        	os.write(sndData, 0, sndData.length);
        	while (is.available() <= 0)
        	{
        		try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					System.err.println(e);
		            FCount = Quote.ERROR_RECIVE;
				}
        		_intDelay++;
        		if (_intDelay >= (_TimeOut / 10))
        		{
        			break;
        		}
        	}
        	try {
				if(flag){
					Thread.sleep(150);
				}else{
					Thread.sleep(50);
				}
					
			} catch (InterruptedException e) {
				System.err.println(e);
	            FCount = Quote.ERROR_RECIVE;
			}
        	if (is.available() > 0)
        	{
        		_rcvByte2 = new byte[is.available()];
                is.read(_rcvByte2, 0, _rcvByte2.length);

                FCount = Quote.SUCCEED;
            }
            else{
            	FCount = Quote.TIMEOUT_RECIVE;
            }
        }
        catch (IOException e) {
			System.err.println(e);
            FCount = Quote.ERROR_RECIVE;
        }
        _IsActive = commEnabled;
        return FCount;
    }  
    
    public int GetAutoData()
    {
    	if(!_IsActive) return Quote.FAIL;
    	_rcvByte2 = new byte[1];
    	if(!_isConnected) return Quote.ERROR_CONNECT;
    	
    	try {
			if (is.available() > 0)
			{
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				_rcvByte2 = new byte[is.available()];
			    is.read(_rcvByte2, 0, _rcvByte2.length);

		        return Quote.SUCCEED;
			}
			return Quote.FAIL;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println(e);
			return Quote.ERROR_RECIVE;
		}
    }
}
