package ADSDK.Communication;

/// <summary>
/// basic constants
/// </summary>
public class Quote
{
    /// <summary>
    /// Succeed
    /// </summary>
    public static final int SUCCEED = 0;
    /// <summary>
    /// Fails
    /// </summary>
    public static final int FAIL = 1;
    /// <summary>
    /// Connect Error
    /// </summary>
    public static final int ERROR_CONNECT = 201;
    /// <summary>
    /// Get data Error
    /// </summary>
    public static final int ERROR_GET = 202;
    /// <summary>
    /// Set data Error
    /// </summary>
    public static final int ERROR_SET = 203;
    /// <summary>
    /// Set data TimeOut
    /// </summary>
    public static final int TIMEOUT_SET = 204;
    /// <summary>
    /// Send data error
    /// </summary>
    public static final int ERROR_SEND = 205;
    /// <summary>
    /// Receive data error
    /// </summary>
    public static final int ERROR_RECIVE = 206;
    /// <summary>
    /// DisConnect Error
    /// </summary>
    public static final int ERROR_DISCONNECT = 207;
    /// <summary>
    ///  Send data TimeOut
    /// </summary>
    public static final int TIMEOUT_SEND = 208;
    /// <summary>
    /// Receive data TimeOut
    /// </summary>
    public static final int TIMEOUT_RECIVE = 209;
    
    public static final String getError(int error){
    	String rtnState = "";
    	switch(error)
    	{
    	case SUCCEED:
    		rtnState = "Succeed";
    		break;
    	case FAIL:
    		rtnState = "Fails";
    		break;
    	case ERROR_CONNECT:
    		rtnState = "Connect Error";
    		break;
    	case ERROR_GET:
    		rtnState = "Get data Error";
    		break;
    	case ERROR_SET:
    		rtnState = "Set data Error";
    		break;
    	case ERROR_SEND:
    		rtnState = "Set data TimeOut";
    		break;
    	case ERROR_RECIVE:
    		rtnState = " Receive data error";
    		break;
    	case ERROR_DISCONNECT:
    		rtnState = "DisConnect Error";
    		break;
    	case TIMEOUT_SEND:
    		rtnState = "Send data TimeOut";
    		break;
    	case TIMEOUT_RECIVE:
    		rtnState = "Receive data TimeOut";
    		break;
    	
    	}
    	return rtnState;
    }
    
    /**
	 * 这是将16进形式的字符串要转化为byte[],就是要以两位的形式读取
	 */
	public static final byte[] HexStringToByteArray(String s) {
  		byte[] bytes;
  		s = s.replace(" ", "");
  		bytes = new byte[s.length() / 2];
  		for (int i = 0; i < bytes.length; i++) {
  			bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2),
  					16);
  		}
  		return bytes;
  	} 
	/**
	 * 这是将16进形式的字符串要转化为byte[],就是要以两位的形式读取
	 */
	public static final byte[] HexStringToByteArray(String s,int start, int size) { 
  		byte[] bytes;
  		s = s.replace(" ", "");
  		bytes = new byte[s.length() / 2];
  		for (int i = start; i < start+size; i++) {
  			bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2),
  					16);
  		}
  		return bytes;
  	}
	/**
	 * 这是将16进形式的byte[]要转化为字符串,就是要以两位的形式读取
	 */
	public static final String ByteArrayToHexString(byte[] b) { 
 		String ret = ""; 
 		for (int i = 0; i < b.length; i++) { 
	 		String hex = Integer.toHexString(b[i] & 0xFF); 
	 		if (hex.length() == 1) { 
	 			hex = '0' + hex; 
	 		} 
	 		ret += hex.toUpperCase(); 
 		} 
 		return ret; 
 	} 
	
	/**
	 * 这是将16进形式的byte[]要转化为字符串,就是要以两位的形式读取
	 */
	public static final String ByteArrayToHexString(byte[] b,int start, int size) { 
 		String ret = ""; 
 		for (int i = start; i <start+size; i++) { 
	 		String hex = Integer.toHexString(b[i] & 0xFF); 
	 		if (hex.length() == 1) { 
	 			hex = '0' + hex; 
	 		} 
	 		ret += hex.toUpperCase(); 
 		} 
 		return ret; 
	} 
	
	/**
	 * 通讯命令数据校验和
	 */
    public static final int Checksum(byte[] buffer, int start, int size)
    {
        int inttemp = 0;
        for (int i = start; i < start + size; i++)
        {
            inttemp += buffer[i];
            if (inttemp >= 256) inttemp = inttemp - 256;
        }
        return inttemp == 0 ? 0 : 256 - inttemp;
    }
}
