/*
          The Data Logic foundation of MESSAGE
          referenced in the Mail Server
*/

package domain.jdbc.user;

import domain.jdbc.*;
import util.RowSet;

public class UserData extends DataManager
{
     public UserData()
     {

     }
     public String getUserName(int iUserCode) 
     {
          String QS = " Select MailUser.UserName From MailUser "+
                      " Where MailUser.UserCode = "+iUserCode;

          return getStringValue(QS);
     }
     public String getPassword(int iUserCode) 
     {
          String QS = " Select MailUser.Password From MailUser "+
                      " Where MailUser.UserCode = "+iUserCode;

          return getStringValue(QS);
     }
     public RowSet getRecipients(int iUserCode)
     {
          System.out.println("Inside recipients");
          String QS = " Select MailUser.UserName,MailUser.UserCode "+
                      " From MailUser Where MailUsser.UserCode <> "+iUserCode+
                      " Order By 2 ";

          return getRowSet(QS);
     }
     public void addUsers( String name, String pass)
     {

          insertNewUser(name,pass,getNextValue());

     }
     public RowSet getUsers()
     {
          
          String QS = "Select UserCode,UserName,Password from MailUser";
          return getRowSet(QS);
     }
}

