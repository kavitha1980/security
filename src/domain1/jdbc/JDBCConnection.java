/*
     A Singleton Class for getting the Connection
     object of a Database.

     Change this when you port in a different RDBMS
     environment

*/

package domain.jdbc;

import java.sql.*;

public class JDBCConnection
{
     
     static JDBCConnection connect = null;

	Connection theConnection = null;

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@oracle1:1521:hrd";
     String SUser     = "scott";
     String SPassword = "tiger";

     private JDBCConnection()
	{
		try
		{
               Class.forName(SDriver);
               theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCConnection : "+e);
		}
	}

     public static JDBCConnection getJDBCConnection()
	{
		if (connect == null)
		{
               connect = new JDBCConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

