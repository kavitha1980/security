package domain.jdbc.stores;

import java.sql.*;
import java.util.*;
import domain.jdbc.*;
                     
public class StoresData extends DataManager
{
     public StoresData()
     {
     }
     public Vector getInvName()
     {
          return setInvName();
     }
     public Vector getInvCode()
     {
          return setInvCode();
     }
     public int getNo(String QS)
     {
          return setNo(QS);
     }

     public void setUpdateStoresData(String no,String Columnname)
     {
          updateStoresData(no,Columnname);
     }
     public Vector getPendingNames(String QS)
     {
          return setStoresData(QS);
     }    
     public Vector getSupplierData(String QS)
     {
          return setStoresData(QS);
     }
     public Vector getListVector(String QS)
     {
          return setStoresData(QS);
     }
     public Vector getRDCData(String QS)
     {
          return setStoresData(QS);
     }
     public Vector getRDCOutData(String QS)
     {
          return setStoresData(QS);
     }
     
     public void insertGISDetails(String QS)
     {
          insertStoresDetails(QS);
     }

     public void insertGIDSDetails(String QS)
     {
          insertStoresDetails(QS);
     }

     public void insertRDCInData(String QS)
     {
          insertStoresDetails(QS);
     }
     public void updateRDC(String QS)
     {
          insertStoresDetails(QS);
     }
     


}
