/*
          The RMI server that implements all
          the business methods of the system
          as defined in the business logic foundation package.

          Where as it simply delegates the data logic
          to appropriate data objects

               SecurityData
*/

package domain;

import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;

import java.sql.*;

import util.RowSet;
import blf.Info;
import blf.VisitorIn;
import blf.Stores;
import blf.VehicleInfo;
import blf.RawInfo;

import domain.jdbc.info.*;
import domain.jdbc.visitorIn.*;
import domain.jdbc.stores.*;
import domain.jdbc.vehicleInfo.*;
import domain.jdbc.rawInfo.*;

import java.util.*;

public class SecurityServer extends UnicastRemoteObject implements VehicleInfo,Info,VisitorIn,Stores,RawInfo,rndi.CodedNames
{
     SecurityData    theData;
     VisitorData     VisData;
     StoresData stData;
     VehicleData vData;
     RawData     rData;

     public SecurityServer() throws RemoteException
     {
          theData    = new SecurityData();
          VisData     = new VisitorData();    
          stData     = new StoresData();
          vData      = new VehicleData();
          rData      = new RawData();    
          System.out.println("Security Server Starts...");
     }
     public void insertInwardData(RowSet rowset) throws RemoteException
     {
          rData.insertInwardData(rowset);
     }
     public int getNextId()
     {
          return VisData.getNextId();
     }

     public void updateInwardData(RowSet rowset,int p) throws RemoteException
     {
          rData.updateInwardData(rowset,p);
     }

     public int getCheckCardStatus(String CardNo)
     {
          return vData.getCheckCardStatus(CardNo);
     }
     public String getVisitorInitialCheck(String QS)
     {
          return VisData.getVisitorInitialCheck(QS);
     }
     public int getHodCode(int iDeptCode) throws RemoteException
     {
          return VisData.getHodCode(iDeptCode);
     }
     public String getHostId(int iDeptCode) throws RemoteException
     {
          return VisData.getHostId(iDeptCode);
     }

     public int getDeptCode(int iStaffCode)
     {
          return VisData.getDeptCode(iStaffCode);
     }
     public Vector sendMessageSource(int id,RowSet rowset)
     {
          return VisData.sendMessageSource(id,rowset);
     }
     public void setMessageText(String str,int id)
     {
          VisData.setMessageText(str,id);
     }

     public void sendMessageTarget(RowSet rowset)
     {
          VisData.sendMessageTarget(rowset);
     }
     public int getGuestInitialCheck(String Card) throws RemoteException
     {
          return theData.getGuestInitialCheck(Card);
     }
     public int getInterviewInitialCheck(String QS) throws RemoteException
     {
          return theData.getInterviewInitialCheck(QS);
     }
     public int getCheckCard(String Card,int date) throws RemoteException
     {
          return theData.getCheckCard(Card,date);
     }
     public int getGuestUpdateSlipNo(String Card) throws RemoteException
     {
          return theData.getGuestUpdateSlipNo(Card);
     }
     public int getVehicleCardNo(String VCard,int vdate) throws RemoteException
     {
          return vData.getVehicleCardNo(VCard,vdate);
     }
     public Vector getVehicleInInfo(String CardNo)
     {
          return vData.setVehicleInInfo(CardNo);
     }
     public Vector getVehicleModelInfo(int InDate)
     {
          return vData.getVehicleModelInfo(InDate);
     }
     public Vector getVehicleData()
     {
          return vData.getVehicleData();
     }
     public Vector getGuestName()
     {
          return theData.setGuestName();
     }
     public ResultSet getVehicleReport(String QS)
     {
          return vData.getVehicleReport(QS);
     }
     public ResultSet getVehicleFuelReport(String QS)
     {
          return vData.getVehicleFuelReport(QS);
     }
     public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime,int KDate,int KNo,int KQty)
     {
          vData.setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime,KDate,KNo,KQty);     
     }

     public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime)
     {
          vData.setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime);     
     }

     public void updateVehicleKm(String CardNo,int endkm)  throws RemoteException
     {
          vData.updateVehicleKm(CardNo,endkm);
     }   
     public Vector getKm(String CardNo)   throws RemoteException
     {
          return  vData.getKm(CardNo);
     }

     public Vector getSecurityInfo()   throws RemoteException
     {
          return vData.getSecurityInfo();
     } 
     public Vector getPlaces()   throws RemoteException
     {
          return vData.getPlaces();
     } 


     public Vector getFinanceYear()  throws RemoteException
     {
          return VisData.getFinanceYear();
     }

     public Vector getCurrentInfo(int Date) throws RemoteException
     {
          return vData.getCurrentInfo(Date);
     }
                 
     public Vector getCurrentInInfo(int Date) throws RemoteException
     {
          return vData.getCurrentInInfo(Date);
     }
                 
     public void insertData(Vector vect) throws RemoteException
     {
          vData.insertData(vect);
     }

     public void insertVData(Vector vect) throws RemoteException
     {
          vData.insertData(vect);
     }
     public Vector getDriverInfo() throws RemoteException
     {
          return vData.getDriverInfo();
     }
     public Vector getInfo(String CardNo,int i) throws RemoteException
     {
          return vData.getInfo(CardNo,i);
     }
                                     

     public Vector getPurpose() throws RemoteException
     {
          return vData.getPurpose();
     }

     public int getSlipNo() throws RemoteException
     {
          return theData.getSlipNo();
     }

     public int getInterviewCard(String sl) throws RemoteException
     {
          return theData.getInterviewCard(sl);
     }

     public Vector getCategory() throws RemoteException
     {
          return theData.getCategory();
     }
/*     public Vector getHostelCode() throws RemoteException
     {
          return theData.getHostelCode();
     }
*/
     public Vector getToMeetCode()
     {
          return theData.getToMeetCode();
     }
     public Vector getHod() throws RemoteException
     {
          return theData.getHod();
     }
     public Vector getGuestInitValues(int Date)
     {
          return theData.setGuestInitValues(Date);
     }
     public int getGuestSlipNo() throws   RemoteException
     {
          return theData.getGuestSlipNo();
     }
     public Vector getGuestNames()
     {
          return theData.getGuestNames();
     }
     public Vector getGuestInInitValues(int Date)
     {
          return theData.setGuestInInitValues(Date);
     }
     public Vector getGuestValues(String slno)
     {
          return theData.getGuestValues(slno);
     }
     public void setGuestUpdateTime(int slno,String SOutTime,int iOutDate,String InTime)
     {
          theData.setGuestUpdateTime(slno,SOutTime,iOutDate,InTime);
     }

     public void getGuestInsertData(String QS)
     {
          theData.getGuestInsertData(QS);
     }
                             

     public Vector getAgent() throws RemoteException
     {
          return theData.getAgent();
     }
     public Vector getQuali() throws RemoteException
     {
          return theData.getQuali();
     }
     public int getId() throws   RemoteException
     {
          return theData.getId();
     }
     public Vector getCategoryCode() throws RemoteException
     {
          return theData.getCategoryCode();
     }
     public Vector getAgentCode() throws RemoteException
     {
          return theData.getAgentCode();
     }
     public Vector getHodCode() throws RemoteException
     {
          return theData.getHodCode();
     }
     public Vector getQualiCode() throws RemoteException
     {
          return theData.getQualiCode();
     }

     public int setCode(int code , Vector vect)
     {
          return theData.setCode(code,vect);
     }
     public Vector initValues(int Date)
     {
          return theData.setInitValues(Date);
     }
     public void insertValues(Vector infoVector)
     {
          theData.insertValues(infoVector);
     }                   
     public Vector getNames()
     {
          return theData.getNames();
     }
     public Vector getEmployeeNames()
     {
          return theData.getEmployeeNames();
     }

     public Vector getValues(String slno)
     {
          return theData.getValues(slno);
     }
     public Vector getOutValues(int iDate)
     {
          return theData.getOutValues(iDate);
     }
     public Vector getReportVector(int iStDate,int iEndDate)
     {
          return theData.getReportVector(iStDate,iEndDate);
     }

     public void getUpdateTimeAndDate(String SOutTime,int iOutDate,int id,int iResult,String SHod)
     {
          theData.getUpdateTimeAndDate(SOutTime,iOutDate,id,iResult,SHod);
     }
     public int setId(String sl)
     {
          return theData.setId(sl);
     }
     public void insertCompanyName(String name,String Code)
     {
          VisData.insertCompanyName(name,Code);
     }
     public void insertVisitorPurpose(String name,String Scode)
     {
          VisData.insertVisitorPurpose(name,Scode);
     }
     public void insertDept(int Code,String name)
     {
          VisData.insertDept(Code,name);
     }

     public void insertRep(String name,String code)
     {
          VisData.insertRep(name,code);
     }
     public void insertStaff(int Code,String name)
     {
          VisData.insertStaff(Code,name);
     }
     public String getMaxCode()
     {
          return VisData.getMaxCode();
     }          
     public String getMaxDeptCode()
     {
          return VisData.getMaxDeptCode();
     }          

     public String getMaxPurposeCode()
     {
          return VisData.getMaxPurposeCode();
     }          
     public String getMaxRepCode()
     {
          return VisData.getMaxRepCode();
     }          
     public String getMaxStaffCode()
     {
          return VisData.getMaxStaffCode();
     }          
     public String getMaxCompanyCode()
     {
          return VisData.getMaxCompanyCode();
     }          

     public Vector getCompanyInfo()
     {
          return VisData.getCompanyInfo();
     }
     public Vector getPurposeInfo()
     {
          return VisData.getPurposeInfo();
     }
     public Vector getStaffInfo()
     {
          return VisData.getStaffInfo();
     }
     public Vector getDeptInfo()
     {
          return VisData.getDeptInfo();
     }

     public Vector getRepInfo(int code)
     {
          return VisData.getRepInfo(code);
     }
     public void saveData(String slipno,Vector vect)
     {
          VisData.saveData(slipno,vect);
     }
     public void saveErrectorData(String slipno,Vector vect)
     {
          VisData.saveErrectorData(slipno,vect);
     }

     public String getNextSlipNo(int iIndicator)
     {
          return VisData.getNextSlipNo(iIndicator);
     }
     public int getSlipNo(String CardNo)
     {
          return VisData.getSlipNo(CardNo);
     }                          
     public Vector getErrectorInfo(String SCompanyCode)
     {
          return VisData.setErrectorInfo(SCompanyCode);
     }

     public int getErrectorSlipNo(String CardNo)
     {
          return VisData.getErrectorSlipNo(CardNo);
     }                          
     public Vector getErrectorSlipNo(int iStDate,int iEndDate)
     {
          return VisData.getErrectorSlipNo(iStDate,iEndDate);
     }                          

     public int getSlNo(String Seq)
     {
          return VisData.getSlNo(Seq);
     }

     public Vector getVect(int slipno)
     {
          return VisData.getVect(slipno);
     }
     public Vector getErrectorVect(int slipno)
     {
          return VisData.getErrectorVect(slipno);
     }

     public Vector getErrectorAsOnReport(int EndDate)
     {
          return VisData.getErrectorAsOnReport(EndDate);
     }
     public Vector getVisitorReport(int StDate,int EndDate)
     {
          return VisData.getVisitorReport(StDate,EndDate);
     }
     public Vector getVisitorReport(int iSlipNo,int StDate,int EndDate)
     {
          return VisData.getVisitorReport(iSlipNo,StDate,EndDate);
     }
     public Vector getVisitorSlipNo(int StDate,int EndDate)
     {
          return VisData.getVisitorSlipNo(StDate,EndDate);
     }

     public Vector getVisitorAbstractReport(int StDate,int EndDate,int CompanyCode,int CCode)
     {
          return VisData.getVisitorAbstractReport(StDate,EndDate,CompanyCode,CCode);
     }
     public Vector getVisitorInInfo(int StDate,int EndDate)
     {
          return VisData.getVisitorInInfo(StDate,EndDate);
     }
     public Vector getVisitorOutInfo(int StDate,int EndDate)
     {
          return VisData.getVisitorOutInfo(StDate,EndDate);
     }
     public Vector getErrectorInInfo(int StDate,int EndDate) 
     {
          return VisData.getErrectorInInfo(StDate,EndDate);
     }
     public Vector getErrectorOutInfo(int StDate,int EndDate)
     {
          return VisData.getErrectorOutInfo(StDate,EndDate);
     }
     public Vector getErrectorWiseReport(int StDate,int EndDate,int iErrectorCode)
     {
          return VisData.getErrectorWiseReport(StDate,EndDate,iErrectorCode);
     }

     public Vector getVisitorCompanyInfo()
     {
          return VisData.getVisitorCompanyInfo();
     }
     public Vector getVisitorRepInfo(int CCode)
     {
          return VisData.getVisitorRepInfo(CCode);
     }

     public Vector getErrectorReport(int StDate,int EndDate)
     {
          return VisData.getErrectorReport(StDate,EndDate);
     }
     public Vector getErrectorReport(int iSlipNo,int StDate,int EndDate)
     {
          return VisData.getErrectorReport(iSlipNo,StDate,EndDate);
     }

     public Vector getInvName()
     {
         return stData.getInvName();
     }
     public Vector getInvCode()
     {
         return stData.getInvCode();
     }
     
     public Vector getPendingNames(String QS)
     {
          return stData.getPendingNames(QS);
     }
     public Vector getSupplierData(String QS)
     {
          return stData.getSupplierData(QS);
     }
     public Vector getListVector(String QS)
     {
          return stData.getListVector(QS);
     }
     public Vector getRDCData(String QS)
     {
          return stData.getRDCData(QS);
     }
     public Vector getRDCOutData(String QS)
     {
          return stData.getRDCOutData(QS);
     }
     
     public void insertGISDetails(String QS)
     {
          stData.insertGISDetails(QS);
     }

     public void insertGIDSDetails(String QS)
     {
          stData.insertGIDSDetails(QS);
     }

     public void insertRDCInData(String QS)
     {
          stData.insertRDCInData(QS);
     }
     public void updateRDC(String QS)
     {
          stData.updateRDC(QS);
     }
     public void setUpdateStoresData(String no,String column)
     {
          stData.setUpdateStoresData(no,column);
     }

     public int getNo(String QS)
     {
          return stData.getNo(QS);
     }

     public void setOutTime(String STime,int RepCode)
     {
          VisData.setOutTime(STime,RepCode);
     }
     public void setErrectorOutTime(String STime,int RepCode)
     {
          VisData.setErrectorOutTime(STime,RepCode);
     }

     public static void main(String arg[])
     {
          try
          {
               SecurityServer theSecurityServer = new SecurityServer();
               Registry theRegistry = LocateRegistry.createRegistry(RMIPORT);
               theRegistry.rebind(SECURITYDOMAIN,theSecurityServer);
          }
          catch(Exception ex)
          {
               System.out.println("SecurityServer : "+ex);
          }
     }
}

