package domain.jdbc.info;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

public class SecurityData extends DataManager
{
     public SecurityData()
     {

     }
     public int getInterviewCard(String sl)
     {
          return setInterviewCard(sl);
     }
     public int getGuestUpdateSlipNo(String sl)
     {
          return setGuestUpdateSlipNo(sl);
     }
     public int getCheckCard(String cardNo,int iDate)
     {
          return setCheckCard(cardNo,iDate);
     }   

     public int getInterviewInitialCheck(String QS)
     {
          return setInitialCheck(QS);
     }
     public int getGuestInitialCheck(String card)
     {
          return setInitialCheck(card);
     }

     public int getSlipNo()
     {
          //String QS = "select Slip_seq.nextVal from Dual";
          String QS  = "Select Max(SlipNo) from InterviewNames";
          return setSlip(QS);
     }

     public Vector getGuestName()
     {
          return setGuestName();          
     }
     public Vector getCategory()
     {
          String QS = " select  CategoryName from Category Union All select DesignationName from Designation ";
          return  getVect(QS);
     }
/*     public Vector getHostelCode()
     {
          String QS = " select HostelCode from Hostel ";
          return  getCode(QS);
     }
*/
/*     public Vector getToMeetCode()
     {
          String QS =    "select SchemeApprentice.EmpName,"+
                         "SchemeApprentice.FatherName,"+
                         "SchemeApprentice.MotherName,"+
                         "SchemeApprentice.Address4,"+
                         "SchemeApprentice.DeptCode,"+
                         "hostel.hostelname,"+
                         "from SchemeApprentice"+
                         "inner join Hostel on"+
                         "SchemeApprentice.hostelcode= hostel.hostelcode"+
                         "UNION ALL"+
                         "Select ContractApprentice.EmpName,"+
                         "ContractApprentice.FatherName,"+
                         "ContractApprentice.MotherName,"+
                         "ContractApprentice.Address4,"+
                         "ContractApprentice.DeptCode,"+
                         "Hostel.hostelname,"+
                         "from ContractApprentice"+
                         "inner join Hostel on"+
                         "ContractApprentice.hostelcode = hostel.hostelcode";
                         
          return getToMeet(QS);
     }
*/
     public Vector getToMeetCode()
     {
          String QS =" select schemeapprentice.empname,schemeapprentice.fathername,"+
                     " schemeapprentice.mothername,schemeapprentice.address4,"+
                     " schemeapprentice.deptcode,hostel.hostelname,SchemeApprentice.HRDTicketNo"+
                     " from schemeapprentice inner join hostel on"+
                     " schemeapprentice.hostelcode=hostel.hostelcode"+
                     " union all "+
                     " Select contractapprentice.empname,"+
                     " contractapprentice.fathername,"+
                     " contractapprentice.mothername,contractapprentice.address4,"+
                     " Contractapprentice.deptcode,hostel.hostelname,ContractApprentice.HRdTicketNo"+
                     " from contractapprentice inner join hostel on "+
                     " contractapprentice.hostelcode=hostel.hostelcode order by 1 ";
		return getToMeet(QS);
     }

     public Vector getHodCode()
     {
          String QS = " select  DeptCode from Department ";
          return  getCode(QS);
     }

     public Vector getAgent()
     {
          String QS = " select  BrokerName from Broker ";
          return  getVect(QS);
     }

     public Vector getHod()
     {
          String QS = " select DeptName from Department ";
          return  getVect(QS);
     }
     public Vector getQuali()
     {
          String QS = " select  EducationName from Education ";
          return  getVect(QS);
     }
     public Vector getCategoryCode()
     {
          String QS = " select  CategoryCode from Category ";
          return  getCode(QS);
     }

     public Vector getAgentCode()
     {
          String QS = " select  BrokerCode from Broker ";
          return  getCode(QS);
     }

     public Vector getQualiCode()
     {
          String QS = " select  EducationCode from Education ";
          return  getCode(QS);
     }


     public int getId()
     {
          String  QS = "Select interview_seq.nextval from Dual";
          return getNextId(QS);
     }
     public int setCode(int code , Vector vect)
     {
          return setComboCode(code,vect);
     }
     public Vector initValues(int Date)
     {
          return setInitValues(Date);
     }          
     public Vector getNames()
     {
          return getName();
     }
     public Vector getEmployeeNames()
     {
          return getEmpName();
     }




     public Vector getValues(String slno)
     {
          return setData(slno);
     }
     public Vector getOutValues(int IDate)
     {
          return setOutValues(IDate);
     }
     public Vector getReportVector(int iStDate ,int iEndDate)
     {
          return setReportVector(iStDate,iEndDate);
     }
     public int setId(String sl)
     {
          return getId(sl);
     }
     public void getUpdateTimeAndDate(String SOutTime,int iOutDate,int Iid,int iResult,String SHod)
     {
          setUpdateTimeAndDate(SOutTime,iOutDate,Iid,iResult,SHod);
     }
      

     public Vector getGuestNames()
     {
          return setGuestNames();
     }
     public Vector getGuestInitValues(int Date)
     {
          return setGuestInitValues(Date);
     }          
     public Vector getGuestValues(String slno)
     {
          return setGuestData1(slno);
     }
     public void setGuestUpdateTime(int slno,String SOutTime,int iOutDate,String InTime)
     {
          GuestUpdateTime(slno,SOutTime,iOutDate,InTime);
     }
     public int getGuestSlipNo()
     {
          String QS = "Select Max(slipno) from GuestTable";
          return setGuestSlipNo(QS);
     }
     public Vector getGuestInInitValues(int Date)
     {
          return setGuestInInitValues(Date);
     }          
     public void getGuestInsertData(String QS)
     {
          setGuestInsertData(QS);
     }   


     public void insertValues(Vector vect)
     {
          insertDetails(vect);
     }
}

