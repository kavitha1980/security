


/*
     The Data Manager manages data that is
     typically required by AJSM in longterm

     This architecture completely decouple
     the underlying storage from the Server;
*/

package domain.jdbc;

import java.sql.*;
import java.util.Vector;
import util.RowSet;
import util.Common;

public abstract class DataManager
{

     Connection theConnection=null;
     Statement theStatement=null;
     CallableStatement theCall=null;

     Common common  = new Common();
     protected DataManager()
     {

     }
     public Vector setVehicleInInfo(String CardNo)
     {
          Vector vehicleInVector     = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = "Select VehicleNo,VehicleName,DriverName,Purpose,SecurityName,Place ,OutTime from Vehicles where CardNo= '"+CardNo+"'  And Status=0 And VCheckCard=1 ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                         for(int i=0;i<7;i++)
                              vehicleInVector.addElement(result.getString(i+1));
                    }
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return vehicleInVector;
     }
     public Vector setPlaces()
     {
          Vector VPlace     = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = "Select Place from VehiclePlaces ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                              VPlace.addElement(result.getString(1));
                    }
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return VPlace;
     }


     public int setVehicleCardNo(String CardNo,int iDate)
     {
          int Vcard=0;
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select VCheckCard from Vehicles where cardNo = '"+CardNo+"' And Status=0 ";
                    //System.out.println(QS);
                    ResultSet  result   = st.executeQuery(QS);
                    while(result.next())
                    {
                         String SVCard   = result.getString(1);
                         Vcard           = Integer.parseInt(SVCard);
                    }
               

          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setVehicleCardNo) : "+ex);
          }

          return Vcard;
     }
     public int setCheckCard(String CardNo,int iDate)
     {
          int card=-1;
          
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select CheckCard from GuestTable where CardNo = '"+CardNo+"' And Status=0 ";
                    //System.out.println(QS);
                    ResultSet  result   = st.executeQuery(QS);
                    while(result.next())
                    {
                         String SCard   = result.getString(1);
                         card           = Integer.parseInt(SCard);
                    }
                    result.close();               
	  st.close();
	  theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setGuestInit) : "+ex);
          }

          //System.out.println("Card:"+card);
          return card;

     }
     public int setInitialCheck(String QS)
     {
          int card=-1;
          
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    //String QS           = " Select CheckCard from GuestTable where CardNo = '"+CardNo+"' And Status=0 ";
                    //System.out.println(QS);
                    ResultSet  result   = st.executeQuery(QS);
                    while(result.next())
                    {
                         String SCard   = result.getString(1);
                         card           = Integer.parseInt(SCard);
                    }
       	  result.close();        
	  st.close();
	  theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setGuestInit) : "+ex);
          }

          //System.out.println("Card:"+card);
          return card;

     }


     public void setVehicleUpdate(String VCardNo,String SInTime,int IInDate,int SEndKm,String SOutTime)
     {
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();

               String QS           = "Update Vehicles set InTime ='"+SInTime+"',InDate="+IInDate+",EndKm="+SEndKm+",Status= 1,VCheckCard=0 where CardNo = '"+VCardNo+"' And  Status=0 And VCheckCard=1 And OutTime='"+SOutTime+"' ";
               st.executeUpdate(QS);                   
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

    }

      protected int setInterviewCard(String slno)
      {
          int card=0,status=0,ok=0;
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        = conn.createStatement();
               String QS           = " Select CheckCard,Status from InterviewNames where SlipNo='"+slno+"' and Status=0 and CheckCard=0 ";
               ResultSet rs        = st.executeQuery(QS);
               int i=0;
               while(rs.next())
               {
                    
                    String SCard   = rs.getString(1);
                    String SStatus = rs.getString(2);

                    card           = Integer.parseInt(SCard);
                    status         = Integer.parseInt(SStatus);
                    //System.out.println("i"+i);
                    i=i+1;

               }
               conn.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
               
          }
          //System.out.println("Card:"+card);
          //System.out.println("status"+status);

          if((card==0)&&(status==0))
               ok=1;

          //System.out.println("Ok"+ok);
          return ok;
     }

      protected Vector setGuestNames()
     {
          Vector GuestnameVector = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    String QS           = "Select GuestName from GuestTable where status=0 ";
                    Statement st        = theConnection.createStatement();
                    ResultSet result    = st.executeQuery(QS);
                    GuestnameVector.addElement(String.valueOf(" "));
                    while(result.next())
                    {
                         GuestnameVector.addElement(result.getString(1));
                    }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return GuestnameVector;
     }

      protected void setGuestInsertData(String QS)
     {
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    st.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public Vector setGuestInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select CardNo,GuestName,InTime,OutTime,GuestPlace,Department,ToMeet from GuestTable where InDate="+inDate+" And Status= 1 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                         vect.addElement(result.getString(5));
                         vect.addElement(result.getString(6));
                         vect.addElement(result.getString(7));
                    }                             
               
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setGuestInit) : "+ex);
          }

          return vect;

     }

     protected Vector setGuestData1(String slno)
     {
          Vector dataVector   = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select GuestName,Sex,GuestPlace,NoPersons,ToMeet,Category,Department,InTime from GuestTable where CardNo = '"+slno+"' And Status=0 And CheckCard=0 " ;
                    //System.out.println("InTime:"+QS);

                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())                                           
                    {
                         //System.out.println("DM:SetGuestData"+result.getString(6));
                         for(int i=0;i<8;i++)
                              dataVector.addElement(result.getString(i+1));
                    }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          //System.out.println("Size:"+dataVector.size());
          return dataVector;
     }


     public void GuestUpdateTime(int slno,String SOutTime,int IOutDate,String Card)
     {
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               //System.out.println("Guest in DM:"+slno);
               String QS           = "Update GuestTable set OutTime ='"+SOutTime+"',OutDate="+IOutDate+",Status= 1,CheckCard=1 where CardNo = '"+Card+"' And Status=0 And CheckCard=0 ";
               st.executeUpdate(QS);                   
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }          

     public int setGuestUpdateSlipNo(String SlipNo)
     {
          int iSlip=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();

               String QS           = "Select SlipNo from GuestTable where CardNo='"+SlipNo+"' and Status=0 and CheckCard=0";
               //System.out.println(QS);
               ResultSet rs=st.executeQuery(QS);
               while(rs.next())
               {
                    String sSlip = rs.getString(1);
                    iSlip = Integer.parseInt(sSlip);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          //System.out.println(iSlip);
          return iSlip;
     }          


     public Vector outValues(int iDate)
     {
          Vector vect = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select Name,Sex,InTime,OutTime,Place,AgentName,CategoryName,HodName from InterviewNames where OutDate="+iDate+" And Status =1 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                         vect.addElement(result.getString(5));
                         vect.addElement(result.getString(6));
                         vect.addElement(result.getString(7));
                         vect.addElement(result.getString(8));
                    }
               
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet"+ex);
          }

          return vect;

     }


     public int setGuestSlipNo(String QS)
     {
          int id=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet  result   = st.executeQuery(QS);                   
               while(result.next())
               {
                    String Sid     = (String)result.getString(1);
                    id             = Integer.parseInt(Sid);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return id;
     }          



     protected Vector getVect(String QS)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getObject() : "+ex.getMessage());
          }
          return vect;
     }


     protected Vector getToMeet(String QS)
     {
          Vector VFatherName  = new Vector();
          Vector VMotherName  = new Vector();
          Vector VAddress     = new Vector();
          Vector ToMeetVector = new Vector();
          Vector VDeptCode    = new Vector();
          Vector VTotal       = new Vector();
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet result    = st.executeQuery(QS);
               ToMeetVector.addElement(String.valueOf(" "));
               while(result.next())
               {
                    ToMeetVector.addElement(result.getString(1));
                    VFatherName.addElement(result.getString(2));
                    VMotherName.addElement(result.getString(3));
                    VAddress.addElement(result.getString(4));
                    VDeptCode.addElement(result.getString(5));
               }
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          VTotal.addElement(ToMeetVector);
          VTotal.addElement(VFatherName);
          VTotal.addElement(VMotherName);
          VTotal.addElement(VAddress);
          VTotal.addElement(VDeptCode);

          return VTotal;
     }
     protected int getNextId(String QS)
     {
          int id=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                 String   Sid = (String)result.getString(1);
                 id           = Integer.parseInt(Sid);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getObject() : "+ex.getMessage());
          }
          
          return id;

     }



     public Vector setGuestInInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select CardNo,GuestName,Sex,InTime,GuestPlace,Department,ToMeet from GuestTable where InDate="+inDate+" and Status=0 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())                                                                                                                        
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                         vect.addElement(result.getString(5));
                         vect.addElement(result.getString(6));
                         vect.addElement(result.getString(7));
                    }                             
               
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet"+ex);
          }

          return vect;

     }


	
     protected Vector setVisitorReport(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,Staff.EmpName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,Staff,Visitor"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         " Visitor.StaffCode   = Staff.EmpCode AND"+
                         " Visitor.Out         = '1' And "+ 
                        "  Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" ";

               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getObject() : "+ex.getMessage());
          }
          return vect;
     }

     protected String setVisitorInitialCheck(String QS)
     {
          String SReturn =null;
          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SReturn = result.getString(1);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SReturn;
     }
               
     protected Vector getCode(String QS)
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getObject() : "+ex.getMessage());
          }
          return vect;
     }
     protected Vector getName()
     {
          Vector nameVector = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    String QS           = "Select Name from InterviewNames where status=0 ";
                    Statement st        = theConnection.createStatement();
                    ResultSet result    = st.executeQuery(QS);
                    nameVector.addElement(String.valueOf(" "));
                    while(result.next())
                    {
                         nameVector.addElement(result.getString(1));
                    }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return nameVector;
     }
     protected Vector getEmpName()
     {
          Vector nameVector = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    String QS           = "Select EmpName from SchemeApprentice                            ";
                    Statement st        = theConnection.createStatement();
                    ResultSet result    = st.executeQuery(QS);
                    nameVector.addElement(String.valueOf(" "));
                    while(result.next())
                    {
                         nameVector.addElement(result.getString(1));
                    }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return nameVector;
     }
     

     protected int setSlNo(String QS)
     {
          int id=0;
          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                 String   Sid = (String)result.getString(1);
                 id           = Integer.parseInt(Sid);
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getObject() : "+ex.getMessage());
          }
          
          return id;

     }

     protected int setComboCode(int index,Vector vect)
     {

          String Scode   = (String)vect.elementAt(index);
          int    iCode   = Integer.parseInt(Scode);
          return iCode;
     }
     protected Vector setReportVector(int iStDate,int iEndDate)
     {
          Vector dataVector   = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" And Status= 1 ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                     {
                         for(int i=0;i<9;i++)
                              dataVector.addElement(result.getString(i+1));
                    }
                    
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }


     protected Vector setData(String slno)
     {
          Vector dataVector   = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select InDate,Name,Age,Sex,Quali,Place,InTime,CategoryName,HodName,AgentName from InterviewNames where SlipNo='"+slno+"' and Status=0 and CheckCard=0 ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())                                           
                    {
                         for(int i=0;i<10;i++)
                              dataVector.addElement(result.getString(i+1));
                    }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }


     protected void insertDetails(Vector infoVector)
     {
          //theCall = theConnection.prepareCall(getCallSQS(procedure,VRows.size()));
          
          String SDate        = (String)infoVector.elementAt(0);
          int iInDate         = Integer.parseInt(SDate);
          String SName        = (String)infoVector.elementAt(1);
          String Sage         = (String)infoVector.elementAt(2);
          int iAge            = Integer.parseInt(Sage);
          String SSex         = (String)infoVector.elementAt(3);
          String SQuali       = (String)infoVector.elementAt(4);
          String SInTime      = (String)infoVector.elementAt(5);
          String SCategory    = (String)infoVector.elementAt(6);
          String SHod         = (String) infoVector.elementAt(7);
          String SAgent       = (String) infoVector.elementAt(8);
          String SOutTime     = (String)infoVector.elementAt(9);
          String SId          = (String)infoVector.elementAt(10);
          String SOutDate     = (String)infoVector.elementAt(11);
          String SPlace       = (String)infoVector.elementAt(12);
          String slNo         = (String)infoVector.elementAt(14);
          int iId             =   Integer.parseInt(SId);
          int iOutDate        = Integer.parseInt(SOutDate);
          
          String QS =  " insert into interviewNames(InDate,Name,Age,Sex,Quali,InTime,CategoryName,HodName,AgentName,OutTime,Id,OutDate,Place,Status,slipno,CheckCard) values("+iInDate+",'"+SName+"',"+iAge+",'"+SSex+"','"+SQuali+"','"+SInTime+"','"+SCategory+"','"+SHod+"','"+SAgent+"','"+SOutTime+"',"+iId+","+iOutDate+",'"+SPlace+"',0,'"+slNo+"',0) ";
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    st.executeUpdate(QS);
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet"+ex);
          }

     }
     public int getId(String sl)
     {
          int id=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = "Select Id from InterviewNames where SlipNo='"+sl+"' And Status=0 And CheckCard=0 ";
               ResultSet  result   = st.executeQuery(QS);                   
               while(result.next())
               {
                    String Sid     = (String)result.getString(1);
                    id             = Integer.parseInt(Sid);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return id;
     }          
     public void  insertName(String QS)
     {
          int id=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               //String QS           = "Select Id from InterviewNames where Name = '"+SName+"' And InTime ='"+SinTime+"' ";
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }          
     public String  setMaxCode(String QS)
     {
          String SCode="";
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                  SCode     = (String)rs.getString(1);
                  //code = Integer.parseInt(SCode);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SCode;
     }
     public Vector setInfo(String QS)
     {
          Vector vect = new Vector();
          int code=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }
     public String  setNextSlipNo()
     {
          String SlipNo="";
          int iSlip=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = " Select  Max(SlipNo) from Visitor ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    SlipNo = rs.getString(1);
                    //System.out.println("Slipno:"+SlipNo);

                    if(SlipNo == null)
                    {
                         iSlip=0;     
                    }
                    else
                    {
                         iSlip = Integer.parseInt(SlipNo);
                    }
                    //System.out.println("Slipno"+iSlip);
                    iSlip = iSlip+1;
                    SlipNo = Integer.toString(iSlip);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SlipNo;
     }
     public int setSlip(String QS)
     {
          int slNo=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SSlNo   = (String)rs.getString(1);
                    slNo           = Integer.parseInt(SSlNo);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return slNo;
     }

     public int  setSlipNo(String iCardNo)
     {
          int cardNo=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = "Select  SlipNo from Visitor where Out=0 and  CardNo='"+iCardNo+"' ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCardNo = (String)rs.getString(1);
                    cardNo         = Integer.parseInt(SCardNo);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return cardNo;
     }
     public void updateOutTime(String STime,int repCode)
     {
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = " Update Visitor set OutTime='"+STime+"',Out='1' where RepCode="+repCode+" And Out='0' ";
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public Vector setVect(int iSlipNo)
     {
          Vector vect = new Vector();
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = "Select Visitor.slipno,Company.Name,Representative.Name,VisitorPurpose.Name,"+
                                     " Staff.EmpName,Visitor.cardno,Visitor.intime,Visitor.repcode"+
                                     " from"+
                                     " Company,Representative,VisitorPurpose,Staff,Visitor"+
                                     " where"+
                                     " Visitor.CompanyCode =   Company.code and"+
                                     " Visitor.repcode     =   Representative.code and"+
                                     " Visitor.purposecode =   VisitorPurpose.Code and"+
                                     " Visitor.staffcode   =   Staff.empcode and"+
                                     " Visitor.out         =   '0' and"+
                                     " Visitor.slipno      =   "+iSlipNo+" ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
                    vect.addElement(rs.getString(7));
                    vect.addElement(rs.getString(8));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }
     public void  storeData(String slipno,Vector vect)
     {
          int code=0;
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String  QS          = " Insert into Visitor(slno,SlipNo,CompanyCode,RepCode,PurposeCode,StaffCode,CardNo,InTime,VisitorDate,Out,OutTime) Values("+vect.elementAt(0)+","+vect.elementAt(1)+","+vect.elementAt(2)+","+vect.elementAt(3)+","+vect.elementAt(4)+","+vect.elementAt(5)+",'"+vect.elementAt(6)+"','"+vect.elementAt(7)+"',"+vect.elementAt(8)+",'0','0')";
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void  updateStoresData(String AutoNo,String Column)
     {
          try     
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement st        = conn.createStatement();
               String QS           = "Update book set "+Column+" = '"+AutoNo+"' ";
               //System.out.println(QS);
               st.executeUpdate(QS);                            
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public void updateTimeAndDate(String SOutTime,int iOutDate,int id)
     {
          try
          {
               JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
               theConnection       = jdbc.getConnection();
               Statement st        = theConnection.createStatement();
               String QS           = " Update InterviewNames set OutTime ='"+SOutTime+"',OutDate="+iOutDate+",Status= 1,CheckCard=1 where Id = "+id+" And Status=0 And CheckCard=0 ";
               st.executeUpdate(QS);                   
          }
          catch(Exception e)                                                                                                                                                       
          {
               e.printStackTrace();
          }
     }          

     public Vector setInvName()
     {
          Vector VRows        = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement  st       = conn.createStatement();
               ResultSet  rs       = st.executeQuery("Select Item_Name from InvItems order by Item_Name ");
               while(rs.next())
               {
                    VRows.addElement(rs.getString(1));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     return VRows;
     }
     public Vector setInvCode()
     {
          Vector VRows        = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement  st       = conn.createStatement();
               ResultSet  rs       = st.executeQuery("Select Item_Code from InvItems order by Item_Name ");
               while(rs.next())
               {
                    VRows.addElement(rs.getString(1));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     return VRows;
     }

     public int setNo(String QS)
     {
          int no=0;
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement  st       = conn.createStatement();
               ResultSet  rs       = st.executeQuery(QS);
               while(rs.next())
               {
                   String sno      = rs.getString(1);
                   no = Integer.parseInt(sno);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     return no;
     }

     public Vector setStoresData(String QS)
     {
          Vector VRows        = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement  st       = conn.createStatement();
               ResultSet  rs       = st.executeQuery(QS);
               ResultSetMetaData rsmd = rs.getMetaData();
               int iCount          = rsmd.getColumnCount();
               while(rs.next())
               {
                    for(int i=0;i<iCount;i++)
                    {
                         VRows.addElement(rs.getString(i+1));
                    }
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     return VRows;
     }
     public void insertStoresDetails(String QS)
     {
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement  st       = conn.createStatement();
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     

     public Vector setInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select Name,Sex,InTime,Place,AgentName,CategoryName,HodName from InterviewNames where InDate="+inDate+" And status=0 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                         vect.addElement(result.getString(5));
                         vect.addElement(result.getString(6));
                         vect.addElement(result.getString(7));
                    }                             
               
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet"+ex);
          }

          return vect;

     }
     public void updateKm(String CardNo,int endKm)
     {
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st    =       conn.createStatement();
               Statement st1   =       conn.createStatement(); 
               String QS       =       "Update VehicleKm set endkm="+endKm+" where cardNo= '"+CardNo+"' ";
               //System.out.println("QS:"+QS);
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
                e.printStackTrace();
          }
          

     }
     public void insertVehicleValues(Vector vect)
     {
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st    =       conn.createStatement();
               Statement st1   =       conn.createStatement(); 
               String QS       =       "Insert into Vehicles(Cardno,VehicleNo,VehicleName,OutTime,Stkm,EndKm,place,Purpose,OutDate,InDate,"+
                                        "InTime,DIno,Kino,Didate,kidate,Dqty,kqty,DriverName,SecurityName,Traveller,Status,VCheckCard,BunkName)"+
                                        "Values('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"',"+vect.elementAt(4)+", "+
                                        " "+vect.elementAt(5)+",'"+vect.elementAt(6)+"', '"+vect.elementAt(7)+"',"+vect.elementAt(8)+","+vect.elementAt(9)+", "+
                                        " '"+vect.elementAt(10)+"','"+vect.elementAt(11)+"','"+vect.elementAt(12)+"',"+vect.elementAt(13)+","+vect.elementAt(14)+", "+
                                        " "+vect.elementAt(15)+","+vect.elementAt(16)+",'"+vect.elementAt(17)+"','"+vect.elementAt(18)+"','"+vect.elementAt(19)+"',0,1,'"+vect.elementAt(21)+"') ";

               
               st.executeUpdate(QS);
          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

     }        
     public Vector setCurrentInfo(int Date)
     {
          Vector vect = new Vector();
          try
          {                               
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select VehicleName,VehicleNo,OutTime,Purpose from Vehicles where OutDate="+Date+" and Status=0 ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));

               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setDriverInfo()
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select Name  from VehicleDrivers order by 1";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }
     public Vector setSecurityInfo()
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select Name  from SecurityNames";
               ResultSet rs        = st.executeQuery(QS);
               vect.addElement(" ");
               while(rs.next())
               {                    
                    vect.addElement(rs.getString(1));
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setPurpose()
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select Purpose  from VehiclePurpose";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setKm(String card)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select StKm,EndKm from VehicleKm where CardNo = '"+card+"' ";
               ResultSet rs        = st.executeQuery(QS);
               //System.out.println(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setInfo(String card,int i)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           = "Select VehicleRegNo,VehicleName from VehicleInfo  where CardNo ='"+card+"'  ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }    
     public int setCheckCardStatus(String CardNo)
     {
          int card=-1;

          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement st        =       conn.createStatement();
               String QS           =      "Select  Status from Vehicles  where CardNo ='"+CardNo+"' And VCheckCard=1 And Status=0 ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCard = (String)rs.getString(1);
                    card         = Integer.parseInt(SCard);
               }

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return card;
     }

     public Vector setFinanceYear()
     {
          Vector vect = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","inventory0405","stores0405");
               Statement st        = conn.createStatement();
               String QS           = "Select StartDate,EndDate from Config";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SS      = rs.getString(1);
                    String SE      = rs.getString(2);
                    vect.addElement(SS);
                    vect.addElement(SE);
               }

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }
     protected Vector appendRowSet(RowSet rowSet,String STable,String SSequence,String SPKColumn)
     {
          Vector vect    = new Vector();
          Vector VColumn = rowSet.getColumnName();
          Vector VRows   = rowSet.getRows();
          Vector VType   = rowSet.getColumnType();

          String QS   = prepareInsertQS(VColumn,STable,SPKColumn);
          String SQS  = "Select "+SSequence+".nextval from dual";

          try
          {
               RJDBCConnection jdbc  = RJDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               PreparedStatement pstmt   = theConnection.prepareStatement(QS);
               theStatement              = theConnection.createStatement();

               for(int i=0;i<VRows.size();i++)
               {
                    int iValue = 0;
                    ResultSet result = theStatement.executeQuery(SQS);
                    if(result.next())
                    {
                         iValue=result.getInt(1);
                         vect.addElement(String.valueOf(iValue));
                    }
                    result.close();
                    pstmt.setInt(1,iValue);
                    Vector aRow = (Vector)VRows.elementAt(i);
                    for(int j=0;j<VColumn.size();j++)
                    {
                         int iType = common.toInt((String)VType.elementAt(j));
                         if(iType == java.sql.Types.INTEGER)
                              pstmt.setInt(j+2,common.toInt((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.DOUBLE)
                              pstmt.setDouble(j+2,common.toDouble((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.VARCHAR)
                              pstmt.setString(j+2,(String)aRow.elementAt(j));
                    }
                    pstmt.execute();
               }
               pstmt.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               //System.out.println(ex);
               ex.printStackTrace();
          }
          return vect;
     }
     private String prepareInsertQS(Vector VColumn,String STable)
     {
          int iSize = VColumn.size();

          String QS = "Insert Into "+STable+"(";
          for(int i=0;i<VColumn.size();i++)
          {
               String SCol = (String)VColumn.elementAt(i);
               QS = QS+(String)VColumn.elementAt(i)+",";
          }
          QS = QS.substring(0,QS.length()-1);
          QS = QS+") Values (";

          for(int i=0;i<iSize;i++)
               QS = QS+"?,";
          QS = QS.substring(0,QS.length()-1)+")";
          return QS;
     }

     private String prepareInsertQS(Vector VColumn,String STable,String SPKColumn)
     {
          int iSize = VColumn.size();

          String QS = "Insert Into "+STable+"(";
          if(SPKColumn != null)
          {
               QS = QS+SPKColumn+",";
               iSize++;
          }
          for(int i=0;i<VColumn.size();i++)
          {
               String SCol = (String)VColumn.elementAt(i);
               if(SCol.equals(SPKColumn))
               {
                    iSize=iSize-1;
                    continue;
               }
               QS = QS+(String)VColumn.elementAt(i)+",";
          }
          QS = QS.substring(0,QS.length()-1);
          QS = QS+") Values (";

          for(int i=0;i<iSize;i++)
               QS = QS+"?,";
          QS = QS.substring(0,QS.length()-1)+")";
          return QS;
     }

     private String prepareUpdateQS(Vector VColumn,String STable,String SPKColumn,String SPKValue)
     {
          int iSize = VColumn.size();

          String QS = "Update "+STable+" set ";
          for(int i=0;i<VColumn.size();i++)
          {
               QS = QS+(String)VColumn.elementAt(i)+"=?,";
          }
          QS = QS.substring(0,QS.length()-1);

          QS = QS+" Where "+SPKColumn+"="+SPKValue;

          return QS;
     }

     private String prepareUpdateQS(Vector VColumn,String STable,String SPKColumn)
     {
          int iSize = VColumn.size();

          String QS = "Update "+STable+" set ";
          for(int i=0;i<VColumn.size();i++)
          {
               QS = QS+(String)VColumn.elementAt(i)+"=?,";
          }
          QS = QS.substring(0,QS.length()-1);

          QS = QS+" Where "+SPKColumn+"=?";

          return QS;
     }

     protected void setRowSet(RowSet rowSet,String STable,String SPKColumn,String SPKValue)
     {
          Vector VColumn = rowSet.getColumnName();
          Vector VRows   = rowSet.getRows();
          Vector VType   = rowSet.getColumnType();

          String QS   = prepareUpdateQS(VColumn,STable,SPKColumn,SPKValue);
          try
          {
               RJDBCConnection jdbc  = RJDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               PreparedStatement pstmt   = theConnection.prepareStatement(QS);
               for(int i=0;i<VRows.size();i++)
               {
                    Vector aRow = (Vector)VRows.elementAt(i);
                    for(int j=0;j<VColumn.size();j++)
                    {
                         int iType = common.toInt((String)VType.elementAt(j));
                         if(iType == java.sql.Types.INTEGER)
                              pstmt.setInt(j+1,common.toInt((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.DOUBLE)
                              pstmt.setDouble(j+1,common.toDouble((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.VARCHAR)
                              pstmt.setString(j+1,(String)aRow.elementAt(j));
                    }
                    pstmt.execute();
               }
               pstmt.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }



     private String getCallSQS(String theProcedure,int isize)
     {
          String QS = "Call"+theProcedure+"(";

          for(int i=0;i<isize-1;i++)
               QS = QS+"?,";

          QS = QS + "?)";
          return QS;
     }


}

 
