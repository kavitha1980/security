package guiutil;
import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.*;

public class InterNetField extends JTextField {

	int iColumns = 30;
     double myDouble = 0.0;
     private Toolkit toolkit;

     public InterNetField()
	{
		super();
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(255,215,235));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());

	}

     public InterNetField(String value, int columns) {
          super(columns);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(255,215,235));
          setFont(new Font("",Font.BOLD,10));   
		iColumns = columns;
          setText(value);
          addKeyListener(new KeyList());
     }

     public InterNetField(int columns) {
          super(columns);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(255,215,235));
          setFont(new Font("",Font.BOLD,10));   

		iColumns = columns;
          addKeyListener(new KeyList());
     }

     protected Document createDefaultModel()
     {
          return new WholeNumberDocument();
     }

     protected class WholeNumberDocument extends PlainDocument {

          public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException
          {

               if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

               char[] source = str.toCharArray();
               char[] result = new char[source.length];
               int j = 0;

               for (int i = 0; i < result.length; i++)
               {
                    if (Character.isLetter(source[i]) || Character.isDigit(source[i]) || source[i] == '-' || source[i] == '@'|| source[i] == '.')
                    {
                         result[j++] = Character.toUpperCase(source[i]);
                    }
                    else
                    {
                         Toolkit.getDefaultToolkit().beep();
                    }
               }
               super.insertString(offs, new String(result, 0, j), a);
          }
     }
     public class KeyList implements KeyListener
     {
          int i=0;
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}

          public void keyReleased(KeyEvent ke)
          {
               int myLength = getText().trim().length();
               if (myLength==0)
               {
                    setText("");
                    return;
               }
          }
     }
}
