package guiutil;

import javax.swing.*;
import java.awt.event.*;
import java.util.Date;
import java.text.*;


public class ClockField extends JLabel implements ActionListener
{
     private Timer timer;


     public ClockField()
     {
          timer = new Timer(1000,this);
          timer.setInitialDelay(0);
          timer.start();

     }

     public void actionPerformed(ActionEvent ae)
     {

          if(ae.getSource()==timer)
          {
               try
               {
                    Date t = new Date();
                    DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM);
                    String time = df.format(t);
                    setText(time);
               }
               catch(Exception ex)
               {
               }
          }
     }
     
}
