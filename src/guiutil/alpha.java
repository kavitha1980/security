package guiutil;

import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class alpha extends JTextField
{
     double mydouble = 0.0;

     public alpha()
     {
          addKeyListener(new KeyList());
     }

     public alpha(int size)
     {
          super(size);
          addKeyListener(new KeyList());
     }

     public class KeyList implements KeyListener
     {
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}
          public void keyReleased(KeyEvent ke)
          {
               try
               {
                    mydouble = Double.parseDouble(getText());
                    
               }
               catch(NumberFormatException nfe)
               {
                    System.out.println(nfe);
               }

               if(!(mydouble==0))
               {     setText("");
                     mydouble = 0.0;
               }
          }
     }
     
}

/*
                    if(ke.getKeyChar()=='d' || ke.getKeyChar()=='f' || ke.getKeyChar()=='D' || ke.getKeyChar()=='F')
                    {
                         String str = getText();
                         str = str.substring(0,str.length()-1);
                         setText(str);
                    }

*/
