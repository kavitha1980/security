package guiutil;
import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

public class NameField extends JTextField {

	int iColumns = 30;

	public NameField()
	{
		super();
	}

    public NameField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
    }

    public NameField(int columns) {
        super(columns);
		iColumns = columns;
    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {
                if (Character.isLetter(source[i]) || source[i] == '.' || source[i] == ' ')
				{
                     result[j++] = Character.toUpperCase(source[i]);
				}
                else {
                    Toolkit.getDefaultToolkit().beep();
               }
            }
            super.insertString(offs, new String(result, 0, j), a);
        }
    }

}
