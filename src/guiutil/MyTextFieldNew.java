package guiutil;

import javax.swing.*;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;
import java.awt.event.*;

/**
* Same as a JTextField except we can conrol
* the no of characters displayed
**/

public class MyTextFieldNew extends JTextField {

	int iColumns = 30;

     public MyTextFieldNew()
	{
		super();
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));
          addFocusListener(new FocusList());

	}

    public MyTextFieldNew(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
        setBackground(new Color(255,217,179));
        setFont(new Font("",Font.BOLD,10));   
        addFocusListener(new FocusList());

    }

    public MyTextFieldNew(int columns) {
        super(columns);
		iColumns = columns;
        setBackground(new Color(255,217,179));
        setFont(new Font("",Font.BOLD,10));   
        addFocusListener(new FocusList());

    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}
            super.insertString(offs, str.toUpperCase(), a);
        }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            setCaretPosition(0);
            moveCaretPosition(getText().length());

            if(getText().length()>0) {

               setEditable(false);
            }
        }

        public void focusLost(FocusEvent fe)
        {
            try
            {

            if(getText().length()>0) {

               setEditable(false);
            }


            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

    }

}
