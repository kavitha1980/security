package guiutil;
import javax.swing.*;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

/**
* Same as a JTextField except we can conrol
* the no of characters displayed
**/
public class MyTextField extends JTextField {

	int iColumns = 60;

	public MyTextField()
	{
		super();
	}

     public MyTextField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
     }

     public MyTextField(int columns) {
        super(columns);
		iColumns = columns;
     }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}
            super.insertString(offs, str, a);
        }
    }

}
