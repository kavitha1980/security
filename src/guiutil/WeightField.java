/*
     A Bean to capture the weight of the content
     placed on a balance
*/

package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

import util.*;

public class WeightField extends JPanel implements ActionListener
{
     JLabel  theLabel;
     JButton BOkay;     
     static SnapSerialPort serialReader;
     int iMultiply = 1;

     Common common = new Common();

     public WeightField(int iMultiply)
     {
          this.iMultiply = iMultiply;
          createComponents();
     }

     private void createComponents()
     {
          theLabel  = new JLabel("0.000",JLabel.CENTER);
          BOkay     = new JButton("Read\n Scale");
          setLayout(new BorderLayout());
          theLabel.setFont(new Font("SanSerif", Font.BOLD, 24));
          theLabel.setBorder(new BevelBorder(1));
          add("Center",theLabel);
          add("East",BOkay);
          serialReader = new SnapSerialPort(theLabel,iMultiply);
          BOkay.addActionListener(this);
          //BOkay.setEnabled(false); temp
     }

     public void activate()
     {
       //theLabel.setText("0.000");
       //serialReader = new SnapSerialPort(theLabel,iMultiply);
       serialReader.getWeight();
       System.out.println("Scale Activated");
     }

     public void passivate()
     {
          closePort();  
     }


     public String getText()
     {
          return theLabel.getText();
     }

     public void setText(String SText)
     {
          theLabel.setText(SText);
     }

     public void closePort()
     {
          try
          {
               if(serialReader.serialPort != null)
               {
                    serialReader.freeze();
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void actionPerformed(ActionEvent ae)
     {
          activate();
          //passivate();

     }

}

