package guiutil;

import java.awt.*;
import java.awt.Component.*;
import javax.swing.table.TableCellRenderer.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.Vector;
import util.Common;

public class TabReportNew extends JPanel
{
     public JTable         ReportTable;
     public Object         RowData[][];
     public String         ColumnData[],ColumnType[];

     boolean        Flag;
     JPanel         thePanel;
     Common common = new Common();


     public TabReportNew(Object RowData[][],String ColumnData[],String ColumnType[])
     {
          this.RowData     = RowData;

          this.ColumnData  = ColumnData;
          this.ColumnType  = ColumnType;
          thePanel         = new JPanel();
          verifyRowData();
          setReportTable();
     }
     
     public void setReportTable()
     {
          AbstractTableModel dataModel = new AbstractTableModel()
          {
               public int     getColumnCount(){return ColumnData.length;}
               public int     getRowCount(){return RowData.length;}
               public Object  getValueAt(int row,int col){return RowData[row][col];}
               public String  getColumnName(int col){ return ColumnData[col];}
               public Class   getColumnClass(int col){ return getValueAt(0,col).getClass(); }
               public boolean isCellEditable(int row,int col)
               {
                    if(ColumnType[col]=="B" || ColumnType[col]=="E")
                         return true;
                    return false;
               }
               /*
               public void setValueAt(Object element,int row,int col)
               {
                    RowData[row][col]=element;
                    setPosition(element,row,col);
               }
               */
          };
          ReportTable            = new JTable(dataModel);
          ReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
          DefaultTableCellRenderer cellRenderer = new ColoredTableCellRenderer();

          ReportTable.setFont( new Font("Arial",Font.BOLD,10));
          ReportTable.setGridColor(new Color( 0,120,0));

          cellRenderer.setHorizontalAlignment(JLabel.RIGHT);

          for (int col=0;col<ReportTable.getColumnCount();col++)
          {
               if(ColumnType[col]=="N" || ColumnType[col]=="E") 
                    ReportTable.getColumn(ColumnData[col]).setCellRenderer(cellRenderer);

          }
                    
          setLayout(new BorderLayout());
          thePanel.setLayout(new BorderLayout());
          thePanel.add(ReportTable.getTableHeader(),BorderLayout.NORTH);
          thePanel.add(new JScrollPane(ReportTable),BorderLayout.CENTER);
          add("Center",thePanel);
     }    
     public void setPrefferedColumnWidth(int iColWidth[])
     {
          if(iColWidth.length != ColumnData.length)
               return;
          for(int i=0;i<ColumnData.length;i++)
          {
               if(iColWidth[i]>0)
                    (ReportTable.getColumn(ColumnData[i])).setPreferredWidth(iColWidth[i]);
          }
     }
     public void setHeaderColor(String ColumnData)
     {
          DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
          cellRenderer.setHorizontalAlignment(JLabel.CENTER);
          cellRenderer.setBackground(new Color(128,128,255));
          cellRenderer.setForeground(new Color(255,255,18));
          (ReportTable.getTableHeader()).setBorder(new BevelBorder(BevelBorder.RAISED));
          cellRenderer.setBorder(new SoftBevelBorder(0));
          (ReportTable.getColumn(ColumnData)).setHeaderRenderer(cellRenderer);
     }
     private void setPosition(Object element,int iRow,int iCol)
     {

     }

     private void verifyRowData()
     {
          for(int i=0;i<RowData.length;i++)
          {
               for(int j=0;j<ColumnType.length;j++)
               {
                    if(ColumnType[j].equals("D"))
                    {
                         RowData[i][j] = common.parseDate((String)RowData[i][j]);
                    }
               }
          }
     }

     public Vector getColumnValues(int j)
     {
          Vector vect = new Vector();

          try
          {
               for(int i=0;i<RowData.length;i++)
                    vect.addElement((String)RowData[i][j]);     
          }
          catch(Exception ex){}
          return vect;
     }
     public boolean isAllColumnFilled(int iRow)
     {
          boolean isfilled = true;

          for(int i=0;i<ColumnData.length;i++)
          {
               String SData = common.parseNull((String)RowData[iRow][i]);
               if(SData.equals(""))
                    isfilled = false;
               break;
          }
          return isfilled;
     }
     public void setColor(int iFrom,int iTo,int iR,int iG,int iB)
     {
           ReportTable.setRowSelectionInterval(iFrom,iTo);
           ReportTable.setSelectionBackground(new Color(iR,iG,iB));
     }

class ColoredTableCellRenderer extends DefaultTableCellRenderer
{
      public void setValue(Object value) 
      {
            super.setValue(value);
      }
}

     
    

}
