package guiutil;
import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.*;

public class FractionNumberField1 extends JTextField {

     int     iColumns        = 13;
     int     iFractionDigits = 0; 
     int     dotCount        = 0;
     double  myDouble = 0.0;
     int     myLength,indexPlace;
     String  myValue,myString;
     private Toolkit toolkit;

     public FractionNumberField1()
	{
          super();
          setColumns(iColumns);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(122,203,201));
          setFont(new Font("",Font.BOLD,10));

          addKeyListener(new KeyList());
	}

     public FractionNumberField1(String value, int columns) {
          super(columns);
          setColumns(columns);
		iColumns = columns;
          setText(value);
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(122,203,201));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());
     }

     public FractionNumberField1(int columns) {
          super(columns);
          setColumns(columns);
		iColumns = columns;
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(122,203,201));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());
     }

     public FractionNumberField1(int columns,int iWDigits) {
          super(columns);
          setColumns(columns);
          iColumns = columns;
          iFractionDigits = iWDigits;
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(122,203,201));
          setFont(new Font("",Font.BOLD,10));   

          addKeyListener(new KeyList());
     }

     protected Document createDefaultModel()
     {
          return new WholeNumberDocument();
     }

     protected class WholeNumberDocument extends PlainDocument {

          public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException
          {

               if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

               char[] source = str.toCharArray();
               char[] result = new char[source.length];
               int j=0,digitCount=0;

               for (int i = 0; i < result.length; i++)
               {
                    //System.out.println("source[i] : "+i+" "+source[i]);
                    if (source[i]=='.')
                    {
                         dotCount++;
                         //System.out.println("dotCount : "+dotCount);
                    }
                    if ((source[i]=='-') || Character.isDigit(source[i]) || dotCount==1)
                    {
                         if (Character.isDigit(source[i])) digitCount++;

                              result[j++] = source[i];
                    }
                    else
                    {
                         Toolkit.getDefaultToolkit().beep();
                    }
               }
               dotCount=0;
               super.insertString(offs, new String(result, 0, j), a);
          }
     }
     public class KeyList implements KeyListener
     {
          int i=0;
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}

          public void keyReleased(KeyEvent ke)
          {
               myLength = getText().trim().length();
               myString = getText().trim();
               if (myLength==0)
               {
                    setText("");
                    return;
               }
               if (myLength>0)
               {

                    indexPlace=getText().trim().indexOf(".");
                    if (indexPlace>0 && myLength-indexPlace+1>2)
                    {
                        if (myLength>=indexPlace+3+iFractionDigits)
                        {
                             myValue = myString.substring(0,indexPlace+3+iFractionDigits);
                             setText(myValue);
                        }
                    }
                    else
                    {
                        myValue = myString;
                    }
                    return;
               }
          }
     }
}
