package guiutil;
import javax.swing.*;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;

public class IntegerField extends JTextField {

    private Toolkit toolkit;
    private NumberFormat integerFormatter;
	int iColumns = 35;

     int dotFlag = 0,dotIndex = 0;
     public IntegerField()
	{
		super();
          setHorizontalAlignment(SwingConstants.RIGHT);
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));   
	}
     public IntegerField(double value, int columns)
     {
        super(columns);
		iColumns = columns;
        toolkit = Toolkit.getDefaultToolkit();
        setHorizontalAlignment(SwingConstants.RIGHT);
        integerFormatter = (DecimalFormat)NumberFormat.getNumberInstance(Locale.US);
        setValue(value);
        setBackground(new Color(255,217,179));
        setFont(new Font("",Font.BOLD,10));   
     }
     public IntegerField(int columns) {
        super(columns);
		iColumns = columns;
        toolkit = Toolkit.getDefaultToolkit();
        integerFormatter = (DecimalFormat)NumberFormat.getNumberInstance(Locale.US);
        setBackground(new Color(197,156,224));
        setFont(new Font("",Font.BOLD,10));   
        setHorizontalAlignment(SwingConstants.RIGHT);
     }

     public double getValue() {
        double retVal = 0;
        try {
            retVal = (integerFormatter).parse(getText()).doubleValue();
        } catch (ParseException e) {
            toolkit.beep();
        }
        return retVal;
    }

    public void setValue(double value) {
        setText(integerFormatter.format(value));
    }

    protected Document createDefaultModel() {
        return new IntegerNumberDocument();
    }

    protected class IntegerNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {
			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {
                if (Character.isDigit(source[i]) || 
                         (source[i] == '.' && getText(0, getLength()).indexOf('.') == -1))
                {
                    result[j++] = source[i];
                }
                else {
                    toolkit.beep();
                }
            }

            super.insertString(offs, new String(result, 0, j), a);

        }
    }

}
