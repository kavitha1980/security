package guiutil;

import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

public class AddressField extends JTextField {

     int iColumns = 40;

     public AddressField()
	{
		super();
	}

    public AddressField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
    }

    public AddressField(int columns) {
        super(columns);
		iColumns = columns;
    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++)
            {
                    result[j++] = source[i];
            }
            super.insertString(offs, (new String(result, 0, j)).toUpperCase(), a);
        }
    }

}
