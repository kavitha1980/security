package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

public class CritPanel extends JPanel
{
     MyComboBox theList;
     JRadioButton JRAll,JRSelected;

     Vector VCode,VName;

     public CritPanel(Vector VCode,Vector VName,String STitle)
     {
          this.VCode = VCode;
          this.VName = VName;

          theList = new MyComboBox(VName);

          JRAll   = new JRadioButton("All",true);
          JRSelected = new JRadioButton("Selected",false);
          theList.setEnabled(false);

          JRAll.addActionListener(new ActList());
          JRSelected.addActionListener(new ActList());

          setLayout(new GridLayout(3,1));
          setBorder(new TitledBorder(STitle));

          add(JRAll);
          add(JRSelected);
          add(theList);
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               if(ae.getSource()==JRAll)
               {
                    JRSelected.setSelected(false);
                    theList.setEnabled(false);
                    JRAll.setSelected(true);
               }
               else
               {
                    JRAll.setSelected(false);
                    JRSelected.setSelected(true);
                    theList.setEnabled(true);
               }
          }
     }

     public boolean isSelected(String SName)
     {
          if(JRAll.isSelected())
               return true;

          String str = (String)theList.getSelectedItem();


          if(str.equals(SName))
               return true;

          return false;
     }

     public String getName(String SCode)
     {
          int index = VCode.indexOf(SCode);

          if(index == -1)
               return "";

          return (String)VName.elementAt(index);
     }
}
