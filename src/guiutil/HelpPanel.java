package guiutil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import rndi.*;


public class HelpPanel extends JPanel implements CodedNames
{
     JButton     helpButton;
     String      SFile;

     JDialog     dialog;
     JPanel      thePanel;
     JEditorPane TA;

     public HelpPanel(String SFile)
     {
          this.SFile = SFile;
          helpButton = new JButton("Help",new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/help/cbook.gif"));
          helpButton.setMnemonic('H');
          helpButton.setToolTipText("Click for help");
          helpButton.addActionListener(new ActList());

          thePanel   = new JPanel();
          dialog     = new JDialog(new Frame(),"Help on request",true);
          try
          {
               TA = new JEditorPane();
               TA.setContentType("text/html");
               TA.setText(getContent());
               TA.setEditable(false);
               thePanel.setLayout(new BorderLayout());
               thePanel.add("Center",new JScrollPane(TA));
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          add(helpButton);
     }

     private class ActList implements ActionListener
     {
          public void actionPerformed(ActionEvent ae)
          {
               activate();
          }
     }

     private void activate()
     {
          helpButton.setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/help/obook.gif"));
          dialog.getContentPane().add("Center",thePanel);
          dialog.setBounds(86,64,629,485);
          dialog.setVisible(true);
          helpButton.setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/help/cbook.gif"));
     }

     private String getContent()
     {
          String SURL ="//"+SCRIPTHOST+"/software/rawmaterial/src/help/"+SFile+".htm";
          String str = "";
          try
          {
               Reader in = new FileReader(SURL);
               char[] buff = new char[4096];
               int nch;
               while ((nch = in.read(buff, 0, buff.length)) != -1)
               {
                    str = str+new String(buff, 0, nch);
               }
          }
          catch(Exception ex){}
          return str;               
     }
}
