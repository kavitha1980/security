package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class MyList extends JList
{
     protected Vector VName;
     String str="";

     public MyList(Vector VName)
     {
          this.VName = VName;
          setListData(VName);
          addKeyListener(new KeyList());
     }

     public MyList()
     {
          this.VName = new Vector();
          setListData(VName);
          addKeyListener(new KeyList());
     }

     public void setMyListData(Vector VName)
     {
          this.VName = VName;
          setListData(VName);
     }
     private class KeyList extends KeyAdapter
     {
          public void keyReleased(KeyEvent ke)
          {
               char lastchar=ke.getKeyChar();
               try
               {
                    if(ke.getKeyCode()==8)
                    {
                         str=str.substring(0,(str.length()-1));
                         setCursor();
                    }
                    else if((lastchar>='A' && lastchar<='Z') || (lastchar==' ') || (lastchar=='.') || (lastchar=='-') || (lastchar>='0' && lastchar <= '9'))
                    {
                         str=str+lastchar;
                         setCursor();
                    }
               }
               catch(Exception ex){}
          }
          public void keyPressed(KeyEvent ke)
          {
               if(ke.getKeyCode()==KeyEvent.VK_ESCAPE)
                    str="";
          }
     }                      
     private void setCursor()
     {
          int index=0;
          for(index=0;index<VName.size();index++)
          {
               String str1 = ((String)VName.elementAt(index));
               if(str1.startsWith(str))
               {
                    setSelectedValue(str1,true);   
                    break;
               }
          }
     }

     public int poolSize()
     {
          return VName.size();
     }
}
