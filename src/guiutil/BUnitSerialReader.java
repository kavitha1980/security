
/*
     The Subclass of the Balance Bean to capture the weight of the content
     placed on a balance
     Arulvel
*/

package guiutil;

import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import client.packing.*;
import util.*;

public class BUnitSerialReader implements SerialPortEventListener,Runnable
{
     protected JLabel theLabel;
     protected FractionNumberField TTare1,TTare2,TGWeight;
     protected JLabel  LNet;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;

     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     int    ctr=0;
     boolean heartAttack = false;
     StringBuffer inputBuffer = new StringBuffer();
     BalePackingFrame yarnpacking;

     public BUnitSerialReader(JLabel theLabel,FractionNumberField TGWeight,FractionNumberField TTare1,FractionNumberField TTare2,JLabel LNet,BalePackingFrame yarnpacking)
     {
          this.theLabel    = theLabel;
          this.TGWeight    = TGWeight;
          this.TTare1      = TTare1;
          this.TTare2      = TTare2;
          this.LNet        = LNet;
          this.yarnpacking = yarnpacking;
          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          theLabel.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE); // 2400
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println(" Ex 1->"+ex);
//               ex.printStackTrace();
          }
     }
     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setWeight(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                if(ctr < 10 )  
                                  outputStream.write((int)'a');
                                else
                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                         System.out.println(" E---2"+e);
//                         e.printStackTrace();
                    }
          }
     }

     private void setWeight(String str)
     {
          try
          {
               int index1 = str.indexOf("E"); // +
               int index2 = str.indexOf("k"); // g
               String xtr = str.substring(index1+2,index2); //index1+1
               theLabel.setText( common.getRound(common.toDouble(xtr.trim()),3));
               TGWeight.setText(theLabel.getText());
               ctr++;      
               yarnpacking.setWeightData();
               setNetWeight();
          }
          catch(Exception ex)
          {
                System.out.println(" E---3"+ex);
          }
     }

     public void freeze()
     {
          serialPort.close();
          theLabel.setForeground(new Color(0,174,174));
          setNetWeight();
     }

     private void setNetWeight()
     {

        //LNet.setText(common.getRound(common.toDouble(TGWeight.getText())-common.toDouble(TTare1.getText())-common.toDouble(TTare2.getText()),3));

     }

}
