/*
     A Bean to capture the weight of the content
     placed on a balance
*/

package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import util.*;
import client.packing.*;
import java.io.*;

public class AUnitBalanceBean extends JPanel 
{
     protected FractionNumberField TTare1,TTare2,TGWeight;
     protected JButton BOkay;
     
     JLabel LNet;

     JLabel  theLabel;
     
     static AUnitSerialReader serialReader;

     Common common = new Common();
     boolean flag ;
     BalePackingFrame yarnpacking;

     public AUnitBalanceBean(JButton BOkay,FractionNumberField TGWeight,FractionNumberField TTare1,FractionNumberField TTare2,JLabel LNet)
     {
          this.BOkay     = BOkay;
          this.TGWeight  = TGWeight;
          this.TTare1    = TTare1;
          this.TTare2    = TTare2;
          this.LNet      = LNet;
          createComponents();
     }
     public AUnitBalanceBean(FractionNumberField TGWeight,FractionNumberField TTare1,FractionNumberField TTare2,JLabel LNet,boolean flag,BalePackingFrame yarnpacking)
     {
          this.TGWeight    = TGWeight;
          this.TTare1      = TTare1;
          this.TTare2      = TTare2;
          this.LNet        = LNet;
          this.flag        = flag;
          this.yarnpacking = yarnpacking;
          createComponents();
     }


     public void createComponents()
     {
          theLabel    = new JLabel("0.000",JLabel.CENTER);
          setLayout(new BorderLayout());
          theLabel.setFont(new Font("SanSerif", Font.BOLD, 48));
          theLabel.setBorder(new BevelBorder(1));
          add("Center",new JScrollPane(theLabel));



          TTare1.setText("2.000");
          TTare2.setText(".200");
     }

     public void activate()
     {
                if(flag)
                {
                     LNet.setText("0");
                     theLabel.setText("0.000");
                     serialReader = new AUnitSerialReader(theLabel,TGWeight,TTare1,TTare2,LNet,yarnpacking);
               }
               else
               {
                     TGWeight.setText("0");
                     LNet.setText("0");
                     theLabel.setText("00.01");
               }

     }
     public double getGrossWeight()
     {
          return common.toDouble(theLabel.getText());
     }

     public void closePort()
     {
          try
          {
               if(serialReader.serialPort != null)
               {
                    serialReader.freeze();
               }
          }
          catch(Exception ex)
          {
          }
     }

}

