package guiutil;

import javax.swing.*;
import java.awt.*;
import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import rndi.*;

public class BaleIdSField extends JTextField implements CodedNames
{

     public BaleIdSField()
     {
          addKeyListener(new KeyList());
          setBackground(BALEID_BACKGROUND);
          setForeground(Color.white);
     }

     public BaleIdSField(int size)
     {
          super(size);
          addKeyListener(new KeyList());
          setBackground(BALEID_BACKGROUND);
          setForeground(Color.white);
     }

     public class KeyList implements KeyListener
     {
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}
          public void keyReleased(KeyEvent ke)
          {
               try
               {
                     //if (Integer.parseInt(getText())==0) setText("");
                     //Integer.parseInt(getText());
                     //Float.parseFloat(getText());
               }
               catch(NumberFormatException nfe)
               {
                    setText("");
               }
          }
     }
     
}

