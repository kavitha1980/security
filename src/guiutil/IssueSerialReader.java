package guiutil;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;
import java.awt.Color;

import util.*;

public class IssueSerialReader implements SerialPortEventListener,Runnable
{

     protected FractionNumberField TScan;
     protected JButton             BScan;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort   serialPort;
     InputStream  inputStream;
     OutputStream outputStream;

     Thread       readThread;

     String SPort     = "COM1";
     String strBuffer = "";

     int     ctr=0;
     boolean heartAttack = false;
     Common common = new Common();

     public IssueSerialReader(FractionNumberField TScan,JButton BScan)
     {
          try
          {
               this.TScan     = TScan;
               this.BScan     = BScan;
     
               createComponents();
               readThread = new Thread(this);
               readThread.start();
          }
          catch(Exception ex){System.out.println(ex); }
     }

     private void createComponents()
     {
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();

               System.out.println("Port"+portList);
               while(portList.hasMoreElements())
               {

                    portId = (CommPortIdentifier)portList.nextElement();
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         System.out.println("Port"+portId.getName());
                        break;
                    }
              }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_2,SerialPort.PARITY_EVEN);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {

               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e)
          {
               System.out.println("Run :"+e);
          }
     }

     public void serialEvent(SerialPortEvent event)
     {
          StringBuffer inputBuffer = new StringBuffer();
          int newData = 0;

          switch(event.getEventType())
          {

               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                            newData = inputStream.read();
                            System.out.println(newData);

                         while ((char)newData !='')
                         {
                                      newData = inputStream.read();
                                      inputBuffer.append((char)newData);
                                      if((char)newData=='')
                                           heartAttack = true;
                         }

                         if(heartAttack)
                         {
                                setWeight(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                outputStream.write((int)'a');
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }

     private void setWeight(String str)
     {
          try
          {
               int index = str.indexOf("+");
               int index1= str.indexOf("g");
               String xtr ="";
               String SDecimal="";
               String SFraction="";
               String SFraction1="";
               String SFraction2="";

               String STWt =str.substring(index+1,index1);
               StringTokenizer Token= new StringTokenizer(STWt,".");

               while(Token.hasMoreTokens())
               {
                  SDecimal  =(String)Token.nextToken();
                  SFraction =(String)Token.nextToken();


                  if(SFraction.length() > 3)
                  {
                         SFraction1 = SFraction.substring(0,SFraction.length()-3);
                         SFraction2 = SFraction.substring(2,SFraction.length());
                         SFraction =SFraction1+SFraction2;
                  }
                  else
                  {
                          SFraction=SFraction;
                  }

               }

               xtr =SDecimal+"."+SFraction;

               double dNet= common.toDouble(xtr.trim());

               if(dNet <= 0)
                    TScan.setText("0.0");
               else
                    TScan.setText(common.getRound(dNet,3));

               if(common.toDouble(TScan.getText()) > 0)
                    BScan.setEnabled(true);

          }
          catch(Exception ex){ System.out.println(ex);}
     }


     public void freeze()
     {
          serialPort.close();
     }
}
