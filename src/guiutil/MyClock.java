/*

*/
package guiutil;
import java.util.*;
import java.awt.*;
import java.text.*;
import javax.swing.*;

/**
 * Time!
 *
 * @author R.Natarajan
 */

public class MyClock extends MyPanel implements Runnable {
    Thread timer;                // The thread that displays clock
    int lastxs, lastys, lastxm,
        lastym, lastxh, lastyh;  // Dimensions used to draw hands 
    SimpleDateFormat formatter;  // Formats the date displayed
    String lastdate;             // String to hold date displayed
    Font clockFaceFont;          // Font for number display on clock
    Date currentDate;            // Used to get date to display
    Color handColor;             // Color of main hands and dial
    Color numberColor;           // Color of second hand and numbers
    JTextField theDate;


    public MyClock() {
        /*
        theDate = new JTextField(7);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);
        add(new JLabel(new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/guiutil/blue-ball.gif")));

        theDate.setBackground(new Color(255,222,206));    
        */

        int x,y;
        lastxs = lastys = lastxm = lastym = lastxh = lastyh = 0;
        //formatter = new SimpleDateFormat ("EEE MMM dd hh:mm:ss yyyy", Locale.getDefault());
        formatter = new SimpleDateFormat ("HH:mm:ss", Locale.getDefault());
        currentDate = new Date();
        lastdate = formatter.format(currentDate);
        clockFaceFont = new Font("Serif", Font.PLAIN, 14);
        handColor = Color.blue;
        numberColor = Color.darkGray;
//        setEditable(false);

/*
        setSize(150,150);              // Set clock window size
*/
        start();
    }


    // Paint is the main part of the program
    public void paint(Graphics g) {
        int xh=0, yh=0, xm=0, ym=0, xs=0, ys=0, s = 0, m = 10, h = 10, xcenter, ycenter;
        String today;

        currentDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("s",Locale.getDefault());
        try {
            s = Integer.parseInt(formatter.format(currentDate));
        } catch (NumberFormatException n) {
            s = 0;
        }
        formatter.applyPattern("m");
        try {
            m = Integer.parseInt(formatter.format(currentDate));
        } catch (NumberFormatException n) {
            m = 10;
        }    
        formatter.applyPattern("h");
        try {
            h = Integer.parseInt(formatter.format(currentDate));
        } catch (NumberFormatException n) {
            h = 10;
        }
//        formatter.applyPattern("EEE MMM dd HH:mm:ss yyyy");
        formatter.applyPattern("HH:mm:ss");
        today = formatter.format(currentDate);
    
    
        g.setFont(clockFaceFont);


//
        g.setColor(getBackground());
        if (s != lastxs ) { 
            g.drawString(lastdate, 0,25);
        }
        g.setColor(numberColor);
        g.drawString("", 0, 25);
        g.drawString(today, 0, 25);
        g.setColor(handColor);
        lastxs=s; lastys=s;
        lastxm=m; lastym=m;
        lastxh=h; lastyh=h;
        lastdate = today;
        currentDate=null;
    }

    public void start() {
        timer = new Thread(this);
        timer.start();
    }

    public void stop() {
        timer = null;
    }

    public void run() {
        Thread me = Thread.currentThread();
        while (timer == me) {
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
            }
            repaint();
        }
    }

    public void update(Graphics g) {
        paint(g);
    }

}
