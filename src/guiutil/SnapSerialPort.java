/*

*/

package guiutil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;

import util.*;
public class SnapSerialPort implements SerialPortEventListener
{
     protected JLabel theLabel;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;

     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     int    ctr=0;
     int    iMultiply = 1;

     public SnapSerialPort(JLabel theLabel,int iMultiply)
     {
          this.theLabel   = theLabel;
          this.iMultiply  = iMultiply;
          createComponents();
     }

     private void createComponents()
     {
          theLabel.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         try {
                              serialPort = (SerialPort)portId.open("comapp",2000);
                              break;
                         } catch (PortInUseException e) {}
                    }
               }

               if(serialPort != null)
               {
                    theLabel.setForeground(new Color(255,94,94));
                    ctr=0;

                    serialPort.setSerialPortParams(2400, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);

                    try {
                         serialPort.addEventListener(this);
                    } catch (TooManyListenersException e) {}

                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);

                    try {
                         inputStream  = serialPort.getInputStream();
                    } catch (IOException e) {}

                    try {
                         outputStream = serialPort.getOutputStream();
                    } catch (IOException e) {}

                    outputStream.write((int)'a'); //temp

               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public void serialEvent(SerialPortEvent event)
     {
          StringBuffer inputBuffer = new StringBuffer();
          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                         }
                         System.out.println("Data Available Event");
                         System.out.println(new String(inputBuffer));
                         setWeight(new String(inputBuffer));
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     public void getWeight()
     {
          try
          {
               theLabel.setText("0.000");
               outputStream.write((int)'a'); 
          }
          catch(Exception ex){}
     }




     private void setWeight(String str)
     {
          try
          {
               //System.out.println(str);
               //theLabel.setForeground(new Color(0,174,174));
               int index1 = str.indexOf("+");
               int index2 = str.indexOf("g");
               String xtr = str.substring(index1+1,index2);
               System.out.println(xtr);
               theLabel.setText( common.getRound( (common.toDouble(xtr.trim())/iMultiply )  ,3) );
               ctr++;
          }
          catch(Exception ex){}
     }

     public void freeze()
     {
          serialPort.close();
     }


}
/*
                         if(ctr < 10)
                              outputStream.write((int)'a');
                         else
                              freeze();

     public void actionPerformed(ActionEvent ae)
     {
               if(ae.getSource()==BOkay)
               {
                        try
                        {
                                  BOkay.setEnabled(false);
                                  theLabel.setForeground(new Color(255,94,94));
                                  ctr=0;
                                  if(serialPort != null)
                                       return;
                
                                  outputStream.write((int)'a');
                
                                  theLabel.setForeground(new Color(0,174,174));
                                  BOkay.setEnabled(true);
                        }
                        catch(Exception ex)
                        {
                                System.out.println(ex);
                        }
               }
             
     }


*/
