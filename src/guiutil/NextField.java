package guiutil;

import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NextField extends JTextField
{
     public NextField()
     {
          addKeyListener(new KeyList());
     }

     public NextField(int size)
     {
          super(size);
          addKeyListener(new KeyList());
     }

     public class KeyList implements KeyListener
     {
          public void keyPressed(KeyEvent ke){}
          public void keyTyped(KeyEvent ke){}
          public void keyReleased(KeyEvent ke)
          {
               try
               {
                    Double.parseDouble(getText());
                    if(ke.getKeyChar()=='d' || ke.getKeyChar()=='f' || ke.getKeyChar()=='D' || ke.getKeyChar()=='F')
                    {
                         String str = getText();
                         str = str.substring(0,str.length()-1);
                         setText(str);
                    }
                    
               }
               catch(NumberFormatException nfe)
               {
                    setText("");
               }
          }
     }
     
}

