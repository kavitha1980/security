package guiutil;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import util.*;

public class DateField1 extends JPanel
{
      public JTextField TDay,TMonth,TYear;
      int Year=0,Month=0,Day=0;
      Border border;
      boolean mode=true;


      public DateField1()
      {
             setLayout(new FlowLayout(0,0,0));

             border=new BevelBorder(BevelBorder.LOWERED);

             TDay   = new JTextField(2);
             TMonth = new JTextField(2);
             TYear  = new JTextField(3);

             TDay   .setBackground(Color.pink);
             TMonth .setBackground(Color.pink);
             TYear  .setBackground(Color.pink);
             TDay   .setBorder(border);
             TMonth .setBorder(border);
             TYear  .setBorder(border);

             add(TDay);
             add(TMonth);
             add(TYear);
   
             TDay.addKeyListener(new calc());
             TMonth.addKeyListener(new calc());
             TYear.addKeyListener(new calc());
      }


      public class calc extends KeyAdapter
      {
      
           public void keyReleased(KeyEvent KE)
           {
               if(!mode)
                  return;
               int Num=0;
               if(KE.getSource()==TDay)
               {
                   String str1 = TDay.getText();
                   try  {  Num = Integer.parseInt(str1);  }
                   catch(NumberFormatException NFE) { TDay.setText("");   }
      
                   if (str1.length()==2 || str1.length() > 2)
                   {

                       if (Num>31)
                       {
                            JOptionPane.showMessageDialog(null,"Maximun 31 days in a Month","Error Message",JOptionPane.INFORMATION_MESSAGE);
                            TDay.setText("");
                            str1 = TDay.getText();
                       }
                       else if(Num<=31)
                       {
                          TMonth.setText(""); 
                          TMonth.requestFocus();
                       }
                   }
               }
               if(KE.getSource()== TMonth)
               {
                   String str2 = TMonth.getText();
                   String str1 = TDay.getText();
                   try
                   {
                       Month = Integer.parseInt(str2);
                       Day   = Integer.parseInt(str1);
                       if(Day<10)
                       {
                           TDay.setText("0"+String.valueOf(Day));
                       }
                   }
                   catch(NumberFormatException NFE) {  TMonth.setText("");    }
                   if(Day==0)
                   {
                        JOptionPane.showMessageDialog(null,"Day should not be 0","Error Message",JOptionPane.INFORMATION_MESSAGE);
                        TDay.setText("");
                        TDay.requestFocus();
                   }
                   if (str2.length()==2  || str2.length() > 2)
                   {
                       if (Month>12 || Month<1)
                       {
                           JOptionPane.showMessageDialog(null,"Maximum 12 Month in a Year","Error Message",JOptionPane.INFORMATION_MESSAGE);
                            TMonth.setText("");
                            str2 = TMonth.getText();
                       }
                       else if(Month>=1 && Month<=12)
                       {
                          TYear.setText(""); 
                          TYear.requestFocus();
                       }
                   }
                   if(Month==4 || Month==6 || Month==9 || Month==11)
                   {
                        if(Day>30)
                        {
                          JOptionPane.showMessageDialog(null,"This Month have only 30 days","Error Message",JOptionPane.INFORMATION_MESSAGE);
                          TDay.setText(""); 
                          TDay.requestFocus();
                        }
                   }
                   if(Month==2 && Day>29)
                   {
                       JOptionPane.showMessageDialog(null,"February month have only 29 days","Error Message",JOptionPane.INFORMATION_MESSAGE);
                       TDay.setText(""); 
                       TDay.requestFocus();
                   }
               }
               if(KE.getSource()==TYear)
               {
                      int Year=0;
                      String str3 = TYear.getText();
                      try
                      {
                           Year = Integer.parseInt(TYear.getText());
                           Month= Integer.parseInt(TMonth.getText());
                           Day  = Integer.parseInt(TDay.getText());
                      }
                      catch(NumberFormatException NFE) {   TYear.setText("");  }
                      if(Month==0)
                      {
                           JOptionPane.showMessageDialog(null,"Month should not be 0","Error Message",JOptionPane.INFORMATION_MESSAGE);
                           TMonth.setText("");
                           TMonth.requestFocus();
                      }
                      if(Month<10)
                      {
                          TMonth.setText("0"+String.valueOf(Month));
                      }
                      if((TYear.getText()).length()>4)
                      {
                        TYear.setText((TYear.getText()).substring(0,4));
                      }
                      if (str3.length()==4)
                      {
                             if (Year>3001 || Year==0)
                             {
                                  TYear.setText("");
                                  TYear.requestFocus();
                             }
                             if(Year%4 != 0 && Month==2 && Day>=29)
                             {
                                    JOptionPane.showMessageDialog(null,"Not a Leap Year","Error Message",JOptionPane.INFORMATION_MESSAGE);
                                    TDay.setText("");
                                    TDay.requestFocus();
                             }
                      }
               }
           }
      }
      public void setTodayDate()
      {
        Date dt = new Date();
        int iDay   = dt.getDate();
        int iMonth = dt.getMonth()+1;
        int iYear  = dt.getYear()+1900;
        if(iDay < 10)
           TDay.setText("0"+iDay);
        else
           TDay.setText(""+iDay);
        if(iMonth < 10)
           TMonth.setText("0"+iMonth);
        else
           TMonth.setText(""+iMonth);
        TYear.setText(""+iYear);
      }
      public String toString()
      {
            return TDay.getText()+"."+TMonth.getText()+"."+TYear.getText();
      }
      public String toNormal()
      {
            return TYear.getText()+TMonth.getText()+TDay.getText();
      }
      public void fromString(String str)
      {
            Common common = new Common();
            int iYear=0,iMonth=0,iDay=0;
            try
            {
                  iYear  = common.toInt(str.substring(6,10));
                  iMonth = common.toInt(str.substring(3,5));
                  iDay   = common.toInt(str.substring(0,2));
            }
            catch(Exception ex)
            {
                  return;
            }

            String SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            String SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            String SYear  = ""+iYear;

            TDay.setText(SDay);
            TMonth.setText(SMonth);
            TYear.setText(SYear);
      }

      public void fromString1(String str)
      {
            Common common = new Common();
            int iYear=0,iMonth=0,iDay=0;
            try
            {
                  iYear  = common.toInt(str.substring(0,4));
                  iMonth = common.toInt(str.substring(4,6));
                  iDay   = common.toInt(str.substring(6,8));
            }
            catch(Exception ex)
            {
                  return;
            }

            String SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            String SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            String SYear  = ""+iYear;

            TDay.setText(SDay);
            TMonth.setText(SMonth);
            TYear.setText(SYear);
      }

      public void setEditable(boolean flag)
      {
            TDay.setEditable(flag);
            TMonth.setEditable(flag);
            TYear.setEditable(flag);
            mode=flag;
      }

}
