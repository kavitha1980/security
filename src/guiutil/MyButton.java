package guiutil;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

import rndi.*;

public class MyButton extends JButton implements CodedNames
{
     public MyButton()
     {
          setBackground(BUTTON_NORMAL_BACKGROUND);
          setForeground(BUTTON_NORMAL_FOREGROUND);
          setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/guiutil/save.gif"));
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }

     public MyButton(String str)
     {
          super(str);
          setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/rawmaterial/src/guiutil/save.gif"));
          setBackground(BUTTON_NORMAL_BACKGROUND);
          setForeground(BUTTON_NORMAL_FOREGROUND);
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }

     private class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               enhance();
          }
          public void mouseExited(MouseEvent me)
          {
               deEnhance();
          }
     }

     private class FocusList extends FocusAdapter
     {
          public void focusGained(FocusEvent fe)
          {
               enhance();
          }
          public void focusLost(FocusEvent fe)
          {
               deEnhance();
          }
     }

     private void enhance()
     {
          setBackground(BUTTON_ENHANCED_BACKGROUND);
          setForeground(BUTTON_ENHANCED_FOREGROUND);
     }
     private void deEnhance()
     {
          setBackground(BUTTON_NORMAL_BACKGROUND);
          setForeground(BUTTON_NORMAL_FOREGROUND);
     }
}
