package guiutil;

import javax.swing.*;
import java.awt.*;
import rndi.*;

public class MyLabel extends JLabel implements CodedNames
{
     public MyLabel()
     {
          super();
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(FOREGROUND);
     }

     public MyLabel(String SName)
     {
          setText(SName);
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(FOREGROUND);
     }

     public MyLabel(String SName,int iAlignment)
     {
          super(SName,iAlignment);
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(FOREGROUND);
     }

}
