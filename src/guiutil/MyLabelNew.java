package guiutil;

import javax.swing.*;
import java.awt.*;
import rndi.*;

public class MyLabelNew extends JLabel implements CodedNames
{
     public MyLabelNew()
     {
          super();
          setFont(new Font("Arial",Font.BOLD,35));
          setForeground(FOREGROUND);
     }

     public MyLabelNew(String SName)
     {
          setText(SName);
          setFont(new Font("Arial",Font.BOLD,35));
          setForeground(FOREGROUND);
     }

     public MyLabelNew(String SName,int iAlignment)
     {
          super(SName,iAlignment);
          setFont(new Font("Arial",Font.BOLD,35));
          setForeground(FOREGROUND);
     }
}
