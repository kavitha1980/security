package guiutil;

import javax.swing.*;
import java.awt.*;
import rndi.*;

public class TamilLabel extends JLabel implements CodedNames
{
     public TamilLabel()
     {
          super();
          setFont(new Font("Kruti Tamil 100",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

     public TamilLabel(String SName)
     {
          setText(SName);
          setFont(new Font("Kruti Tamil 100",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

     public TamilLabel(String SName,int iAlignment)
     {
          super(SName,iAlignment);
          setFont(new Font("Kruti Tamil 100",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

}
