package guiutil;

import java.awt.*;
import javax.swing.*;
import rndi.*;


public class PrintButton extends JButton implements CodedNames
{
     public PrintButton()
     {
          setText("Print");
          setMnemonic('P');
          setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/Dispatch/src/guiutil/print.gif"));
     }

     public PrintButton(String str)
     {
          super(str);
          setIcon(new ImageIcon("//"+SCRIPTHOST+"/software/Dispatch/src/guiutil/print.gif"));
     }

}
