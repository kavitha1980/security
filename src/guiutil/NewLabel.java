package guiutil;

import javax.swing.*;
import java.awt.*;
import rndi.*;

public class NewLabel extends JLabel implements CodedNames
{
     public NewLabel()
     {
          super();
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

     public NewLabel(String SName)
     {
          setText(SName);
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

     public NewLabel(String SName,int iAlignment)
     {
          super(SName,iAlignment);
          setFont(new Font("Arial",Font.BOLD,11));
          setForeground(new Color(0,128,0));
     }

}
