package guiutil;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import util.*;

public class TimeField extends JPanel
{
      JTextField THours,TMinutes,TSeconds;
      int Seconds=0,Minutes=0,Hours=0;
      Border border;
      boolean mode=true;
      boolean compl;
      Common common = new Common();
      public TimeField(boolean compl)
      {
             this.compl = compl;  
             setLayout(new FlowLayout(0,0,0));

             border=new BevelBorder(BevelBorder.LOWERED);

             THours   = new JTextField(2);
             TMinutes = new JTextField(2);
             TSeconds  = new JTextField(2);

             THours   .setBackground(Color.pink);
             TMinutes .setBackground(Color.pink);
             TSeconds  .setBackground(Color.pink);
             THours   .setBorder(border);
             TMinutes .setBorder(border);
             TSeconds  .setBorder(border);

             add(THours);
             add(TMinutes);
             add(new MyLabel("  "));
             add(new MyLabel("hh24:mi"));

   
             THours.addKeyListener(new calc());
             TMinutes.addKeyListener(new calc());
      }
      public class FocListener implements FocusListener
      {
          public void focusLost(FocusEvent fe)
          {
               if (fe.getSource() == TSeconds)
                    {
                          if((THours.getText().length()!=2) || (TMinutes.getText().length()!=2) || (TSeconds.getText().length()!=2))
                              {
                                   THours.requestFocus();
                              }
                    }
               if (fe.getSource() == TMinutes)
                    {
                          if((THours.getText().length()!=2) || (TMinutes.getText().length()!=2) || (TSeconds.getText().length()!=2))
                              {
                                   THours.requestFocus();
                              }
                    }
               if (fe.getSource() == THours)
                    {
                          if((THours.getText().length()!=2) || (TMinutes.getText().length()!=2) || (TSeconds.getText().length()!=2))
                              {
                                   THours.requestFocus();
                              }
                    }
          }
          public void focusGained(FocusEvent fe)
          {
          }
      }

      public class calc extends KeyAdapter
      {
           public void keyReleased(KeyEvent KE)
           {
               if(!mode)
                  return;
               int Num=0;
               if(KE.getSource()==THours)
               {
                   String str1 = THours.getText();
                   try  {  Num = Integer.parseInt(str1);  }
                   catch(NumberFormatException NFE) { THours.setText("");   }
      
                   if (str1.length()==2 || str1.length() > 2)
                   {

                       if (Num>23)
                       {
                            JOptionPane.showMessageDialog(null,"Hours Range 0-24","Error Message",JOptionPane.INFORMATION_MESSAGE);
                            THours.setText("");
                            str1 = THours.getText();
                       }
                       else if(Num<=23)
                       {
                          //TMinutes.setText(""); 
                          TMinutes.requestFocus();
                       }
                   }
               }
               if(KE.getSource()== TMinutes)
               {
                   String str2 = TMinutes.getText();
                   String str1 = THours.getText();
                   try
                   {
                       Minutes = Integer.parseInt(str2);
                       Hours   = Integer.parseInt(str1);
                       if(Hours<10)
                       {
                           THours.setText("0"+String.valueOf(Hours));
                       }
                   }
                   catch(NumberFormatException NFE) {  TMinutes.setText("");    }
                   if (str2.length()==2  || str2.length() > 2)
                   {
                       if (Minutes>59 || Minutes<0)
                       {
                           JOptionPane.showMessageDialog(null,"Minutes Range 0-59","Error Message",JOptionPane.INFORMATION_MESSAGE);
                            TMinutes.setText("");
                            str2 = TMinutes.getText();
                       }
                       else if(Minutes>=0 && Minutes<=59)
                       {
                          //TSeconds.setText(""); 
                          TSeconds.requestFocus();
                       }
                   }
               }
               if(KE.getSource()== TSeconds)
               {
                   String str2 = TSeconds.getText();
                   String str1 = TMinutes.getText();
                   try
                   {
                       Seconds   = Integer.parseInt(str2);
                       Minutes   = Integer.parseInt(str1);
                       if(Minutes<10)
                       {
                           TMinutes.setText("0"+String.valueOf(Minutes));
                       }
                   }
                   catch(NumberFormatException NFE) {  TSeconds.setText("");    }
                   if (str2.length()==2  || str2.length() > 2)
                   {
                       if (Seconds>59 || Seconds<0)
                       {
                           JOptionPane.showMessageDialog(null,"Second Rang 0-59","Error Message",JOptionPane.INFORMATION_MESSAGE);
                            TSeconds.setText("");
                            str2 = TSeconds.getText();
                       }
                       else if(Minutes>=0 && Minutes<=59)
                       {
                          TSeconds.requestFocus();
                       }
                   }
               }

           }
      }
      public String toString()
      {
            if(THours.getText().equals("") || TMinutes.getText().equals(""))
               return "null";
            return THours.getText()+":"+TMinutes.getText();
      }
      public String toNormal()
      {
            return TSeconds.getText()+TMinutes.getText()+THours.getText();
      }
      public void fromString(String str)
      {
            int iSeconds=0,iMinutes=0,iHours=0;
            try
            {
                  //iSeconds  = common.toInt(str.substring(6,10));
                  iMinutes = common.toInt(str.substring(3,5));
                  iHours   = common.toInt(str.substring(0,2));
            }
            catch(Exception ex)
            {
                  return;
            }

            String SHours   = (iHours<10 ?"0"+iHours:""+iHours);
            String SMinutes = (iMinutes<10 ?"0"+iMinutes:""+iMinutes);
            //String SSeconds  = ""+iSeconds;

            THours.setText(SHours);
            TMinutes.setText(SMinutes);
            //TSeconds.setText(SSeconds);
      }
      public void fromPureString(String str)
      {
            System.out.println(str);
            int iSeconds=0,iMinutes=0,iHours=0;
            try
            {
                  iHours   = common.toInt(str.substring(0,2));
                  iMinutes = common.toInt(str.substring(3,5));
                  iSeconds  = common.toInt(str.substring(6,8));
            }
            catch(Exception ex)
            {
//                ex.printStackTrace();
                  return;
            }
            THours.setText(iHours<10 ?"0"+iHours:""+iHours);
            TMinutes.setText(iMinutes<10 ?"0"+iMinutes:""+iMinutes);
            //TSeconds.setText(iSeconds<10 ?"0"+iSeconds:""+iSeconds);
      }
      public void setEditable(boolean flag)
      {
            THours.setEditable(flag);
            TMinutes.setEditable(flag);
            TSeconds.setEditable(flag);
            mode=flag;
      }
}
