
/*
     The Subclass of the Balance Bean to capture the weight of the content
     placed on a balance
*/

package client.key;
import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;

import util.*;
public class KeySerialReader implements SerialPortEventListener,Runnable
{
     protected JLabel theLabel;
     protected JButton BOkay;
     protected JLabel  LNet;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;

     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     int    ctr=0;
     int    iMultiply = 1;
     boolean heartAttack = false;
     StringBuffer inputBuffer = new StringBuffer();

     public KeySerialReader(JLabel theLabel,JButton BOkay)
     {
          this.theLabel   = theLabel;
          this.BOkay      = BOkay;          

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          theLabel.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              System.out.println("Weight"+(char)newData);

                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setKeyCode(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                if(ctr < 10)
                                  outputStream.write((int)'a');
                                else
                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }
     private void setKeyCode(String str)
     {
          try
          {
               theLabel.setText(str);
          }
          catch(Exception ex){}
     }
     public void freeze()
     {
          serialPort.close();
          theLabel.setForeground(new Color(0,174,174));
          BOkay.setEnabled(true);
     }   
}