package util;

import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NextField extends JTextField
{
   public NextField()
   {
      addKeyListener(new KeyList());
   }
   public NextField(int size)
   {
      super(size);
      addKeyListener(new KeyList());
   }
   public class KeyList implements KeyListener
   {
      public void keyPressed(KeyEvent ke){}
      public void keyTyped(KeyEvent ke){}
      public void keyReleased(KeyEvent ke)
      {
         try
         {
            Float.parseFloat(getText());
         }
         catch(NumberFormatException nfe)
         {
            setText("");
         }
      }
   }
}

