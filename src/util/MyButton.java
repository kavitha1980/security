package util;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class MyButton extends JButton
{
     public MyButton()
     {
          setBackground(new Color(79,157,157));
          setForeground(new Color(255,255,255));
          setIcon(new ImageIcon("e:\\schedule\\save.gif"));
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }

     public MyButton(String str)
     {
          super(str);
          setIcon(new ImageIcon("e:/schedule/save.gif"));
          setBackground(new Color(79,157,157));
          setForeground(new Color(255,255,255));
          addMouseListener(new MouseList());
          addFocusListener(new FocusList());
     }

     private class MouseList extends MouseAdapter
     {
          public void mouseEntered(MouseEvent me)
          {
               enhance();
          }
          public void mouseExited(MouseEvent me)
          {
               deEnhance();
          }
     }

     private class FocusList extends FocusAdapter
     {
          public void focusGained(FocusEvent fe)
          {
               enhance();
          }
          public void focusLost(FocusEvent fe)
          {
               deEnhance();
          }
     }

     private void enhance()
     {
          setBackground(new Color(255,255,255));
          setForeground(new Color(139,197,197));
     }
     private void deEnhance()
     {
          setBackground(new Color(79,157,157));
          setForeground(new Color(255,255,255));
     }
}
