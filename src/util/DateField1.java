package util;

import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;
//import util.Common;

public class DateField1 extends JPanel
{

    protected int iStDate,iEnDate;
    protected boolean bFlag=false;
    String SDate="";

    public JTextField theDate;
    Common common = new Common();

    public DateField1()
    {
        iStDate = 0;
        iEnDate = 0;
        bFlag = false;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate.setBackground(Color.pink);

        theDate.addFocusListener(new FocusList());
    }

    public DateField1(int iStDate,int iEnDate)
    {
        this.iStDate = iStDate;
        this.iEnDate = iEnDate;
        bFlag = true;

        theDate = new JTextField(8);
        setLayout(new FlowLayout(0,0,0));
        add(theDate);

        theDate.setBackground(Color.pink);

        theDate.addFocusListener(new FocusList());
    }

    private class FocusList extends FocusAdapter
    {

        public void focusLost(FocusEvent fe)
        {
            String str = theDate.getText();
            str = common.pureDate(common.getDate(str,0,1));

            if(common.toInt(str)==0)
                setTodayDate();
            else
                theDate.setText(common.parseDate(str));

            if(bFlag)
            {
                if(isBeyond())
                {
                    JOptionPane.showMessageDialog(null,"Date Falls Beyond the Finance Year","Error Message",JOptionPane.INFORMATION_MESSAGE);                    
                    setTodayDate();
                }
            }
        }
    }

    /*public String getServerDate()
     {
          String SQry=" Select to_char(Sysdate,'dd.mm.yyyy') from Dual ";
          try
          {

               Class.forName("oracle.jdbc.OracleDriver");
               Connection   theConnection     = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrd0405","personnel0405");
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
                    return theResult.getString(1);
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return "";
     }*/

    public String getServerDate()
     {
          String SQry=" Select to_char(Sysdate,'dd.mm.yyyy') from Dual ";
          try
          {

               Class.forName("oracle.jdbc.OracleDriver");
               Connection   theConnection     = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
               if(theResult.next())
                    return theResult.getString(1);
               theStatement.close();     
          }
          catch(Exception ex)
          {
               return "";
          }
          return "";
     }

    private boolean isBeyond()
    {

        int iDate = common.toInt(toNormal());

        if(iDate >= iStDate && iDate <= iEnDate)
            return false;

        return true;
    }

    //-- Data Accessor methods---//

    public void setEditable(boolean bFlag)
    {
        theDate.setEditable(bFlag);
    }
    public void setEnabled(boolean bFlag)
    {
        theDate.setEnabled(bFlag);
    }
    public void setTodayDate()
    {
        String SDay,SMonth,SYear;
        Date dt = new Date();
        int iDay   = dt.getDate();
        int iMonth = dt.getMonth()+1;
        int iYear  = dt.getYear()+1900;


        if(iDay < 10)
           SDay = "0"+iDay;
        else
           SDay = String.valueOf(iDay);

        if(iMonth < 10)
           SMonth = "0"+iMonth;
        else
           SMonth = String.valueOf(iMonth);

        SYear = String.valueOf(iYear);


        theDate.setText(getServerDate());
        //theDate.setText(SDay+"."+SMonth+"."+SYear);

    }

    public String toNormal()
    {
        return common.pureDate(theDate.getText());
    }

    public int toNumber()
    {
        return common.toInt(common.pureDate(theDate.getText()));
    }

    public String toString()
    {
        return theDate.getText();
    }
    public void fromString(String str)
    {
        theDate.setText(common.parseDate(str));
    }

    public void setString(String str)
    {
        theDate.setText(str);
    }

}
