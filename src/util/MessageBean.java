package util;

import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;
import java.sql.*;
import java.lang.*;
import util.*;


public class MessageBean

{
     Connection connection;
     Statement statement;

     Vector VCategory,VHod,VAgent,VQuali;
     RowSet rowset ;

     String QS;
     MessageBean()
     {
          rowset    = new RowSet();
          VCategory = new Vector();
          VHod      = new Vector();
          VAgent    = new Vector();
          VQuali    = new Vector();
          
          
     }
     public RowSet getCategory()
     {
          ResultSet result=null;
          QS      = "Select CategoryCode,CategoryName from Category";
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               connection = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               statement           = connection.createStatement();
               result              = statement.executeQuery(QS);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return getRowSet(result);
     }
     public RowSet getRowSet(ResultSet resultset)
     {
          Vector VResult = new Vector();
          int iCount =0;
          try
          {
               ResultSetMetaData rsmd   = resultset.getMetaData();
               iCount               = rsmd.getColumnCount();
     
               String SName[]  = new String[iCount];
               
               for(int i=0;i<iCount;i++)
               {
                    SName[i]            = rsmd.getColumnName(i);
               }
               rowset.setColumnName(SName);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          try
          {
               while(resultset.next())
               {
                    Vector vect = new Vector();
                    for(int i=0;i<iCount;i++)
                    {
                         vect.addElement(resultset.getString(i+1));
                    }
                    rowset.appendRow(vect);
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return rowset;
     }
}
