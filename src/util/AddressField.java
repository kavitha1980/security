package util;

import javax.swing.*; 
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;
import java.awt.event.*;

public class AddressField extends JTextField {

     int iColumns = 40;


    public AddressField()
	{
          super();
          setFont(new Font("",Font.BOLD,11));   
          setBackground(new Color(255,217,179));
          addFocusListener(new FocusList());
	}

    public AddressField(String value, int columns) {
        super(columns);
		iColumns = columns;
        setText(value);
        setFont(new Font("",Font.BOLD,11));   
        setBackground(new Color(255,217,179));
        addFocusListener(new FocusList());
    }

    public AddressField(int columns) {
        super(columns);
        iColumns = columns;
        setFont(new Font("",Font.BOLD,11));   
        //setBackground(new Color(255,217,179));
        addFocusListener(new FocusList());

    }
    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }


    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++)
            {
                    result[j++] = source[i];
            }
            super.insertString(offs, (new String(result, 0, j)).toUpperCase(), a);
        }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            setCaretPosition(0);
            moveCaretPosition(getText().length());
        }
    }
}
