package util;

import javax.swing.*;
import java.awt.event.*;
import java.util.Date;
import java.text.*;

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;
import java.awt.event.*;

public class ClockField extends JTextField implements ActionListener
{
     private Timer timer;


     public ClockField()
     {
		super();
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));
          timer = new Timer(1000,this);
          timer.setInitialDelay(0);
          timer.start();
     }

     public void actionPerformed(ActionEvent ae)
     {

          if(ae.getSource()==timer)
          {
               try
               {
                    Date t = new Date();
                    DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM);
//                    DateFormat df = DateFormat.getTimeInstance("HH:mm:ss");
                    String time = df.format(t);
                    setText(time);
               }
               catch(Exception ex)
               {
               }
          }
     }
     
}
/*
        formatter = new SimpleDateFormat ("HH:mm:ss", Locale.getDefault());
        SimpleDateFormat formatter = new SimpleDateFormat("s",Locale.getDefault());
        formatter.applyPattern("HH:mm:ss");
*/
