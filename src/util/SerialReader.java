
/*
     The Subclass of the Balance Bean to capture the weight of the content
     placed on a balance
*/

package util;

import java.awt.Color;
import javax.swing.*;
import javax.comm.*;
import java.io.*;
import java.util.*;

import util.*;
public class SerialReader implements SerialPortEventListener,Runnable
{
     protected JLabel theLabel;
     protected JButton BOkay;
     protected FractionNumberField TTare;
     protected MyLabel LGWeight;
     //protected NextField TGross,TTare;
     protected JLabel  LNet;

     static CommPortIdentifier portId;
     static Enumeration portList;

     SerialPort serialPort;
     InputStream inputStream;
     OutputStream outputStream;

     Thread  readThread;
     Common common = new Common();

     String SPort     = "COM1";
     String strBuffer = "";
     int    ctr=0;
     int    iMultiply = 1;
     boolean heartAttack = false;
     StringBuffer inputBuffer = new StringBuffer();

     public SerialReader(JLabel theLabel,JButton BOkay,MyLabel LGWeight,FractionNumberField TTare,JLabel LNet,int iMultiply)
     {
          this.theLabel   = theLabel;
          this.BOkay      = BOkay;
          this.LGWeight   = LGWeight;
          this.TTare      = TTare;
          this.LNet       = LNet;
          this.iMultiply  = iMultiply;

          createComponents();
          readThread = new Thread(this);
          readThread.start();
     }

     private void createComponents()
     {
          theLabel.setForeground(new Color(255,94,94));
          ctr=0;
          if(serialPort != null)
               return;
          try
          {
               portList = CommPortIdentifier.getPortIdentifiers();          
               while(portList.hasMoreElements())
               {
                    portId = (CommPortIdentifier)portList.nextElement();
                    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL && portId.getName().equals(SPort))
                    {
                         serialPort = (SerialPort)portId.open("comapp",2000);
                         break;
                    }
               }

               if(serialPort != null)
               {
                    serialPort.setSerialPortParams(2400, SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                    serialPort.addEventListener(this);
                    serialPort.notifyOnDataAvailable(true);
                    serialPort.enableReceiveTimeout(30);
                    inputStream  = serialPort.getInputStream();
                    outputStream = serialPort.getOutputStream();
                    outputStream.write((int)'a');
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     
     public void run()
     {
          try
          {
               Thread.sleep(2000);
          }
          catch (Exception e){}
     }

     public void serialEvent(SerialPortEvent event)
     {

          int newData = 0;

          switch(event.getEventType())
          {
               case SerialPortEvent.BI:
               case SerialPortEvent.OE:
               case SerialPortEvent.FE:
               case SerialPortEvent.PE:
               case SerialPortEvent.CD:
               case SerialPortEvent.CTS:
               case SerialPortEvent.DSR:
               case SerialPortEvent.RI:
               case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
               case SerialPortEvent.DATA_AVAILABLE:
                    try
                    {
                         while (newData != -1)
                         {
                              newData = inputStream.read();
                              if(newData == -1)
                                   break;
                              inputBuffer.append((char)newData);
                              if((char)newData=='')
                                   heartAttack = true;
                         }
                         if(heartAttack)
                         {
                                setWeight(new String(inputBuffer));
                                heartAttack = false;
                                inputBuffer = new StringBuffer();
                                if(ctr < 10)
                                  outputStream.write((int)'a');
                                else
                                  freeze();
                         }                                
                    }
                    catch (Exception e)
                    {
                    }
          }
     }

     private void setWeight(String str)
     {
          try
          {
               int index1 = str.indexOf("+");
               int index2 = str.indexOf("g");
               String xtr = str.substring(index1+1,index2);
               theLabel.setText( common.getRound( (common.toDouble(xtr.trim())/iMultiply )  ,3) );
               LGWeight.setText(theLabel.getText());
               ctr++;
               setNetWeight();
          }
          catch(Exception ex){}
     }

     public void freeze()
     {
          serialPort.close();
          theLabel.setForeground(new Color(0,174,174));
          BOkay.setEnabled(true);
          setNetWeight();
     }

     private void setNetWeight()
     {
          LNet.setText(common.getRound(common.toDouble(LGWeight.getText())-common.toDouble(TTare.getText()),3));
     }

}
