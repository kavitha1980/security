package util;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends JPanel
{
     public JLabel LLabel,MLabel,RLabel;
     JPanel MPanel;

     public StatusPanel()
     {
          LLabel = new JLabel(" ",JLabel.LEFT);
          RLabel = new JLabel("Developed By FIPL for Amarjothi Spinning Mills Ltd",new ImageIcon("cupanim.gif"),Label.RIGHT);
          MLabel = new JLabel(" ",JLabel.CENTER);
          MPanel = new JPanel(true);

          setLayout(new GridLayout(1,4));

          add(LLabel);
          add(MPanel);
          add(MLabel);
          add(RLabel);
     }
}
