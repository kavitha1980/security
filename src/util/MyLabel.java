package util;

import javax.swing.*;
import java.awt.*;

public class MyLabel extends JLabel
{
  public MyLabel()
  {
     super();
     setFont(new Font("Arial",Font.BOLD,11));
  }
  public MyLabel(String SName)
  {
     setText(SName);
     setFont(new Font("Arial",Font.BOLD,11));
  }
  public MyLabel(String SName,int iAlign)
  {
     setText(SName);
     setHorizontalAlignment(iAlign);
     setFont(new Font("Arial",Font.BOLD,11));
  }

}
