/*
     A general information notifier
     about a process status to the user. 
*/

package Reports.Guest;
import java.io.*;
import java.util.Vector;

public class GuestIndividualAsOnInfo
{

     GuestIndividualAsOnInfo()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
     
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='300' height='20' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='300' height='30' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector GHead        = (Vector)VPrint.elementAt(0);
          Vector VInDate      = (Vector)VPrint.elementAt(1);
          Vector VGuestName   = (Vector)VPrint.elementAt(2);
          Vector VGuestPlace  = (Vector)VPrint.elementAt(3);
          Vector VSex         = (Vector)VPrint.elementAt(4);
          Vector VNoPersons   = (Vector)VPrint.elementAt(5);
          Vector VToMeet      = (Vector)VPrint.elementAt(6);
          Vector VDepartment  = (Vector)VPrint.elementAt(7);
          Vector VOutDate     = (Vector)VPrint.elementAt(8);
          Vector VInTime      = (Vector)VPrint.elementAt(9);
          Vector VOutTime     = (Vector)VPrint.elementAt(10);
          Vector VSpentTime   = (Vector)VPrint.elementAt(11);


          out.println(" <table border='1' width='1500' height='15' cellpadding='0' cellspacing='0'   >");
          int m=0;
          for(int i=0;i<VInDate.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<GHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='30' bgcolor='#9aadff' >"+(String)GHead.elementAt(j)+"</td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                    out.println(" <tr>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VGuestName.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VGuestPlace.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSex.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VNoPersons.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VToMeet.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDepartment.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOutDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInTime.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOutTime.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSpentTime.elementAt(i)+"</td>");
                    
                    out.println(" </tr>");
                    
               }
          
          out.println("</table>");


          out.println("</body>");
          out.println("</html>");
     }

}
