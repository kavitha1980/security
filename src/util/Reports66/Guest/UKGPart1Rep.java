import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class UKGPart1Rep extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,V14,V15,V16,V17,V18;
     Vector VSPDate,VSProd;
     double dProd=0;
     double dTUnits;
     String SStDate,SEnDate,SUnit;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;

     String SFOpening="",SFPurc="",SFTrIn="",SFTrOut="",SFCons="";
     String SFClosing="",STUnits="",STUnitsWOWeb="",STProd="",STUKg="",STUKgWOWeb="";
     String STBWebUnits="",STBSampleUnits="",STEBGenUnits="",STPTr1="",STPTr2="",STPTr3="",STPTr4="";

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          SStDate            = (request.getParameter("T1")).trim();
          SStDate            = common.pureDate(SStDate);
          SEnDate            = (request.getParameter("T2")).trim();
          SEnDate            = common.pureDate(SEnDate);
          SUnit            = (request.getParameter("unit")).trim();
          setVector();

          try
          {
               FW      = new FileWriter("D:/Electric/Reports/Month1.prn");
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}

          out.println("<html>");
          out.println("<head>");
          out.println("<title>Elect</title>");
          out.println("</head>");
          out.println("<body bgcolor='"+bgColor+"'>");
          out.println("<p align='center'><font size='5' color='#0000FF'><b>Power House Report For The");
          out.println("Period From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" (ELC - 43)</b></font></p>");
          out.println("<div align='center'>");
          out.println("  <center>");
          out.println("  <table border='0' width='1700' height='34' bordercolor='"+bgColor+"'>");
          out.println("    <tr>");
          out.println("      <td width='100' height='34' rowspan='3' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Date</font></p>");
          out.println("      </td>");
          out.println("      <td width='300' height='-6' colspan='3' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Fuel Stock</font></td>");
          out.println("      <td width='100' height='34' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Opening Balance</font></td>");
          out.println("      <td width='100' height='34' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Purchase</font></td>");
          out.println("      <td width='200' height='19' colspan='2' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Fuel Transfer</font></td>");
          out.println("      <td width='100' height='34' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Genset Consumption</font></td>");
          out.println("      <td width='100' height='34' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Closing Balance</font></td>");
          out.println("      <td width='100' height='34' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>EB Units</font></td>");
          if(common.toInt(SUnit)==1)
          {
               out.println("      <td width='300' height='19' colspan='3' bgcolor='"+bgHead+"'>");
               out.println("        <p align='center'><font color='"+fgHead+"'>Power Transfer</font></td>");
          }
          if(common.toInt(SUnit)==2)
          {
               out.println("      <td width='300' height='19' colspan='4' bgcolor='"+bgHead+"'>");
               out.println("        <p align='center'><font color='"+fgHead+"'>Power Transfer</font></td>");
               out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>B Web Units</font></td>");
          }
          out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Total Units With Web </font></td>");
          out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Total Units Without Web</font></td>");
          out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Spinning Production</font></td>");
          out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Units/Kg With Web</font></td>");
          out.println("      <td width='100' height='38' rowspan='3' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Units/Kg Without Web</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='100' height='38' rowspan='2' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Fuel Tank</font></td>");
          out.println("      <td width='100' height='32' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>@ Generators</font></td>");
          out.println("      <td width='100' height='32' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Total Stock</font></td>");
          out.println("      <td width='100' height='32' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>From Another Unit</font></td>");
          out.println("      <td width='100' height='32' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>To Another Unit</font></td>");
          if(common.toInt(SUnit)==1)
          {
               out.println("      <td width='200' height='38' colspan='2' bgcolor='"+bgHead+"'>");
               out.println("        <p align='center'><font color='"+fgHead+"'>Input</font></td>");
               out.println("      <td width='100' height='38' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Output</font></td>");
          }
          if(common.toInt(SUnit)==2)
          {
               out.println("      <td width='100' height='38' rowspan='2'  align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Input</font></td>");
               out.println("      <td width='200' height='38' colspan='3' bgcolor='"+bgHead+"'>");
               out.println("        <p align='center'><font color='"+fgHead+"'>Output</font></td>");
          }
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Web</font></td>");
          out.println("      <td width='100' height='32' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Compressor</font></td>");
          if(common.toInt(SUnit)==2)
          {
               out.println("      <td width='100' height='32' align='right' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>SampleUnit</font></td>");
          }
          out.println("    </tr>");

          dProd=0;

          for(int i=0;i<V1.size();i++)
          {
               double dTotUnits    = common.toDouble((String)V15.elementAt(i));
               double dTWOWebUnits = common.toDouble((String)V16.elementAt(i));
               String SForty       = common.getRound(getForty((String)V1.elementAt(i)),2);
               String SUKg         = common.getRound(dTotUnits/common.toDouble(SForty),3);
               String SUKgWOWeb    = common.getRound(dTWOWebUnits/common.toDouble(SForty),3);
               dProd               = dProd+common.toDouble(SForty);

               out.println("    <tr>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V1.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V2.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='32' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V3 .elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='32' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V4.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V5.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V6.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='32' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V7.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='32' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V8.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V9.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V10.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V11.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='34' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V12.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V13.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V14.elementAt(i)+"</font></td>");
               if(common.toInt(SUnit)==2)
               {
                    out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V18.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V17.elementAt(i)+"</font></td>");
               }
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V15.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V16.elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SForty+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SUKg+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SUKgWOWeb+"</font></td>");
               out.println("    </tr>");
          }
          out.println("    <tr>");
          out.println("      <td width='100' height='34' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>T O T A L</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
          out.println("      <td width='100' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
          out.println("      <td width='100' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'></font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFOpening+"</font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFPurc+"</font></td>");
          out.println("      <td width='100' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFTrIn+"</font></td>");
          out.println("      <td width='100' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFTrOut+"</font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFCons+"</font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SFClosing+"</font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STEBGenUnits+"</font></td>");
          out.println("      <td width='100' height='34' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STPTr1+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STPTr2+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STPTr3+"</font></td>");
          if(common.toInt(SUnit)==2)
          {
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STBWebUnits+"</font></td>");
               out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STBSampleUnits+"</font></td>");
          }
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STUnits+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STUnitsWOWeb+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STProd+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STUKg+"</font></td>");
          out.println("      <td width='100' height='38' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STUKgWOWeb+"</font></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");
          out.println("");
          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");
          out.close();
     }
     public void setVector()
     {
           V1    = new Vector();
           V2    = new Vector();
           V3    = new Vector();
           V4    = new Vector();
           V5    = new Vector();
           V6    = new Vector();
           V7    = new Vector();
           V8    = new Vector();
           V9    = new Vector();
           V10   = new Vector();
           V11   = new Vector();
           V12   = new Vector();
           V13   = new Vector();
           V14   = new Vector();
           V15   = new Vector();
           V16   = new Vector();
           V17   = new Vector();
           V18   = new Vector();

           VSPDate = new Vector();
           VSProd  = new Vector();

           String QS="";
 
           String QS1 = "SELECT [FuelLedger].[FuelDate], Sum([FuelLedger].[Purchase]) AS Purchase, Sum([FuelLedger].[Consumption]) AS Consumption, Sum([FuelLedger].[FTLevel]) AS FTLevel INTO f2 "+
                        " FROM FuelLedger "+
                        " WHERE ((([FuelLedger].[PowerId])>0)) "+
                        " GROUP BY [FuelLedger].[FuelDate], [FuelLedger].[Unit_Code] "+
                        " HAVING (((FuelLedger.FuelDate)>='"+SStDate+"' And (FuelLedger.FuelDate)<='"+SEnDate+"') AND ((FuelLedger.Unit_Code)="+SUnit+"))";
 
           String QS2 = "SELECT [FuelJournal].[FuelDate], Sum([FuelJournal].[Debit]) AS Income, Sum([FuelJournal].[Credit]) AS Expen INTO f3 "+
                        " FROM FuelJournal "+
                        " GROUP BY [FuelJournal].[FuelDate], [FuelJournal].[Unit_Code] "+
                        " HAVING (((FuelJournal.FuelDate)>='"+SStDate+"' And (FuelJournal.FuelDate)<='"+SEnDate+"') AND ((FuelJournal.Unit_Code)="+SUnit+"))";
 
           String QS3 = "SELECT [TNEBDet].[EBDate], Sum(Units) AS TNEBUnits, Sum(BWebUnits) AS BWeb INTO p1 "+
                        " FROM TNEBDet "+
                        " GROUP BY [TNEBDet].[EBDate], [TNEBDet].[Unit_Code] "+
                        " HAVING (((TNEBDet.EBDate)>='"+SStDate+"' And (TNEBDet.EBDate)<='"+SEnDate+"') AND ((TNEBDet.Unit_Code)="+SUnit+"))";
 
           String QS4 = "SELECT [PowerTranDet].[TrnDate], Sum([PowerTranDet].[Debit]) AS WebDebit, Sum([PowerTranDet].[Credit]) AS WebCredit INTO p2 "+
                        " FROM PowerTranDet "+
                        " Where PowerTranDet.TrnCode<>2 "+
                        " GROUP BY [PowerTranDet].[TrnDate], [PowerTranDet].[Unit_Code] "+
                        " HAVING (((PowerTranDet.TrnDate)>='"+SStDate+"' And (PowerTranDet.TrnDate)<='"+SEnDate+"') AND ((PowerTranDet.Unit_Code)="+SUnit+"))";

           String QS5 = "SELECT [PowerTranDet].[TrnDate], Sum([PowerTranDet].[Debit]) AS CompDebit, Sum([PowerTranDet].[Credit]) AS CompCredit INTO p3 "+
                        " FROM PowerTranDet "+
                        " Where PowerTranDet.TrnCode=2 "+
                        " GROUP BY [PowerTranDet].[TrnDate], [PowerTranDet].[Unit_Code] "+
                        " HAVING (((PowerTranDet.TrnDate)>='"+SStDate+"' And (PowerTranDet.TrnDate)<='"+SEnDate+"') AND ((PowerTranDet.Unit_Code)="+SUnit+"))";

           String QS6 = "SELECT [GeneratorDet].[GenDate], Sum([GeneratorDet].[GFLevel]) AS GenStock, Sum(GeneratorDet.GenUnits) AS GenUnits INTO g1 "+
                        " FROM GeneratorDet "+
                        " GROUP BY [GeneratorDet].[GenDate], [GeneratorDet].[Unit_Code] "+
                        " HAVING (((GeneratorDet.GenDate)>='"+SStDate+"' And (GeneratorDet.GenDate)<='"+SEnDate+"') AND ((GeneratorDet.Unit_Code)="+SUnit+"))";
 
           String QS7 = "SELECT [spintran].[sp_date],Sum([spintran].[prod]) AS SpinProd "+
                        " FROM spintran "+
                        " GROUP BY [spintran].[sp_date], [spintran].[Unit_Code] "+
                        " HAVING (((spintran.sp_date)>='"+SStDate+"' And (spintran.sp_date)<='"+SEnDate+"') AND ((spintran.Unit_Code)="+SUnit+"))";

           String QS8 = "SELECT [TNEBDet].[EBDate], Sum(Units) AS TNEBUnits, Sum(BSampleUnit) AS BSample INTO p8 "+
                        " FROM TNEBDet "+
                        " GROUP BY [TNEBDet].[EBDate], [TNEBDet].[Unit_Code] "+
                        " HAVING (((TNEBDet.EBDate)>='"+SStDate+"' And (TNEBDet.EBDate)<='"+SEnDate+"') AND ((TNEBDet.Unit_Code)="+SUnit+"))";

           QS  = "SELECT f2.FuelDate, f2.FTLevel, g1.GenStock, f2.Purchase, f3.Income, f3.Expen, f2.Consumption, g1.GenUnits, p1.TNEBUnits, p2.WebDebit, p2.WebCredit, p3.CompDebit, p3.CompCredit, p1.BWeb ,p8.BSample "+
                 " FROM (((((f2 INNER JOIN f3 ON f2.FuelDate = f3.FuelDate) INNER JOIN p1 ON f2.FuelDate = p1.EBDate) INNER JOIN p2 ON f2.FuelDate = p2.TrnDate) INNER JOIN p3 ON f2.FuelDate = p3.TrnDate) INNER JOIN g1 ON f2.FuelDate = g1.GenDate) INNER JOIN p8 ON f2.FuelDate = p8.EBDate";

            try
            {
                    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                    Connection con = DriverManager.getConnection("jdbc:odbc:Inventory","","");
                    Statement stat = con.createStatement();
                    ResultSet res1 = stat.executeQuery(QS7);
                    while(res1.next())
                    {
                         VSPDate.addElement(res1.getString(1));
                         VSProd.addElement(res1.getString(2));
                    }
                    con.close();
            }
            catch(Exception ex)
            {
                 System.out.println(ex);
            }

            try
            {
                    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                    Connection con = DriverManager.getConnection("jdbc:odbc:Inventory","","");
                    Statement stat = con.createStatement();

                    try{stat.execute("Drop Table f2");}catch(Exception ex){}
                    try{stat.execute("Drop Table f3");}catch(Exception ex){}
                    try{stat.execute("Drop Table p1");}catch(Exception ex){}
                    try{stat.execute("Drop Table p2");}catch(Exception ex){}
                    try{stat.execute("Drop Table p3");}catch(Exception ex){}
                    try{stat.execute("Drop Table g1");}catch(Exception ex){}
                    try{stat.execute("Drop Table p8");}catch(Exception ex){}
          
                    stat.execute(QS1);
                    stat.execute(QS2);
                    stat.execute(QS3);
                    stat.execute(QS4);
                    stat.execute(QS5);
                    stat.execute(QS6);
                    stat.execute(QS8);

                    ResultSet res = stat.executeQuery(QS);
                    while(res.next())
                    {
                         String SDate      = res.getString(1);
                         String SFTLevel   = common.getRound(res.getString(2),0);
                         String SGenStock  = common.getRound(res.getString(3),0);
                         String SFPurc     = common.getRound(res.getString(4),0);
                         String SFIn       = common.getRound(res.getString(5),0);
                         String SFOut      = common.getRound(res.getString(6),0);
                         String SFCons     = common.getRound(res.getString(7),0);
                         String SGenUnits  = common.getRound(res.getString(8),0);
                         String SEBUnits   = common.getRound(res.getString(9),0);
                         String SPWebIn    = common.getRound(res.getString(10),2);
                         String SPWebOut   = common.getRound(res.getString(11),2);
                         String SPCompIn   = common.getRound(res.getString(12),2);
                         String SPCompOut  = common.getRound(res.getString(13),2);
                         String SBWebUnits = common.getRound(res.getString(14),2);
                         String SBSampleUnits = common.getRound(res.getString(15),2);

                         double dPIn        = common.toDouble(SPWebIn)+common.toDouble(SPCompIn);
                         double dPOut       = common.toDouble(SPWebOut)+common.toDouble(SPCompOut);

                         double dFTotal     = common.toDouble(SFTLevel)+common.toDouble(SGenStock);
                         double dFOpening   = dFTotal-common.toDouble(SFPurc)-common.toDouble(SFIn)+common.toDouble(SFOut)+common.toDouble(SFCons);
                         double dFClosing   = dFOpening+common.toDouble(SFPurc)+common.toDouble(SFIn)-common.toDouble(SFOut)-common.toDouble(SFCons);
                         double dEBGenUnits = common.toDouble(SGenUnits)+common.toDouble(SEBUnits);
                         if(common.toInt(SUnit)==1)
                         {
                              dTUnits     = common.toDouble(SGenUnits)+common.toDouble(SEBUnits)+common.toDouble(SPWebIn)+common.toDouble(SPCompIn)-common.toDouble(SPWebOut)-common.toDouble(SPCompOut);
                         }
                         if(common.toInt(SUnit)==2)
                         {
                              dTUnits     = common.toDouble(SGenUnits)+common.toDouble(SEBUnits)+common.toDouble(SPWebIn)+common.toDouble(SPCompIn)-common.toDouble(SPWebOut)-common.toDouble(SPCompOut)-common.toDouble(SBSampleUnits);
                         }
                         double dTUnitWOWeb = 0;
                         if(common.toInt(SUnit)==1)
                         {
                              dTUnitWOWeb = dTUnits - common.toDouble(SPWebIn);
                         }
                         if(common.toInt(SUnit)==2)
                         {
                              dTUnitWOWeb = dTUnits - common.toDouble(SBWebUnits);
                         }
          
                         V1.addElement(common.parseDate(SDate));
                         V2.addElement(SFTLevel);
                         V3.addElement(SGenStock);
                         V4.addElement(common.getRound(dFTotal,0));
                         V5.addElement(common.getRound(dFOpening,0));
                         V6.addElement(SFPurc);
                         V7.addElement(SFIn);
                         V8.addElement(SFOut);
                         V9.addElement(SFCons);
                         V10.addElement(common.getRound(dFClosing,0));
                         V11.addElement(common.getRound(dEBGenUnits,0));

                         if(common.toInt(SUnit)==1)
                         {
                              V12.addElement(SPWebIn);
                              V13.addElement(SPCompIn);
                              V14.addElement(common.getRound(dPOut,2));
                         }
                         if(common.toInt(SUnit)==2)
                         {
                              V12.addElement(common.getRound(dPIn,2));
                              V13.addElement(SPWebOut);
                              V14.addElement(SPCompOut);
                         }

                         V15.addElement(common.getRound(dTUnits,2));
                         V16.addElement(common.getRound(dTUnitWOWeb,2));
                         V17.addElement(SBWebUnits);
                         if(common.toInt(SUnit)==2)
                         {
                              V18.addElement(SBSampleUnits);
                         }
                    }
                    con.close();
            }
            catch(Exception ex)
            {
                 System.out.println(ex);
            }
     }
     public double getForty(String SDate)
     {
         SDate = common.pureDate(SDate);
         int i = VSPDate.indexOf(SDate);
         if(i==-1)
              return 0.00;
         return common.toDouble((String)VSProd.elementAt(i));
     }
     public void getTotal()
     {
          SFOpening     = (String)V5.elementAt(0);
          SFPurc        = common.getSum(V6,"S",0);
          SFTrIn        = common.getSum(V7,"S",0);
          SFTrOut       = common.getSum(V8,"S",0);
          SFCons        = common.getSum(V9,"S",0);
          SFClosing     = common.getRound(common.toDouble(SFOpening)+common.toDouble(SFPurc)+common.toDouble(SFTrIn)-common.toDouble(SFTrOut)-common.toDouble(SFCons),0);
          STUnits       = common.getSum(V15,"S",2);
          STUnitsWOWeb  = common.getSum(V16,"S",2);
          STBWebUnits   = common.getSum(V17,"S",2);
          STProd        = common.getRound(dProd,2);
          STUKg         = common.getRound(common.toDouble(STUnits)/common.toDouble(STProd),3);
          STUKgWOWeb    = common.getRound(common.toDouble(STUnitsWOWeb)/common.toDouble(STProd),3);
          STEBGenUnits  = common.getSum(V11,"S",0);
          STPTr1        = common.getSum(V12,"S",2);
          STPTr2        = common.getSum(V13,"S",2);
          STPTr3        = common.getSum(V14,"S",2);
          STBSampleUnits= common.getSum(V18,"S",2);
     }
     
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
                  String Strl="";
                  if(common.toInt(SUnit)==1)
                  {
                         Strl = "읕컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컨컴컴컴컨컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴좔컴컴컴좔컴컴컴�\n";
                  }
                  if(common.toInt(SUnit)==2)
                  {
                         Strl = "읕컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컨컴컴컴컨컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컴좔컴컴컴컴컴좔컴컴컴컴컴좔컴컴컴컴컨컴컴컴컨컴컴컴켸\n";
                  }
                  FW.write(Strl+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Power House Monthly Report",80);
          String Str3    = common.Pad("Unit       : "+(common.toInt(SUnit)==1?"A":"B"),70);
          String Str4    = common.Pad("Period     : "+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate),80);
          String Str5    = common.Pad("Page No    : "+Pctr,80);

          VHead   =  MainHead();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");
               FW.write(Str5+"\n");

               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 5+VHead.size();
     }
     public Vector MainHead()
     {
          Vector VHead = new Vector();

          String Str1="",Str2="",Str3="",Str4="",Str5="",Str6="",Str7="";

          if(common.toInt(SUnit)==1)
          {
               Str1  = "旼컴컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컴쩡컴컴컴컫컴컴컴컴컴컴컴컫컴컴컴쩡컴컴컴쩡컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컴컴컴컫컴컴컴컴컴컫컴컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴�\n";
               Str2  = "�   Date   �          F U E L  S T O C K          쿚pening쿛urchase� FUEL TRANSFER 쿒enset쿎losing�  E.B.  �        POWER TRANSFER       쿟otal Units쿟otal Units쿞pinning  � Units � Units �\n";
               Str3  = "�          쳐컴컴컴컴컴컫컴컴컴컴컴컴쩡컴컴컴컴컴캑Balance쿝eceived쳐컴컴컴쩡컴컴컴퀰onsum쿍alance�   +    쳐컴컴컴컴컴컴컴컴컴쩡컴컴컴컴� With Web  쿥ithout Web쿛roduction� Per Kg� Per Kg�\n";
               Str4  = "�          �  Fuel Tank �@ Generators� Total Stock�       �        쿑rom   쿟o     �-ption�       �  Gen   �       Input       �  Output �(EB+Gen+   �(Total-Web)�          � With  쿥ithout�\n";
               Str5  = "�          �            �            �            �       �        쿌nother쿌nother�      �       � Units  쳐컴컴컴컴쩡컴컴컴컴�         � Web+Compr)�           �          � Web   �  Web  �\n";
               Str6  = "�          �            �            �            �       �        쿢nit   쿢nit   �      �       �        �    Web  �  Compr. �         �           �           �          �       �       �\n";
               Str7  = "쳐컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컵컴컴컴컵컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴탠컴컴컴탠컴컴컴�\n";
          }
          if(common.toInt(SUnit)==2)
          {
               Str1  = "旼컴컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컴쩡컴컴컴컫컴컴컴컴컴컴컴컫컴컴컴쩡컴컴컴쩡컴컴컴컫컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컴컴컫컴컴컴컴컴컫컴컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴�\n";
               Str2  = "�   Date   �          F U E L  S T O C K          쿚pening쿛urchase� FUEL TRANSFER 쿒enset쿎losing�  E.B.  �        POWER TRANSFER                �   B Web  쿟otal Units쿟otal Units쿞pinning  � Units � Units �\n";
               Str3  = "�          쳐컴컴컴컴컴컫컴컴컴컴컴컴쩡컴컴컴컴컴캑Balance쿝eceived쳐컴컴컴쩡컴컴컴퀰onsum쿍alance�   +    쳐컴컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴컴컴캑   Units  � With Web  쿥ithout Web쿛roduction� Per Kg� Per Kg�\n";
               Str4  = "�          �  Fuel Tank �@ Generators� Total Stock�       �        쿑rom   쿟o     �-ption�       �  Gen   �  Input  �       Output               �          �(EB+Gen-   �(Total -   �          � With  쿥ithout�\n";
               Str5  = "�          �            �            �            �       �        쿌nother쿌nother�      �       � Units  �         쳐컴컴컴컴쩡컴컴컴컴쩡컴컴컴캑          쿌 Web-Compr� B Web)    �          � Web   �  Web  �\n";
               Str6  = "�          �            �            �            �       �        쿢nit   쿢nit   �      �       �        �         �    Web  �  Compr. 쿞am.Uni �          �  - S/U)   �           �          �       �       �\n";
               Str7  = "쳐컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컵컴컴컴컵컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴탠컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴탠컴컴컴탠컴컴컴�\n";
          }

          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(Str6);
          VHead.addElement(Str7);

          return VHead;
     }
     public void Body()
     {
          dProd = 0;
          for(int i=0;i<V1.size();i++)
          {
               double dTotUnits    = common.toDouble((String)V15.elementAt(i));
               double dTWOWebUnits = common.toDouble((String)V16.elementAt(i));
               String SForty       = common.getRound(getForty((String)V1.elementAt(i)),2);
               String SUKg         = common.getRound(dTotUnits/common.toDouble(SForty),3);
               String SUKgWOWeb    = common.getRound(dTWOWebUnits/common.toDouble(SForty),3);
               dProd               = dProd+common.toDouble(SForty);

               String Str1 = "";
               if(common.toInt(SUnit)==1)
               {
                      Str1 = "�"+common.Pad((String)V1.elementAt(i),10)+"�"+common.Rad((String)V2.elementAt(i),12)+
                             "�"+common.Rad((String)V3.elementAt(i),12)+"�"+common.Rad((String)V4.elementAt(i),12)+
                             "�"+common.Rad((String)V5.elementAt(i),7)+"�"+common.Rad((String)V6.elementAt(i),8)+
                             "�"+common.Rad((String)V7.elementAt(i),7)+"�"+common.Rad((String)V8.elementAt(i),7)+
                             "�"+common.Rad((String)V9.elementAt(i),6)+"�"+common.Rad((String)V10.elementAt(i),7)+
                             "�"+common.Rad((String)V11.elementAt(i),8)+"�"+common.Rad((String)V12.elementAt(i),9)+
                             "�"+common.Rad((String)V13.elementAt(i),9)+"�"+common.Rad((String)V14.elementAt(i),9)+
                             "�"+common.Rad((String)V15.elementAt(i),11)+"�"+common.Rad((String)V16.elementAt(i),11)+
                             "�"+common.Rad(SForty,10)+"�"+common.Rad(SUKg,7)+"�"+common.Rad(SUKgWOWeb,7)+"�";
               }
               if(common.toInt(SUnit)==2)
               {
                      Str1 = "�"+common.Pad((String)V1.elementAt(i),10)+"�"+common.Rad((String)V2.elementAt(i),12)+
                             "�"+common.Rad((String)V3.elementAt(i),12)+"�"+common.Rad((String)V4.elementAt(i),12)+
                             "�"+common.Rad((String)V5.elementAt(i),7)+"�"+common.Rad((String)V6.elementAt(i),8)+
                             "�"+common.Rad((String)V7.elementAt(i),7)+"�"+common.Rad((String)V8.elementAt(i),7)+
                             "�"+common.Rad((String)V9.elementAt(i),6)+"�"+common.Rad((String)V10.elementAt(i),7)+
                             "�"+common.Rad((String)V11.elementAt(i),8)+"�"+common.Rad((String)V12.elementAt(i),9)+
                             "�"+common.Rad((String)V13.elementAt(i),9)+"�"+common.Rad((String)V14.elementAt(i),9)+
                             "�"+common.Rad((String)V18.elementAt(i),8)+"�"+common.Rad((String)V17.elementAt(i),10)+
                             "�"+common.Rad((String)V15.elementAt(i),11)+"�"+common.Rad((String)V16.elementAt(i),11)+
                             "�"+common.Rad(SForty,10)+"�"+common.Rad(SUKg,7)+"�"+common.Rad(SUKgWOWeb,7)+"�";
               }

               try
               {
                    FW.write(Str1+"\n");
                    Lctr++;
                    Head();
               }catch(Exception ex){}
          }
          getTotal();
          getTotalHead();
          dProd=0;
          Lctr = 70;
          Pctr = 0;
     }
     public void getTotalHead()
     {
          String Str1="",Str2="",Str3="";

          if(common.toInt(SUnit)==1)
          {
                 Str1 = "쳐컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컵컴컴컴컵컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴탠컴컴컴탠컴컴컴�\n";

                 Str2 = "�"+common.Pad("T O T A L",10)+"�"+common.Space(12)+
                        "�"+common.Space(12)+"�"+common.Space(12)+
                        "�"+common.Rad(SFOpening,7)+"�"+common.Rad(SFPurc,8)+
                        "�"+common.Rad(SFTrIn,7)+"�"+common.Rad(SFTrOut,7)+
                        "�"+common.Rad(SFCons,6)+"�"+common.Rad(SFClosing,7)+
                        "�"+common.Rad(STEBGenUnits,8)+"�"+common.Rad(STPTr1,9)+
                        "�"+common.Rad(STPTr2,9)+"�"+common.Rad(STPTr3,9)+
                        "�"+common.Rad(STUnits,11)+"�"+common.Rad(STUnitsWOWeb,11)+
                        "�"+common.Rad(STProd,10)+"�"+common.Rad(STUKg,7)+"�"+common.Rad(STUKgWOWeb,7)+"�";

                 Str3 = "읕컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컨컴컴컴컨컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴좔컴컴컴좔컴컴컴�\n";
          }
          if(common.toInt(SUnit)==2)
          {
                 Str1 = "쳐컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴컴컴컵컴컴컴컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컵컴컴컴컵컴컴컴탠컴컴컴탠컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴컵컴컴컴컴탠컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴탠컴컴컴탠컴컴컴�\n";

                 Str2 = "�"+common.Pad("T O T A L",10)+"�"+common.Space(12)+
                        "�"+common.Space(12)+"�"+common.Space(12)+
                        "�"+common.Rad(SFOpening,7)+"�"+common.Rad(SFPurc,8)+
                        "�"+common.Rad(SFTrIn,7)+"�"+common.Rad(SFTrOut,7)+
                        "�"+common.Rad(SFCons,6)+"�"+common.Rad(SFClosing,7)+
                        "�"+common.Rad(STEBGenUnits,8)+"�"+common.Rad(STPTr1,9)+
                        "�"+common.Rad(STPTr2,9)+"�"+common.Rad(STPTr3,9)+
                        "|"+common.Rad(STBSampleUnits,8)+"�"+common.Rad(STBWebUnits,10)+
                        "�"+common.Rad(STUnits,11)+"�"+common.Rad(STUnitsWOWeb,11)+
                        "�"+common.Rad(STProd,10)+"�"+common.Rad(STUKg,7)+"�"+common.Rad(STUKgWOWeb,7)+"�";

                 Str3 = "읕컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴컴컴컨컴컴컴컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컨컴컴컴컨컴컴컴좔컴컴컴좔컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴컨컴컴컴컴좔컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴좔컴컴컴탠컴컴컴�\n";

          }

          try
          {
               FW.write(Str1);
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
          }catch(Exception ex){}
     }

}

