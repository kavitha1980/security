package Reports.Guest;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GuestExcessHoursRep extends HttpServlet
{
     HttpSession session;
     
     Common common;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          GuestExcessHoursInformation info    = new GuestExcessHoursInformation();
          
          response.setContentType("text/html");

          PrintWriter out          = response.getWriter();
          session                  = request.getSession(false);
          String SServer           = (String)session.getValue("Server");

          String SValue            = common.parseNull(request.getParameter("t1"));
          String SStDate           = common.pureDate(request.getParameter("TStDate"));
          String SEnDate           = common.pureDate(request.getParameter("TEnDate"));
          String SFile             = common.parseNull(request.getParameter("TFile"));
          String SPrint            = common.parseNull(request.getParameter("Print"));
          
          SFile                    = (SFile.trim()).length()==0?"d:/GuestExcess.prn ":SFile;

          int iValue               = Integer.parseInt(SValue);

          GuestExcessHoursList theList= new GuestExcessHoursList(SStDate,SEnDate,SFile,iValue,SPrint);
        
          out.println(" alert(' Printing ')");

          /* if(SPrint==b3)
          {
               out.println("<html><head>");
               out.println("<script>");
               out.println(" alert(' Printing ');");
               out.println(" </script>");
               out.println("</head>");
               out.println("</html>");
          
          }
          */
          Vector vect              = new Vector();  
          vect                     = theList.getPrintValues();
          
          Vector VInfo             = new Vector();
          Vector VValue            = new Vector();
          
          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");
          
          VValue.addElement("Guest List From "+common.parseDate(SStDate)+"To "+common.parseDate(SEnDate));
          VValue.addElement("2(e)");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());
          
          
          info.flashMessage(VInfo,VValue,vect,out);
                             
          out.close();
     }
}
