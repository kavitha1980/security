package Reports.Guest;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class GuestExcessHoursList
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SPrint;
     protected String SDSN;
     protected int iSelected;

     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;
     int iCheck=0;
     Vector VInDate,VGuestName,VGuestPlace,VSex,VNoPersons,VToMeet,VSpentTime;
     Vector GHead,VDepartment,VInTime,VOutTime,VExcessTime;
     Common common = new Common();

     GuestExcessHoursList(String SStDate,String SEnDate,String SFile,int iSelected,String SPrint)
     {
          this.SStDate = SStDate;
          this.SEnDate = SEnDate;
          this.SFile   = SFile;
          this.SPrint  = SPrint;
          this.iSelected=iSelected;
          try
          {
               SStatus = (new java.util.Date()).toString();
               setGuestList();
               //setgetOption();
          }
          catch(Exception ex)
          {
               SStatus = ex.getMessage();   
          }
     }

     public void setGuestList()
     {
            setDataIntoVector();
            String STitle     = "Guest Register List From "+common.parseDate(SStDate)+"To" +common.parseDate(SEnDate)+ "\n";
            Vector GHead      = getGuestHead();
            iLen              = ((String)GHead.elementAt(0)).length();
            Strline           = common.Replicate("-",iLen)+"\n";
            Vector VBody      = getGuestListBody();

            new DocPrint(VBody,GHead,STitle,SFile);
      }

     public void setDataIntoVector ()
     {
          VInDate       = new Vector();
          VGuestName    = new Vector();
          VGuestPlace   = new Vector();
          VSex          = new Vector();
          VNoPersons    = new Vector();
          VToMeet       = new Vector();
          VDepartment   = new Vector();
          VInTime       = new Vector();
          VOutTime      = new Vector();
          VSpentTime    = new Vector();
          VExcessTime   = new Vector();

          try
          {
               String SDate        = SEnDate;
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat      = conn.createStatement();
               ResultSet res       = stat.executeQuery(getQString());

               while (res.next())
               {
                    String SInDate      = common.parseDate((String)res.getString(1));
                    String SGuestName   = common.parseNull(res.getString(2));
                    String SGuestPlace  = common.parseNull(res.getString(3));
                    String SSex         = common.parseNull(res.getString(4));
                    String SNoPersons   = common.parseNull((String)res.getString(5));
                    String SToMeet      = common.parseNull(res.getString(6));
                    String SDepartment  = common.parseNull(res.getString(7));   
                    String SInTime      = common.parseNull(res.getString(8));
                    String SOutTime     = common.parseNull(res.getString(9));
                    String SOutDate     = common.parseNull(res.getString(10));

                    int iodate          = Integer.parseInt(SOutDate);
                    int iindate         = Integer.parseInt((String)res.getString(1));

                    String SSpentTime   = common.getTimeDiff(iodate,iindate,SOutTime,SInTime);

                    if(iSelected==200)
                    {
                    
                         String Shours       = SSpentTime.substring(0,2);
                         int iHours          = Integer.parseInt(Shours);

                         if(iHours >= 24 )
                         {

                              int iOutDate        = Integer.parseInt(common.pureDate(SOutDate));
                              int iInDate         = Integer.parseInt(common.pureDate(SInDate));
          
                              //SSpentTime   = common.getTimeDiff(iOutDate,iInDate,SOutTime,SInTime);
                              String SExcessTime  = common.getTimeDiff(iOutDate,iInDate,SOutTime,SInTime);
                              String SEhours      = SExcessTime.substring(0,2);
                              int iEhours         = Integer.parseInt(SEhours)-24;
                              SEhours             = Integer.toString(iEhours);
                              SExcessTime         = SEhours+":"+SExcessTime.substring(3,5)+":"+SExcessTime.substring(6,8);
                              
                              VInDate.addElement(SInDate);
                              VGuestName.addElement(SGuestName);
                              VGuestPlace.addElement(SGuestPlace);
                              VSex.addElement(SSex);
                              VNoPersons.addElement(SNoPersons);
                              VToMeet.addElement(SToMeet);
                              VDepartment.addElement(SDepartment);
                              VInTime.addElement(SInTime);
                              VOutTime.addElement(SOutTime);
                              VSpentTime.addElement(SSpentTime);            
                              VExcessTime.addElement(SExcessTime);
                         }
                    }
                    else
                    {
                              int iOutDate        = Integer.parseInt(common.pureDate(SOutDate));
                              int iInDate         = Integer.parseInt(common.pureDate(SInDate));
          
                              SSpentTime   = common.getTimeDiff(SOutTime,SInTime);
          
                              String SExcessTime  = common.getTimeDiff(iOutDate,iInDate,SOutTime,SInTime);
                              String SEhours      = SExcessTime.substring(0,2);

                              int iEhours         = Integer.parseInt(SEhours);
                              SEhours             = Integer.toString(iEhours);
                              SExcessTime         = SEhours+":"+SExcessTime.substring(3,5)+":"+SExcessTime.substring(6,8);
          
                              VInDate.addElement(SInDate);
                              VGuestName.addElement(SGuestName);
                              VGuestPlace.addElement(SGuestPlace);
                              VSex.addElement(SSex);
                              VNoPersons.addElement(SNoPersons);
                              VToMeet.addElement(SToMeet);
                              VDepartment.addElement(SDepartment);
                              VInTime.addElement(SInTime);
                              VOutTime.addElement(SOutTime);
                              VSpentTime.addElement(SSpentTime);
                              VExcessTime.addElement(SExcessTime);
                    }

               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public Vector getGuestHead()
     {
          Vector vect = new Vector();
          
          String Head1[]={"InDate","GuestName","GuestPlace","Sex","No.of.Persons","ToMeet","Department","InTime","OutTime","SpentTime","ExcessTime"};    

          String Field1  =((String)Head1[0]).trim();
          String Field2  =((String)Head1[1]).trim();
          String Field3  =((String)Head1[2]).trim();
          String Field4  =((String)Head1[3]).trim();
          String Field5  =((String)Head1[4]).trim();

          String Field6  =((String)Head1[5]).trim();
          String Field7  =((String)Head1[6]).trim();
          String Field8  =((String)Head1[7]).trim();
          String Field9  =((String)Head1[8]).trim();
          String Field10 =((String)Head1[9]).trim();
          String Field11 =((String)Head1[10]).trim();
          
          GHead          = new Vector();
          
          GHead.addElement(Field1);
          GHead.addElement(Field2);
          GHead.addElement(Field3);
          GHead.addElement(Field4);
          GHead.addElement(Field5);
          GHead.addElement(Field6);
          GHead.addElement(Field7);
          GHead.addElement(Field8);
          GHead.addElement(Field9);
          GHead.addElement(Field10);
          GHead.addElement(Field11);
          
          Field1    = common.Pad(Field1,15);
          Field2    = common.Pad(Field2,15)+SInt;
          Field3    = common.Pad(Field3,15)+SInt;
          Field4    = common.Pad(Field4,15)+SInt;
          Field5    = common.Pad(Field5,15)+SInt;
          Field6    = common.Pad(Field6,15)+SInt;
          Field7    = common.Pad(Field7,15)+SInt;
          Field8    = common.Pad(Field8,15)+SInt;
          Field9    = common.Pad(Field9,15)+SInt;
          Field10   = common.Pad(Field10,15)+SInt;
          Field11   = common.Pad(Field11,15)+SInt;

          String Strh1 = Field1+Field2+Field3+Field4+Field5+Field6+Field7+Field8+Field9+Field10+Field11+"\n";
          vect.add(Strh1);
          return vect;
     }

     public Vector getGuestListBody()
     {
          Vector vect    = new Vector();
          
          for(int i=0;i<VGuestName.size();i++)
          {
               String List1   = (String)VInDate.elementAt(i);
               String List2   = (String)VGuestName.elementAt(i);
               String List3   = (String)VGuestPlace.elementAt(i);
               String List4   = (String)VSex.elementAt(i);
               String List5   = (String)VNoPersons.elementAt(i);
               String List6   = (String)VToMeet.elementAt(i);
               String List7   = (String)VDepartment.elementAt(i);
               String List8   = (String)VInTime.elementAt(i);
               String List9   = (String)VOutTime.elementAt(i);
               String List10  = (String)VSpentTime.elementAt(i);
               String List11  = (String)VExcessTime.elementAt(i);
               
               List1     = common.Pad(List1,15);
               List2     = common.Pad(List2,15)+SInt;
               List3     = common.Pad(List3,15)+SInt;
               List4     = common.Pad(List4,15)+SInt;
               List5     = common.Pad(List5,15)+SInt;
               List6     = common.Pad(List6,15)+SInt;
               List7     = common.Pad(List7,15)+SInt;
               List8     = common.Pad(List8,15)+SInt;
               List9     = common.Pad(List9,15)+SInt;
               List10    = common.Pad(List10,15)+SInt;
               List10    = common.Pad(List11,15)+SInt;
               
               String Strd    = List1+List2+List3+List4+List5+List6+List7+List8+List9+List10+List11+"\n";
               vect.add(Strd);
          }
          return vect;
     }

     public Vector getPrintValues()
     {
          Vector vect    = new Vector();
          
          vect.addElement(GHead);
          vect.addElement(VInDate);
          vect.addElement(VGuestName);
          vect.addElement(VGuestPlace);
          vect.addElement(VSex);
          vect.addElement(VNoPersons);
          vect.addElement(VToMeet);
          vect.addElement(VDepartment);
          vect.addElement(VInTime);
          vect.addElement(VOutTime);
          vect.addElement(VSpentTime);
          vect.addElement(VExcessTime);

          return vect;
     }

     /*public String getOption()
     {
          if(SSelect=200)
          {
               System.out.println("getOption");
          }
     }*/

     public String getQString()
     {
          String QString = "Select InDate,GuestName,GuestPlace,Sex,NoPersons,ToMeet,Department,InTime,OutTime,OutDate from GuestTable "+
                           "where OutDate >= '"+SStDate+"' And OutDate <= '"+SEnDate+"' And Status=1 order by 1";   
          return QString;
     }
     
     public String getStatus()
     {
          return SStatus;
     }

}
