package Reports.Guest;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GuestIndividualPeriodRep extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector V1,V2,V3,V4,V5,V6,V7;
     String SStDate,SEnDate,SToMeet;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;

     String SInDate="",SInTime="",SGuestName="",SGuestPlace="",SOutDate="",SOutTime="";

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          SStDate            = (request.getParameter("TStDate")).trim();
          SStDate            = common.pureDate(SStDate);
          SEnDate            = (request.getParameter("TEnDate")).trim();
          SEnDate            = common.pureDate(SEnDate);
          setVector();

          try
          {
               FW      = new FileWriter("D:/Guest.prn");
               if(Lctr < 60)
                  return;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}

          out.println("<html>");

          out.println("<head>");
          out.println("<title>Security</title>");
          out.println("</head>");

          out.println("<body bgcolor='"+bgColor+"'>");
          
          for(int i=0;i<V7.size();i++)
          {
               out.println("<p align='center'><font size='5' color='#000000'><b>Guest Individual Report For The");
               out.println("Period From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+"</b></font></p>");
               out.println("  <table border='0' width='1700' height='34' bordercolor='"+bgColor+"'>");
     
               out.println("    <tr>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>InDate</h3></font></td>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>InTime</h3></font></td>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>GuestName</h3></font></td>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>GuestPlace</h3></font></td>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>OutDate</h3></font></td>");
               out.println("      <td width='100' height='34' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><h3>OutTime</h3></font></td>");
               out.println("    </tr>");
     
               for(i=0;i<V1.size();i++)
               {
                    out.println("<p align='center'><font size='3' color='#000000'><b>Employee Name: '"+V7.elementAt(i)+"' ");
                    out.println("    <tr>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V1.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V2.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V3.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V4.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V5.elementAt(i)+"</font></td>");
                    out.println("      <td width='100' height='34' align='left' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+(String)V6.elementAt(i)+"</font></td>");
                    out.println("    </tr>");
               }
               out.println("  </table>");
               out.println("</body>");
               out.println("</html>");
               out.close();
          }
     }
     public void setVector()
     {
           V1    = new Vector();
           V2    = new Vector();
           V3    = new Vector();
           V4    = new Vector();
           V5    = new Vector();
           V6    = new Vector();
           V7    = new Vector();   

           String QS="";
 
                  QS = "SELECT InDate,InTime,GuestName,GuestPlace,OutDate,OutTime,ToMeet"+
                        " FROM  Guesttable"+
                        " WHERE OutDate >='"+SStDate+"' And OutDate <='"+SEnDate+"'"; 

            try
            {
                    Class.forName("oracle.jdbc.OracleDriver");
                    Connection con = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                    Statement stat = con.createStatement();
                    ResultSet res = stat.executeQuery(QS);
                    while(res.next())
                    {
                         String SInDate      = common.parseDate((String)res.getString(1));
                         String SInTime      = common.parseNull((String)res.getString(2));
                         String SGuestName   = common.parseNull((String)res.getString(3));
                         String SGuestPlace  = common.parseNull((String)res.getString(4));
                         String SOutDate     = common.parseDate((String)res.getString(5));
                         String SOutTime     = common.parseNull((String)res.getString(6));
                         SToMeet      = common.parseNull((String)res.getString(7));

                         V1.addElement(common.parseDate(SInDate));
                         V2.addElement(SInTime);
                         V3.addElement(SGuestName);
                         V4.addElement(SGuestPlace);
                         V5.addElement(SOutDate);
                         V6.addElement(SOutTime);
                         V7.addElement(SToMeet);     
                    }
                    con.close();
            }
            catch(Exception ex)
            {
                 System.out.println(ex);
            }
     }
     public void Head()
     {
               try
               {
         String Strl = "읕컴컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴좔컴컴컴컴컴좔컴컴컴컴컴켸\n";
                    FW.write(Strl+"\n");
               }
               catch(Exception e)
               { e.printStackTrace(); }

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Guest Individual Period Report",80);
          String Str3    = common.Pad("Period     : "+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate),80);
          String Str4    = common.Pad("Page No    : "+Pctr,80);

          VHead   =  MainHead();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");

               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public Vector MainHead()
     {
          Vector VHead = new Vector();

          String Str1="",Str2="",Str3="",Str4="",Str5="";

               Str1  = "旼컴컴컴컴컴컫컴컴컴컴컴컫컴컴컴컴컴컴컴컴컴컴컫컴컴컴컴컴컴컴컴컴컴쩡컴컴컴컴컴쩡컴컴컴컴컴커\n";
               Str2  = "�            �           �                     �                    �           �            �\n";
               Str3  = "�   InDate   �  In-Time  �      GuestName      �     GuestPlace     �  OutDate  �   OutTime  �\n";
               Str4  = "�            �           �                     �                    �           �            �\n";
               Str5  = "쳐컴컴컴컴컴컵컴컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴탠컴컴컴컴컴탠컴컴컴컴컴캑\n";
                        
          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);

          return VHead;
     }
     public void Body()
     {
          for(int i=0;i<V1.size();i++)
          {

               String Str1 = "";
               Str1 = "�"+common.Pad((String)V1.elementAt(i),12)+"�"+common.Pad((String)V2.elementAt(i),11)+
                      "�"+common.Pad((String)V3.elementAt(i),21)+"�"+common.Pad((String)V4.elementAt(i),20)+
                      "�"+common.Pad((String)V5.elementAt(i),11)+"�"+common.Pad((String)V6.elementAt(i),12)+"�"; 
               try
               {
                    FW.write(Str1+"\n");
                    Lctr++;
                    Head();
               }catch(Exception ex){}
          }
          getEnd();
          Lctr = 70;
          Pctr = 0;
     }
     public void getEnd()
     {
          String Str1="";
          Str1 = "읕컴컴컴컴컴컨컴컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴좔컴컴컴컴컴좔컴컴컴컴컴켸\n";
          try
          {
               FW.write(Str1+"\n");
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}
