package Reports;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DirectionPanel extends HttpServlet
{
     HttpSession session;
     Common common;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;

     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");

          out.println("<html>");
          out.println("");
          out.println("<head>");
          out.println("<base target='topmain'>");
          out.println("</head>");
          out.println("");
          out.println("<body bgcolor='#FEFCD8' link='#552B00' vlink='#005500' alink='#002346'>");
          out.println("<div align='left'>");
          out.println("  <table border='0' width='270' height='585' bgcolor='#FEFCD8' cellspacing='0' cellpadding='0'>");





          //Enquiry start

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Inward Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"InwardRegisterCrit'>-Material Inward Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"InwardSupplierCrit'>-SupplierWise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"NoEntryCrit'>-GIN made - No Entry Found In Inventory Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"MaterialwiseCrit'>-Materialwise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"StoresMaterialsCrit'>-StoresMaterials not Account in GRN </a></font></td>");
          out.println("    </tr>");



          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Outward Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"ROutStoresCrit'>-Returnable Outward Register-For Stores</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"ROutDyeingCrit'>-Returnable Outward Register-For Dyeing</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"NROutStoresCrit'>-Non-Returnable Outward Register-For Stores</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"NROutRelateCrit'>-Non-Returnable Outward Register-Relating To Stroes</a></font></td>");
          out.println("    </tr>");




          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Errector Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Errector.ErrectorRegisterCrit'>-Errector Register-DateWise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Errector.ErrectorRegisterAsCrit'>-Errector Register-As On</a></font></td>");
          out.println("    </tr>");


          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Errector.ErrectorWiseCrit'>-Errectorwise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Interview Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Interview.InterviewAsOnCrit'>-Interview Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Interview.InterviewRegisterCrit'>-InterviewRegister-DateWise</a></font></td>");
          out.println("    </tr>");




          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Vehcicle Movement Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Vehicle.VehicleRegisterCrit'>-VehicleMovement Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Vehicle.FuelConsumptionCrit'>-Selected Vehicles-Fuel Consumption</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Vehicle.FuelConsumptionCrit3'>-(New)Selected Vehicles-Fuel Consumption</a></font></td>");
          out.println("    </tr>");



          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Vehicle.FuelMonthCrit'>-Fuel Consumption-Monthwise Report</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Vehicle.FuelMonthAnalysisCrit'>-Fuel Consumption MonthWise Analysis</a></font></td>");
          out.println("    </tr>");

/*          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"SelectedVehicleCrit'>-Selected Vehicles-Fuel Consumption</a></font></td>");
          out.println("    </tr>");
*/
          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"VehicleMaintananceCrit'>-VehicleMaintanance</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"VehicleMainExpensesCrit'>-Maintenance Expenses Analysis Report-Yearly</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"SelectedVehicleMaintenanceCrit'>-Selected Vehicle Maintanance-Monthwise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;RawMaterial-Inward</font></b></td>");
          out.println("    </tr>");


          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Cotton.CottonPurchaseCrit'>-Cotton-GateInward </a></font></td> ");
          out.println("    </tr>");

          

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.DyedCotton.DyedCottonGICrit'>-DyedCotton-GateInward </a></font></td>");
          out.println("    </tr>");


          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Visitors Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorRegisterCrit'>-Visitor Register - For the Period </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorAsOnRegisterCrit'>-Visitor Register - As On Date</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorHourCrit'>-Visitor-Hourwise</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorNonHoursCrit'>-Non-Hourwise </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorExcessHourCrit'>-Excess-Hourwise </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Visitor.VisitorAbstractCrit'>-Abstarct </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Guest Register</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Guest.GuestRegisterCrit'>-Guest Register </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Guest.GuestExcessHoursCrit'>-Guest Excess-Hourwise </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Guest.GuestIndividualAsOnCrit'>-Guest Register- Individual As On</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"Reports.Guest.GuestIndividualPeriodCrit'>-Guest Register- Individual For the Period</a></font></td>");
          out.println("    </tr>");
          




          /*out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Follow-Up</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"POAfterDue'>");
          out.println("      -PO Pending After Due Date</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"POBeforeDue'>-P O Pending Before Due Date");
          out.println("        </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"RDCPending'>-RDC Pending ");
          out.println("      </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"POAdvanceDue'>-Advance Paid But Goods");
          out.println("        Not Recd</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"MRSWEnq'>-MRS Pending With");
          out.println("        Enquiry</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"MRSWOEnq'>-MRS Pending Without");
          out.println("        Enquiry</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Issue</font></b></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"IssueDayCrit'>-Daywise ");
          out.println("        Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"IssueDeptCrit'>-DeptGroupwise ");
          out.println("        Register</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"IssueDeptAbsCrit'>-Consumption ");
          out.println("        Matrix</a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"CostCrit'>-Stores Cost/Spdl");
          out.println("        </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"CostPurchaseCrit'>-Purchase Cost/Spdl");
          out.println("        </a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'>");
          out.println("        <p align='left'><b><font color='#800000' size='4'>&nbsp;Stock</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"StockBSCrit'>-Balance Sheet</a></font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"StockBSAbsCrit'>-Balance Sheet Abstract</a></font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"ClosingStockCrit'>-Closing Stock</a></font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#000080'><a href='"+SServer+"MatLedgerCrit'>-Material Ledger</a></font></td>");
          out.println("    </tr>");*/


          out.println("  </table>");
          out.println("</div>");
          out.println("");
          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");
          out.println("");

          out.close();
     }
}



