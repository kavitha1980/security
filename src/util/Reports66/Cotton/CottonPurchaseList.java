package Reports.Cotton;
import util.*;

import java.util.*;
import java.io.*;
import java.sql.*;

public class CottonPurchaseList
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Object RowData[][];
     String ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","Delay Days"};
     String ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","N"};

     Vector VInwardDate,VInwardNo,VSupplierName,VDCNo,VDCDate,VInvNo,VInvDate,VTruckNo,VTimeIn,VSecurityCode,VPartyLotNo,VNoofBales;
     Vector Vhead,VSNo;
     Common common = new Common();


     String SStatus = "";


     CottonPurchaseList(String SStDate,String SEnDate,String SFile)
     {
            this.SStDate = SStDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Material Inward List From "+common.parseDate(SStDate)+" TO "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getInwardHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getInwardListBody();
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getInwardHead()
      {
           Vector vect = new Vector();

           String Head1[]={"SNO","GIDATE","GINO","SUPPLIER_NAME","DCNO","DCDATE","INVOICE_NO","INVOICE_DATE","TRUCK_NO","TIME_IN","SECURITY_CODE","PARTY_LOT_NO","NO_OF_BALES" };

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           
           Sha1  = common.Rad(Sha1,5)+common.Space(5)+SInt;;
           Sha2  = common.Pad(Sha2,15)+SInt;
           Sha3  = common.Pad(Sha3,15)+SInt;
           Sha4  = common.Pad(Sha4,35)+SInt;
           Sha5  = common.Pad(Sha5,10)+SInt;
           Sha6  = common.Pad(Sha6,15)+SInt;
           Sha7  = common.Pad(Sha7,15)+SInt;
           Sha8  = common.Pad(Sha8,15)+SInt;
           Sha9  = common.Pad(Sha9,15)+SInt;
           Sha10 = common.Pad(Sha10,13)+SInt;
           Sha11 = common.Pad(Sha11,15)+SInt;
           Sha12 = common.Pad(Sha12,15)+SInt;
           Sha13 = common.Rad(Sha13,10);

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+"\n";
           vect.add(Strh1);
           return vect;
     }
     public Vector getInwardListBody()
     {
           Vector vect = new Vector();
           for(int i=0;i<VInwardDate.size();i++)
           {

                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VInwardDate.elementAt(i);
                 String Sda3  = (String)VInwardNo.elementAt(i);
                 String Sda4  = (String)VSupplierName.elementAt(i);
                 String Sda5  = (String)VDCNo.elementAt(i);
                 String Sda6  = (String)VDCDate.elementAt(i);
                 String Sda7  = (String)VInvNo.elementAt(i);
                 String Sda8  = (String)VInvDate.elementAt(i);
                 String Sda9  = (String)VTruckNo.elementAt(i);
                 String Sda10 = (String)VTimeIn.elementAt(i);
                 String Sda11 = (String)VSecurityCode.elementAt(i);
                 String Sda12 = (String)VPartyLotNo.elementAt(i);
                 String Sda13 = (String)VNoofBales.elementAt(i);

                 Sda1  = common.Rad(Sda1,5)+common.Space(5)+SInt;
                 Sda2  = common.Pad(Sda2,15)+SInt;
                 Sda3  = common.Pad(Sda3,15)+SInt;
                 Sda4  = common.Pad(Sda4,35)+SInt;
                 Sda5  = common.Pad(Sda5,10)+SInt;
                 Sda6  = common.Pad(Sda6,15)+SInt;
                 Sda7  = common.Pad(Sda7,15)+SInt;
                 Sda8  = common.Pad(Sda8,15)+SInt;
                 Sda9  = common.Pad(Sda9,15)+SInt;
                 Sda10 = common.Pad(Sda10,15)+SInt;
                 Sda11 = common.Pad(Sda11,15)+SInt;
                 Sda12 = common.Pad(Sda12,15)+SInt;
                 Sda13 = common.Rad(Sda13,5);
                 


                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+"\n";
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

           VSNo          = new Vector();
           VInwardDate   = new Vector();
           VInwardNo     = new Vector();
           VSupplierName = new Vector();
           VDCNo         = new Vector();
           VDCDate       = new Vector();
           VInvNo        = new Vector();
           VInvDate      = new Vector();
           VTruckNo      = new Vector();
           VTimeIn       = new Vector();
           VSecurityCode = new Vector();
           VPartyLotNo   = new Vector();
           VNoofBales    = new Vector();


           try
           {

                      String SDate   = SEnDate;
                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","scm","rawmat");
                      Statement stat                   = conn.createStatement();
                      ResultSet res                    = stat.executeQuery(getQString());

                      int iSno=1;
                      while (res.next())
                      {
                                 String SInwardNo      = res.getString(3);
                                 String SInwardDate    = common.parseDate((String)res.getString(4));
                                 String SSupplierName  = res.getString(6);
                                 String SDCNo          = res.getString(7);
                                 String SDCDate        = common.parseDate((String)res.getString(8));
                                 String SInvNo         = res.getString(9);
                                 String SInvDate       = common.parseDate((String)res.getString(10));
                                 String STruckNo       = common.parseNull((String)res.getString(11));
                                 String STimeIn        = common.parseNull(res.getString(14));
                                 String SSecurityCode  = common.parseDate((String)res.getString(16));
                                 String SPartyLotNo    = common.parseNull(res.getString(13));
                                 String SNoofBales     = common.parseDate((String)res.getString(12));

                                 VSNo.addElement(String.valueOf(iSno));
                                 VInwardDate.addElement(SInwardDate);
                                 VInwardNo.addElement(SInwardNo);
                                 VSupplierName.addElement(SSupplierName);
                                 VDCNo.addElement(SDCNo);
                                 VDCDate.addElement(SDCDate);
                                 VInvNo.addElement(SInvNo);
                                 VInvDate.addElement(SInvDate);
                                 VTruckNo.addElement(STruckNo);
                                 VTimeIn.addElement(STimeIn);
                                 VSecurityCode.addElement(SSecurityCode);
                                 VPartyLotNo.addElement(SPartyLotNo);
                                 VNoofBales.addElement(SNoofBales);
                                 iSno=iSno+1;
                      }
           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VInwardDate);
           vect.addElement(VInwardNo);
           vect.addElement(VSupplierName);
           vect.addElement(VDCNo);
           vect.addElement(VDCDate);
           vect.addElement(VInvNo);
           vect.addElement(VInvDate);
           vect.addElement(VTruckNo);
           vect.addElement(VTimeIn);
           vect.addElement(VSecurityCode);
           vect.addElement(VPartyLotNo);
           vect.addElement(VNoofBales);

           return vect;
     }
           

     public String getQString()
     {
          String QString = " Select CottonGI.AGNNo,CottonGI.AGNDate, "+
                         " CottonGI.GINo,CottonGI.GIDate, "+
                         " CottonGI.AcCode, "+
                         " Supplier.Name as Supplier,CottonGI.DCNo, "+
                         " CottonGI.DCDate,CottonGI.InvNo, "+
                         " CottonGI.InvDate,CottonGI.TruckNo, "+
                         " CottonGI.Bales,CottonGI.partylotno,"+
                         " CottonGI.TimeIn,CottonGI.Status,CottonGI.seccode,CottonGI.milllotno"+
                         " From (CottonGI "+
                         " Inner Join Supplier On Supplier.AcCode = CottonGI.AcCode) "+
                         " Where CottonGI.GIDate >= "+SStDate+" AND CottonGI.GIDate<="+SEnDate+" Order by 3 ";

           return QString;
     }


     public String getStatus()
     {
           return SStatus;
     }

}
