/*
     A general information notifier
     about a process status to the user. 
*/

package Reports.Errector;

import java.io.*;
import java.util.Vector;

public class ErrectorAsInformation
{

     ErrectorAsInformation()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
     
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead        = (Vector)VPrint.elementAt(0);
          Vector VSNo         = (Vector)VPrint.elementAt(1);
          Vector VCompany     = (Vector)VPrint.elementAt(2);
          Vector VRep         = (Vector)VPrint.elementAt(3);
          Vector VPurpose     = (Vector)VPrint.elementAt(4);
          Vector VInTime      = (Vector)VPrint.elementAt(5);
          Vector VOutTime     = (Vector)VPrint.elementAt(6);
          Vector VDate        = (Vector)VPrint.elementAt(7);

          out.println(" <table border='1' width='1500' height='20' cellpadding='0' cellspacing='0'   >");
          int m=0;
          for(int i=0;i<VDate.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<VHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+(String)VHead.elementAt(j)+"</td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                    out.println(" <tr>");

                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSNo        .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VCompany    .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VRep        .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VPurpose    .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInTime     .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOutTime    .elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDate       .elementAt(i)+"</td>");

                    out.println(" </tr>");
                    
               }
          
          out.println("</table>");


          out.println("</body>");
          out.println("</html>");
     }

}
