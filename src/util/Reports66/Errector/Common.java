package Reports.Errector;

import java.io.*;
import java.util.*;

public class Common
{
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     String SFinDSN,SInvDSN,SFibreDSN;
     String SPeriod,SUser,SFinSt;

     public Common()
     {
          bgColor = "#FEF5E2";
          bgHead  = "#DFD9F2";     // "#000080";
          bgUom   = "#FFB0B0";     //"#CCCCFF";
          bgBody  = "#9DECFF";
          fgHead  = "#000080";     // "#00FFFF";
          fgUom   = "#000080";
          fgBody  = "#000080";
          setDSN();
    }

     public String getTime()
     {
          Date dt        = new Date();

          int iHrs       = dt.getHours();
          int iMin       = dt.getMinutes();

          String sHrs    =(iHrs<10?"0"+iHrs:""+iHrs);
          String sMin    =(iMin<10?"0"+iMin:""+iMin);

          return sHrs+":"+sMin;
     }

     public String getRound(double dAmount,int iScale)
     {
            try
            {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
                  return (bd1.setScale(iScale,4)).toString();
            }
            catch(Exception ex){}
            return "";
     }
     public String getTimeDiff(String SOutTime,String SInTime)
     {

                String str1 = SInTime;
                String str2 = SOutTime;
                int sTh =0;

                String shh  = str1.substring(0,2);
                String smm  = str1.substring(3,5);
                String sss  = str1.substring(6,8);

                int ihh = Integer.parseInt(shh);
                int imm = Integer.parseInt(smm);
                int iss = Integer.parseInt(sss);
                

                String sohh  = str2.substring(0,2);
                String somm  = str2.substring(3,5);
                String soss  = str2.substring(6,8);

                String SC1    = str1.substring(9,11);
                String SC2    = str2.substring(9,11);

                int ohh = Integer.parseInt(sohh);
                int omm = Integer.parseInt(somm);
                int oss = Integer.parseInt(soss);


                int itotal = (ihh*3600)+(imm*60)+iss;


                int ototal = (ohh*3600)+(omm*60)+oss;

                if(SC1.equals("AM")&& SC2.equals("PM"))
                {
                    ototal = ototal+(12*3600);

                }
                if(SC1.equals("PM")&& SC2.equals("AM"))
                {
                    ototal = ototal+(12*3600);
                }

                if(SC1.equals("PM")&& SC2.equals("PM"))
                {
                    if(ihh>ohh)
                    {
                         ototal = ototal+(12*3600);
                    }
                }


                int diff   = ototal-itotal;

                int sTm = diff/60;

                if(sTm>=60)
                {
                        sTh =sTh+(sTm/60);
                        sTm=sTm%60;
                }
                int sTs = diff%60;


                String Tdiff = sTh+":"+sTm+":"+sTs;




                if(SC1.equals("AM")&& SC2.equals("AM"))
                {

                    Tdiff = sTh+":"+sTm+":"+sTs;
                }
                if(SC1.equals("PM")&& SC2.equals("PM"))
                {

                    Tdiff = sTh+":"+sTm+":"+sTs;
                }

                String SSth   = Integer.toString(sTh);
                String SStm   = Integer.toString(sTm);
                String SSts   = Integer.toString(sTs);

                if(SSth.length() <2)
                {
                    SSth      = "0"+SSth;
                }
                if(SStm.length() <2)
                {
                    SStm      = "0"+SStm;
                }
                if(SSts.length() <2)
                {
                    SSts      = "0"+SSts;
                }


                Tdiff = SSth+":"+SStm+":"+SSts;

                return Tdiff;
        

     }

     /*public String getTotalTime(Vector VTimes)
     {


                for(int i=0;i<(VTimes.size())-1;i++)
                {
                     String str1 = (String)VTimes.elementAt(i);
                     String str2 = (String)VTimes.elementAt(i+1);   

                     String shh  = str1.substring(0,2);
                     String smm  = str1.substring(3,5);
                     String sss  = str1.substring(6,8);
     
                     int ihh = Integer.parseInt(shh);
                     int imm = Integer.parseInt(smm);
                     int iss = Integer.parseInt(sss);
                     
     
                     String sohh  = str2.substring(0,2);
                     String somm  = str2.substring(3,5);
                     String soss  = str2.substring(6,8);
                    
                     int ohh = Integer.parseInt(sohh);
                     int omm = Integer.parseInt(somm);
                     int oss = Integer.parseInt(soss);
                    

      */



     public String getTimeDiff(int IOutDate,int IInDate,String SOutTime,String SInTime)
     {

                String str1 = SInTime;
                String str2 = SOutTime;
                int sTh =0;

                String shh  = str1.substring(0,2);
                String smm  = str1.substring(3,5);
                String sss  = str1.substring(6,8);

                int ihh = Integer.parseInt(shh);
                int imm = Integer.parseInt(smm);
                int iss = Integer.parseInt(sss);
                

                String sohh  = str2.substring(0,2);
                String somm  = str2.substring(3,5);
                String soss  = str2.substring(6,8);

                String SC1    = str1.substring(9,11);
                String SC2    = str2.substring(9,11);

                int ohh = Integer.parseInt(sohh);
                int omm = Integer.parseInt(somm);
                int oss = Integer.parseInt(soss);


                int itotal = (ihh*3600)+(imm*60)+iss;


                int ototal = (ohh*3600)+(omm*60)+oss;

                if(SC1.equals("AM")&& SC2.equals("PM"))
                {
                    ototal = ototal+(12*3600);

                }
                if(SC1.equals("PM") && SC2.equals("AM"))
                {
                    ototal = ototal+(12*3600);
                }
                if(SC1.equals("PM") && SC2.equals("PM"))
                {
                    if(ihh>ohh)
                    {
                         ototal = ototal+(12*3600);
                    }
                }


                int diff   = ototal-itotal;

                int sTm = diff/60;

                if(sTm>=60)
                {
                        sTh =sTh+(sTm/60);
                        sTm=sTm%60;
                }
                int sTs = diff%60;

                int iDateDiff = IOutDate-IInDate;

                String Tdiff = sTh+":"+sTm+":"+sTs;


                if(iDateDiff==0)
                {
                    Tdiff = sTh+":"+sTm+":"+sTs;
                }    
                else
                {

                    if((SC1.equals("AM")&&SC2.equals("AM"))||(SC1.equals("PM")&&SC2.equals("PM")))
                    {
                         if(ohh>ihh)
                         {
                              sTh       = sTh+(iDateDiff*24);
                              Tdiff     = sTh+":"+sTm+":"+sTs;
                         }
                         else
                         {

                              int iHDiff    = 24-(ihh-ohh);

                              int iMDiff    = 60-(imm-omm);

                              int iSDiff    = 60-(iss-oss);

                              itotal = (iHDiff*3600)+(iMDiff*60)+iSDiff;


                              ototal = (iHDiff*3600)+(iMDiff*60)+iSDiff;

                              diff   = ototal-itotal;

                              sTm = diff/60;

                              if(sTm>=60)
                              {
                                   sTh =sTh+(sTm/60);
                                   sTm =sTm%60;
                              }
                              sTs = diff%60;

                              iHDiff    = 24-(ihh-ohh);


                              sTh       = iHDiff;
                              Tdiff     = sTh+":"+sTm+":"+sTs;
                         }          
                    }
                    else
                    {
                         Tdiff = sTh+":"+sTm+":"+sTs;
                    }     
                }

                if(SC1.equals("AM")&& SC2.equals("AM")||SC1.equals("PM")&& SC2.equals("PM"))
                {

                    Tdiff = sTh+":"+sTm+":"+sTs;
                }

                String SSth   = Integer.toString(sTh);
                String SStm   = Integer.toString(sTm);
                String SSts   = Integer.toString(sTs);

                if(SSth.length() <2)
                {
                    SSth      = "0"+SSth;
                }
                if(SStm.length() <2)
                {
                    SStm      = "0"+SStm;
                }
                if(SSts.length() <2)
                {
                    SSts      = "0"+SSts;
                }


                Tdiff = SSth+":"+SStm+":"+SSts;

                                             
                return Tdiff;
        

     }


     public String getRound(String SAmount,int iScale)
     {
          try
          {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
                  return (bd1.setScale(iScale,4)).toString();
          }
          catch(Exception ex){}
          return "";
     }

     public String Pad(String Str,int Len)
     {
          Str = ""+Str;
          if(Str.startsWith("null"))
               return Space(Len);

            if(Str.length() > Len)
                  return Str.substring(0,Len);
            else
                  return Str+Space(Len-Str.length());
     }

     public String Rad(String Str,int Len)
     {
          
          Str = ""+Str;
          if(Str.startsWith("null"))
               return Space(Len);

          if(Str.length() > Len)
               return Str.substring(0,Len);
          else
               return Space(Len-Str.length())+Str;
     }

     public String Space(int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr=RetStr+" ";
            return RetStr;
     }

     public String Replicate(String Str,int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr = RetStr+Str;
            return RetStr;
     }

     public String parseNull(String str)
     {
          str = ""+str;
          if(str.startsWith("null"))
               return "";
          return str;
     }

     public String Cad(String str,int Len)
     {
          if(Len < str.length())
               return Pad(str,Len);

          String RetStr="";
          int FirstLen = (Len-str.length())/2;
          return Pad(Space(FirstLen)+str,Len);
     }

     public String parseDate(String str)
     {
          str=str.trim();
          if(str.length()!=8)
               return str;
          try
          {
               return str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
          }
          catch(Exception ex){}
          return "";
     }

     public String parseDate(int iDate)
     {
          String str = String.valueOf(iDate);
          str=str.trim();
          if(str.length()!=8)
               return str;
          try
          {
               return str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
          }
          catch(Exception ex){}
          return "";
     }

     public String pureDate(String str)
     {
          str=str.trim();
          if(str.length()!=10)
               return str;
          try
          {
               return str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
          }
          catch(Exception ex){}
          return "";
     }

     public double toDouble(String str)
     {
          try
          {
               return Double.parseDouble(str.trim());       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public int toInt(String str)
     {
          try
          {
               return Integer.parseInt(str);       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public float toFloat(String str)
     {
          try
          {
               return Float.parseFloat(str);       
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public String getDateDiff(String str1,String str2)
     {
            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
            return String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000));
     }

     public double getHours(java.util.Date Dt1,java.util.Date Dt2)
     {
			try
			{
				long lFact  = (long)(1000*60*60);
				long lMilli = Dt1.getTime()-Dt2.getTime();
				return (double)(lMilli/lFact);
			}
			catch(Exception ex)
			{
			}
			return 0;
	 }
     public double getHours(long long1,long long2)
     {
            long lFact  = (long)(1000*60*60);
            return (double)((long1-long2)/lFact);
     }

     public String getDateDiff(String str1)
     {
            int iYear1,iMonth1,iDay1;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new Date();
            return String.valueOf((dt2.getTime()-dt1.getTime())/(24*60*60*1000));
     }

     public String getDate(String str1,long days,int Sig)
     {
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return "0";
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (days*24*60*60*1000);
            if (Sig == -1)
                  dt1.setTime(num1-num2);
            else
                  dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return SDay+"."+SMonth+"."+SYear;
     }

     public int indexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();
           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.startsWith(str))
                        return i;
           }
           return iindex;
     }

     // SOp - operation : "S" - SUM; "A" - Average
     public String getSum(Vector Vx,String SOp)
     {
          
          double dAmount=0.0;   
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,2);
     }

     // SOp - operation : "S" - SUM; "A" - Average , iScale = int
     public String getSum(Vector Vx,String SOp,int iScale)
     {
          
          double dAmount=0.0;   
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,iScale);
     }

     public Vector getLines(String str)
     {
          Vector VLines = new Vector();
          String SLine  = "";
          StringTokenizer ST = new StringTokenizer(str," ");
          while(ST.hasMoreElements())
          {
               String SWord = (ST.nextToken()).trim();
               if((SLine+" "+SWord).length() < 30)
               {
                    SLine = SLine+" "+SWord;
               }
               else
               {
                    VLines.addElement(SLine.trim());
                    SLine=SWord;
               }
          }
          if(SLine.length() > 0)
               VLines.addElement(SLine.trim());
          return VLines;
     }

     //Vector of Dates given Start Date and End Date in pure form
     public Vector formDateVector(String SStDate,String SEnDate)
     {
          Vector retVect = new Vector();
          if(toInt(SEnDate)<toInt(SStDate))
               return retVect;

          String STemp   = SStDate;
          retVect.addElement(SStDate);
          while(toInt(SEnDate)>toInt(STemp))
          {
               STemp  = pureDate(getDate(parseDate(STemp),1,1));
               retVect.addElement(STemp);
          }
          return retVect;
     }

     public String getTagValue(String str,int iLen,String SColType)
	{
		String SRetStr="";			
		int iSig=0;
		for(int i=0;i<str.length();i++)
		{
			String xtr = str.substring(i,i+1);
			if(xtr.equals("<"))
				iSig=0;
			if(xtr.equals(">"))
			{
				iSig=1;
				continue;
			}
			if(iSig==1)
				SRetStr=SRetStr+xtr;
		}
          SRetStr = SRetStr.trim();
          if(SColType.equals("S"))
               SRetStr = Pad(SRetStr,iLen);
          else
               SRetStr = Rad(SRetStr,iLen);
		if(isBoldable(str))
               SRetStr = "E"+SRetStr+"F";
          return SRetStr;
	}
	private boolean isBoldable(String str)
	{
		int index = str.indexOf("<b");
		if(index < 0)
			return false;
		return true;
	}
        public int getPreviousMonth(int iMonth)
        {
		String SMonth = null;
		SMonth = String.valueOf(iMonth);
		int iYear = toInt(SMonth.substring(0,4));
		String SPreMonth = String.valueOf(iMonth-1);
		if(SPreMonth.substring(4,6).equals("00"))
			SPreMonth=String.valueOf(iYear-1)+"12";
		return toInt(SPreMonth);
        }

	public int getPreviousMonth(int iMonth,int iMany)
	{
		int iTemp = iMonth;
                for(int i = 0; i < iMany; i++)
                {
                        iTemp = getPreviousMonth(iTemp);
                }
                return iTemp;
	}

        public int[] getPreviousMonths(int iMonth,int iMany)  
        {
                int iMonthArr[] = new int[iMany];
                for(int i=0;i<iMonthArr.length;i++)
                        iMonthArr[i]=0;
                for(int i=0;i<iMonthArr.length;i++)
                {
                        iMonthArr[i]=getPreviousMonth(iMonth);
                        iMonth=iMonthArr[i];
                }
                return iMonthArr;
        }
        public String getMonthName(int iMonth)
        {
                String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
				String SMonth = null;
                try
                {
			SMonth = String.valueOf(iMonth);
			iMonth = toInt(SMonth.substring(4,6))-1;
			int iYear  = toInt(SMonth.substring(0,4));  
                        SMonth = SArr[iMonth]+" "+iYear;
                }
                catch(Exception ex)
		{
			return "";
                }
                return SMonth;
        }

        public String[] getMonths(int iMonthArr[])
        {
                String SMonthArr[] = new String[iMonthArr.length];
                for(int i=0;i<SMonthArr.length;i++)
                        SMonthArr[i]=getMonthName(iMonthArr[i]);
                return SMonthArr;
        }
        //Returns a date less by iMonths from iDate (in pure form)
        public int getPreviousDate(int iDate,int iStMonth)
        {
                int iRetDate = iDate;
                if(iStMonth==0)
                        return iRetDate;
                if(iStMonth<0)
                        return 0;                         

                String SDay   = (String.valueOf(iDate)).substring(6,8);
                String SMonth = (String.valueOf(iDate)).substring(4,6);
                String SYear  = (String.valueOf(iDate)).substring(0,4);

                if((toInt(SMonth)-iStMonth)<=0)
                {
                        SYear    = String.valueOf(toInt(SYear)-1);
                        int iMon = 12-(iStMonth-toInt(SMonth));
                        SMonth   = iMon<=9?"0"+String.valueOf(iMon):String.valueOf(iMon);
                        iRetDate = toInt(SYear+SMonth+SDay);
                }
                else
                {
                        int iMon = toInt(SMonth)-iStMonth;
                        SMonth   = iMon<=9?"0"+String.valueOf(iMon):String.valueOf(iMon);
                        iRetDate = toInt(SYear+SMonth+SDay);                        
                }
                return iRetDate;
        }

	public int getNextMonth(int iMonth)
	{
		iMonth = toInt(String.valueOf(iMonth).substring(0,6));
		String SMonth = String.valueOf(iMonth);
		String STmp = "";
		int iYear = toInt(SMonth.substring(0,4));
		int iNextMonth = iMonth+1;
		if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
			STmp=String.valueOf(iYear+1)+"01";
		else
			STmp=String.valueOf(iNextMonth);
		return toInt(STmp);
	}

	public int getNextMonth(int iMonth,int iNoOfMonth)
	{
		int iTMonth = iMonth;
		for (int i = 1; i <= iNoOfMonth; i++)
			iTMonth = getNextMonth(iTMonth);
		return iTMonth;
	}

        public int[] getNextMonths(int iMonth,int iMany)  
        {
                int iMonthArr[] = new int[iMany];
		iMonth = toInt(String.valueOf(iMonth).substring(0,6));
		iMonthArr[0] = iMonth;
                for(int i=1;i<iMonthArr.length;i++)
                {
                        iMonthArr[i]=getNextMonth(iMonth);
                        iMonth=iMonthArr[i];
                }
                return iMonthArr;
        }

	public String getDayOfDate(int iDate)
        {
                if(String.valueOf(iDate).length()<8)
                    return "000";
                String[] retString = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
                String SYear  = String.valueOf(toInt((String.valueOf(iDate)).substring(0,4))-1900);
                String SMonth = String.valueOf(toInt((String.valueOf(iDate)).substring(4,6))-1);
                String SDate  = (String.valueOf(iDate)).substring(6,8);

                java.util.Date DT = new java.util.Date(toInt(SYear),toInt(SMonth),toInt(SDate));
                return retString[DT.getDay()]; 
        }

	public int getSysDate()
	{
		java.util.Date dt1 = new java.util.Date();
		int iDay   = dt1.getDate();
		int iMonth = dt1.getMonth()+1;
		int iYear  = dt1.getYear()+1900;

		String SDay   = (iDay<10 ?"0"+iDay:""+iDay);
		String SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
		String SYear  = ""+iYear;
		return toInt(pureDate(SDay+"."+SMonth+"."+SYear));
	}

     // Finance Data Source
     public String getDSN()
     {
          return "jdbc:odbc:Correspondance";
     }

     // Inventory Data Source
     public String getInventoryDSN()
     {
          return SInvDSN;
     }

     // Raw-Material Data Source
     public String getFibreDSN()
     {
          return SFibreDSN;
     }

     // setting the DSNs()
     private void setDSN()
     {
          Vector vect = new Vector();
          String str  = getDSNString();
          StringTokenizer ST = new StringTokenizer(str,"\n");
          while(ST.hasMoreElements())
               vect.addElement(ST.nextToken());
          for(int i=0;i<vect.size();i++)
               organizeProperty((String)vect.elementAt(i));
     }
     private void organizeProperty(String str)
     {
          Vector vect = new Vector();
          if(str.startsWith("#"))
               return;
          StringTokenizer ST = new StringTokenizer(str,"|");
          while(ST.hasMoreElements())
          {
               String xtr = ST.nextToken();
               vect.addElement(xtr.substring(0,xtr.length()-1));
          }
          if(vect.size() != 2)
               return;
          String SId     = (String)vect.elementAt(0);
          String SSource = (String)vect.elementAt(1);
          if(SId.startsWith("Finance"))
               SFinDSN = SSource.trim();
          else if(SId.startsWith("Inventory"))
               SInvDSN = SSource.trim();
          else if(SId.startsWith("Fibre"))
               SFibreDSN = SSource.trim();
          else if(SId.startsWith("Period"))
               SPeriod = SSource.trim();
          else if(SId.startsWith("Start"))
               SFinSt = SSource.trim();
          else if(SId.startsWith("User"))
               SUser = SSource.trim();
     }
     // Reading the Finance.properties File
     private String  getDSNString()
     {
          String str="";
          try
          {
               Reader in = new FileReader("Finance.properties");
               char[] buff = new char[4096];
               int nch;
               while ((nch = in.read(buff, 0, buff.length)) != -1)
                   str = str+new String(buff, 0, nch);
          }
          catch(Exception ex)
          {
          }
          return str;
     }

     // Returing the Finance Period
     public String getFinancePeriod()
     {
          return SPeriod;
     }

     // Returing the Finance Start Date
     public String getFinStartDate()
     {
          return SFinSt;
     }

     // Returing the Previlaged User
     public String getUser()
     {
          return SUser;
     }

     public String getPYDSN()
     {
          return "jdbc:odbc:HRD2004";
     }
}
