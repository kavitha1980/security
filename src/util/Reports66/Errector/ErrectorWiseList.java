
package Reports.Errector;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;

//�


public class ErrectorWiseList implements rndi.CodedNames
{
     protected String SErrectorCode;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VSNo,VDate,VCompany,VRep,VPurpose,VToMeet,VInTime,VOutTime,VTotalHours;
     Vector Vhead,VSpentTime,VDate1;
     Common common = new Common();
     VisitorIn visitorDomain;

     String SStatus = "";


     ErrectorWiseList(String SErrectorCode,String StDate,String SEnDate,String SFile)
     {
            this.SErrectorCode = SErrectorCode;
            this.SStDate      = StDate;
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;

            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  visitorDomain         = (VisitorIn)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Errector Wise List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getVisitorHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVisitorListBody();
            new ErrectorDocPrint(VBody,VHead,STitle,SFile,(String)VCompany.elementAt(0),(String)VRep.elementAt(0));
      }

      public Vector getVisitorHead()
      {
           Vector vect = new Vector();

           String Head1[]={" SNo","Date","Purpose Of Visit","InTime","OutTime","TotalWorkedHours"};        

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=common.parseNull((String)Head1[5]);

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);


           Sha1  = common.Rad(Sha1,5)+common.Space(2)+"�";
           Sha2  = common.Space(1)+common.Pad(Sha2,11)+SInt+"�";
           Sha3  = common.Space(1)+common.Pad(Sha3,25)+SInt+"�";
           Sha4  = common.Space(1)+common.Pad(Sha4,15)+SInt+"�";
           Sha5  = common.Space(1)+common.Pad(Sha5,15)+SInt+"�";
           Sha6  = common.Space(1)+common.Pad(Sha6,20)+SInt+"�";

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVisitorListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDate1.elementAt(i);
                 String Sda3  = (String)VPurpose.elementAt(i);
                 String Sda4  = (String)VInTime.elementAt(i);
                 String Sda5  = (String)VOutTime.elementAt(i);
                 String Sda6  = (String)VSpentTime.elementAt(i);


                 Sda1    = common.Rad(Sda1,5)+common.Space(2)+"�";
                 Sda2    = common.Space(1)+common.Pad(Sda2,11)+SInt+"�";
                 Sda3    = common.Space(1)+common.Pad(Sda3,25)+SInt+"�";
                 Sda4    = common.Space(1)+common.Pad(Sda4,15)+SInt+"�";
                 Sda5    = common.Space(1)+common.Pad(Sda5,15)+SInt+"�";
                 Sda6    = common.Space(1)+common.Pad(Sda6,20)+SInt+"�";
                 
               String  Sda21    = common.Rad("",5)+common.Space(2)+"�";
               String  Sda22    = common.Space(1)+common.Pad("",11)+SInt+"�";
               String  Sda23    = common.Space(1)+common.Pad("",25)+SInt+"�";
               String  Sda24    = common.Space(1)+common.Pad("",15)+SInt+"�";
               String  Sda25    = common.Space(1)+common.Pad("",15)+SInt+"�";
               String  Sda26    = common.Space(1)+common.Pad("",20)+SInt+"�";


                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+"\n";
                 String Strd1= Sda21+Sda22+Sda23+Sda24+Sda25+Sda26+"\n";

                 vect.add(Strd);
                 vect.add(Strd1);
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

           VDate         = new Vector();
           VCompany      = new Vector();
           VRep          = new Vector();
           VPurpose      = new Vector();
           VToMeet       = new Vector();
           VInTime       = new Vector();
           VOutTime      = new Vector();
           VSNo          = new Vector();
           VTotalHours   = new Vector();
           VSpentTime    = new Vector();
           VDate1        = new Vector();

           String STotalKm="";
           try
           {
               int iErrectorCode = Integer.parseInt(SErrectorCode);
               int iStDate       = Integer.parseInt(common.pureDate(SStDate));
               int iDate         = Integer.parseInt(common.pureDate(SEnDate));
               Vector tVector    = visitorDomain.getErrectorWiseReport(iStDate,iDate,iErrectorCode);
               System.out.println("VEctor Size:"+tVector.size());
               int size= (tVector.size())/9;
               int m=0;
               for(int i=0;i<size;i++)      
               {
                    VSNo.addElement(String.valueOf(i+1));
                    VCompany.addElement(tVector.elementAt(m+0));
                    VRep.addElement(tVector.elementAt(m+2));
                    VPurpose.addElement(tVector.elementAt(m+3));
                    VToMeet.addElement(tVector.elementAt(m+4));
                    VInTime.addElement(tVector.elementAt(m+6));
                    VOutTime.addElement(tVector.elementAt(m+7));
                    VDate.addElement((String)tVector.elementAt(m+8));
                    int iEDate  = Integer.parseInt((String)VDate.elementAt(i));
                    VSpentTime.addElement(common.getTimeDiff(iEDate,iEDate,(String)VOutTime.elementAt(i),(String)VInTime.elementAt(i)));
                    VDate1.addElement(common.parseDate((String)tVector.elementAt(m+8)));

                    m=m+9;
               }

           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate1);
           //vect.addElement(VRep);
           //vect.addElement(VCompany);
           vect.addElement(VPurpose);
           //vect.addElement(VToMeet);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
           vect.addElement(VSpentTime);

           return vect;
     }
           
     public String getStatus()
     {
           return SStatus;
     }

}
