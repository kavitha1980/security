package Reports.Visitor;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;


public class VisitorAbstractRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     VisitorIn VDomain;

     Common common;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VDomain             = (VisitorIn) registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
               String SCompany  = (String)request.getParameter("CompanySelect");   
               System.out.println("Selected in rep:"+SCompany);

               String SStDate   = common.pureDate(request.getParameter("TStDate"));
               String SEnDate   = common.pureDate(request.getParameter("TEnDate"));

               String Diff      = common.getDateDiff(request.getParameter("TEnDate"),request.getParameter("TStDate"));

               System.out.println("Diff:"+Diff);

               int iDiff        = Integer.parseInt(Diff);

               if(iDiff>7)
               {
                    out.println("<html><head>");
                    out.println("<script>");
                    out.println(" alert(' 7 days r Valid');");
                    out.println(" </script>");
                    out.println("</head>");
                    out.println("</heml>");
               }
               else
               {
                    String SFile     = common.parseNull(request.getParameter("TFile"));
          
                    Vector VTotal    = VDomain.getVisitorCompanyInfo();
                    Vector VCode     = (Vector)VTotal.elementAt(0);
                    Vector VName     = (Vector)VTotal.elementAt(1);
          
                    int CCode        = Integer.parseInt(SCompany);   
                    int index        = VCode.indexOf(String.valueOf(CCode));
     
                    System.out.println("index:"+index);
                    System.out.println("Name:"+VName.elementAt(index));
     
                    SFile = (SFile.trim()).length()==0?"d:/VisitorAbstract.prn":SFile;
          
                    VisitorAbstractInformation info  = new VisitorAbstractInformation();
          
                    VisitorAbstractList theList    = new VisitorAbstractList(SStDate,SEnDate,SFile,index);
          
                    Vector vect            = new Vector();
                    vect                   = theList.getPrintValues();
          
                    Vector VInfo  = new Vector();
                    Vector VValue = new Vector();
          
                    VInfo.addElement("Report Name");
                    VInfo.addElement("Report Code");
                    VInfo.addElement("Report url/File Name");
                    VInfo.addElement("Status");
          
                    VValue.addElement("Visitor's  Abstract as on a Particular Date");
                    VValue.addElement(" ");
                    VValue.addElement(SFile);
                    VValue.addElement(theList.getStatus());
          
          
                    info.flashMessage(VInfo,VValue,vect,out);
          
               
               }                   
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

}
