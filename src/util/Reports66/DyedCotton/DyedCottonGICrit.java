/*


*/
package Reports.DyedCotton;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class DyedCottonGICrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     Vector VDyer,VDyerCode;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");
          setDyerData();

          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='main'>");
          out.println("<form method='GET' action='"+SServer+"Reports.DyedCotton.DyedCottonGIRep'>");

          out.println("<table>");
          out.println("<tr>");
          out.println("       <td><b>Dyer</b></td>");
          out.println("       <td><b>Material</b></td>");  
          out.println("       <td><b>From </b></td>");
          out.println("       <td><b>To  </b></td>");
          out.println("       <td><b>File</b></td>");
          out.println("       <td></td>");
          out.println("       <td></td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("       <td><select name='DyerSelect'>");
          for(int i=0;i<VDyerCode.size();i++)
          {
               out.println("<option value='"+(String)VDyerCode.elementAt(i)+"'>"+VDyer.elementAt(i)+"</option>");
          }
          out.println("</select>");
          out.println("</td>");

          out.println("       <td><select name='MatSelect'>");
          out.println("       <option value='WEB'>WEB</option>");
          out.println("       <option value='DC'>DC</option>");
          out.println("       <option value='SF'>SF</option>");
          out.println("       <option value='ALL'>ALL</option>");
          out.println("</select>");
          out.println("</td>");
          out.println("       <td><input type='text' name='TStDate' size='15'></td>");
          out.println("       <td><input type='text' name='TEnDate' size='15'></td>");
          out.println("       <td><input type='text' name='TFile' size='15'></td>");
          out.println("       <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("       <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("<tr>");

          out.println("</table>");

          out.println("</form>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
     private void setDyerData()
     {
          VDyer          = new Vector();
          VDyerCode      = new Vector();
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","scm","rawmat");
               Statement theStatement   = theConnection.createStatement(); 
               String QS                = "Select accode,Name from Supplier where length(rtrim(accode))>1 order by 2 ";
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VDyerCode.addElement(theResult.getString(1));
                    VDyer.addElement(theResult.getString(2));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

}

