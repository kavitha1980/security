package Reports.DyedCotton;
import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class DyedCottonGIList
{
     protected String SStDate;
     protected String SEndDate;
     protected String SDyerId;
     protected String SMaterial;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;


     Vector VSNo,VGINo,VGIDate,VAGNNo,VAGNDate,VDCNo,VDCDate,VMixLotNo,VMixLotName,VWebType,VNoils;
     Vector VBaseCode,VMoisture,VDyerWeight,VMillWeight,VSF;
 

     Vector Vhead;
     Common common = new Common();


     String SStatus = "";

     DyedCottonGIList(String SDyerId,String SMaterial,String SStDate,String SEndDate,String SFile)
     {
            this.SDyerId      = SDyerId;
            this.SMaterial    = SMaterial;
            this.SStDate      = SStDate;
            this.SEndDate     = SEndDate;
            this.SFile        = SFile;

            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Dyed Material Inward List from  "+common.parseDate(SStDate)+" TO  "+common.parseDate(SEndDate)+" \n";
            Vector VHead  = getInwardHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getInwardListBody();

            new DyedMaterialDocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getInwardHead()
      {
           Vector vect = new Vector();
           System.out.println("Inside: head");
 

           String Head1[]={"SNO","G.I.No","GIDate","AGNNo","AGNDate","DCNo","DCDate","MixLotNo","MixLotName","WebType","Noils","BaseCode","Moisture","DyerWeight","MillWeight","SF"};
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();
           String Sha16=((String)Head1[15]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);
           Vhead.addElement(Sha16);
           
           Sha1  = common.Rad(Sha1,5)+common.Space(5);
           Sha2  = common.Pad(Sha2,10)+SInt;
           Sha3  = common.Pad(Sha3,10)+SInt;
           Sha4  = common.Pad(Sha4,10)+SInt;
           Sha5  = common.Pad(Sha5,10)+SInt;
           Sha6  = common.Pad(Sha6,10)+SInt;
           Sha7  = common.Pad(Sha7,10)+SInt;
           Sha8  = common.Pad(Sha8,10)+SInt;
           Sha9  = common.Pad(Sha9,10)+SInt;
           Sha10 = common.Rad(Sha10,10)+SInt;
           Sha11 = common.Rad(Sha11,10)+SInt;
           Sha12 = common.Rad(Sha12,10)+SInt;
           Sha13 = common.Rad(Sha13,10)+SInt;
           Sha14 = common.Rad(Sha14,10)+SInt;
           Sha15 = common.Rad(Sha15,10)+SInt;
           Sha16 = common.Rad(Sha16,10)+SInt;

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+"\n";
           vect.add(Strh1);
           return vect;
     }
     public Vector getInwardListBody()
     {
           Vector vect = new Vector();
           System.out.println("InwardList");
           for(int i=0;i<VGIDate.size();i++)
           {

                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VGINo.elementAt(i);
                 String Sda3  = (String)VGIDate.elementAt(i);
                 String Sda4  = (String)VAGNNo.elementAt(i);
                 String Sda5  = (String)VAGNDate.elementAt(i);
                 String Sda6  = (String)VDCNo.elementAt(i);
                 String Sda7  = (String)VDCDate.elementAt(i);
                 String Sda8  = (String)VMixLotNo.elementAt(i);
                 String Sda9  = (String)VMixLotName.elementAt(i);
                 String Sda10 = (String)VWebType.elementAt(i);
                 String Sda11 = (String)VNoils.elementAt(i);
                 String Sda12 = (String)VBaseCode.elementAt(i);
                 String Sda13 = (String)VMoisture.elementAt(i);
                 String Sda14 = (String)VDyerWeight.elementAt(i);
                 String Sda15 = (String)VMillWeight.elementAt(i);
                 String Sda16 = (String)VSF.elementAt(i);

                 Sda1  = common.Rad(Sda1,5)+common.Space(5);
                 Sda2  = common.Pad(Sda2,10)+SInt;
                 Sda3  = common.Pad(Sda3,10)+SInt;
                 Sda4  = common.Pad(Sda4,10)+SInt;
                 Sda5  = common.Pad(Sda5,10)+SInt;
                 Sda6  = common.Pad(Sda6,10)+SInt;
                 Sda7  = common.Pad(Sda7,10)+SInt;
                 Sda8  = common.Pad(Sda8,10)+SInt;
                 Sda9  = common.Pad(Sda9,10)+SInt;
                 Sda10 = common.Rad(Sda10,10)+SInt;
                 Sda11 = common.Rad(Sda11,10)+SInt;
                 Sda12 = common.Rad(Sda12,10)+SInt;
                 Sda13 = common.Rad(Sda13,10)+SInt;
                 Sda14 = common.Rad(Sda14,10)+SInt;
                 Sda15 = common.Rad(Sda15,10)+SInt;
                 Sda16 = common.Rad(Sda16,10);
                 


                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+"\n";
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

          System.out.println("inside data");

          VSNo               = new Vector();
          VGINo              = new Vector();
          VGIDate            = new Vector();
          VAGNNo             = new Vector();
          VAGNDate           = new Vector();
          VDCNo              = new Vector();
          VDCDate            = new Vector();
          VMixLotNo          = new Vector();
          VMixLotName        = new Vector();
          VWebType           = new Vector();
          VNoils             = new Vector();
          VBaseCode          = new Vector();
          VMoisture          = new Vector();
          VDyerWeight        = new Vector();
          VMillWeight        = new Vector();
          VSF                = new Vector();
          
          try
          {

               String SDate   = SEndDate;
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@bulls:1521:amarml","scm","rawmat");
               Statement stat                   = conn.createStatement();



               ResultSet res                    = stat.executeQuery(getQString());
               int iSNo=1;
               while(res.next())
               {


                    String SGINo             = res.getString(1);
                    String SGIDate           = common.parseDate((String)res.getString(2));
                    String SAGNNo            = res.getString(3);
                    String SAGNDate          = common.parseDate((String)res.getString(4));
                    String SDCNo             = res.getString(5);
                    String SDCDate           = common.parseDate((String)res.getString(6));
                    String SMixLotNo         = res.getString(7);
                    String SMixLotName       = common.parseDate((String)res.getString(8));
                    String SWebType          = common.parseNull(res.getString(9));
                    String SNoils            = common.parseDate((String)res.getString(10));
                    String SBaseCode         = res.getString(11);
                    String SMoisture         = res.getString(12);
                    String SDyerWeight       = res.getString(13);
                    String SMillWeight       = res.getString(14);
                    String SSF               = res.getString(15);
                    
                    VSNo.addElement(String.valueOf(iSNo));
                    VGINo.addElement(SGINo);
                    VGIDate.addElement(SGIDate);
                    VAGNNo.addElement(SAGNNo);
                    VAGNDate.addElement(SAGNDate);
                    VDCNo.addElement(SDCNo);
                    VDCDate.addElement(SDCDate);
                    VMixLotNo.addElement(SMixLotNo);
                    VMixLotName.addElement(SMixLotName);
                    VWebType.addElement(SWebType);
                    VNoils.addElement(SNoils);
                    VBaseCode.addElement(SBaseCode);
                    VMoisture.addElement(SMoisture);
                    VDyerWeight.addElement(SDyerWeight);
                    VMillWeight.addElement(SMillWeight);
                    VSF.addElement(SSF);
                    iSNo=iSNo+1;                     

              }
          }
          catch(Exception ex)
          {
              System.out.println(ex);
          }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VGINo);
           vect.addElement(VGIDate);
           vect.addElement(VAGNNo);
           vect.addElement(VAGNDate);
           vect.addElement(VDCNo);
           vect.addElement(VDCDate);
           vect.addElement(VMixLotNo);
           vect.addElement(VMixLotName);
           vect.addElement(VWebType);
           vect.addElement(VNoils);
           vect.addElement(VBaseCode);
           vect.addElement(VMoisture);
           vect.addElement(VDyerWeight);
           vect.addElement(VMillWeight);
           vect.addElement(VSF);

           return vect;
     }
           
     
     public String getQString()
     {
          String QString="";

          if (SMaterial.equals("WEB"))
          {
               QString        = getDyerWebReceiptsQS();
               return QString;
          }

          if (SMaterial.equals("DC"))
          {
               QString        = getDyerDCReceiptsQS();
               return QString;
          }
          if (SMaterial.equals("SF"))
          {
               QString        = getDyerSFReceiptsQS();
               return QString;
          }
          if(SMaterial.equals("ALL"))
          {
               QString        = getDyerWebReceiptsQS()+" UNION ALL "+getDyerDCReceiptsQS()+" UNION ALL "+getDyerSFReceiptsQS();

try
{
PrintWriter out  = new PrintWriter(new BufferedWriter(new FileWriter("d:\\security\\test.sql")));
out.println(QString);
out.close();
}
catch(Exception e){}
               return QString;
          }               
          return QString;
     }


     public String getStatus()
     {
           return SStatus;
     }
               //" WEBAGNDETAILS.WebType as WEBTYPE,"+

     private String getDyerWebReceiptsQS()
     {
          String QS = "";

          QS = " Select AGN.GINo as GINO,"+
               " AGN.GIDate as GIDATE,"+
               " AGN.AGNNO as AGNNO,"+
               " AGN.AGNDate as AGNDATE,"+
               " AGN.DCNo as DCNO,"+
               " AGN.DCDate as DCDATE,"+
               " WEBAGNDETAILS.MixLotNo as MIXLOTNO,"+
               " MIXLOT.MixLotName as MIXLOTNAME,"+
               " WEBTYPE.WebTypeName as webtype,"+
               " WEBAGNDETAILS.Noils as NOILS,"+
               " ' ' as BASECODE,"+
               " 0.0 as MOISTURE, "+
               " WEBAGNDETAILS.DyerWeight as DYERWEIGHT,"+
               " WEBAGNDETAILS.MillWeight as MILLWEIGHT,"+
               " 'WEB' as TRAN "+
               " From ((AGN "+
               " Inner Join  WEBAGNDETAILS On AGN.AGNNo=WEBAGNDETAILS.AGNNo) "+
               
               " Inner Join MixLot On WEBAGNDETAILS.MixLotNo = MixLot.MixLotNo) "+
               " Inner Join WEBTYPE On WEBAGNDETAILS.WebType = WEBTYPE.WebTypeCode"+
               " Where AGN.ACCODE='"+SDyerId+"' And (AGN.GIDate>="+SStDate+" And AGN.GIDate<="+SEndDate+")";
          return QS;
     }
    
     private String getDyerDCReceiptsQS()
     {
               //" AGNDETAILS.WebType as WEBTYPE,"+

          String QS = "";

          QS = " Select AGN.GINo as GINO,"+
               " AGN.GIDate as GIDATE,"+
               " AGN.AGNNO as AGNNO,"+
               " AGN.AGNDate as AGNDATE,"+
               " AGN.DCNo as DCNO,"+
               " AGN.DCDate as DCDATE,"+
               " AGNDETAILS.MixLotNo as MIXLOTNO,"+
               " MIXLOT.MixLotName as MIXLOTNAME,"+
               " WEBTYPE.WebTypeName as webtype,"+
               " AGNDETAILS.Noils as NOILS,"+
               " AGNDETAILS.BaseCode as BASECODE,"+
               " AGNDETAILS.Moisture as MOISTURE, "+
               " AGNDETAILS.DyerWeight as DYERWEIGHT,"+
               " AGNDETAILS.MillWeight as MILLWEIGHT,"+
               " 'DC' as TRAN "+
               " From ((AGN "+
               " Inner Join  AGNDETAILS On AGN.AGNNo = AGNDETAILS.AGNNo) "+
               " Inner Join MixLot On AGNDETAILS.MixLotNo = MixLot.MixLotNo )"+
               " Inner Join WEBTYPE On AGNDETAILS.WebType = WEBTYPE.WebTypeCode "+
               " Where  AGNDETAILS.DCorSF = 1 AND AGN.ACCODE = '"+SDyerId+"' And (AGN.GIDate>="+SStDate+" And AGN.GIDate<="+SEndDate+")";

          return QS;
     }

     private String getDyerSFReceiptsQS()
     {
          String QS = "";

          QS = " Select AGN.GINo as GINO,"+
               " AGN.GIDate as GIDATE,"+
               " AGN.AGNNO as AGNNO,"+
               " AGN.AGNDate as AGNDATE,"+
               " AGN.DCNo as DCNO,"+
               " AGN.DCDate as DCDATE,"+
               " ' ' as MIXLOTNO, "+
               " ' ' as MIXLOTNAME, "+
               " ' ' as WEBTYPE, "+
               " 0 as NOILS, "+
               " AGNDETAILS.BaseCode as BASECODE,"+
               " AGNDETAILS.Moisture as MOISTURE, "+
               " AGNDETAILS.DyerWeight as DYERWEIGHT,"+
               " AGNDETAILS.MillWeight as MILLWEIGHT,"+
               " 'SF' as TRAN "+
               " From AGN "+
               " Inner Join  AGNDETAILS On AGN.AGNNo = AGNDETAILS.AGNNo "+
               " Where AGNDETAILS.DCorSF = 2 AND AGN.ACCODE = '"+SDyerId+"' And (AGN.GIDate>="+SStDate+" And AGN.GIDate<="+SEndDate+")";           
 
          return QS;
     }


}
