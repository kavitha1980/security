package Reports.Vehicle;
import java.util.*;
import java.sql.*;

public class MonthClass implements rndi.CodedNames
{
        int iMonth=0;
        Vector VEmployees;

        MonthClass()
        {
        }
        MonthClass(int iMonth)
        {
                this.iMonth = iMonth;
                VEmployees  = new Vector();
        }
        public void append(EmployeeId theEmployeeId,int iEmployeeCode)
        {
                Employee employee=null;
                int index = indexOf(iEmployeeCode);
                if(index == -1)
                {
                        employee = new Employee(theEmployeeId,iEmployeeCode);
                        VEmployees.addElement(employee);
                        index = VEmployees.size()-1;
                }
                employee = (Employee)VEmployees.elementAt(index);
                employee.setStatus(-1);
        }

        public int getMonth()
        {
                return iMonth;
        }
        // this will return no of male/female employees of this dept
        public int getAlloted(String SSex)
        {
                int iNos=0;

                for(int i = 0; i<VEmployees.size(); i++)
		{	
                        Employee employee = (Employee)VEmployees.elementAt(i);
                        if(!(employee.getSex().equals(SSex)))
                                continue;                                
                        iNos++;
                }
                return iNos;
        }

        private int indexOf(int iEmployeeCode)
        {
                int index = -1;
                for(int i=0;i<VEmployees.size();i++)
                {
                        Employee employee = (Employee)VEmployees.elementAt(i);
                        if(employee.getEmployeeCode()==iEmployeeCode)
                        {
                                index = i;
                                break;
                        }
                }
                return index;
        }

}
