package Reports.Vehicle;
import util.*;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import  domain.jdbc.*;


public class VehicleRegisterList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;
     protected String SSelected;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VSNo,VRegNo,VName,VOutDate,VOutTime,VInDate,VInTime,VStKm,VEndKm,VTotalKm,VPlace,VPurpose,VDriver,VSecurity,VDiNo,VKiNo,VDiDate,VKiDate,VDQty,VKQty,VBunk;
     Vector Vhead,VTotalKms;
     Common common = new Common();

     VehicleInfo vDomain;

     String SStatus = "";

     String SVehicleName="";
     String SVehicleRegNo="";

     VehicleRegisterList(String SStDate,String SEnDate,String SFile,String SSelected)
     {
            this.SStDate      = SStDate;
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            this.SSelected    = SSelected;
            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  vDomain               = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Vehicle Register List From "+common.parseDate(SStDate)+"  To  "+common.parseDate(SEnDate)+ "\n";
            Vector VHead  = getVehicleHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVehicleListBody();
            new VehicleDocPrint(VBody,VHead,STitle,SFile,SVehicleName,SVehicleRegNo);
      }

      public Vector getVehicleHead()
      {
           Vector vect = new Vector();

           String Head1[]={"OutDate","OutTime","InDate","InTime","StartingKm","EndingKm","TotalKms","Place","Purpose","TripDriverName","SecurityName","DINo","KiNo","DIDate","KIDate","DQty","KQty","SNo","BunkName"};        
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();
           String Sha16=((String)Head1[15]).trim();
           String Sha17=((String)Head1[16]).trim();
           String Sha18=((String)Head1[17]).trim();
           String Sha19=((String)Head1[18]).trim();


           Vhead  = new Vector();

           Vhead.addElement(Sha18);
           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);
           Vhead.addElement(Sha16);
           Vhead.addElement(Sha17);
           Vhead.addElement(Sha19);


           Sha18 = common.Rad(Sha18,5)+common.Space(5)+"�"+SInt;
           Sha1  = common.Pad(Sha1,12)+"�"+SInt;
           Sha2  = common.Pad(Sha2,15)+"�"+SInt;
           Sha3  = common.Pad(Sha3,12)+"�"+SInt;
           Sha4  = common.Pad(Sha4,15)+"�"+SInt;
           Sha5  = common.Pad(Sha5,10)+"�"+SInt;
           Sha6  = common.Pad(Sha6,10)+"�"+SInt;
           Sha7  = common.Pad(Sha7,10)+"�"+SInt;
           Sha8  = common.Pad(Sha8,20)+"�"+SInt;
           Sha9  = common.Pad(Sha9,15)+"�"+SInt;
           Sha10  = common.Pad(Sha10,20)+"�"+SInt;
           Sha11  = common.Pad(Sha11,20)+"�"+SInt;
           Sha12  = common.Pad(Sha12,8)+"�"+SInt;
           Sha13  = common.Pad(Sha13,8)+"�"+SInt;
           Sha14  = common.Pad(Sha14,15)+"�"+SInt;
           Sha15  = common.Pad(Sha15,15)+"�"+SInt;
           Sha16  = common.Pad(Sha16,5)+"�"+SInt;
           Sha17  = common.Pad(Sha17,5)+"�"+SInt;
           Sha19  = common.Pad(Sha19,15)+"�"+SInt;

           String Strh1 = Sha18+Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+Sha17+Sha19+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVehicleListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VOutDate.size();i++)
           {


                 String Sda1  = (String)VOutDate.elementAt(i);
                 String Sda2  = (String)VOutTime.elementAt(i);
                 String Sda3  = (String)VInDate.elementAt(i);
                 String Sda4  = (String)VInTime.elementAt(i);
                 String Sda5  = (String)VStKm.elementAt(i);
                 String Sda6  = (String)VEndKm.elementAt(i);
                 String Sda7  = (String)VTotalKm.elementAt(i);
                 String Sda8 = (String)VPlace.elementAt(i);
                 String Sda9 = (String)VPurpose.elementAt(i);
                 String Sda10 = (String)VDriver.elementAt(i);
                 String Sda11 = (String)VSecurity.elementAt(i);
                 String Sda12 = (String)VDiNo.elementAt(i);
                 String Sda13 = (String)VKiNo.elementAt(i);
                 String Sda14 = (String)VDiDate.elementAt(i);
                 String Sda15 = (String)VKiDate.elementAt(i);
                 String Sda16 = (String)VDQty.elementAt(i);
                 String Sda17 = (String)VKQty.elementAt(i);
                 String Sda18 = (String)VSNo.elementAt(i);
                 String Sda19 = (String)VBunk.elementAt(i);


                 Sda18   = common.Rad(Sda18,5)+common.Space(5)+"�"+SInt;
                 Sda1    = common.Pad(Sda1,12)+"�"+SInt;
                 Sda2    = common.Pad(Sda2,15)+"�"+SInt;
                 Sda3    = common.Pad(Sda3,12)+"�"+SInt;
                 Sda4    = common.Pad(Sda4,15)+"�"+SInt;
                 Sda5    = common.Pad(Sda5,10)+"�"+SInt;
                 Sda6    = common.Pad(Sda6,10)+"�"+SInt;
                 Sda7    = common.Pad(Sda7,10)+"�"+SInt;
                 Sda8    = common.Pad(Sda8,20)+"�"+SInt;
                 Sda9    = common.Pad(Sda9,15)+"�"+SInt;
                 Sda10   = common.Pad(Sda10,20)+"�"+SInt;
                 Sda11   = common.Pad(Sda11,20)+"�"+SInt;
                 Sda12   = common.Pad(Sda12,8)+"�"+SInt;
                 Sda13   = common.Pad(Sda13,8)+"�"+SInt;
                 Sda14   = common.Pad(Sda14,15)+"�"+SInt;
                 Sda15   = common.Pad(Sda15,15)+"�"+SInt;
                 Sda16   = common.Pad(Sda16,5)+"�"+SInt;
                 Sda17   = common.Pad(Sda17,5)+"�"+SInt;
                 Sda19   = common.Pad(Sda19,15)+"�"+SInt;

                 //Sda10 = common.Rad(Sda10,10);
                 


                 String Strd = Sda18+Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda19;
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {
     //Vector VRegNo,VName,VOutDate,VOutTime,VInDate,VInTime,VStKm,VEndKm,VTotalKm,VPlace,VPurpose,VDriver,VSecurity;



           VSNo          = new Vector();
           VRegNo        = new Vector();
           VName         = new Vector();
           VOutDate      = new Vector();
           VOutTime      = new Vector();
           VInDate       = new Vector();
           VInTime       = new Vector();
           VStKm         = new Vector();
           VEndKm        = new Vector();
           VTotalKm      = new Vector();
           VPlace        = new Vector();
           VPurpose      = new Vector();
           VDriver       = new Vector();
           VSecurity     = new Vector();
           VDiNo         = new Vector();
           VKiNo         = new Vector();
           VDiDate       = new Vector();
           VKiDate       = new Vector();
           VDQty         = new Vector();
           VKQty         = new Vector();
           VBunk         = new Vector();
           VTotalKms     = new Vector();

           String STotalKm="";
           try
           {

                      String SDate   = SEnDate;
                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                      Statement stat                   = conn.createStatement();
                      ResultSet res                    = stat.executeQuery(getQString());

                      //ResultSet res                      = vDomain.getVehicleReport(getQString());

                      int i=0;
                      while (res.next())
                      {
                                 String SRegNo         = res.getString(1);
                                 String SName          = res.getString(2);
                                 String SOutDate       = common.parseDate((String)res.getString(3));
                                 String SOutTime       = res.getString(4);
                                 String SInDate        = common.parseDate((String)res.getString(5));
                                 String SInTime        = res.getString(6);
                                 String SStKm          = res.getString(7);
                                 String SEndKm         = res.getString(8);
                                 int iskm              = Integer.parseInt(SStKm);
                                 int iendkm            = Integer.parseInt(SEndKm);
                                 int itot              = iendkm-iskm;

                                 if(itot<0)
                                     STotalKm   = "Not Valid";   
                                 else
                                     STotalKm       = Integer.toString(itot);

                                 //String STotalKm       = common.parseNull(res.getString(9));
                                 String SPlace         = res.getString(9);
                                 String SPurpose       = res.getString(10);
                                 String SDriver        = res.getString(11);
                                 String SSecurity      = common.parseNull((String)res.getString(12));
                                 String SDiNo                 = common.parseNull((String)res.getString(13));
                                 String SKiNo                 = common.parseNull((String)res.getString(14));
                                 String SDiDate               = common.parseDate(common.parseNull((String)res.getString(15)));
                                 String SKiDate               = common.parseDate(common.parseNull((String)res.getString(16)));
                                 String SDQty                 = common.parseNull((String)res.getString(17));
                                 String SKQty                 = common.parseNull((String)res.getString(18));
                                 String SBunk                 = common.parseNull((String)res.getString(19));  

                                 SVehicleName  = SName;
                                 SVehicleRegNo = SRegNo;
                                 VSNo.addElement(String.valueOf(i+1));
                                 VRegNo.addElement(SRegNo);
                                 VName.addElement(SName);
                                 VOutDate.addElement(SOutDate);
                                 VOutTime.addElement(SOutTime);
                                 VInDate.addElement(SInDate);
                                 VInTime.addElement(SInTime);
                                 VStKm.addElement(SStKm);
                                 VEndKm.addElement(SEndKm);
                                 VTotalKm.addElement(STotalKm);
                                 VPlace.addElement(SPlace);
                                 VPurpose.addElement(SPurpose);
                                 VDriver.addElement(SDriver);
                                 VSecurity.addElement(SSecurity);
                                 VDiNo.addElement(SDiNo);
                                 VKiNo.addElement(SKiNo);


                                 if(Integer.parseInt(SKQty)==0)
                                 {
                                        VKQty.addElement(" ");

                                 }
                                 else
                                 {
                                        VKQty.addElement(SKQty);
                                 }       
                                 if(Integer.parseInt(SDQty)==0)
                                 {
                                        VDQty.addElement(" ");
                                 }
                                 else
                                 {
                                        VDQty.addElement(SDQty);
                                   
                                 }
                                 if(SDiDate.equals("0"))
                                 {
                                        VDiDate.addElement(" ");
                                 }
                                 else
                                 {
                                        VDiDate.addElement(SDiDate);
                                 }       

                                 if(SKiDate.equals("0"))
                                 {
                                        VKiDate.addElement(" ");
                                 }
                                 else
                                 {
                                        VKiDate.addElement(SKiDate);
                                 }       

                                 VBunk.addElement(SBunk);

                                 i=i+1;
                                   
                      }
                      res.close();
                      res          = stat .executeQuery(getTotalKmQuery());
                      while(res.next())
                      {
                              VTotalKms.addElement(res.getString(1));
                      }
                      res.close();

           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VRegNo);
           vect.addElement(VName);
           vect.addElement(VOutDate);
           vect.addElement(VOutTime);
           vect.addElement(VInDate);
           vect.addElement(VInTime);
           vect.addElement(VStKm);
           vect.addElement(VEndKm);
           vect.addElement(VTotalKm);
           vect.addElement(VPlace);
           vect.addElement(VPurpose);
           vect.addElement(VDriver);
           vect.addElement(VSecurity);
           vect.addElement(VDiNo);
           vect.addElement(VKiNo);
           vect.addElement(VDiDate);
           vect.addElement(VKiDate);
           vect.addElement(VDQty);
           vect.addElement(VKQty);
           vect.addElement(VBunk);
           vect.addElement(VTotalKms);
           return vect;
     }
           

     public String getQString()
     {
           //String QString = "SELECT GateInward.GINo, GateInward.GIDate, Supplier.Name, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate "+
                    //" FROM (GateInward INNER JOIN Supplier ON GateInward.Sup_Code = Supplier.Ac_Code) INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                    // "Where GateInward.GIDate <= '"+SEnDate+"' ";
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty,BunkName from Vehicles "+
                               " where OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' And Status=1 AND VehicleNo = '"+SSelected+"' order by OutDate ";    
           return QString;
     }


     public String getTotalKmQuery()
     {
           String QString =    " Select Sum(runkm) from ("+
                               " select Sum(ekm-skm) as runkm,VDate from "+
                               " (select distinct(OutDate) as VDate ,Min(StKm) as skm ,"+
                               " Max(EndKm)  as eKm from Vehicles "+
                               " where  OutDate>='"+SStDate+"' and  outDate <='"+SEnDate+"' and "+
                               " VehicleNo = '"+SSelected+"' "+
                               " group by OutDate) group by VDate ) ";
           return QString;
     }

     public String getStatus()
     {
           return SStatus;
     }

}
