package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class FuelMonthAnalysisList
{

     protected String SSelected;
     protected int iMonthCode1,iMonthCode2;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;
     int index=1,iCount=0;


     String SMonths[]={};

     Vector VSNo,VMonthName,VResultSt,VResultEnd,VResultRunned,VResultKero,VResultDiesel,VResultTotal,VResultAvg,VResultVar,VResultExcess;
     Vector VVehicleNo,VKQty,VDQty,VFuelTotal,VSMonths;
     Vector VSno,VVehicleName,VInitialKm;
     Vector VClosingKm,VRunningKm,VDieselConsumed,VAverageKm,VStdKm;
     Vector VVariationKm,VExcessDiesel,VStKm,VMonthStKm,VMonthEndKm;
     Vector VHead;

     Common common = new Common();

     Vector VHead2      = new Vector(); 

     int iMonth[] = new int[12]; 

     FuelMonthAnalysisList(String SSelected,int iMonthCode1,int iMonthCode2,String SFile)
     {
            this.SSelected    = SSelected; 
            this.iMonthCode1  = iMonthCode1;
            this.iMonthCode2  = iMonthCode2;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setFuelList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setFuelList()
     {
            try
            {
                 int iNextMonth=iMonthCode1;
                 iMonth[0]      = iMonthCode1;

                 if(iMonthCode1==iMonthCode2)
                 {

                      iMonth[0]= iNextMonth;
                      iCount=1;
                 }
                 else
                 {
                      do
                      {
                         
                         iNextMonth = common.getNextMonth(iNextMonth);
                         System.out.println("NextMonth:"+iNextMonth);
                         iMonth[index]= iNextMonth;
                         iCount = iCount+1;
                         index++;
                      }while(!(iNextMonth==iMonthCode2));
                 }

                 for(int i=0;i<iCount;i++)
                 {
                    System.out.println("Month:"+iMonth[i]);
                 }   

                 setDataIntoVector();
                 String STitle = " Fuel Consumption  List For The Month : "+common.getMonthName(iMonthCode1)+"And"+common.getMonthName(iMonthCode2)+"\n";
                 Vector VHead  = getVehicleHead();
                 iLen = ((String)VHead.elementAt(0)).length();
                 Strline = common.Replicate("-",iLen)+"\n";
                 Vector VBody  = getVehicleListBody();
                 //String SName      = (String)VName.elementAt(0);
                 //new Fuel(VBody,VHead,STitle,SFile,VTotal,SSelected,SName);
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }

      public Vector getVehicleHead()
      {
           Vector VStore      = new Vector();
           VSMonths    = new Vector();

           //               {    1  ,    2  ,     3      ,     4       ,      5      ,         6       ,       7       ,      8     ,     9      ,       10        ,            11           };
           String SHead[] = { "S.NO","MONTH","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","KERO.CONSUMED","TOTAL FUEL","AVERAGE/KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };



           for(int i=0;i<iCount;i++)
           {
               //SMonths[i]= common.getMonthName(iMonth[i]);
               System.out.println("MonthNAme:"+common.getMonthName(iMonth[i]));
               VSMonths.addElement(common.getMonthName(iMonth[i]));
           }
           String SHead1  = ((String)SHead[0]).trim();
           String SHead2  = ((String)SHead[1]).trim();
           String SHead3  = ((String)SHead[2]).trim();
           String SHead4  = ((String)SHead[3]).trim();


           String SHead5  = ((String)SHead[4]).trim();
           String SHead6  = ((String)SHead[5]).trim();
           String SHead7  = ((String)SHead[6]).trim();
           String SHead8  = ((String)SHead[7]).trim();
           String SHead9  = ((String)SHead[8]).trim();
           String SHead10 = ((String)SHead[9]).trim();
           String SHead11 = ((String)SHead[10]).trim();


           VHead  = new Vector();


           SHead1   = common.Pad(SHead1,5)+SInt;
           SHead2   = common.Pad(SHead2,8)+SInt;
           SHead3   = common.Pad(SHead3,8)+SInt;
           SHead4   = common.Pad(SHead4,15)+SInt;
           SHead5   = common.Pad(SHead5,15)+SInt;
           SHead6   = common.Pad(SHead6,15)+SInt;
           SHead7   = common.Pad(SHead7,20)+SInt;
           SHead8   = common.Pad(SHead8,20)+SInt;
           SHead9   = common.Pad(SHead9,20)+SInt;
           SHead10  = common.Pad(SHead10,20)+SInt;
           SHead11  = common.Pad(SHead11,6);

           VHead.addElement(SHead1);
           VHead.addElement(SHead2);
           VHead.addElement(SHead3);
           VHead.addElement(SHead4);
           VHead.addElement(SHead5);
           VHead.addElement(SHead6);
           VHead.addElement(SHead7);
           VHead.addElement(SHead8);
           VHead.addElement(SHead9);
           VHead.addElement(SHead10);
           VHead.addElement(SHead11);

           return VHead;

     }
     public Vector getVehicleListBody()
     {
          Vector vect = new Vector();

          VSNo           = new Vector();
          VMonthName     = new Vector();
          VResultSt      = new Vector();
          VResultEnd     = new Vector();
          VResultRunned  = new Vector();
          VResultKero    = new Vector();
          VResultDiesel  = new Vector();
          VResultTotal   = new Vector();
          VResultAvg     = new Vector();
          VResultVar     = new Vector();
          VResultExcess  = new Vector();


          for(int i=0;i<iCount;i++)
          {
               int iStdKm           = 6;
               String SNo          = String.valueOf(i+1);
               String SMonth       = (String)VSMonths.elementAt(i);
               String SInitial     = (String)VMonthStKm.elementAt(i);
               String SEnd         = (String)VMonthEndKm.elementAt(i);
               String SRunnedKms   = Integer.toString((Integer.parseInt((String)VMonthEndKm.elementAt(i)))-(Integer.parseInt((String)VMonthStKm.elementAt(i))));
               String SKQty        = (String)VKQty.elementAt(i);
               String SDQty        = (String)VDQty.elementAt(i);
               String STotalFuelQty= Integer.toString((Integer.parseInt((String)VKQty.elementAt(i)))+(Integer.parseInt((String)VDQty.elementAt(i))));    
               String SAvgKms      = Integer.toString(Integer.parseInt(SRunnedKms)/Integer.parseInt(STotalFuelQty));
               String SVariation   = Integer.toString((iStdKm*(common.toInt(STotalFuelQty)))-(common.toInt(SRunnedKms)));
               String SExcess      = Integer.toString((common.toInt(SVariation))/iStdKm);



               SNo            = common.Pad(SNo,5)+SInt;
               SMonth         = common.Pad(SMonth,8)+SInt;
               SInitial       = common.Pad(SInitial,8)+SInt;
               SEnd           = common.Pad(SEnd,15)+SInt;
               SRunnedKms     = common.Pad(SRunnedKms,15)+SInt;
               SKQty          = common.Pad(SKQty,15)+SInt;
               SDQty          = common.Pad(SDQty,20)+SInt;
               STotalFuelQty  = common.Rad(STotalFuelQty,5)+common.Space(5)+SInt;
               SAvgKms        = common.Rad(SAvgKms,6)+common.Space(5)+SInt;
               SVariation     = common.Rad(SVariation,5)+common.Space(5)+SInt;
               SExcess        = common.Rad(SExcess,6)+common.Space(5);


               VSNo .addElement(SNo);
               VMonthName     .addElement(SMonth);
               VResultSt      .addElement(SInitial);
               VResultEnd     .addElement(SEnd);
               VResultRunned  .addElement(SRunnedKms);
               VResultKero    .addElement(SKQty);
               VResultDiesel  .addElement(SDQty);
               VResultTotal   .addElement(STotalFuelQty);
               VResultAvg     .addElement(SAvgKms);
               VResultVar     .addElement(SVariation);
               VResultExcess  .addElement(SExcess);

          }
          vect.addElement(VSNo);
          vect.addElement(VMonthName);
          System.out.println("VMonthSize:"+VMonthName.size());
          vect.addElement(VResultSt);
          vect.addElement(VResultEnd);
          vect.addElement(VResultRunned);
          vect.addElement(VResultKero);
          vect.addElement(VResultDiesel);
          vect.addElement(VResultTotal);
          vect.addElement(VResultAvg);
          vect.addElement(VResultVar);
          vect.addElement(VResultExcess);

          return vect;
     }

     public void setDataIntoVector()
     {
          VVehicleNo     = new Vector();
          VKQty          = new Vector();
          VDQty          = new Vector();
          VStKm          = new Vector();
          VClosingKm     = new Vector();
          VMonthStKm     = new Vector();
          VInitialKm     = new Vector();
          VMonthEndKm    = new Vector();

           try
           {

                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                      Statement stat                   = conn.createStatement();
                      for(int i=0;i<iCount;i++)
                      {
                           ResultSet res                    = stat.executeQuery(getQString(iMonth[i]));
                           while(res.next())
                           {
                                String SVehicleNo = res.getString(1);
                                String SKQTY      = res.getString(2);
                                String SDQTY      = res.getString(3);

                                VVehicleNo.addElement(SVehicleNo);
                                VKQty.addElement(SKQTY);
                                VDQty.addElement(SDQTY);
                                System.out.println("SKQty:"+SKQTY);
                                System.out.println("SKQty:"+SDQTY);

                           }
                           res.close();
                           res = stat.executeQuery(getQString1(iMonth[i]));
                           while(res.next())
                           {
                                   String SFuelKms = res.getString(1);
                                   VMonthStKm.addElement(SFuelKms);
                           }
                           res.close();
                           res = stat.executeQuery(getQString2(iMonth[i]));
                           while(res.next())
                           {
                                  String SClosingReading = res.getString(1);
                                  VMonthEndKm.addElement(SClosingReading);
                           }
                           res.close();
                      }
           }
           catch(Exception e)
           {
               e.printStackTrace();
           }    

     }
     public Vector getPrintValues()
     {
           Vector VStore = new Vector();

           VStore.addElement(VHead);
           //VStore.addElement(VVehicleNo);
           //VStore.addElement(VVehicleName);
           VStore.addElement(VSNo);
           VStore.addElement(VMonthName);
           VStore.addElement(VResultSt);
           VStore.addElement(VResultEnd);
           VStore.addElement(VResultRunned);
           VStore.addElement(VResultKero);
           VStore.addElement(VResultDiesel);
           VStore.addElement(VResultTotal);
           VStore.addElement(VResultAvg);
           VStore.addElement(VResultVar);
           VStore.addElement(VResultExcess);


           return VStore;

     }
                                   

     public String getQString(int iMonthCode)
     {

           System.out.println("Month:"+iMonthCode);
           String QString    = " SELECT VEHICLENO,SUM(KQTY),SUM(DQTY) FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iMonthCode1+" AND Status=1 AND VehicleName = '"+SSelected+"' GROUP BY VEHICLENO ";

           return QString;
     }
     public String getQString1(int iCurMonth)
     {
           String QString1   = " SELECT FUELKMS FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iCurMonth+" AND Status=1 AND VehicleName = '"+SSelected+"' and Purpose='TO FILL FUEL' ";

           return QString1;
     }

     public String getQString2(int iCurMonth)
     {
           int iNextMonth     = common.getNextMonth(iCurMonth);
           String QString2   = " SELECT FUELKMS FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iNextMonth+" AND  Status=1 AND VehicleName = '"+SSelected+"' and Purpose='TO FILL FUEL' ";

           return QString2;

     }


     public String getStatus()
     {
           return SStatus;
     }

}
