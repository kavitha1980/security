package Reports.Vehicle;

import util.*;
import java.io.*;
import java.util.*;
public class FuelDocPrint1
{
      Vector VBody,VHead;
      String STitle,SFile;
      String RegNo,Name;
      int ipctr=0,ilctr=100,iLen=0;
      String SExpectedKm ,SActualKm ;

      FileWriter FW;

      Common common = new Common();

      public FuelDocPrint1(Vector VBody,Vector VHead,Vector VTotal,String STitle,String SFile,String RegNo,String Name,String SExpectedKm,String SActualKm)
      {
            this.VBody  = VBody;
            this.VHead  = VHead;
            this.STitle = STitle;
            this.SFile  = SFile;
            this.RegNo  = RegNo;
            this.Name   = Name;
            this.SExpectedKm =  SExpectedKm;
            this.SActualKm   =  SActualKm; 


            double DEk = Double.parseDouble(SExpectedKm);
            double DAk = Double.parseDouble(SActualKm);

            double VKm = DEk - DAk;


            if((SFile.trim()).length()==0)
               SFile = "FuelReport.prn";
            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(SFile);

                  int index=0;
                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl);
                        setLine();
                        if(index==(VBody.size()-1))
                        {
                              setFoot1();

                              String StrV = (String)VTotal.elementAt(1);
                              //String Strh = (String)VTotal.elementAt(0);
                              FW.write(StrV);

                        }
                        //setLine();
                        //ilctr++;
                        ilctr = ilctr + 2;

                        index=index+1;

                  }

                  String strl = common.Replicate("-",iLen)+"\n";


                  FW.write(strl);

                  FW.write("\n\n\n");

                  String SEKm         = "ExpectedKm       :\t\t" + SExpectedKm +"  Per Lit";

                  String SAKm         = "ActualKm         :\t\t" + common.getRound(SActualKm,2)+"  Per Lit";

                  String SVariation   = "Variation in Km  :\t\t" + common.getRound(String.valueOf(VKm),2)+"  Per Lit";

                  FW.write(SEKm+"\n\n");
                  FW.write(SAKm+"\n\n");
                  FW.write(SVariation+"\n\n");

                  setFoot();
                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }
      public void setHead()
      {
            if(ilctr < 45)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;
            String str1 = "\t\t\t Amarjothi Spinning Mills Ltd\n\n";
            String str2 = "\t\t "+STitle+"\n";
            String str4 = "Page     : "+ipctr+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";

            String str6 =  "VehicleRegNo:"+RegNo+ "\t\t\t" ;
            String str5 =  "VehicleName:"+Name+"\n\n";
            try
            {
                  FW.write(str1);
                  FW.write(str2);
                  FW.write(str6);
                  FW.write(str5);
                  FW.write(str4);
                  FW.write(str3);

                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = VHead.size()+5;
            }
            catch(Exception ex)
            {
               System.out.println(ex);
            }
      }
      public void setFoot()
      {
            try
            {
                  String SStatus = (new java.util.Date()).toString();

                  String str3 = common.Replicate("-",iLen)+"\n";
                  str3        = str3+"Report Taken on :"+SStatus+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
      public void setFoot1()
      {
            try
            {
                  String str3 = common.Replicate("-",iLen)+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
      public void setLine()
      {
          try
          {
               String s1 = "\n";
               FW.write(s1);
          }
          
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
}
