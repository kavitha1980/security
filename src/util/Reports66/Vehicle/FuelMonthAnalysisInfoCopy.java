package Reports.Vehicle;

import util.*;
import java.io.*;
import java.util.Vector;

public class FuelMonthAnalysisInfo
{

     FuelMonthAnalysisInfo()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
          Common common  = new Common();
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead        = (Vector)VPrint.elementAt(0);

          Vector VSNo              = (Vector)VPrint.elementAt(1);
          Vector VVehicleNo        = (Vector)VPrint.elementAt(2);
          Vector VVehicleName      = (Vector)VPrint.elementAt(3);
          Vector VApril            = (Vector)VPrint.elementAt(4);
          Vector VMay              = (Vector)VPrint.elementAt(5);
          Vector VJune             = (Vector)VPrint.elementAt(6);
          Vector VJuly             = (Vector)VPrint.elementAt(7);
          Vector VAugust           = (Vector)VPrint.elementAt(8);
          Vector VSeptember        = (Vector)VPrint.elementAt(9);
          Vector VOctober          = (Vector)VPrint.elementAt(10);
          Vector VNovember         = (Vector)VPrint.elementAt(11);
          Vector VDecember         = (Vector)VPrint.elementAt(12);
          Vector VJanuary          = (Vector)VPrint.elementAt(13);
          Vector VFebruary         = (Vector)VPrint.elementAt(14);
          Vector VMarch            = (Vector)VPrint.elementAt(15);
          Vector VInitialKm        = (Vector)VPrint.elementAt(16);
          Vector VClosingKm        = (Vector)VPrint.elementAt(17);
          Vector VRunningKm        = (Vector)VPrint.elementAt(18);
          Vector VDieselConsumed   = (Vector)VPrint.elementAt(19);
          Vector VAverageKm        = (Vector)VPrint.elementAt(20);
          Vector VStdKm            = (Vector)VPrint.elementAt(21);
          Vector VVariationKm      = (Vector)VPrint.elementAt(22);
          Vector VExcessDiesel     = (Vector)VPrint.elementAt(23);

          int m=0;
          for(int i=0;i<(VStKm.size())-1;i++)
          {
               if(m==0)
               {
                    out.println("       <tr>"); 
                    for(int j=0;j<VHead.size();j++)
                    {
                              out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+(String)VHead.elementAt(j)+"</td>");
                    }
                    out.println(" </tr>");
               }               
               m=1;

               out.println(" <tr>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSNo.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VVehicleNo.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VVehicleName.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VApril.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VMay.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VJune.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VJuly.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VAugust.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSeptember.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOctober.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VNovember.elementAt(i+1)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDecember.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VJanuary.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VFebruary.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VMarch.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInitialKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VClosingKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VRunningKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDieselConsumed.elementAt(i+1)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VAverageKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VStdKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VVariationKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VExcessDiesel.elementAt(i)+"</td>");
     
               out.println(" </tr>");
     
               out.println("</body>");
               out.println("</html>");
          }
     }

}
