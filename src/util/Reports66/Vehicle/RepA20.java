package Reports.Vehicle;
import java.io.*;
import java.sql.*;
import java.util.Vector;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.registry.*;
import java.rmi.*;
import blf.*;
import domain.jdbc.*;
import util.*;

public class RepA20 extends HttpServlet implements rndi.CodedNames
{
	HttpSession  session;
	Common		 common  =  new Common();


	Vector		 theVector;
	int			 iFMonth,iTMonth;
	int			 iSCode;
     Vector          VVehicleRegNo,VVehicleName,VInitialKm,VClosingKm,VRunningKm,VDieselConsumed,VAverageKm,VSTD,VVariationKm,VExcessDiesel;

    String      SFile;
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
	}
   	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");


        //SFile = "d:\\HRDReports\\HRDPrn\\Reports.Vehicle.RepA20.html";


        //PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(SFile)));
 
        //PrintWriter pw = response.getWriter();

		session          =  request.getSession(false);
		String SServer   =  (String)session.getValue("Server");
		iSCode			 =  common.toInt(request.getParameter("C1"));
		iFMonth			 =  common.toInt(request.getParameter("C2"));
		iTMonth			 =  common.toInt(request.getParameter("C3"));
          VVehicleRegNo        =  (Vector)session.getValue("VehicleCodes");
          VVehicleName         =  (Vector)session.getValue("VehicleNames");
		setDataIntoVector();

        SFile               = request.getParameter("FileName");
                
        if(SFile.equals(""))
          SFile = "d:\\HRDReports\\HRDPrn\\Reports.Vehicle.RepA20.html";


        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(SFile)));
        PrintWriter pw = response.getWriter();

	   toPrint(out);
        out.close();

        toPrint(pw);
        pw.close();

//        new HTMLToDraft(SFile);
	}
    private void toPrint(PrintWriter out)
    {

		toHead(out);
          //toBody(out);
          //toFoot(out);
		out.close();
	}
    private void toHead(PrintWriter out)
 	{
          int index  =  VVehicleRegNo.indexOf(String.valueOf(iSCode));
		if (index ==  -1)
		return;
		out.println("<html>");
		out.println("<body bgcolor = '"+common.bgColor+"'>");
        out.println("<table border = '1' width = '100%' cellspacing   =  '0'>");
        out.println("<!--TableTitle-->");
          out.println("<p align = 'center'><b> Fuel Consumption For the Period   from "+ common.getMonthName(iFMonth)+" - "+ common.getMonthName(iTMonth)+" A(20)</b></p>"); 
          out.println("<p align = 'center'><b> Vehicle Name :" + VVehicleName.elementAt(index) + "</b></p>"); 
        out.println("<!--/TableTitle-->");
        out.println("<!--TableHead-->");
		out.println("<tr bgColor  =  '"+common.bgHead+"'> ");
		out.println("    <td width = '12%' align = 'center' rowspan = '2'>Month </td>");
          out.println("    <td width = '12%' align = 'center' rowspan = '2'>Starting Km </td>");
          out.println("    <td width = '12%' align = 'center' rowspan = '2'>Closing Km</td>");
          out.println("    <td width = '14%' align = 'center' rowspan = '2'>Running Km</td>");
          out.println("    <td width = '14%' align = 'center' rowspan = '2'>Diesel Consumed</td>");
          out.println("    <td width = '14%' align = 'center' rowspan = '2'>Average per Km</td>");
          out.println("    <td width = '10%' align = 'center' rowspan = '2'>STD Km</td>");
          out.println("    <td width = '14%' align = 'center' rowspan = '2'>Variation in Km</td>");
          out.println("    <td width = '14%' align = 'center' rowspan = '2'>Excess Diesel Consumed</td>");
		out.println("<!--/TableHead-->");
		out.println("<!--TableBody-->");
	}
	private void setDataIntoVector()
	{
		theVector    =  new Vector();
          VInitialKm   =  new Vector();
          VClosingKm  =  new Vector();
          VRunningKm  =  new Vector();
          VDieselConsumed          =  new Vector();
          VAverageKm      =  new Vector();
          VSTD  =  new Vector();
          VVariationKm   =  new Vector();
          VExcessDiesel      =  new Vector();
/*        String SQry  = 
            " SELECT SchemeAppWage.SalaryMonth, SchemeAppWage.EmpCode, SchemeAppWage.WorkedDays,"+
            " SchemeAppWage.Wages, SchemeAppWage.Allowance, SchemeAppWage.PFAmount, SchemeAppWage.Advance,"+
            " SchemeAppWage.Food,SchemeAppWage.Card,SchemeAppWage.Others,SchemeAppWage.RoundOff  FROM (SchemeAppWage INNER JOIN SchemeApprentice ON "+
            " SchemeAppWage.EmpCode  =  SchemeApprentice.EmpCode) INNER JOIN "+
            " Scheme ON SchemeApprentice.SchemeCode  =  Scheme.SchemeCode"+
            " WHERE SchemeAppWage.SalaryMonth> = " + iFMonth +" and "+
            " SchemeAppWage.SalaryMonth< = " + iTMonth + " and "+
            " Scheme.SchemeCode = "+ iSCode +
            " union all "+
            " SELECT StaffWage.SalaryMonth,StaffWage.EmpCode,StaffWage.WorkedDays, StaffWage.Wages,"+
            " StaffWage.Allowance, StaffWage.PFAmount, StaffWage.Advance,"+
            " StaffWage.Food,StaffWage.Card,StaffWage.Others,StaffWage.RoundOff  FROM (StaffWage INNER JOIN Staff ON "+
            " StaffWage.EmpCode  =  Staff.EmpCode) INNER JOIN "+
            " Scheme ON Staff.SchemeCode  =  Scheme.SchemeCode"+
            " WHERE StaffWage.SalaryMonth> = " + iFMonth +" and "+
            " StaffWage.SalaryMonth< = " + iTMonth + " and "+
            " Scheme.SchemeCode = "+ iSCode ;

        try
        {
            JDBCConnection connect     =  JDBCConnection.getJDBCConnection();
            Connection theConnection  =  connect.getConnection();
            Statement theStatement    =  theConnection.createStatement();
            ResultSet theResult       =  theStatement.executeQuery(SQry);
            while(theResult.next())
            organizeData(theResult);
            theStatement.close();
        }
		catch(Exception ex)
		{
               System.out.println("Report A(20) : "+ex);
			   ex.printStackTrace();
          }        */
	}
	private void organizeData(ResultSet theResult) throws Exception
	{
		MonthClass monthclass = null;
		int    iSalMonth	 =  theResult.getInt(1);	
		int	   iEmpCode		 =  theResult.getInt(2);
		int	   iWorkDays	 =  theResult.getInt(3);	
		double dWages		 =  theResult.getDouble(4);
		double dAllowance	 =  theResult.getDouble(5);
		double dPfAmount	 =  theResult.getDouble(6);
		double dAdvance		 =  theResult.getDouble(7);
		double dFood		 =  theResult.getDouble(8);
		double dPunchCard	 =  theResult.getDouble(9);
		double dOthers		 =  theResult.getDouble(10);
		double dRound		 =  theResult.getDouble(11);
          VInitialKm  .addElement(String.valueOf(iEmpCode));
          VClosingKm .addElement(String.valueOf(iSalMonth));
          VRunningKm .addElement(String.valueOf(iWorkDays));
          VDieselConsumed        .addElement(String.valueOf(dWages));
          VAverageKm    .addElement(String.valueOf(dAllowance));
          VSTD .addElement(String.valueOf(dPfAmount));
          VVariationKm  .addElement(String.valueOf(dAdvance));
          VExcessDiesel     .addElement(String.valueOf(dPunchCard));
	}
/*    private void toBody(PrintWriter out)
	{
		for (int i = 0; i<theVector.size();i++)
		{
		MonthClass monthclass = (MonthClass)theVector.elementAt(i); 
		int  iMonth = monthclass.getMonth();
		out.println("<tr bgColor = '"+common.bgBody+"'>");
		out.println("<td>"+common.getMonthName(iMonth)+"</td>");
		out.println("<td>"+getNoofPersons(iMonth)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getNoofDays(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getStipendAmount(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getAllowance(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getTotal(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getPunchCardAmt(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getOthers(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getDedTotal(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getRound(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getNetStipendAmtPayable(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getFoodDed(iMonth),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getNetAmtPayable(iMonth),2)+"</td>");
		out.println("</tr>");
		}
	}
*/
/*    private int getNoofPersons(int iMonth)
	{
		int iPersons = 0;
          for(int i = 0; i<V(CLosing)SalMonth.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
			iPersons = iPersons+1;
			}
		}
		return iPersons;
	}
	private double getNoofDays(int iMonth)
	{
		int iNofdays = 0;
          for(int i = 0; i<(runningKm)VWorkDays.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				iNofdays = iNofdays+common.toInt((String)VWorkDays.elementAt(i));
			}
		}
		return common.toDouble(String.valueOf(iNofdays));
	}
	private double getStipendAmount(int iMonth)
	{
		double dStipendAmt = 0;
          for(int i = 0; i<(DieselConsumed)VWages.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dStipendAmt = dStipendAmt+common.toDouble((String)VWages.elementAt(i));
			}
		}
		return dStipendAmt;
	}
	private double getAllowance(int iMonth)
	{
		double dAllowance = 0;
          for(int i = 0; i<(averagekm)VAllowance.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dAllowance = dAllowance+common.toDouble((String)VAllowance.elementAt(i));
			}
		}
		return dAllowance;
	}
	private double getTotal(int iMonth)
	{
		double dTotal;
		dTotal = getAllowance(iMonth)+getStipendAmount(iMonth);
		return dTotal;
	}

	private double getPunchCardAmt(int iMonth)
	{
		double dPunch = 0;
          for(int i = 0; i<(Excess)VPunchCard.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dPunch = dPunch+common.toDouble((String)VPunchCard.elementAt(i));
			}
		}
		return dPunch;
	}
	private double getOthers(int iMonth)
	{
		double dOthers = 0;
		for(int i = 0; i<VOthers.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dOthers = dOthers+common.toDouble((String)VOthers.elementAt(i));
			}
		}
		return dOthers;
	}
	private double getDedTotal(int iMonth)
	{
		double dDedTotal;		
		dDedTotal = getPunchCardAmt(iMonth)+getOthers(iMonth);
		return dDedTotal;
	}
	private double getRound(int iMonth)
	{
		double dRound = 0;
		for(int i = 0; i<VRound.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dRound = dRound+common.toDouble((String)VRound.elementAt(i));
			}
		}
		return dRound;	
	}
	private double getNetStipendAmtPayable(int iMonth)
	{
		double dNetSPay;
		dNetSPay = getTotal(iMonth)+getRound(iMonth)-getDedTotal(iMonth);
		return dNetSPay;

    }
	private double getFoodDed(int iMonth)
	{
		double dFood = 0;
		for(int i = 0; i<VFood.size(); i++)
		{
			if((common.toInt((String)VSalMonth.elementAt(i))) ==  iMonth)
			{
				dFood = dFood+common.toDouble((String)VFood.elementAt(i));
			}
		}
		return dFood;
	}
	private double getNetAmtPayable(int iMonth)
	{
		double dNetPay;
		dNetPay = getNetStipendAmtPayable(iMonth)-getFoodDed(iMonth);
		return dNetPay;	
	}
    private void toFoot(PrintWriter out)
    {
        out.println("<!--/TableBody-->");
        out.println("<!--TableFoot-->");
		out.println("<tr bgcolor = '"+common.bgUom+"'>");
		out.println("<td>Total</td>");
		out.println("<td align = 'right'>"+getNoofPersonsTotal()+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getManDaysTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getStipendAmtTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getAllowanceTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getTotAmtTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getPunchTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getOthersTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getTotDedTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getRoundTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getNetStipendAmtPayableTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getFoodDedTotal(),2)+"</td>");
		out.println("<td align = 'right'>"+common.getRound(getNetAmountPayableTotal(),2)+"</td>");
		out.println("</tr>");
        out.println("<!--/TableFoot-->");
        out.println("</table>");   
        out.println("</body>");
        out.println("</html>");

	}
	private double getNoofPersonsTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();          
            dTotal +=  getNoofPersons(iMonth);
        }
		return dTotal;
	}
	private double getManDaysTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getNoofDays(iMonth);
        }
		return dTotal;
	}
	private double getStipendAmtTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getStipendAmount(iMonth);
        }
		return dTotal;
	}
	private double getAllowanceTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getAllowance(iMonth);
        }
		return dTotal;
	}
	private double getTotAmtTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getTotal(iMonth);
        }
		return dTotal;
	}
	private double getOthersTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getOthers(iMonth);
        }
		return dTotal;
	}
	private double getPunchTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getPunchCardAmt(iMonth);
        }
		return dTotal;
	}
	private double getTotDedTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getDedTotal(iMonth);
        }
		return dTotal;
	}
	private double getRoundTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal+= getRound(iMonth);
        }
		return dTotal;
	}
	private double getNetStipendAmtPayableTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal += getNetStipendAmtPayable(iMonth);
        }
		return dTotal;
	}
	private double getFoodDedTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal += getFoodDed(iMonth);
        }
		return dTotal;
	}
	private double 	getNetAmountPayableTotal()
	{
		double dTotal  =  0;
        for (int i = 0;i<theVector.size();i++)
        {
            MonthClass monthclass = (MonthClass)theVector.elementAt(i);
            int iMonth = monthclass.getMonth();
            dTotal += getNetAmtPayable(iMonth);
        }
		return dTotal;
	}
*/
}
