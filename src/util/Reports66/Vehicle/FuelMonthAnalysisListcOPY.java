package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class FuelMonthAnalysisList
{

     protected String SSelected;
     protected int iMonthCode1,iMonthCode2;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;
     int index=1;

     Vector VVehicleNo,VKqty,VDqty;
     Vector VSno,VVehicleName,VApril,VMay,VJune,VJuly,VAugust,VSeptember;
     Vector VOctober,VNovember,VDecember,VJanuary,VFebruary,VMarch,VInitialKm;
     Vector VClosingKm,VRunningKm,VDieselConsumed,VAverageKm,VStdKm;
     Vector VVariationKm,VExcessDiesel,VStKm,VMonthStKm,VMonthEndKm;
     Vector VHead;
     Common common = new Common();

     Vector VHead2      = new Vector(); 

     int iMonth[] = new int[12]; 

     FuelMonthAnalysisList(String SSelected,int iMonthCode1,int iMonthCode2,String SFile)
     {
            this.SSelected    = SSelected; 
            this.iMonthCode1  = iMonthCode1;
            this.iMonthCode2  = iMonthCode2;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setFuelList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setFuelList()
     {
            try
            {
                 int iNextMonth=iMonthCode1;
                 iMonth[0]      = iMonthCode1;
                 do
                 {
                    
                    iNextMonth = common.getNextMonth(iNextMonth);
                    iMonth[index]= iNextMonth;
                    index++;
                 }while(!(iNextMonth==iMonthCode2));

                 for(int i=0;i<index;i++)
                 {
                    System.out.println("Month:"+iMonth[i]);
                 }   

                 setDataIntoVector();
                 String STitle = " Fuel Consumption  List For The Month : "+common.getMonthName(iMonthCode1)+"And"+common.getMonthName(iMonthCode2)+"\n";
                 Vector VHead  = getVehicleHead();
                 iLen = ((String)VHead.elementAt(0)).length();
                 Strline = common.Replicate("-",iLen)+"\n";
                 Vector VBody  = getVehicleListBody();
                 //String SName      = (String)VName.elementAt(0);
                 //new Fuel(VBody,VHead,STitle,SFile,VTotal,SSelected,SName);
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }

      public Vector getVehicleHead()
      {
           Vector VStore = new Vector();
           //               {    1  ,      2     ,      3       ,  4    ,  5  ,  6   ,   7  ,    8   ,      9    ,    10   ,     11   ,     12   ,    13   ,    14    ,   15  ,     16     ,      17     ,       18    ,       19        ,     20     ,   21   ,         22      ,            23           };
           //String SHead[] = { "S.NO","VEHICLE.NO","VEHICLE NAME","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER","JANUARY","FEBRUARY","MARCH","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","AVERAGE/KM","STD KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };
           //String SHead[] = { "S.NO","VEHICLE.NO","VEHICLE NAME","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","AVERAGE/KM","STD KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };
           String SHead[] = { "S.NO","MONTH","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","AVERAGE/KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };


           String SMHead[]    = new String[index];
           String SMonths[]= new String[index];

           for(int i=0;i<SMonths.length;i++)
           {
               SMonths[i]= common.getMonthName(iMonth[i]);
           }    
           String SHead1  = ((String)SHead[0]).trim();
           String SHead2  = ((String)SHead[1]).trim();
           String SHead3  = ((String)SHead[2]).trim();
           String SHead4  = ((String)SHead[3]).trim();


           String SHead5  = ((String)SHead[4]).trim();
           String SHead6  = ((String)SHead[5]).trim();
           String SHead7  = ((String)SHead[6]).trim();
           String SHead8  = ((String)SHead[7]).trim();
           String SHead9  = ((String)SHead[8]).trim();
           String SHead10 = ((String)SHead[9]).trim();
           String SHead11 = ((String)SHead[10]).trim();
           /*String SHead12 = ((String)SHead[11]).trim();
           String SHead13 = ((String)SHead[12]).trim();
           String SHead14 = ((String)SHead[13]).trim();
           String SHead15 = common.parseNull((String)SHead[14]).trim();
           String SHead16 = common.parseNull((String)SHead[15]).trim();
           String SHead17 = common.parseNull((String)SHead[16]).trim();
           String SHead18 = common.parseNull((String)SHead[17]).trim();
           String SHead19 = common.parseNull((String)SHead[18]).trim();
           String SHead20 = common.parseNull((String)SHead[19]).trim();
           String SHead21 = common.parseNull((String)SHead[20]).trim();
           String SHead22 = common.parseNull((String)SHead[21]).trim();
           String SHead23 = common.parseNull((String)SHead[22]).trim();*/

           VHead  = new Vector();

           for(int i=0;i<SMonths.length;i++)
           {
               SMHead[i] = SMonths[i];
           }


           for(int i=0;i<SMonths.length;i++)
           {
               SMHead[i] = common.Pad(SMHead[i],5)+"�"+SInt;

           }

           /*VHead.addElement(SHead12);
           VHead.addElement(SHead13);
           VHead.addElement(SHead14);
           VHead.addElement(SHead15);
           VHead.addElement(SHead16);
           VHead.addElement(SHead17);
           VHead.addElement(SHead18);
           VHead.addElement(SHead19);
           VHead.addElement(SHead20);
           VHead.addElement(SHead21);
           VHead.addElement(SHead22);
           VHead.addElement(SHead23);*/

           SHead1   = common.Pad(SHead1,5)+"|"+SInt;
           SHead2   = common.Pad(SHead2,8)+"|"+SInt;
           SHead3   = common.Pad(SHead3,8)+"|"+SInt;
           SHead4   = common.Pad(SHead4,15)+"|"+SInt;
           SHead5   = common.Pad(SHead5,15)+"|"+SInt;
           SHead6   = common.Pad(SHead6,15)+"|"+SInt;
           SHead7   = common.Pad(SHead7,20)+"|"+SInt;
           SHead8   = common.Rad(SHead8,5)+common.Space(5)+"|"+SInt;
           SHead9   = common.Rad(SHead9,6)+common.Space(5)+"|"+SInt;
           SHead10  = common.Pad(SHead10,10)+"|"+SInt;
           SHead11  = common.Pad(SHead11,10)+"|"+SInt;
           

           VHead.addElement(SHead1);
           VHead.addElement(SHead2);
           VHead.addElement(SHead3);
           VHead.addElement(SHead4);
           VHead.addElement(SHead5);
           VHead.addElement(SHead6);
           VHead.addElement(SHead7);
           VHead.addElement(SHead8);
           VHead.addElement(SHead9);
           VHead.addElement(SHead10);
           VHead.addElement(SHead11);

           for(int i=0;i<SMonths.length;i++)
           {
               VHead.addElement(SMHead[i]); 

           }
           //String STHead = SHead1+SHead2+SHead3+SHead4+SHead5+SHead6+SHead7+SHead8+SHead9+SHead10+SHead11+SHead12+SHead13+SHead14+SHead15+SHead16+SHead17+SHead18+SHead19+SHead20+SHead21+SHead22+SHead23+"\n";
           String STHead = SHead1+SHead2+SHead3+SHead4+SHead5+SHead6+SHead7+SHead8+SHead9+SHead10+SHead11+"\n";
           for(int i=0;i<SMonths.length;i++)
           {
               STHead            = STHead+"+"+SMHead[i];  
           }

           System.out.println("Head:"+STHead);
           VStore.add(STHead);

           return VStore;

     }
     public Vector getVehicleListBody()
     {
          Vector vect = new Vector();
          return vect;
     }

     public void setDataIntoVector()
     {
          VVehicleNo     = new Vector();
          VKqty          = new Vector();
          VDqty          = new Vector();
          VStKm          = new Vector();
          VClosingKm     = new Vector();
          VMonthStKm     = new Vector();
          VInitialKm     = new Vector();
          VMonthEndKm    = new Vector();

           try
           {

                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                      Statement stat                   = conn.createStatement();
                      for(int i=0;i<index;i++)
                      {
                           ResultSet res                    = stat.executeQuery(getQString(iMonth[i]));
                           while(res.next())
                           {
                                String SVehicleNo = res.getString(1);
                                String SKQTY      = res.getString(2);
                                String SDQTY      = res.getString(3);

                                VVehicleNo.addElement(SVehicleNo);
                                VKqty.addElement(SKQTY);
                                VDqty.addElement(SDQTY);
                           }
                           res.close();
                           res = stat.executeQuery(getQString1(iMonth[i]));
                           while(res.next())
                           {
                                 if(i==0)
                                 {
                                   String SFuelKms = res.getString(1);
                                   VMonthStKm.addElement(SFuelKms);
                                 }

                           }
                           res.close();
                           res = stat.executeQuery(getQString2(iMonth[i]));
                           while(res.next())
                           {
                                  if(i==0)
                                  {
                                       String SClosingReading = res.getString(1);
                                       VMonthEndKm.addElement(SClosingReading);
                                  }
                           }
                           res.close();
                      }
           }
           catch(Exception e)
           {
               e.printStackTrace();
           }    

     }
     public Vector getPrintValues()
     {
           Vector VStore = new Vector();

           VStore.addElement(VSno);
           VStore.addElement(VVehicleNo);
           VStore.addElement(VVehicleName);

           return VStore;
     }
                                   

     public String getQString(int iMonthCode)
     {
           String QString    = " SELECT VEHICLENO,SUM(KQTY),SUM(DQTY) FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iMonthCode1+" AND Status=1 AND VehicleName = '"+SSelected+"' GROUP BY VEHICLENO ";

           return QString;
     }
     public String getQString1(int iCurMonth)
     {
           String QString1   = " SELECT FUELKMS FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iCurMonth+" AND Status=1 AND VehicleName = '"+SSelected+"' and Purpose='TO FILL FUEL' ";

           return QString1;

     }

     public String getQString2(int iCurMonth)
     {
           int iNextMonth     = common.getNextMonth(iCurMonth);
           String QString2   = " SELECT FUELKMS FROM VEHICLES "+
                               " WHERE SUBSTR(OUTDATE,1,6) = "+iNextMonth+" AND  Status=1 AND VehicleName = '"+SSelected+"' and Purpose='TO FILL FUEL' ";

           return QString2;

     }


     public String getStatus()
     {
           return SStatus;
     }

}
