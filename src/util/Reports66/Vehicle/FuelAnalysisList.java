package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class FuelAnalysisList
{

     protected String SSelected;
     protected int iMonthCode1,iMonthCode2;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String SStatus = "";
     String Strline = "";
     int iLen=0;
     int index=1;


     Vector VSno,VVehicleNo;
     Vector VHead,VHead2;
     Common common = new Common();

     FuelAnalysisList(String SSelected,int iMonthCode1,int iMonthCode2,String SFile)
     {
            this.SSelected    = SSelected; 
            this.iMonthCode1  = iMonthCode1;
            this.iMonthCode2  = iMonthCode2;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setFuelList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setFuelList()
     {
           try
           {
                 setDataIntoVector();
                 System.out.println("Month1: "+iMonthCode1);
                 System.out.println("Month2: "+iMonthCode2);
                 System.out.println("VName: "+SSelected);

                 String STitle = " Fuel Consumption  List For The Month : "+common.getMonthName(iMonthCode1)+"And"+common.getMonthName(iMonthCode2)+"\n";
                 Vector VHead  = getVehicleHead();
                 iLen          = ((String)VHead.elementAt(0)).length();
                 Strline       = common.Replicate("-",iLen)+"\n";
                 Vector VBody  = getVehicleListBody();
                 String SName  = (String)VVehicleNo.elementAt(0);
                 new FuelAnalysisDocPrint(VBody,VHead,STitle,SFile,SSelected);
                 System.out.println("VBody"+VBody.elementAt(0));
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }

      public Vector getVehicleHead()
      {
           Vector vect  = new Vector();

           String SHead[] = {"S.NO","VEHICLE.NO"};

           String SHead1  = ((String)SHead[0]).trim();
           String SHead2  = ((String)SHead[1]).trim();

           VHead2 = new Vector();

           VHead2.addElement(SHead1);
           VHead2.addElement(SHead2);

           SHead1   = common.Pad(SHead1,5)+"|"+SInt;
           SHead2   = common.Pad(SHead2,8)+"|"+SInt;

           String SAdd = SHead1+SHead2;

           vect.add(SAdd);
           return VHead2;
      }               

     public Vector getVehicleListBody()
     {
          Vector vect = new Vector();
          for(int i=0;i<VVehicleNo.size();i++)
          {
               String SVh1  = (String)VSno.elementAt(i);
               String SVh2  = (String)VVehicleNo.elementAt(i);

               SVh1    = common.Pad(SVh1,5)+"|"+SInt;
               SVh2    = common.Pad(SVh2,8)+"|";

               String SVLAdd = SVh1+SVh2+"\n";

               vect.add(SVLAdd);
          }
          return vect;
     }
     public void setDataIntoVector()
     {
           VSno          = new Vector();
           VVehicleNo    = new Vector();
           try
           {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection con = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat = con.createStatement();
               ResultSet res = stat.executeQuery(getQString());
               int sno=1;
               while(res.next())
               {
                    String SVehicleNo = res.getString(1);

                    VVehicleNo.addElement(SVehicleNo);
                    VSno.addElement(String.valueOf(sno));
                    sno=sno+1;
               }
           }
           catch(Exception e)
           {

           }
     }
     public Vector getPrintValues()
     {
           Vector vect = new Vector();
           vect.addElement(VHead2);
           vect.addElement(VSno);
           vect.addElement(VVehicleNo);
           return vect;
     }
                                   

     public String getQString()
     {
           String QString    = "Select VehicleNo from Vehicles "+
                               "where Substr(OutDate,1,6) >= "+iMonthCode1+" And Substr(OutDate,1,6) <= "+iMonthCode2+" and Status=1 AND  VehicleName = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 2 ";
           return QString;

     }
     public String getStatus()
     {
           return SStatus;
     }

}

