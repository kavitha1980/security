package Reports.Vehicle;

import util.*;
import java.io.*;
import java.util.Vector;

public class FuelAnalysisInfo
{

     FuelAnalysisInfo()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
          Common common  = new Common();
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead             = (Vector)VPrint.elementAt(0);

          Vector VSNo              = (Vector)VPrint.elementAt(1);
          Vector VMonthName        = (Vector)VPrint.elementAt(2);
          Vector VInitialKm        = (Vector)VPrint.elementAt(3);
          Vector VClosingKm        = (Vector)VPrint.elementAt(4);
          Vector VKmRunned         = (Vector)VPrint.elementAt(5);
          Vector VResultKero       = (Vector)VPrint.elementAt(6);
          Vector VResultDiesel     = (Vector)VPrint.elementAt(7);
          Vector VResultTotal      = (Vector)VPrint.elementAt(8);
          Vector VResultAvgKm      = (Vector)VPrint.elementAt(9);
          Vector VVariationKm      = (Vector)VPrint.elementAt(10);
          Vector VExcessDiesel     = (Vector)VPrint.elementAt(11);

          System.out.println(" SNo Size"+VSNo.size());
          int m=0;

          for(int i=0;i<VSNo.size();i++)
          {
               out.println(" <tr>");
               System.out.println("Inside print");
               
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSNo.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VMonthName.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInitialKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VClosingKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKmRunned.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VResultKero.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VResultDiesel.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VResultTotal.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VResultAvgKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VVariationKm.elementAt(i)+"</td>");
               out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VExcessDiesel.elementAt(i)+"</td>");
               out.println(" </tr>");

          }
          out.println("</body>");
          out.println("</html>");

          
     }

}
