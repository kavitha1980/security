package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class VehicleFuelRegisterList3
{

     protected String SSelected;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;
     int iFlag=0;
     int iMonthCode;
     double DAvg=0;
     Vector VSNo,VName,VOutDate,VOutTime,VInTime,VStKm,VEndKm,VTotalKm,VDriver,VSecurity,VDiNo,VKiNo,VDiDate,VKiDate,VDQty,VKQty,VBunk,VClosing,VAverage,VRemarks,VTank;
     Vector Vhead,VFuelKm;
     Common common = new Common();


     String SExpectedKm = "",SActualKm=" ";
     String SStatus = "";
     Vector VTotal;

     Vector VDTotal     = new Vector();
     Vector VKTotal     = new Vector();
     Vector VKmTotal    = new Vector();
     Vector VHead2      = new Vector(); 
     Vector VPName,VPDate,VPDriver,VPSecurity,VPDiNo,VPKiNo,VPDiDate,VPKiDate,VPDqty,VPKqty,VPBunk;
     Vector VClose,VOpen,VRunned,VDates,VTotalKQty,VTotalDQty,VPAverage;

     VehicleFuelRegisterList3(String SSelected,String SStDate,String SEnDate,String SFile)
     {
            this.SStDate      = SStDate;
            this.SSelected    = SSelected; 
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     VehicleFuelRegisterList3(String SSelected,int iMonthCode,String SFile,int iFlag)
     {
            this.SSelected    = SSelected; 
            this.iMonthCode   = iMonthCode;
            this.SFile        = SFile;
            this.iFlag        = iFlag;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }


     public void setInwardList()
     {
            try
            {

                 String STitle="";
                 VTotal     = new Vector();
                 setDataIntoVector();
                 fillEmpty();
                 if(iFlag==1)
                     STitle = " Fuel Consumption  List For The Month : "+common.getMonthName(iMonthCode)+" ";
                 else
                     STitle = " Fuel Consumption  List From "+common.parseDate(SStDate)+"  To "+common.parseDate(SEnDate)+" \n";
     
                 Vector VHead  = getVehicleHead();
                 getVehicleHTotal();
     
                 iLen              = ((String)VHead.elementAt(0)).length();
                 Strline           = common.Replicate("-",iLen)+"\n";
                 Vector VBody      = getVehicleListBody();
                 String SName      = (String)VName.elementAt(0);
                 SActualKm         = getActualKm();
                 setMixingQty();
                 new FuelDocPrint1(VBody,VHead,VTotal,STitle,SFile,SSelected,SName,SExpectedKm,SActualKm);
               
            }
            catch(Exception e)
            {
            }
      }
      public void getVehicleHTotal()
      {
           Vector vThead = new Vector();

           String Head1[]={"Total","  ","  " };
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();


           Sha1  = common.Pad(Sha1,30);
           Sha2  = common.Pad(Sha2,30)+SInt;
           Sha3  = common.Pad(Sha3,20)+SInt;

           String Strh2 = Sha1;
           VTotal.addElement(Strh2);
      }

      public Vector getVehicleHead()
      {
           Vector vect = new Vector();

           String SHead[]={"SNo","D.I.no","K.In.No","D.I.Date","K.I.Date","DriverName","Security","Dies.Qty","Kero.Qty","Init.Reading","Clos.Reading","kms Runned","AvgKms","TankFilled","Remarks"};

           String Head1[]={"SlNo",  " Fuel Inside " ," Fuel OutSide"  ,"  ","      "  , "Driver","Security"," "," " ,"Initial" ,"Closing", " Kms"    ,"AvgKms" ,"TankFilled","Remarks"};
           String Head2[]={" "   ,  " Indent", "Indent","Indent","Indent", " Name" ," Name   "," in  " ," in " ,"Reading" ,"Reading", " Runned" ,"  Per " ,"Fully/Part", "   "   };
           String Head3[]={" "   ,  "   No  ", " No "  ,"Date  "," Date" ,  "    " , "       "," Ltrs" ,"Ltrs ", "(kms)"  ,"(kms)  ", "       " ," Ltrs " ,"          ", "   "   };        


           String Sa1=((String)SHead[0]).trim();
           String Sa2=((String)SHead[1]).trim();
           String Sa3=((String)SHead[2]).trim();
           String Sa4=((String)SHead[3]).trim();
           String Sa5=((String)SHead[4]).trim();
           String Sa6=((String)SHead[5]).trim();
           String Sa7=common.parseNull((String)SHead[6]);
           String Sa8=common.parseNull((String)SHead[7]);
           String Sa9=((String)SHead[8]).trim();
           String Sa10=((String)SHead[9]).trim();
           String Sa11=((String)SHead[10]).trim();
           String Sa12=((String)SHead[11]).trim();
           String Sa13=((String)SHead[12]).trim();
           String Sa14=((String)SHead[13]).trim();
           String Sa15=((String)SHead[14]).trim();


           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();


           String Sha21=((String)Head2[0]).trim();
           String Sha22=((String)Head2[1]).trim();
           String Sha23=((String)Head2[2]).trim();
           String Sha24=((String)Head2[3]).trim();
           String Sha25=((String)Head2[4]).trim();
           String Sha26=((String)Head2[5]).trim();
           String Sha27=common.parseNull((String)Head2[6]);
           String Sha28=common.parseNull((String)Head2[7]);
           String Sha29=((String)Head2[8]).trim();
           String Sha210=((String)Head2[9]).trim();
           String Sha211=((String)Head2[10]).trim();
           String Sha212=((String)Head2[11]).trim();
           String Sha213=((String)Head2[12]).trim();
           String Sha214=((String)Head2[13]).trim();
           String Sha215=((String)Head2[14]).trim();

           String Sha31=((String)Head3[0]).trim();
           String Sha32=((String)Head3[1]).trim();
           String Sha33=((String)Head3[2]).trim();
           String Sha34=((String)Head3[3]).trim();
           String Sha35=((String)Head3[4]).trim();
           String Sha36=((String)Head3[5]).trim();
           String Sha37=common.parseNull((String)Head3[6]);
           String Sha38=common.parseNull((String)Head3[7]);
           String Sha39=((String)Head3[8]).trim();
           String Sha310=((String)Head3[9]).trim();
           String Sha311=((String)Head3[10]).trim();
           String Sha312=((String)Head3[11]).trim();
           String Sha313=((String)Head3[12]).trim();
           String Sha314=((String)Head3[13]).trim();
           String Sha315=((String)Head3[14]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);

           VHead2.addElement(Sa1);
           VHead2.addElement(Sa2);
           VHead2.addElement(Sa3);
           VHead2.addElement(Sa4);
           VHead2.addElement(Sa5);
           VHead2.addElement(Sa6);
           VHead2.addElement(Sa7);
           VHead2.addElement(Sa8);
           VHead2.addElement(Sa9);
           VHead2.addElement(Sa10);
           VHead2.addElement(Sa11);
           VHead2.addElement(Sa12);
           VHead2.addElement(Sa13);
           VHead2.addElement(Sa14);
           VHead2.addElement(Sa15);

           Sha1  = common.Pad(Sha1,5)+"|"+SInt;
           Sha2  = common.Pad(Sha2,13)+" "+SInt;
           Sha3  = common.Pad(Sha3,14)+" "+SInt;
           Sha4  = common.Pad(Sha4,15)+"|"+SInt;
           Sha5  = common.Pad(Sha5,15)+"|"+SInt;
           Sha6  = common.Pad(Sha6,15)+"|"+SInt;
           Sha7  = common.Pad(Sha7,20)+"|"+SInt;
           Sha8  = common.Rad(Sha8,0)+common.Space(5)+"|"+SInt;
           Sha9  = common.Rad(Sha9,0)+common.Space(5)+"|"+SInt;
           Sha10  = common.Pad(Sha10,10)+"|"+SInt;
           Sha11  = common.Pad(Sha11,10)+"|"+SInt;
           Sha12  = common.Pad(Sha12,5)+"|"+SInt;
           Sha13  = common.Pad(Sha13,10)+"|"+SInt;
           Sha14  = common.Pad(Sha14,8)+"|"+SInt;
           Sha15  = common.Pad(Sha15,15)+"|"+SInt;

           Sha21  = common.Pad(Sha21,5)+"|"+SInt;
           Sha22  = common.Pad(Sha22,8)+" "+SInt;
           Sha23  = common.Pad(Sha23,8)+" "+SInt;
           Sha24  = common.Pad(Sha24,15)+"|"+SInt;
           Sha25  = common.Pad(Sha25,15)+"|"+SInt;
           Sha26  = common.Pad(Sha26,15)+"|"+SInt;
           Sha27  = common.Pad(Sha27,20)+"|"+SInt;
           Sha28  = common.Rad(Sha28,5)+common.Space(5)+"|"+SInt;
           Sha29  = common.Rad(Sha29,6)+common.Space(5)+"|"+SInt;
           Sha210  = common.Pad(Sha210,10)+"|"+SInt;
           Sha211  = common.Pad(Sha211,10)+"|"+SInt;
           Sha212  = common.Pad(Sha212,5)+"|"+SInt;
           Sha213  = common.Pad(Sha213,10)+"|"+SInt;
           Sha214  = common.Pad(Sha214,8)+"|"+SInt;
           Sha215  = common.Pad(Sha215,15)+"|"+SInt;

           Sha31  = common.Pad(Sha31,5)+"|"+SInt;
           Sha32  = common.Pad(Sha32,8)+" "+SInt;
           Sha33  = common.Pad(Sha33,8)+" "+SInt;
           Sha34  = common.Pad(Sha34,15)+"|"+SInt;
           Sha35  = common.Pad(Sha35,15)+"|"+SInt;
           Sha36  = common.Pad(Sha36,15)+"|"+SInt;
           Sha37  = common.Pad(Sha37,20)+"|"+SInt;
           Sha38  = common.Rad(Sha38,5)+common.Space(5)+"|"+SInt;
           Sha39  = common.Rad(Sha39,6)+common.Space(5)+"|"+SInt;
           Sha310  = common.Pad(Sha310,10)+"|"+SInt;
           Sha311  = common.Pad(Sha311,10)+"|"+SInt;
           Sha312  = common.Pad(Sha312,5)+"|"+SInt;
           Sha313  = common.Pad(Sha313,10)+"|"+SInt;
           Sha314  = common.Pad(Sha314,8)+"|"+SInt;
           Sha315  = common.Pad(Sha315,15)+"|"+SInt;


           /*String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
           String Strh2 = Sha21+Sha22+Sha23+Sha24+Sha25+Sha26+Sha27+Sha28+Sha29+Sha210+Sha211+Sha212+Sha213+Sha214+Sha215+"\n";
           String Strh3 = Sha31+Sha32+Sha33+Sha34+Sha35+Sha36+Sha37+Sha38+Sha39+Sha310+Sha311+Sha312+Sha313+Sha314+Sha315+"\n";
           String Strh4 = Sa1+Sa2+Sa3+Sa4+Sa5+Sa6+Sa7+Sa8+Sa9+Sa10+Sa11+Sa12+Sa13+Sa14+Sa15+"\n";*/

           String Strh1 = Sha1+Sha5+Sha6+Sha7+Sha2+Sha8+Sha3+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
           String Strh2 = Sha21+Sha25+Sha26+Sha27+Sha22+Sha28+Sha23+Sha29+Sha210+Sha211+Sha212+Sha213+Sha214+Sha215+"\n";
           String Strh3 = Sha31+Sha35+Sha36+Sha37+Sha32+Sha38+Sha33+Sha39+Sha310+Sha311+Sha312+Sha313+Sha314+Sha315+"\n";
           String Strh4 = Sa1+Sa5+Sa6+Sa7+Sa2+Sa8+Sa3+Sa9+Sa10+Sa11+Sa12+Sa13+Sa14+Sa15+"\n";


           vect.add(Strh1);
           vect.add(Strh2);
           vect.add(Strh3);

           return vect;

     }
     public Vector getVehicleListBody()
     {
           Vector vect = new Vector();
           int iDTotal=0,iKTotal=0,iKmTotal=0;

           int index=0;

           String Sda11="",Sda17="",Sda18="",Sda19="",Sda20="",Sda21="";
           String Sda12="",Sda13="",Sda14="",Sda15="";

           for(int i=0;i<VDates.size();i++)
           
           {
                 String Sda1  = String.valueOf(i+1);
                 String Sda2  = (String)VPDiNo.elementAt(i);
                 String Sda3  = (String)VPKiNo.elementAt(i);
                 String Sda4  = (String)VPDiDate.elementAt(i);
                 String Sda5  = (String)VPKiDate.elementAt(i);
                 String Sda6  = (String)VPDriver.elementAt(i);
                 String Sda7  = (String)VPSecurity.elementAt(i);
                 String Sda8  = (String)VPDqty.elementAt(i);
                 String Sda9  = (String)VPKqty.elementAt(i);
                 String Sda10 = (String)VOpen.elementAt(i);
                 Sda11        = (String)VClose.elementAt(i);
                 Sda12        = (String)VRunned.elementAt(i);
                 Sda13        = (String)VPAverage.elementAt(i);
                 //Sda13        = "";
                 Sda14        = "";
                 Sda15        = (String)VRemarks.elementAt(i);

                 Sda1    = common.Pad(Sda1,5)+"|"+SInt;
                 Sda2    = common.Pad(Sda2,8)+"|"+SInt;
                 Sda3    = common.Pad(Sda3,8)+"|"+SInt;
                 Sda4    = common.Pad(Sda4,15)+"|"+SInt;
                 Sda5    = common.Pad(Sda5,15)+"|"+SInt;
                 Sda6    = common.Pad(Sda6,15)+"|"+SInt;
                 Sda7    = common.Pad(Sda7,20)+"|"+SInt;
                 Sda8    = common.Rad(Sda8,5)+common.Space(5)+"|"+SInt;
                 Sda9    = common.Rad(Sda9,6)+common.Space(5)+"|"+SInt;
                 Sda10   = common.Pad(Sda10,10)+"|"+SInt;
                 Sda11   = common.Pad(Sda11,10)+"|"+SInt;
                 Sda12   = common.Rad(Sda12,5)+"|"+SInt;
                 Sda13   = common.Pad(Sda13,10)+"|"+SInt;
                 Sda14   = common.Pad(Sda14,8)+"|"+SInt;
                 Sda15   = common.Pad(Sda15,15)+"|";

                 //String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";
                 String Strd = Sda1+Sda5+Sda6+Sda7+Sda2+Sda8+Sda3+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";
                 vect.add(Strd);

           }
     
           return vect;
               
     }
     private void fillEmpty()
     {

          VPName         = new Vector();
          VPDate         = new Vector();
          VPDriver       = new Vector();
          VPSecurity     = new Vector();
          VPDiNo         = new Vector();
          VPKiNo         = new Vector();
          VPDiDate       = new Vector();
          VPKiDate       = new Vector();
          VPDqty         = new Vector();
          VPKqty         = new Vector();
          VPBunk         = new Vector();
          VPAverage      = new Vector();
          int iPreIndex  = 0;
          
          try
          {


               for(int i=0;i<VDates.size();i++)
               {
                    int iDate1 = common.toInt((String)VDates.elementAt(i));
                    int iIndex   = VOutDate.indexOf(String.valueOf(iDate1));
                    

                    /*int iDate2 = common.toInt((String)VOutDate.elementAt(i));
                    int iIndex   = VDates.indexOf(String.valueOf(iDate2));
                    
                    if(iIndex==iPreIndex);
                         iIndex +=1;
                    */

                    if(iIndex!=-1)
                    {
                         //if(iIndex!=i)
                         //     iIndex=iIndex-1;

                         VSNo.addElement(String.valueOf(i+1));
                         insertValues(iIndex);
                         if(VOutDate.size()>(iIndex+1))
                         {
                              if(iDate1 == common.toInt((String)VOutDate.elementAt(iIndex+1)))
                              {
                                   VSNo.addElement(String.valueOf(i+1));
                                   insertValues(iIndex+1);
                              }
                         }
                    }
                    else
                    {

                         VSNo.addElement(String.valueOf(i+1));
                         VPDate.addElement(common.parseDate(String.valueOf(iDate1)));
                         VPName.addElement("---");
                         VPDriver.addElement("---");
                         VPSecurity.addElement("---");
                         VPKqty.addElement("---");
                         VPDqty.addElement("---");
                         VPKiDate.addElement("---");
                         VPDiDate.addElement("---");
                         VPBunk.addElement("");
                         VPDiNo.addElement("---");
                         VPKiNo.addElement("---");
                         VPAverage.addElement("--- ");
                         VRemarks.addElement("Fuel Not Filled");
                         VTank.addElement("---");
                         
                    }
               }
          }
          catch(Exception e)
          {
          }
     }

     public void insertValues(int iIndex)
     {

          VPDate.addElement(VOutDate.elementAt(iIndex));
          VPName.addElement(VName.elementAt(iIndex));
          VPDriver.addElement(VDriver.elementAt(iIndex));
          VPSecurity.addElement(VSecurity.elementAt(iIndex));
          VPKqty.addElement(VKQty.elementAt(iIndex));
          VPDqty.addElement(VDQty.elementAt(iIndex));
          VPDiNo.addElement(VDiNo.elementAt(iIndex));
          VPKiNo.addElement(VKiNo.elementAt(iIndex));
          VPKiDate.addElement(VKiDate.elementAt(iIndex));
          VPDiDate.addElement(VDiDate.elementAt(iIndex));
          VPBunk.addElement(VBunk.elementAt(iIndex));
          double DTot    = Double.parseDouble((String)VDQty.elementAt(iIndex))+Double.parseDouble((String)VKQty.elementAt(iIndex));
          DAvg    = Double.parseDouble((String)VRunned.elementAt(iIndex))/DTot;
          VPAverage.addElement(common.getRound(String.valueOf(DAvg),3));
          VRemarks.addElement("Fuel Filled");
          VTank.addElement("PARTLY");



     }

     public void setMixingQty()
     {
          try
          {




               String Sda20             = "";
               String Sda17             = (String)VTotalDQty.elementAt(0);
               String Sda18             = (String)VTotalKQty.elementAt(0);
               String Sda19             = (String)VTotalKm.elementAt(0);

               String Sda21             = SActualKm;


               double dDTotalLit        = Double.parseDouble((String)VTotalDQty.elementAt(0));
               double dKTotalLit        = Double.parseDouble((String)VTotalKQty.elementAt(0));
               double dTotalFuelLit     = dDTotalLit+dKTotalLit;

               double dDMix             = (dDTotalLit/dTotalFuelLit)*100;
               double dKMix             = (dKTotalLit/dTotalFuelLit)*100;

               String Sdmix             =  common.getRound(String.valueOf(dDMix),2);
               String Skmix             =  common.getRound(String.valueOf(dKMix),2);

               Sda20                    = common.Pad(Sda20,75)+"|"+SInt;
               Sda17                    = common.Rad(Sda17,5)+common.Space(5)+"|"+SInt;
               Sda18                    = common.Rad(Sda18,17)+common.Space(5)+"|"+SInt;
               Sda19                    = common.Rad(Sda19,31)+"|"+SInt;
               Sda21                    = common.Rad(Sda21,5)+common.Space(5)+"|"+SInt;  


               String SMix = common.Pad("Mixing % ",75)+"�"+SInt;
               String SDMix = common.Rad(Sdmix,5)+common.Space(5)+"|"+SInt;
               String SKMix = common.Rad(Skmix,17)+common.Space(5)+"|"+SInt;

               String Strd1= Sda20+Sda17+Sda18+Sda19+Sda21+"\n\n\n"+SMix+SDMix+SKMix+"\n";
               VTotal.add(Strd1);

          }
          catch(Exception e)
          {
          }
     }


     public void setDataIntoVector ()
     {
          VName         = new Vector();
          VOutDate      = new Vector();
          VOutTime      = new Vector();
          VInTime       = new Vector();
          VStKm         = new Vector();
          VEndKm        = new Vector();
          VTotalKm      = new Vector();
          VDriver       = new Vector();
          VSecurity     = new Vector();
          VDiNo         = new Vector();
          VKiNo         = new Vector();
          VDiDate       = new Vector();
          VKiDate       = new Vector();
          VDQty         = new Vector();
          VKQty         = new Vector();
          VBunk         = new Vector();
          VClosing      = new Vector();
          VTank         = new Vector();
          VAverage      = new Vector();
          VRemarks      = new Vector();
          VSNo          = new Vector();          
          VFuelKm       = new Vector();
          VClose        = new Vector();
          VOpen         = new Vector();
          VRunned       = new Vector();
          VDates        = new Vector(); 
          VTotalKQty    = new Vector();
          VTotalDQty    = new Vector();

          String STotalKm="",STempTotalKm   = "0";
          try
          {
               String SDate   = SEnDate;
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat                   = conn.createStatement();
               ResultSet res                    = stat.executeQuery(getQString());
               int inc=0;
               int sno=1;
               while(res.next())
               {
                    VDates.addElement(common.parseNull(res.getString(1)));
                    VOpen.addElement(common.parseNull(res.getString(2)));
                    VClose.addElement(common.parseNull(res.getString(3)));
                    VRunned.addElement(common.parseNull(res.getString(4)));
               }
               res                    = stat.executeQuery(getLastQuery());
               while(res.next())
               {
                    String SName          = res.getString(1);
                    String SOutDate       = (String)res.getString(2);
                    String SOutTime       = res.getString(3);
                    String SInTime        = res.getString(4);
                    
                    String SStKm          = res.getString(5);
                    String SEndKm         = res.getString(6);
                    
                    VStKm.addElement(SStKm);
                    VEndKm.addElement(SEndKm);
                    
                    VClosing.addElement(SStKm);
                    
                    String SDriver               = res.getString(7);
                    String SSecurity             = common.parseNull((String)res.getString(8));
                    String SDiNo                 = common.parseNull((String)res.getString(9));
                    String SKiNo                 = common.parseNull((String)res.getString(10));
                    String SDiDate               = common.parseDate(common.parseNull((String)res.getString(11)));
                    String SKiDate               = common.parseDate(common.parseNull((String)res.getString(12)));
                    String SDQty                 = common.parseNull((String)res.getString(13));
                    String SKQty                 = common.parseNull((String)res.getString(14));
                    String SBunk                 = common.parseNull((String)res.getString(15));     
                    String SFuelKm               = common.parseNull((String)res.getString(16)); 
                    
                    VName.addElement(SName);
                    VOutDate.addElement(SOutDate);
                    VOutTime.addElement(SOutTime);
                    VInTime.addElement(SInTime);
                    VDriver.addElement(SDriver);
                    VSecurity.addElement(SSecurity);
                    VDiNo.addElement(SDiNo);
                    VKiNo.addElement(SKiNo);
                    VDiDate.addElement(SDiDate);
                    VKiDate.addElement(SKiDate);
                    VDQty.addElement(SDQty);
                    VKQty.addElement(SKQty);
                    VBunk.addElement(SBunk);
                    VFuelKm.addElement(SFuelKm);
                    VAverage.addElement("");
                    sno=sno+1;
               }
               res.close();
               res                 = stat.executeQuery(getFuelQuery());
               while(res.next())
               {
                    VTotalKQty.addElement(res.getString(1));
                    VTotalDQty.addElement(res.getString(2));
               }
               res.close();
               res                 = stat.executeQuery(getExpectedKmQuery());
               while(res.next())
               {
                    SExpectedKm    = res.getString(1);
               }   
               res.close();
               res                 = stat.executeQuery(getTotalKmQuery());
               while(res.next())
               {
                    VTotalKm.addElement(res.getString(1));
               }

          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public Vector getPrintValues()
     {

           Vector vect = new Vector();

           vect.addElement(VHead2);
           vect.addElement(VSNo);
           vect.addElement(VPDiNo);
           vect.addElement(VPKiNo);
           vect.addElement(VPDiDate);
           vect.addElement(VPKiDate);
           vect.addElement(VPDriver);
           vect.addElement(VPSecurity);
           vect.addElement(VPDqty);
           vect.addElement(VPKqty);
           vect.addElement(VOpen);
           vect.addElement(VClose);
           vect.addElement(VRunned);
           vect.addElement(VPAverage);
           vect.addElement(VTank);
           vect.addElement(VRemarks);

           return vect;
     }
                                   
     public Vector getTotalValues()
     {
          Vector VTotalValues = new Vector();

          try
          {
               VTotalValues.addElement(VTotalDQty.elementAt(0));
               VTotalValues.addElement(VTotalKQty.elementAt(0));
               VTotalValues.addElement(VTotalKm.elementAt(0));
               VTotalValues.addElement(SActualKm);
               VTotalValues.addElement(SExpectedKm);
               double DVariation = Double.parseDouble(SExpectedKm)-Double.parseDouble(SActualKm);
               VTotalValues.addElement(String.valueOf(DVariation));
          }
          catch(Exception e)
          {
          }
          return VTotalValues;
     }

     private String getActualKm()
     {
          try
          {
               double DTKQty       = Double.parseDouble((String)VTotalKQty.elementAt(0));
               double DTDQty       = Double.parseDouble((String)VTotalDQty.elementAt(0));

               double DTFuelQty    = DTKQty + DTDQty;

               double DTotalKm     = Double.parseDouble((String)VTotalKm.elementAt(0));


               SActualKm           = String.valueOf(DTotalKm/DTFuelQty);

          }
          catch(Exception e)
          {
          }
          return SActualKm;
     }

     private String getQString()
     {
           String QString =    " select VDate ,skm,ekm ,ekm-skm  as runkm from "+
                               " (select distinct(OutDate) as VDate ,Min(StKm) as skm ,"+
                               " Max(EndKm)  as eKm from Vehicles ";

                    if(iFlag==1)
                         QString   = QString   +  " where  Substr(OutDate,1,6) = "+iMonthCode+"  and  ";
                    else
                         QString   = QString   +  " where  OutDate>='"+SStDate+"' and  outDate <='"+SEnDate+"' and ";

                         QString   = QString   +   " VehicleNo = '"+SSelected+"'  group by OutDate )  " ;


           return QString;
     }

     private String getLastQuery()
     {
           String Query       = "Select VehicleName,OutDate,OutTime,InTime,StKm,EndKm,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty,BunkName ,FuelKms from Vehicles ";

                  if(iFlag==1)
                      Query   = Query + " where  Substr(OutDate,1,6) = "+iMonthCode+"  And Status=1 AND  VehicleNo = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 2 " ;  
                  else
                      Query   = Query + " where OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' And Status=1 AND  VehicleNo = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 2 ";

           return Query;
     }
     private String getFuelQuery()
     {
          String SFQuery   = " Select Sum(KQTy),Sum(DQty) from Vehicles Where ";

                  if(iFlag==1)

                         SFQuery   = SFQuery + " Substr(OutDate,1,6) = "+iMonthCode+" ";
                  else
                         SFQuery   = SFQuery + " OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' ";
                         SFQuery   = SFQuery + " And Status=1 AND  VehicleNo = '"+SSelected+"' ";

          return SFQuery;
     }

     public String getTotalKmQuery()
     {
           String QString =    " Select Sum(runkm) from ("+
                               " select Sum(ekm-skm) as runkm,VDate from "+
                               " (select distinct(OutDate) as VDate ,Min(StKm) as skm ,"+
                               " Max(EndKm)  as eKm from Vehicles " ;
                  if(iFlag==1)
                         QString = QString + " where Substr(OutDate,1,6) = "+iMonthCode+" and " ;
                  else
                         QString = QString + " where  OutDate>='"+SStDate+"' and  outDate <='"+SEnDate+"' and " ;

                         QString = QString+  " VehicleNo = '"+SSelected+"' "+
                                             " group by OutDate) group by VDate ) ";

           return QString;
     }


     public String getExpectedKmQuery()
     {
           String SExpQuery    = "Select StdKmpl from VehicleInfo  where VehicleRegNo = '"+SSelected+"' " ;

           return SExpQuery;
     }

     public String getStatus()
     {
           return SStatus;
     }

}
