package Reports.Vehicle;


import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;
import util.*;

public class FuelMonthCrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     VehicleInfo vDomain;

     String SPatch="20050101";
     Vector VCode,VName;

     Common common = new Common();
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               vDomain             = (VehicleInfo)registry.lookup(SECURITYDOMAIN);

               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
               setMonthVectors();

     
               out.println("<html>");
               out.println("<body bgcolor='#9AA8D6'>");
               out.println("");
               out.println("<base target='main'>");
               out.println("<form method='GET' action='"+SServer+"Reports.Vehicle.FuelMonthRep'>");
     
               Vector VNo = vDomain.getVehicleData();
     
               out.println("<table>");
               out.println("<tr>");
               out.println("     <td>Month</td>");
               out.println("     <td>Vehicle</td>");
               out.println("     <td>File</td>");
               //out.println("    <td></td>");
               out.println("<tr>");
     
               out.println("<tr>");
               //out.println("     <td>");
               out.println("    <td  height='10'><font color='"+common.bgUom+"'>");

               out.println("<select size='1' name='C1'>");
               for(int i=0;i<VName.size();i++)
               {
                    String SName   = (String)VName.elementAt(i);
                    String SCode   = (String)VCode.elementAt(i);
                    out.println("<option Value='"+SCode+"'>"+SName+"</Option>");
               }                             
               out.println("</select></td>");
               out.println("<td>");
               out.println("<Select name='s1'>");
               int m=0;
               Vector VTemp = new Vector();
               for(int j=0;j<(VNo.size())/2;j++)
               {
                    VTemp.addElement(VNo.elementAt(1+m));
                    out.println("<option value="+j+">"+VTemp.elementAt(j)+"</option>");
                    m=m+2;
               }
               out.println("</select></td>");
               out.println("     <td><input type='text' name='TFile' size='15'></td>");
               out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
               out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
               out.println("<tr>");
     
               out.println("</table>");
     
               out.println("</form>");
               out.println("</body>");
               out.println("</html>");
     
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
	}

	private void setMonthVectors()
	{
		VCode = new Vector();
		VName = new Vector();
		String SYe1 = SPatch.substring(0,4);

          VName.addElement("Jan`"+SYe1);
          VName.addElement("Feb`"+SYe1);
          VName.addElement("Mar`"+SYe1);
		VName.addElement("Apr`"+SYe1);
		VName.addElement("May`"+SYe1);
		VName.addElement("Jun`"+SYe1);
		VName.addElement("Jul`"+SYe1);
		VName.addElement("Aug`"+SYe1);
		VName.addElement("Sep`"+SYe1);
		VName.addElement("Oct`"+SYe1);
		VName.addElement("Nov`"+SYe1);
		VName.addElement("Dec`"+SYe1);

          VCode.addElement(SYe1+"01");
          VCode.addElement(SYe1+"02");
          VCode.addElement(SYe1+"03");
		VCode.addElement(SYe1+"04");
		VCode.addElement(SYe1+"05");
		VCode.addElement(SYe1+"06");
		VCode.addElement(SYe1+"07");
		VCode.addElement(SYe1+"08");
		VCode.addElement(SYe1+"09");
		VCode.addElement(SYe1+"10");
		VCode.addElement(SYe1+"11");
		VCode.addElement(SYe1+"12");
     }
    public Vector getMonthNames()
    {
          return VName;
    }
    public Vector getMonthCodes()
    {
          return VCode;
    }
     
}
