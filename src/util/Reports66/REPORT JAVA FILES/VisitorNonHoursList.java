package Reports;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class VisitorNonHoursList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Object RowData[][];
     String ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","Delay Days"};
     String ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","N"};


     Vector VSNo,VDate,VCompany,VRep,VPurpose,VToMeet,VInTime,VOutTime;
     Vector Vhead;
     Common common = new Common();
     VisitorIn visitorDomain;

     String SStatus = "";


     VisitorNonHoursList(String StDate,String SEnDate,String SFile)
     {

            this.SStDate = StDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  visitorDomain         = (VisitorIn)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Visitor Non-Hours(other Days) List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getVisitorHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVisitorListBody();
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getVisitorHead()
      {
           Vector vect = new Vector();

           String Head1[]={" SNo","Date","CompanyName","RepresentativeName","Purpose Of Visit","Whom To Meet","InTime","OutTime" };        

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);


           Sha1  = common.Rad(Sha1,5)+common.Space(2);
           Sha2  = common.Pad(Sha2,11)+SInt;
           Sha3  = common.Pad(Sha3,40)+SInt;
           Sha4  = common.Pad(Sha4,25)+SInt;
           Sha5  = common.Pad(Sha5,30)+SInt;
           Sha6  = common.Pad(Sha6,30)+SInt;
           Sha7  = common.Pad(Sha7,13)+SInt;
           Sha8  = common.Pad(Sha8,13);

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVisitorListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDate.elementAt(i);
                 String Sda3  = (String)VCompany.elementAt(i);
                 String Sda4  = (String)VRep.elementAt(i);
                 String Sda5  = (String)VPurpose.elementAt(i);
                 String Sda6  = (String)VToMeet.elementAt(i);
                 String Sda7  = (String)VInTime.elementAt(i);
                 String Sda8  = (String)VOutTime.elementAt(i);


                 Sda1    = common.Rad(Sda1,5)+common.Space(2);
                 Sda2    = common.Pad(Sda2,11)+SInt;
                 Sda3    = common.Pad(Sda3,40)+SInt;
                 Sda4    = common.Pad(Sda4,25)+SInt;
                 Sda5    = common.Pad(Sda5,30)+SInt;
                 Sda6    = common.Pad(Sda6,30)+SInt;
                 Sda7    = common.Pad(Sda7,13)+SInt;
                 Sda8    = common.Pad(Sda8,13);
                 //Sda10 = common.Rad(Sda10,10);
                 


                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+"\n";
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

           VDate         = new Vector();
           VCompany      = new Vector();
           VRep          = new Vector();
           VPurpose      = new Vector();
           VToMeet       = new Vector();
           VInTime       = new Vector();
           VOutTime      = new Vector();
           VSNo          = new Vector();

           String STotalKm="";
           try
           {
                 int iStDate       = Integer.parseInt(SStDate);
                 int iDate         = Integer.parseInt(SEnDate);
                 Vector tVector    = visitorDomain.getVisitorReport(iStDate,iDate);
                 int size= (tVector.size())/9;
                 int m=0,s=0;
                 for(int i=0;i<size;i++)      
                 {
                         String SDate   = (String)tVector.elementAt(m+8);
                         int IDate      = Integer.parseInt(SDate);
                         String Sday    = common.getDayOfDate(IDate);
                         if(Sday.equals("Wed"))
                         {

                         }
                         else
                         {

                              VSNo.addElement(String.valueOf(s+1));
                              VCompany.addElement(tVector.elementAt(m+0));
                              VRep.addElement(tVector.elementAt(m+2));
                              VPurpose.addElement(tVector.elementAt(m+3));
                              VToMeet.addElement(tVector.elementAt(m+4));
                              VInTime.addElement(tVector.elementAt(m+6));
                              VOutTime.addElement(tVector.elementAt(m+7));
                              VDate.addElement(common.parseDate((String)tVector.elementAt(m+8)));
                              s=s+1;
                         }
                         m=m+9;

                 }


           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate);
           vect.addElement(VCompany);
           vect.addElement(VRep);
           vect.addElement(VPurpose);
           vect.addElement(VToMeet);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
           return vect;
     }
           

     public String getQString()
     {
           //String QString = "SELECT GateInward.GINo, GateInward.GIDate, Supplier.Name, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate "+
                    //" FROM (GateInward INNER JOIN Supplier ON GateInward.Sup_Code = Supplier.Ac_Code) INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                    // "Where GateInward.GIDate <= '"+SEnDate+"' ";
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty from Vehicles "+
                               " where OutDate <= '"+SEnDate+"' And Status=1  Order by 5 ";    

           return QString;                                                                     
     }


     public String getStatus()
     {
           return SStatus;
     }

}
