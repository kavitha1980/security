package Reports;

import java.util.*;
import java.io.*;
import java.sql.*;

public class FuelRegisterList
{

     protected String SSelected;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VSNo,VName,VOutDate,VOutTime,VInTime,VStKm,VEndKm,VTotalKm,VDriver,VSecurity,VDiNo,VKiNo,VDiDate,VKiDate,VDQty,VKQty,VBunk,VClosing,VAverage,VRemarks,VTank;
     Vector Vhead;
     Common common = new Common();

     String SStatus = "";
     Vector VTotal;

     Vector VDTotal     = new Vector();
     Vector VKTotal     = new Vector();
     Vector VKmTotal    = new Vector();


     FuelRegisterList(String SSelected,String SStDate,String SEnDate,String SFile)
     {
            this.SStDate      = SStDate;
            this.SSelected    = SSelected; 
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            VTotal     = new Vector();
            setDataIntoVector();
            String STitle = " Fuel Consumption  List From "+common.parseDate(SStDate)+"  To "+common.parseDate(SEnDate)+" \n";

            Vector VHead  = getVehicleHead();
            getVehicleHTotal();

            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVehicleListBody();
            String SName      = (String)VName.elementAt(0);
            new FuelDocPrint(VBody,VHead,STitle,SFile,VTotal,SSelected,SName);
      }
      public void getVehicleHTotal()
      {
           Vector vThead = new Vector();

           String Head1[]={"Total","  ","  " };
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();


           Sha1  = common.Pad(Sha1,30);
           Sha2  = common.Pad(Sha2,30)+SInt;
           Sha3  = common.Pad(Sha3,20)+SInt;

           String Strh2 = Sha1;
           VTotal.addElement(Strh2);
      }

      public Vector getVehicleHead()
      {
           Vector vect = new Vector();

           String Head1[]={"SlNo","DINo","KiNo","DIDate","KIDate","DriverName","SecurityName","DQty","KQty","InitialReading","ClosingReading","TotalKmsTravelled","Avg Kms/Litre","TankFull/Part","Remarks"};

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);



           Sha1  = common.Pad(Sha1,5);
           Sha2  = common.Pad(Sha2,8)+SInt;
           Sha3  = common.Pad(Sha3,8)+SInt;
           Sha4  = common.Pad(Sha4,15)+SInt;
           Sha5  = common.Pad(Sha5,15)+SInt;
           Sha6  = common.Pad(Sha6,15)+SInt;
           Sha7  = common.Pad(Sha7,20)+SInt;
           Sha8  = common.Rad(Sha8,5)+common.Space(5);
           Sha9  = common.Rad(Sha9,6)+common.Space(5);
           Sha10  = common.Pad(Sha10,10)+SInt;
           Sha11  = common.Pad(Sha11,10)+SInt;
           Sha12  = common.Pad(Sha12,15)+SInt;
           Sha13  = common.Pad(Sha13,10)+SInt;
           Sha14  = common.Pad(Sha14,8)+SInt;
           Sha15  = common.Pad(Sha15,15)+SInt;

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVehicleListBody()
     {
           Vector vect = new Vector();
           int iDTotal=0,iKTotal=0,iKmTotal=0;


           int index=0;

           String Sda17="",Sda18="",Sda19="",Sda20="",Sda21="";
           int iSize = (VOutDate.size()-2);


           for(int i=0;i<(VOutDate.size())-1;i++)
           {
           String Head1[]={"SlNo","DINo","KiNo","DIDate","KIDate","DriverName","SecurityName","DQty","KQty","InitialReading","ClosingReading","TotalKmsTravelled","Avg Kms/Litre","TankFull/Part","Remarks"};


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDiNo.elementAt(i);
                 String Sda3  = (String)VKiNo.elementAt(i);
                 String Sda4  = (String)VDiDate.elementAt(i);
                 String Sda5  = (String)VKiDate.elementAt(i);
                 String Sda6  = (String)VDriver.elementAt(i);
                 String Sda7  = (String)VSecurity.elementAt(i);
                 String Sda8  = (String)VDQty.elementAt(i);
                 String Sda9  = (String)VKQty.elementAt(i);
                 String Sda10 = (String)VStKm.elementAt(i);
                 String Sda11 = (String)VClosing.elementAt(i+1);
                 String Sda12 = (String)VTotalKm.elementAt(i);
                 String Sda13 = (String)VAverage.elementAt(i);
                 String Sda14 = (String)VTank.elementAt(i);
                 String Sda15 = (String)VRemarks.elementAt(i);

                 
                 String SDQty        = (String)VDQty.elementAt(i);
                 iDTotal             = iDTotal+Integer.parseInt(SDQty);
                 String SKQty        = (String)VKQty.elementAt(i);
                 iKTotal             = iKTotal+Integer.parseInt(SKQty);
                 String SKmTotal     = (String)VTotalKm.elementAt(i);
                 iKmTotal            = iKmTotal+Integer.parseInt(SKmTotal);


                 System.out.println("Index:"+index);
                 if(index==iSize)
                 {
              
                         VDTotal.addElement(String.valueOf(iDTotal));
                         VKTotal.addElement(String.valueOf(iKTotal));
                         //System.out.println("Element:"+VTotalKm.elementAt(VTotalKm.size()-1));
                         //VKmTotal.addElement(VTotalKm.elementAt(VTotalKm.size()-1));
                         VKmTotal.addElement(String.valueOf(iKmTotal));

                         Sda17          = (String)VDTotal.elementAt(0);
                         Sda18          = (String)VKTotal.elementAt(0);
                         Sda19          = (String)VKmTotal.elementAt(0);
                         Sda20          = " ";

                         double iFTotal    = common.toDouble(String.valueOf((Integer.parseInt(Sda17)+Integer.parseInt(Sda18))));
                         double iKm        = common.toDouble(String.valueOf(Integer.parseInt(Sda19)));

                         double iAvg       = iKm/iFTotal;
                         Sda21          = Double.toString(iAvg);
                 }


                 Sda1    = common.Pad(Sda1,5);
                 Sda2    = common.Pad(Sda2,8)+SInt;
                 Sda3    = common.Pad(Sda3,8)+SInt;
                 Sda4    = common.Pad(Sda4,15)+SInt;
                 Sda5    = common.Pad(Sda5,15)+SInt;
                 Sda6    = common.Pad(Sda6,15)+SInt;
                 Sda7    = common.Pad(Sda7,20)+SInt;
                 Sda8    = common.Rad(Sda8,5)+common.Space(5);
                 Sda9    = common.Rad(Sda9,6)+common.Space(5);
                 Sda10   = common.Pad(Sda10,10)+SInt;
                 Sda11   = common.Pad(Sda11,10)+SInt;
                 Sda12   = common.Rad(Sda12,15)+SInt;
                 Sda13   = common.Pad(Sda13,10)+SInt;
                 Sda14   = common.Pad(Sda14,8)+SInt;
                 Sda15   = common.Pad(Sda15,15);

                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";


                 vect.add(Strd);

                 if(index==iSize)
                 {
                         Sda20   = common.Pad(Sda20,96)+SInt;
                         Sda17   = common.Rad(Sda17,5)+common.Space(6);
                         Sda18   = common.Rad(Sda18,5)+common.Space(27);
                         Sda19   = common.Rad(Sda19,17)+common.Space(2);
                         Sda21   = common.Rad(Sda21,5)+common.Space(3);  
                                                                     
                         String Strd1= Sda20+Sda17+Sda18+Sda19+Sda21+"\n";
                         VTotal.add(Strd1);
                 }                            
                 
                 index=index+1;
                    

           }
           return vect;
               
     }

     public void setDataIntoVector ()
     {


           VName         = new Vector();
           VOutDate      = new Vector();
           VOutTime      = new Vector();
           VInTime       = new Vector();
           VStKm         = new Vector();
           VEndKm        = new Vector();
           VTotalKm      = new Vector();
           VDriver       = new Vector();
           VSecurity     = new Vector();
           VDiNo         = new Vector();
           VKiNo         = new Vector();
           VDiDate       = new Vector();
           VKiDate       = new Vector();
           VDQty         = new Vector();
           VKQty         = new Vector();
           VBunk         = new Vector();
           VClosing      = new Vector();
           VTank         = new Vector();
           VAverage      = new Vector();
           VRemarks      = new Vector();
           VSNo          = new Vector();          

           String STotalKm="";

           try
           {

                      String SDate   = SEnDate;
                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                      Statement stat                   = conn.createStatement();
                      ResultSet res                    = stat.executeQuery(getQString());
                      int inc=0;
                      int sno=1;
                      while(res.next())
                      {
                                 
                              String SName          = res.getString(1);
                              String SOutDate       = common.parseDate((String)res.getString(2));
                              String SOutTime       = res.getString(3);
                              String SInTime        = res.getString(4);
                              
                              String SStKm          = res.getString(5);
                              String SEndKm         = res.getString(6);
                              
                              VStKm.addElement(SStKm);
                              VEndKm.addElement(SEndKm);
                              
                              VClosing.addElement(SStKm);
                              
                              //String STotalKm       = common.parseNull(res.getString(9));
                              String SDriver        = res.getString(7);
                              String SSecurity      = common.parseNull((String)res.getString(8));
                              String SDiNo                 = common.parseNull((String)res.getString(9));
                              String SKiNo                 = common.parseNull((String)res.getString(10));
                              String SDiDate               = common.parseDate(common.parseNull((String)res.getString(11)));
                              String SKiDate               = common.parseDate(common.parseNull((String)res.getString(12)));
                              String SDQty                 = common.parseNull((String)res.getString(13));
                              String SKQty                 = common.parseNull((String)res.getString(14));
                              String SBunk                 = common.parseNull((String)res.getString(15));     
                              
                              VName.addElement(SName);
                              VOutDate.addElement(SOutDate);
                              VOutTime.addElement(SOutTime);
                              VInTime.addElement(SInTime);
                              VDriver.addElement(SDriver);
                              VSecurity.addElement(SSecurity);
                              VDiNo.addElement(SDiNo);
                              VKiNo.addElement(SKiNo);
                              VDiDate.addElement(SDiDate);
                              VKiDate.addElement(SKiDate);
                              VDQty.addElement(SDQty);
                              VKQty.addElement(SKQty);
                              VBunk.addElement(SBunk);
                              VSNo.addElement(String.valueOf(sno));
                              sno=sno+1;
                              VTank.addElement("PARTLY");
                              VRemarks.addElement(" ");

                      }
                      int VSize= (VClosing.size())-1;
                      for(int index=0;index<VSize;index++)
                      {
                              int iskm              = Integer.parseInt((String)VStKm.elementAt(index));
                              int iendkm            = Integer.parseInt((String)VClosing.elementAt(index+1));
                              int itot              = iendkm-iskm;

                              if(itot<0)
                                  STotalKm   = "0";   
                              else
                                  STotalKm       = Integer.toString(itot);


                              int iDQty             = Integer.parseInt(common.parseNull((String)VDQty.elementAt(index)));
                              int iKQty             = Integer.parseInt(common.parseNull((String)VKQty.elementAt(index)));

                              int iFuel             = iDQty+iKQty;
                              double sAvg           = common.toDouble(String.valueOf(itot))/iFuel;

                              String iAvg           = common.getRound(sAvg,3);

                              VAverage.addElement(iAvg);

                              VTotalKm.addElement(STotalKm);

                      }

                      int iDiff=0,itot=0;
           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }
     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDiNo);
           vect.addElement(VKiNo);
           vect.addElement(VDiDate);
           vect.addElement(VKiDate);
           vect.addElement(VDriver);
           vect.addElement(VSecurity);
           vect.addElement(VDQty);
           vect.addElement(VKQty);
           vect.addElement(VStKm);
           vect.addElement(VEndKm);
           vect.addElement(VTotalKm);
           vect.addElement(VAverage);
           vect.addElement(VTank);
           vect.addElement(VRemarks);

           return vect;
     }
                                   

     public String getQString()
     {
           String QString    = "Select VehicleName,OutDate,OutTime,InTime,StKm,EndKm,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty,BunkName from Vehicles "+
                               " where OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' And Status=1 AND  VehicleNo = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' ";    

           return QString;
     }

     public String getStatus()
     {
           return SStatus;
     }

}
