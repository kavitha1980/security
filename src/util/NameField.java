package util;

import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;
import java.awt.event.*;

public class NameField extends JTextField {

	int iColumns = 30;

	public NameField()
	{
		super();
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));   
          addFocusListener(new FocusList());
	}

     public NameField(String value, int columns)
     {
          super(columns);
		iColumns = columns;
          setText(value);
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));   
          addFocusListener(new FocusList());
     }
     public NameField(int columns)
     {
          super(columns);
          iColumns = columns;
          setBackground(new Color(255,217,179)); 
          setFont(new Font("",Font.BOLD,10));   
          addFocusListener(new FocusList());
     }

     protected Document createDefaultModel()
     {
         return new WholeNumberDocument();
     }

     protected class WholeNumberDocument extends PlainDocument
     {
          public void insertString(int offs, String str, AttributeSet a)  throws BadLocationException
          {
               if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

               char[] source = str.toCharArray();
               char[] result = new char[source.length];
               int j = 0;

                for (int i = 0; i < result.length; i++)
                {
                     if (Character.isLetter(source[i]) || source[i] == '.' || source[i] == ' ' || source[i] == '-' ||  source[i] == '(' || source[i]== ')')
                     {
                         result[j++] = source[i];
                     }
                     else
                     {
                         Toolkit.getDefaultToolkit().beep();
                     }
                 }
          super.insertString(offs, (new String(result, 0, j)).toUpperCase(), a);
          }
     }
    private class FocusList extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            setCaretPosition(0);
            moveCaretPosition(getText().length());
        }
    }

}
