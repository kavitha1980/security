package util;

import javax.swing.*;
import javax.swing.text.*; 
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.awt.*;
import java.awt.event.*;

public class WholeNumberField extends JTextField {

    private Toolkit toolkit;
    private NumberFormat integerFormatter;
	int iColumns = 35;

	public WholeNumberField()
	{
		super();
          toolkit = Toolkit.getDefaultToolkit();
          setBackground(new Color(255,217,179));
          setFont(new Font("",Font.BOLD,10));   
          addFocusListener(new FocusList());

	}

    public WholeNumberField(int value, int columns) {
        super(columns);
		iColumns = columns;
        toolkit = Toolkit.getDefaultToolkit();
        integerFormatter = NumberFormat.getNumberInstance(Locale.US);
        integerFormatter.setParseIntegerOnly(true);
        setValue(value);
        setBackground(new Color(255,217,179));
        setFont(new Font("",Font.BOLD,10));   
        addFocusListener(new FocusList());

    }

    public WholeNumberField(int columns) {
        super(columns);
		iColumns = columns;
        toolkit = Toolkit.getDefaultToolkit();
        integerFormatter = NumberFormat.getNumberInstance(Locale.US);
        integerFormatter.setParseIntegerOnly(true);
        setBackground(new Color(255,217,179));
        setFont(new Font("",Font.BOLD,10));   
        addFocusListener(new FocusList());

    }


    public int getValue() {
        int retVal = 0;
        try {
            retVal = integerFormatter.parse(getText()).intValue();
        } catch (Exception e) {
            toolkit.beep();
        }
        return retVal;
    }

    public void setValue(int value) {
        setText(integerFormatter.format(value));
    }

    protected Document createDefaultModel() {
        return new WholeNumberDocument();
    }

    protected class WholeNumberDocument extends PlainDocument {

        public void insertString(int offs, String str, AttributeSet a) 
            throws BadLocationException {

			if (str == null || getText(0, getLength()).length() == iColumns)
			{
				return;
			}

            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {
                if (Character.isDigit(source[i]))
                    result[j++] = source[i];
                else {
                    toolkit.beep();
                }
            }
            super.insertString(offs, new String(result, 0, j), a);
        }
    }
    private class FocusList extends FocusAdapter
    {
        public void focusGained(FocusEvent fe)
        {
            setCaretPosition(0);
            moveCaretPosition(getText().length());
        }
    }
}
