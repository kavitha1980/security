package Reports.Vehicle2;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;


public class RentVehicleRegisterRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     RentVehicleInfo VDomain;
     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VDomain             = (RentVehicleInfo)registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
     
               String SStDate   = common.pureDate(request.getParameter("TStDate"));
               String SEnDate   = common.pureDate(request.getParameter("TEnDate"));
               String SFile     = common.parseNull(request.getParameter("TFile"));
     
               SFile = (SFile.trim()).length()==0?"d:/RentVehicleRegister.prn":SFile;
     
               RentVehicleInformation info  = new RentVehicleInformation();
     
               RentVehicleRegisterList theList    = new RentVehicleRegisterList(SStDate,SEnDate,SFile);
               Vector vect            = new Vector();  
               vect                   = theList.getPrintValues();
     
               Vector VInfo  = new Vector();
               Vector VValue = new Vector();
     
               VInfo.addElement("Report Name");
               VInfo.addElement("Report Code");
               VInfo.addElement("Report url/File Name");
               VInfo.addElement("Status");
     
               VValue.addElement("Rental Vehicle Movement  List as on a Particular Date");
               VValue.addElement("(e)");
               VValue.addElement(SFile);
               VValue.addElement(theList.getStatus());
     
     
               info.flashMessage(VInfo,VValue,vect,out);
     
               
                                  
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }

}
