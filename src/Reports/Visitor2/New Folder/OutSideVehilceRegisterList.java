//   � - alt+179,� - Alt+196;
     
package domain.servlet.Visitor;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;
//import java.rmi.*;
//import java.rmi.registry.*;
//import blf.*;
import domain.jdbc.*;

public class OutSideVehilceRegisterList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;
     Connection theConnection;

     Vector VSNo,VDate,VCompany,VRep,VPurpose,VToMeet,VInTime,VOutTime,VSpentTime,VVehicleType,VRegNo,VDriver;
     Vector Vhead,VSlipNo,VLine;
     Common common = new Common();
     VisitorIn visitorDomain;

     String SStatus = "",SRemarks="";


     public OutSideVehilceRegisterList(String StDate,String SEnDate,String SFile)
     {

            this.SStDate = StDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                //  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               //   visitorDomain         = (VisitorIn)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Visitor Register List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getVisitorHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVisitorListBody();
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getVisitorHead()
      {
           Vector vect = new Vector();

           String Head1[]={" SNo","Date","VehicleType","DriverName","RegNo","RepresentativeName","Purpose Of Visit","InTime","OutTime","SpentTime"};        

           String Sha1  = ((String)Head1[0]).trim();
           String Sha2  = ((String)Head1[1]).trim();
           String Sha3  = ((String)Head1[2]).trim();
           String Sha4  = ((String)Head1[3]).trim();
           String Sha5  = ((String)Head1[4]).trim();
           String Sha6  = ((String)Head1[5]).trim();
           String Sha7  = common.parseNull((String)Head1[6]);
           String Sha8  = common.parseNull((String)Head1[7]);
           String Sha9  = common.parseNull((String)Head1[8]);
           String Sha10 = common.parseNull((String)Head1[9]);


           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);


           Sha1  = common.Rad(Sha1,5)+common.Space(2)+"�"+SInt;
           Sha2  = common.Pad(Sha2,11)+"�"+SInt;
           Sha3  = common.Pad(Sha3,20)+"�"+SInt;
           Sha4  = common.Pad(Sha4,20)+"�"+SInt;
           Sha5  = common.Pad(Sha5,20)+"�"+SInt;
           Sha6  = common.Pad(Sha6,25)+"�"+SInt;
           Sha7  = common.Pad(Sha7,20)+"�"+SInt;
           Sha8  = common.Pad(Sha8,13)+"�"+SInt;
           Sha9  = common.Pad(Sha9,13)+"�"+SInt;
           Sha10 = common.Pad(Sha10,13)+"�";

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVisitorListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDate.elementAt(i);
                 String Sda3  = (String)VVehicleType.elementAt(i);
                 String Sda4  = (String)VDriver.elementAt(i);
                 String Sda5  = (String)VRep.elementAt(i);
                 String Sda6  = (String)VPurpose.elementAt(i);
                 String Sda7  = (String)VToMeet.elementAt(i);
                 String Sda8  = (String)VInTime.elementAt(i);
                 String Sda9  = (String)VOutTime.elementAt(i);
                 String Sda10  = (String)VSpentTime.elementAt(i);

                 Sda1    = common.Rad(Sda1,5)+common.Space(2)+"�"+SInt;
                 Sda2    = common.Pad(Sda2,11)+"�"+SInt;
                 Sda3    = common.Pad(Sda3,20)+"�"+SInt;
                 Sda4    = common.Pad(Sda4,20)+"�"+SInt;
                 Sda5    = common.Pad(Sda5,20)+"�"+SInt;
                 Sda6    = common.Pad(Sda6,25)+"�"+SInt;
                 Sda7    = common.Pad(Sda7,20)+"�"+SInt;
                 Sda8    = common.Pad(Sda8,13)+"�"+SInt;
                 Sda9    = common.Pad(Sda9,13)+"�"+SInt;
                 Sda10   = common.Pad(Sda10,13)+"�";
                 
                 String Sda11    = common.Rad("",5)+common.Space(2)+"�"+SInt;
                 String Sda12    = common.Pad("",11)+"�"+SInt;
                 String Sda13    = common.Pad("",20)+"�"+SInt;
                 String Sda14    = common.Pad("",20)+"�"+SInt;
                 String Sda15    = common.Pad("",20)+"�"+SInt;
                 String Sda16    = common.Pad("",25)+"�"+SInt;
                 String Sda17    = common.Pad("",20)+"�"+SInt;
                 String Sda18    = common.Pad("",13)+"�"+SInt;
                 String Sda19    = common.Pad("",13)+"�"+SInt;
                 String Sda20    = common.Pad("",13)+"�";


                 String Strd  = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+"\n";
                 String Strd1 = Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17+Sda18+Sda19+Sda20+"\n";

                 String SLCheck = common.parseNull((String)VSNo.elementAt(i));
                 String SLine="";

                 if(SLCheck.equals(""))
                 {
                    Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+"\n"+Strd1;
                    
                 }
                 else
                 {
                   SLine = common.Replicate("�",Strd.length());

                   if(i==0)
                   {
                          SLine="";
                   }
                          Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+"\n"+Strd1 ;
                 }

                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

          VSlipNo       = new Vector();
          VDate         = new Vector();
          VCompany      = new Vector();
          VDriver       = new Vector();
          VRegNo        = new Vector();
          VVehicleType  = new Vector();
          VRep          = new Vector();
          VPurpose      = new Vector();
          VToMeet       = new Vector();
          VInTime       = new Vector();
          VOutTime      = new Vector();
          VSNo          = new Vector();
          VSpentTime    = new Vector();
          VLine         = new Vector();
          int i=1;
          try
          {
               int iStDate            = Integer.parseInt(SStDate);
               int iDate              = Integer.parseInt(SEnDate);
               JDBCConnection jdbc    = JDBCConnection.getJDBCConnection();
               theConnection          = jdbc.getConnection();
               Statement    theStmt   = theConnection.createStatement();
               ResultSet theResult    = theStmt.executeQuery(getQS());
               while(theResult.next())
               {

                    VSNo.addElement(String.valueOf(i));
                    String SDate   = (String)theResult.getString(1);
                    VDate.addElement(common.parseDate(SDate));
                    VDriver.addElement(theResult.getString(2));
                    VVehicleType.addElement(theResult.getString(3));
                    VRegNo.addElement(theResult.getString(4));
                    VRep.addElement(theResult.getString(5));
                    String SInTime      = (String)theResult.getString(6);
                    String SOutTime     = (String)theResult.getString(7);
                    VInTime.addElement(SInTime);
                    VOutTime.addElement(SOutTime);
                    String STimeDiff = common.getTimeDiff(common.toInt(SDate),common.toInt(SDate),SOutTime,SInTime);
                    VSpentTime.addElement(STimeDiff);
                    VPurpose.addElement(theResult.getString(8));
                    i = i+1;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public String getQS()
     {
          String QS = " Select VisitorDate,OtherVehicleDriver.Name,OthervehicleType.Name,"+
                      " OtherVehicleInfo.RegNo,Representative.Name,Visitor.inTime, "+
                      " Visitor.OutTime,VisitorPurpose.Name from "+
                      " Visitor inner join Representative on Representative.Code=Visitor.RepCode"+
                      " inner join OtherVehicleDriver.Code = Visitor.DriverCode "+
                      " inner join OtherVehicleType.Code = Visitor.VehicleTypeCode"+
                      " inner join OtherVehicleInfo.Code = Visitor.RegCode "+
                      " inner join VisitorPurpose.Code   = Visitor.PurposeCode "+
                      " where Visitor.Out=1 and Visitor.VisitorDate>="+SStDate+" and "+
                      " Visitor.VisitorDate<="+SEnDate+"  and Out=1 and OtherVehicleType.Code >0 ";
          return QS;
     }

     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate);
           vect.addElement(VDriver);
           vect.addElement(VVehicleType);
           vect.addElement(VRegNo);
           vect.addElement(VRep);
           vect.addElement(VPurpose);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
         //  vect.addElement(VSpentTime);

           return vect;
     }

     public String getStatus()
     {
           return SStatus;
     }

}

