

/*
     The Main Servlet Starter.
*/

package Reports;

import util.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class Login1 extends HttpServlet
{
     HttpSession    session;
     Common         common;
     Vector         VSCode,VSName,VSPass;
     String         SServer;
     String         bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common  = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;

          setInventoryYears();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          try
          {

          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          session          = request.getSession(true);

          session.putValue("Server","http://erp4:2004/servlet/");
          SServer = "http://erp4:2004/servlet/";                  
            
          setInventoryYears();
          out.println("<html><head><title>AMARJOTHI SPINNING MILLS LIMITED</title></head><body bgcolor='"+bgColor+"' text='#003E48'>");
          out.println("<form method='POST' action='"+SServer+"Reports.Login'><div align='center'>");
          out.println("  <table border='0' width='876' height='37' cellspacing='0' cellpadding='0'>");
          out.println("    <tr>");
          out.println("      <td width='95' height='37'>");
          out.println("      <td width='459' height='37'>");
          out.println("      <p align='center'><font color='000000' size='6'><b>SECURITY MONITORING SYSTEM</b></font></p></td>");
          out.println("     </tr>");
          out.println("     <tr>");
          out.println("     <td>");
          out.println("     <p align='center'><font color= '000000' size='4'>Amarjothi Spinning Mills Ltd</font></p></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println("<p align='center'><b><font face='Chevara' size='4' color='"+fgBody+"'>LOGIN SCREEN</font></b></p><p align='center'>&nbsp;</p>");
          out.println("<div align='center'>");
          out.println("<center><table border='0' width='41%'>");
          out.println("<tr>");
          out.println("<td width='42%' bgcolor='"+bgHead+"'><font face='Book Antiqua' size='4' color='"+fgHead+"'><b>User ID</b></font></td>");
          out.println("<td width='64%'>");
          out.println("<select size='1' name='DSNId' style='font-family: Book Antiqua; font-size: 12pt; font-weight: bold'>");

          for(int i=0;i<VSName.size();i++)
          {
               String SName   = (String)VSName.elementAt(i);
               out.println("<option Value='"+i+"'>"+SName+"</Option>");
          }
          out.println("</select></td>");
          out.println("</tr>");
          out.println("</table></center></div><p align='center'>");
          out.println("<input type='submit' value='Submit' name='B1' style='font-family: Book Antiqua; font-size: 10pt; color: "+fgBody+"; text-transform: uppercase; font-weight: bold; background-color: "+bgColor+"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("<input type='reset' value='Reset' name='B2' style='font-family: Book Antiqua; font-size: 10pt; color: "+fgBody+"; text-transform: uppercase; font-weight: bold; background-color: "+bgColor+"'></p></form><p align='center'>&nbsp;</p></body></html>");
          out.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }


     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          
          try
          {
          response.setContentType("text/html");

          session       = request.getSession(false);
          SServer = (String)session.getValue("Server");
          response.sendRedirect(SServer+"Reports.SecurityMainFrame");
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }

     private void setInventoryYears()
     {
          VSName  = new Vector();
          VSName.addElement("2004-2005");
          VSName.addElement("2003-2004");
          VSName.addElement("2002-2003");
     }

}
