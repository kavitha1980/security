package Reports.Errector;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class ErrectorRegisterAsList1 implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VSNo,VDate,VCompany,VRep,VPurpose,VToMeet,VInTime,VOutTime;
     Vector Vhead;
     Common common = new Common();
     VisitorIn visitorDomain;

     String SStatus = "";


     ErrectorRegisterAsList1(String StDate,String SEnDate,String SFile)
     {

            this.SStDate = StDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  visitorDomain         = (VisitorIn)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Errector Register List As  "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getVisitorHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVisitorListBody();
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getVisitorHead()
      {
           Vector vect = new Vector();

           String Head1[]={" SNo","Date","RepresentativeName","CompanyName","Purpose Of Visit","InTime","OutTime" };        

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=common.parseNull((String)Head1[5]);
           String Sha7=common.parseNull((String)Head1[6]);

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);


           Sha1  = common.Rad(Sha1,5)+common.Space(2)+"�"+SInt;
           Sha2  = common.Pad(Sha2,11)+"�"+SInt;
           Sha3  = common.Pad(Sha3,25)+"�"+SInt;
           Sha4  = common.Pad(Sha4,40)+"�"+SInt;
           Sha5  = common.Pad(Sha5,30)+"�"+SInt;
           Sha6  = common.Pad(Sha6,13)+"�"+SInt;
           Sha7  = common.Pad(Sha7,13)+"�";

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVisitorListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDate.elementAt(i);
                 String Sda3  = (String)VCompany.elementAt(i);
                 String Sda4  = (String)VRep.elementAt(i);
                 String Sda5  = (String)VPurpose.elementAt(i);
                 //String Sda6  = (String)VToMeet.elementAt(i);
                 String Sda6  = (String)VInTime.elementAt(i);
                 String Sda7  = (String)VOutTime.elementAt(i);


                 Sda1    = common.Rad(Sda1,5)+common.Space(2)+"�"+SInt;
                 Sda2    = common.Pad(Sda2,11)+"�"+SInt;
                 Sda3    = common.Pad(Sda3,25)+"�"+SInt;
                 Sda4    = common.Pad(Sda4,40)+"�"+SInt;
                 Sda5    = common.Pad(Sda5,30)+"�"+SInt;
                 Sda6    = common.Pad(Sda6,13)+"�"+SInt;
                 Sda7    = common.Pad(Sda7,13)+"�";

                 String Sda11    = common.Rad("",5)+common.Space(2)+"�"+SInt;
                 String Sda12    = common.Pad("",11)+"�"+SInt;
                 String Sda13    = common.Pad("",25)+"�"+SInt;
                 String Sda14    = common.Pad("",40)+"�"+SInt;
                 String Sda15    = common.Pad("",30)+"�"+SInt;
                 String Sda16    = common.Pad("",13)+"�"+SInt;
                 String Sda17    = common.Pad("",13)+"�";


                 String Strd  = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+"\n";
                 String Strd1 = Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+Sda17 ;

                 String SLCheck = common.parseNull((String)VSNo.elementAt(i));
                 String SLine="";

                 if(SLCheck.equals(""))
                 {
                         Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+"\n"+Strd1;
                 }
                 else
                 {
                        //SLine = common.Replicate("�",Strd.length());

                    SLine="";
                    if(i==0)
                    {
                        SLine="";
                    }
                        Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+"\n"+Strd1 ;
                 }

                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {

           VDate         = new Vector();
           VCompany      = new Vector();
           VRep          = new Vector();
           VPurpose      = new Vector();
           VToMeet       = new Vector();
           VInTime       = new Vector();
           VOutTime      = new Vector();
           VSNo          = new Vector();

           String STotalKm="";
           try
           {
                 //int iStDate       = Integer.parseInt(SStDate);
                 int iDate         = Integer.parseInt(SEnDate);
                 Vector VSlipNo    = visitorDomain.getErrectorSlipNo(iDate,iDate);


                 for(int index=0;index<VSlipNo.size();index++)
                 {

                    int iSlipNo       = Integer.parseInt((String)VSlipNo.elementAt(index));

                    Vector tVector    = visitorDomain.getErrectorReport(iSlipNo,iDate,iDate);
                    int size= (tVector.size())/9;
                    int m=0;
                    int iCount=0;

                    for(int i=0;i<size;i++)
                    {
                         if(iCount==0)
                         {

                              VSNo.addElement(String.valueOf(index+1));
                              VCompany.addElement(tVector.elementAt(m+0));
                              VRep.addElement(tVector.elementAt(m+2));
                              VPurpose.addElement(tVector.elementAt(m+3));
                              VToMeet.addElement(tVector.elementAt(m+4));
                              VInTime.addElement(tVector.elementAt(m+6));
                              VOutTime.addElement(tVector.elementAt(m+7));
                              VDate.addElement(common.parseDate((String)tVector.elementAt(m+8)));
                              iCount++;
                        }
                        else
                        {


                              VSNo.addElement("");
                              VCompany.addElement("");
                              VRep.addElement(tVector.elementAt(m+2));
                              VPurpose.addElement(tVector.elementAt(m+3));
                              VToMeet.addElement("");
                              VInTime.addElement(tVector.elementAt(m+6));
                              VOutTime.addElement(tVector.elementAt(m+7));
                              VDate.addElement(" ");
                        }
                         
                        m=m+9;
                    }

                 }


           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate);
           vect.addElement(VRep);
           vect.addElement(VCompany);
           vect.addElement(VPurpose);
           //vect.addElement(VToMeet);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
           return vect;
     }
           

/*     public String getQString()
     {
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty from Vehicles "+
                               " where OutDate <= '"+SEnDate+"' And Status=1  Order by 5 ";    

           return QString;                                                                     
     }

*/
     public String getStatus()
     {
           return SStatus;
     }

}
