/*


*/
package Reports.Errector;
import util.*;
import java.io.*;
import java.util.*;
public class DocPrint
{
      Vector VBody,VHead;
      String STitle,SFile;

      int ipctr=0,ilctr=100,iLen=0;

      FileWriter FW;

      Common common = new Common();
      
      public DocPrint(Vector VBody,Vector VHead,String STitle,String SFile)
      {
            this.VBody  = VBody;
            this.VHead  = VHead;
            this.STitle = STitle;
            this.SFile  = SFile;
            if((SFile.trim()).length()==0)
               SFile = "1.prn";
            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(SFile);

                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl+"\n");
                        ilctr++;

                  }

                  setFoot();
                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }
      public void setHead()
      {
            if(ilctr < 31)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;
            String str1 = "Company : Amarjothi Spinning Mills Ltd\n";
            String str2 = "Document : "+STitle+"\n";
            String str4 = "Page     : "+ipctr+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";
            try
            {
                  FW.write(str1);
                  FW.write(str2);
                  FW.write(str4);
                  FW.write(str3);
      
                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = VHead.size()+5;
            }
            catch(Exception ex)
            {
               System.out.println(ex);
            }
      }
      public void setFoot()
      {
            try
            {
                  String SStatus = (new java.util.Date()).toString();

                  String str3 = common.Replicate("-",iLen)+"\n";
                  str3        = str3+"Report Taken on :"+SStatus+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
}
