package Reports.Errector;

import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class ErrectorWiseCrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     VisitorIn ErrectorDomain;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          setDomain();
     }
     
     private void setDomain()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               ErrectorDomain      = (VisitorIn)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");

          Vector VCompanyInfo = ErrectorDomain.getVisitorCompanyInfo();
          Vector VCCode       = (Vector)VCompanyInfo.elementAt(0);
          Vector VCName       = (Vector)VCompanyInfo.elementAt(1);

          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='topmain'>");
          out.println("<form method='GET' action='"+SServer+"Reports.Errector.ErrectorWiseCrit2' >");

          out.println("<table>");
          out.println("<tr>");
          out.println("     <td><b>CompanyName</b></td>");
          out.println("     <td></td>");
          out.println("    <td></td>");
          out.println("<tr>");

          out.println("<tr>");
          out.println("<td><select name='CompanySelect'>");
          for(int i=0;i<VCCode.size();i++)
          {
               out.println("<option value='"+VCCode.elementAt(i)+"'>"+VCName.elementAt(i)+"</option>");
          }
          out.println("     </select>");
          out.println("     </td>");  

          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("<tr>");

          out.println("</table>");

          out.println("</form>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
}