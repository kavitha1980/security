package Reports.RentVehicle;
import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class SelectedRVRegisterCrit1 extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     RentVehicleMasterInfo VInfoDomain;
     Common common;

     public void init(ServletConfig config) throws ServletException
     {

          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VInfoDomain         = (RentVehicleMasterInfo)registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
     
               out.println("<html>");

               out.println("<body bgcolor='#9AA8D6'>");
               out.println("");
               //out.println("<base target='main'>");
               out.println("<form method='GET' action='"+SServer+"Reports.RentVehicle.SelectedRVRegisterCrit2'>");

               Vector VNo = VInfoDomain.getOwnerInfo();
     
               out.println("<table>");
               out.println("<tr>");
               out.println("     <td><b>Owner</b></td>");  
               out.println("    <td></td>");
               out.println("<tr>");

               out.println("     <td>");
               out.println("<Select name='s1'>");

               int m=0;

               Vector VOwnerCode = new Vector();

               for(int j=0;j<(VNo.size())/2;j++)
               {
                    VOwnerCode.addElement(VNo.elementAt(0+m));
                    out.println("<option value="+VNo.elementAt(0+m)+">"+VNo.elementAt(1+m)+"</option>");
                    m=m+2;
               }

               out.println("</select>");

               out.println("</td>");

               out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
               out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
               out.println("<tr>");
     
               out.println("</table>");
     
               out.println("</form>");
               out.println("</body>");
               out.println("</html>");
     
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}

