package Reports.RentVehicle;
import util.*;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import  domain.jdbc.*;


public class SelectedRVRegisterList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;
     protected String SSelected;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     String SVehicleName="";
     String SVehicleNo="";

     Vector VSNo,VRegNo,VName,VOutDate,VOutTime,VInDate,VInTime,VStKm,VEndKm,VRunKm,VPlace,VPurpose,VDriver,VPersons;
     Vector Vhead;
     Common common = new Common();

     RentVehicleInfo vDomain;

     String SStatus = "";


     SelectedRVRegisterList(String SStDate,String SEnDate,String SFile,String SSelected)
     {
            this.SStDate      = SStDate;
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            this.SSelected    = SSelected;
            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  vDomain               = (RentVehicleInfo)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Rental Vehicle Register List From "+common.parseDate(SStDate)+"  To  "+common.parseDate(SEnDate)+ "\n";
            Vector VHead  = getRentVehicleHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getRentVehicleListBody();
            new SelectedRVDocPrint(VBody,VHead,STitle,SFile,SVehicleName,SVehicleNo);
      }

      public Vector getRentVehicleHead()
      {
           Vector vect = new Vector();

           String Head1[]={"SNo","InDate","InTime","OutDate","OutTime","Starting Km","Ending Km","Running Kms","Place","Purpose","TripDriverName","No. of Persons"};        
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=((String)Head1[6]).trim();
           String Sha8=((String)Head1[7]).trim();
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();


           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);


           Sha1  = SInt+common.Rad(Sha1,5)+common.Space(3)+"�"+SInt;
           Sha2  = common.Pad(Sha2,12)+"�"+SInt;
           Sha3  = common.Pad(Sha3,12)+"�"+SInt;
           Sha4  = common.Pad(Sha4,12)+"�"+SInt;
           Sha5  = common.Pad(Sha5,12)+"�"+SInt;
           Sha6  = common.Pad(Sha6,12)+"�"+SInt;
           Sha7  = common.Pad(Sha7,12)+"�"+SInt;
           Sha8  = common.Pad(Sha8,12)+"�"+SInt;
           Sha9  = common.Pad(Sha9,20)+"�"+SInt;
           Sha10 = common.Pad(Sha10,20)+"�"+SInt;
           Sha11 = common.Pad(Sha11,15)+"�"+SInt;
           Sha12 = common.Pad(Sha12,15)+"�";

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getRentVehicleListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VOutDate.size();i++)
           {


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VInDate.elementAt(i);
                 String Sda3  = (String)VInTime.elementAt(i);
                 String Sda4  = (String)VOutDate.elementAt(i);
                 String Sda5  = (String)VOutTime.elementAt(i);
                 String Sda6  = (String)VStKm.elementAt(i);
                 String Sda7  = (String)VEndKm.elementAt(i);
                 String Sda8  = (String)VRunKm.elementAt(i);
                 String Sda9  = (String)VPlace.elementAt(i);
                 String Sda10 = (String)VPurpose.elementAt(i);
                 String Sda11 = (String)VDriver.elementAt(i);
                 String Sda12 = (String)VPersons.elementAt(i);


                 Sda1    = SInt+common.Rad(Sda1,5)+common.Space(3)+"�"+SInt;
                 Sda2    = common.Pad(Sda2,12)+"�"+SInt;
                 Sda3    = common.Pad(Sda3,12)+"�"+SInt;
                 Sda4    = common.Pad(Sda4,12)+"�"+SInt;
                 Sda5    = common.Pad(Sda5,12)+"�"+SInt;
                 Sda6    = common.Pad(Sda6,12)+"�"+SInt;
                 Sda7    = common.Pad(Sda7,12)+"�"+SInt;
                 Sda8    = common.Pad(Sda8,12)+"�"+SInt;
                 Sda9    = common.Pad(Sda9,20)+"�"+SInt;
                 Sda10   = common.Pad(Sda10,20)+"�"+SInt;
                 Sda11   = common.Pad(Sda11,15)+"�"+SInt;
                 Sda12   = common.Pad(Sda12,15)+"�";

                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12;
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {


           VSNo          = new Vector();
           VRegNo        = new Vector();
           VName         = new Vector();
           VOutDate      = new Vector();
           VOutTime      = new Vector();
           VInDate       = new Vector();
           VInTime       = new Vector();
           VStKm         = new Vector();
           VEndKm        = new Vector();
           VRunKm        = new Vector();
           VPlace        = new Vector();
           VPurpose      = new Vector();
           VDriver       = new Vector();
           VPersons      = new Vector();

           try
           {

                      String SDate   = SEnDate;
                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","gate","gatepass");
                      Statement stat                   = conn.createStatement();
                      ResultSet res                    = stat.executeQuery(getQString());

                      int i=0;
                      while (res.next())
                      {
                                 String SRegNo         = res.getString(1);
                                 String SName          = res.getString(2);
                                 String SOutDate       = common.parseDate((String)res.getString(3));
                                 String SOutTime       = res.getString(4);
                                 String SInDate        = common.parseDate((String)res.getString(5));
                                 String SInTime        = res.getString(6);
                                 String SStKm          = res.getString(7);
                                 String SEndKm         = res.getString(8);
                                 String SRunKm         = res.getString(9);
                                 String SPlace         = res.getString(10);
                                 String SPurpose       = res.getString(11);
                                 String SDriver        = res.getString(12);
                                 String SPersons       = res.getString(13);

                                 SVehicleName = SName;
                                 SVehicleNo   = SRegNo;

                                 VSNo.addElement(String.valueOf(i+1));
                                 VRegNo.addElement(SRegNo);
                                 VName.addElement(SName);
                                 VOutDate.addElement(SOutDate);
                                 VOutTime.addElement(SOutTime);
                                 VInDate.addElement(SInDate);
                                 VInTime.addElement(SInTime);
                                 VStKm.addElement(SStKm);
                                 VEndKm.addElement(SEndKm);
                                 VRunKm.addElement(SRunKm);
                                 VPlace.addElement(SPlace);
                                 VPurpose.addElement(SPurpose);
                                 VDriver.addElement(SDriver);
                                 VPersons.addElement(SPersons);


                                 i=i+1;
                                   
                      }
                      res.close();
                      stat.close();

           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VRegNo);
           vect.addElement(VName);
           vect.addElement(VOutDate);
           vect.addElement(VOutTime);
           vect.addElement(VInDate);
           vect.addElement(VInTime);
           vect.addElement(VStKm);
           vect.addElement(VEndKm);
           vect.addElement(VRunKm);
           vect.addElement(VPlace);
           vect.addElement(VPurpose);
           vect.addElement(VDriver);
           vect.addElement(VPersons);
           return vect;
     }
           

     public String getQString()
     {
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,RunKm,Place,Purpose,DriverName,NoOfPersons from RentVehicles "+
                               " where InDate >= '"+SStDate+"' AND InDate <= '"+SEnDate+"' And Status=1  and Id='"+SSelected+"' order by InDate,InTime ";

           return QString;
     }


     public String getStatus()
     {
           return SStatus;
     }

}
