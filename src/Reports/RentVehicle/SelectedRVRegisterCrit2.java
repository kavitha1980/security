package Reports.RentVehicle;
import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class SelectedRVRegisterCrit2 extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     RentVehicleMasterInfo VInfoDomain;
     Common common;

     public void init(ServletConfig config) throws ServletException
     {

          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VInfoDomain         = (RentVehicleMasterInfo)registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
     
               out.println("<html>");

               out.println("<body bgcolor='#9AA8D6'>");
               out.println("");
               out.println("<base target='main'>");
               out.println("<form method='GET' action='"+SServer+"Reports.RentVehicle.SelectedRVRegisterRep'>");

               out.println("<table>");
               out.println("<tr>");
               out.println("     <td><b>Vehicle</b></td>");  
               out.println("     <td><b>From </b></td>");
               out.println("     <td><b> To  </b></td>");
               out.println("     <td><b>File</b></td>");
               out.println("     <td></td>");
               out.println("    <td></td>");
               out.println("<tr>");

               String SOwnerCode = common.parseNull(request.getParameter("s1"));

               Vector VVehicle = VInfoDomain.getRVInfo(SOwnerCode);

               out.println("     <td>");
               out.println("<Select name='s2'>");

               int n=0;

               Vector VRVRegNo = new Vector();

               for(int j=0;j<(VVehicle.size())/3;j++)
               {
                    VRVRegNo.addElement(VVehicle.elementAt(0+n));
                    out.println("<option value="+(String)VVehicle.elementAt(2+n)+">"+VVehicle.elementAt(1+n)+"</option>");
                    n=n+3;
               }
               out.println("</select>");

               out.println("</td>");

               out.println("     <td><input type='text' name='TStDate' size='15'></td>");
               out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
               out.println("     <td><input type='text' name='TFile' size='15'></td>");
               out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
               out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
               out.println("<tr>");
     
               out.println("</table>");
     
               out.println("</form>");
               out.println("</body>");
               out.println("</html>");
     
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}

