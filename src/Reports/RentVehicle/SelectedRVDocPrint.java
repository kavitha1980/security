
package Reports.RentVehicle;

import util.*;
import java.io.*;
import java.util.*;
public class SelectedRVDocPrint
{
      Vector VBody,VHead;
      String STitle,SFile,SVehicleName,SVehicleNo;
      int ipctr=0,ilctr=100,iLen=0;

      FileWriter FW;

      Common common = new Common();
      
      public SelectedRVDocPrint(Vector VBody,Vector VHead,String STitle,String SFile,String SVehicleName,String SVehicleNo)
      {
            this.VBody        = VBody;
            this.VHead        = VHead;
            this.STitle       = STitle;
            this.SFile        = SFile;
            this.SVehicleName = SVehicleName;
            this.SVehicleNo   = SVehicleNo;

            if((SFile.trim()).length()==0)
               SFile = "1.prn";
            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(SFile);

                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl+"\n");
                        ilctr++;

                  }

                  setFoot();
                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }
      public void setHead()
      {
            if(ilctr < 54)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;
            String str1 = "\t\t\t\t Amarjothi Spinning Mills Ltd\n";
            String str2 = " \t\t\t "+STitle+"\n";
            String str4 = " Page : "+ipctr+" \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t RegNo:"+SVehicleNo+" \t  Name:"+SVehicleName+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";
            try
            {
                  FW.write(str1);
                  FW.write(str2);
                  FW.write(str4);
                  FW.write(str3);
      
                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = VHead.size()+5;
            }
            catch(Exception ex)
            {
               System.out.println(ex);
            }
      }
      public void setFoot()
      {
            try
            {
                  String SStatus = (new java.util.Date()).toString();

                  String str3 = common.Replicate("-",iLen)+"\n";
                  str3        = str3+"Report Taken on :"+SStatus+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
}
