package Reports.RentVehicle;
import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class SelectedRVRegisterCrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     RentVehicleInfo VDomain;
     RentVehicleMasterInfo VInfoDomain;
     Common common;

     public void init(ServletConfig config) throws ServletException
     {

          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry1  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VDomain             = (RentVehicleInfo)registry1.lookup(SECURITYDOMAIN);

               Registry registry2  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VInfoDomain         = (RentVehicleMasterInfo)registry2.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
     
               out.println("<html>");

               out.println("<SCRIPT LANGUAGE='JavaScript'>");
    
               out.println("function func1(xyz)");
               out.println("{");
               out.println(" str=xyz.options.value;");
               out.println(" return;");
               out.println("}");
               out.println("</Script>");

               out.println("<body bgcolor='#9AA8D6'>");
               out.println("");
               out.println("<base target='main'>");
               out.println("<form method='GET' action='"+SServer+"Reports.RentVehicle.SelectedRVRegisterRep'>");

               Vector VNo = VInfoDomain.getOwnerInfo();
     
               out.println("<table>");
               out.println("<tr>");
               out.println("     <td><b>Owner</b></td>");  
               out.println("     <td><b>Vehicle</b></td>");  
               out.println("     <td><b>From </b></td>");
               out.println("     <td><b> To  </b></td>");
               out.println("     <td><b>File</b></td>");
               out.println("     <td></td>");
               out.println("    <td></td>");
               out.println("<tr>");

               out.println("     <td>");
               out.println("<Select name='s1' onSelect='func1(this.form.s1)'>");

               int m=0;

               Vector VOwnerCode = new Vector();

               for(int j=0;j<(VNo.size())/2;j++)
               {
                    VOwnerCode.addElement(VNo.elementAt(0+m));
                    out.println("<option value="+j+">"+VNo.elementAt(1+m)+"</option>");
                    m=m+2;
               }

               out.println("</select>");

               out.println("</td>");


               //String SSelected  = common.parseNull(request.getParameter("s1"));
               //int iSelected     = common.toInt(SSelected);
                                 
               //String SOwnerCode = (String)VOwnerCode.elementAt(iSelected);
               System.out.println(str);

               //Vector VVehicle = VInfoDomain.getRVInfo(SOwnerCode);
               Vector VVehicle = VInfoDomain.getRVInfo(str);

               out.println("     <td>");
               out.println("<Select name='s2'>");

               int n=0;

               Vector VRVRegNo = new Vector();

               for(int j=0;j<(VVehicle.size())/2;j++)
               {
                    VRVRegNo.addElement(VVehicle.elementAt(0+n));
                    out.println("<option value="+VVehicle.elementAt(0+n)+">"+VVehicle.elementAt(1+n)+"</option>");
                    n=n+2;
               }
               out.println("</select>");

               out.println("</td>");

               out.println("     <td><input type='text' name='TStDate' size='15'></td>");
               out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
               out.println("     <td><input type='text' name='TFile' size='15'></td>");
               out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
               out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
               out.println("<tr>");
     
               out.println("</table>");
     
               out.println("</form>");
               out.println("</body>");
               out.println("</html>");
     
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
}

