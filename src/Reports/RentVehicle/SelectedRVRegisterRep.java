package Reports.RentVehicle;
import util.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;


public class SelectedRVRegisterRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     RentVehicleInfo VDomain;
     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry  registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               VDomain             = (RentVehicleInfo)registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
               String SSelected  = common.parseNull(request.getParameter("s2")).trim();
     
               String SStDate   = common.pureDate(request.getParameter("TStDate"));
               String SEnDate   = common.pureDate(request.getParameter("TEnDate"));
               String SFile     = common.parseNull(request.getParameter("TFile"));
     
               SFile = (SFile.trim()).length()==0?"f:/RentVehicleRegister.prn":SFile;
     
               SelectedRVInformation info  = new SelectedRVInformation();
     
               SelectedRVRegisterList theList    = new SelectedRVRegisterList(SStDate,SEnDate,SFile,SSelected);
               Vector vect            = new Vector();  
               vect                   = theList.getPrintValues();
     
               Vector VInfo  = new Vector();
               Vector VValue = new Vector();
     
               VInfo.addElement("Report Name");
               VInfo.addElement("Report Code");
               VInfo.addElement("Report url/File Name");
               VInfo.addElement("Status");
     
               VValue.addElement("Rental Vehicle Movement  List as on a Particular Date");
               VValue.addElement("2(e)");
               VValue.addElement(SFile);
               VValue.addElement(theList.getStatus());
     
     
               info.flashMessage(VInfo,VValue,vect,out);
     
               
                                  
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }

}
