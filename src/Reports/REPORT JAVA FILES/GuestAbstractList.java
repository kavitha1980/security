package Reports;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class VisitorAbstractList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;
     String SCompany="";

     Object RowData[][];


     Vector VSNo,VDate,VCompany,VRep,VToMeet,VMon,VTue,VThu,VFri,VSat,VTot,VDays;
     Vector Vhead,VName;
     Common common = new Common();
     Info InfoDomain;

     String SStatus = "";
     int iSelected   =0;

     VisitorAbstractList(String StDate,String SEnDate,String SFile,int index)
     {

            this.SStDate = StDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  InfoDomain            = (Info)registry.lookup(SECURITYDOMAIN);
                  Vector VTotal         = visitorDomain.getEmployeeNames();
                  Vector VCode          = (Vector)VTotal.elementAt(0);
                  VName                 = (Vector)VTotal.elementAt(1);

                  iSelected              = Integer.parseInt((String)VCode.elementAt(index));
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();
            }

     }

     public void setInwardList()
     {
            setDataIntoVector();

            Vector VHead  = getVisitorHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVisitorListBody();
            String STitle = " Visitor Abstract List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" +  \n "+
                            " For the Company :"+SCompany+" \n " ;
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getVisitorHead()
      {
           Vector vect = new Vector();

           String Head1[]={" SNo","RepresentativeName","Whom To Meet","Monday","TuesDay","ThursDay","FriDay","SaturDay","TotalNos" } ;        

           String Sha1=((String)Head1[0]).trim();
           String Sha4=((String)Head1[1]).trim();
           String Sha5=((String)Head1[2]).trim();
           String Sha6=((String)Head1[3]).trim();
           String Sha7=common.parseNull((String)Head1[4]);
           String Sha8=common.parseNull((String)Head1[5]);
           String Sha9=common.parseNull((String)Head1[6]);
           String Sha10=common.parseNull((String)Head1[7]);
           String Sha11=common.parseNull((String)Head1[8]);

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);


           Sha1  = common.Rad(Sha1,5)+common.Space(2);
           Sha4  = common.Pad(Sha4,25)+SInt;
           Sha5  = common.Pad(Sha5,30)+SInt;
           Sha6  = common.Pad(Sha6,10)+SInt;
           Sha7  = common.Pad(Sha7,10)+SInt;
           Sha8  = common.Pad(Sha8,10)+SInt;
           Sha9  = common.Pad(Sha9,10)+SInt;
           Sha10  = common.Pad(Sha10,10)+SInt;
           Sha11  = common.Pad(Sha11,10)+SInt;

           String Strh1 = Sha1+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVisitorListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {
               VSNo.addElement(String.valueOf(i+1));
           }

           for(int i=0;i<VDate.size();i++)
           {


                 String Sda1   = (String)VSNo.elementAt(i);
                 String Sda4   = (String)VRep.elementAt(i);
                 String Sda5   = (String)VToMeet.elementAt(i);
                 String Sda6   = (String)VMon.elementAt(i);
                 String Sda7   = (String)VTue.elementAt(i);
                 String Sda8   = (String)VThu.elementAt(i);
                 String Sda9   = (String)VFri.elementAt(i);
                 String Sda10  = (String)VSat.elementAt(i);
                 String Sda11  = (String)VTot.elementAt(i);


                 Sda1    = common.Rad(Sda1,5)+common.Space(2);
                 Sda4    = common.Pad(Sda4,25)+SInt;
                 Sda5    = common.Pad(Sda5,30)+SInt;
                 Sda6    = common.Pad(Sda6,10)+SInt;
                 Sda7    = common.Pad(Sda7,10)+SInt;
                 Sda8    = common.Pad(Sda8,10)+SInt;
                 Sda9    = common.Pad(Sda9,10)+SInt;
                 Sda10   = common.Pad(Sda10,10)+SInt;
                 Sda11   = common.Pad(Sda11,10);
                 

                 String Strd = Sda1+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+"\n";
                 vect.add(Strd);
                 
           }
           return vect;
     }



     public void setDataIntoVector()
     {

           VDate         = new Vector();
           VCompany      = new Vector();
           VRep          = new Vector();
           VToMeet       = new Vector();
           VMon          = new Vector();
           VTue          = new Vector();
           VThu          = new Vector();
           VFri          = new Vector();
           VSat          = new Vector();
           VTot          = new Vector();
           VDays         = new Vector();
           VSNo          = new Vector();

           String STotalKm="";

           try
           {
                 int iStDate       = Integer.parseInt(SStDate);
                 int iDate         = Integer.parseInt(SEnDate);
                 int iRCode=0;

                 Vector tRepVector = visitorDomain.getVisitorRepInfo(iSelected);

                 Vector VRepCode   = (Vector) tRepVector.elementAt(0);
                 Vector VRepName   = (Vector) tRepVector.elementAt(1);
                 Vector tVector    = new Vector();
                 
                 int iSNo          =1;

                 for(int k=0;k<VRepCode.size();k++)
                 {

                      iRCode            = Integer.parseInt((String)VRepCode.elementAt(k));

                      tVector           = visitorDomain.getVisitorAbstractReport(iStDate,iDate,iSelected,iRCode);


                      int size= (tVector.size())/9;
     
                      int m=0,tot=0;
                      String Mon="",Tue="",Thu="",Fri="",Sat="";
                      int mi=1,ti=1,thi=1,fi=1,si=1;

                      for(int i=0;i<size;i++)
                      {

                              String SDate   = (String)tVector.elementAt(m+8);
                              int IDate      = Integer.parseInt(SDate);
     
                              String SDay    = common.getDayOfDate(IDate);
                              if(i==0)
                              {
                                   //VSNo.addElement(String.valueOf(iSNo));
                                   VCompany.addElement(tVector.elementAt(m+0));
                                   SCompany  =(String)tVector.elementAt(m+0);

                                   VRep.addElement(tVector.elementAt(m+2));
                                   VToMeet.addElement(tVector.elementAt(m+4));
                                   VDate.addElement(common.parseDate((String)tVector.elementAt(m+8)));
                              }
                              if(SDay.equals("Mon"))
                              {
          
                                   if(i!=0)
                                   {
                                        Mon="X +"+mi;
                                        mi=mi+1;
                                        tot=tot+1;
                                   }
                                   else
                                   {
                                        Mon="X"+(i+1);
                                        tot=tot+1;
                                        
                                   }
                              }
     
                              if(SDay.equals("Tue"))
                              {
                                   if(i!=0)
                                   {

                                        Tue="X +"+ti;
                                        ti=ti+1;
                                        tot=tot+1;

                                   }
                                   else
                                   {
                                        Tue="X +"+(i+1);     
                                        tot=tot+1;
                                        
                                   }

                              }
     
                              if(SDay.equals("Thu"))
                              {
                                   if(i!=0)
                                   {
                                        Thu="X +"+thi;
                                        thi=thi+1;
                                        tot = tot+1;
                                   }
                                   else
                                   {
                                        Thu="X"+(i+1);     
                                        tot = tot+1;
                                   }     
                              }
     
                              if(SDay.equals("Fri"))
                              {
          
                                   if(i!=0)
                                   {

                                        Fri="X +"+fi;
                                        fi=fi+1;
                                        tot = tot+1;
                                   }
                                   else
                                   {
                                        Fri="X"+(i+1);
                                        tot = tot+1;
                                   }     
                              }


                              if(SDay.equals("Sat"))
                              {
          
                                   if(i!=0)
                                   {
                                        Sat="X +"+si;
                                        si=si+1;
                                        tot = tot+1;
                                   }
                                   else
                                   {
                                        Sat="X"+(i+1);
                                        tot = tot+1;
                                   }
                              }
                              m=m+9;

                      }

                              VMon.addElement(Mon);
                              VTue.addElement(Tue);
                              VThu.addElement(Thu);
                              VFri.addElement(Fri);
                              VSat.addElement(Sat);

                              System.out.println("code:"+iRCode);
                              System.out.println(tot);

                              VTot.addElement(String.valueOf(tot));
                              Mon="";Tue="";Thu="";Fri="";Sat="";
                              

                              tot=0;

                      tVector.removeAllElements();
                      
                 }

           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate);
           vect.addElement(VCompany);
           vect.addElement(VRep);
           vect.addElement(VToMeet);
           vect.addElement(VMon);
           vect.addElement(VTue);
           vect.addElement(VThu);
           vect.addElement(VFri);
           vect.addElement(VSat);
           vect.addElement(VTot);

           return vect;
     }
           

/*     public String getQString()
     {
           //String QString = "SELECT GateInward.GINo, GateInward.GIDate, Supplier.Name, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate "+
                    //" FROM (GateInward INNER JOIN Supplier ON GateInward.Sup_Code = Supplier.Ac_Code) INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                    // "Where GateInward.GIDate <= '"+SEnDate+"' ";
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty from Vehicles "+
                               " where OutDate <= '"+SEnDate+"' And Status=1  Order by 5 ";    

           return QString;                                                                     
     }

*/
     public String getStatus()
     {
           return SStatus;
     }

}
