package Reports;

import java.util.*;
import java.io.*;
import java.sql.*;

public class GuestRegisterList
{
     String SSelected;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;
     
     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;
     
     Vector VInDate,VGuestName,VGuestPlace,VSex,VNoPersons,VToMeet,VSpentTime,VSNo;
     Vector GHead,VDepartment,VInTime,VOutTime,VOutDate;
     Common common = new Common();

     GuestRegisterList(String SSelected,String SStDate,String SEnDate,String SFile)
     {
          this.SSelected = SSelected;
          this.SStDate = SStDate;
          this.SEnDate = SEnDate;
          this.SFile   = SFile;
          
          try
          {
               SStatus = (new java.util.Date()).toString();
               setGuestList();
          }
          catch(Exception ex)
          {
               SStatus = ex.getMessage();   
          }
     }

     public void setGuestList()
     {
            setDataIntoVector();
            String STitle     = "Guest Register List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" \n";
            Vector GHead      = getGuestHead();
            iLen              = ((String)GHead.elementAt(0)).length();
            Strline           = common.Replicate("-",iLen)+"\n";
            Vector VBody      = getGuestListBody();

            new DocPrint(VBody,GHead,STitle,SFile);
      }

     public void setDataIntoVector ()
     {
          VInDate       = new Vector();
          VGuestName    = new Vector();
          VGuestPlace   = new Vector();
          VSex          = new Vector();
          VNoPersons    = new Vector();
          VToMeet       = new Vector();
          VDepartment   = new Vector();
          VOutDate      = new Vector();
          VInTime       = new Vector();
          VOutTime      = new Vector();
          VSpentTime    = new Vector();
          VSNo          = new Vector();       

          try
          {
               String SDate        = SEnDate;
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn     = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat      = conn.createStatement();
               ResultSet res       = stat.executeQuery(getQString());
               int iSno=1;
               while (res.next())
               {
                    String SInDate      = common.parseDate((String)res.getString(1));
                    String SGuestName   = res.getString(2);
                    String SGuestPlace  = res.getString(3);
                    String SSex         = res.getString(4);
                    String SNoPersons   = common.parseNull((String)res.getString(5));
                    String SToMeet      = res.getString(6);
                    String SDepartment  = res.getString(7);   
                    String SOutDate     = common.parseDate((String)res.getString(8));
                    String SInTime      = res.getString(9);
                    String SOutTime     = res.getString(10);

                    int iOutDate        = Integer.parseInt((String)res.getString(8));
                    int iInDate         = Integer.parseInt((String)res.getString(1));
                    String SSpentTime   = common.getTimeDiff(iOutDate,iInDate,SOutTime,SInTime);

                    VSNo.addElement(String.valueOf(iSno));
                    VInDate.addElement(SInDate);
                    VGuestName.addElement(SGuestName);
                    VGuestPlace.addElement(SGuestPlace);
                    VSex.addElement(SSex);
                    VNoPersons.addElement(SNoPersons);
                    VToMeet.addElement(SToMeet);
                    VDepartment.addElement(SDepartment);
                    VOutDate.addElement(SOutDate);
                    VInTime.addElement(SInTime);
                    VOutTime.addElement(SOutTime);
                    VSpentTime.addElement(SSpentTime);
                    iSno= iSno+1;
                    
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     public Vector getGuestHead()
     {
          Vector vect = new Vector();
          
          String Head1[]={"InDate","GuestName","GuestPlace","Sex","No.of.Persons","ToMeet","Department","OutDate","InTime","OutTime","SpentTime","SNo"};    

          String Field1  =((String)Head1[0]).trim();
          String Field2  =((String)Head1[1]).trim();
          String Field3  =((String)Head1[2]).trim();
          String Field4  =((String)Head1[3]).trim();
          String Field5  =((String)Head1[4]).trim();

          String Field6  =((String)Head1[5]).trim();
          String Field7  =((String)Head1[6]).trim();
          String Field8  =((String)Head1[7]).trim();
          String Field9  =((String)Head1[8]).trim();
          String Field10 =((String)Head1[9]).trim();
          String Field11 =((String)Head1[10]).trim();
          String Field12 =((String)Head1[11]).trim();
          
          GHead          = new Vector();
          
          GHead.addElement(Field1);
          GHead.addElement(Field2);
          GHead.addElement(Field3);
          GHead.addElement(Field4);
          GHead.addElement(Field5);
          GHead.addElement(Field6);
          GHead.addElement(Field7);
          GHead.addElement(Field8);
          GHead.addElement(Field9);
          GHead.addElement(Field10);
          GHead.addElement(Field11);
          GHead.addElement(Field12);

          Field12   = common.Rad(Field12,5)+common.Space(5);
          Field1    = common.Pad(Field1,15)+SInt;
          Field2    = common.Pad(Field2,15)+SInt;
          Field3    = common.Pad(Field3,15)+SInt;
          Field4    = common.Pad(Field4,15)+SInt;
          Field5    = common.Pad(Field5,15)+SInt;
          Field6    = common.Pad(Field6,15)+SInt;
          Field7    = common.Pad(Field7,15)+SInt;
          Field8    = common.Pad(Field8,15)+SInt;
          Field9    = common.Pad(Field9,15)+SInt;
          Field10   = common.Pad(Field10,15)+SInt;
          Field11   = common.Pad(Field11,15)+SInt;

          String Strh1 = Field12+Field1+Field2+Field3+Field4+Field5+Field6+Field7+Field8+Field9+Field10+Field11+"\n";
          vect.add(Strh1);
          return vect;
     }

     public Vector getGuestListBody()
     {
          Vector vect    = new Vector();
          
          for(int i=0;i<VGuestName.size();i++)
          {
               String List1   = (String)VInDate.elementAt(i);
               String List2   = (String)VGuestName.elementAt(i);
               String List3   = (String)VGuestPlace.elementAt(i);
               String List4   = (String)VSex.elementAt(i);
               String List5   = (String)VNoPersons.elementAt(i);
               String List6   = (String)VToMeet.elementAt(i);
               String List7   = (String)VDepartment.elementAt(i);
               String List8   = (String)VOutDate.elementAt(i);
               String List9   = (String)VInTime.elementAt(i);
               String List10  = (String)VOutTime.elementAt(i);
               String List11  = (String)VSpentTime.elementAt(i);
               String List12  = (String)VSNo.elementAt(i);
               
               List12    = common.Rad(List12,5)+common.Space(5);
               List1     = common.Pad(List1,15)+SInt;
               List2     = common.Pad(List2,15)+SInt;
               List3     = common.Pad(List3,15)+SInt;
               List4     = common.Pad(List4,15)+SInt;
               List5     = common.Pad(List5,15)+SInt;
               List6     = common.Pad(List6,15)+SInt;
               List7     = common.Pad(List7,15)+SInt;
               List8     = common.Pad(List8,15)+SInt;
               List9     = common.Pad(List9,15)+SInt;
               List10    = common.Pad(List10,15)+SInt;
               List11    = common.Pad(List11,15)+SInt;
               
               String Strd    = List12+List1+List2+List3+List4+List5+List6+List7+List8+List9+List10+List11+"\n";
               vect.add(Strd);
          }
          return vect;
     }

     public Vector getPrintValues()
     {
          Vector vect    = new Vector();
          
          vect.addElement(GHead);
          vect.addElement(VSNo);
          vect.addElement(VInDate);
          vect.addElement(VGuestName);
          vect.addElement(VGuestPlace);
          vect.addElement(VSex);
          vect.addElement(VNoPersons);
          vect.addElement(VToMeet);
          vect.addElement(VDepartment);
          vect.addElement(VOutDate);
          vect.addElement(VInTime);
          vect.addElement(VOutTime);
          vect.addElement(VSpentTime);

          return vect;
     }
     
     public String getQString()
     {
          String QString = "Select InDate,GuestName,GuestPlace,Sex,NoPersons,ToMeet,Department,OutDate,InTime,OutTime from GuestTable "+
                           "where OutDate >="+SStDate+" And OutDate <="+SEnDate+" And Status=1 and TOMeet ="+SSelected+" order by 1";   
          return QString;
     }
     
     public String getStatus()
     {
          return SStatus;
     }

}
                             
