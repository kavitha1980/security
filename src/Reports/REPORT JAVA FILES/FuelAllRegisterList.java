package Reports;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;


public class FuelAllRegisterList implements rndi.CodedNames
{

     protected String SSelected;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;
     VehicleInfo VehicleDomain;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Object RowData[][];
     String ColumnData[] = {"Date","MRS No","Code","Name","Quantity","Catl","Draw","Make","Dept","Due Date","Delay Days"};
     String ColumnType[] = {"S","N","S","S","N","S","S","S","S","S","N"};


     Vector VSNo,VName,VOutDate,VOutTime,VInTime,VStKm,VEndKm,VTotalKm,VDriver,VSecurity,VDiNo,VKiNo,VDiDate,VKiDate,VDQty,VKQty,VBunk,VClosing,VAverage,VRemarks,VTank;
     Vector Vhead;
     Common common = new Common();


     String SStatus = "";
     Vector VTotal;

      Vector VDTotal     = new Vector();
      Vector VKTotal     = new Vector();
      Vector VKmTotal    = new Vector();


     FuelAllRegisterList(String SSelected,String SStDate,String SEnDate,String SFile)
     {

            this.SStDate      = SStDate;
            this.SSelected    = SSelected; 
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            try
            {
                  Registry    registry  = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  VehicleDomain         = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            VTotal     = new Vector();
            setDataIntoVector();
            String STitle = " Fuel Consumption  List From "+common.parseDate(SStDate)+"  To "+common.parseDate(SEnDate)+" \n";

            Vector VHead  = getVehicleHead();
            getVehicleHTotal();

            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getVehicleListBody();
            String SName      = (String)VName.elementAt(0);
            new FuelAllDocPrint(VBody,VHead,STitle,SFile,VTotal,SSelected,SName);
      }
      public void getVehicleHTotal()
      {
           Vector vThead = new Vector();

           String Head1[]={"TotalDieselConsumption","Total Kero Consumption","Total Km Travelled" };
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();

           Sha1  = common.Pad(Sha1,30);
           Sha2  = common.Pad(Sha2,30)+SInt;
           Sha3  = common.Pad(Sha3,20)+SInt;

           String Strh2 = Sha1+Sha2+Sha3+"\n";
           VTotal.addElement(Strh2);
      }

      public Vector getVehicleHead()
      {
           Vector vect = new Vector();

           String Head1[]={"SlNo","VehicleName","DINo","KiNo","DIDate","KIDate","DriverName","SecurityName","DQty","KQty","InitialReading","ClosingReading","TotalKmsTravelled","Avg Kms/Litre","TankFull/Part","Remarks"};

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();
           String Sha16=((String)Head1[15]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);
           Vhead.addElement(Sha16);


           Sha1  = common.Pad(Sha1,5);
           Sha2  = common.Pad(Sha2,15)+SInt;
           Sha3  = common.Pad(Sha3,8)+SInt;
           Sha4  = common.Pad(Sha4,8)+SInt;
           Sha5  = common.Pad(Sha5,15)+SInt;
           Sha6  = common.Pad(Sha6,15)+SInt;
           Sha7  = common.Pad(Sha7,15)+SInt;
           Sha8  = common.Pad(Sha8,20)+SInt;
           Sha9  = common.Pad(Sha9,5)+SInt;
           Sha10 = common.Pad(Sha10,5)+SInt;
           Sha11 = common.Pad(Sha11,8)+SInt;
           Sha12 = common.Pad(Sha12,8)+SInt;
           Sha13 = common.Pad(Sha13,15)+SInt;
           Sha14 = common.Pad(Sha14,10)+SInt;
           Sha15 = common.Pad(Sha15,8)+SInt;
           Sha16 = common.Pad(Sha16,15)+SInt;

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+Sha16+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getVehicleListBody()
     {
           Vector vect = new Vector();
           int iDTotal=0,iKTotal=0,iKmTotal=0;


           int index=0;

           String Sda17="",Sda18="",Sda19="";
           int iSize = (VOutDate.size()-2);


           for(int i=0;i<(VOutDate.size())-1;i++)
           {
           String Head1[]={"SlNo","DINo","KiNo","DIDate","KIDate","DriverName","SecurityName","DQty","KQty","InitialReading","ClosingReading","TotalKmsTravelled","Avg Kms/Litre","TankFull/Part","Remarks"};


                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VName.elementAt(i);
                 String Sda3  = (String)VDiNo.elementAt(i);
                 String Sda4  = (String)VKiNo.elementAt(i);
                 String Sda5  = (String)VDiDate.elementAt(i);
                 String Sda6  = (String)VKiDate.elementAt(i);
                 String Sda7  = (String)VDriver.elementAt(i);
                 String Sda8  = (String)VSecurity.elementAt(i);
                 String Sda9  = (String)VDQty.elementAt(i);
                 String Sda10  = (String)VKQty.elementAt(i);
                 String Sda11 = (String)VStKm.elementAt(i);
                 String Sda12 = (String)VClosing.elementAt(i+1);
                 String Sda13 = (String)VTotalKm.elementAt(i);
                 String Sda14 = (String)VAverage.elementAt(i);
                 String Sda15 = (String)VTank.elementAt(i);
                 String Sda16 = (String)VRemarks.elementAt(i);

                 
                 String SDQty        = (String)VDQty.elementAt(i);
                 iDTotal             = iDTotal+Integer.parseInt(SDQty);
                 String SKQty        = (String)VKQty.elementAt(i);
                 iKTotal             = iKTotal+Integer.parseInt(SKQty);
                 String SKmTotal     = (String)VTotalKm.elementAt(i);
                 iKmTotal            = iKmTotal+Integer.parseInt(SKmTotal);



                 if(index==iSize)
                 {

                         VDTotal.addElement(String.valueOf(iDTotal));
                         VKTotal.addElement(String.valueOf(iKTotal));
                         VKmTotal.addElement(String.valueOf(iKmTotal));

                         Sda17   = (String)VDTotal.elementAt(0);
                         Sda18   = (String)VKTotal.elementAt(0);
                         Sda19   = (String)VKmTotal.elementAt(0);
                 }


                 Sda1    = common.Pad(Sda1,5);
                 Sda2    = common.Pad(Sda2,15)+SInt;
                 Sda3    = common.Pad(Sda3,8)+SInt;
                 Sda4    = common.Pad(Sda4,8)+SInt;
                 Sda5    = common.Pad(Sda5,15)+SInt;
                 Sda6    = common.Pad(Sda6,15)+SInt;
                 Sda7    = common.Pad(Sda7,15)+SInt;
                 Sda8    = common.Pad(Sda8,20)+SInt;
                 Sda9    = common.Pad(Sda9,5)+SInt;
                 Sda10   = common.Pad(Sda10,5)+SInt;
                 Sda11   = common.Pad(Sda11,8)+SInt;
                 Sda12   = common.Pad(Sda12,8)+SInt;
                 Sda13   = common.Pad(Sda13,15)+SInt;
                 Sda14   = common.Pad(Sda14,10)+SInt;
                 Sda15   = common.Pad(Sda15,8)+SInt;
                 Sda16   = common.Pad(Sda16,15);

                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+Sda16+"\n";


                 vect.add(Strd);

                 if(index==iSize)
                 {
                         Sda17   = common.Pad(Sda17,30)+SInt;
                         Sda18   = common.Pad(Sda18,30);
                         Sda19   = common.Pad(Sda19,20);

                         String Strd1= Sda17+Sda18+Sda19+"\n";
                         VTotal.add(Strd1);
                 }                            
                 
                 index=index+1;
                    

           }
           return vect;
               
     }

     public void setDataIntoVector ()
     {


           VName         = new Vector();
           VOutDate      = new Vector();
           VOutTime      = new Vector();
           VInTime       = new Vector();
           VStKm         = new Vector();
           VEndKm        = new Vector();
           VTotalKm      = new Vector();
           VDriver       = new Vector();
           VSecurity     = new Vector();
           VDiNo         = new Vector();
           VKiNo         = new Vector();
           VDiDate       = new Vector();
           VKiDate       = new Vector();
           VDQty         = new Vector();
           VKQty         = new Vector();
           VBunk         = new Vector();
           VClosing      = new Vector();
           VTank         = new Vector();
           VAverage      = new Vector();
           VRemarks      = new Vector();
           VSNo          = new Vector();          

           String STotalKm="";
           try
           {


               String SDate   = SEnDate;
               Class.forName("oracle.jdbc.OracleDriver");
               Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat                   = conn.createStatement();
               int inc=0,sno=1,iOffset=0;

               Vector VVehicleData      = VehicleDomain.getVehicleData();
               Vector VVehicleNo        = new Vector();
               for(int iVIndex=0;iVIndex<(VVehicleData.size())/2;iVIndex++)
               {
                    VVehicleNo.addElement(VVehicleData.elementAt(iOffset+0));     
                    iOffset = iOffset+2;
               }
               for(int i=0;i<VVehicleNo.size();i++)
               {
                    String SSelectedNo       = (String)VVehicleNo.elementAt(i);     
                    ResultSet res            = stat.executeQuery(getQString(SSelectedNo));
                    while(res.next())
                    {
                                      
                         String SName        = res.getString(1);
                         String SOutDate     = common.parseDate((String)res.getString(2));
                         String SOutTime     = res.getString(3);
                         String SInTime      = res.getString(4);
                         
                         String SStKm        = res.getString(5);
                         String SEndKm       = res.getString(6);
                         
                         VStKm.addElement(SStKm);
                         VEndKm.addElement(SEndKm);
                         
                         VClosing.addElement(SStKm);
                         
                         //String STotalKm       = common.parseNull(res.getString(9));
                         String SDriver        = res.getString(7);
                         String SSecurity      = common.parseNull((String)res.getString(8));
                         String SDiNo                 = common.parseNull((String)res.getString(9));
                         String SKiNo                 = common.parseNull((String)res.getString(10));
                         String SDiDate               = common.parseDate(common.parseNull((String)res.getString(11)));
                         String SKiDate               = common.parseDate(common.parseNull((String)res.getString(12)));
                         String SDQty                 = common.parseNull((String)res.getString(13));
                         String SKQty                 = common.parseNull((String)res.getString(14));
                         String SBunk                 = common.parseNull((String)res.getString(15));     
                         
                         
                         VName.addElement(SName);
                         VOutDate.addElement(SOutDate);
                         VOutTime.addElement(SOutTime);
                         VInTime.addElement(SInTime);
                         VDriver.addElement(SDriver);
                         VSecurity.addElement(SSecurity);
                         VDiNo.addElement(SDiNo);
                         VKiNo.addElement(SKiNo);
                         VDiDate.addElement(SDiDate);
                         VKiDate.addElement(SKiDate);
                         VDQty.addElement(SDQty);
                         VKQty.addElement(SKQty);
                         VBunk.addElement(SBunk);
                         VSNo.addElement(String.valueOf(sno));
                         sno=sno+1;
                         VTank.addElement("PARTLY");
                         VRemarks.addElement(" ");
                         
                    }
                         int VSize= (VClosing.size())-1;
                         for(int index=0;index<VSize;index++)
                         {
                              
                              int iskm              = Integer.parseInt((String)VStKm.elementAt(index));
                              int iendkm            = Integer.parseInt((String)VClosing.elementAt(index+1));
                              int itot              = iendkm-iskm;
     
                              if(itot<0)
                                  STotalKm   = "0";   
                              else
                                  STotalKm       = Integer.toString(itot);
     
                              int iDQty             = Integer.parseInt(common.parseNull((String)VDQty.elementAt(index)));
                              int iKQty             = Integer.parseInt(common.parseNull((String)VKQty.elementAt(index)));
     
                              int iFuel             = iDQty+iKQty;
                              double sAvg           = common.toDouble(String.valueOf(itot))/iFuel;
     
                              String iAvg           = common.getRound(sAvg,3);
     
                              VAverage.addElement(iAvg);
     
                              VTotalKm.addElement(STotalKm);
                         }
               }

           }
           catch(Exception ex)
           {
                    System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VName);
           vect.addElement(VSNo);
           vect.addElement(VDiNo);
           vect.addElement(VKiNo);
           vect.addElement(VDiDate);
           vect.addElement(VKiDate);
           vect.addElement(VDriver);
           vect.addElement(VSecurity);
           vect.addElement(VDQty);
           vect.addElement(VKQty);
           vect.addElement(VStKm);
           vect.addElement(VEndKm);
           vect.addElement(VTotalKm);
           vect.addElement(VAverage);
           vect.addElement(VTank);
           vect.addElement(VRemarks);

           return vect;
     }
                                   

     public String getQString(String SSelectedVehicleNo)
     {
           String QString    = " Select VehicleName,OutDate,OutTime,InTime,StKm,EndKm,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty,BunkName from Vehicles "+
                               " where OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' And Status=1 AND Purpose = 'TO FILL FUEL' And VehicleNo = '"+SSelectedVehicleNo+"'";    

           return QString;
     }

     public String getStatus()
     {
           return SStatus;
     }

}
