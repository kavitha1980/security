package Reports;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import blf.*;
import domain.jdbc.*;

public class FuelAllRegisterRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;

     Common common;
      VehicleInfo vDomain;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               vDomain             = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
     
               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
               Vector VVehicle  = vDomain.getVehicleData();
               
               int m=0;

               Vector VTemp = new Vector();

               for(int j=0;j<(VVehicle.size())/2;j++)
               {
                    VTemp.addElement(VVehicle.elementAt(0+m));
                    m=m+2;
               }

               //String SSelected = common.parseNull(request.getParameter("s1"));
               //int iSelected    = Integer.parseInt(SSelected);

               //SSelected        = (String)VTemp.elementAt(iSelected);
               String SSelected         = " "   ;
               String SStDate   = common.pureDate(request.getParameter("TStDate"));
               String SEnDate   = common.pureDate(request.getParameter("TEnDate"));
               String SFile     = common.parseNull(request.getParameter("TFile"));
     
               SFile = (SFile.trim()).length()==0?"d:/Security/FuelRegisterList.prn":SFile;
     
               FuelAllInformation info  = new FuelAllInformation();
     
               FuelAllRegisterList theList    = new FuelAllRegisterList(SSelected,SStDate,SEnDate,SFile);

               Vector vect            = new Vector();  
               vect                   = theList.getPrintValues();
     
               Vector VInfo  = new Vector();
               Vector VValue = new Vector();
     
               VInfo.addElement("Report Name");
               VInfo.addElement("Report Code");
               VInfo.addElement("Report url/File Name");
               VInfo.addElement("Status");
     
               VValue.addElement("Fuel Consumption  List as on a Particular Date");
               VValue.addElement("2(e)");
               VValue.addElement(SFile);
               VValue.addElement(theList.getStatus());
     
               SStDate   = common.parseDate(SStDate);
               SEnDate   = common.parseDate(SEnDate);

               info.flashMessage(VInfo,VValue,vect,out,SStDate,SEnDate);
     
               
                                  
               out.close();

          }
          catch(Exception e)
          {
               //e.printStackTrace();
               System.out.println("No Records");
//               JOptionPane.showMessageDialog(null,"No Records");
          }
     }

}
