package Reports;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class VisitorHourRep extends HttpServlet
{
     HttpSession session;

     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");

          String SStDate   = common.pureDate(request.getParameter("TStDate"));
          String SEnDate   = common.pureDate(request.getParameter("TEnDate"));
          String SFile     = common.parseNull(request.getParameter("TFile"));

          SFile = (SFile.trim()).length()==0?"d:/Security/VisitorHourList.prn":SFile;

          VisitorHourInformation info  = new VisitorHourInformation();

          VisitorHourList theList    = new VisitorHourList(SStDate,SEnDate,SFile);
          Vector vect            = new Vector();  
          vect                   = theList.getPrintValues();

          Vector VInfo  = new Vector();
          Vector VValue = new Vector();

          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");

          VValue.addElement("Visitor Hour's (WednesDay) Visitor List as on a Particular Date");
          VValue.addElement(" ");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());


          info.flashMessage(VInfo,VValue,vect,out);

          
                             
          out.close();
     }

}
