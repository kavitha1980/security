/*
     The God Frame contains
     All Frames in use by the inventory reporting system

     see the following servlets forming the individual frames

          AmarLogo1
1          InvCrit
          DirectionPanel
          InvDisplay
*/

package Reports;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SecurityMainFrame extends HttpServlet
{
     HttpSession session;
     Vector VName,VCode;
     Common common = new Common(); 
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out= response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");

          //setVector();

          //session.putValue("VCode",VCode);
          //session.putValue("VName",VName);

          out.println("<html>");
          out.println("<frameset framespacing='0' border='0' rows='15%,*' frameborder='0'>");
          out.println(" <frame name='top'  target='contents' src='"+SServer+"Reports.AmarLogo1'>");
          out.println(" <frameset cols='150,*'  frameborder='0'>");
          out.println(" <frame name='contents' target='topmain' src='"+SServer+"Reports.DirectionPanel' scrolling='auto'>");
          out.println(" <frameset rows='*,55%'>");
          out.println(" <frame name='topmain' src='"+SServer+"Reports.SecCrit' target='main'  scrolling='no'>");
          out.println(" <frame name='main' src='"+SServer+"Reports.SecDisplay' target='_self'>");
          out.println(" </frameset>");           
          out.println(" </frameset>");
          out.println(" </frameset>");
          out.println(" <noframes>");
          out.println(" <body>");
          out.println(" <p>This page uses frames, but your browser doesn't support them.</p>");
          out.println(" </body>");
          out.println(" </noframes>");
          out.println("</frameset>");

          out.println("</html>");
          out.close();
     }
     
     public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

     }

     /*private void setVector()
     {
          VName = new Vector();
          VCode = new Vector();

          VName.addElement("< All Materials >");
          VCode.addElement("999999");

          try
          {
               InventoryConnection connect = InventoryConnection.getInventoryConnection();
               Connection conn       = connect.getConnection();
               Statement stat        = conn.createStatement();
               ResultSet result      = stat.executeQuery("Select Item_Name,Item_Code From InvItems Order By Item_Name");
               while(result.next())
               {
                    VName.addElement(common.parseNull(result.getString(1)));
                    VCode.addElement(common.parseNull(result.getString(2)));
               }
		result.close();
		stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }*/
}
