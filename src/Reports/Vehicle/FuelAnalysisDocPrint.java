package Reports.Vehicle;

import util.*;
import java.io.*;
import java.util.*;
public class FuelAnalysisDocPrint
{
      Vector VBody,VHead;
      String STitle,SFile;
      String RegNo,Name;
      int ipctr=0,ilctr=100,iLen=0;

      FileWriter FW;

      Common common = new Common();

      public FuelAnalysisDocPrint(Vector VBody,Vector VHead,String STitle,String SFile,String RegNo)
      {
            this.VBody  = VBody;
            this.VHead  = VHead;
            this.STitle = STitle;
            this.SFile  = SFile;
            this.RegNo  = RegNo;
            this.Name   = Name;

            if((SFile.trim()).length()==0)
               SFile = "FuelReport.prn";
            try
            {
                  iLen = ((String)VHead.elementAt(0)).length();
                  FW = new FileWriter(SFile);

                  int index=0;
                  for(int i=0;i<VBody.size();i++)
                  {
                        setHead();
                        String strl = (String)VBody.elementAt(i);
                        FW.write(strl);
                        setLine();
                        if(index==(VBody.size()-1))
                        {
                              setFoot1();
//                              String StrV = (String)VTotal.elementAt(1);
//                              String Strh = (String)VTotal.elementAt(0);
//                              FW.write(StrV);
                        }

                        ilctr++;
                        index=index+1; 

                  }

                  setFoot();
                  FW.close();
            }
            catch(Exception ex)
            {
                  System.out.println("From DocPrint"+ex);
            }
      }
      public void setHead()
      {
            if(ilctr < 63)
                  return;
            if(ipctr > 0)
                  setFoot();
            ipctr++;
            String str1 = "\t\t\t Amarjothi Spinning Mills Ltd\n\n";
            String str2 = "\t\t "+STitle+"\n";
            String str4 = "Page     : "+ipctr+"\n";
            String str3 = common.Replicate("-",iLen)+"\n";

            String str6 =  "VehicleRegNo:"+RegNo+ "\t\t\t" ;
            String str5 =  "VehicleName:"+Name+"\n\n";
            try
            {
                  FW.write(str1);
                  FW.write(str2);
                  FW.write(str6);
                  FW.write(str5);
                  FW.write(str4);
                  FW.write(str3);

                  for(int i=0;i<VHead.size();i++)
                        FW.write((String)VHead.elementAt(i));
                  FW.write(str3);
                  ilctr = VHead.size()+5;
            }
            catch(Exception ex)
            {
               System.out.println(ex);
            }
      }
      public void setFoot()
      {
            try
            {
                  String SStatus = (new java.util.Date()).toString();

                  String str3 = common.Replicate("-",iLen)+"\n";
                  str3        = str3+"Report Taken on :"+SStatus+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
      public void setFoot1()
      {
            try
            {
                  String str3 = common.Replicate("-",iLen)+"\n";

                  FW.write(str3);
            }
            catch(Exception ex){}
      }
      public void setLine()
      {
          try
          {
               String s1 = "\n";
               FW.write(s1);
          }
          
          catch(Exception e)
          {
               e.printStackTrace();
          }
      }
}
