package  Reports.Vehicle;
import util.*;

import java.io.*;
import java.util.Vector;

public class FuelConsumptionInfo
{

     FuelConsumptionInfo()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out,String SStDate,String SEnDate)
     {
     
          Common common  = new Common();
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead        = (Vector)VPrint.elementAt(0);

          Vector VSNo         = (Vector)VPrint.elementAt(1);
          Vector VDiNo        = (Vector)VPrint.elementAt(2);
          Vector VKiNo        = (Vector)VPrint.elementAt(3);
          Vector VDiDate      = (Vector)VPrint.elementAt(4);
          Vector VKiDate      = (Vector)VPrint.elementAt(5);
          Vector VDriver      = (Vector)VPrint.elementAt(6);
          Vector VSecurity    = (Vector)VPrint.elementAt(7);
          Vector VDQty        = (Vector)VPrint.elementAt(8);
          Vector VKQty        = (Vector)VPrint.elementAt(9);
          Vector VStKm        = (Vector)VPrint.elementAt(10);
          Vector VEndKm       = (Vector)VPrint.elementAt(11);
          Vector VTotalKm     = (Vector)VPrint.elementAt(12);
          Vector VAverage     = (Vector)VPrint.elementAt(13);
          Vector VTank        = (Vector)VPrint.elementAt(14);
          Vector VRemarks     = (Vector)VPrint.elementAt(15);

          /*out.println(" <table border='1' width='1800' height='20' cellpadding='0' cellspacing='0'   >");
          out.println("       <tr>"); 
          for(int i=0;i<VHead.size();i++)
          {
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VHead.elementAt(i)+"</td>");
          }
          out.println(" </tr>");
          out.println("</table>");*/

          out.println(" <table border='1' width='1500' height='20' cellpadding='0' cellspacing='0'   >");
          int m=0;
          int iDTotal=0,iKTotal=0,iKmTotal=0;

          int iRowSize   = VStKm.size()-1;

          //for(int i=0;i<(VStKm.size())-1;i++)

          for(int i=0;i<VStKm.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<VHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+(String)VHead.elementAt(j)+"</td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                                                                 
                    out.println(" <tr>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDriver.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSecurity.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VStKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VEndKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VTotalKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VAverage.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VTank.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VRemarks.elementAt(i)+"</td>");

                    String SDQty        = (String)VDQty.elementAt(i);
                    iDTotal             = iDTotal+Integer.parseInt(SDQty);
                    String SKQty        = (String)VKQty.elementAt(i);
                    if(((String)VTotalKm.elementAt(i)).equals("0"))
                    {
                        SKQty           = "0";
                    }
                    iKTotal             = iKTotal+Integer.parseInt(SKQty);
                    String SKmTotal     = (String)VTotalKm.elementAt(i);
                    iKmTotal            = iKmTotal+Integer.parseInt(SKmTotal);

                    out.println(" </tr>");
                    
               }
          
          //System.out.println("Size:"+VDQty.elementAt(iRowSize));
          out.println("</table>");

          out.println(" <table border='1' width='300' height='20' cellpadding='0' cellspacing='0'   >");
          out.println("<tr>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >From</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >To</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >Diesel</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >Kero</td>");
                    
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >TotalKms Travelled</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >Avg.KM /Litre</td>");

          out.println("</tr>");

          int    iFTotal         = iKTotal+iDTotal;
          double iAvg            = (common.toDouble(String.valueOf(iKmTotal)))/(common.toDouble(String.valueOf(iFTotal)));
          String SAvg            = common.getRound(iAvg,3);  
          
          out.println("<tr>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+SStDate+"</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+SEnDate+"</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+iDTotal+"</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+iKTotal+"</td>");
                    
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+iKmTotal+"</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+SAvg+"</td>");

          out.println("</tr>");

          out.println("</table>");

          out.println("</body>");
          out.println("</html>");
     }

}
