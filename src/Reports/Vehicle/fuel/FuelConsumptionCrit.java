package Reports.Vehicle.fuel;

import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class FuelConsumptionCrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     VehicleInfo vDomain;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               vDomain             = (VehicleInfo)registry.lookup(SECURITYDOMAIN);

               response.setContentType("text/html");
               PrintWriter out = response.getWriter();
               session         = request.getSession(false);
               String SServer  = (String)session.getValue("Server");
     
     
               out.println("<html>");
               out.println("<body bgcolor='#9AA8D6'>");
               out.println("");
               out.println("<base target='main'>");
               out.println("<form method='GET' action='"+SServer+"Reports.Vehicle.fuel.FuelConsumptionRep'>");
     
               
               Vector VNo = vDomain.getVehicleData();
     
               out.println("<table>");
               out.println("<tr>");
               out.println("     <td><b>Vehicle</b></td>");
               out.println("     <td><b>From </b></td>");
               out.println("     <td><b>To   </b></td>");
               out.println("     <td><b>File</b></td>");
               out.println("     <td></td>");
               out.println("    <td></td>");
               out.println("<tr>");
     
               out.println("<tr>");

               out.println("     <td>");
               out.println("<Select name='s1'>");

               int m=0;

               Vector VTemp = new Vector();

               for(int j=0;j<(VNo.size())/2;j++)
               {
                    VTemp.addElement(VNo.elementAt(1+m));
                    out.println("<option value="+j+">"+VTemp.elementAt(j)+"</option>");
                    m=m+2;
               }
               out.println("</select>");

               out.println("     <td><input type='text' name='TStDate' size='15'></td>");
               out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
               out.println("     <td><input type='text' name='TFile' size='15'></td>");
               out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
               out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
               out.println("<tr>");
     
               out.println("</table>");
     
               out.println("</form>");
               out.println("</body>");
               out.println("</html>");
     
               out.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }
}

