package Reports.Vehicle;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;
import util.*;
import rndi.*;


public class FuelMonthRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;
     VehicleInfo vDomain;

     Common common;
     int  iMonthCode;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }

     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               vDomain             = (VehicleInfo)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }                              
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer    = (String)session.getValue("Server");

          int    iMonthCode = common.toInt(request.getParameter("C1"));
          String SVehicleCode = request.getParameter("s1");

          int iIndex          = Integer.parseInt(SVehicleCode);
          Vector VNo = vDomain.getVehicleData();

          Vector VTemp = new Vector();
          int m=0;

          for(int j=0;j<(VNo.size())/2;j++)
          {
               VTemp.addElement(VNo.elementAt(0+m));
               //out.println("<option value="+j+">"+VTemp.elementAt(j)+"</option>");
               m=m+2;
          }

          SVehicleCode     = (String) VTemp.elementAt(iIndex);
          System.out.println("Name:"+SVehicleCode);

          String SFile     = common.parseNull(request.getParameter("TFile"));

          SFile = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/FuelMonthwise.prn":SFile;

          //FuelMonthInfo info  = new FuelMonthInfo();
          FuelInformation3 info     = new FuelInformation3();

          //FuelMonthWiseList theList    = new FuelMonthWiseList(SSVehicleCode,iMonthCode,SFile);
          VehicleFuelRegisterList3 theList    = new VehicleFuelRegisterList3(SVehicleCode,iMonthCode,SFile,1);

          Vector vect            = new Vector();
          vect                   = theList.getPrintValues();
          Vector VTotal          = theList.getTotalValues();  

          Vector VInfo  = new Vector();
          Vector VValue = new Vector();

          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");

          VValue.addElement("Vehicle's  List as on a Particular Date");
          VValue.addElement(" ");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());

          String SMonthName        = common.getMonthName(iMonthCode);
          info.flashMessage(VInfo,VValue,vect,out,SMonthName,SMonthName,VTotal);
                                                                       
          //info.flashMessage(VInfo,VValue,vect,out);

          out.close();
     }
     
}
