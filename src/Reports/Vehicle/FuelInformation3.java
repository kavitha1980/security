package  Reports.Vehicle;
import util.*;

import java.io.*;
import java.util.Vector;

public class FuelInformation3
{
     FuelInformation3()
     {
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out,String SStDate,String SEnDate,Vector VTotal)
     {
          try
          {

          Common common  = new Common();
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead        = (Vector)VPrint.elementAt(0);

          Vector VSNo         = (Vector)VPrint.elementAt(1);
          Vector VDiNo        = (Vector)VPrint.elementAt(2);
          Vector VKiNo        = (Vector)VPrint.elementAt(3);
          Vector VDiDate      = (Vector)VPrint.elementAt(4);
          Vector VKiDate      = (Vector)VPrint.elementAt(5);
          Vector VDriver      = (Vector)VPrint.elementAt(6);
          Vector VSecurity    = (Vector)VPrint.elementAt(7);
          Vector VDQty        = (Vector)VPrint.elementAt(8);
          Vector VKQty        = (Vector)VPrint.elementAt(9);
          Vector VStKm        = (Vector)VPrint.elementAt(10);
          Vector VEndKm       = (Vector)VPrint.elementAt(11);
          Vector VTotalKm     = (Vector)VPrint.elementAt(12);
          Vector VAverage     = (Vector)VPrint.elementAt(13);
          Vector VTank        = (Vector)VPrint.elementAt(14);
          Vector VRemarks     = (Vector)VPrint.elementAt(15);

          out.println(" <table border='1' width='1500' height='20' cellpadding='0' cellspacing='0'   >");
          int m=0;
          int iDTotal=0,iKTotal=0,iKmTotal=0;

          int iRowSize   = VStKm.size()-1;

          //for(int i=0;i<(VStKm.size())-1;i++)


          for(int i=0;i<VStKm.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<VHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='40' bgcolor='#9aadff' ><b>"+(String)VHead.elementAt(j)+"</b></td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                                                                 
                    out.println(" <tr>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VSNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VDiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VKiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDriver.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSecurity.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VDQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VKQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VStKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VEndKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VTotalKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' align='right'>"+(String)VAverage.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VTank.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VRemarks.elementAt(i)+"</td>");

                    out.println(" </tr>");

               
               }
          out.println("</table>");

          out.println(" <table border='1' width='600' height='20'  cellpadding='0' cellspacing='0'   >");
          out.println("<tr>");

          out.println("            <td =width='150' height='40' bgcolor='#9baaff' >From</td>");
          out.println("            <td =width='150' height='40' bgcolor='#9baaff' >To</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >DieselConsumed</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >Kero Consumed</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >TotalKms Travelled</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >Avg.KM /Litre</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >ExpectedKm/Litre</td>");
          out.println("            <td =width='250' height='40' bgcolor='#9baaff' >Variation in Km/Litre</td>");

          out.println("</tr>");

          String SDTotal           = (String)VTotal.elementAt(0);
          String SKTotal           = (String)VTotal.elementAt(1);
          String SKmTotal          = (String)VTotal.elementAt(2);
          String SAvg              = (String)VTotal.elementAt(3);
          String SExp              = (String)VTotal.elementAt(4);
          String SVar              = (String)VTotal.elementAt(5);

          SAvg                     = common.getRound(SAvg,3);  
          SExp                     = common.getRound(SExp,3);  
          SVar                     = common.getRound(SVar,3);  
                    
          out.println("<tr>");
          out.println("            <td =width='150' height='40' bgcolor='#9bddff' ><b>"+SStDate+"</b></td>");
          out.println("            <td =width='150' height='40' bgcolor='#9bddff' ><b>"+SEnDate+"</b></td>");
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SDTotal+"</b></td>");
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SKTotal+"</b></td>");
                    
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SKmTotal+"</b></td>");
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SAvg+"</b></td>");
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SExp+"</b></td>");
          out.println("            <td =width='250' height='40' bgcolor='#9bddff' align='right'><b>"+SVar+"</b></td>");

          out.println("</tr>");

          out.println("</table>");

          out.println("</body>");
          out.println("</html>");
     }
     catch(Exception e)
     { e.printStackTrace();}
     }
}
