
/*
     A general information notifier
     about a process status to the user. 
*/

package Reports.Vehicle;
import util.*;

import java.io.*;
import java.util.Vector;

public class MonthAnalysisInfo
{

     MonthAnalysisInfo()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
     
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");


          Vector VHead        = (Vector)VPrint.elementAt(0);
          Vector VSNo         = (Vector)VPrint.elementAt(1);
          Vector VRegNo       = (Vector)VPrint.elementAt(2);
          Vector VName        = (Vector)VPrint.elementAt(3);
          Vector VOutDate     = (Vector)VPrint.elementAt(4);
          Vector VOutTime     = (Vector)VPrint.elementAt(5);
          Vector VInDate      = (Vector)VPrint.elementAt(6);
          Vector VInTime      = (Vector)VPrint.elementAt(7);
/*          Vector VStKm        = (Vector)VPrint.elementAt(8);
          Vector VEndKm       = (Vector)VPrint.elementAt(9);
          Vector VTotalKm     = (Vector)VPrint.elementAt(10);
          Vector VPlace       = (Vector)VPrint.elementAt(11);
          Vector VPurpose     = (Vector)VPrint.elementAt(12);
          Vector VDriver      = (Vector)VPrint.elementAt(13);
          Vector VSecurity    = (Vector)VPrint.elementAt(14);
          Vector VDiNo        = (Vector)VPrint.elementAt(15);
          Vector VKiNo        = (Vector)VPrint.elementAt(16);
          Vector VDiDate      = (Vector)VPrint.elementAt(17);
          Vector VKiDate      = (Vector)VPrint.elementAt(18);
          Vector VDQty        = (Vector)VPrint.elementAt(19);
          Vector VKQty        = (Vector)VPrint.elementAt(20);
*/          Vector VBunk        = (Vector)VPrint.elementAt(21);

          /*out.println(" <table border='1' width='1800' height='20' cellpadding='0' cellspacing='0'   >");
          out.println("       <tr>"); 
          for(int i=0;i<VHead.size();i++)
          {
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VHead.elementAt(i)+"</td>");
          }
          out.println(" </tr>");
          out.println("</table>");*/

          out.println(" <table border='1' width='3000' height='20' cellpadding='0' cellspacing='0'   >");
          int m=0;
          for(int i=0;i<VOutDate.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<VHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+(String)VHead.elementAt(j)+"</td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                    out.println(" <tr>");

                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSNo.elementAt(i)+"</td>");
                    //out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VRegNo.elementAt(i)+"</td>");
                    //out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VName.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOutDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VOutTime.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInTime.elementAt(i)+"</td>");
/*                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VStKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VEndKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VTotalKm.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VPlace.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VPurpose.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDriver.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSecurity.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKiDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VKQty.elementAt(i)+"</td>");
*/                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VBunk.elementAt(i)+"</td>");

                    out.println(" </tr>");                                                                                         
                    
               }
          
          out.println("</table>");


          out.println("</body>");
          out.println("</html>");
     }

}
