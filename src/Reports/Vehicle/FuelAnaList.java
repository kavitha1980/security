package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class FuelAnaList
{

     protected String SSelected;
     protected int iMonthCode1,iMonthCode2;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;
     int index=1;


     Vector VSno,VVehicleNo,VVehicleName,VApril,VMay,VJune,VJuly,VAugust,VSeptember;
     Vector VOctober,VNovember,VDecember,VJanuary,VFebruary,VMarch,VInitialKm;
     Vector VClosingKm,VRunningKm,VDieselConsumed,VAverageKm,VStdKm;
     Vector VVariationKm,VExcessDiesel;
     Vector VHead,VSAdd;
     Common common = new Common();

     Vector VHead2      = new Vector(); 

     int iMonth[] = new int[12]; 

     FuelAnaList(String SSelected,int iMonthCode1,int iMonthCode2,String SFile)
     {
            this.SSelected    = SSelected; 
            this.iMonthCode1  = iMonthCode1;
            this.iMonthCode2  = iMonthCode2;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setFuelList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setFuelList()
     {
           try
           {
                 setDataIntoVector();
                 System.out.println("Month1: "+iMonthCode1);
                 System.out.println("Month2: "+iMonthCode2);
                 System.out.println("VName: "+SSelected);

                 String STitle = " Fuel Consumption  List For The Month : "+common.getMonthName(iMonthCode1)+"And"+common.getMonthName(iMonthCode2)+"\n";
                 Vector VHead  = getVehicleHead();
                 iLen          = ((String)VHead.elementAt(0)).length();
                 Strline       = common.Replicate("-",iLen)+"\n";
                 Vector VBody  = getVehicleListBody();
                 String SName  = (String)VVehicleNo.elementAt(0);
//                 new FuelAnalysisDocPrint(VBody,VHead,STitle,SFile,SSelected,SName);
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }

      public Vector getVehicleHead()
      {
           String SHead[] = { "S.NO","VEHICLE.NO"};
           VSAdd = new Vector();
           Vector vect  = new Vector();
           String SHead1  = ((String)SHead[0]).trim();
           String SHead2  = ((String)SHead[1]).trim();

           VSAdd.addElement(SHead1);
           VSAdd.addElement(SHead2);

           SHead1   = common.Pad(SHead1,5)+"|"+SInt;
           SHead2   = common.Pad(SHead2,8)+"|"+SInt;

           String SAdd = SHead1+SHead2;

           vect.add(SAdd);
           return VSAdd;
      }               

/*           Vector VStore = new Vector();
           //               {    1  ,      2     ,      3       ,  4    ,  5  ,  6   ,   7  ,    8   ,      9    ,    10   ,     11   ,     12   ,    13   ,    14    ,   15  ,     16     ,      17     ,       18    ,       19        ,     20     ,   21   ,         22      ,            23           };
           //String SHead[] = { "S.NO","VEHICLE.NO","VEHICLE NAME","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER","JANUARY","FEBRUARY","MARCH","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","AVERAGE/KM","STD KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };
           String SHead[] = { "S.NO","VEHICLE.NO"};//,"VEHICLE NAME","INITIAL KM","CLOSING K.M","RUNNING K.M","DIESEL CONSUMED","AVERAGE/KM","STD KM","VARIATION IN KM","EXCESS DIESEL CONSUMED" };

           String SMHead[]    = new String[index];
           String SMonths[]= new String[index];
           for(int i=0;i<SMonths.length;i++)
           {
               SMonths[i]= common.getMonthName(iMonth[i]);
           }    
           String SHead1  = ((String)SHead[0]).trim();
           String SHead2  = ((String)SHead[1]).trim();
           String SHead3  = ((String)SHead[2]).trim();
           String SHead4  = ((String)SHead[3]).trim();


           String SHead5  = ((String)SHead[4]).trim();
           String SHead6  = ((String)SHead[5]).trim();
           String SHead7  = ((String)SHead[6]).trim();
           String SHead8  = ((String)SHead[7]).trim();
           String SHead9  = ((String)SHead[8]).trim();
           String SHead10 = ((String)SHead[9]).trim();
           String SHead11 = ((String)SHead[10]).trim();   */
           /*String SHead12 = ((String)SHead[11]).trim();
           String SHead13 = ((String)SHead[12]).trim();
           String SHead14 = ((String)SHead[13]).trim();
           String SHead15 = common.parseNull((String)SHead[14]).trim();
           String SHead16 = common.parseNull((String)SHead[15]).trim();
           String SHead17 = common.parseNull((String)SHead[16]).trim();
           String SHead18 = common.parseNull((String)SHead[17]).trim();
           String SHead19 = common.parseNull((String)SHead[18]).trim();
           String SHead20 = common.parseNull((String)SHead[19]).trim();
           String SHead21 = common.parseNull((String)SHead[20]).trim();
           String SHead22 = common.parseNull((String)SHead[21]).trim();
           String SHead23 = common.parseNull((String)SHead[22]).trim();*/
/*
           VHead  = new Vector();

           for(int i=0;i<SMonths.length;i++)
           {
               SMHead[i] = SMonths[i];
           }


           for(int i=0;i<SMonths.length;i++)
           {
               SMHead[i] = common.Pad(SMHead[i],5)+"�"+SInt;

           }      */

           /*VHead.addElement(SHead12);
           VHead.addElement(SHead13);
           VHead.addElement(SHead14);
           VHead.addElement(SHead15);
           VHead.addElement(SHead16);
           VHead.addElement(SHead17);
           VHead.addElement(SHead18);
           VHead.addElement(SHead19);
           VHead.addElement(SHead20);
           VHead.addElement(SHead21);
           VHead.addElement(SHead22);
           VHead.addElement(SHead23);*/

/*           SHead1   = common.Pad(SHead1,5)+"|"+SInt;
           SHead2   = common.Pad(SHead2,8)+"|"+SInt;
           SHead3   = common.Pad(SHead3,8)+"|"+SInt;
           SHead4   = common.Pad(SHead4,15)+"|"+SInt;
           SHead5   = common.Pad(SHead5,15)+"|"+SInt;
           SHead6   = common.Pad(SHead6,15)+"|"+SInt;
           SHead7   = common.Pad(SHead7,20)+"|"+SInt;
           SHead8   = common.Rad(SHead8,5)+common.Space(5)+"|"+SInt;
           SHead9   = common.Rad(SHead9,6)+common.Space(5)+"|"+SInt;
           SHead10  = common.Pad(SHead10,10)+"|"+SInt;
           SHead11  = common.Pad(SHead11,10)+"|"+SInt;
*/
           /*SHead12  = common.Pad(SHead12,5)+"|"+SInt;
           SHead13  = common.Pad(SHead13,10)+"|"+SInt;
           SHead14  = common.Pad(SHead14,8)+"|"+SInt;
           SHead15  = common.Pad(SHead15,15)+"|"+SInt;
           SHead16  = common.Pad(SHead10,10)+"|"+SInt;
           SHead17  = common.Pad(SHead11,10)+"|"+SInt;
           SHead18  = common.Pad(SHead12,5)+"|"+SInt;
           SHead19  = common.Pad(SHead13,10)+"|"+SInt;
           SHead20  = common.Pad(SHead14,8)+"|"+SInt;
           SHead21  = common.Pad(SHead21,5)+"|"+SInt;
           SHead22  = common.Pad(SHead22,8)+"|"+SInt;
           SHead23  = common.Pad(SHead23,8)+"|"+SInt;*/
/*
           

           VHead.addElement(SHead1);
           VHead.addElement(SHead2);
           VHead.addElement(SHead3);
           VHead.addElement(SHead4);
           VHead.addElement(SHead5);
           VHead.addElement(SHead6);
           VHead.addElement(SHead7);
           VHead.addElement(SHead8);
           VHead.addElement(SHead9);
           VHead.addElement(SHead10);
           VHead.addElement(SHead11);

           for(int i=0;i<SMonths.length;i++)
           {
               VHead.addElement(SMHead[i]); 

           }



           //String STHead = SHead1+SHead2+SHead3+SHead4+SHead5+SHead6+SHead7+SHead8+SHead9+SHead10+SHead11+SHead12+SHead13+SHead14+SHead15+SHead16+SHead17+SHead18+SHead19+SHead20+SHead21+SHead22+SHead23+"\n";
           String STHead = SHead1+SHead2+SHead3+SHead4+SHead5+SHead6+SHead7+SHead8+SHead9+SHead10+SHead11+"\n";
           for(int i=0;i<SMonths.length;i++)
           {
               STHead            = STHead+SMHead[i];  
           }

           System.out.println("Head:"+STHead);
           VStore.add(STHead);

           return VStore;
     }
     */
     public Vector getVehicleListBody()
     {
          Vector vect = new Vector();
          for(int i=0;i<VVehicleNo.size();i++)
          {
               String SVh1  = (String)VSno.elementAt(i);
               String SVh2  = (String)VVehicleNo.elementAt(i);

               SVh1    = common.Pad(SVh1,5)+"|"+SInt;
               SVh2    = common.Pad(SVh2,8)+"|";

               String SVLAdd = SVh1+SVh2+"\n";

               vect.add(SVLAdd);
          }
          return vect;
     }
     public void setDataIntoVector()
     {
           VSno          = new Vector();
           VVehicleNo    = new Vector();
/*           VVehicleName  = new Vector();
           VInitialKm    = new Vector();
           VClosingKm    = new Vector();
           VRunningKm    = new Vector();
           VDieselConsumed= new Vector();
           VAverageKm    = new Vector();
           VStdKm        = new Vector();          
           VVariationKm  = new Vector();
           VExcessDiesel = new Vector();
*/
           try
           {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection con = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
               Statement stat = con.createStatement();
               ResultSet res = stat.executeQuery(getQString());
               int sno=1;
               while(res.next())
               {
                    String SVehicleNo = res.getString(1);

                    VVehicleNo.addElement(SVehicleNo);
                    VSno.addElement(String.valueOf(sno));
                    sno=sno+1;
               }
           }
           catch(Exception e)
           {

           }
     }
     public Vector getPrintValues()
     {
           Vector VStore = new Vector();
           VStore.addElement(VSAdd);
           VStore.addElement(VSno);
           VStore.addElement(VVehicleNo);
/*           VStore.addElement(VVehicleName);
           VStore.addElement(VInitialKm);
           VStore.addElement(VClosingKm);
           VStore.addElement(VRunningKm);
           VStore.addElement(VDieselConsumed);
           VStore.addElement(VAverageKm);
           VStore.addElement(VStdKm);
           VStore.addElement(VVariationKm);
           VStore.addElement(VExcessDiesel);
*/
           return VStore;
     }
                                   

     public String getQString()
     {
           String QString    = "Select VehicleNo from Vehicles "+
                               " where Substr(OutDate,1,6) >= "+iMonthCode1+" And Substr(OutDate,1,6) <= "+iMonthCode2+" and Status=1 AND  VehicleName = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 2 ";
           return QString;

     }
     public String getStatus()
     {
           return SStatus;
     }

}


/*

           VApril        = new Vector();
           VMay          = new Vector();
           VJune         = new Vector();
           VJuly         = new Vector();
           VAugust       = new Vector();
           VSeptember    = new Vector();
           VOctober      = new Vector();
           VNovember      = new Vector();
           VDecember     = new Vector();
           VJanuary      = new Vector();
           VFebruary     = new Vector();
           VMarch        = new Vector();

           VStore.addElement(VApril);
           VStore.addElement(VMay);
           VStore.addElement(VJune);
           VStore.addElement(VJuly);
           VStore.addElement(VAugust);
           VStore.addElement(VSeptember);
           VStore.addElement(VOctober);
           VStore.addElement(VNovember);
           VStore.addElement(VDecember);
           VStore.addElement(VJanuary);
           VStore.addElement(VFebruary);
           VStore.addElement(VMarch);



*/
