package Reports.Vehicle;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class VehicleFuelRegisterList2
{

     protected String SSelected;
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VSNo,VName,VOutDate,VOutTime,VInTime,VStKm,VEndKm,VTotalKm,VDriver,VSecurity,VDiNo,VKiNo,VDiDate,VKiDate,VDQty,VKQty,VBunk,VClosing,VAverage,VRemarks,VTank;
     Vector Vhead,VFuelKm;
     Common common = new Common();


     String SExpectedKm = "",SActualKm=" ";
     String SStatus = "";
     Vector VTotal;

     Vector VDTotal     = new Vector();
     Vector VKTotal     = new Vector();
     Vector VKmTotal    = new Vector();
     Vector VHead2      = new Vector(); 

     VehicleFuelRegisterList2(String SSelected,String SStDate,String SEnDate,String SFile)
     {
            System.out.println("Inside Selected2");
            this.SStDate      = SStDate;
            this.SSelected    = SSelected; 
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInwardList()
     {
            try
            {

                 VTotal     = new Vector();
                 setDataIntoVector();
                 String STitle = " Fuel Consumption  List From "+common.parseDate(SStDate)+"  To "+common.parseDate(SEnDate)+" \n";
     
                 Vector VHead  = getVehicleHead();
                 getVehicleHTotal();
     
                 iLen = ((String)VHead.elementAt(0)).length();
                 Strline = common.Replicate("-",iLen)+"\n";
                 Vector VBody  = getVehicleListBody();
                 String SName      = (String)VName.elementAt(0);
                 new FuelDocPrint(VBody,VHead,VTotal,STitle,SFile,SSelected,SName,SExpectedKm,SActualKm);
            }
            catch(Exception e)
            {
               e.printStackTrace();
            }
      }
      public void getVehicleHTotal()
      {
           Vector vThead = new Vector();

           String Head1[]={"Total","  ","  " };
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();


           Sha1  = common.Pad(Sha1,30);
           Sha2  = common.Pad(Sha2,30)+SInt;
           Sha3  = common.Pad(Sha3,20)+SInt;

           String Strh2 = Sha1;
           VTotal.addElement(Strh2);
      }

      public Vector getVehicleHead()
      {
           Vector vect = new Vector();

           String SHead[]={"SNo","D.I.no","K.In.No","D.I.Date","K.I.Date","DriverName","Security","Dies.Qty","Kero.Qty","Init.Reading","Clos.Reading","kms Reunned","AvgKms","TankFilled","Remarks"};
           String Head1[]={"SlNo",  " Fuel Inside " ," Fuel OutSide"  ,"  ","      "  , "Driver","Security"," "," " ,"Initial" ,"Closing", " Kms"    ,"AvgKms" ,"TankFilled","Remarks"};
           String Head2[]={" "   ,  " Indent", "Indent","Indent","Indent", " Name" ," Name   "," in  " ," in " ,"Reading" ,"Reading", " Runned" ,"  Per " ,"Fully/Part", "   "   };
           String Head3[]={" "   ,  "   No  ", " No "  ,"Date  "," Date" ,  "    " , "       "," Ltrs" ,"Ltrs ", "(kms)"  ,"(kms)  ", "       " ," Ltrs " ,"          ", "   "   };        


           String Sa1=((String)SHead[0]).trim();
           String Sa2=((String)SHead[1]).trim();
           String Sa3=((String)SHead[2]).trim();
           String Sa4=((String)SHead[3]).trim();
           String Sa5=((String)SHead[4]).trim();
           String Sa6=((String)SHead[5]).trim();
           String Sa7=common.parseNull((String)SHead[6]);
           String Sa8=common.parseNull((String)SHead[7]);
           String Sa9=((String)SHead[8]).trim();
           String Sa10=((String)SHead[9]).trim();
           String Sa11=((String)SHead[10]).trim();
           String Sa12=((String)SHead[11]).trim();
           String Sa13=((String)SHead[12]).trim();
           String Sa14=((String)SHead[13]).trim();
           String Sa15=((String)SHead[14]).trim();


           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=((String)Head1[8]).trim();
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();
           String Sha15=((String)Head1[14]).trim();


           String Sha21=((String)Head2[0]).trim();
           String Sha22=((String)Head2[1]).trim();
           String Sha23=((String)Head2[2]).trim();
           String Sha24=((String)Head2[3]).trim();
           String Sha25=((String)Head2[4]).trim();
           String Sha26=((String)Head2[5]).trim();
           String Sha27=common.parseNull((String)Head2[6]);
           String Sha28=common.parseNull((String)Head2[7]);
           String Sha29=((String)Head2[8]).trim();
           String Sha210=((String)Head2[9]).trim();
           String Sha211=((String)Head2[10]).trim();
           String Sha212=((String)Head2[11]).trim();
           String Sha213=((String)Head2[12]).trim();
           String Sha214=((String)Head2[13]).trim();
           String Sha215=((String)Head2[14]).trim();

           String Sha31=((String)Head3[0]).trim();
           String Sha32=((String)Head3[1]).trim();
           String Sha33=((String)Head3[2]).trim();
           String Sha34=((String)Head3[3]).trim();
           String Sha35=((String)Head3[4]).trim();
           String Sha36=((String)Head3[5]).trim();
           String Sha37=common.parseNull((String)Head3[6]);
           String Sha38=common.parseNull((String)Head3[7]);
           String Sha39=((String)Head3[8]).trim();
           String Sha310=((String)Head3[9]).trim();
           String Sha311=((String)Head3[10]).trim();
           String Sha312=((String)Head3[11]).trim();
           String Sha313=((String)Head3[12]).trim();
           String Sha314=((String)Head3[13]).trim();
           String Sha315=((String)Head3[14]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);
           Vhead.addElement(Sha15);

           VHead2.addElement(Sa1);
           VHead2.addElement(Sa2);
           VHead2.addElement(Sa3);
           VHead2.addElement(Sa4);
           VHead2.addElement(Sa5);
           VHead2.addElement(Sa6);
           VHead2.addElement(Sa7);
           VHead2.addElement(Sa8);
           VHead2.addElement(Sa9);
           VHead2.addElement(Sa10);
           VHead2.addElement(Sa11);
           VHead2.addElement(Sa12);
           VHead2.addElement(Sa13);
           VHead2.addElement(Sa14);
           VHead2.addElement(Sa15);

           Sha1  = common.Pad(Sha1,5)+"|"+SInt;
           Sha2  = common.Pad(Sha2,13)+" "+SInt;
           Sha3  = common.Pad(Sha3,14)+" "+SInt;
           Sha4  = common.Pad(Sha4,15)+"|"+SInt;
           Sha5  = common.Pad(Sha5,15)+"|"+SInt;
           Sha6  = common.Pad(Sha6,15)+"|"+SInt;
           Sha7  = common.Pad(Sha7,20)+"|"+SInt;
           Sha8  = common.Rad(Sha8,0)+common.Space(5)+"|"+SInt;
           Sha9  = common.Rad(Sha9,0)+common.Space(5)+"|"+SInt;
           Sha10  = common.Pad(Sha10,10)+"|"+SInt;
           Sha11  = common.Pad(Sha11,10)+"|"+SInt;
           Sha12  = common.Pad(Sha12,5)+"|"+SInt;
           Sha13  = common.Pad(Sha13,10)+"|"+SInt;
           Sha14  = common.Pad(Sha14,8)+"|"+SInt;
           Sha15  = common.Pad(Sha15,15)+"|"+SInt;

           Sha21  = common.Pad(Sha21,5)+"|"+SInt;
           Sha22  = common.Pad(Sha22,8)+" "+SInt;
           Sha23  = common.Pad(Sha23,8)+" "+SInt;
           Sha24  = common.Pad(Sha24,15)+"|"+SInt;
           Sha25  = common.Pad(Sha25,15)+"|"+SInt;
           Sha26  = common.Pad(Sha26,15)+"|"+SInt;
           Sha27  = common.Pad(Sha27,20)+"|"+SInt;
           Sha28  = common.Rad(Sha28,5)+common.Space(5)+"|"+SInt;
           Sha29  = common.Rad(Sha29,6)+common.Space(5)+"|"+SInt;
           Sha210  = common.Pad(Sha210,10)+"|"+SInt;
           Sha211  = common.Pad(Sha211,10)+"|"+SInt;
           Sha212  = common.Pad(Sha212,5)+"|"+SInt;
           Sha213  = common.Pad(Sha213,10)+"|"+SInt;
           Sha214  = common.Pad(Sha214,8)+"|"+SInt;
           Sha215  = common.Pad(Sha215,15)+"|"+SInt;

           Sha31  = common.Pad(Sha31,5)+"|"+SInt;
           Sha32  = common.Pad(Sha32,8)+" "+SInt;
           Sha33  = common.Pad(Sha33,8)+" "+SInt;
           Sha34  = common.Pad(Sha34,15)+"|"+SInt;
           Sha35  = common.Pad(Sha35,15)+"|"+SInt;
           Sha36  = common.Pad(Sha36,15)+"|"+SInt;
           Sha37  = common.Pad(Sha37,20)+"|"+SInt;
           Sha38  = common.Rad(Sha38,5)+common.Space(5)+"|"+SInt;
           Sha39  = common.Rad(Sha39,6)+common.Space(5)+"|"+SInt;
           Sha310  = common.Pad(Sha310,10)+"|"+SInt;
           Sha311  = common.Pad(Sha311,10)+"|"+SInt;
           Sha312  = common.Pad(Sha312,5)+"|"+SInt;
           Sha313  = common.Pad(Sha313,10)+"|"+SInt;
           Sha314  = common.Pad(Sha314,8)+"|"+SInt;
           Sha315  = common.Pad(Sha315,15)+"|"+SInt;


           /*String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
           String Strh2 = Sha21+Sha22+Sha23+Sha24+Sha25+Sha26+Sha27+Sha28+Sha29+Sha210+Sha211+Sha212+Sha213+Sha214+Sha215+"\n";
           String Strh3 = Sha31+Sha32+Sha33+Sha34+Sha35+Sha36+Sha37+Sha38+Sha39+Sha310+Sha311+Sha312+Sha313+Sha314+Sha315+"\n";
           String Strh4 = Sa1+Sa2+Sa3+Sa4+Sa5+Sa6+Sa7+Sa8+Sa9+Sa10+Sa11+Sa12+Sa13+Sa14+Sa15+"\n";*/

           String Strh1 = Sha1+Sha5+Sha6+Sha7+Sha2+Sha8+Sha3+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+Sha15+"\n";
           String Strh2 = Sha21+Sha25+Sha26+Sha27+Sha22+Sha28+Sha23+Sha29+Sha210+Sha211+Sha212+Sha213+Sha214+Sha215+"\n";
           String Strh3 = Sha31+Sha35+Sha36+Sha37+Sha32+Sha38+Sha33+Sha39+Sha310+Sha311+Sha312+Sha313+Sha314+Sha315+"\n";
           String Strh4 = Sa1+Sa5+Sa6+Sa7+Sa2+Sa8+Sa3+Sa9+Sa10+Sa11+Sa12+Sa13+Sa14+Sa15+"\n";


           vect.add(Strh1);
           vect.add(Strh2);
           vect.add(Strh3);

           return vect;

     }
     public Vector getVehicleListBody()
     {
           Vector vect = new Vector();
           int iDTotal=0,iKTotal=0,iKmTotal=0;


           int index=0;

           String Sda11="",Sda17="",Sda18="",Sda19="",Sda20="",Sda21="";
           String Sda12="",Sda13="",Sda14="",Sda15="";

           int iVSize    = VFuelKm.size()-1;


           for(int i=0;i<iVSize;i++)
           
           {
                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDiNo.elementAt(i);
                 String Sda3  = (String)VKiNo.elementAt(i);
                 String Sda4  = (String)VDiDate.elementAt(i);
                 String Sda5  = (String)VKiDate.elementAt(i);
                 String Sda6  = (String)VDriver.elementAt(i);
                 String Sda7  = (String)VSecurity.elementAt(i);
                 String Sda8  = (String)VDQty.elementAt(i);
                 String Sda9  = (String)VKQty.elementAt(i);
                 String Sda10 = (String)VFuelKm.elementAt(i);
                 Sda11        = (String)VFuelKm.elementAt(i+1);
                 Sda12        = (String)VTotalKm.elementAt(i);
                 Sda13        = (String)VAverage.elementAt(i);
                 Sda14        = (String)VTank.elementAt(i);
                 Sda15        = (String)VRemarks.elementAt(i);
                 

                 String SDQty        = (String)VDQty.elementAt(i);
                 iDTotal             = iDTotal+Integer.parseInt(SDQty);
                 String SKQty        = (String)VKQty.elementAt(i);
                 iKTotal             = iKTotal+Integer.parseInt(SKQty);
                 String SKmTotal     = (String)VTotalKm.elementAt(i);
                 iKmTotal            = iKmTotal+Integer.parseInt(SKmTotal);


                 if(index==(iVSize-1))
                 {
              
                         VDTotal.addElement(String.valueOf(iDTotal));
                         VKTotal.addElement(String.valueOf(iKTotal));
                         VKmTotal.addElement(String.valueOf(iKmTotal));

                         Sda17          = (String)VDTotal.elementAt(0);
                         Sda18          = (String)VKTotal.elementAt(0);
                         Sda19          = (String)VKmTotal.elementAt(0);
                         Sda20          = " ";

                         double iFTotal    = common.toDouble(String.valueOf((Integer.parseInt(Sda17)+Integer.parseInt(Sda18))));
                         double iKm        = common.toDouble(String.valueOf(Integer.parseInt(Sda19)));

                         double iAvg       = iKm/iFTotal;

                         Sda21          = Double.toString(iAvg);

                         SActualKm      = Sda21;
                 }


                 Sda1    = common.Pad(Sda1,5)+"|"+SInt;
                 Sda2    = common.Pad(Sda2,8)+"|"+SInt;
                 Sda3    = common.Pad(Sda3,8)+"|"+SInt;
                 Sda4    = common.Pad(Sda4,15)+"|"+SInt;
                 Sda5    = common.Pad(Sda5,15)+"|"+SInt;
                 Sda6    = common.Pad(Sda6,15)+"|"+SInt;
                 Sda7    = common.Pad(Sda7,20)+"|"+SInt;
                 Sda8    = common.Rad(Sda8,5)+common.Space(5)+"|"+SInt;
                 Sda9    = common.Rad(Sda9,6)+common.Space(5)+"|"+SInt;
                 Sda10   = common.Pad(Sda10,10)+"|"+SInt;
                 Sda11   = common.Pad(Sda11,10)+"|"+SInt;
                 Sda12   = common.Rad(Sda12,5)+"|"+SInt;
                 Sda13   = common.Pad(Sda13,10)+"|"+SInt;
                 Sda14   = common.Pad(Sda14,8)+"|"+SInt;
                 Sda15   = common.Pad(Sda15,15)+"|";

                 //String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";
                 String Strd = Sda1+Sda5+Sda6+Sda7+Sda2+Sda8+Sda3+Sda9+Sda10+Sda11+Sda12+Sda13+Sda14+Sda15+"\n";


                 vect.add(Strd);

                 if(index==(iVSize-1))
                 {

                         double dDTotalLit  =  Double.parseDouble(Sda17);
                         double dKTotalLit =  Double.parseDouble(Sda18);

                         double dTotalFuelLit = dDTotalLit + dKTotalLit;

                         double dDMix         = (dDTotalLit/dTotalFuelLit)*100;
                         double dKMix         = (dKTotalLit/dTotalFuelLit)*100;

                         String Sdmix         =  common.getRound(String.valueOf(dDMix),2);
                         String Skmix         =  common.getRound(String.valueOf(dKMix),2);

                         Sda20   = common.Pad(Sda20,75)+"|"+SInt;
                         Sda17   = common.Rad(Sda17,5)+common.Space(5)+"|"+SInt;
                         Sda18   = common.Rad(Sda18,4)+common.Space(18)+"|"+SInt;
                         Sda19   = common.Rad(Sda19,31)+"|"+SInt;
                         Sda21   = common.Rad(Sda21,5)+common.Space(5)+"|"+SInt;  



                         String SMix = common.Pad("Mixing % ",75)+"�"+SInt;
                         String SDMix = common.Rad(Sdmix,5)+common.Space(5)+"|"+SInt;
                         String SKMix = common.Rad(Skmix,5)+common.Space(17)+"|"+SInt;


                         String Strd1= Sda20+Sda17+Sda18+Sda19+Sda21+"\n\n\n"+SMix+SDMix+SKMix+"\n";
                         VTotal.add(Strd1);
                 }                            
                 
                 index=index+1;

           }

           return vect;
               
     }

     public void setDataIntoVector ()
     {


           VName         = new Vector();
           VOutDate      = new Vector();
           VOutTime      = new Vector();
           VInTime       = new Vector();
           VStKm         = new Vector();
           VEndKm        = new Vector();
           VTotalKm      = new Vector();
           VDriver       = new Vector();
           VSecurity     = new Vector();
           VDiNo         = new Vector();
           VKiNo         = new Vector();
           VDiDate       = new Vector();
           VKiDate       = new Vector();
           VDQty         = new Vector();
           VKQty         = new Vector();
           VBunk         = new Vector();
           VClosing      = new Vector();
           VTank         = new Vector();
           VAverage      = new Vector();
           VRemarks      = new Vector();
           VSNo          = new Vector();          
           VFuelKm       = new Vector();

           String STotalKm="",STempTotalKm   = "0";   

           try
           {

                      String SDate   = SEnDate;
                      Class.forName("oracle.jdbc.OracleDriver");
                      Connection conn                  = DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");
                      Statement stat                   = conn.createStatement();
                      ResultSet res                    = stat.executeQuery(getQString());
                      int inc=0;
                      int sno=1;

                      while(res.next())
                      {
                                 
                         String SName          = res.getString(1);
                         String SOutDate       = common.parseDate((String)res.getString(2));
                         String SOutTime       = res.getString(3);
                         String SInTime        = res.getString(4);
                         
                         String SStKm          = res.getString(5);
                         String SEndKm         = res.getString(6);
                         
                         VStKm.addElement(SStKm);
                         VEndKm.addElement(SEndKm);
                         
                         VClosing.addElement(SStKm);
                         
                         String SDriver        = res.getString(7);
                         String SSecurity      = common.parseNull((String)res.getString(8));
                         String SDiNo                 = common.parseNull((String)res.getString(9));
                         String SKiNo                 = common.parseNull((String)res.getString(10));
                         String SDiDate               = common.parseDate(common.parseNull((String)res.getString(11)));
                         String SKiDate               = common.parseDate(common.parseNull((String)res.getString(12)));
                         String SDQty                 = common.parseNull((String)res.getString(13));
                         String SKQty                 = common.parseNull((String)res.getString(14));
                         String SBunk                 = common.parseNull((String)res.getString(15));     
                         String SFuelKm               = common.parseNull((String)res.getString(16)); 

                         VName.addElement(SName);
                         VOutDate.addElement(SOutDate);
                         VOutTime.addElement(SOutTime);
                         VInTime.addElement(SInTime);
                         VDriver.addElement(SDriver);
                         VSecurity.addElement(SSecurity);
                         VDiNo.addElement(SDiNo);
                         VKiNo.addElement(SKiNo);
                         VDiDate.addElement(SDiDate);
                         VKiDate.addElement(SKiDate);
                         VDQty.addElement(SDQty);
                         VKQty.addElement(SKQty);
                         VBunk.addElement(SBunk);
                         VFuelKm.addElement(SFuelKm);



                         VSNo.addElement(String.valueOf(sno));
                         sno=sno+1;
                         VTank.addElement("PARTLY");

                         VRemarks.addElement(" ");

                      }
                      res.close();
                      res          = stat.executeQuery(getLastQuery());
                      int iCount=0;
                      while(res.next())
                      {
                         if(iCount==0)
                         {
                              String SKm     = common.parseNull(res.getString(1));
                              if(SKm.equals(""))
                              {
                                   SKm= "0";
                                   VFuelKm.addElement(SKm);
                              }
                              else
                              {
                                   VFuelKm.addElement(SKm);
                              }
                         }
                         

                      iCount++;
                      }

                      if(iCount==0)
                      {
                         VFuelKm.addElement("0");
                      }
                      res.close();
                      res          = stat.executeQuery(getExpectedKmQuery());
                      while(res.next())
                      {
                         SExpectedKm    = res.getString(1);
                      }   
                      res.close();

           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }


                      int iFuelSize     =  VFuelKm.size()-1;

                      if(iFuelSize==0)
                      {
                         VTotalKm.addElement("0");
                         VAverage.addElement("0");
                      }
                      else
                      {
                           for(int index=0;index<(VFuelKm.size()-1);index++)
                           {
          
                              int itot;
                              String iAvg="";

                              if(common.parseNull((String)VFuelKm.elementAt(index+1)).equals("0"))
                              {
                                   itot           =  0 ;
                                   iAvg           = "0";
                                   STotalKm       = Integer.toString(itot);
                                   
                              }
                              else
                              {
                                   int iskm              = Integer.parseInt((String)VFuelKm.elementAt(index));
                                   int iendkm            = Integer.parseInt((String)VFuelKm.elementAt(index+1));
                                   itot              = iendkm-iskm;
     
                                   if(itot<0)
                                       STotalKm   = "0";   
                                   else
                                       STotalKm       = Integer.toString(itot);
     
     
                                   int iDQty             = Integer.parseInt(common.parseNull((String)VDQty.elementAt(index)));
                                   int iKQty             = Integer.parseInt(common.parseNull((String)VKQty.elementAt(index)));
     
                                   int iFuel             = iDQty+iKQty;
                                   double sAvg           = common.toDouble(String.valueOf(itot))/iFuel;
     
                                   iAvg           = common.getRound(sAvg,3);
     
                              }
                              VAverage.addElement(iAvg);
     
                              VTotalKm.addElement(STotalKm);
     
                           }

                      int iDiff=0,itot=0;
                      }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();
           VEndKm.removeAllElements();
           VStKm.removeAllElements();
           for(int i=0;i<(VFuelKm.size()-1);i++)
           {
               VStKm.addElement(VFuelKm.elementAt(i));
               VEndKm.addElement(VFuelKm.elementAt(i+1));
           }
                               
           vect.addElement(VHead2);
           vect.addElement(VSNo);
           vect.addElement(VDiNo);
           vect.addElement(VKiNo);
           vect.addElement(VDiDate);
           vect.addElement(VKiDate);
           vect.addElement(VDriver);
           vect.addElement(VSecurity);
           vect.addElement(VDQty);
           vect.addElement(VKQty);
           vect.addElement(VStKm);
           vect.addElement(VEndKm);
           vect.addElement(VTotalKm);
           vect.addElement(VAverage);
           vect.addElement(VTank);
           vect.addElement(VRemarks);

           return vect;
     }
                                   

     public String getQString()
     {
           String QString    = "Select VehicleName,OutDate,OutTime,InTime,StKm,EndKm,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty,BunkName ,FuelKms from Vehicles "+
                               " where OutDate >= '"+SStDate+"' AND OutDate <= '"+SEnDate+"' And Status=1 AND  VehicleNo = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 2 ";    

           return QString;
     }

     public String getLastQuery()
     {
           String Query    = "Select FuelKms from Vehicles "+
                               " where OutDate > '"+SEnDate+"' And Status=1 AND  VehicleNo = '"+SSelected+"' AND Purpose = 'TO FILL FUEL' order by 1 ";    

           return Query;
     }

     public String getExpectedKmQuery()
     {
           String SExpQuery    = "Select StdKmpl from VehicleInfo  where VehicleRegNo = '"+SSelected+"' " ;

           return SExpQuery;
     }



     public String getStatus()
     {
           return SStatus;
     }

}
