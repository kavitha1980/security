package Reports.Key;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import Reports.*;

public class KeyRep extends HttpServlet
{
     HttpSession session;
     
     Common common;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          Information info         = new Information();
          
          response.setContentType("text/html");

          PrintWriter out          = response.getWriter();
          session                  = request.getSession(false);
          String SServer           = (String)session.getValue("Server");
          String SKeyCode          = (String)request.getParameter("KeySelector");
          String SValue            = common.parseNull(request.getParameter("t1"));
          String SStDate           = common.pureDate(request.getParameter("TStDate"));
          String SEnDate           = common.pureDate(request.getParameter("TEnDate"));
          String SFile             = common.parseNull(request.getParameter("TFile"));
          String SPrint            = common.parseNull(request.getParameter("Print"));
          SFile                    = (SFile.trim()).length()==0?"d:/KeyList.prn ":SFile;
          KeyList theList          = new KeyList(SStDate,SEnDate,SFile,SKeyCode);
          Vector vect              = new Vector();  
          vect                     = theList.getPrintValues();
          Vector VInfo             = new Vector();
          Vector VValue            = new Vector();
          
          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");
          
          VValue.addElement("Key List From "+common.parseDate(SStDate)+"To "+common.parseDate(SEnDate));
          VValue.addElement("2(e)");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());
          
          
          info.flashMessage(VInfo,VValue,vect,out);
                             
          out.close();
     }
}
