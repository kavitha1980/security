
package Reports.Key;

import utility.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;
import util.*;
import Reports.*;

public class KeyList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;
     protected String SKeyCode;

     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Vector VDate,VKey,VWDPerson,VWDTime,VRetPerson,VRetTime,VSNo,Vhead;
     Common common = new Common();

     String SStatus = "";
     Connection theConnection;
     KeyList(String SStDate,String SEnDate,String SFile,String SKeyCode)
     {
            this.SStDate      = SStDate;
            this.SEnDate      = SEnDate;
            this.SFile        = SFile;
            this.SKeyCode     = SKeyCode;

            try
            {
                  SStatus = (new java.util.Date()).toString();
                  setKeyList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setKeyList()
     {
           setDataIntoVector();
           System.out.println("Datesize:"+VDate.size());
           
           String STitle = " Key List From "+common.parseDate(SStDate)+" To :"+common.parseDate(SEnDate)+" ";
           Vector VHead  = getKeyHead();
           iLen = ((String)VHead.elementAt(0)).length();
           Strline = common.Replicate("-",iLen)+"\n";
           Vector VBody  = getKeyListBody();
           new DocPrint(VBody,VHead,STitle,SFile);
     }

      public Vector getKeyHead()
      {
           Vector vect   = new Vector();
           Vhead         = new Vector();
           
           String Head1[]={"SNo","Date","Key","WithDrawn By","WithDrawnTime","Returned By","Returned Time"};

           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=common.parseNull((String)Head1[6]);

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);

           Sha1  = common.Rad(Sha1,5)+common.Space(3);
           Sha2  = common.Pad(Sha2,15)+SInt;
           Sha3  = common.Pad(Sha3,20)+SInt;
           Sha4  = common.Pad(Sha4,25)+SInt;
           Sha5  = common.Pad(Sha5,15)+SInt;
           Sha6  = common.Pad(Sha6,25)+SInt;
           Sha7  = common.Pad(Sha7,15)+SInt;

           String Strh1 = Sha1+Sha2+Sha3+Sha4+Sha5+Sha6+Sha7+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getKeyListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {
                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VDate.elementAt(i);
                 String Sda3  = (String)VKey.elementAt(i);
                 String Sda4  = (String)VWDPerson.elementAt(i);
                 String Sda5  = (String)VWDTime.elementAt(i);
                 String Sda6  = (String)VRetPerson.elementAt(i);
                 String Sda7  = (String)VRetTime.elementAt(i);

                 Sda1    = common.Rad(Sda1,5)+common.Space(3);
                 Sda2    = common.Pad(Sda2,15)+SInt;
                 Sda3    = common.Pad(Sda3,20)+SInt;
                 Sda4    = common.Pad(Sda4,25)+SInt;
                 Sda5    = common.Pad(Sda5,15)+SInt;
                 Sda6    = common.Pad(Sda6,25)+SInt;
                 Sda7    = common.Pad(Sda7,15)+SInt;

                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+"\n";
                 vect.add(Strd);
           }
           return vect;
     }



     public void setDataIntoVector ()
     {
           VSNo          = new Vector();     
           VDate         = new Vector();
           VKey          = new Vector();
           VWDPerson     = new Vector();
           VWDTime       = new Vector();
           VRetPerson    = new Vector();
           VRetTime      = new Vector();
           int i=1;
           try   
           {
               JDBCConnection jdbc           = JDBCConnection.getJDBCConnection();
               theConnection                 = jdbc.getConnection();
               Statement theStatement        = theConnection.createStatement();
               ResultSet theResult           = theStatement.executeQuery(getQS());
               while(theResult.next())
               {
                    VSNo.addElement(String.valueOf(i));
                    VDate.addElement(common.parseDate(theResult.getString(1)));
                    VKey.addElement(theResult.getString(2));
                    VWDPerson.addElement(theResult.getString(3));
                    VWDTime.addElement(theResult.getString(4));
                    VRetPerson.addElement(theResult.getString(5));
                    VRetTime.addElement(theResult.getString(6));
                    System.out.println("i:"+i);
                    i=i+1;
               }
           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }
     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VDate);
           vect.addElement(VKey);
           vect.addElement(VWDPerson);
           vect.addElement(VWDTime);
           vect.addElement(VRetPerson);
           vect.addElement(VRetTime);
           return vect;

     }
           

     public String getQS()
     {
           String SFilter = "";

           if(!SKeyCode.equals("99"))
               SFilter   = " And KeyTrans.KeyId = "+SKeyCode+" ";

           String QS =   " Select WDDate,KeyName,WDPerson,WDTime,EmpName as RetPerson, "+
                         " RetTime from      "+
                         " (                 "+

                         " Select TO_CHAR(WDTime,'yyyymmdd') as WDDate,ReturnedBy,"+
                         " KeyName,EmpName as WDPerson, "+
                         " TO_CHAR(WDTime,'hh24:mi:ss') as WDTime,"+
                         " TO_CHAR(ReturnTime,'hh24:mi:ss') as RetTime "+
                         " from KeyTrans inner join KeyInfo on "+
                         " KeyTrans.keyId    = KeyInfo.KeyId "+
                         " inner join VisitorStaff on          "+
                         " VisitorStaff.EmpCode = KeyTrans.staffCode "+
                         " where Status=1 and TO_CHAR(ReturnTime,'yyyymmdd')>="+SStDate+" and "+
                         " TO_CHAR(ReturnTime,'yyyymmdd') <= "+SEnDate+" "+SFilter+" "+
                         " ) Temp "+
                         " inner join VisitorStaff on VisitorStaff.EmpCode = Temp.ReturnedBy "+
                         " order by WDDate " ;

               try
               {
                    FileWriter fw = new FileWriter("d:/test.sql");
                    fw.write(QS);
                    fw.close();
               }
               catch(Exception e)
               {
                    e.printStackTrace();
               }
           return QS;
     }


     public String getStatus()
     {
           return SStatus;
     }

}
