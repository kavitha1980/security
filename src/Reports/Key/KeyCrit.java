package Reports.Key;

import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import domain.jdbc.*;

public class KeyCrit extends HttpServlet
{
     HttpSession session;
     
     Vector VKeyCode,VKeyName;
     Connection theConnection;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");

          PrintWriter out     = response.getWriter();
          session             = request.getSession(false);
          String SServer      = (String)session.getValue("Server");
          setVector();


          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='main'>");
          out.println("<form method='GET' action='"+SServer+"Reports.Key.KeyRep'>");
          
          out.println("<table>");
          out.println("<tr>");
          out.println("     <td><b>Key</b></td>");
          out.println("     <td><b>From</b></td>");
          out.println("     <td><b>To</b></td>");
          out.println("     <td><b>File</b></td>");
          out.println("     <td></td>");
          out.println("     <td></td>");
          out.println("</tr>");
          out.println("<tr>");
          out.println("     <td> ");
          out.println(" <select name ='KeySelector'>");
          for(int i=0;i<VKeyCode.size();i++)
          {
               out.println("<option value='"+VKeyCode.elementAt(i)+"'>"+VKeyName.elementAt(i)+"</option>");
          }
          out.println("</select></td>");

          out.println("     <td><input type='text' name='TStDate' size='15'></td>");
          out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
          out.println("     <td><input type='text' name='TFile' size='15'></td>");
          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("<tr>");
          
          out.println("</table>");

          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          
          out.close();
     }
     private void setVector()
     {
          VKeyCode       = new Vector();
          VKeyName       = new Vector();
          try
          {
               JDBCConnection jdbc      = JDBCConnection.getJDBCConnection();
               theConnection            = jdbc.getConnection();
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(getQS());
               VKeyCode.addElement("99");
               VKeyName.addElement("All");
               while(theResult.next())
               {
                    VKeyCode.addElement(theResult.getString(1));
                    VKeyName.addElement(theResult.getString(2));
               }
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     
     private String getQS()
     {
          String QS = " Select KeyId,KeyName from KeyInfo ";
		
          return QS;
     }
}
     

