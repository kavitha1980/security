
package Reports.ErrectorCheck;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class ErrectorRegisterAsList implements rndi.CodedNames
{
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;

     String SInt = "  ";
     String Strline = "";
     String SStatus = "";
     int iLen=0;

     Vector VSNo,VDate,VCompany,VRep,VPurpose,VInTime,VOutTime;
     Vector VEHead;
     Common common = new Common();
     VisitorIn visitorDomain;

     ErrectorRegisterAsList(String SEnDate,String SFile)
     {
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  visitorDomain         = (VisitorIn)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInwardList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }
     }

     public void setInwardList()
     {
            setDataIntoVector();
            String STitle = " Errector Register List As on "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getErrectorHead();

            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getErrectorListBody();

            new DocPrint(VBody,VHead,STitle,SFile);
      }

      public Vector getErrectorHead()
      {
           Vector VSumErHead = new Vector();

           String Head1[]={"SNo","CompanyName","RepresentativeName","Purpose Of Visit","InTime","OutTime","Date" };        

           String SEH1 = ((String)Head1[0]).trim();
           String SEH2 = ((String)Head1[1]).trim();
           String SEH3 = ((String)Head1[2]).trim();
           String SEH4 = ((String)Head1[3]).trim();
           String SEH5 = common.parseNull((String)Head1[4]);
           String SEH6 = common.parseNull((String)Head1[5]);
           String SEH7 = ((String)Head1[6]).trim();

           VEHead  = new Vector();

           VEHead.addElement(SEH1);
           VEHead.addElement(SEH2);
           VEHead.addElement(SEH3);
           VEHead.addElement(SEH4);
           VEHead.addElement(SEH5);
           VEHead.addElement(SEH6);
           VEHead.addElement(SEH7);

           SEH1  = common.Rad(SEH1,5)+common.Space(2);
           SEH2  = common.Pad(SEH2,25)+SInt;
           SEH3  = common.Pad(SEH3,20)+SInt;
           SEH4  = common.Pad(SEH4,30)+SInt;
           SEH5  = common.Pad(SEH5,13)+SInt;
           SEH6  = common.Pad(SEH6,13)+SInt;
           SEH7  = common.Pad(SEH7,13);

           String SErHead = SEH1+SEH2+SEH3+SEH4+SEH5+SEH6+SEH7+"\n";
           VSumErHead.add(SErHead);
           return VSumErHead;
     }
     public Vector getErrectorListBody()
     {
          Vector VErListBody = new Vector();
          
          for(int i=0;i<VDate.size();i++)
          {
               String SLiBody1  = (String)VSNo        .elementAt(i);
               String SLiBody2  = (String)VCompany    .elementAt(i);
               String SLiBody3  = (String)VRep        .elementAt(i);
               String SLiBody4  = (String)VPurpose    .elementAt(i);
               String SLiBody5  = (String)VInTime     .elementAt(i);
               String SLiBody6  = (String)VOutTime    .elementAt(i);
               String SLiBody7  = (String)VDate       .elementAt(i);
               
               SLiBody1    = common.Rad(SLiBody1,5)+common.Space(2);
               SLiBody2    = common.Pad(SLiBody2,25)+SInt;
               SLiBody3    = common.Pad(SLiBody3,20)+SInt;
               SLiBody4    = common.Pad(SLiBody4,30)+SInt;
               SLiBody5    = common.Pad(SLiBody5,13)+SInt;
               SLiBody6    = common.Pad(SLiBody6,13)+SInt;
               SLiBody7    = common.Pad(SLiBody7,13);
               
               String SLiBody = SLiBody1+SLiBody2+SLiBody3+SLiBody4+SLiBody5+SLiBody6+SLiBody7+"\n";
               VErListBody.add(SLiBody);
          }
          return VErListBody;
     }
     public void setDataIntoVector()
     {
          VSNo          = new Vector();
          VCompany      = new Vector();
          VRep          = new Vector();
          VPurpose      = new Vector();
          VInTime       = new Vector();
          VOutTime      = new Vector();
          VDate         = new Vector();
          
          try
          {
               int iDate         = Integer.parseInt(SEnDate);
               Vector VErrector  = visitorDomain.getErrectorAsOnReport(iDate);
               int m=0;
               
               for(int i=0;i<(VErrector.size()/6);i++)
               {
                    VSNo      .addElement(String.valueOf(i+1));
                    VCompany  .addElement(VErrector.elementAt(m+0));
                    VRep      .addElement(VErrector.elementAt(m+1));
                    VPurpose  .addElement(VErrector.elementAt(m+2));
                    VInTime   .addElement(VErrector.elementAt(m+3));
                    VOutTime  .addElement(VErrector.elementAt(m+4));
                    VDate     .addElement(common.parseDate((String)VErrector.elementAt(m+5)));
                    
                    m=m+6;
               }
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(VEHead);
           vect.addElement(VSNo);
           vect.addElement(VCompany);
           vect.addElement(VRep);
           vect.addElement(VPurpose);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
           vect.addElement(VDate);
           return vect;
     }
     public String getStatus()
     {
          return SStatus;
     }
}
