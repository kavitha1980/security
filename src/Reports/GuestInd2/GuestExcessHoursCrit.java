package Reports.GuestInd2;

import util.*;
import java.io.*;
import java.sql.*;           
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GuestExcessHoursCrit extends HttpServlet
{
     HttpSession session;
     
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");

          PrintWriter out     = response.getWriter();
          session             = request.getSession(false);
          String SServer      = (String)session.getValue("Server");
          
          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='main'>");
          out.println("<form method='GET' action='"+SServer+"Reports.GuestInd2.GuestExcessHoursRep'>");
          
          out.println("<table>");
          out.println("<tr>");
          out.println("     <td><b>SpentTime</b></td>");
          out.println("     <td><b>From</b></td>");
          out.println("     <td><b>To</b></td>");
          out.println("     <td><b>File</b></td>");
          out.println("     <td></td>");
          out.println("     <td></td>");
          out.println("     <td></td>");
          out.println("     <td></td>");
          out.println("<tr>");

          out.println("<tr>");

          out.println("     <td>");
          out.println("     <Select name='t1'>");
          out.println("     <option value='200'>       </option>");
          out.println("     <option value='200'> > 24  </option>");
          out.println("     <option value='300'> < 24 </option>");
          out.println("     </select>");

          out.println("     <td><input type='text' name='TStDate' size='15'></td>");
          out.println("     <td><input type='text' name='TEnDate' size='15'></td>");
          out.println("     <td><input type='text' name='TFile' size='15'></td>");
          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='submit' value='Print' name='BPrint' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("<tr>");
          
          out.println("</table>");

          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          
          out.close();
     }
}
     

