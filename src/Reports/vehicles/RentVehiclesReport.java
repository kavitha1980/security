package Reports.vehicles;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import domain.jdbc.*;
import rndi.*;

public class RentVehiclesReport extends HttpServlet implements rndi.CodedNames
{                 
     HttpSession session;
     
     Common common;

      
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
                
               RentVehiclesInformation info    = new RentVehiclesInformation();
               
               response.setContentType("text/html");
     
               PrintWriter out          = response.getWriter();
               session                  = request.getSession(false);
               String SServer           = (String)session.getValue("Server");
     
     
               String SStDate           = common.pureDate(request.getParameter("TStDate"));
               String SEnDate           = common.pureDate(request.getParameter("TEnDate"));
               String SFile             = common.parseNull(request.getParameter("TFile"));
               String SRegNo            = common.parseNull(request.getParameter("RegNo"));

               System.out.println(SRegNo);

              SFile                    = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/src/Reports/vehicles/rentvehicle.prn ":SFile;
                                                                                                        
               
               RentVehiclesRegisterList theList= new  RentVehiclesRegisterList(SStDate,SEnDate,SFile,SRegNo); 

               
               Vector vect              = new Vector();  
               vect                     = theList.getPrintValues();
               
               Vector VInfo             = new Vector();
               Vector VValue            = new Vector();
               
               VInfo.addElement("Report Name");
               VInfo.addElement("Report Code");
               VInfo.addElement("Report url/File Name");
               VInfo.addElement("Status");
               
               VValue.addElement("RentVehicles Report "+common.parseDate(SStDate)+"To"+common.parseDate(SEnDate));
               VValue.addElement("2(e)");
               VValue.addElement(SFile);
               VValue.addElement(theList.getStatus());
               
               
               info.flashMessage(VInfo,VValue,vect,out);
                                  
               out.close();
          }
          catch(Exception e)
          {
                System.out.println(e);
          }
     }
}
