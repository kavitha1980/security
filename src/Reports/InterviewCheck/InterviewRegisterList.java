package Reports.InterviewCheck;

import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class InterviewRegisterList implements rndi.CodedNames
{
     protected String SStDate;
     protected String SEnDate;
     protected String SFile;
     protected String SDSN;


     String SInt = "  ";
     String Strline = "";
     int iLen=0;

     Object RowData[][];

     Vector VSNo,VName,VAge,VSex,VQuali,VDate,VCategory,VPlace,VInTime,VOutTime,VAgentName,VSpentTime,VResult;
     Vector Vhead;
     Common common = new Common();


     String SStatus = "";

     Info  interviewDomain;

     InterviewRegisterList(String SStDate,String SEnDate,String SFile)
     {
            this.SStDate = SStDate;
            this.SEnDate = SEnDate;
            this.SFile   = SFile;

            try
            {
                  
                  Registry registry     = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
                  interviewDomain       = (Info)registry.lookup(SECURITYDOMAIN);
                  SStatus = (new java.util.Date()).toString();
                  setInterviewList();
            }
            catch(Exception ex)
            {
               SStatus = ex.getMessage();   
            }

     }

     public void setInterviewList()
     {
            setDataIntoVector();
            String STitle = " Interview Register List From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate)+" \n";
            Vector VHead  = getInterviewHead();
            iLen = ((String)VHead.elementAt(0)).length();
            Strline = common.Replicate("-",iLen)+"\n";
            Vector VBody  = getInterviewListBody();
            new DocPrint(VBody,VHead,STitle,SFile);
      }

      String SSpace = common.Space(3);

      public Vector getInterviewHead()
      {
           Vector vect = new Vector();

           String Head1[]={"SNo","Select","Name","Age","Sex","Qualification","Place","Date","InTime","OutTime","Category","AgentName","SpentTime" ," Result" };        
           String Sha1=((String)Head1[0]).trim();
           String Sha2=((String)Head1[1]).trim();
           String Sha3=((String)Head1[2]).trim();
           String Sha4=((String)Head1[3]).trim();
           String Sha5=((String)Head1[4]).trim();
           String Sha6=((String)Head1[5]).trim();
           String Sha7=((String)Head1[6]).trim();
           String Sha8=common.parseNull((String)Head1[7]);
           String Sha9=common.parseNull((String)Head1[8]);
           String Sha10=((String)Head1[9]).trim();
           String Sha11=((String)Head1[10]).trim();
           String Sha12=((String)Head1[11]).trim();
           String Sha13=((String)Head1[12]).trim();
           String Sha14=((String)Head1[13]).trim();

           Vhead  = new Vector();

           Vhead.addElement(Sha1);
           Vhead.addElement(Sha2);
           Vhead.addElement(Sha3);
           Vhead.addElement(Sha4);
           Vhead.addElement(Sha5);
           Vhead.addElement(Sha6);
           Vhead.addElement(Sha7);
           Vhead.addElement(Sha8);
           Vhead.addElement(Sha9);
           Vhead.addElement(Sha10);
           Vhead.addElement(Sha11);
           Vhead.addElement(Sha12);
           Vhead.addElement(Sha13);
           Vhead.addElement(Sha14);


           Sha1  = common.Rad(Sha1,5)+common.Space(3)+"�"+SInt;
         //  Sha2  = common.Pad(Sha2,5)+"�"+SInt;
           Sha3  = common.Pad(Sha3,25)+"�"+SInt;
           Sha4  = common.Pad(Sha4,5)+"�"+SInt;
           Sha5  = common.Pad(Sha5,8)+"�"+SInt;
           Sha6  = common.Pad(Sha6,15)+"�"+SInt;
           Sha7  = common.Pad(Sha7,15)+"�"+SInt;
           Sha8  = common.Pad(Sha8,15)+"�"+SInt;
           Sha9  = common.Pad(Sha9,15)+"�"+SInt;
           Sha10  = common.Pad(Sha10,15)+"�"+SInt;
           Sha11  = common.Pad(Sha11,20)+"�"+SInt;
           Sha12  = common.Pad(Sha12,20)+"�"+SInt;
           Sha13  = common.Pad(Sha13,15)+"�"+SInt;
           Sha14  = common.Pad(Sha14,15)+"�"+SInt;

           String Strh1 = Sha1+Sha3+Sha4+Sha5+Sha6+Sha7+Sha8+Sha9+Sha10+Sha11+Sha12+Sha13+Sha14+"\n";
           vect.add(Strh1);
           return vect;

     }
     public Vector getInterviewListBody()
     {
           Vector vect = new Vector();

           for(int i=0;i<VDate.size();i++)
           {

                 String Sda1  = (String)VSNo.elementAt(i);
                 String Sda2  = (String)VName.elementAt(i);
                 String Sda3  = (String)VAge.elementAt(i);
                 String Sda4  = (String)VSex.elementAt(i);
                 String Sda5  = (String)VQuali.elementAt(i);
                 String Sda6  = (String)VPlace.elementAt(i);
                 String Sda7  = (String)VDate.elementAt(i);
                 String Sda8  = (String)VInTime.elementAt(i);
                 String Sda9  = (String)VOutTime.elementAt(i);
                 String Sda10 = (String)VCategory.elementAt(i);
                 String Sda11 = (String)VAgentName.elementAt(i);
                 String Sda12 = (String)VSpentTime.elementAt(i);
                 String Sda13 = (String)VResult.elementAt(i);




                 Sda1    = common.Rad(Sda1,5)+common.Space(3)+"�"+SInt;
                 Sda2    = common.Pad(Sda2,25)+"�"+SInt;
                 Sda3    = common.Pad(Sda3,5)+"�"+SInt;
                 Sda4    = common.Pad(Sda4,8)+"�"+SInt;
                 Sda5    = common.Pad(Sda5,15)+"�"+SInt;
                 Sda6    = common.Pad(Sda6,15)+"�"+SInt;
                 Sda7    = common.Pad(Sda7,15)+"�"+SInt;
                 Sda8    = common.Pad(Sda8,15)+"�"+SInt;
                 Sda9    = common.Pad(Sda9,15)+"�"+SInt;
                 Sda10   = common.Pad(Sda10,20)+"�"+SInt;
                 Sda11   = common.Pad(Sda11,20)+"�"+SInt;
                 Sda12   = common.Pad(Sda12,15)+"�"+SInt;
                 Sda13   = common.Pad(Sda13,15)+"�"+SInt;

                 String Sda21    = common.Rad(" ",5)+common.Space(3)+"�"+SInt;
                 String Sda22    = common.Pad(" ",25)+"�"+SInt;
                 String Sda23    = common.Pad("",5)+"�"+SInt;
                 String Sda24    = common.Pad("",8)+"�"+SInt;
                 String Sda25    = common.Pad("",15)+"�"+SInt;
                 String Sda26    = common.Pad("",15)+"�"+SInt;
                 String Sda27    = common.Pad("",15)+"�"+SInt;
                 String Sda28    = common.Pad("",15)+"�"+SInt;
                 String Sda29    = common.Pad("",15)+"�"+SInt;
                 String Sda210   = common.Pad("",20)+"�"+SInt;
                 String Sda211   = common.Pad("",20)+"�"+SInt;
                 String Sda212   = common.Pad("",15)+"�"+SInt;
                 String Sda213   = common.Pad("",15)+"�"+SInt;



                 String Strd = Sda1+Sda2+Sda3+Sda4+Sda5+Sda6+Sda7+Sda8+Sda9+Sda10+Sda11+Sda12+Sda13;
                 String Strd1 = Sda21+Sda22+Sda23+Sda24+Sda25+Sda26+Sda27+Sda28+Sda29+Sda210+Sda211+Sda212+Sda213;

                 vect.add(Strd);
                 vect.add(Strd1);

                 
           }
           return vect;
     }



     public void setDataIntoVector ()
     {
     //Vector VRegNo,VName,VOutDate,VOutTime,VInDate,VInTime,VStKm,VEndKm,VTotalKm,VPlace,VPurpose,VDriver,VSecurity;



           VSNo          = new Vector();     
           VName         = new Vector();
           VAge          = new Vector();
           VSex          = new Vector();
           VQuali        = new Vector();
           VDate         = new Vector();
           VCategory     = new Vector();
           VPlace        = new Vector();
           VInTime       = new Vector();
           VOutTime      = new Vector();
           VAgentName    = new Vector();
           VSpentTime    = new Vector();
           VResult       = new Vector();
           try
           {

                 int iDate         = Integer.parseInt(SEnDate);
                 int istDate       = Integer.parseInt(SStDate);
                 Vector tVector    = interviewDomain.getReportCheckVector(istDate,iDate);
                 int size= (tVector.size())/11;
                 int m=0;

                 String SInTime = "" ;
                 String SOutTime = "" ;
                 String SSpentTime= "";

                 for(int i=0;i<size; i++)
                 {
                         VSNo.addElement(String.valueOf(i+1));
                         VName.addElement(tVector.elementAt(m+0));
                         VAge.addElement(tVector.elementAt(m+9));
                         VSex.addElement(tVector.elementAt(m+1));
                         VQuali.addElement(tVector.elementAt(m+2));

                        
                         String SResult = common.parseNull((String)tVector.elementAt(m+10));

                         if(SResult.equals(""))
                         {
                              VResult.addElement(" ");
                              
                         }     
                         else
                         {
                              int iResult = Integer.parseInt(SResult);
                              if(iResult==0)
                                   VResult.addElement("UnKnown ");
                              if(iResult==1)
                                   VResult.addElement("Selected");
                              if(iResult==2)
                                   VResult.addElement("Not-Selected");

                         }
                         VDate.addElement(common.parseDate((String)tVector.elementAt(m+8)));

                         VCategory.addElement(tVector.elementAt(m+5));
                         VPlace.addElement(tVector.elementAt(m+3));
                         VInTime.addElement(tVector.elementAt(m+6));
                         VOutTime.addElement(tVector.elementAt(m+7));

                         SInTime = (String)tVector.elementAt(m+6);
                         SOutTime = (String)tVector.elementAt(m+7);

                         int iInterviewDate = common.toInt((String)tVector.elementAt(m+8));
                         SSpentTime = common.getTimeDiff(iInterviewDate,iInterviewDate,SOutTime,SInTime);

                         VSpentTime.addElement(SSpentTime);
                         VAgentName.addElement(tVector.elementAt(m+4));

                         m=m+11;
                 }
           }
           catch(Exception ex)
           {
               System.out.println(ex);
           }

     }
     public Vector getPrintValues()
     {
           Vector vect   = new Vector();

           vect.addElement(Vhead);
           vect.addElement(VSNo);
           vect.addElement(VName);
           vect.addElement(VAge);
           vect.addElement(VSex);
           vect.addElement(VQuali);
           vect.addElement(VPlace);
           vect.addElement(VDate);
           vect.addElement(VInTime);
           vect.addElement(VOutTime);
           vect.addElement(VCategory);
           vect.addElement(VAgentName);
           vect.addElement(VSpentTime);
           vect.addElement(VResult);
           return vect;

     }
           

     public String getQString()
     {
           //String QString = "SELECT GateInward.GINo, GateInward.GIDate, Supplier.Name, GateInward.Item_Code, InvItems.Item_Name, GateInward.SupQty, GateInward.InvNo, GateInward.InvDate, GateInward.DcNo, GateInward.DcDate "+
                    //" FROM (GateInward INNER JOIN Supplier ON GateInward.Sup_Code = Supplier.Ac_Code) INNER JOIN InvItems ON GateInward.Item_Code = InvItems.Item_Code "+
                    // "Where GateInward.GIDate <= '"+SEnDate+"' ";
           String QString    = "Select VehicleNo,VehicleName,OutDate,OutTime,InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,SecurityName,DINo,KiNo,DiDate,KiDate,DQty,KQty from Vehicles "+
                               " where OutDate <= '"+SEnDate+"' And Status=1  ";    

           return QString;
     }


     public String getStatus()
     {
           return SStatus;
     }





}















         
