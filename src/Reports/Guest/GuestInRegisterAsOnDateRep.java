package Reports.Guest;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import blf.*;
import domain.jdbc.*;
import rndi.*;

public class GuestInRegisterAsOnDateRep extends HttpServlet implements rndi.CodedNames
{                 
     HttpSession session;
     
     Common common;

     Info infoDomain;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               infoDomain          = (Info)registry.lookup(SECURITYDOMAIN);
          
               GuestInRegisterAsOnDateInformation info    = new GuestInRegisterAsOnDateInformation();
               
               response.setContentType("text/html");
     
               PrintWriter out          = response.getWriter();
               session                  = request.getSession(false);
               String SServer           = (String)session.getValue("Server");
     
     
               String SEnDate           = common.pureDate(request.getParameter("TEnDate"));
               String SFile             = common.parseNull(request.getParameter("TFile"));
               
               SFile                    = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/GuestAsOnDateRep.prn ":SFile;
                                                                                                        
               GuestInRegisterAsOnDateList theList= new GuestInRegisterAsOnDateList(SEnDate,SFile,"Guest");
               
               Vector vect              = new Vector();  
               vect                     = theList.getPrintValues();
               
               Vector VInfo             = new Vector();
               Vector VValue            = new Vector();
               
               VInfo.addElement("Report Name");
               VInfo.addElement("Report Code");
               VInfo.addElement("Report url/File Name");
               VInfo.addElement("Status");
               
               VValue.addElement("Guest List As On "+common.parseDate(SEnDate));
               VValue.addElement("2(e)");
               VValue.addElement(SFile);
               VValue.addElement(theList.getStatus());
               
               
               info.flashMessage(VInfo,VValue,vect,out);
                                  
               out.close();
          }
          catch(Exception e)
          {
          }
     }
}
