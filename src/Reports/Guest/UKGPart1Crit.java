import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class UKGPart1Crit extends HttpServlet
{
     HttpSession session;
     RepVect repvect = new RepVect();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");

          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='main'>");
          out.println("<form method='GET' action='http://"+SServer+":8080/servlet/UKGPart1Rep'>");
          out.println("  <p><font color='#000000' size='4'><b>Unit </b></font> ");

          //Unit ComboBox
          out.println(" <select size='1' name='unit'>");
          for(int i=0;i<repvect.VUnitCode.size();i++)
              out.println("<option value="+(String)repvect.VUnitCode.elementAt(i)+">"+(String)repvect.VUnit.elementAt(i)+"</option>");
          out.println(" </select>");

          out.println("  <b>&nbsp;<font size='4'>Period From </font></b><input type='text' name='T1' size='15'>");
          out.println("  <b>&nbsp;</b>");
          out.println("  <b>&nbsp;<font size='4'>To </font></b><input type='text' name='T2' size='15'>");
          out.println("  <b>&nbsp;</b>");
          out.println("  <input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("  <input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'>");
          out.println("  &nbsp;&nbsp;(ELC - 43)</p>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
}

