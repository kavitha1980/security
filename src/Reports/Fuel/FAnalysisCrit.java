package Reports.Fuel;
import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;

public class FAnalysisCrit extends HttpServlet implements rndi.CodedNames
{
	HttpSession session;
	Common common = new Common();
	String bgBody = common.bgBody;
	String fgUom  = common.fgUom;
	Vector VName,VCode;
	Vector VSName,VSCode;
	String SPatch;
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		setDataIntoVector();
	}
     
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
          SPatch          ="20040401";
		setMonthVectors();
          session         = request.getSession(false);
          session.putValue("VehicleCodes",VSCode);
          session.putValue("VehicleNames",VSName);
		String SServer  = (String)session.getValue("Server");
		out.println("<html>");
		out.println("<body bgcolor='#9AA8D6'>");
		out.println("<base target='main'>");
          out.println("<form method='GET' action='http://"+SServer+"/Reports.Fuel.FAnalysisRep'>");
		out.println("<table border='0' bgcolor='#9AA8D6' cellspacing='0' cellpadding='0'>");
		out.println("<tr>");
          out.println("<td  height='10' align='center'><font color='"+fgUom+"'>Select the Vehicle</td>");
		out.println("<td  height='10' align='center'><font color='"+fgUom+"'>From</td>");
		out.println("<td  height='10' align='center'><font color='"+fgUom+"'>To</td>");
          out.println("    <td  height='10'><font color='"+fgUom+"'>FileName</td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td  height='10'><font color='"+bgBody+"'><select name='C1'>");

		for(int i=0; i<VSCode.size(); i++)
		{
		out.println("<option value='"+ VSCode.elementAt(i)+"'> " + VSName.elementAt(i)+"</option>");
		}
		out.println("</select></td>");
		out.println("<td  height='10'><font color='"+bgBody+"'>	<select name='C2'>");
		for(int i=0;i<VName.size();i++)
		{
			String SName   = (String)VName.elementAt(i);
			String SCode   = (String)VCode.elementAt(i);
			out.println("<option Value='"+SCode+"'>"+SName+"</Option>");
		}				
		out.println("</select></td>");	 
		out.println("<td  height='10'><font color='"+bgBody+"'>	<select name='C3'>");
		for(int i=0;i<VName.size();i++)
		{
			String SName   = (String)VName.elementAt(i);
			String SCode   = (String)VCode.elementAt(i);
			out.println("<option Value='"+SCode+"'>"+SName+"</Option>");
		}				
		out.println("</select></td>");	 

          out.println(" <td><input type='text' value='' name='FileName'></td>");

          out.println("<td  height='10'><font color='"+bgBody+"'><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
		out.println("<td  height='10'><font color='"+bgBody+"'><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
		out.close();
		}
	private void setDataIntoVector()
	{
		VSCode= new Vector();
		VSName= new Vector();	
          String SQry = " Select VehicleRegNo,VehicleName from VehicleInfo order by 1";
		try
		{
               JDBCConnection connect = JDBCConnection.getJDBCConnection();
               Connection theConnection = connect.getConnection();
			Statement theStatement = theConnection.createStatement();
			ResultSet theResult = theStatement.executeQuery(SQry);
			while(theResult.next())
			{
				VSCode.addElement(theResult.getString(1));
				VSName.addElement(theResult.getString(2));
			}
			theStatement.close();
		}
		catch(Exception ex)
		{
			System.out.println("RepA20Crit" + ex);
		}
  	}

		private void setMonthVectors()
	{
		VCode = new Vector();
		VName = new Vector();
		String SYe1 = SPatch.substring(0,4);
		String SYe2 = String.valueOf(common.toInt(SPatch.substring(0,4))+1);

		VName.addElement("Apr`"+SYe1);
		VName.addElement("May`"+SYe1);
		VName.addElement("Jun`"+SYe1);
		VName.addElement("Jul`"+SYe1);
		VName.addElement("Aug`"+SYe1);
		VName.addElement("Sep`"+SYe1);
		VName.addElement("Oct`"+SYe1);
		VName.addElement("Nov`"+SYe1);
		VName.addElement("Dec`"+SYe1);
		VName.addElement("Jan`"+SYe2);
		VName.addElement("Feb`"+SYe2);
		VName.addElement("Mar`"+SYe2);

		VCode.addElement(SYe1+"04");
		VCode.addElement(SYe1+"05");
		VCode.addElement(SYe1+"06");
		VCode.addElement(SYe1+"07");
		VCode.addElement(SYe1+"08");
		VCode.addElement(SYe1+"09");
		VCode.addElement(SYe1+"10");
		VCode.addElement(SYe1+"11");
		VCode.addElement(SYe1+"12");
		VCode.addElement(SYe2+"01");
		VCode.addElement(SYe2+"02");
		VCode.addElement(SYe2+"03");
	}

}

