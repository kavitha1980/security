/*
     A general information notifier
     about a process status to the user. 
*/
package Reports.DyedCotton;

import java.io.*;
import java.util.Vector;

public class DyedInformation
{

     DyedInformation()
     {
          
     }


     public void flashMessage(Vector VInfo,Vector VValue,Vector VPrint, PrintWriter out)
     {
     
          out.println("<html>");
      
          out.println("<head>");
          out.println("<title>Name of the Report</title>");
          out.println("</head>");
      
          out.println("<body bgcolor='#C0C0C0'>");
      

          out.println("<div align='center'>");
          out.println("  <center>");

            
          out.println("  <table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='634' height='147' id='AutoNumber1'>");
          for(int i=0;i<VInfo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='220' height='32' bgcolor='#9AA8D6' bordercolor='#C0C0C0'><b><font color='#FFFFFF'>"+(String)VInfo.elementAt(i)+"</font></b></td>");
               out.println("      <td width='368' height='32' bgcolor='#FFFFFF' bordercolor='#C0C0C0'><b><font color='#9AA8D6'>"+(String)VValue.elementAt(i)+"</font></b></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");

          Vector VHead        = (Vector)VPrint.elementAt(0);
          Vector VGiNo        = (Vector)VPrint.elementAt(1);
          Vector VDate        = (Vector)VPrint.elementAt(2);
          Vector VSupplier    = (Vector)VPrint.elementAt(3);
          Vector VCode        = (Vector)VPrint.elementAt(4);
          Vector VName        = (Vector)VPrint.elementAt(5);
          Vector VInvQty      = (Vector)VPrint.elementAt(6);
          Vector VInvNo       = (Vector)VPrint.elementAt(7);
          Vector VInvDate     = (Vector)VPrint.elementAt(8);
          Vector VDcNo        = (Vector)VPrint.elementAt(9);
          Vector VDcDate      = (Vector)VPrint.elementAt(10);
          Vector VInfo11      = (Vector)VPrint.elementAt(11);
          Vector VInfo12      = (Vector)VPrint.elementAt(12);
          Vector VInfo13      = (Vector)VPrint.elementAt(13);
          Vector VInfo14      = (Vector)VPrint.elementAt(14);
          Vector VInfo15      = (Vector)VPrint.elementAt(15);
          Vector VInfo16      = (Vector)VPrint.elementAt(16);


          /*out.println(" <table border='1' width='1800' height='20' cellpadding='0' cellspacing='0'   >");
          out.println("       <tr>"); 
          for(int i=0;i<VHead.size();i++)
          {
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VHead.elementAt(i)+"</td>");
          }
          out.println(" </tr>");
          out.println("</table>");*/

          out.println(" <table border='1' width='1800' height='20' cellpadding='0' cellspacing='0'   >");
          int m=0;
          for(int i=0;i<VDate.size();i++)
               {
                    if(m==0)
                    {
                         out.println("       <tr>"); 
                         for(int j=0;j<VHead.size();j++)
                         {
                                   out.println("            <td =width='150' height='40' bgcolor='#9aadff' >"+(String)VHead.elementAt(j)+"</td>");
                         }
                         out.println(" </tr>");
                    }               
                    m=1;
                    out.println(" <tr>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VGiNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VSupplier.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VCode.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VName.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInvQty.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInvNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInvDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDcNo.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VDcDate.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo11.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo12.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo13.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo14.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo15.elementAt(i)+"</td>");
                    out.println("            <td =width='150' height='20' bgcolor='#9aa8d6' >"+(String)VInfo16.elementAt(i)+"</td>");
                    
                    out.println(" </tr>");
                    
               }
          
          out.println("</table>");


          out.println("</body>");
          out.println("</html>");
     }

}
