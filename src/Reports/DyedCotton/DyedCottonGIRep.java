package Reports.DyedCotton;
import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import rndi.*;

public class DyedCottonGIRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;

     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out     = response.getWriter();
          session             = request.getSession(false);
          String SServer      = (String)session.getValue("Server");

          String SDyer        = common.parseNull(request.getParameter("DyerSelect"));
          String SMaterial    = common.parseNull(request.getParameter("MatSelect"));
          String SStDate      = common.pureDate(request.getParameter("TStDate"));
          String SEnDate      = common.pureDate(request.getParameter("TEnDate"));
          String SFile        = common.parseNull(request.getParameter("TFile"));

          SFile = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/security1.1/src/temp/DyedMaterialGIList.prn":SFile;

          DyedInformation info  = new DyedInformation();

          DyedCottonGIList theList       = new DyedCottonGIList(SDyer,SMaterial,SStDate,SEnDate,SFile);
          Vector vect                   = new Vector();  
          vect                          = theList.getPrintValues();

          Vector VInfo  = new Vector();
          Vector VValue = new Vector();

          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");

          VValue.addElement("Dyed Material Inward  List as on a Particular Date");
          VValue.addElement("2(e)");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());


          info.flashMessage(VInfo,VValue,vect,out);

          
                             
          out.close();
     }

}
