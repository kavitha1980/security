
package Reports.ErrectorIn;

import util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import rndi.*;

public class ErrectorRegisterAsRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;

     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
		   System.out.println("In ErrectorRegisterAsRep 1");

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
           System.out.println("In ErrectorRegisterAsRep2");
		  
		  response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");

          String SEnDate   = common.pureDate(request.getParameter("TEnDate"));
          String SFile     = common.parseNull(request.getParameter("TFile"));

          SFile = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/ErrectorAs.prn":SFile;
		  
		  System.out.println("In ErrectorRegisterAsRep 3");
          ErrectorAsInformation info  = new ErrectorAsInformation();

          System.out.println("In ErrectorRegisterAsRep 4");
		  
		  ErrectorRegisterAsList1 theList    = new ErrectorRegisterAsList1(SEnDate,SEnDate,SFile);
		  
		  System.out.println("In after  ErrectorRegisterAsList");

          Vector vect            = new Vector();  
          vect                   = theList.getPrintValues();

          Vector VInfo  = new Vector();
          Vector VValue = new Vector();

          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");

          VValue.addElement("Errector's  List as on a Particular Date");
          VValue.addElement(" ");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());

          info.flashMessage(VInfo,VValue,vect,out);
                                             
          out.close();
     }
}
