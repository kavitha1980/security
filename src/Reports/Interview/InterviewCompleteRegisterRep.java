package Reports.Interview;
import util.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import rndi.*;

public class InterviewCompleteRegisterRep extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;

     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out     = response.getWriter();
          session             = request.getSession(false);
          String SServer      = (String)session.getValue("Server");

         // String SStDate      = common.pureDate(request.getParameter("TStDate"));
          String SEnDate      = common.pureDate(request.getParameter("TEnDate"));
          String SFile        = common.parseNull(request.getParameter("TFile"));
          String SResult      = common.parseNull(request.getParameter("Result"));
          String SCatogory    = common.parseNull(request.getParameter("Catogory"));
          String SAgent       = common.parseNull(request.getParameter("Agent"));
          String SPlace        = common.parseNull(request.getParameter("Place"));
          String SQualification  = common.parseNull(request.getParameter("Qualification"));


          SFile = (SFile.trim()).length()==0?"//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/Interview.prn":SFile;

          InterviewCompleteInformation info  = new InterviewCompleteInformation();

          InterviewCompleteList theList    = new InterviewCompleteList(SEnDate,SFile,SResult,SCatogory,SAgent,SPlace,SQualification);
          Vector vect            = new Vector();  
          vect                   = theList.getPrintValues();

          Vector VInfo  = new Vector();
          Vector VValue = new Vector();

          VInfo.addElement("Report Name");
          VInfo.addElement("Report Code");
          VInfo.addElement("Report url/File Name");
          VInfo.addElement("Status");

          VValue.addElement("Interview  List as on a Particular Date");
          VValue.addElement(" ");
          VValue.addElement(SFile);
          VValue.addElement(theList.getStatus());


          info.flashMessage(VInfo,VValue,vect,out);

          
                             
          out.close();
     }

}
