package Reports.Interview;
import util.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.registry.*;
import blf.*;
import domain.jdbc.*;
         
import javax.servlet.*;
import javax.servlet.http.*;


public class InterviewCompleteCrit extends HttpServlet
{
     HttpSession session;
     Common common = new Common();
     JDBCConnection theConnect;
     Connection connect;
     Vector VResult,VCatogory,VPlace,VAgent,VQualification;
     String SResult;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          setDataintoVector();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session         = request.getSession(false);
          String SServer  = (String)session.getValue("Server");


          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6'>");
          out.println("");
          out.println("<base target='main'>");
          out.println("<form method='GET' action='"+SServer+"Reports.Interview.InterviewCompleteRegisterRep'>");
          out.println("  <center>");
          out.println("<div align='center'>");
          out.println("  <table border='1' width='600' height='14'>");

         /* out.println("<table>");                                     
          out.println("<tr>");
          out.println("     <td><b>As On </b></td>");
          out.println("     <td><b>File</b></td>");
          out.println("     <td></td>");
          out.println("    <td></td>");
          out.println("<tr>");*/

          out.println("<tr>");
          //out.println("     <td>As On<input type='text' name='TStDate' size='15'></td>");
          out.println("     <td>As On<input type='text' name='TEnDate' size='15'></td>");
          out.println("     <td>File<input type='text' name='TFile' size='15'></td>");

          out.println("<tr>");
          out.println("      <td>Result<select  name='Result'>");
         // out.println("    <tr>");
        //  out.println("  <td><font color='#FFFF66'><b>Godown</b></font></td>");
        //  out.println("  <td> <select name ='GoDownCode'>");
          out.println("  <option value=0>UnKnown</option>");
          out.println("  <option value=1>Selected</option>");
          out.println("  <option value=2>Not-Selected</option>");
          out.println("  <option value='All'>All</option>");

          out.println("</select></td>");
          out.println("</tr>"); 

         /* for(int i=0;i<VResult.size();i++)
          {
               out.println("      <option value='"+VResult.elementAt(i)+"'>"+VResult.elementAt(i)+"</option>");
          }
          out.println("      </select>"); */

          out.println("<tr>");
          out.println("      <td>Catogory<select  name='Catogory'>");
          for(int i=0;i<VCatogory.size();i++)
          {
               out.println("      <option value='"+VCatogory.elementAt(i)+"'>"+VCatogory.elementAt(i)+"</option>");
          }
          out.println("      </select>");

         // out.println("<tr>");
          out.println("      <td>Agent<select  name='Agent'>");
          for(int i=0;i<VAgent.size();i++)
          {
               out.println("      <option value='"+VAgent.elementAt(i)+"'>"+VAgent.elementAt(i)+"</option>");

          }
          out.println("      </select>");


          out.println("<tr>");
          out.println("      <td>Place<select  name='Place'>");
          for(int i=0;i<VPlace.size();i++)
          {
               out.println("      <option value='"+VPlace.elementAt(i)+"'>"+VPlace.elementAt(i)+"</option>");
          }
          out.println("      </select> ");




        // out.println("<tr>");
          out.println("      <td>Qualification<select  name='Qualification'>");
          for(int i=0;i<VQualification .size();i++)
          {
               out.println("      <option value='"+VQualification.elementAt(i)+"'>"+VQualification.elementAt(i)+"</option>");
          }
          out.println("      </select> </td>");

          out.println("<tr>");
          out.println("     <td><input type='submit' value='Submit' name='B1' style='color: #003300; font-size: 10pt; font-weight: bold'></td>");
          out.println("     <td><input type='reset' value='Reset' name='B2' style='color: #800000; font-size: 10pt; font-weight: bold'></td>");
          out.println("<tr>");

          out.println("</table>");

          out.println("</form>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }

     public void setDataintoVector()
     {
          System.out.println("Entering...");
         // VResult   =new Vector();
          VCatogory = new Vector();
          VAgent = new Vector();
          VPlace = new Vector();
          VQualification = new Vector();
          String QS1="";
          String QS2="";
          String QS3="";
          String QS4="";
        //  String QS5="";
          try
          {

               QS1= "Select distinct categoryname From interviewnames Order By 1" ;
               QS2= "Select distinct Agentname From interviewnames Order By 1";
               QS3= "Select distinct Place From interviewnames Order By 1";
               QS4= "Select distinct Quali From interviewnames Order By 1";
             //  QS5="Select distinct result from interviewnames" ;

               theConnect = JDBCConnection.getJDBCConnection();
               connect      = theConnect.getConnection();
               Statement theStatement   =connect.createStatement();
               ResultSet rs = theStatement.executeQuery(QS1);

               while(rs.next())
               {
                    VCatogory.addElement(rs.getString(1));
               }
               VCatogory  . addElement("All");
               System.out.println(VCatogory.size());
               rs.close();
                rs = theStatement.executeQuery(QS2);

               while(rs.next())
               {
                    VAgent.addElement(rs.getString(1));
                                       
               }

               VAgent . addElement("All");
               rs.close();
                rs = theStatement.executeQuery(QS3);

               while(rs.next())
               {
                    VPlace.addElement(rs.getString(1));
                                       
               }

               VPlace . addElement("All");
               rs.close();

                rs = theStatement.executeQuery(QS4);

               while(rs.next())
               {
                    VQualification.addElement(rs.getString(1));
                                       
               }


               VQualification . addElement("All");
               rs.close();
		
             /*  rs = theStatement.executeQuery(QS5);
               for(int i=0;i<VResult.size();i++)
               {
                 SResult = common.parseNull((String)VResult.elementAt(i));
               }
                    if(SResult.equals(""))
                    {
                         VResult.addElement("All");
                         
                    }     
                    else
                    {
                         int iResult = Integer.parseInt(SResult);
                         if(iResult==0)
                              VResult.addElement("UnKnown ");
                         if(iResult==1)
                              VResult.addElement("Selected");
                         if(iResult==2)
                              VResult.addElement("Not-Selected");
                            
                            

                    }

               while(rs.next())
               {
                    VResult.addElement(rs.getString(1));
                                       
               }


              // VResult . addElement("All");
               rs.close();  */

		theStatement.close();
               
          }     
          catch(Exception ex)
          {
               ex.printStackTrace();
          }



          

     }

}
