/*
     The God Frame contains
     All Frames in use by the inventory reporting system

     see the following servlets forming the individual frames

          AmarLogo1
          InvCrit
          DirectionPanel
          InvDisplay
*/

package domain.servlet;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import util.*;
public class SecMainFrame extends HttpServlet
{
     HttpSession session;
     String SSelection = "";
     Common common;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out= response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");
          int iVal         = common.toInt((String)request.getParameter("selected"));

          out.println("<html>");
          out.println(" <frameset cols='200,*'>");
          out.println(" <frame name='contents' target='main' src='"+SServer+"DirectionPanel' scrolling='auto'>");
          out.println(" <frame name='main' src='"+SServer+"SecCrit' target='_self'>");
          out.println(" <noframes>");
          out.println(" <body>");
          out.println(" <p>This page uses frames, but your browser doesn't support them.</p>");
          out.println(" </body>");
          out.println(" </noframes>");
          out.println("</frameset>");

          out.println("</html>");

          out.close();
     }
}
