 /*


*/
package domain.servlet;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;
import rndi.*;
import blf.*;

import java.sql.*;
//import domain.jdbc.*;

import domain.servlet.jreporter.*;

public class ErrectorCrit extends HttpServlet implements rndi.CodedNames
{
     HttpSession session;

     PropertiesHandler theHandler;
     Vector VCompany,VCompCode;
     vital theData;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          theHandler = new PropertiesHandler("Errector");
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out = response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");
          session.putValue("Server",SServer);

          setVital();
          setDataVector();

          out.println("<html>");
          out.println("<body bgcolor='#9AA8D6' >");
          out.println("<form method='GET' action='"+SServer+"errector.Errector'>");
          out.println("  <center><font color='#FFFFFF' size='5'><b>Errector Register</b><br><br></font></center>");
          out.println("  <center>");
          out.println("<div align='center'>");
          out.println("  <table border='1' width='600' height='14'>");


          out.println("    <tr>");
          out.println("      <td><b><font color='#FFFF66' size='3'>Company</font></b></td>");
          out.println("      <td><select  name='company'>");

          for(int i=0; i<VCompCode.size(); i++)
          {
               out.println("<option value='"+(String)VCompCode.elementAt(i)+"'>"+VCompany.elementAt(i)+"</option>");
          }
          out.println("      </select></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><font color='#FFFF66'><b>Start Date</b></font></td>");
          out.println("      <td><input type='text' size='10' name='stDate'><font color='#FFFF66'><b>(dd/mm/yyyy)</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><font color='#FFFF66'><b>End Date</b></font></td>");
          out.println("      <td><input type='text' size='10' name='endDate'><font color='#FFFF66'><b>(dd/mm/yyyy)</b></font></td>");
          out.println("    </tr>");

          out.println("  </table>");
          out.println("</div>");

          try
          {
               theHandler.toHTML(out);
          }
          catch(Exception ex){}

          out.println("  <font color='#800000' face='Arial'><input type='submit' value='Submit' name='B1' style='font-family: Arial'></font><input type='reset' value='Reset' name='B2'></h1>");
          out.println("  </center>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          out.close();
     }
     private void setVital()
     {
          try
          {
               Registry registry   = LocateRegistry.getRegistry(RMIHOST,RMIPORT);
               theData             = (vital)registry.lookup(SECURITYDOMAIN);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private void setDataVector()
     {
          try
          {
               VCompany       = theData.getData("COMPANY");
               VCompCode      = theData.getData("COMPANYCODE");

               VCompany    .insertElementAt("ALL",0);
               VCompCode   .insertElementAt("999999",0);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}

