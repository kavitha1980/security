package domain.servlet;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DirectionPanel extends HttpServlet
{
     HttpSession session;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          session          = request.getSession(false);
          String SServer   = (String)session.getValue("Server");

          out.println("<html>");
          out.println("<head>");
          out.println("<base target='main'>");
          out.println("</head>");
          out.println("<body bgcolor='#C0C0C0' link='#003366' vlink='#005500' alink='#002346'>");
          out.println("<div align='left'>");
          out.println("<table border='0' width='270' bgcolor='#C0C0C0' cellspacing='0' cellpadding='0'>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#005500' size='6'><b>Reports</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#990033' size='4'><b>Guest</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='"+SServer+"GuestHistoryCrit'><b>Guest IN & OUT</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><hr></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#990033' size='4'><b>Vehicle</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='"+SServer+"VehicleHistoryCrit'><b>Vehicle IN & OUT</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><hr></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#990033' size='4'><b>Visitor</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='"+SServer+"VisitorCrit'><b>Visitor IN & OUT</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><hr></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='29'><font color='#990033' size='4'><b>Errector</b></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='228' height='13'><font color='#003366'><a href='"+SServer+"ErrectorCrit'><b>Errector IN & OUT</b></a></font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td><hr></td>");
          out.println("    </tr>");

          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
}



