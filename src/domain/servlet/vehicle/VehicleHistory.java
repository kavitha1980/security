package domain.servlet.vehicle;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;

import rndi.*;
import domain.servlet.jreporter.*;

public class VehicleHistory extends HttpServlet implements CodedNames
{

     HttpSession session;
     String SServer="";
     String SEndDate="",SStDate="";
     String SVehicle;
     String SVehicleSelection = "";

     int  iStDate=0,iEnDate=0;

     Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          response.setContentType("text/html");
          PrintWriter out    = response.getWriter();
          session            = request.getSession(false);
          SServer            = (String)session.getValue("Server");
          SVehicle           = (String)request.getParameter("vehicle"); 
          
          iStDate            = common.toInt(common.pureDate(request.getParameter("stDate")));
          iEnDate            = common.toInt(common.pureDate(request.getParameter("endDate")));

          if(!SVehicle.equals(String.valueOf(999999)))
               SVehicleSelection  = " And VehicleNo = '"+SVehicle+"' ";

          System.out.println("Vehicle:"+SVehicle);
          System.out.println("VehicleSelect:"+SVehicleSelection);

          /*
          iStDate            = common.toInt((String)request.getParameter("stDate"));
          iEnDate            = common.toInt((String)request.getParameter("endDate"));
          */
          System.out.println("StDate :"+iStDate);
          System.out.println("EnDate :"+iEnDate);

          SEndDate           = common.pureDate(request.getParameter("endDate"));
          SStDate            = common.pureDate(request.getParameter("stDate")); 


          out.println("<html>");
          out.println("<head>");
          out.println("<title>Vehicle Movement Register</title>");
          out.println("</head>");
          out.println("");
          out.println("<body bgcolor='#9AA8D6'>");

          try
          {
               setSlicedReport(out,request);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          out.print("</body>");
          out.print("</html>");
          out.close();
     }

     private void setSlicedReport(PrintWriter out,HttpServletRequest request) throws Exception
     {
          try
          {

               PropertiesHandler theHandler = new PropertiesHandler("VehicleHistory");

               String SHead[]     = theHandler.getHead();
               String SHead2[]    = theHandler.getHead2();
               String SMap[]      = theHandler.getMap();
               String SMapType[]  = theHandler.getType();
               int    iMapWidth[] = theHandler.getMapWidth();

               int iDisp[]        = theHandler.getConst(request,"Display");
               int iGroup[]       = theHandler.getConst(request,"Group");
               int iSum[]         = theHandler.getConst(request,"Sum");
               int iDec[]         = theHandler.getConst(request,"Dec");

               String SStart      = common.parseDate(String.valueOf(iStDate));
               String SEnd        = common.parseDate(String.valueOf(iEnDate));

               SliceManager sm    = new SliceManager(SHead,SHead2,SMap,SMapType);
               sm.setConstraint(iDisp,iGroup,iSum,iDec);
               sm.setMapWidth(iMapWidth);
               sm.setInterval(" | ");
               sm.setTitle("Vehicle Movement Register");
               sm.setLimits(SStart,SEnd);

               try
               {
                    sm.setHistory(getVehicleQS());
               }
               catch(Exception e){ }
               sm.toHTML(out);
                    
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/temp/VehicleReg.prn");
               sm.toFile(FW);
               FW.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getVehicleQS()
     {              
          String QS =    " Select VehicleNo,VehicleName,OutDate,OutTime,        "+
                         " InDate,InTime,StKm,EndKm,Place,Purpose,DriverName,   "+
                         " SecurityName,DiNo,KiNo,DiDate,KiDate,DQty,KQty,      "+
                         " BunkName From Vehicles "+
                         " Where OutDate>="+iStDate+" And OutDate<="+iEnDate+" "+
                         " And Status = 1 "+SVehicleSelection+" Order by OutDate,Stkm ";

          try
          {
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/VehicleReg.sql");
               FW.write(QS);
               FW.close();
          }
          catch(Exception e){e.printStackTrace();}
          return QS;
     } 
}

