package domain.servlet.visitor;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;

import rndi.*;
import domain.servlet.jreporter.*;

public class Visitor extends HttpServlet implements rndi.CodedNames
{

     HttpSession session;
     String SServer="";
     String SEndDate="",SStDate="";
     String SVisitor;
     String SVisitorSelection = "";

     int  iStDate=0,iEnDate=0;

     Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          response.setContentType("text/html");
          PrintWriter out    = response.getWriter();
          session            = request.getSession(false);
          SServer            = (String)session.getValue("Server");
          
          iStDate            = common.toInt(common.pureDate(request.getParameter("stDate")));
          iEnDate            = common.toInt(common.pureDate(request.getParameter("endDate")));

          SEndDate           = common.pureDate(request.getParameter("endDate"));
          SStDate            = common.pureDate(request.getParameter("stDate")); 

          SVisitor           = (String)request.getParameter("company");

          if(!SVisitor.equals(String.valueOf(999999)))
               SVisitorSelection = " And Visitor.CompanyCode = "+SVisitor+" ";

          out.println("<html>");
          out.println("<head>");
          out.println("<title>Visitors Register</title>");
          out.println("</head>");
          out.println("");
          out.println("<body bgcolor='#9AA8D6'>");

          try
          {
               setSlicedReport(out,request);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          out.print("</body>");
          out.print("</html>");
          out.close();
          SVisitorSelection = "";
     }

     private void setSlicedReport(PrintWriter out,HttpServletRequest request) throws Exception
     {
          try
          {

               PropertiesHandler theHandler = new PropertiesHandler("Visitor");

               String SHead[]     = theHandler.getHead();
               String SHead2[]    = theHandler.getHead2();
               String SMap[]      = theHandler.getMap();
               String SMapType[]  = theHandler.getType();
               int    iMapWidth[] = theHandler.getMapWidth();

               int iDisp[]        = theHandler.getConst(request,"Display");
               int iGroup[]       = theHandler.getConst(request,"Group");
               int iSum[]         = theHandler.getConst(request,"Sum");
               int iDec[]         = theHandler.getConst(request,"Dec");

               String SStart      = common.parseDate(String.valueOf(iStDate));
               String SEnd        = common.parseDate(String.valueOf(iEnDate));

               SliceManager sm    = new SliceManager(SHead,SHead2,SMap,SMapType);
               sm.setConstraint(iDisp,iGroup,iSum,iDec);
               sm.setMapWidth(iMapWidth);
               sm.setInterval(" | ");
               sm.setTitle("Visitors Register");
               sm.setLimits(SStart,SEnd);

               try
               {
                    sm.setHistory(getVisitorQS());
               }
               catch(Exception e){ }
               sm.toHTML(out);
                    
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/temp/Visitor.prn");
               sm.toFile(FW);
               FW.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getVisitorQS()
     {              
          String QS = " SELECT COMPANY.NAME as CompName,REPRESENTATIVE.NAME as RepName,VISITOR.VISITORDATE as VDate, "+
                      " VISITORPURPOSE.NAME as PurName,VISITORSTAFF.EMPNAME as EmpName,INTIME,OUTTIME "+
                      " FROM VISITOR "+
                      " INNER JOIN COMPANY ON COMPANY.CODE = VISITOR.COMPANYCODE "+
                      " INNER JOIN REPRESENTATIVE ON REPRESENTATIVE.CODE = VISITOR.REPCODE "+
                      " INNER JOIN VISITORPURPOSE ON VISITORPURPOSE.CODE = VISITOR.PURPOSECODE "+
                      " INNER JOIN VISITORSTAFF ON VISITORSTAFF.EMPCODE = VISITOR.STAFFCODE "+
                      " WHERE VISITORDATE >= "+iStDate+" AND VISITORDATE <= "+iEnDate+" "+SVisitorSelection+" ";
          try
          {
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/Visitor.sql");
               FW.write(QS);
               FW.close();
          }
          catch(Exception e){e.printStackTrace();}
          return QS;
     } 
}

