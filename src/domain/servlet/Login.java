/*
     The Main Servlet Starter.
*/
package domain.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import rndi.*;

public class Login extends HttpServlet implements CodedNames
{
     HttpSession    session;
     String         SServer;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          session          = request.getSession(true);

          session.putValue("Server","http://localhost:2010/servlet/domain.servlet."); 
          SServer = "http://localhost:2010/servlet/domain.servlet."; 

          out.println("<html>");
          out.println("<head><title>AMARJOTHI SPINNING MILLS LIMITED</title>");
          /*
          out.println("<script language='JavaScript' fptype='dynamicanimation'>");
          out.println("<!--");
          out.println("function dynAnimation() {}");
          out.println("function clickSwapImg() {}");
          out.println("//-->");
          out.println("</script>");
          out.println("<script language='JavaScript1.2' fptype='dynamicanimation' src='file://///"+SCRIPTHOST+"/e/Security/src/animate.js'>");
          out.println("</script>");
          */
          out.println("</head>");

          //out.println("<body bgcolor='#9AA8D6' onload='dynAnimation()' language='Javascript1.2'>");
          out.println("<body bgcolor='#9AA8D6'> ");
          out.println("<form method='POST' action='"+SServer+"Login'>");
          out.println("<font color='#FFFFFF' size='5'><b>Security Monitoring System</b><br><br></font>");
          out.println("<font color='#FFFFFF' size='4'><b>Amarjothi Spinning Mills Ltd</b><br></font></td>");
          out.println("<font color='#FFFFFF' size='3'>Pudusuripalayam<br></font></td>");
          out.println("<font color='#FFFFFF' size='3'>Nambiyur<br><br><br></font></td>");
          out.println("<center>");
          //out.println("<h1 dynamicanimation='fpAnimelasticBottomFP1' id='fpAnimelasticBottomFP1' style='position: relative !important; visibility: hidden' language='Javascript1.2'><font color='#800000' face='Arial'><input type='submit' value='Submit' name='B1' style='font-family: Arial'></font><input type='reset' value='Reset' name='B2'></h1>");
          out.println("<font color='#800000' face='Arial'><input type='submit' value='Submit' name='B1' style='font-family: Arial'></font><input type='reset' value='Reset' name='B2'></h1>");
          out.println("</center>");
          out.println("</form>");
          out.println("<hr color='000099' size='1'>");
          out.println("<p>");
          out.println("<p>");
          out.println("<center>");
          out.println("<font color='#FFFFFF' size='3'><b>Developed by The Department of Information Technology-AJSM Limited.<br></b></font>");
          out.println("<font color='#FFFFCC' size='3'>Dedicated to the people of Java and Oracle community, who invented this technology and makes us to pay<br></font>");
          out.println("<font color='#FFFFCC' size='3'>more concentration on the Business Logic.<br></font>");
          out.println("</center>");
          out.println("<hr color='000099' size='1'>");
          out.println("</body>");
          out.println("</html>");
          out.close();

     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          session       = request.getSession(false);
          SServer = (String)session.getValue("Server");
          response.sendRedirect(SServer+"SecMainFrame");
     }
}
