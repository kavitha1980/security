/*
          The Abstraction of 
          storage and retreival logics
*/
package domain.servlet.jreporter;

import java.util.*;
import util.*;
public abstract class AbstractHistoryModel
{
     protected String SHead[]; 
     protected String SBody[];

     protected Vector  VHead;
     protected Vector  VBody;

     Common common = new Common();

     public AbstractHistoryModel()
     {
     }

     public void setMap(String SHead[],String SBody[])
     {
          this.SHead = SHead;
          this.SBody = SBody;

          VHead        = new Vector();
          VBody        = new Vector();
          createComponents();
     }

     public void setValue(String SMap,String SValue)
     {
          int index = headIndexOf(SMap);
          if(index == -1)
               return; 
          VHead.setElementAt(common.parseNull(SValue),index);          
     }

     public void appendValue(String SMap,String SValue)
     {
          int index = bodyIndexOf(SMap);
          if(index == -1)
               return;
          Vector vect = (Vector)VBody.elementAt(index);
          vect.addElement(common.parseNull(SValue));
     }

     public void appendValue(String SMap,String SValue,String SType)
     {
          int index = bodyIndexOf(SMap);
          if(index == -1)
               return;

          SValue = common.parseNull(SValue);

          if(SType.equals("T"))
               SValue = common.parseDate(SValue);

          Vector vect = (Vector)VBody.elementAt(index);
          vect.addElement(SValue);
     }

     public void setAppendedValue(String SMap,String SValue,int iRow)
     {
          int index = bodyIndexOf(SMap);
          if(index == -1)
               return;
          Vector vect = (Vector)VBody.elementAt(index);
          vect.setElementAt(common.parseNull(SValue),iRow);
     }

     public String getValue(String SMap)
     {
          int index = headIndexOf(SMap);
          if(index == -1)
               return "";
          return (String)VHead.elementAt(index);
     }
     public String getValue(String SMap,int iRow)
     {
          int index = bodyIndexOf(SMap);
          
          if(index == -1)
               return "";
          try
          {
               Vector vect = (Vector)VBody.elementAt(index);
               return (String)vect.elementAt(iRow);
          }
          catch(Exception ex){System.out.println(ex);}
          return "";
     }

     public int size()
     {
          if(VBody.size()==0)
               return 0;
          Vector vect = (Vector)VBody.elementAt(0);
          return vect.size();
     }

     private void createComponents()
     {
          for(int i=0;i<SHead.length;i++)
          {
               VHead.addElement("");
          }
          for(int i=0;i<SBody.length;i++)
          {
               VBody.addElement(new Vector());
          }
     }

     private int headIndexOf(String SMap)
     {
          for(int i=0;i<SHead.length;i++)
          {
               if(SHead[i].equals(SMap))
                    return i;
          }
          return -1;
     }

     private int bodyIndexOf(String SMap)
     {
          for(int i=0;i<SBody.length;i++)
          {
               if(SBody[i].equals(SMap))
                    return i;
          }
          return -1;
     }
}
