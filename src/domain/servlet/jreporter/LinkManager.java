/*
     A Manager Class to manage the transactions
     thro collection of slices

     Slice Manager adapted to provide Links in Report;
*/

package domain.servlet.jreporter;

import java.io.*;
import java.sql.*;
import java.util.*;

import domain.jdbc.*;
import util.*;

public class LinkManager
{
     Connection theConnection = null;

     protected String SHead[],SHead2[],SHead3[],SPreviousValue[],SMap[],SMapType[];
     protected int    iMapWidth[];
     protected String SInt;

     protected String SStart,SEnd;      // The Major Limits
     protected String STitle;

     protected int iDisp[];          // Displayable
     protected int iGroup[];         // Groupable 
     protected int iSum[];           // Summable
     protected int iDec[];           // The Decimal Places
     protected int iSuppress[];      // Suppress if Duplicate
     protected int iProgram[];       // Accessible only through program
     protected int iSummary[];       // to enable Summary on the footer
     protected int iIsLink[];        // to enable HyperLink
     protected int iLinkData[];      // HyperLink parameter field index.     

     protected double dFoot[];
     protected double dGroupFoot[];

     protected Vector VSlice;                  // The Collection of Slices

     Common common = new Common();
     protected int    Pctr=0,Lctr=70;
     String SServerDate;
     //String SServer="http://localhost:7070/servlet/domain.servlet.";

     String SServer      = "";
     String SRelativeURL = "";
     int iDisplayIndex   = 0 ;
     int iLinkIndex      = 0 ;
     boolean bShowLink   = false;

     int iMixLotIndex  = 0 ;
     int iWebTypeIndex = 0 ;
     int iNoilsIndex   = 0 ;
     int iBinCodeIndex = 0 ;
     int iDateIndex    = 0 ;  
     int i2ndLink      = 0 ;

     boolean bShowWebLink      = false;
     boolean bShowBinWebLink   = false;
     String SFormula = "";
     double dPercentage;

     public LinkManager(String SHead[],String SMap[],String SMapType[])
     {
          this.SHead    = SHead;
          this.SMap     = SMap; 
          this.SMapType = SMapType;
          VSlice        = new Vector();
     }

     public LinkManager(String SHead[],String SHead2[],String SMap[],String SMapType[])
     {
          this.SHead    = SHead;
          this.SHead2   = SHead2;
          this.SMap     = SMap; 
          this.SMapType = SMapType;
          VSlice        = new Vector();
     }

     public LinkManager(String SHead[],String SHead2[],String SHead3[],String SMap[],String SMapType[])
     {
          this.SHead    = SHead;
          this.SHead2   = SHead2;
          this.SHead3   = SHead3;
          this.SMap     = SMap; 
          this.SMapType = SMapType;
          VSlice        = new Vector();
     }



     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[])
     {
          this.iDisp    = iDisp;
          this.iGroup   = iGroup;
          this.iSum     = iSum;
          this.iDec     = iDec;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[],int iSuppress[])
     {
          this.iDisp      = iDisp;
          this.iGroup     = iGroup;
          this.iSum       = iSum;
          this.iDec       = iDec;
          this.iSuppress  = iSuppress;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[],int iSuppress[],int iProgram[])
     {
          this.iDisp      = iDisp;
          this.iGroup     = iGroup;
          this.iSum       = iSum;
          this.iDec       = iDec;
          this.iSuppress  = iSuppress;
          this.iProgram   = iProgram;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[],int iSuppress[],int iProgram[],int iSummary[])
     {
          this.iDisp      = iDisp;
          this.iGroup     = iGroup;
          this.iSum       = iSum;
          this.iDec       = iDec;
          this.iSuppress  = iSuppress;
          this.iProgram   = iProgram;
          this.iSummary   = iSummary;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[],int iSuppress[],int iProgram[],int iSummary[],int iIsLink[],int iLinkData[])
     {
          this.iDisp      = iDisp;
          this.iGroup     = iGroup;
          this.iSum       = iSum;
          this.iDec       = iDec;
          this.iSuppress  = iSuppress;
          this.iProgram   = iProgram;
          this.iSummary   = iSummary;
          this.iIsLink    = iIsLink;
          this.iLinkData  = iLinkData;
     }


     public void setMapWidth(int iMapWidth[])
     {
          this.iMapWidth = iMapWidth;
     }

     public void setInterval(String SInt)
     {
          this.SInt = SInt;
     }

     public void setLimits(String SStart,String SEnd)
     {
          this.SStart = SStart;
          this.SEnd   = SEnd;
     }

     public void setTitle(String STitle)
     {
          this.STitle = STitle;
     }

     public void setLink(String SServer,String SRelativeURL,int iDisplayIndex,int iLinkIndex,boolean bShowLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          this.iLinkIndex     = iLinkIndex;
          this.bShowLink      = bShowLink;
          bShowWebLink = false;

     }

     public void setLink(String SServer,String SRelativeURL,int iDisplayIndex,int iLinkIndex,boolean bShowLink,int i2ndLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          this.iLinkIndex     = iLinkIndex;
          this.bShowLink      = bShowLink;
          this.i2ndLink       = i2ndLink;
          bShowWebLink = false;

     }


     public void setWebLink(String SServer,String SRelativeURL,int iDisplayIndex,int iMixLotIndex,int iWebTypeIndex,int iNoilsIndex,boolean bShowWebLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          //this.iLinkIndex   = iLinkIndex;
          this.iMixLotIndex   = iMixLotIndex ;
          this.iWebTypeIndex  = iWebTypeIndex;
          this.iNoilsIndex    = iNoilsIndex  ;
          this.bShowWebLink   = bShowWebLink;
          bShowLink = true;
          bShowBinWebLink = false;
     }
     public void setDateWebLink(String SServer,String SRelativeURL,int iDisplayIndex,int iMixLotIndex,int iWebTypeIndex,int iNoilsIndex,int iDateIndex ,boolean bShowWebLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          //this.iLinkIndex   = iLinkIndex;
          this.iMixLotIndex   = iMixLotIndex ;
          this.iWebTypeIndex  = iWebTypeIndex;
          this.iNoilsIndex    = iNoilsIndex  ;
          this.iDateIndex     = iDateIndex;
          this.bShowWebLink   = bShowWebLink;
          bShowLink = true;
          //bShowBinWebLink = false;
     }


     public void setBinWebLink(String SServer,String SRelativeURL,int iDisplayIndex,int iMixLotIndex,int iWebTypeIndex,int iNoilsIndex,int iBinCodeIndex,boolean bShowWebLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          //this.iLinkIndex   = iLinkIndex;
          this.iMixLotIndex   = iMixLotIndex ;
          this.iWebTypeIndex  = iWebTypeIndex;
          this.iNoilsIndex    = iNoilsIndex  ;
          this.iBinCodeIndex  = iBinCodeIndex  ;
          this.bShowWebLink   = bShowWebLink;
          bShowLink = true;
          bShowBinWebLink = true;
     }



     public void setHistory(String QS)
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    organizeData(result);
               }
               organizeDate(stat);
               result.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void organizeDate(Statement stat) throws Exception
     {
          ResultSet result = stat.executeQuery(getDateQS());
          if(result.next())
               SServerDate = result.getString(1);
          result.close();
     }
     private String getDateQS()
     {
          String QS = "Select to_Char(sysdate,'dd.mm.yyyy hh:mi:ss') from Dual";
          return QS;
     }

     public void setHistory(SliceBroker SB)
     {
          try
          {
               for(int i=0;i<SB.size();i++)
               {
                    organizeData(SB.getData(i));
               }
          }
          catch(Exception ex)
          {
               System.out.println("setHistory : "+ex);
          }
     }

     public Vector getSlices()
     {
          return VSlice;
     }
     public void toHtml4Web(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody4Web(out);

     }

     public void toHTML(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody(out);

     }

     public void toHTML1(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody1(out);

     }

     public void toHTML5(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody5(out);

     }

     public void toHTML2(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody2(out);

     }

     public void toHTML3(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody3(out);
          setFoot3(out);

     }
     public void toHTML4(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody4(out);

     }
     public void toHTML6(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody6(out);

     }



     public void toFile(FileWriter FW) throws Exception
     {
          setHead(FW);
          setBody(FW);
     }

     public void toFile2(FileWriter FW) throws Exception
     {
          setHead2(FW);
          setBody(FW);
     }

     public void toFile1(FileWriter FW) throws Exception
     {
          setHead2(FW);
          setBody1(FW);
     }

     public void toFile3(FileWriter FW) throws Exception
     {
          setHead(FW);
          setBody3(FW);
          setFoot3(FW);
     }

     public void toFile4(FileWriter FW) throws Exception
     {
          setHead2(FW);
          setBody4(FW);

     }


     //--- The Internal Methods ----/

     protected void organizeData(ResultSet result) throws Exception
     {
          String SValue[] = new String[SMap.length];
          for(int i=0;i<SMap.length;i++)
          {
               SValue[i] = common.parseNull(result.getString(SMap[i]));
          }
          manageSlice(SValue);
     }

     protected void organizeData(HashMap result) throws Exception
     {
          String SValue[] = new String[SMap.length];
          for(int i=0;i<SMap.length;i++)
          {
               SValue[i] = (String)result.get(SMap[i]);
          }
          manageSlice(SValue);
     }

     private void manageSlice(String[] SValue)
     {
          String SKey[]     = getKeyValue(SValue);
          String SKeyHead[] = getKeyHead();

          Slice slice = findByKey(SKey);

          if(slice == null)
          {
               slice = new Slice(SKey,SKeyHead,SHead,SMap,SMapType,SValue);
               slice.setConstraint(iDisp,iGroup,iSum,iDec);
               slice.setMapWidth(iMapWidth);
               slice.setInterval(SInt);
               slice.setLimits(SStart,SEnd);
               slice.setTitle(STitle);
               VSlice.addElement(slice);
          }
          for(int i=0;i<SMap.length;i++)
          {
               slice.appendValue(SMap[i],SValue[i],SMapType[i]);
          }
     }

     private String[] getMapValue(String SValue[])
     {
          Vector vect = new Vector();
          for(int i=0;i<SValue.length;i++)
          {
               vect.addElement(SValue[i]);
          }
          String SKey[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKey[i] = (String)vect.elementAt(i);
          }
          return SKey;
     }


     private String[] getKeyValue(String SValue[])
     {
          Vector vect = new Vector();
          for(int i=0;i<SValue.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SValue[i]);
          }
          String SKey[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKey[i] = (String)vect.elementAt(i);
          }
          return SKey;
     }

     public String[] getKeyHead()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SHead[i]);
          }
          String SKeyHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyHead[i] = (String)vect.elementAt(i);
          }
          return SKeyHead;
     }

     public String[] getKeyHead2()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SHead2[i]);
          }
          String SKeyHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyHead[i] = (String)vect.elementAt(i);
          }
          return SKeyHead;
     }

     public String[] getKeyType()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMapType.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SMapType[i]);
          }
          String SKeyType[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyType[i] = (String)vect.elementAt(i);
          }
          return SKeyType;
     }

     public int[] getKeyWidth()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iMapWidth[i]));
          }
          int iKeyWidth[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iKeyWidth[i] = common.toInt((String)vect.elementAt(i));
          }
          return iKeyWidth;
     }


     public String[] getSumHead()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SHead[i]);
          }
          String SSumHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSumHead[i] = (String)vect.elementAt(i);
          }
          return SSumHead;
     }

     public String[] getDuplicateSuppressField()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iGroup[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iSuppress[i]));
          }
          String SSuppressField[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSuppressField[i] = (String)vect.elementAt(i);
          }
          return SSuppressField;
     }


     


     public int[] getZeroSuppressField()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iSuppress[i]));
          }

          int iSumIndex[] = new int[vect.size()];

          for(int i=0;i<vect.size();i++)
          {
               iSumIndex[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumIndex;

     }


     public String[] getSumHead2()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(SHead2[i]);
          }
          String SSumHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSumHead[i] = (String)vect.elementAt(i);
          }
          return SSumHead;
     }

     public int[] getSumWidth()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iMapWidth[i]));
          }
          int iSumWidth[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iSumWidth[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumWidth;
     }

     public int[] getSumIndex()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(i));
          }
          int iSumIndex[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iSumIndex[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumIndex;
     }

     public int[] getFooterIndex()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMap.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               if(iProgram[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iSummary[i]));
          }
          int iSumIndex[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iSumIndex[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumIndex;
     }


     private Slice findByKey(String SKey[])
     {
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               if(slice.isEqual(SKey))
               {
                    return slice;
               }
          }
          return null;
     }


     public void setHead(PrintWriter out) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          String SumHead[]  = getSumHead();
          int iColWidth = 40;

          if(common.toInt(SStart)==0 && common.toInt(SEnd)==0)
               out.println("<p><font color='#000099'><b>"+STitle+"</b></font></p>");
          else
               out.println("<p><font color='#000099'><b>Abstract on "+STitle+"&nbsp;"+SStart+"-"+SEnd+"</b></font></p>");

          out.println("<table border='1'>");
          out.println("  <tr>");
          out.println("  <td align='center'><b><font color='#FFFF00'>Sl No</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+KeyHead[i]+"</font></b></td>");
          }
          for(int i=0;i<SumHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+SumHead[i]+"</font></b></td>");
          }
          out.println("  </tr>");
          initSum();
     }

     public void setBody4Web(PrintWriter out) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                                   out.println("  <td>"+addLink(SKeyField[j])+"</td>");
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }

                    }
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot4Web(out);
     }


     public void setBody(PrintWriter out) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                                   out.println("  <td>"+addLink(SKeyField[j])+"</td>");
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }

                    }
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }

     private void setBody1(PrintWriter out) throws Exception
     {

          int iSumIndex[] = getSumIndex();
          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {

                    if(j==iDisplayIndex)
                    {
                         if(bShowBinWebLink && !bShowLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iBinCodeIndex]);
                         else if(bShowWebLink && !bShowLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex]);

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else 
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }

     /*private void setBody5(PrintWriter out) throws Exception
     {

          int iSumIndex[] = getSumIndex();
          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {

                    if(j==iDisplayIndex)
                    {

                         if(bShowBinWebLink && !bShowLink)
                              SLink = formatDateWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iBinCodeIndex]);
                         else if(bShowWebLink && !bShowLink)
                              SLink = formatDateWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iDateIndex]);

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else 
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }*/


     private void setBody2(PrintWriter out) throws Exception
     {

          int iSumIndex[] = getSumIndex();
          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {

                    if(j==iDisplayIndex)
                    {
                         if(bShowLink)
                              SLink = SData[iLinkIndex];

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else 
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }

     public void setBody3(PrintWriter out) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                                   out.println("  <td>"+addLink(SKeyField[j])+"</td>");
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }

                    }
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }

     }

     private void setBody4(PrintWriter out) throws Exception
     {
          initSum();
          initGroupSum();
          int iSumIndex[] = getSumIndex();
          //String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isNewGroup = false;
          String SKeyField[];
          String SPreviousKey[];
          String SDuplicateSuppressKey[];


          if(VSlice.size()>0)
          {

               Slice slice = (Slice)VSlice.elementAt(0);
               SKeyField   = slice.getKeyField();


               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();


          }
          else
               return;


          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               SKeyField = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               for(int j=0;j<SKeyField.length;j++)
               {
                    if((i>0) && (Integer.parseInt(SDuplicateSuppressKey[j])==1))
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isNewGroup=false;
                         else
                         {
                              isNewGroup=true;
                              break;
                         }
                    }    

               }

               for(int j=0;j<SKeyField.length;j++)
               {
                    SPreviousKey[j]=SKeyField[j];
               }

               if(isNewGroup)
               {
                    setGroupFooter(out);
               }

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(j==iDisplayIndex && bShowLink)
                    {
                         if(bShowBinWebLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iBinCodeIndex]);
                         else if(bShowWebLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex]);

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else
                         {
                              //SLink =SLink+SData[iLinkIndex];
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                         }
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

                    if(!isNewGroup)
                    {
                         dGroupFoot[index]=dGroupFoot[index]+slice.getSumOf(index);
                    }
                    else
                    {
                         dGroupFoot[index]=slice.getSumOf(index);
                    }

               }
               out.println("</tr>");
          }
          setGroupFooter(out);
          setFoot(out);
     }

     private void setBody6(PrintWriter out) throws Exception
     {
          initSum();
          initGroupSum();
          int iSumIndex[] = getSumIndex();
          //String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isNewGroup = false;
          String SKeyField[];
          String SPreviousKey[];
          String SDuplicateSuppressKey[];


          if(VSlice.size()>0)
          {

               Slice slice = (Slice)VSlice.elementAt(0);
               SKeyField   = slice.getKeyField();


               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();


          }
          else
               return;


          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               SKeyField = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               for(int j=0;j<SKeyField.length;j++)
               {
                    if((i>0) && (Integer.parseInt(SDuplicateSuppressKey[j])==1))
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isNewGroup=false;
                         else
                         {
                              isNewGroup=true;
                              break;
                         }
                    }    

               }

               for(int j=0;j<SKeyField.length;j++)
               {
                    SPreviousKey[j]=SKeyField[j];
               }

               if(isNewGroup)
               {
                    setGroupFooter(out);
               }

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(j==iDisplayIndex && bShowLink)
                    {
                         if(bShowBinWebLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iBinCodeIndex]);
                         else if(bShowWebLink)
                              SLink = formatWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex]);

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else
                         {
                              SLink = SData[iLinkIndex];
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                         }
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

                    if(!isNewGroup)
                    {
                         dGroupFoot[index]=dGroupFoot[index]+slice.getSumOf(index);
                    }
                    else
                    {
                         dGroupFoot[index]=slice.getSumOf(index);
                    }

               }
               out.println("</tr>");
          }
          setGroupFooter(out);
          setFoot(out);
     }


     private void setBody5(PrintWriter out) throws Exception
     {
          initSum();
          initGroupSum();
          int iSumIndex[] = getSumIndex();
          //String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isNewGroup = false;
          String SKeyField[];
          String SPreviousKey[];
          String SDuplicateSuppressKey[];


          if(VSlice.size()>0)
          {

               Slice slice = (Slice)VSlice.elementAt(0);
               SKeyField   = slice.getKeyField();


               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();


          }
          else
               return;


          String SLink ="";
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               SKeyField = slice.getKeyField();
               String SKeyType[] = getKeyType();
               String SData[]    = slice.getFieldData();

               for(int j=0;j<SKeyField.length;j++)
               {
                    if((i>0) && (Integer.parseInt(SDuplicateSuppressKey[j])==1))
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isNewGroup=false;
                         else
                         {
                              isNewGroup=true;
                              break;
                         }
                    }    

               }

               for(int j=0;j<SKeyField.length;j++)
               {
                    SPreviousKey[j]=SKeyField[j];
               }

               if(isNewGroup)
               {
                    setGroupFooter(out);
               }

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(j==iDisplayIndex && bShowLink)
                    {
                         if(bShowBinWebLink)
                              SLink = formatDateWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iBinCodeIndex]);
                         else if(bShowWebLink)
                              SLink = formatDateWebCode(SData[iMixLotIndex],SData[iWebTypeIndex],SData[iNoilsIndex],SData[iDateIndex]);

                         if(SKeyType[j].equals("T"))
                              out.println("  <td>"+addLink(common.parseDate(SKeyField[j]),SLink)+"</td>");
                         else 
                              out.println("  <td>"+addLink(SKeyField[j],SLink)+"</td>");
                    }
                    else
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }

               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

                    if(!isNewGroup)
                    {
                         dGroupFoot[index]=dGroupFoot[index]+slice.getSumOf(index);
                    }
                    else
                    {
                         dGroupFoot[index]=slice.getSumOf(index);
                    }

               }
               out.println("</tr>");
          }
          setGroupFooter(out);
          setFoot(out);
     }




     private void setFoot(PrintWriter out) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.getRound(dFoot[index],iDec[index]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }
               else
                    out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          out.println("  </tr>");
          out.println("  </table>");
     }
     private void setGroupFooter(PrintWriter out) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.getRound(dGroupFoot[index],iDec[index]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }
               else
                    out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          out.println("  </tr>");
         // out.println("  </table>");
     }


     private void setFoot3(PrintWriter out) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.getRound(dFoot[index],iDec[index]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }
               else if(ifooterIndex[i]==0)
                    out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
               else if(ifooterIndex[i]==2)
               {
                    int index  = iSumIndex[i];
                    //String str = common.getRound(dFoot[index],iDec[index]);
                    String str = common.getRound((dFoot[index-3]/dFoot[index-3-2])*100,iDec[index]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }

          }
          out.println("  </tr>");
          out.println("  </table>");
     }

     private void setFoot4Web(PrintWriter out) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.getRound(dFoot[index],iDec[index]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }
               else if(ifooterIndex[i]==0)
                    out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
               else if(ifooterIndex[i]==2)
               {
                    int index  = iSumIndex[i];
                    //String str = common.getRound(dFoot[index],iDec[index]);
                    String str = common.getRound((dFoot[9-2]/dFoot[9-5])*100,iDec[9]);
                    out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
               }

          }
          out.println("  </tr>");
          out.println("  </table>");
     }



     protected void initSum()
     {
          dFoot = new double[SMap.length];
          for(int i=0;i<dFoot.length;i++)
               dFoot[i]=0;
     }

     private void setHead(FileWriter FW) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          int KeyWidth[]  = getKeyWidth();
          String SumHead[]  = getSumHead();
          int SumWidth[]  = getSumWidth();
          String Strl = common.Cad("Sl",4);




          for(int i=0;i<KeyHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead[i],SumWidth[i]);
          }

          if(Lctr < 60)
           return;

          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }

          Pctr++;

          FW.write("Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");
          FW.write("EAbstract on "+STitle+" "+SStart+"-"+SEnd+"\n");
          FW.write("Page:"+Pctr+"\n\n");


          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 4+KeyHead.length;
     }

     private void setHead2(FileWriter FW) throws Exception
     {

          if(Lctr < 60)
               return;

          String KeyHead1[]  = getKeyHead();
          int KeyWidth[]    = getKeyWidth();
          String SumHead1[]  = getSumHead();
          int SumWidth[]    = getSumWidth();

          String KeyHead2[]  = getKeyHead2();
          String SumHead2[]  = getSumHead2();

          String Strl = common.Cad("Sl",4);

          for(int i=0;i<KeyHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead1[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead1[i],SumWidth[i]);
          }

          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }
          Pctr++;


          FW.write("Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");
          //FW.write("EAbstract on "+STitle+" "+SStart+"-"+SEnd+"\n");
          if(SStart.length()==10)
               FW.write("E"+STitle+" "+SStart+"-"+SEnd+"\n");
          else
               FW.write("E"+STitle+" as on "+SEnd+"\n");
          FW.write("Page:"+Pctr+"\n\n");


          String Strl2 = common.Cad(" ",4);
          for(int i=0;i<KeyHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Cad(KeyHead2[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Rad(SumHead2[i],SumWidth[i]);
          }

          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(Strl2+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 4+KeyHead1.length;
     }

     private void setBody(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]    = getSumIndex();
          String SKeyType[]  = getKeyType();
          int    iKeyWidth[] = getKeyWidth();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead(FW);
          }
          setFoot(FW);
     }

     private void setBody1(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isDuplicate = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];
          int iZeroSuppressKey[];


          if(VSlice.size()>0)
          {
               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();
               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();
               iZeroSuppressKey = getZeroSuppressField();

          }
          else
               return;


          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {

                    if(Integer.parseInt(SDuplicateSuppressKey[j])!=1)
                    {
                         if(SKeyType[j].equals("T"))
                              Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                         else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                              Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                         else
                              Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                    }
                    else
                    {

                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isDuplicate=true;
                         else
                              isDuplicate=false;
                    
                         if(!isDuplicate)
                         {
                              if(SKeyType[j].equals("T"))
                                   Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                              else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                                   Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                              else
                                   Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                              SPreviousKey[j]=SKeyField[j];

                         }
                         else
                         {
                              Strl = Strl+SInt+common.Cad(",,",iKeyWidth[j]);     
                         }
                    }

               }


               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    int suppressZero = iZeroSuppressKey[j];
                    String str = "";

                    if(suppressZero==1 && slice.getSumOf(index)==0)
                         str = common.Cad(" ",iMapWidth[index]);
                    else
                         str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead2(FW);
          }
          setFoot(FW);
     }

     private void setBody4(FileWriter FW) throws Exception
     {
          initSum();
          initGroupSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isNewGroup = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];


          if(VSlice.size()>0)
          {

               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();


               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();


          }
          else
               return;


          for(int i=0;i<VSlice.size();i++)
          {

               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
              

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {

                    if((i>0) && (Integer.parseInt(SDuplicateSuppressKey[j])==1))
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isNewGroup=false;
                         else
                              isNewGroup=true;
                    }


                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);


                    SPreviousKey[j]=SKeyField[j];

               }


               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = "";

                    str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

                    if(!isNewGroup)
                    {
                         dGroupFoot[index]=dGroupFoot[index]+slice.getSumOf(index);
                    }

               }

               if(isNewGroup)
               {
                    setGroupFooter(FW);
                    
                    for(int j=0;j<iSumIndex.length;j++)
                    {
                         int index  = iSumIndex[j];
                         dGroupFoot[index]=slice.getSumOf(index);
                    }


               }

               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;


               setHead2(FW);
          }
          setGroupFooter(FW);
          setFoot(FW);
     }



     private void setBody3(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]    = getSumIndex();
          String SKeyType[]  = getKeyType();
          int    iKeyWidth[] = getKeyWidth();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead(FW);
          }

     }



     private void setFoot(FileWriter FW) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.Rad(common.getRound(dFoot[index],iDec[index]),iMapWidth[index]);
                    Strl = Strl+SInt+str;

               }
               else
               {
                    int index  = iSumIndex[i];
                    Strl       = Strl+SInt+common.Rad(" ",iMapWidth[index]);
               }
          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write("\n");
          FW.write(" Report Taken As On  Date " + SServerDate.substring(0,10)+" Time : " + SServerDate.substring(11,19));
          FW.write("\n");
     }

     private void setFoot3(FileWriter FW) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.Rad(common.getRound(dFoot[index],iDec[index]),iMapWidth[index]);
                    Strl = Strl+SInt+str;

               }
               else if(ifooterIndex[i]==0)
               {
                    int index  = iSumIndex[i];
                    Strl       = Strl+SInt+common.Rad(" ",iMapWidth[index]);
               }
               else if(ifooterIndex[i]==2)
               {
                    int index  = iSumIndex[i];
                    //String str = common.Rad(common.getRound((dFoot[index-3]/dFoot[index-3-2])*100,iDec[index]),iMapWidth[index]);
                    dPercentage= (dFoot[index-3]/dFoot[index-3-2])*100;
                    String str = common.Rad(common.getRound(dPercentage,iDec[index]),iMapWidth[index]);
                    Strl = Strl+SInt+str;
               }

          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write("\n");
          FW.write(" Report Taken As On  Date " + SServerDate.substring(0,10)+" Time : " + SServerDate.substring(11,19));
          FW.write("\n");
     }



     private String addLink(String SDisplay)
     {
          String str="";
          str="<font color='#003366'><a href='"+SServer+SRelativeURL+SDisplay+"'><b>"+SDisplay+"</b></a></font>";
          return str;
          
     }

     private String addLink(String SDisplay,String SLink)
     {
          String str="";
          if(i2ndLink==1) SLink = SLink+"~"+SDisplay ;
          str="<font color='#003366'><a href='"+SServer+SRelativeURL+SLink+"'><b>"+SDisplay+"</b></a></font>";
          return str;
          
     }


     private String formatWebCode(String SMixLotNo, String SWebType, String SNoils)
     {
          String xtr = "";
          try
          {
               xtr  =     "MLNB"+SMixLotNo+"MLNE";
               xtr  = xtr+"WTCB"+SWebType +"WTCE";
               xtr  = xtr+"NOIB"+SNoils   +"NOIE";
          }
          catch(Exception ex)
          {
               return "";
          }
          return xtr;
     }
     private String formatDateWebCode(String SMixLotNo, String SWebType, String SNoils,String SDate)
     {
          String xtr = "";
          try
          {
               xtr  =     "MLNB"+SMixLotNo+"MLNE";
               xtr  = xtr+"WTCB"+SWebType +"WTCE";
               xtr  = xtr+"NOIB"+SNoils   +"NOIE";
               xtr  = xtr+"DATB"+SDate+"DATE"; 
          }
          catch(Exception ex)
          {
               return "";
          }
          return xtr;
     }


     private String formatWebCode(String SMixLotNo, String SWebType, String SNoils, String SBinCode)
     {
          String xtr = "";
          try
          {
               xtr  =     "MLNB"+SMixLotNo+"MLNE";
               xtr  = xtr+"WTCB"+SWebType +"WTCE";
               xtr  = xtr+"NOIB"+SNoils   +"NOIE";
               xtr  = xtr+"BINB"+SBinCode +"BINE";
          }
          catch(Exception ex)
          {
               return "";
          }
          return xtr;
     }


     public void setFooterValue(double dPercent)
     {

          dPercentage= dPercent;
          //dPercentage= dFoot[index-3]/dFoot[index-3-2])*100;

     }


     public void setGroupFooter(FileWriter FW) throws Exception
     {    

          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();
          int ifooterIndex[] = getFooterIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               if(ifooterIndex[i]==1)
               {
                    int index  = iSumIndex[i];
                    String str = common.Rad(common.getRound(dGroupFoot[index],iDec[index]),iMapWidth[index]);
                    Strl = Strl+SInt+str;

               }
               else
               {
                    int index  = iSumIndex[i];
                    Strl       = Strl+SInt+common.Rad(" ",iMapWidth[index]);
               }
          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = Lctr+3;

     }

     protected void initGroupSum()
     {
          dGroupFoot = new double[SMap.length];
          for(int i=0;i<dGroupFoot.length;i++)
               dGroupFoot[i]=0;
     }


     public HashMap setData(String QS)
     {
          HashMap row = new HashMap();
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat         = theConnection.createStatement();
               ResultSet result       = stat.executeQuery(QS);
               ResultSetMetaData rsmd = result.getMetaData();
               while(result.next())
               {
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         

               }
               result.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return row;
     }




}

