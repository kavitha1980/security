package domain.servlet.jreporter;

import java.util.*;

public interface SliceBroker
{
     public int size();
     public HashMap getData(int i);
}
