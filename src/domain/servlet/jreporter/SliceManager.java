/*
     A Manager Class to manage the transactions
     thro collection of slices
*/

package domain.servlet.jreporter;

import java.io.*;
import java.sql.*;
import java.util.*;

import domain.jdbc.*;
import util.*;

import javax.servlet.*;
import javax.servlet.http.*;
import rndi.*;


public class SliceManager
{
     Connection theConnection = null;

     protected String SHead[],SHead2[],SPreviousValue[],SMap[],SMapType[];
     protected int    iMapWidth[];
     protected String SInt;

     protected String SStart,SEnd;      // The Major Limits

     protected String STitle;

     protected int iDisp[];          // Displayable
     protected int iGroup[];         // Groupable 
     protected int iSum[];           // Summable
     protected int iDec[];           // The Decimal Places
     protected int iSuppress[];      // Suppress if Duplicate     

     protected double dFoot[];
     protected double dSubFoot[];

     protected Vector VSlice;                  // The Collection of Slices

     Common common = new Common();
     protected int    Pctr=0,Lctr=70;
     protected String SServerDate;
     //String SServer="http://localhost:7070/servlet/domain.servlet.";

     String SServer      = "";
     String SRelativeURL = "";
     int iDisplayIndex   = 0 ;
     int iLinkIndex      = 0 ;
     boolean bShowLink   = false;
     boolean addQuote = false;
     int i2ndLink      = 0 ;
     int iMixLotIndex  = 0 ;
     int iWebTypeIndex = 0 ;
     int iNoilsIndex   = 0 ;
     boolean bShowWebLink   = false;
     public boolean isSamePage = true;

     public SliceManager(String SHead[],String SMap[],String SMapType[])
     {
          this.SHead    = SHead;
          this.SMap     = SMap; 
          this.SMapType = SMapType;
          VSlice        = new Vector();
     }

     public SliceManager(String SHead[],String SHead2[],String SMap[],String SMapType[])
     {
          this.SHead    = SHead;
          this.SHead2   = SHead2;
          this.SMap     = SMap; 
          this.SMapType = SMapType;
          VSlice        = new Vector();
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[])
     {
          this.iDisp    = iDisp;
          this.iGroup   = iGroup;
          this.iSum     = iSum;
          this.iDec     = iDec;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[],int iSuppress[])
     {
          this.iDisp      = iDisp;
          this.iGroup     = iGroup;
          this.iSum       = iSum;
          this.iDec       = iDec;
          this.iSuppress  = iSuppress;
     }

     public void setMapWidth(int iMapWidth[])
     {
          this.iMapWidth = iMapWidth;
     }

     public void setInterval(String SInt)
     {
          this.SInt = SInt;
     }

     public void setLimits(String SStart,String SEnd)
     {
          this.SStart = SStart;
          this.SEnd   = SEnd;
     }

     public void setTitle(String STitle)
     {
          this.STitle = STitle;
     }

     public void clearSlice()
     {
          VSlice.removeAllElements();
     }


     public void setLink(String SServer,String SRelativeURL,int iDisplayIndex,int iLinkIndex,boolean bShowLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          this.iLinkIndex     = iLinkIndex;
          this.bShowLink      = bShowLink;
          bShowWebLink = false;
     }
     public void setLink(String SServer,String SRelativeURL,int iDisplayIndex,int iLinkIndex,boolean bShowLink,int i2ndLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          this.iLinkIndex     = iLinkIndex;
          this.bShowLink      = bShowLink;
          this.i2ndLink       = i2ndLink;   
          bShowWebLink = false;
     }


     public void setLink1(String SServer,String SRelativeURL,int iDisplayIndex,int iLinkIndex,boolean bShowLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
          this.iLinkIndex     = iLinkIndex;
          this.bShowLink      = bShowLink;
          bShowWebLink = false;
          this.addQuote = true;
     }

     public void setWebLink(String SServer,String SRelativeURL,int iDisplayIndex,int iMixLotIndex,int iWebTypeIndex,int iNoilsIndex,boolean bShowWebLink)
     {
          this.SServer        = SServer;
          this.SRelativeURL   = SRelativeURL;
          this.iDisplayIndex  = iDisplayIndex;
        //this.iLinkIndex   = iLinkIndex;
          this.iMixLotIndex   = iMixLotIndex ;
          this.iWebTypeIndex  = iWebTypeIndex;
          this.iNoilsIndex    = iNoilsIndex  ;
          this.bShowWebLink   = bShowWebLink;
          bShowLink = false;
     }


     public void setHistory(String QS)
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    organizeData(result);
               }
               organizeDate(stat);
               result.close();

          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private void organizeDate(Statement stat) throws Exception
     {
          ResultSet result = stat.executeQuery(getDateQS());
          if(result.next())
               SServerDate = result.getString(1);
          result.close();
     }
     private String getDateQS()
     {
          String QS = "Select to_Char(sysdate,'dd.mm.yyyy hh:mi:ss') from Dual";
          return QS;
     }

     public void setHistory(SliceBroker SB)
     {
          try
          {
               for(int i=0;i<SB.size();i++)
               {
                    organizeData(SB.getData(i));
               }
          }
          catch(Exception ex)
          {
               System.out.println("setHistory : "+ex);
          }
     }

     public Vector getSlices()
     {
          return VSlice;
     }

     public void toHTML(PrintWriter out) throws Exception
     {
          setHead(out);
          setBody(out);
     }


     public void toSubHTML(PrintWriter out,int iKey) throws Exception
     {
          setHead(out,iKey);
          setSubBody(out,iKey);
     }

     public void toSubHTML(PrintWriter out,int iKey1,int iKey2) throws Exception
     {
          setHead(out);
          setSubBody(out,iKey1,iKey2);
     }

     public void toFile(FileWriter FW) throws Exception
     {
          setHead(FW);
          setBody(FW);
     }

     public void toFile2(FileWriter FW) throws Exception
     {
          setHead2(FW);
          setBody(FW);
     }

     public void toFile1(FileWriter FW) throws Exception
     {
          setHead2(FW);
          setBody1(FW);
     }
     public void toFile6(FileWriter FW) throws Exception
     {
          setHead6(FW);
          setBody6(FW);
     }


     public void toSubFile(FileWriter FW,int iKey) throws Exception
     {
          setHead(FW,iKey);
          setSubBody(FW,iKey);
     }

     public void toSubFile2(FileWriter FW,int iKey) throws Exception
     {
          setHead2(FW);
          setSubBody(FW,iKey);
     }

     public void toSubFile1(FileWriter FW,int iKey) throws Exception
     {
          setHead2(FW);
          setSubBody1(FW,iKey);
     }

     public void toSubFile(FileWriter FW,int iKey1,int iKey2) throws Exception
     {
          setHead(FW);
          setSubBody(FW,iKey1,iKey2);
     }

     public void toSubFile2(FileWriter FW,int iKey1,int iKey2) throws Exception
     {
          setHead2(FW);
          setSubBody(FW,iKey1,iKey2);
     }

     public void toSubFile1(FileWriter FW,int iKey1,int iKey2) throws Exception
     {
          setHead2(FW);
          setSubBody1(FW,iKey1,iKey2);
     }


     //--- The Internal Methods ----/

     protected void organizeData(ResultSet result) throws Exception
     {
          String SValue[] = new String[SMap.length];
          for(int i=0;i<SMap.length;i++)
          {
               SValue[i] = common.parseNull(result.getString(SMap[i]));
          }
          manageSlice(SValue);
     }

     protected void organizeData(HashMap result) throws Exception
     {
          String SValue[] = new String[SMap.length];
          for(int i=0;i<SMap.length;i++)
          {
               SValue[i] = (String)result.get(SMap[i]);
          }
          manageSlice(SValue);
     }

     private void manageSlice(String[] SValue)
     {
          String SKey[]     = getKeyValue(SValue);
          String SKeyHead[] = getKeyHead();

          Slice slice = findByKey(SKey);

          if(slice == null)
          {
               slice = new Slice(SKey,SKeyHead,SHead,SMap,SMapType);
               slice.setConstraint(iDisp,iGroup,iSum,iDec);
               slice.setMapWidth(iMapWidth);
               slice.setInterval(SInt);
               slice.setLimits(SStart,SEnd);
               slice.setTitle(STitle);

               VSlice.addElement(slice);
          }
          for(int i=0;i<SMap.length;i++)
          {
               slice.appendValue(SMap[i],SValue[i],SMapType[i]);
          }
     }


     private String[] getKeyValue(String SValue[])
     {
          Vector vect = new Vector();
          for(int i=0;i<SValue.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               vect.addElement(SValue[i]);
          }
          String SKey[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKey[i] = (String)vect.elementAt(i);
          }
          return SKey;
     }

     public String[] getKeyHead()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               vect.addElement(SHead[i]);
          }
          String SKeyHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyHead[i] = (String)vect.elementAt(i);
          }
          return SKeyHead;
     }

     public String[] getKeyHead2()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               vect.addElement(SHead2[i]);
          }
          String SKeyHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyHead[i] = (String)vect.elementAt(i);
          }
          return SKeyHead;
     }

     public String[] getKeyType()
     {
          Vector vect = new Vector();
          for(int i=0;i<SMapType.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               vect.addElement(SMapType[i]);
          }
          String SKeyType[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SKeyType[i] = (String)vect.elementAt(i);
          }
          return SKeyType;
     }

     public int[] getKeyWidth()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iGroup[i] == 0)
                    continue;
               vect.addElement(String.valueOf(iMapWidth[i]));
          }
          int iKeyWidth[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iKeyWidth[i] = common.toInt((String)vect.elementAt(i));
          }
          return iKeyWidth;
     }


     public String[] getSumHead()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               vect.addElement(SHead[i]);
          }
          String SSumHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSumHead[i] = (String)vect.elementAt(i);
          }
          return SSumHead;
     }

     public String[] getDuplicateSuppressField()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iGroup[i] != 1)
                    continue;
               vect.addElement(String.valueOf(iSuppress[i]));
          }
          String SSuppressField[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSuppressField[i] = (String)vect.elementAt(i);
          }
          return SSuppressField;
     }


     


     public int[] getZeroSuppressField()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;

               vect.addElement(String.valueOf(iSuppress[i]));
          }

          int iSumIndex[] = new int[vect.size()];

          for(int i=0;i<vect.size();i++)
          {
               iSumIndex[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumIndex;

     }


     public String[] getSumHead2()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               vect.addElement(SHead2[i]);
          }
          String SSumHead[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SSumHead[i] = (String)vect.elementAt(i);
          }
          return SSumHead;
     }

     public int[] getSumWidth()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               vect.addElement(String.valueOf(iMapWidth[i]));
          }
          int iSumWidth[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iSumWidth[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumWidth;
     }

     public int[] getSumIndex()
     {
          Vector vect = new Vector();
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i] != 1)
                    continue;
               if(iSum[i] != 1)
                    continue;
               vect.addElement(String.valueOf(i));
          }
          int iSumIndex[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iSumIndex[i] = common.toInt((String)vect.elementAt(i));
          }
          return iSumIndex;
     }


     private Slice findByKey(String SKey[])
     {
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               if(slice.isEqual(SKey))
               {
                    return slice;
               }
          }
          return null;
     }


     public void setHead(PrintWriter out) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          String SumHead[]  = getSumHead();
          int iColWidth = 40;
          //String SWidth = Integer.toString((iColWidth*SMapType.length));

          if(SStart.length()>9 || SEnd.length()>9 )
               out.println("<p><font color='#000099'><b>Abstract on "+STitle+"&nbsp;"+SStart+"-"+SEnd+"</b></font></p>");
          else
               out.println("<p><font color='#000099'><b>Abstract on "+STitle+"&nbsp;</b></font></p>");


          //out.println("<table border='1' width="+SWidth+">");
          out.println("<table border='1'>");
          out.println("  <tr>");
          out.println("  <td align='center'><b><font color='#FFFF00'>Sl No</font></b></td>");

          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+KeyHead[i]+"</font></b></td>");
          }
          for(int i=0;i<SumHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+SumHead[i]+"</font></b></td>");
          }
          out.println("  </tr>");
          initSum();
     }

     public void setHead(PrintWriter out,int iKey) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          String SumHead[]  = getSumHead();
          int iColWidth = 40;

          //String SWidth = Integer.toString((iColWidth*SMapType.length));

          
          if(SStart.length()>9 || SEnd.length()>9)
               out.println("<p><font color='#000099'><b>"+STitle+"&nbsp;"+SStart+"-"+SEnd+"</b></font></p>");
          else
               out.println("<p><font color='#000099'><b>"+STitle+"&nbsp;</b></font></p>");


          //out.println("<table border='1' width="+SWidth+">");
          out.println("<table border='1'>");
          out.println("  <tr>");
          out.println("  <td align='center'><b><font color='#FFFF00'>Sl No</font></b></td>");

          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+KeyHead[i]+"</font></b></td>");
          }
          for(int i=0;i<SumHead.length;i++)
          {
               out.println("  <td align='center'><b><font color='#FFFF00'>"+SumHead[i]+"</font></b></td>");
          }
          out.println("  </tr>");
          initSum();
     }

     private void setBody15092004(PrintWriter out) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                    else
                         out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }
     private void setBody(PrintWriter out) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                              {

                                   if(!addQuote)
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                                   else
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                              }
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }

                    }
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.getRound(slice.getSumOf(index),iDec[index]);

                    out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);
               }
               out.println("</tr>");
          }
          setFoot(out);
     }

     private void setSubBody(PrintWriter out,int iKey) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          String SOld = "";
          boolean bNew=true;
          initSubSum();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               if(i==0)
               {
                    SOld = SKeyField[iKey];
                    bNew = false;
               }
               else if(!SOld.equals(SKeyField[iKey]))
               {
                    setSubFoot(out);
                    bNew = true;
               }

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                              {
                                   if(!addQuote)
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                                   else
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                              }
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }

                    }

                    if(bNew == true)
                    {
                         SOld = SKeyField[iKey];
                         initSubSum();
                         bNew = false;
                    }
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    try
                    {
                         int index  = iSumIndex[j];
                         String str = common.getRound(slice.getSumOf(index),iDec[index]);
     
                         out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                         dFoot[index]    = dFoot[index]    + slice.getSumOf(index);
                         dSubFoot[index] = dSubFoot[index] + slice.getSumOf(index);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               out.println("</tr>");
          }
          setSubFoot(out);
          setFoot(out);
     }

     private void setSubBody(PrintWriter out,int iKey1,int iKey2) throws Exception
     {
          int iSumIndex[] = getSumIndex();
          String SOld1 = "";
          String SOld2 = "";
          
          boolean bNew=true;
          initSubSum();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               String SKeyType[] = getKeyType();

               if(i==0)
               {
                    SOld1 = SKeyField[iKey1];
                    SOld2 = SKeyField[iKey2];

                    bNew  = false;
               }
               else if(!SOld1.equals(SKeyField[iKey1]))
               {
                    if(!SOld2.equals(SKeyField[iKey2]))
                    {
                         setSubFoot(out);
                         bNew = true;
                    }
               }

               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SKeyField.length;j++)
               {
                    if(!bShowLink)
                    {
                         if(SKeyType[j].equals("T"))
                              out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                         else 
                              out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                    }
                    else
                    {
                         if(j==iDisplayIndex)
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td>"+addLink(common.parseDate(SKeyField[j]))+"</td>");
                              else 
                              {
                                   if(!addQuote)
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                                   else
                                        out.println("  <td>"+addLink(SKeyField[j],SKeyField[iLinkIndex])+"</td>");
                              }
                         }
                         else
                         {
                              if(SKeyType[j].equals("T"))
                                   out.println("  <td><font color='#FFFFFF'>"+common.parseDate(SKeyField[j])+"</font></td>");
                              else 
                                   out.println("  <td><font color='#FFFFFF'>"+SKeyField[j]+"</font></td>");
                         }
                    }

                    if(bNew == true)
                    {
                         SOld1 = SKeyField[iKey1];
                         SOld2 = SKeyField[iKey2];

                         initSubSum();
                         bNew = false;
                    }
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    try
                    {
                         int index  = iSumIndex[j];
                         String str = common.getRound(slice.getSumOf(index),iDec[index]);
     
                         out.println("  <td align='right'><font color='#FFFFFF'>"+str+"</font></td>");
                         dFoot[index]    = dFoot[index]    + slice.getSumOf(index);
                         dSubFoot[index] = dSubFoot[index] + slice.getSumOf(index);
                    }
                    catch(Exception ex)
                    {
                         System.out.println(ex);
                    }
               }
               out.println("</tr>");
          }
          setSubFoot(out);
          setFoot(out);
     }

     private void setFoot(PrintWriter out) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          int iSumIndex[] = getSumIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               int index  = iSumIndex[i];
               String str = common.getRound(dFoot[index],iDec[index]);
               out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
          }
          out.println("  </tr>");
          out.println("  </table>");
     }

     private void setSubFoot(PrintWriter out) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          int iSumIndex[] = getSumIndex();
          out.println("  <tr>");
          out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>&nbsp;</font></b></td>");
          }
          for(int i=0;i<iSumIndex.length;i++)
          {
               int index  = iSumIndex[i];
               String str = common.getRound(dSubFoot[index],iDec[index]);
               out.println("  <td align='right'><b><font color='#000099'>"+str+"</font></b></td>");
          }
          out.println("  </tr>");
     }

     public void initSum()
     {
          dFoot = new double[SMap.length];
          for(int i=0;i<dFoot.length;i++)
               dFoot[i]=0;
     }

     public void initSubSum()
     {
          dSubFoot = new double[SMap.length];
          for(int i=0;i<dSubFoot.length;i++)
               dSubFoot[i]=0;
     }

     private void setHead(FileWriter FW) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          int KeyWidth[]  = getKeyWidth();
          String SumHead[]  = getSumHead();
          int SumWidth[]  = getSumWidth();
          String Strl = common.Cad("Sl",4);




          for(int i=0;i<KeyHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead[i],SumWidth[i]);
          }

          if(Lctr < 63)
           return;

          /*
          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }
          */

          Pctr++;

          FW.write("Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");
          FW.write("EAbstract on "+STitle+" "+SStart+"-"+SEnd+"\n");
          FW.write("Page:"+Pctr+"\n\n");


          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 4+KeyHead.length;
     }

     private void setHead(FileWriter FW,int iKey) throws Exception
     {
          String KeyHead[]  = getKeyHead();
          int KeyWidth[]  = getKeyWidth();
          String SumHead[]  = getSumHead();
          int SumWidth[]  = getSumWidth();
          String Strl = common.Cad("Sl",4);

          for(int i=0;i<KeyHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead[i],SumWidth[i]);
          }

          if(Lctr < 63)
           return;

          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }

          Pctr++;

          FW.write("Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");
          FW.write("E"+STitle+" "+SStart+"-"+SEnd+"\n");
          FW.write("Page:"+Pctr+"\n\n");


          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 4+KeyHead.length;
     }
     private void setHead6(FileWriter FW) throws Exception
     {

          if(Lctr < 60)
               return;

          String KeyHead1[]  = getKeyHead();
          int    KeyWidth[]  = getKeyWidth();
          String SumHead1[]  = getSumHead();
          int    SumWidth[]  = getSumWidth();

          String KeyHead2[]  = getKeyHead2();
          String SumHead2[]  = getSumHead2();

          String Strl = common.Cad("Sl",4);

          for(int i=0;i<KeyHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead1[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead1[i],SumWidth[i]);
          }

          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }
          Pctr++;


          FW.write("     Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");

          FW.write("Page:"+Pctr+"\n\n");
          FW.write("\n\n\n\n");
          FW.write(" To JMD sir,                 "  );
          FW.write("\n");
          FW.write("            "+STitle+"  WebBale and WasteBale Packing Work \n");
          FW.write(" for the Period from "+SStart+" To "+SEnd+"                \n\n");


          //FW.write("EAbstract on "+STitle+" "+SStart+"-"+SEnd+"\n");
          /*if(SStart.length()==10)
               FW.write("E"+STitle+" "+SStart+"-"+SEnd+"\n");
          else
               FW.write("E"+STitle+" as on "+SEnd+"\n");
          */


          String Strl2 = common.Cad(" ",4);
          for(int i=0;i<KeyHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Cad(KeyHead2[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Rad(SumHead2[i],SumWidth[i]);
          }

          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(Strl2+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 6+KeyHead1.length;
     }


     private void setHead2(FileWriter FW) throws Exception
     {

          if(Lctr < 65 && isSamePage)
               return;

          if(isSamePage)
               isSamePage = true;

          String KeyHead1[]  = getKeyHead();
          int    KeyWidth[]  = getKeyWidth();
          String SumHead1[]  = getSumHead();
          int    SumWidth[]  = getSumWidth();

          String KeyHead2[]  = getKeyHead2();
          String SumHead2[]  = getSumHead2();

          String Strl = common.Cad("Sl",4);

          for(int i=0;i<KeyHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(KeyHead1[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead1.length;i++)
          {
               Strl = Strl+SInt+common.Cad(SumHead1[i],SumWidth[i]);
          }

          if(Pctr > 0)
          {
               FW.write(common.Replicate("-",Strl.length())+"\n");
          }
          Pctr++;


          FW.write("Amarjothi Spinning Mills Ltd - Nambiyur"+"\n");
          //FW.write("EAbstract on "+STitle+" "+SStart+"-"+SEnd+"\n");
          if(SStart.length()==10)
               FW.write("E"+STitle+" "+SStart+"-"+SEnd+"\n");
          else
               FW.write("E"+STitle+" as on "+SEnd+"\n");
          FW.write("Page:"+Pctr+"\n\n");


          String Strl2 = common.Cad(" ",4);
          for(int i=0;i<KeyHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Cad(KeyHead2[i],KeyWidth[i]);
          }

          for(int i=0;i<SumHead2.length;i++)
          {
               Strl2 = Strl2+SInt+common.Rad(SumHead2[i],SumWidth[i]);
          }

          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(Strl2+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          Lctr = 4+KeyHead1.length;
     }

     public void setBody(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]    = getSumIndex();
          String SKeyType[]  = getKeyType();
          int    iKeyWidth[] = getKeyWidth();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead(FW);
          }
          setFoot(FW);
     }

     public void setBody1(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isDuplicate = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];
          int iZeroSuppressKey[];


          if(VSlice.size()>0)
          {
               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();
               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();
               iZeroSuppressKey = getZeroSuppressField();
          }
          else
               return;


          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {

                    if(Integer.parseInt(SDuplicateSuppressKey[j])!=1)
                    {
                         if(SKeyType[j].equals("T"))
                              Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                         else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                              Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                         else
                              Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                    }
                    else
                    {

                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isDuplicate=true;
                         else
                              isDuplicate=false;
                    
                         if(!isDuplicate)
                         {
                              if(SKeyType[j].equals("T"))
                                   Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                              else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                                   Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                              else
                                   Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                              SPreviousKey[j]=SKeyField[j];

                         }
                         else
                         {
                              Strl = Strl+SInt+common.Cad(",,",iKeyWidth[j]);     
                         }
                    }

               }


               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    int suppressZero = iZeroSuppressKey[j];
                    String str = "";

                    if(suppressZero==1 && slice.getSumOf(index)==0)
                         str = common.Cad(" ",iMapWidth[index]);
                    else
                         str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead2(FW);
          }
          setFoot(FW);
     }

     public void setBody6(FileWriter FW) throws Exception
     {
          initSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isDuplicate = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];
          int iZeroSuppressKey[];


          if(VSlice.size()>0)
          {
               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();
               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();
               iZeroSuppressKey = getZeroSuppressField();
          }
          else
               return;


          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {

                    if(Integer.parseInt(SDuplicateSuppressKey[j])!=1)
                    {
                         if(SKeyType[j].equals("T"))
                              Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                         else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                              Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                         else
                              Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                    }
                    else
                    {

                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isDuplicate=true;
                         else
                              isDuplicate=false;
                    
                         if(!isDuplicate)
                         {
                              if(SKeyType[j].equals("T"))
                                   Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                              else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                                   Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                              else
                                   Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                              SPreviousKey[j]=SKeyField[j];

                         }
                         else
                         {
                              Strl = Strl+SInt+common.Cad(",,",iKeyWidth[j]);     
                         }
                    }

               }


               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    int suppressZero = iZeroSuppressKey[j];
                    String str = "";

                    if(suppressZero==1 && slice.getSumOf(index)==0)
                         str = common.Cad(" ",iMapWidth[index]);
                    else
                         str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead2(FW);
          }
          setFoot6(FW);
     }


     public void setSubBody(FileWriter FW,int iKey) throws Exception
     {

          initSum();
          int iSumIndex[]    = getSumIndex();
          String SKeyType[]  = getKeyType();
          int    iKeyWidth[] = getKeyWidth();
          String  SOld       = "";
          boolean bNew       = true;

          initSubSum();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();

               if(i==0)
               {
                    SOld = SKeyField[iKey];
                    bNew = false;
               }
               else if(!SOld.equals(SKeyField[iKey]))
               {
                    setSubFoot(FW);
                    Lctr = Lctr + 3;
                    bNew = true;
               }

               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                    if(bNew == true)
                    {
                         SOld = SKeyField[iKey];
                         initSubSum();
                         bNew = false;
                    }
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]    = dFoot[index]+slice.getSumOf(index);
                    dSubFoot[index] = dSubFoot[index] + slice.getSumOf(index);
               }

               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead(FW,iKey);
          }
          Lctr = Lctr + 3;
          setSubFoot(FW);
          setFoot(FW);
     }

     public void setSubBody(FileWriter FW,int iKey1,int iKey2) throws Exception
     {

          initSum();
          int iSumIndex[]    = getSumIndex();
          String SKeyType[]  = getKeyType();
          int    iKeyWidth[] = getKeyWidth();
          String  SOld1      = "";
          String  SOld2      = "";
          boolean bNew       = true;

          initSubSum();

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();

               if(i==0)
               {
                    SOld1 = SKeyField[iKey1];
                    SOld2 = SKeyField[iKey2];
                    bNew = false;
               }
               else if(!SOld1.equals(SKeyField[iKey1]))
               {
                    if(!SOld2.equals(SKeyField[iKey2]))
                    {
                         setSubFoot(FW);
                         Lctr = Lctr + 3;
                         bNew = true;
                    }
               }
               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(SKeyType[j].equals("T"))
                         Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                    else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                         Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                    else
                         Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                    if(bNew == true)
                    {
                         SOld1 = SKeyField[iKey1];
                         SOld2 = SKeyField[iKey2];

                         initSubSum();
                         bNew = false;
                    }
               }

               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    String str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]    = dFoot[index]+slice.getSumOf(index);
                    dSubFoot[index] = dSubFoot[index] + slice.getSumOf(index);
               }

               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead(FW);
          }
          Lctr = Lctr + 3;
          setSubFoot(FW);
          setFoot(FW);
     }

     public void setSubBody1(FileWriter FW,int iKey) throws Exception
     {
          initSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isDuplicate = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];
          int iZeroSuppressKey[];


          if(VSlice.size()>0)
          {
               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();
               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();
               iZeroSuppressKey = getZeroSuppressField();

          }
          else
               return;

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               
               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(Integer.parseInt(SDuplicateSuppressKey[j])!=1)
                    {
                         if(SKeyType[j].equals("T"))
                              Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                         else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                              Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                         else
                              Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);
                    }
                    else
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isDuplicate=true;
                         else
                              isDuplicate=false;
                    
                         if(!isDuplicate)
                         {
                              if(SKeyType[j].equals("T"))
                                   Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                              else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                                   Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                              else
                                   Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                              SPreviousKey[j]=SKeyField[j];
                         }
                         else
                         {
                              Strl = Strl+SInt+common.Cad(",,",iKeyWidth[j]);     
                         }
                    }
               }
               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    int suppressZero = iZeroSuppressKey[j];
                    String str = "";

                    if(suppressZero==1 && slice.getSumOf(index)==0)
                         str = common.Cad(" ",iMapWidth[index]);
                    else
                         str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead2(FW);
          }
          setFoot(FW);
     }

     public void setSubBody1(FileWriter FW,int iKey1,int iKey2) throws Exception
     {
          initSum();
          int iSumIndex[]     = getSumIndex();
          String SKeyType[]   = getKeyType();
          int    iKeyWidth[]  = getKeyWidth();
          boolean isDuplicate = false;
          String SPreviousKey[];
          String SDuplicateSuppressKey[];
          int iZeroSuppressKey[];


          if(VSlice.size()>0)
          {
               Slice slice = (Slice)VSlice.elementAt(0);
               String SKeyField[] = slice.getKeyField();
               SPreviousKey = new String[SKeyField.length];
               for(int i=0;i<SPreviousKey.length;i++)
               {
                    SPreviousKey[i]="";
               }
               SDuplicateSuppressKey = getDuplicateSuppressField();
               iZeroSuppressKey = getZeroSuppressField();

          }
          else
               return;

          for(int i=0;i<VSlice.size();i++)
          {
               Slice slice = (Slice)VSlice.elementAt(i);
               String SKeyField[] = slice.getKeyField();
               
               String Strl = common.Rad(String.valueOf(i+1),4);

               for(int j=0;j<SKeyField.length;j++)
               {
                    if(Integer.parseInt(SDuplicateSuppressKey[j])!=1)
                    {
                         if(SKeyType[j].equals("T"))
                              Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                         else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                              Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                         else
                              Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);
                    }
                    else
                    {
                         if(((String)SKeyField[j]).equals((String)SPreviousKey[j]))
                              isDuplicate=true;
                         else
                              isDuplicate=false;
                    
                         if(!isDuplicate)
                         {
                              if(SKeyType[j].equals("T"))
                                   Strl = Strl+SInt+common.Pad(common.parseDate(SKeyField[j]),iKeyWidth[j]);
                              else if(SKeyType[j].equals("N")||SKeyType[j].equals("D"))
                                   Strl = Strl+SInt+common.Rad(SKeyField[j],iKeyWidth[j]);
                              else
                                   Strl = Strl+SInt+common.Pad(SKeyField[j],iKeyWidth[j]);

                              SPreviousKey[j]=SKeyField[j];

                         }
                         else
                         {
                              Strl = Strl+SInt+common.Cad(",,",iKeyWidth[j]);     
                         }
                    }

               }


               for(int j=0;j<iSumIndex.length;j++)
               {
                    int index  = iSumIndex[j];
                    int suppressZero = iZeroSuppressKey[j];
                    String str = "";

                    if(suppressZero==1 && slice.getSumOf(index)==0)
                         str = common.Cad(" ",iMapWidth[index]);
                    else
                         str = common.Rad(common.getRound(slice.getSumOf(index),iDec[index]),iMapWidth[index]);

                    Strl = Strl+SInt+str;
                    dFoot[index]=dFoot[index]+slice.getSumOf(index);

               }
               Strl=Strl+"\n";
               FW.write(Strl);
                Lctr++;
               setHead2(FW);
          }
          setFoot(FW);
     }

     public void setFoot(FileWriter FW) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               int index  = iSumIndex[i];
               String str = common.Rad(common.getRound(dFoot[index],iDec[index]),iMapWidth[index]);
               Strl = Strl+SInt+str;               
          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write("\n");
          FW.write(" Report Taken As On  Date " + SServerDate.substring(0,10)+" Time : " + SServerDate.substring(11,19));
          FW.write("\n");
     }

     public void setFoot6(FileWriter FW) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               int index  = iSumIndex[i];
               String str = common.Rad(common.getRound(dFoot[index],iDec[index]),iMapWidth[index]);
               Strl = Strl+SInt+str;               
          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write("\n\n\n\n\n\n\n");
          FW.write("      Prepared    Checked   GodownClerk    RMI   Sm(A)    Dm(B)     \n\n\n\n\n\n\n");
          FW.write(" Report Taken As On  Date " + SServerDate.substring(0,10)+" Time : " + SServerDate.substring(11,19));
          FW.write("\n");
     }


     public void setSubFoot(FileWriter FW) throws Exception
     {
          String KeyHead[]   = getKeyHead();
          int    iKeyWidth[] = getKeyWidth();
          int iSumIndex[]    = getSumIndex();

          String Strl = common.Space(4);
          for(int i=0;i<KeyHead.length;i++)
          {
               Strl  = Strl+SInt+common.Space(iKeyWidth[i]);
          }

          for(int i=0;i<iSumIndex.length;i++)
          {
               int index  = iSumIndex[i];
               String str = common.Rad(common.getRound(dSubFoot[index],iDec[index]),iMapWidth[index]);
               Strl = Strl+SInt+str;               
          }
          FW.write(common.Replicate("-",Strl.length())+"\n");
          FW.write(Strl+"\n");
          FW.write(common.Replicate("-",Strl.length())+"\n");
     }

     private String addLink(String SDisplay) 
     {
          String str="";
          str="<font color='#003366'><a href='"+SServer+SRelativeURL+SDisplay+"'><b>"+SDisplay+"</b></a></font>";
          return str;
          
     }

     private String addLink(String SDisplay,String SLink)
     {

          if (i2ndLink==1) SLink=SLink+"~"+SDisplay;

          String str="";
          if(!addQuote)
               str="<font color='#003366'><a href='"+SServer+SRelativeURL+SLink+"'><b>"+SDisplay+"</b></a></font>";
          else
               str="<font color='#003366'><a href='"+SServer+SRelativeURL+SLink+"'><b>"+SDisplay+"</b></a></font>";
          return str;

     }

     public HashMap setData(String QS)
     {
          HashMap row = new HashMap();
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat         = theConnection.createStatement();
               ResultSet result       = stat.executeQuery(QS);
               ResultSetMetaData rsmd = result.getMetaData();
               while(result.next())
               {
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                         row.put(rsmd.getColumnName(i+1),result.getString(i+1));
                    }                         
               }
               result.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
          return row;
     }
}

