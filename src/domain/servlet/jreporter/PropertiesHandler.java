/*
     The Properties Handler does

          1. Reading from a given "properties" file.

          2. Display the content as a HTML format for
             User modification.

          3. provide methods for exposing the property information.

          Author : Rajasubramaniam.K
*/

package domain.servlet.jreporter;

import javax.servlet.http.*;
import java.util.*;
import java.io.*;
import util.*;
import rndi.*;

public class PropertiesHandler implements CodedNames
{
     protected String SFile;

     int DISPLAY  = 0;
     int GROUP    = 1;
     int SUM      = 2;
     int DEC      = 3;
     int SUPPRESS = 4;
     int PROGRAM  = 5;
     int SUMMARY  = 6;
     int ISLINK   = 7;
     int LINKDATA = 8;
     int PERIOD   = 9;

     Common common = new Common();

     public PropertiesHandler(String SFile)
     {
          this.SFile = SFile;          
     }

     public String[] getHead()
     {

          Vector vect = getData("<Head>","</Head>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;
     }
     public String[] getHead2()
     {
          Vector vect = getData("<Head2>","</Head2>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;
     }

     public String[] getHead3()
     {
          Vector vect = getData("<Head3>","</Head3>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;
     }

     public String[] getMap()
     {
          Vector vect = getData("<Map>","</Map>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;
     }
     public int[] getMapWidth()
     {
          Vector vect = getData("<Width>","</Width>");
          int iArr[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               iArr[i] = common.toInt((String)vect.elementAt(i));
          }
          return iArr;
     }

     public String[] getType()
     {
          Vector vect = getData("<Type>","</Type>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;

     }
     public String[] getConstraint()
     {
          Vector vect = getData("<Const>","</Const>");
          String SArr[] = new String[vect.size()];
          for(int i=0;i<vect.size();i++)
          {
               SArr[i] = (String)vect.elementAt(i);
          }
          return SArr;
     }

     public int[] getConstArray(String SAttribute)
     {
          Vector vect = new Vector();
          if(SAttribute.equals("DISPLAY"))
               vect = getConstraint(DISPLAY);
          else if(SAttribute.equals("GROUP"))
               vect = getConstraint(GROUP);
          else if(SAttribute.equals("SUM"))
               vect = getConstraint(SUM);
          else if(SAttribute.equals("DEC"))
               vect = getConstraint(DEC);
          else if(SAttribute.equals("SUPPRESS"))
               vect = getConstraint(SUPPRESS);
          else if(SAttribute.equals("PROGRAM"))
               vect = getConstraint(PROGRAM);
          else if(SAttribute.equals("SUMMARY"))
               vect = getConstraint(SUMMARY);
          else if(SAttribute.equals("ISLINK"))
               vect = getConstraint(ISLINK);
          else if(SAttribute.equals("LINKDATA"))
               vect = getConstraint(LINKDATA);
          else if(SAttribute.equals("PERIOD"))
               vect = getConstraint(PERIOD);


          int iArr[] = new int[vect.size()];
          for(int i=0;i<vect.size();i++)
               iArr[i] = common.toInt((String)vect.elementAt(i));

          return iArr;
     }

     public Vector getData(String STag1,String STag2)
     {
          Vector vect = new Vector();
          String str  = getString();
          int index1  = str.indexOf(STag1);
          int index2  = str.indexOf(STag2);
          if(index1 == -1)
               return null;
          if(index2 == -1)
               return null;
          try
          {
               String xtr = str.substring(index1+STag1.length(),index2);
               StringTokenizer ST = new StringTokenizer(xtr,",");
               while(ST.hasMoreElements())
               {
                    vect.addElement((ST.nextToken()).trim());
               }
          }
          catch(Exception ex)
          {
               return null;
          }
          return vect;
     }

     public Vector getConstraint(int iConst)
     {
          Vector VData = new Vector();
          Vector vect = getData("<Const>","</Const>");
          for(int i=0;i<vect.size();i++)
          {
               String str = (String)vect.elementAt(i);
               StringTokenizer ST = new StringTokenizer(str,"-");
               int ctr=0;
               while(ST.hasMoreElements())
               {
                    String SToken = ST.nextToken();
                    if(ctr==iConst)
                    {
                         VData.addElement(SToken);
                         break;
                    }
                    ctr++;
               }
          }
          return VData;
     }

     private String getString()
     {
//        String SURL ="//"+SCRIPTHOST+"/e/"+SCRIPTFOLDER+"/src/domain/servlet/resource/"+SFile+".properties";
          String SURL ="//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/domain/servlet/resource/"+SFile+".properties";
          String str = "";
          try
          {
               Reader in = new FileReader(SURL);
               char[] buff = new char[4096];
               int nch;
               while ((nch = in.read(buff, 0, buff.length)) != -1)
               {
                    str = str+new String(buff, 0, nch);
               }
          }
          catch(Exception ex){}
          return str;
     }

     public void toHTML(PrintWriter out) throws Exception
     {
          out.println("  <table border='1' width='600' height='14'>");
          out.println("<tr>");
          out.println("<td width='115' height='18'>Column Name</td>");
          out.println("<td width='54' height='18'>Display</td>");
          out.println("<td width='45' height='18'>Group</td>");
          out.println("<td width='32' height='18'>Sum</td>");
          out.println("<td width='1' height='18'>Decimals</td>");
          out.println("</tr>");

          String SHead[]  = getHead();
          int    iDisp[]  = getConstArray("DISPLAY");
          int    iGroup[] = getConstArray("GROUP");
          int    iSum[]   = getConstArray("SUM");
          int    iDec[]   = getConstArray("DEC");

          for(int i=0;i<SHead.length;i++)
          {
               out.println("<tr>");
               out.println("<td width='115' height='18'>"+SHead[i]+"</td>");
               if(iDisp[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='0'></td>");

               if(iGroup[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='0'></td>");

               if(iSum[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='0'></td>");

               out.println("<td width='1' height='18'><input type='text' name='Dec"+i+"' size='3' value='"+iDec[i]+"'></td>");
               out.println("</tr>");
          }
          out.println("</table>");
     }

     public void toHTML1(PrintWriter out) throws Exception
     {
          out.println("  <table border='1' width='600' height='14'>");
          out.println("<tr>");
          out.println("<td width='115' height='18'>Column Name</td>");
          out.println("<td width='54' height='18'>Display</td>");
          out.println("<td width='45' height='18'>Group</td>");
          out.println("<td width='32' height='18'>Sum</td>");
          out.println("<td width='1' height='18'>Decimals</td>");
          out.println("<td width='1' height='18'>Suppress Duplicate/Zero</td>");
          out.println("</tr>");

          String SHead[]     = getHead();
          int    iDisp[]     = getConstArray("DISPLAY");
          int    iGroup[]    = getConstArray("GROUP");
          int    iSum[]      = getConstArray("SUM");
          int    iDec[]      = getConstArray("DEC");
          int    iSuppress[] = getConstArray("SUPPRESS");

          for(int i=0;i<SHead.length;i++)
          {
               out.println("<tr>");
               out.println("<td width='115' height='18'>"+SHead[i]+"</td>");
               if(iDisp[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='0'></td>");

               if(iGroup[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='0'></td>");

               if(iSum[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='0'></td>");

               out.println("<td width='1' height='18'><input type='text' name='Dec"+i+"' size='3' value='"+iDec[i]+"'></td>");

               if(iSuppress[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Suppress"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Suppress"+i+"' value='0'></td>");

               out.println("</tr>");
          }
          out.println("</table>");
     }

     public void toHTML2(PrintWriter out) throws Exception
     {
          out.println("  <table border='1' width='600' height='14'>");
          out.println("<tr>");
          out.println("<td width='115' height='18'>Column Name</td>");
          out.println("<td width='54' height='18'>Display</td>");
          out.println("<td width='45' height='18'>Group</td>");
          out.println("<td width='32' height='18'>Sum</td>");
          out.println("<td width='1' height='18'>Decimals</td>");
          out.println("<td width='1' height='18'>Suppress Duplicate/Zero</td>");
          out.println("</tr>");

          String SHead[]     = getHead();
          int    iDisp[]     = getConstArray("DISPLAY");
          int    iGroup[]    = getConstArray("GROUP");
          int    iSum[]      = getConstArray("SUM");
          int    iDec[]      = getConstArray("DEC");
          int    iSuppress[] = getConstArray("SUPPRESS");
          int    iProgram[]  = getConstArray("PROGRAM");

          for(int i=0;i<SHead.length;i++)
          {
               if(iProgram[i]==0)
                    continue;

               out.println("<tr>");
               out.println("<td width='115' height='18'>"+SHead[i]+"</td>");
               if(iDisp[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Display"+i+"' value='0'></td>");

               if(iGroup[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Group"+i+"' value='0'></td>");

               if(iSum[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Sum"+i+"' value='0'></td>");

               out.println("<td width='1' height='18'><input type='text' name='Dec"+i+"' size='3' value='"+iDec[i]+"'></td>");

               if(iSuppress[i]==1)
                    out.println("<td width='54' height='18'><input type='checkbox' name='Suppress"+i+"' value='1' checked></td>");
               else
                    out.println("<td width='54' height='18'><input type='checkbox' name='Suppress"+i+"' value='0'></td>");

               out.println("</tr>");
          }
          out.println("</table>");
     }


     public int[] getConst(HttpServletRequest request,String SAttribute) throws Exception
     {
          String SArr[] = getHead();
          int iArr[] = new int[SArr.length];
          for(int i=0;i<SArr.length;i++)
          {
               String str = SAttribute+i;
               String SValue = request.getParameter(str);
               if(SAttribute.equals("Dec"))
               {
                    iArr[i] = common.toInt(SValue);
                    continue;
               }
               if(SValue == null)
                    iArr[i] = 0;
               else
                    iArr[i] = 1;
          }
          return iArr;
     }


}
