/*
     The HTML display unit to assist
     SliceManager

     The Individual Element of a Sliced Report
*/

package domain.servlet.jreporter;

import java.io.*;
import util.*;

public class Slice extends AbstractHistoryModel
{
     protected String KeyField[];
     protected String KeyHead[];
     protected String SKeyType[];

     protected String SHead[],SMap[],SMapType[],SData[],SLinkData[];
     protected int iMapWidth[];
     protected String SInt;

     protected String SStart,SEnd;      // The Major Limits
     protected String STitle,SLnk;

     protected int    iDisp[];          // Displayable
     protected int    iGroup[];         // Groupable 
     protected int    iSum[];           // Summable
     protected int    iDec[];           // The Decimal Places     

     double dFoot[];

     Common common = new Common();

     public Slice(String SKey[],String SKeyHead[],String SHead[],String SMap[],String SMapType[])
     {
          setKey(SKey,SKeyHead);
          setMap(SHead,SMap);

          this.SHead    = SHead;
          this.SMap     = SMap;
          this.SMapType = SMapType;

     }

     public Slice(String SKey[],String SKeyHead[],String SHead[],String SMap[],String SMapType[],String SData[])
     {
          setKey(SKey,SKeyHead);
          setMap(SHead,SMap);

          this.SHead    = SHead;
          this.SMap     = SMap;
          this.SMapType = SMapType;
          this.SData    = SData;

     }

     public Slice(String SKey[],String SKeyHead[],String SHead[],String SMap[],String SMapType[],String SData[],String SLinkData[])
     {
          setKey(SKey,SKeyHead);
          setMap(SHead,SMap);

          this.SHead     = SHead;
          this.SMap      = SMap;
          this.SMapType  = SMapType;
          this.SData     = SData;
          this.SLinkData = SLinkData;

     }


     private void setKey(String SKey[],String SKeyHead[])
     {
          this.KeyField = SKey;
          this.KeyHead  = SKeyHead;
     }

     public void setMapWidth(int iMapWidth[])
     {
          this.iMapWidth = iMapWidth;
     }

     public void setInterval(String SInt)
     {
          this.SInt = SInt;
     }

     public void setLink(String SLnk)
     {
          this.SLnk = SLnk;
     }

     public String getLink()
     {
          return SLnk;
     }


     public boolean isEqual(String SKey[])
     {
          if(SKey.length != KeyField.length)
          {
               return false;
          }
          try
          {
               for(int i=0;i<KeyField.length;i++)
               {
                    if(!KeyField[i].equals(SKey[i]))
                    {
                         return false;
                    }
               }
               return true;
          }
          catch(Exception ex){}
          return false;
     }

     public String[] getKeyField()
     {
          return KeyField;
     }

     public String[] getFieldData()
     {
          return SData;
     }

     public String[] getFieldLinkData()
     {
          return SLinkData;
     }


     public void setLimits(String SStart,String SEnd)
     {
          this.SStart = SStart;
          this.SEnd   = SEnd;
     }

     public void setTitle(String STitle)
     {
          this.STitle = STitle;
     }

     public void setConstraint(int iDisp[],int iGroup[],int iSum[],int iDec[])
     {
          this.iDisp  = iDisp;
          this.iGroup = iGroup;
          this.iSum   = iSum;
          this.iDec   = iDec;
     }
     public void toHTML(PrintWriter out) throws Exception
     {
          setSliceHead(out);
          setTableHead(out);
          setTableBody(out);
     }

     public double getSumOf(int index)
     {
          double dValue = 0;
          String str    = SMap[index];
          for(int i=0;i<size();i++)
          {
               dValue = dValue+common.toDouble(getValue(str,i));
          }
          return dValue;
     }

     //---------------- Internal Methods ----------//
     private void setSliceHead(PrintWriter out) throws Exception
     {
          int iColWidth = 60;
          String SWidth = Integer.toString((iColWidth*SMapType.length));

          out.println("<p><font color='#005500'><b>"+STitle+"&nbsp;"+SStart+"-"+SEnd+"</b></font></p>");
          out.println("<table border='0' width="+SWidth+">");

          out.println("  <tr>");
          for(int i=0;i<KeyHead.length;i++)
          {
               out.println("  <td><b><font color='#000099'>"+KeyHead[i]+"</font></b></td>");
          }               
          out.println("  </tr>");

          out.println("  <tr>");
          for(int i=0;i<KeyField.length;i++)
          {
               if(SMapType[i].equals("T") || i==3)
               { 
                    out.println("  <td><b><font color='#FFFFFF'>"+common.parseDate(KeyField[i])+"</font></b></td>");
               }
               else
                    out.println("  <td><b><font color='#FFFFFF'>"+KeyField[i]+"</font></b></td>");
          }
          out.println("  </tr>");
          out.println("  </table>");
     }
     
     private void setTableHead(PrintWriter out) throws Exception
     {
          out.println("<table border='1'>");
          out.println("<tr>");
          out.println("<td align='center'><b><font color='#FFFF00'>Sl</font></b></td>");
          for(int i=0;i<SHead.length;i++)
          {
               if(iDisp[i]==0)
                    continue;
               if(iGroup[i]!=0)
                    continue;
               out.println("<td align='center'><b><font color='#FFFF00'>"+SHead[i]+"</font></b></td>");
          }
          out.println("</tr>");
          initSum();
     }

     private void setTableBody(PrintWriter out) throws Exception
     {
          for(int i=0;i<size();i++)
          {
               out.println("<tr>");
               out.println("<td align='right'><font color='#FFFFFF'>"+(i+1)+"</font></td>");
               for(int j=0;j<SMap.length;j++)
               {
                    if(iDisp[j]==0)
                         continue;
                    if(iGroup[j]!=0)
                         continue;

                    String str    = SMap[j];
                    String SValue = getValue(str,i);
                    String SAlign = (SMapType[j].equals("N") || SMapType[j].equals("D")?"right":"left");
                    SValue        = (SMapType[j].equals("D")?common.getRound(SValue,iDec[j]):SValue);

                    out.println("<td align='"+SAlign+"'><font color='#FFFFFF'>"+SValue+"</font></td>");
                    if(iSum[j]==1)
                         dFoot[j] = dFoot[j]+common.toDouble(getValue(str,i));
               }
               out.println("</tr>");
          }
          setTableFoot(out);
     }

     private void setTableFoot(PrintWriter out) throws Exception
     {
          out.println("<tr>");
          out.println("<td><b><font color='#000099'>&nbsp;</font></b></td>");
          for(int i=0;i<iSum.length;i++)
          {
               if(iDisp[i]==0)
                    continue;
               if(iGroup[i]!=0)
                    continue;
               if(iSum[i]==0)
                    out.println("<td align='right'><b><font color='#000099'>&nbsp;</font></b></td>");
               else
                    out.println("<td align='right'><b><font color='#000099'>"+common.getRound(dFoot[i],iDec[i])+"</font></b></td>");
          }
          out.println("  </tr>");
          out.println("  </table>");
     }

     private void initSum()
     {
          dFoot = new double[SMap.length];
          for(int i=0;i<dFoot.length;i++)
               dFoot[i]=0;
     }

}

//          out.println("<table border='1' width='750' height='54'>");
