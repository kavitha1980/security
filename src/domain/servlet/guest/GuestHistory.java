package domain.servlet.guest;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;

import rndi.*;
import domain.servlet.jreporter.*;

public class GuestHistory extends HttpServlet implements CodedNames
{

     HttpSession session;
     String SServer="";
     String SEndDate="",SStDate="";
     
     int  iStDate=0,iEnDate=0;

     Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          response.setContentType("text/html");
          PrintWriter out    = response.getWriter();
          session            = request.getSession(false);
          SServer            = (String)session.getValue("Server");

          iStDate            = common.toInt(common.pureDate(request.getParameter("stDate")));
          iEnDate            = common.toInt(common.pureDate(request.getParameter("endDate")));

          SEndDate           = common.pureDate(request.getParameter("endDate"));
          SStDate            = common.pureDate(request.getParameter("stDate")); 


          out.println("<html>");
          out.println("<head>");
          out.println("<title>New Page 1</title>");
          out.println("</head>");
          out.println("");
          out.println("<body bgcolor='#9AA8D6'>");

          try
          {
               setSlicedReport(out,request);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }

          out.print("</body>");
          out.print("</html>");
          out.close();
     }

     private void setSlicedReport(PrintWriter out,HttpServletRequest request) throws Exception
     {
          try
          {

               PropertiesHandler theHandler = new PropertiesHandler("GuestHistory");

               String SHead[]     = theHandler.getHead();
               String SHead2[]    = theHandler.getHead2();
               String SMap[]      = theHandler.getMap();
               String SMapType[]  = theHandler.getType();
               int    iMapWidth[] = theHandler.getMapWidth();

               int iDisp[]        = theHandler.getConst(request,"Display");
               int iGroup[]       = theHandler.getConst(request,"Group");
               int iSum[]         = theHandler.getConst(request,"Sum");
               int iDec[]         = theHandler.getConst(request,"Dec");

               String SStart      = common.parseDate(String.valueOf(iStDate));
               String SEnd        = common.parseDate(String.valueOf(iEnDate));

               SliceManager sm    = new SliceManager(SHead,SHead2,SMap,SMapType);

               sm.setConstraint(iDisp,iGroup,iSum,iDec);
               sm.setMapWidth(iMapWidth);
               sm.setInterval(" | ");
               sm.setTitle("Guest History");
               sm.setLimits(SStart,SEnd);

               sm.setHistory(getQS());
               sm.toHTML(out);
                    
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/temp/GuestHistory.prn");
               sm.toFile(FW);
               FW.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

     private String getQS()
     {              
          String QS =    " Select InDate,InTime,GuestName,GuestPlace,ToMeet, "+
                         " OutTime,OutDate, "+
                         " TO_DATE(CONCAT(INDATE,INTIME),'YYYYMMDD HH:MI:SS AM')- "+
                         " TO_DATE(CONCAT(OUTDATE,OUTTIME),'YYYYMMDD HH:MI:SS AM') as TimeDiff "+
                         " From GuestTable "+
                         " Where OutDate >= "+iStDate+" "+
                         " And OutDate <= "+iEnDate+" ";
          try
          {
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/GuestHistory.sql");
               FW.write(QS);
               FW.close();
          }
          catch(Exception e){}
          return QS;
     } 
}

