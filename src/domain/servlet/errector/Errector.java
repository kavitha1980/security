package domain.servlet.errector;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import java.rmi.*;
import java.rmi.registry.*;

import util.*;

import rndi.*;
import domain.servlet.jreporter.*;

public class Errector extends HttpServlet implements rndi.CodedNames
{

     HttpSession session;
     String SServer="";
     String SEndDate="",SStDate="";
     String SErrector;
     String SErrectorSelection = "";

     int  iStDate=0,iEnDate=0;

     Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }
     
     public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {

          response.setContentType("text/html");
          PrintWriter out    = response.getWriter();
          session            = request.getSession(false);
          SServer            = (String)session.getValue("Server");
          
          iStDate            = common.toInt(common.pureDate(request.getParameter("stDate")));
          iEnDate            = common.toInt(common.pureDate(request.getParameter("endDate")));

          SEndDate           = common.pureDate(request.getParameter("endDate"));
          SStDate            = common.pureDate(request.getParameter("stDate")); 

          SErrector           = (String)request.getParameter("company");

          if(!SErrector.equals(String.valueOf(999999)))
               SErrectorSelection = " And Errector.CompanyCode = "+SErrector+" ";

          out.println("<html>");
          out.println("<head>");
          out.println("<title>Errectors Register</title>");
          out.println("</head>");
          out.println("");
          out.println("<body bgcolor='#9AA8D6'>");

          try
          {
               setSlicedReport(out,request);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }

          out.print("</body>");
          out.print("</html>");
          out.close();
          SErrectorSelection = "";
     }

     private void setSlicedReport(PrintWriter out,HttpServletRequest request) throws Exception
     {
          try
          {

               PropertiesHandler theHandler = new PropertiesHandler("Errector");

               String SHead[]     = theHandler.getHead();
               String SHead2[]    = theHandler.getHead2();
               String SMap[]      = theHandler.getMap();
               String SMapType[]  = theHandler.getType();
               int    iMapWidth[] = theHandler.getMapWidth();

               int iDisp[]        = theHandler.getConst(request,"Display");
               int iGroup[]       = theHandler.getConst(request,"Group");
               int iSum[]         = theHandler.getConst(request,"Sum");
               int iDec[]         = theHandler.getConst(request,"Dec");

               String SStart      = common.parseDate(String.valueOf(iStDate));
               String SEnd        = common.parseDate(String.valueOf(iEnDate));

               SliceManager sm    = new SliceManager(SHead,SHead2,SMap,SMapType);
               sm.setConstraint(iDisp,iGroup,iSum,iDec);
               sm.setMapWidth(iMapWidth);
               sm.setInterval(" | ");
               sm.setTitle("Errectors Register");
               sm.setLimits(SStart,SEnd);

               try
               {
                    sm.setHistory(getErrectorQS());
               }
               catch(Exception e){ }
               sm.toHTML(out);
                    
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/d/"+SCRIPTFOLDER+"/src/temp/Errector.prn");
               sm.toFile(FW);
               FW.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     private String getErrectorQS()
     {              
          String QS = " SELECT COMPANY.NAME as CompName,REPRESENTATIVE.NAME as RepName,ERRECTOR.VISITORDATE as VDate, "+
                      " VISITORPURPOSE.NAME as PurName,INTIME,OUTTIME "+
                      " FROM ERRECTOR "+
                      " INNER JOIN COMPANY ON COMPANY.CODE = ERRECTOR.COMPANYCODE "+
                      " INNER JOIN REPRESENTATIVE ON REPRESENTATIVE.CODE = ERRECTOR.REPCODE "+
                      " INNER JOIN VISITORPURPOSE ON VISITORPURPOSE.CODE = ERRECTOR.PURPOSECODE "+
                      " WHERE VISITORDATE >= "+iStDate+" AND VISITORDATE <= "+iEnDate+" "+SErrectorSelection+" ";
          try
          {
               FileWriter FW = new FileWriter("//"+SCRIPTHOST+"/D/"+SCRIPTFOLDER+"/SRC/TEMP/Errector.sql");
               FW.write(QS);
               FW.close();
          }
          catch(Exception e){e.printStackTrace();}
          return QS;
     } 
}

