/*
     The Data Manager manages data that is
     typically required by AJSM in longterm

     This architecture completely decouple
     the underlying storage from the Server;
*/

package domain.jdbc;

import java.sql.*;
import java.util.Vector;
import util.RowSet;
import util.Common;
import java.io.*;

import domain.jdbc.*;

public abstract class DataManager
{

     Connection theConnection=null;
     Connection theConnection1=null;
     Statement theStatement=null;
     CallableStatement theCall=null;

     Common common  = new Common();

     String SDriver   = "oracle.jdbc.OracleDriver";
     String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser     = "gate";
     String SPassword = "gatepass";

     String SDriver1   = "oracle.jdbc.OracleDriver";
     String SDSN1      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser1     = "hrdnew";
     String SPassword1 = "hrdnew";
     boolean flag= false;

     protected DataManager()
     {

     }

     public int setNextId()
     {
          String Sid="";
          try
          {

                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select Max(Id) from MessageSource ";     
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                         Sid  = result.getString(1);
                    }
                    result.close();
                    st.close();
                    //theConnection.close();
                                        
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          int iid   = Integer.parseInt(Sid);
          return iid;

     }

     /* ---------------Key Information -----------------*/

     public Vector getKeyInformation(String SCardNo)
     {
          String QS = " select KeyId,KeyName from KeyInfo Where KeyCode ='"+SCardNo+"' ";

          Vector VId     = new Vector();
          Vector VName   = new Vector();
          Vector VReturnVector  = new Vector();

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {

                    VId.addElement("1");
                    VName.addElement(theResult.getString(2));
               }
	       theResult.close();
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          VReturnVector.addElement(VId);
          VReturnVector.addElement(VName);
          return VReturnVector;
     }

     public void insertKeyInformation(Vector VInfo)
     {
          int iId=0;
          String QS1 = " Select KeySeq.nextVal from Dual ";
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatment    = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS1);
               while(theResult.next())
               {
                    iId       = common.toInt((String)theResult.getString(1));
               }
               theStatement.close();
               Statement theStatement1  = theConnection.createStatement();
               String QS  = " insert into KeyTrans(Id,KeyId,WDTime,StaffCode) values("+iId+", "+
                            " "+common.toInt((String)VInfo.elementAt(0))+", "+
                            " sysdate,"+common.toInt((String)VInfo.elementAt(1))+") " ;

               theStatement1.executeUpdate(QS);
               theStatement1.close();
          }

          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public Vector getStaffInformation()
     {
          Vector VCode   = new Vector();
          Vector VName   = new Vector();
          Vector VInfo   = new Vector();

          Connection theConnection= null;

          try
          {
//               String QS = " Select EmpCode,EmpName from VisitorStaff  Order by 2  " ;

                 String QS = " select empcode,empname,dept from ( "+
                             " select empcode,empname,concat(concat(empname,'   --  '),deptname) as dept from staff "+
                             " inner join department on department.deptcode=staff.deptcode "+
                             " union all "+
                             " select empcode,empname,concat(concat(empname,'   --  '),deptname) as dept from schemeapprentice "+
                             " inner join department on department.deptcode=schemeapprentice.deptcode where schemecode not in(28,24)  "+
                             " union all "+
                             " select empcode,empname,concat(concat(empname,'   --  '),deptname) as dept from contractapprentice "+
                             " inner join department on department.deptcode=contractapprentice.deptcode) "+
                             " Order by 2 ";


               if(theConnection == null)
               {
                    JDBCConnection1 jdbc = JDBCConnection1.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement theStatement    = theConnection.createStatement();
               ResultSet theResult      = theStatement.executeQuery(QS);
               while(theResult.next())
               {
                    VCode.addElement(common.parseNull(theResult.getString(1)));
                    VName.addElement(common.parseNull(theResult.getString(2)));
                    
               }
		theResult.close();
		theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          VInfo.addElement(VCode);
          VInfo.addElement(VName);

          return VInfo;
     }



     /* ---------------End of Key Information -----------------*/


     public Vector setVehicleInInfo(String CardNo)
     {
          Vector vehicleInVector     = new Vector();
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = "Select VehicleNo,VehicleName,DriverName,Purpose,SecurityName,Place ,OutTime ,StKm from Vehicles where CardNo= '"+CardNo+"'  And Status=0 And VCheckCard=1 ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                         for(int i=0;i<8;i++)
                              vehicleInVector.addElement(result.getString(i+1));
                    }
                    result.close();
                    st.close();
                    //theConnection.close();
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return vehicleInVector;
     }
     public Vector setPlaces()
     {
          Vector VPlace     = new Vector();
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = "Select Place from VehiclePlaces order by Place";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                              VPlace.addElement(result.getString(1));
                    }
                    result.close();
                    st.close();
                    //theConnection.close();

          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return VPlace;
     }
     public Vector setVehicleData()
     {

          Vector VData     = new Vector();
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select VehicleRegNo,VehicleName from VehicleInfo where DisplayStatus=1 Order by VehicleName ";
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                              VData.addElement(result.getString(1));
                              VData.addElement(result.getString(2));
                              
                    }
                    result.close();
                    st.close();
                    //theConnection.close();
                    
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return VData;
     }
     public Vector setVehicleNames()
     {
          Vector VData     = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = "Select VehicleName from VehicleInfo ";
               ResultSet result    = st.executeQuery(QS);
               while(result.next())
               {
                    VData.addElement(result.getString(1));
               }
               result.close();
               st.close();
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return VData;
     }

     public Vector setGuestName()
     {

          Vector VData        = new Vector();
          Vector VGuest       = new Vector();
          Vector VToMeet      = new Vector();
          try
          {

               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = "Select GuestName ,ToMeet from GuestTable ";
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    VGuest.addElement(result.getString(1));
                    VToMeet.addElement(result.getString(2));
               }
               result.close();
               stat.close();
	       myConnection.close();
               //theConnection.close();

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
Statement st        = theConnection.createStatement();
String QS           = "Select GuestName ,ToMeet from GuestTable ";
ResultSet result    = st.executeQuery(QS);
while(result.next())
{
          VGuest.addElement(result.getString(1));
          VToMeet.addElement(result.getString(2));
               
}
result.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          VData.addElement(VGuest);
          VData.addElement(VToMeet);

          return VData;
     }

     public ResultSet setVehicleReport(String QS)
     {
          ResultSet result=null;
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    //String QS           = "Select VehicleRegNo,VehicleName from VehicleInfo ";
                    result    = st.executeQuery(QS);
                    st.close();
                    ////theConnection.close();


          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }

           return  result;
     }
     public ResultSet setVehicleFuelReport(String QS)
     {
          ResultSet result=null;
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    //String QS           = "Select VehicleRegNo,VehicleName from VehicleInfo ";
                    result    = st.executeQuery(QS);

                    st.close();
                    ////theConnection.close();

          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }

          return  result;

     }


     public int setVehicleCardNo(String CardNo,int iDate)
     {
          int Vcard=0;
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select VCheckCard from Vehicles where cardNo = '"+CardNo+"' And Status=0 ";
                    ResultSet  result   = st.executeQuery(QS);
                    while(result.next())
                    {
                         String SVCard   = result.getString(1);
                         Vcard           = Integer.parseInt(SVCard);
                    }
               
                    result.close();
                    st.close();
                    //theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setVehicleCardNo) : "+ex);
          }

          return Vcard;
     }
     public int setCheckCard(String CardNo,int iDate)
     {
          int card=-1;
          
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = " Select CheckCard from GuestTable where CardNo = '"+CardNo+"' And Status=0 ";
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    String SCard   = result.getString(1);
                    card           = Integer.parseInt(SCard);
               }
               result.close();
               stat.close();
               myConnection.close();

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}

Statement st        = theConnection.createStatement();
String QS           = " Select CheckCard from GuestTable where CardNo = '"+CardNo+"' And Status=0 ";
ResultSet  result   = st.executeQuery(QS);
while(result.next())
{
     String SCard   = result.getString(1);
     card           = Integer.parseInt(SCard);
}

result.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setCheckCard()) : "+ex);
          }

          return card;

     }
     public int setGuestInitialCheck(String QS)
     {
          int card=-1;
          
          try
          {

               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
                Statement stat   = myConnection.createStatement();
                ResultSet result = stat.executeQuery(QS);
                while(result.next())
                {
                     String SCard   = result.getString(1);
                     card           = Integer.parseInt(SCard);
                }
                result.close();
               stat.close();
                myConnection.close();

/*               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               //String QS           = " Select CheckCard from GuestTable where CardNo = '"+CardNo+"' And Status=0 ";
               ResultSet  result   = st.executeQuery(QS);
               while(result.next())
               {
                    String SCard   = result.getString(1);
                    card           = Integer.parseInt(SCard);
               }
          
               result.close();
               st.close();
               //theConnection.close();
*/
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setGuestInitialCheck()) : "+ex);
               ex.printStackTrace();
          }

          return card;

     }
     public int setInitialCheck(String QS)
     {
          int card=-1;
          
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
                Statement stat   = myConnection.createStatement();
                ResultSet result = stat.executeQuery(QS);
                while(result.next())
                {
                     String SCard   = result.getString(1);
                     card           = Integer.parseInt(SCard);
                }
                result.close();
               stat.close();
                myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DataManager(setInterviewInitialCheck()) : "+ex);
               ex.printStackTrace();
          }

          return card;

     }


     public void setVehicleUpdate(String VCardNo,String SInTime,int IInDate,int SEndKm,String SOutTime,int KDate,int KNo,int KQty)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = "Update Vehicles set InTime ='"+SInTime+"',InDate="+IInDate+",EndKm="+SEndKm+",Status= 1,VCheckCard=0,KIDate="+KDate+",KINo="+KNo+",KQty="+KQty+" where CardNo = '"+VCardNo+"' And  Status=0 And VCheckCard=1 And OutTime='"+SOutTime+"' ";
               st.executeUpdate(QS);
               
               st.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

    }

     public void setVehicleUpdate(String VCardNo,String SInTime,int IInDate,int SEndKm,String SOutTime)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = "Update Vehicles set InTime ='"+SInTime+"',InDate="+IInDate+",EndKm="+SEndKm+",Status= 1,VCheckCard=0 where CardNo = '"+VCardNo+"' And  Status=0 And VCheckCard=1 And OutTime='"+SOutTime+"' ";
               st.executeUpdate(QS);
               st.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

    }

      protected int setInterviewCard(String slno)
      {
          int card=0,status=0,ok=0;
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select CheckCard,Status from InterviewNames where SlipNo='"+slno+"' and Status=0 and CheckCard=0 ";
               ResultSet result    = stat.executeQuery(QS);
               int i=0;
               while(result.next())
               {
                    String SCard   = result.getString(1);
                    String SStatus = result.getString(2);

                    card           = Integer.parseInt(SCard);
                    status         = Integer.parseInt(SStatus);
                    i=i+1;
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          if((card==0)&&(status==0))
               ok=1;

          return ok;
     }

      protected Vector setGuestNames()
     {
          Vector GuestnameVector = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = "Select GuestName from GuestTable where status=0 ";
               ResultSet result = stat.executeQuery(QS);
               GuestnameVector.addElement(String.valueOf(" "));
               while(result.next())
               {
                    GuestnameVector.addElement(result.getString(1));
               }
               result.close();
               stat.close();
               myConnection.close();

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
String QS           = "Select GuestName from GuestTable where status=0 ";
Statement st        = theConnection.createStatement();
ResultSet result    = st.executeQuery(QS);
GuestnameVector.addElement(String.valueOf(" "));
while(result.next())
{
     GuestnameVector.addElement(result.getString(1));
}
result.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return GuestnameVector;
     }

      protected void setGuestInsertData(String QS)
     {
          try
          {
                Class.forName("oracle.jdbc.OracleDriver");
                Connection myConnection  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun");
                Statement stat   = myConnection.createStatement();
                stat.execute(QS);
               stat.close();
                myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }

     public Vector setGuestInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = " Select CardNo,GuestName,InTime,OutTime,GuestPlace,Department,ToMeet,Indate,Outdate from GuestTable where InDate<="+inDate+" And Status= 1 ";
               ResultSet result = stat.executeQuery(QS);
               result = stat.executeQuery(QS);
               while(result.next())
               {
                    vect.addElement(result.getString(1));
                    vect.addElement(result.getString(2));
                    vect.addElement(result.getString(3));
                    vect.addElement(result.getString(4));
                    vect.addElement(result.getString(5));
                    vect.addElement(result.getString(6));
                    vect.addElement(result.getString(7));
                    vect.addElement(result.getString(8));
                    vect.addElement(result.getString(9));
               }
               result.close();
               stat.close();
               myConnection.close();



/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
Statement st        = theConnection.createStatement();
String QS           = " Select CardNo,GuestName,InTime,OutTime,GuestPlace,Department,ToMeet,Indate,Outdate from GuestTable where InDate<="+inDate+" And Status= 1 ";
ResultSet  result   = st.executeQuery(QS);                   
while(result.next())
{
     vect.addElement(result.getString(1));
     vect.addElement(result.getString(2));
     vect.addElement(result.getString(3));
     vect.addElement(result.getString(4));
     vect.addElement(result.getString(5));
     vect.addElement(result.getString(6));
     vect.addElement(result.getString(7));
     vect.addElement(result.getString(8));
     vect.addElement(result.getString(9));
}                             

result.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception ex)
          {
               System.out.println("setGuestInitValues(int inDate) : "+ex);
          }

          return vect;

     }

     protected Vector setGuestData1(String slno)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = " Select GuestName,Sex,GuestPlace,NoPersons,ToMeet,Category,Department,InTime from GuestTable where CardNo = '"+slno+"' And Status=0 And CheckCard=0 " ;
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    for(int i=0;i<8;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();


/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
Statement st        = theConnection.createStatement();
String QS           = " Select GuestName,Sex,GuestPlace,NoPersons,ToMeet,Category,Department,InTime from GuestTable where CardNo = '"+slno+"' And Status=0 And CheckCard=0 " ;
ResultSet result    = st.executeQuery(QS);
while(result.next())                                           
{
     for(int i=0;i<8;i++)
          dataVector.addElement(result.getString(i+1));
}
result.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }


     public void GuestUpdateTime(int slno,String SOutTime,int IOutDate,String Card)
     {
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = "Update GuestTable set OutTime ='"+SOutTime+"',OutDate="+IOutDate+",Status= 1,CheckCard=1 where CardNo = '"+Card+"' And Status=0 And CheckCard=0 ";
               stat.executeUpdate(QS);
               stat.close();
               myConnection.close();

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
Statement st        = theConnection.createStatement();
String QS           = "Update GuestTable set OutTime ='"+SOutTime+"',OutDate="+IOutDate+",Status= 1,CheckCard=1 where CardNo = '"+Card+"' And Status=0 And CheckCard=0 ";
st.executeUpdate(QS);                   
st.close();
//theConnection.close();
*/
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }          

     public int setGuestUpdateSlipNo(String SlipNo)
     {
          int iSlip=0;
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = "Select SlipNo from GuestTable where CardNo='"+SlipNo+"' and Status=0 and CheckCard=0";
               ResultSet result = stat.executeQuery(QS);
               while(result.next())
               {
                    String sSlip = result.getString(1);
                    iSlip = Integer.parseInt(sSlip);
               }
               result.close();
               stat.close();
               myConnection.close();

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}

Statement st        = theConnection.createStatement();
String QS           = "Select SlipNo from GuestTable where CardNo='"+SlipNo+"' and Status=0 and CheckCard=0";
ResultSet rs=st.executeQuery(QS);
while(rs.next())
{
     String sSlip = rs.getString(1);
     iSlip = Integer.parseInt(sSlip);
}
rs.close();
st.close();
//theConnection.close();
*/
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return iSlip;
     }          
     public Vector setOutValues(int iDate)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection       = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Sex,InTime,OutTime,Place,AgentName,CategoryName,HodName from InterviewNames where OutDate="+iDate+" And Status =1 ";
               ResultSet result    = stat.executeQuery(QS);                   
               while(result.next())
               {
                    vect.addElement(result.getString(1));
                    vect.addElement(result.getString(2));
                    vect.addElement(result.getString(3));
                    vect.addElement(result.getString(4));
                    vect.addElement(result.getString(5));
                    vect.addElement(result.getString(6));
                    vect.addElement(result.getString(7));
                    vect.addElement(result.getString(8));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet 1" +ex);
          }

          return vect;

     }
     public Vector setInterviewValues(int iDate1,int iDate2)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection       = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Age,Sex,Quali,Place,Indate,OutDate,InTime,OutTime, "+
                                     " CategoryName,AgentName,Result from InterviewNames "+
                                     " where InDate>="+iDate1+"  and Indate<="+iDate2+" ";
               System.out.println(QS);

               ResultSet result    = stat.executeQuery(QS);                   
               while(result.next())
               {
                    vect.addElement(result.getString(1));
                    vect.addElement(result.getString(2));
                    vect.addElement(result.getString(3));
                    vect.addElement(result.getString(4));
                    vect.addElement(result.getString(5));
                    vect.addElement(result.getString(6));
                    vect.addElement(result.getString(7));
                    vect.addElement(result.getString(8));
                    vect.addElement(result.getString(9));
                    vect.addElement(result.getString(10));
                    vect.addElement(result.getString(11));
                    vect.addElement(result.getString(12));
               }
               result.close();
               stat.close();
	       myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet 1" +ex);
          }
          return vect;
     }
     public Vector setInitValues(int iDate1,int iDate2)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection       = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();


               String QS           = " Select Name,Age,Sex,Quali,Place,Indate,OutDate,InTime,OutTime, "+
                                     " CategoryName,AgentName,Result from InterviewNames "+
                                     " where InDate>="+iDate1+"  and Indate<="+iDate2+" ";


               ResultSet result    = stat.executeQuery(QS);
               System.out.println("DataManager QS :"+QS);
               while(result.next())
               {
                    vect.addElement(result.getString(1));
                    vect.addElement(result.getString(2));
                    vect.addElement(result.getString(3));
                    vect.addElement(result.getString(4));
                    vect.addElement(result.getString(5));
                    vect.addElement(result.getString(6));
                    vect.addElement(result.getString(7));
                    vect.addElement(result.getString(8));
                    vect.addElement(result.getString(9));
                    vect.addElement(result.getString(10));
                    vect.addElement(result.getString(11));
                    vect.addElement(result.getString(12));
               }
               result.close();
               stat.close();
		myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet 1" +ex);
          }
          return vect;
     }

     public int setGuestSlipNo(String QS)
     {
          int id=0;
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               ResultSet  result   = st.executeQuery(QS);                   
               while(result.next())
               {
                    String Sid     = (String)result.getString(1);
                    id             = Integer.parseInt(Sid);
               }

               result.close();
               st.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return id;
     }          



     protected Vector getVect(String QS)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               theStatement           = myConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
               myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getVect() : "+ex.getMessage());
          }
          return vect;
     }


     protected Vector getToMeet(String QS)
     {
          Vector VFatherName  = new Vector();
          Vector VMotherName  = new Vector();
          Vector VAddress     = new Vector();
          Vector ToMeetVector = new Vector();
          Vector VDeptCode    = new Vector();
          Vector VTotal       = new Vector();
          Vector VHostel      = new Vector();
          Vector VEmpCode     = new Vector();
          try
          {
               //Class.forName("oracle.jdbc.OracleDriver");
               //Connection   theConnection     = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
               if(theConnection1==null)
               {
                    JDBCConnection1 jdbc = JDBCConnection1.getJDBCConnection();
                    theConnection1       = jdbc.getConnection();
               }

               Statement st        = theConnection1.createStatement();
               ResultSet result    = st.executeQuery(QS);
               ToMeetVector.addElement(String.valueOf("UNKNOWN"));
               VEmpCode.addElement(String.valueOf("0"));

               while(result.next())
               {

                    String SEmpName = result.getString(1);
                    VFatherName.addElement(result.getString(2));
                    VMotherName.addElement(result.getString(3));
                    VAddress.addElement(result.getString(4));
                    VDeptCode.addElement(result.getString(5));
                    VHostel.addElement(result.getString(6));
                    String SHrdTicketNo = SEmpName+"("+ result.getString(7) +")";
                    ToMeetVector.addElement(SHrdTicketNo);
                    VEmpCode.addElement(result.getString(8));
               }
               result.close();
               st.close();
               //theConnection.close();

          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          VTotal.addElement(ToMeetVector);
          VTotal.addElement(VFatherName);
          VTotal.addElement(VMotherName);
          VTotal.addElement(VAddress);
          VTotal.addElement(VDeptCode);
          VTotal.addElement(VHostel);
          VTotal.addElement(VEmpCode);


          return VTotal;
     }

     protected int getNextId(String QS)
     {
          int id=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                 String   Sid = (String)result.getString(1);
                 id           = Integer.parseInt(Sid);
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getNextId() : "+ex.getMessage());
          }
          
          return id;

     }

     public Vector setGuestInInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat   = myConnection.createStatement();
               String QS        = " Select CardNo,GuestName,Sex,InTime,GuestPlace,Department,ToMeet,Warden,informedtime,meetedtime from GuestTable where InDate="+inDate+" and Status=0 ";
               ResultSet result = stat.executeQuery(QS);
               while(result.next())                                                                                                                        
               {
                    vect.addElement(result.getString(1));
                    vect.addElement(result.getString(2));
                    vect.addElement(result.getString(3));
                    vect.addElement(result.getString(4));
                    vect.addElement(result.getString(5));
                    vect.addElement(result.getString(6));
                    vect.addElement(result.getString(7));
                    vect.addElement(result.getString(8));
                    vect.addElement(result.getString(9));
                    vect.addElement(result.getString(10));

               }                             
               result.close();
               stat.close();
               myConnection.close();

          

/*
if(theConnection==null)
{
     JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
     theConnection       = jdbc.getConnection();
}
Statement st        = theConnection.createStatement();
String QS           = " Select CardNo,GuestName,Sex,InTime,GuestPlace,Department,ToMeet from GuestTable where InDate="+inDate+" and Status=0 ";
ResultSet  result   = st.executeQuery(QS);                   
while(result.next())                                                                                                                        
{
     vect.addElement(result.getString(1));
     vect.addElement(result.getString(2));
     vect.addElement(result.getString(3));
     vect.addElement(result.getString(4));
     vect.addElement(result.getString(5));
     vect.addElement(result.getString(6));
     vect.addElement(result.getString(7));
}                             
result.close();
st.close();
//theConnection.close();
               
*/
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet 2"+ex);
               ex.printStackTrace();
          }

          return vect;

     }

     public Vector setVehicleModelInfo(int inDate)
     {
          Vector vect = new Vector();
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    String QS           = " Select VehicleName,VehicleRegNo,OutTime,Purpose from Vehicles where InDate="+inDate+" and Status=0 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())                                                                                                                        
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                    }                             
               
                    result.close();
                    st.close();
                    //theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet in DM(setVehicleModelInfo)"+ex);
          }

          return vect;

     }

     public int setDeptCode(int iStaffCode)
     {
          String SDept="";   
          try
          {
               String QS = "Select DeptCode from Staff where EmpCode="+iStaffCode+" ";
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement             = theConnection.createStatement();
               ResultSet result         = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SDept          = result.getString(1);
               }                   
               result.close();
               theStatement.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
               int iDptCode        = Integer.parseInt(SDept);
          return iDptCode;
     }

     public int setHodCode(int iDeptCode)
     {
          String SDept="";   
          try
          {
               String QS = "Select HodCode from Hod where DeptCode="+iDeptCode+" ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement             = theConnection.createStatement();
               ResultSet result         = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SDept          = result.getString(1);
               }                   
               result.close();
               theStatement.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
               int iDptCode        = Integer.parseInt(SDept);
          return iDptCode;
     }
     public String setHostId(int iDeptCode)
     {
          String SHost="";   
          try
          {
               String QS = "Select HostName from Hod where DeptCode="+iDeptCode+" ";
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement             = theConnection.createStatement();
               ResultSet result         = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SHost               = result.getString(1);
               }                   
               result.close();
               theStatement.close();
               //theConnection.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SHost;
     }
     protected Vector setVisitorReport(int StDate,int EndDate,String SPurpose)
     {

          System.out.println("DataManager : SPurpose "+SPurpose);
          String obj=null;
          Vector vect = new Vector();
          try
          {
              /* String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.USERNAME,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,Rawuser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         //" Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         " Visitor.Out         = '1' And "+ 
                         " Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+  
                         " and VisitorPurpose.Name='"+SPurpose+"' "+ 
                         " order by Visitor.VisitorDate ";*/
						 
						  String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo, "+
						" Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate,Visitor.slno from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+""+
						" and VisitorPurpose.Name='"+SPurpose+"' "+ 
						" order by Visitor.VisitorDate ";						

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorReport() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setVisitorReport(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
             /*  String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,RawUser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         //" Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         " Visitor.Out         = '1' And "+ 
                        "  Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+"  order by Visitor.VisitorDate ";*/
						 String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo, "+
						" Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate,Visitor.slno from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+""+
						" order by Visitor.VisitorDate ";						

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorReport() : "+ex.getMessage());
          }
          return vect;
     }
     protected Vector setVisitorCheckReport(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
              /* String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate,Visitor.slno from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,RawUser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         //" Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         " Visitor.Out         = '1' And "+ 
                         "  Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and oncheck is null order by Visitor.VisitorDate ";*/
						 String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo, "+
						" Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate,Visitor.slno from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+""+
						" and oncheck is null order by Visitor.VisitorDate ";						

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     vect.addElement(result.getString(10));
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorReport() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setVisitorReport(int iSlipNo,int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
              /* String QS = " Select 		Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,RawUser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                        // " Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						 " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         " Visitor.Out         = '1' And "+ 
                        "  Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and SlipNo="+iSlipNo+"  order by Visitor.VisitorDate ";*/
												
						String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName, "+
						" Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and SlipNo="+iSlipNo+""+
						" order by Visitor.VisitorDate ";
						
						//System.out.println("Visitor Report=="+QS);

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorReport() : "+ex.getMessage());
          }
          return vect;
     }
     protected Vector setVisitorReport1(int iSlipNo,int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
              /* String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,RawUser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         //" Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         "  Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and SlipNo="+iSlipNo+"  order by Visitor.VisitorDate ";*/
						 
						 String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName, "+
						" Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and SlipNo="+iSlipNo+""+
						" order by Visitor.VisitorDate ";						

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(common.parseNull(result.getString(1)));
                     vect.addElement(common.parseNull(result.getString(2)));
                     vect.addElement(common.parseNull(result.getString(3)));
                     vect.addElement(common.parseNull(result.getString(4)));
                     vect.addElement(common.parseNull(result.getString(5)));
                     vect.addElement(common.parseNull(result.getString(6)));
                     vect.addElement(common.parseNull(result.getString(7)));
                     vect.addElement(common.parseNull(result.getString(8)));
                     vect.addElement(common.parseNull(result.getString(9)));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorReport1() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setVisitorSlipNo(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {

               String QS            = " Select Distinct(SlipNo) from Visitor Where VisitorDate >= "+StDate+" and VisitorDate<="+EndDate+" ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorSlipNO() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setErrectorWiseReport(int StDate,int EndDate,int iErrectorCode)
     {
          String obj=null;
	  String QS = " ";
          Vector vect = new Vector();
          try
          {
		 if(StDate<20160229)
		 {               
			QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as 	EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" AND Errector.RepCode = "+iErrectorCode+"  order by Errector.VisitorDate ";
		}
		else{
			QS = " Select PartyMaster.PartyName,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as 	EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from SCM.PartyMaster,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " to_char(Errector.CompanyCode) = PartyMaster.PartyCode AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" AND Errector.RepCode = "+iErrectorCode+"  order by Errector.VisitorDate ";
		}
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               //System.out.println(QS);
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getErrector wise() : "+ex.getMessage());
          }
          return vect;
     }
     public boolean isCardExists(String SCardNo)
     {
          int iCount=0;
          try
          {
               String QS = "Select Count(*) from Visitor Where CardNo='"+SCardNo+"' and Out='0' ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement theStatement   = theConnection.createStatement();
               ResultSet result         = theStatement.executeQuery(QS);
               while(result.next())
               {
                    iCount              = common.toInt(result.getString(1));
               }
               theStatement.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          if(iCount>0)
               return true;
          else
               return false;
     }
               
     public Vector setErrectorInfo(String SCompanyCode)
     {
          String obj=null;
          Vector VCode = new Vector();
          Vector VName = new Vector();
          //int iCompanyCode = Integer.parseInt(SCompanyCode);

          Vector VTotal  = new Vector();

          try
          {
               //String QS = " Select Code,Name from Representative where CompanyCode = "+iCompanyCode+"  order by 2 ";
			   String QS = " Select Code,Name from Representative where CompanyCode = '"+SCompanyCode+"' and displaystatus=1 order by 2 ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     VCode.addElement(result.getString(1));
                     VName.addElement(result.getString(2));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorInfo() : "+ex.getMessage());
          }
          VTotal.addElement(VCode);
          VTotal.addElement(VName);

          return VTotal;
     }

     protected Vector setErrectorSlipNo(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {

               String QS            = " Select Distinct(SlipNo) from Errector Where VisitorDate >= "+StDate+" and VisitorDate<="+EndDate+" ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorSlipNo() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setVisitorAbstractReport(int StDate,int EndDate,int Companycode,int repCode)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
            /*   String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName,Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Company,Representative,VisitorPurpose,VisitorStaff,Visitor,RawUser"+
                         " where "+
                         " Visitor.CompanyCode = Company.Code AND"+
                         " Visitor.RepCode     = Representative.Code AND"+
                         " Visitor.purposeCode = VisitorPurpose.Code AND"+
                         //" Visitor.StaffCode   = VisitorStaff.EmpCode AND"+
						  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                         " Visitor.Out         = '1' And "+ 
                         " Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" And Visitor.CompanyCode="+Companycode+" And Visitor.RepCode = "+repCode+"  order by Visitor.VisitorDate ";*/
						 
						 
						 String QS = " Select Company.Name,Visitor.SlipNo,Representative.Name,VisitorPurpose.Name,RawUser.UserName, "+
						" Visitor.CardNo,Visitor.InTime,Visitor.OutTime,Visitor.VisitorDate from Visitor "+
						" inner join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
						" inner join company on company.CODE=visitor.COMPANYCODE "+
						" inner join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
						" inner join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
						" where Visitor.Out = '1' And "+
						" Visitor.VisitorDate >="+StDate+" and Visitor.VisitorDate<="+EndDate+" and  Visitor.CompanyCode="+Companycode+""+
						" And Visitor.RepCode = "+repCode+"  order by Visitor.VisitorDate ";
						 

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in VisitorAbst() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setVisitorInInfo(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS =    " Select Company.Name,Visitor.SlipNo,   "+
                              " Representative.Name,                  "+
                              " VisitorPurpose.Name,                  "+
                              " RawUser.UserName,                 "+
                              " Visitor.CardNo,                       "+
                              " Visitor.InTime,                       "+
                              " Visitor.OutTime,                      "+
                              " Visitor.VisitorDate                   "+
                              " from                                  "+
                              " Company,Representative,VisitorPurpose,"+
                              " VisitorStaff,Visitor,RawUser          "+
                              " where                                 "+
                              " Visitor.CompanyCode = Company.Code AND "+
                              " Visitor.RepCode     = Representative.Code AND   "+
                              " Visitor.purposeCode = VisitorPurpose.Code AND   "+
                             // " Visitor.StaffCode   = VisitorStaff.EmpCode AND  "+
							  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                              " Visitor.Out         = '0' And                   "+ 
                              " Visitor.VisitorDate >="+StDate+" And            "+
                              " Visitor.VisitorDate<="+EndDate+"            "+
                              " order by Visitor.VisitorDate ";  

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               //System.out.println("DataManager in setVisitorInfo() : "+ex.getMessage());
               ex.printStackTrace();
          }
          return vect;
     }

     protected Vector setErrectorInInfo(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
          	String QS =    " SELECT ERRECTOR.VISITORDATE, "+
                    " ERRECTOR.INTIME,             "+
                    " ERRECTOR.OUTTIME,            "+
                    " PARTYMASTER.PARTYNAME,       "+
                    " REPRESENTATIVE.NAME,         "+
                    " VISITORPURPOSE.NAME          "+
                    " FROM SCM.PARTYMASTER,ERRECTOR,REPRESENTATIVE,VISITORPURPOSE  "+
                    " WHERE to_char(Errector.CompanyCode) =   PartyMaster.PartyCode AND "+
                    " REPRESENTATIVE.CODE=ERRECTOR.REPCODE AND             "+
                    " VISITORPURPOSE.CODE=ERRECTOR.PURPOSECODE AND         "+
		    " ERRECTOR.VISITORDATE ="+StDate+" "+
                    " ORDER BY ERRECTOR.VISITORDATE ";

		/*		
		String QS =    " SELECT ERRECTOR.VISITORDATE, "+
                    " ERRECTOR.INTIME,             "+
                    " ERRECTOR.OUTTIME,            "+
                    " COMPANY.NAME,                "+
                    " REPRESENTATIVE.NAME,         "+
                    " VISITORPURPOSE.NAME          "+
                    " FROM COMPANY,ERRECTOR,REPRESENTATIVE,VISITORPURPOSE  "+
                    " WHERE COMPANY.CODE=ERRECTOR.COMPANYCODE AND          "+
                    " REPRESENTATIVE.CODE=ERRECTOR.REPCODE AND             "+
                    " VISITORPURPOSE.CODE=ERRECTOR.PURPOSECODE AND         "+
                    " ERRECTOR.OUT='0' ORDER BY ERRECTOR.VISITORDATE ";
		*/
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));

                     //vect.addElement(result.getString(7));
                     //vect.addElement(result.getString(8));
                     //vect.addElement(result.getString(9));
                     
               }
               result.close();
               //theConnection.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               //System.out.println("DataManager in setVisitorInfo() : "+ex.getMessage());
               ex.printStackTrace();
          }
          return vect;
     }
     protected Vector setErrectorOutInfo(int StDate,int EndDate) 
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               /*String QS =    " SELECT ERRECTOR.VISITORDATE, "+
                              " ERRECTOR.INTIME,             "+
                              " ERRECTOR.OUTTIME,            "+
                              " COMPANY.NAME,                "+
                              " REPRESENTATIVE.NAME,         "+
                              " VISITORPURPOSE.NAME          "+
                              " FROM COMPANY,ERRECTOR,REPRESENTATIVE,VISITORPURPOSE  "+
                              " WHERE COMPANY.CODE=ERRECTOR.COMPANYCODE AND          "+
                              " REPRESENTATIVE.CODE=ERRECTOR.REPCODE AND             "+
                              " VISITORPURPOSE.CODE=ERRECTOR.PURPOSECODE AND         "+
                              " ERRECTOR.VISITORDATE  >= "+EndDate+" AND ERRECTOR.OUT='1' ORDER BY ERRECTOR.VISITORDATE ";
              */
	      String QS =     " SELECT ERRECTOR.VISITORDATE, "+
                              " ERRECTOR.INTIME,             "+
                              " ERRECTOR.OUTTIME,            "+
                              " PartyMaster.PartyName,        "+
                              " REPRESENTATIVE.NAME,         "+
                              " VISITORPURPOSE.NAME          "+
                              " FROM SCM.PartyMaster,ERRECTOR,REPRESENTATIVE,VISITORPURPOSE  "+
                              " WHERE to_char(Errector.CompanyCode) =   PartyMaster.PartyCode AND "+
                              " REPRESENTATIVE.CODE=ERRECTOR.REPCODE AND             "+
                              " VISITORPURPOSE.CODE=ERRECTOR.PURPOSECODE AND         "+
                              " ERRECTOR.VISITORDATE  >= "+EndDate+" AND ERRECTOR.OUT='1' ORDER BY ERRECTOR.VISITORDATE ";


               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
               }

               result.close();
               theStatement.close();
               //theConnection.close();
          }
          catch(Exception ex)
          {
               //System.out.println("DataManager in setVisitorInfo() : "+ex.getMessage());
               ex.printStackTrace();
          }
          return vect;
     }

     protected Vector setErrectorAsOnReport(int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS =    " SELECT COMPANY.NAME,REPRESENTATIVE.NAME,VISITORPURPOSE.NAME,ERRECTOR.INTIME, "+
                              " ERRECTOR.OUTTIME,ERRECTOR.VISITORDATE                                        "+
                              " FROM ((COMPANY INNER JOIN ERRECTOR ON COMPANY.CODE=ERRECTOR.COMPANYCODE)     "+
                              " INNER JOIN REPRESENTATIVE ON ERRECTOR.REPCODE=REPRESENTATIVE.CODE)           "+
                              " INNER JOIN VISITORPURPOSE ON ERRECTOR.PURPOSECODE=VISITORPURPOSE.CODE        "+
                              " WHERE ((ERRECTOR.VISITORDATE)="+EndDate+") AND                               "+
                              " ERRECTOR.OUT='1' ORDER BY ERRECTOR.VISITORDATE                               "; 


               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement         = theConnection.createStatement();
               ResultSet result     = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
               }
               result.close();
               theStatement.close();
               //theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println("DM in setErrectorAsOnReport() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setErrectorAsOnReport1(int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS =    " SELECT COMPANY.NAME,REPRESENTATIVE.NAME,VISITORPURPOSE.NAME,ERRECTOR.INTIME, "+
                              " ERRECTOR.OUTTIME,ERRECTOR.VISITORDATE                                        "+
                              " FROM ((COMPANY INNER JOIN ERRECTOR ON COMPANY.CODE=ERRECTOR.COMPANYCODE)     "+
                              " INNER JOIN REPRESENTATIVE ON ERRECTOR.REPCODE=REPRESENTATIVE.CODE)           "+
                              " INNER JOIN VISITORPURPOSE ON ERRECTOR.PURPOSECODE=VISITORPURPOSE.CODE        "+
                              " WHERE ((ERRECTOR.VISITORDATE)='"+EndDate+"')  "+
                              " ORDER BY ERRECTOR.VISITORDATE  "; 

               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement         = theConnection.createStatement();
               ResultSet result     = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
               }
               result.close();
               theStatement.close();
               //theConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println("DM in setErrectorAsOnReport1() : "+ex.getMessage());
               ex.printStackTrace();
          }
          return vect;
     }

     protected Vector setVisitorOutInfo(int StDate,int EndDate) 
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
            /*   String QS =    " Select Company.Name,Visitor.SlipNo,   "+
                              " Representative.Name,                  "+
                              " VisitorPurpose.Name,                  "+
                              " VisitorStaff.EmpName,                 "+
                              " Visitor.CardNo,                       "+
                              " Visitor.InTime,                       "+
                              " Visitor.OutTime,                      "+
                              " Visitor.VisitorDate                   "+
                              " from                                  "+
                              " Company,Representative,VisitorPurpose,"+
                              " VisitorStaff,Visitor                  "+
                              " where                                 "+
                              " Visitor.CompanyCode = Company.Code AND "+
                              " Visitor.RepCode     = Representative.Code AND   "+
                              " Visitor.purposeCode = VisitorPurpose.Code AND   "+
                              " Visitor.StaffCode   = VisitorStaff.EmpCode AND  "+
                              " Visitor.Out         = '1' And                   "+ 
                              " Visitor.VisitorDate >="+StDate+" And            "+
                              " Visitor.VisitorDate<="+EndDate+"            "+
                             " order by Visitor.VisitorDate ";  */ 
 							String QS =    " Select Company.Name,Visitor.SlipNo,   "+
                              " Representative.Name,                  "+
                              " VisitorPurpose.Name,                  "+
                              " RawUser.USERNAME,                 	  "+
                              " Visitor.CardNo,                       "+
                              " Visitor.InTime,                       "+
                              " Visitor.OutTime,                      "+
                              " Visitor.VisitorDate                   "+
                              " from                                  "+
                              " Company,Representative,VisitorPurpose,"+
                              " VisitorStaff,Visitor,RawUser          "+
                              " where                                 "+
                              " Visitor.CompanyCode = Company.Code AND "+
                              " Visitor.RepCode     = Representative.Code AND   "+
                              " Visitor.purposeCode = VisitorPurpose.Code AND   "+
                              //" Visitor.StaffCode   = VisitorStaff.EmpCode AND  "+
							  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                              " Visitor.Out         = '1' And                   "+ 
                              " Visitor.VisitorDate >="+StDate+" And            "+
                              " Visitor.VisitorDate<="+EndDate+"            "+
							  " order by Visitor.VisitorDate ";  							 

               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }

               result.close();
               theStatement.close();
               //theConnection.close();
          }
          catch(Exception ex)
          {
               //System.out.println("DataManager in setVisitorInfo() : "+ex.getMessage());
               ex.printStackTrace();
          }
          return vect;
     }
     public void updateData(String SVehicleNo,int iFuelKm,int iCon1)
     {
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement theStatement = myConnection.createStatement();
               String QS = " update vehicles set FuelKms="+iFuelKm+" Where VehicleNo='"+SVehicleNo+"' and StKm="+iCon1+" ";
               theStatement.executeUpdate(QS);
               theStatement.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }


     protected Vector setVisitorCompanyInfo()
     {
          String obj=null;
          Vector VCode = new Vector();
          Vector VName = new Vector();

          Vector VTotal  = new Vector();

          try
          {
               //String QS = " Select Company.Code,Company.Name from Company  order by 2 ";
			   String QS = " Select Code,Name from Company where displaystatus=1 order by 2 ";


               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     VCode.addElement(result.getString(1));
                     VName.addElement(result.getString(2));
                     
               }
               result.close();
               theStatement.close();
               //theConnection.close();
               
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorCompanyInfo() : "+ex.getMessage());
          }
          VTotal.addElement(VCode);
          VTotal.addElement(VName);

          return VTotal;
     }

     protected Vector setErrectorCompanyInfo()
     {
          String obj=null;
          Vector VCode = new Vector();
          Vector VName = new Vector();

          Vector VTotal  = new Vector();

          try
          {
		String QS = " Select PartyCode,PartyName from scm.PartyMaster order by 2 ";


               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     VCode.addElement(result.getString(1));
                     VName.addElement(result.getString(2));
                     
               }
               result.close();
               theStatement.close();
               //theConnection.close();
               
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorCompanyInfo() : "+ex.getMessage());
          }
          VTotal.addElement(VCode);
          VTotal.addElement(VName);

          return VTotal;
     }

     protected Vector setVisitorRepInfo(int CCode)
     {
          String obj=null;
          Vector VCode = new Vector();
          Vector VName = new Vector();

          Vector VTotal  = new Vector();

          try
          {

               String QS = " Select Code,Name from Representative where CompanyCode = '"+CCode+"' ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }

               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     VCode.addElement(result.getString(1));
                     VName.addElement(result.getString(2));
                     
               }
               result.close();
               theStatement.close();
               //theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setVisitorRep() : "+ex.getMessage());
          }
          VTotal.addElement(VCode);
          VTotal.addElement(VName);

          return VTotal;
     }

     protected Vector setErrectorReport(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' '  as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+"  order by Errector.VisitorDate ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorReport() : "+ex.getMessage());
          }
          return vect;
     }
     protected Vector setErrectorCheckReport(int StDate,int EndDate)
     {
          String obj=null;
          Vector vect = new Vector();
          try
          {
               String QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' '  as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And  "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" and oncheck is null order by Errector.VisitorDate ";

               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorReport() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setErrectorReport(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          String obj=null;
	  String QS = "";
          Vector vect = new Vector();
          try
          {
		System.out.println("===sNewVersion in 20 server --"+common.parseNull(sNewVersion));
		if(common.parseNull(sNewVersion).equals("1"))
		{
			QS = " Select PartyMaster.PartyName,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from SCM.PartyMaster,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " to_char(Errector.CompanyCode) =   PartyMaster.PartyCode and"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
		}
		else{
                QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
		}              
		System.out.println("setErrectorReport Qry From--20-->"+QS);

 /*String QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         " Errector.Out         = '1' And "+ 
                        "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
                      // System.out.println(QS);
*/
               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorReport(slip,std,en)() : "+ex.getMessage());
          }
          return vect;
     }

     protected Vector setErrectorReport1(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          String obj=null;
          Vector vect = new Vector();
		  String QS = "";
          try
          {
		  		 if(common.parseNull(sNewVersion).equals("1")){
			  	QS = " Select PartyMaster.PartyName,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from SCM.PartyMaster,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " to_char(Errector.CompanyCode) =   PartyMaster.PartyCode and"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
			  }
			  else{
			  	QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
			  }
               /*
			   String QS = " Select Company.Name,Errector.SlipNo,Representative.Name,VisitorPurpose.Name,' ' as EmpName,Errector.CardNo,Errector.InTime,Errector.OutTime,Errector.VisitorDate from Company,Representative,VisitorPurpose,Errector"+
                         " where "+
                         " Errector.CompanyCode = Company.Code AND"+
                         " Errector.RepCode     = Representative.Code AND"+
                         " Errector.purposeCode = VisitorPurpose.Code AND"+
                         "  Errector.VisitorDate >="+StDate+" and Errector.VisitorDate<="+EndDate+" And Errector.SlipNo="+iSlipNo+"  order by Errector.VisitorDate ";
                      // System.out.println(QS);
					  
					  FileWriter FW = new FileWriter("/software/YarnGodownStatutoryReports/setErrectorReport.txt");
          			  FW.write(QS);
			          FW.close();
					  
				*/	  
					  
               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
                     vect.addElement(result.getString(2));
                     vect.addElement(result.getString(3));
                     vect.addElement(result.getString(4));
                     vect.addElement(result.getString(5));
                     vect.addElement(result.getString(6));
                     vect.addElement(result.getString(7));
                     vect.addElement(result.getString(8));
                     vect.addElement(result.getString(9));
                     
               }
		result.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setErrectorReport(slip,std,en)() : "+ex.getMessage());
          }
          return vect;
     }

     protected String setVisitorInitialCheck(String QS)
     {
          String SReturn =null;
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                    SReturn = result.getString(1);
               }
               result.close();
               theStatement.close();
          }         
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SReturn;
     }
               
     protected Vector getCode(String QS)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               theStatement           = myConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                     vect.addElement(result.getString(1));
               }
               result.close();
               theStatement.close();
		myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in getcode() : "+ex.getMessage());
          }
          return vect;
     }
     protected Vector getName()
     {
          Vector nameVector = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               String QS           = "Select Name from InterviewNames where status=0 ";
               Statement stat        = myConnection.createStatement();
               ResultSet result    = stat.executeQuery(QS);
               nameVector.addElement(String.valueOf(" "));
               while(result.next())
               {
                    nameVector.addElement(result.getString(1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return nameVector;
     }
     protected Vector getEmpName()
     {
          Vector nameVector = new Vector();
          try
          {
               Class.forName(SDriver1);
               Connection myConnection = DriverManager.getConnection(SDSN1,SUser1,SPassword1);
               String QS           = "Select EmpName from SchemeApprentice  ";
               Statement stat      = myConnection.createStatement();
               ResultSet result    = stat.executeQuery(QS);
               nameVector.addElement(String.valueOf(" "));
               while(result.next())
               {
                    nameVector.addElement(result.getString(1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return nameVector;
     }
     

     protected int setSlNo(String QS)
     {
          int id=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               theStatement           = theConnection.createStatement();
               ResultSet result       = theStatement.executeQuery(QS);
               while(result.next())
               {
                 String   Sid = (String)result.getString(1);
                 id           = Integer.parseInt(Sid);
               }
               result.close();
               theStatement.close();
               //theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("DataManager in setSlno() : "+ex.getMessage());
          }
          
          return id;

     }

     protected int setComboCode(int index,Vector vect)
     {

          String Scode   = (String)vect.elementAt(index);
          int    iCode   = Integer.parseInt(Scode);
          return iCode;
     }
     protected Vector setReportVector(int iStDate,int iEndDate)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,outdate,Age,Result from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" And Status= 1 ";

               ResultSet result    = stat.executeQuery(QS);
               while(result.next())
                {
                    for(int i=0;i<12;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }

     protected Vector setAsOnReportVector(int iStDate,int iEndDate)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,outdate,Age,Result from InterviewNames where InDate ="+iStDate+"  And Status= 1 ";

               ResultSet result    = stat.executeQuery(QS);
               while(result.next())
                {
                    for(int i=0;i<12;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }


     protected Vector setInsideInterviewReportVector(int iStDate,int iEndDate)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,outdate,Age,Result from InterviewNames where InDate >="+iStDate+" And inDate <="+iEndDate+"  order by id";

               ResultSet result    = stat.executeQuery(QS);
               while(result.next())
                {
                    for(int i=0;i<12;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }

     protected Vector setReportCheckVector(int iStDate,int iEndDate)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,Age,Result from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" And Status= 1 and oncheck is null order by 1";

               ResultSet result    = stat.executeQuery(QS);
               while(result.next())
                {
                    for(int i=0;i<11;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }

     protected Vector setReportVector1(int iStDate,int iEndDate,String SResult,String SCatogory,String SAgent,String SPlace,String SQualification)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
              // String QS           = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,Age,Result from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" And Status= 1 ";
            String QString = " Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,outdate,Age,Result from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" and status=1 ";

              if(!SResult.equals("All"))
              {
               QString =QString+ " and result='"+SResult+"' ";
               System.out.println("Result:"+SResult);
               flag=true;
               }
            if(!SCatogory.equals("All"))
           {
              QString  = QString + " and  categoryname='"+SCatogory+"'";
              System.out.println("Category:"+SCatogory);
              flag=true;
           }
            if(!SAgent.equals("All"))
           {
              QString  = QString + " and  agentname='"+SAgent+"'";
              System.out.println("Agent:"+SAgent);
                flag=true;
           }
            if(!SPlace.equals("All"))
            {
              QString  = QString + " and  place  ='"+SPlace+"' ";
              System.out.println("Place:"+SPlace);
              flag=true;
            }
              if(!SQualification.equals("All"))
             {
              QString  = QString + " and  Quali ='"+SQualification+"'" ;
              System.out.println("Qualification:"+SQualification);
              flag=true;
             }

             else if(flag==true)
             {
              System.out.println("Security Project");
             }
             else

             QString =" Select Name,Sex,Quali,Place,AgentName,CategoryName,InTime,OutTime,InDate,outdate,Age,Result from InterviewNames where InDate >="+iStDate+" And OutDate <="+iEndDate+" and status=1 ";


               ResultSet result    = stat.executeQuery(QString);
               System.out.println(QString);
               while(result.next())
                {
                    for(int i=0;i<12;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }
      


     protected Vector setData(String slno)
     {
          Vector dataVector   = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = " Select InDate,Name,Age,Sex,Quali,Place,InTime,CategoryName,HodName,AgentName from InterviewNames where SlipNo='"+slno+"' and Status=0 and CheckCard=0 ";
               ResultSet result    = stat.executeQuery(QS);
               while(result.next())                                           
               {
                    for(int i=0;i<10;i++)
                         dataVector.addElement(result.getString(i+1));
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return dataVector;
     }


     protected void insertDetails(Vector infoVector)
     {
          //theCall = theConnection.prepareCall(getCallSQS(procedure,VRows.size()));
          
          String SDate        = (String)infoVector.elementAt(0);
          int iInDate         = Integer.parseInt(SDate);
          String SName        = (String)infoVector.elementAt(1);
          String Sage         = (String)infoVector.elementAt(2);
          int iAge            = Integer.parseInt(Sage);
          String SSex         = (String)infoVector.elementAt(3);
          String SQuali       = (String)infoVector.elementAt(4);
          String SInTime      = (String)infoVector.elementAt(5);
          String SCategory    = (String)infoVector.elementAt(6);
          String SHod         = (String) infoVector.elementAt(7);
          String SAgent       = (String) infoVector.elementAt(8);
          String SOutTime     = (String)infoVector.elementAt(9);
          String SId          = (String)infoVector.elementAt(10);
          String SOutDate     = (String)infoVector.elementAt(11);
          String SPlace       = (String)infoVector.elementAt(12);
          String slNo         = (String)infoVector.elementAt(14);
          int iId             =   Integer.parseInt(SId);
          int iOutDate        = Integer.parseInt(SOutDate);
          
          String QS =  " insert into interviewNames(InDate,Name,Age,Sex,Quali,InTime,CategoryName,HodName,AgentName,OutTime,Id,OutDate,Place,Status,slipno,CheckCard) values("+iInDate+",'"+SName+"',"+iAge+",'"+SSex+"','"+SQuali+"','"+SInTime+"','"+SCategory+"','"+SHod+"','"+SAgent+"','"+SOutTime+"',"+iId+","+iOutDate+",'"+SPlace+"',0,'"+slNo+"',0) ";
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat     = myConnection.createStatement();
               stat.executeUpdate(QS);
               stat.close();
               myConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println("insert interviewnames"+ex);
          }

     }
     public int getId(String sl)
     {
          int id=0;
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement stat      = myConnection.createStatement();
               String QS           = "Select Id from InterviewNames where SlipNo='"+sl+"' And Status=0 And CheckCard=0 ";
               ResultSet  result   = stat.executeQuery(QS);                   
               while(result.next())
               {
                    String Sid     = (String)result.getString(1);
                    id             = Integer.parseInt(Sid);
               }
               result.close();
               stat.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return id;
     }          
     public void  insertName(String QS)
     {
          int id=0;
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st        = myConnection.createStatement();
               //String QS           = "Select Id from InterviewNames where Name = '"+SName+"' And InTime ='"+SinTime+"' ";
               st.executeUpdate(QS);
               st.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }          
     public String  setMaxCode(String QS)
     {
          String SCode="";
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                  SCode     = (String)rs.getString(1);
                  //code = Integer.parseInt(SCode);
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SCode;
     }
     public Vector setInfo(String QS)
     {
          Vector vect = new Vector();
          int code=0;
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st        = myConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }
               rs.close();
               st.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }
     public String  setNextSlipNo(int iIndicator)
     {
          String SlipNo="";
          int iSlip=0;
          String QS= " ";
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               if(iIndicator ==0)
               {
                    QS           = " Select  Max(SlipNo) from Visitor ";
               }
               else
               {
                    
                    QS           = " Select  Max(SlipNo) from Errector ";
               }     
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    SlipNo = rs.getString(1);

                    if(SlipNo == null)
                    {
                         iSlip=0;     
                    }
                    else
                    {
                         iSlip = Integer.parseInt(SlipNo);
                    }
                    iSlip = iSlip+1;
                    SlipNo = Integer.toString(iSlip);
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SlipNo;
     }
     public int setSlip(String QS)
     {
          int slNo=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SSlNo   = (String)rs.getString(1);
                    slNo           = Integer.parseInt(SSlNo);
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return slNo;
     }

     public int  setSlipNo(String iCardNo)
     {
          int cardNo=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = " Select  SLIPNO from Visitor where Out='0' and  CardNo='"+iCardNo+"' ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCardNo = (String)rs.getString(1);
                    cardNo         = Integer.parseInt(SCardNo);
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return cardNo;
     }
     public int  setErrectorSlipNo(String iCardNo)
     {
          int cardNo=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = "Select  SlipNo from Errector where Out=0 and  CardNo='"+iCardNo+"' ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCardNo = (String)rs.getString(1);
                    cardNo         = Integer.parseInt(SCardNo);
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return cardNo;
     }

     public void updateOutTime(String STime,int repCode)
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = " Update Visitor set OutTime='"+STime+"',Out='1' where RepCode="+repCode+" And Out='0' ";
               st.executeUpdate(QS);
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void updateErrectorOutTime(String STime,int repCode)
     {
          try
          {
               String SOutDate = common.getServerDate();
//               String SOut = 

               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String QS           = " Update Errector set OutTime='"+STime+"',Out='1',VisitorOutDate='"+SOutDate+"'  where RepCode="+repCode+" And Out='0' ";
               st.executeUpdate(QS);
               st.close();

               System.out.println(QS);
               System.out.println("OutDate:"+SOutDate);

               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     
     public Vector setVect(int iSlipNo)
     {
	 	  System.out.println("In Visitor Vect");
          Vector vect = new Vector();
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
              /* String QS           = "Select Visitor.slipno,Company.Name,Representative.Name,VisitorPurpose.Name,"+
                                     " VisitorStaff.EmpName,Visitor.cardno,Visitor.intime,Visitor.repcode"+
                                     " from"+
                                     " Company,Representative,VisitorPurpose,VisitorStaff,Visitor,Rawuser"+
                                     " where"+
                                     " Visitor.CompanyCode =   Company.code and"+
                                     " Visitor.repcode     =   Representative.code and"+
                                     " Visitor.purposecode =   VisitorPurpose.Code and"+
                                     //" Visitor.staffcode   =   VisitorStaff.empcode and"+
									  " Visitor.StaffCode   = RawUser.USERCODE AND  "+
                                     " Visitor.out         =   '0' and"+
                                     " Visitor.slipno      =   "+iSlipNo+" ";*/
									 String QS           =" Select Visitor.slipno,Company.Name,Representative.Name,VisitorPurpose.Name,"+
									" RawUser.UserName,Visitor.cardno,Visitor.intime,Visitor.repcode from Visitor "+
									" Left join REPRESENTATIVE on REPRESENTATIVE.code=visitor.repcode "+
									" Left join company on company.CODE=visitor.COMPANYCODE "+
									" Left join visitorpurpose on visitorpurpose.CODE=visitor.PURPOSECODE "+
									" Left join Rawuser on RawUser.USERCODE=Visitor.StaffCode "+
									" where Visitor.Out = '0' and Visitor.SLIPNO = "+iSlipNo+" order by Visitor.VisitorDate";
									
									System.out.println("In Visitor Qry-->"+QS);

               ResultSet rs        = st.executeQuery(QS);
               
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
                    vect.addElement(rs.getString(7));
                    vect.addElement(rs.getString(8));
               }
               rs.close();
               st.close();
               //theConnection.close();
          
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }
     public Vector setErrectorVect(int iSlipNo)
     {
          Vector vect = new Vector();
	  String STDate = common.getServerDate();
	  String QS           = "";
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
	       			QS =	" Select Errector.slipno,PartyMaster.PartyName,Representative.Name,VisitorPurpose.Name,"+
                                     	" Errector.cardno,Errector.intime,Errector.repcode"+
                                     	" from"+
                                     	" SCM.PartyMaster,Representative,VisitorPurpose,Errector"+
                                     	" where"+
                                     	" to_char(Errector.CompanyCode) =   PartyMaster.PartyCode and"+
                                     	" Errector.repcode     =   Representative.code and"+
                                     	" Errector.purposecode =   VisitorPurpose.Code and"+
                                     	" Errector.out         =   '0' and"+
                                     	" Errector.slipno      =   "+iSlipNo+" ";
	       /*if(common.toDouble(STDate)<20160229)
	       {
			QS	= "Select Errector.slipno,Company.Name,Representative.Name,VisitorPurpose.Name,"+
				" Errector.cardno,Errector.intime,Errector.repcode"+
				" from"+
				" Company,Representative,VisitorPurpose,Errector"+
				" where"+
				" Errector.CompanyCode =   Company.code and"+
				" Errector.repcode     =   Representative.code and"+
				" Errector.purposecode =   VisitorPurpose.Code and"+
				" Errector.out         =   '0' and"+
				" Errector.slipno      =   "+iSlipNo+" ";
	       }
	       else
	       {
			 QS           = "Select Errector.slipno,PartyMaster.PartyName,Representative.Name,VisitorPurpose.Name,"+
                                     " Errector.cardno,Errector.intime,Errector.repcode"+
                                     " from"+
                                     " PartyMaster,Representative,VisitorPurpose,Errector"+
                                     " where"+
                                     " to_char(Errector.CompanyCode) =   PartyMaster.PartyCode and"+
                                     " Errector.repcode     =   Representative.code and"+
                                     " Errector.purposecode =   VisitorPurpose.Code and"+
                                     " Errector.out         =   '0' and"+
                                     " Errector.slipno      =   "+iSlipNo+" ";
	       }*/

               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
                    vect.addElement(rs.getString(7));
               }
               rs.close();
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }


     public void  storeData(String slipno,Vector vect)
     {
          int code=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String  QS          = " Insert into Visitor(slno,SlipNo,CompanyCode,RepCode,PurposeCode,StaffCode,CardNo,InTime,VisitorDate,Out,OutTime,VehicleType,RegCode,DriverCode,DEPTCODE) Values("+vect.elementAt(0)+","+vect.elementAt(1)+","+vect.elementAt(2)+","+vect.elementAt(3)+","+vect.elementAt(4)+","+vect.elementAt(5)+",'"+vect.elementAt(6)+"','"+vect.elementAt(7)+"',"+vect.elementAt(8)+",'0','0',"+vect.elementAt(9)+","+vect.elementAt(10)+","+vect.elementAt(11)+","+vect.elementAt(12)+") ";
               st.executeUpdate(QS);
               st.close();
               //theConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public void storeErrectorData(String slipno,Vector vect)
     {
          int code=0;
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String  QS          = "Insert into Errector(SlipNo,CompanyCode,RepCode,PurposeCode,StaffCode,CardNo,InTime,VisitorDate,Out,OutTime,UnitCode,DeptCode) Values("+vect.elementAt(1)+",'"+vect.elementAt(2)+"',"+vect.elementAt(3)+","+vect.elementAt(4)+","+vect.elementAt(5)+",'"+vect.elementAt(6)+"','"+vect.elementAt(7)+"',"+vect.elementAt(8)+",'0','0',"+vect.elementAt(9)+","+vect.elementAt(10)+")";
               st.executeUpdate(QS);
               st.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     
     public void setUpdateTimeAndDate(String SOutTime,int iOutDate,int id,int iResult,String SHod)
     {
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st        = myConnection.createStatement();
               String QS           = " Update InterviewNames set Result="+iResult+",OutTime ='"+SOutTime+"',OutDate="+iOutDate+",Status= 1,CheckCard=1,HodName='"+SHod+"'  where Id = "+id+" And Status=0 And CheckCard=0 ";
               st.executeUpdate(QS);                   
               st.close();
               myConnection.close();
          }
          catch(Exception e)                                                                                                                                                       
          {
               e.printStackTrace();
          }
     }          

     public int setNo(String QS)
     {
          int no=0;
          try
          {
               Class.forName("oracle.jdbc.OracleDriver");
               Connection myConnection  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","inventory0405","stores0405");
               Statement  st       = myConnection.createStatement();
               ResultSet  rs       = st.executeQuery(QS);
               while(rs.next())
               {
                   String sno      = rs.getString(1);
                   no = Integer.parseInt(sno);
               }
               rs.close();
               st.close();
	       myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     return no;
     }
     public Vector setInitValues(int inDate)
     {
          Vector vect = new Vector();
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
                    Statement st        = myConnection.createStatement();
                    String QS           = " Select Name,Sex,InTime,Place,AgentName,CategoryName,HodName from InterviewNames where InDate>="+inDate+" And status=0 ";
                    ResultSet  result   = st.executeQuery(QS);                   
                    while(result.next())
                    {
                         vect.addElement(result.getString(1));
                         vect.addElement(result.getString(2));
                         vect.addElement(result.getString(3));
                         vect.addElement(result.getString(4));
                         vect.addElement(result.getString(5));
                         vect.addElement(result.getString(6));
                         vect.addElement(result.getString(7));
                    }                             
                    result.close();
                    st.close();
                    myConnection.close();

          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet  4"+ex);
          }

          return vect;

     }
     public void updateKm(String CardNo,int endKm)
     {
          try
          {
               //Class.forName("oracle.jdbc.OracleDriver");
               //Connection conn =       DriverManager.getConnection("jdbc:oracle:thin:@oracle1:1521:hrd","scott","tiger");

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st    =       theConnection.createStatement();
               String QS       =       "Update VehicleKm set endkm="+endKm+" where cardNo= '"+CardNo+"' ";
               st.executeUpdate(QS);
               st.close();
          }
          catch(Exception e)
          {
                e.printStackTrace();
          }
          

     }
     public void insertVehicleValues(Vector vect)
     {
          try
          {

               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st    =       myConnection.createStatement();
               String QS       =       " Insert into Vehicles(Cardno,VehicleNo,VehicleName,OutTime,Stkm,EndKm,place,Purpose,OutDate,InDate,"+
                                       " InTime,DIno,Kino,Didate,kidate,Dqty,kqty,DriverName,SecurityName,Traveller,Status,VCheckCard,BunkName,BunkCode, Remarks,  id) "+
                                       " Values('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"', "+vect.elementAt(4)+", "+
                                       " "+vect.elementAt(5)+",'"+vect.elementAt(6)+"','"+vect.elementAt(7)+"',"+vect.elementAt(8)+","+vect.elementAt(9)+", "+
                                       " '"+vect.elementAt(10)+"','"+vect.elementAt(11)+"','"+vect.elementAt(12)+"',"+vect.elementAt(13)+","+vect.elementAt(14)+","+
//                                       " "+vect.elementAt(15)+","+vect.elementAt(16)+",'"+vect.elementAt(17)+"','"+vect.elementAt(18)+"','"+vect.elementAt(19)+"',0,1,'"+vect.elementAt(21)+"') ";
                                       " "+vect.elementAt(15)+","+vect.elementAt(16)+",'"+vect.elementAt(17)+"','"+vect.elementAt(18)+"','"+vect.elementAt(19)+"',0,1,'"+vect.elementAt(21)+"','"+vect.elementAt(22)+"','"+vect.elementAt(23)+"',vehicles_seq.nextval) ";

               System.out.println("<--QS Value-->"+QS);
               st.executeUpdate(QS);
               st.close();
               myConnection.close();

          }
          catch(Exception e)
          {
                System.out.println(e);
          }

     }

     //Vehicle Movement
     public void insertVehicleMovementValues(Vector vect)
     {
          try
          {

               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st    =       myConnection.createStatement();

              String QS       =  " INSERT INTO VEHICLEDETAILS (CARDNO, VEHICLENO, VEHICLENAME, TOTALKMS, PLACE, PURPOSE, MOVEMENTDATE, "+
                                 " DINO, KINO, DIDATE, KIDATE, KQTY, DQTY, DRIVERNAME, SECURITYNAME, TRAVELLER, "+
                                 " STATUS, VCHECKCARD, SLIPNO, BUNKNAME, TANKFILLED, FUELKMS, ONCHECK, "+
                                 " ID, BUNKCODE, ACCSTATUS, ACCODE, REMARKS) "+
                                 " VALUES('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"','"+vect.elementAt(4)+"', "+
                                 " '"+vect.elementAt(5)+"','"+vect.elementAt(6)+"','"+vect.elementAt(7)+"','"+vect.elementAt(8)+"','"+vect.elementAt(9)+"', "+
                                 " '"+vect.elementAt(10)+"','"+vect.elementAt(11)+"','"+vect.elementAt(12)+"','"+vect.elementAt(13)+"','"+vect.elementAt(14)+"', "+
                                 " '"+vect.elementAt(15)+"','"+vect.elementAt(16)+"','"+vect.elementAt(17)+"','"+vect.elementAt(18)+"','"+vect.elementAt(19)+"', "+
                                 " '"+vect.elementAt(20)+"','"+vect.elementAt(21)+"','"+vect.elementAt(22)+"','"+vect.elementAt(23)+"','"+vect.elementAt(24)+"', "+
                                 "  VEHICLEDETAILS_SEq.nextVal,'"+vect.elementAt(25)+"','"+vect.elementAt(26)+"','"+vect.elementAt(27)+"')";


               System.out.println("<--QS Value-->"+QS);
               st.executeUpdate(QS);
               st.close();
               myConnection.close();

          }
          catch(Exception e)
          {
                System.out.println(e);
          }

     }




     public void insertVValues(Vector vect)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st    =       theConnection.createStatement();
               Statement st1   =       theConnection.createStatement(); 
               String QS       =       "Insert into Vehicles(Cardno,VehicleNo,VehicleName,OutTime,Stkm,EndKm,place,Purpose,OutDate,InDate"+
                                        "InTime,DriverName,Status,VCheckCard,BunkName,Id)"+
                                        "Values('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"',"+vect.elementAt(4)+","+
                                        " "+vect.elementAt(5)+",'"+vect.elementAt(6)+"', '"+vect.elementAt(7)+"',"+vect.elementAt(8)+","+vect.elementAt(9)+","+
                                        " '"+vect.elementAt(10)+"','"+vect.elementAt(11)+"',0,1,vehicles_seq.nextval)";             

               st.executeUpdate(QS);
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

     }

     public Vector setCurrentInfo(int Date)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =       theConnection.createStatement();
               //String QS         = "Select VehicleName,VehicleNo,OutTime,DriverName,StKm,Purpose from Vehicles where OutDate="+Date+" and Status=0 ";
               String QS           = "Select VehicleName,VehicleNo,OutTime,DriverName,StKm,Purpose from Vehicles where  Status=0 ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
               }
               st.close();

               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       

     public Vector setCurrentInInfo(int Date)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =  theConnection.createStatement();
               //String QS           = "Select Indate,VehicleName,VehicleNo,InTime,DriverName,EndKm,Purpose from Vehicles where OutDate="+Date+" and Status=1 ";
               String QS           = "Select Indate,VehicleName,VehicleNo,InTime,DriverName,EndKm,Purpose "+
                                     " from (Select Indate,VehicleName,VehicleNo,InTime,DriverName,EndKm,Purpose,"+
                                     " max(EndKm) over (partition by vehicleno) as MaxKm from Vehicles "+
                                     " where OutDate="+Date+" and Status=1) where EndKm = MaxKm ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
                    vect.addElement(rs.getString(7));
               }
               rs.close();
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }



     public Vector setDriverInfo()
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        =      theConnection.createStatement();
               String QS           = " Select Name  from VehicleDrivers order by 1 ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
               }

               rs.close();
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }
     public Vector setSecurityInfo()
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =      theConnection.createStatement();
               String QS           = "Select Name  from SecurityNames";
               ResultSet rs        = st.executeQuery(QS);
               vect.addElement(" ");
               while(rs.next())
               {                    
                    vect.addElement(rs.getString(1));
               }

               rs.close();
               st.close();

               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setPurpose()
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st        =       theConnection.createStatement();
               String QS           = "Select Purpose  from VehiclePurpose Order By ID";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
               }
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setKm(String card)
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =   theConnection.createStatement();
               String QS           = "Select StKm,EndKm from VehicleKm where CardNo = '"+card+"' ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }

     public Vector setInfo(String card,int i)
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =       theConnection.createStatement();
               String QS           = "Select VehicleRegNo,VehicleName from VehicleInfo  where CardNo ='"+card+"'  ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }    
     public int setCheckCardStatus(String CardNo)
     {
          int card=-1;

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =       theConnection.createStatement();
               String QS           =      "Select  Status from Vehicles  where CardNo ='"+CardNo+"' And VCheckCard=1 And Status=0 ";
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCard = (String)rs.getString(1);
                    card         = Integer.parseInt(SCard);
               }

               rs.close();
               st.close();
               ////conn.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return card;
     }

     protected Vector appendRowSet(RowSet rowSet,String STable,String SSequence,String SPKColumn)
     {
          Vector vect    = new Vector();
          Vector VColumn = rowSet.getColumnName();
          Vector VRows   = rowSet.getRows();
          Vector VType   = rowSet.getColumnType();

          String QS   = prepareInsertQS(VColumn,STable,SPKColumn);
          String SQS  = "Select "+SSequence+".nextval from dual";

          try
          {
               RJDBCConnection jdbc  = RJDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               PreparedStatement pstmt   = theConnection.prepareStatement(QS);
               theStatement              = theConnection.createStatement();

               for(int i=0;i<VRows.size();i++)
               {
                    int iValue = 0;
                    ResultSet result = theStatement.executeQuery(SQS);
                    if(result.next())
                    {
                         iValue=result.getInt(1);
                         vect.addElement(String.valueOf(iValue));
                    }
                    result.close();
                    pstmt.setInt(1,iValue);
                    Vector aRow = (Vector)VRows.elementAt(i);
                    for(int j=0;j<VColumn.size();j++)
                    {
                         int iType = common.toInt((String)VType.elementAt(j));
                         if(iType == java.sql.Types.INTEGER)
                              pstmt.setInt(j+2,common.toInt((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.DOUBLE)
                              pstmt.setDouble(j+2,common.toDouble((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.VARCHAR)
                              pstmt.setString(j+2,(String)aRow.elementAt(j));
                    }
                    pstmt.execute();
               }
               pstmt.close();
               theStatement.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return vect;
     }
     private String prepareInsertQS(Vector VColumn,String STable)
     {
          int iSize = VColumn.size();

          String QS = "Insert Into "+STable+"(";
          for(int i=0;i<VColumn.size();i++)
          {
               String SCol = (String)VColumn.elementAt(i);
               QS = QS+(String)VColumn.elementAt(i)+",";
          }
          QS = QS.substring(0,QS.length()-1);
          QS = QS+") Values (";

          for(int i=0;i<iSize;i++)
               QS = QS+"?,";
          QS = QS.substring(0,QS.length()-1)+")";
          return QS;
     }

     private String prepareInsertQS(Vector VColumn,String STable,String SPKColumn)
     {
          int iSize = VColumn.size();

          String QS = "Insert Into "+STable+"(";
          if(SPKColumn != null)
          {
               QS = QS+SPKColumn+",";
               iSize++;
          }
          for(int i=0;i<VColumn.size();i++)
          {
               String SCol = (String)VColumn.elementAt(i);
               if(SCol.equals(SPKColumn))
               {
                    iSize=iSize-1;
                    continue;
               }
               QS = QS+(String)VColumn.elementAt(i)+",";
          }
          QS = QS.substring(0,QS.length()-1);
          QS = QS+") Values (";

          for(int i=0;i<iSize;i++)
               QS = QS+"?,";
          QS = QS.substring(0,QS.length()-1)+")";
          return QS;
     }

     private String prepareUpdateQS(Vector VColumn,String STable,String SPKColumn,String SPKValue)
     {
          int iSize = VColumn.size();

          String QS = "Update "+STable+" set ";
          for(int i=0;i<VColumn.size();i++)
          {
               QS = QS+(String)VColumn.elementAt(i)+"=?,";
          }
          QS = QS.substring(0,QS.length()-1);

          QS = QS+" Where "+SPKColumn+"="+SPKValue;

          return QS;
     }

     private String prepareUpdateQS(Vector VColumn,String STable,String SPKColumn)
     {
          int iSize = VColumn.size();

          String QS = "Update "+STable+" set ";
          for(int i=0;i<VColumn.size();i++)
          {
               QS = QS+(String)VColumn.elementAt(i)+"=?,";
          }
          QS = QS.substring(0,QS.length()-1);

          QS = QS+" Where "+SPKColumn+"=?";

          return QS;
     }

     protected void setRowSet(RowSet rowSet,String STable,String SPKColumn,String SPKValue)
     {
          Vector VColumn = rowSet.getColumnName();
          Vector VRows   = rowSet.getRows();
          Vector VType   = rowSet.getColumnType();

          String QS   = prepareUpdateQS(VColumn,STable,SPKColumn,SPKValue);
          try
          {
               RJDBCConnection jdbc  = RJDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               PreparedStatement pstmt   = theConnection.prepareStatement(QS);
               for(int i=0;i<VRows.size();i++)
               {
                    Vector aRow = (Vector)VRows.elementAt(i);
                    for(int j=0;j<VColumn.size();j++)
                    {
                         int iType = common.toInt((String)VType.elementAt(j));
                         if(iType == java.sql.Types.INTEGER)
                              pstmt.setInt(j+1,common.toInt((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.DOUBLE)
                              pstmt.setDouble(j+1,common.toDouble((String)aRow.elementAt(j)));
                         else if(iType == java.sql.Types.VARCHAR)
                              pstmt.setString(j+1,(String)aRow.elementAt(j));
                    }
                    pstmt.execute();
               }
               pstmt.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex.getMessage());
          }
     }


     protected Vector setPrimaryRowSet(int id,RowSet rowSet,String theProcedure)
     {

          Vector VPrimaryKey = new Vector();
          VPrimaryKey.addElement(String.valueOf(id));
          Vector VColumn = rowSet.getColumnName();
          Vector VType   = rowSet.getColumnType();
          Vector VRows   = rowSet.getRows();

          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               Statement stat       = theConnection.createStatement();

               /*theCall = theConnection.prepareCall(getCallQS(theProcedure,VColumn.size()));
               System.out.println("RowSize:"+VRows.size());*/

               /*for(int i=0;i<VRows.size();i++)
               {
                    Vector row = (Vector)VRows.elementAt(i);

                    for(int j=0;j<row.size();j++)
                    {
                         int iType = Integer.parseInt((String)VType.elementAt(j));

                         theCall.setObject(j+1,(String)row.elementAt(j));
                    }
                    theCall.registerOutParameter(row.size()+1,java.sql.Types.INTEGER);
                    theCall.execute();
                    Integer Key = new Integer(theCall.getInt(VColumn.size()));
                    VPrimaryKey.addElement(String.valueOf(Key));


                    

               }*/

               Vector row          = (Vector)VRows.elementAt(0);

               int iUserCode       = Integer.parseInt((String)row.elementAt(0));
               int iSubjectCode    = 1;
               int iMsgDate        = Integer.parseInt((String)row.elementAt(2));
               String STime        = (String)row.elementAt(3); 

               String QS           = " Insert into MessageSource(Id,USerCode,SubjectCode,MsgDate,MsgTime,MsgFileName) "+
                                     " values("+id+","+iUserCode+",1,"+iMsgDate+",'"+STime+"',"+id+") ";
               stat.executeUpdate(QS);
		stat.close();

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
          return VPrimaryKey;
     }
     /*
          Setting Primary Rowset
     */    

     protected void setSecondaryRowSet(RowSet rowSet,String theProcedure)
     {

          Vector VColumn = rowSet.getColumnName();
          Vector VType   = rowSet.getColumnType();
          Vector VRows   = rowSet.getRows();

          try
          {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               Statement stat       = theConnection.createStatement();

               /*theCall = theConnection.prepareCall(getCallSQS(theProcedure,VColumn.size()));

               for(int i=0;i<VRows.size();i++)
               {
                    Vector row = (Vector)VRows.elementAt(i);
                    for(int j=0;j<row.size();j++)
                    {
                         int iType = Integer.parseInt((String)VType.elementAt(j));
                         theCall.setObject(j+1,(String)row.elementAt(j));
                    }
                    theCall.execute();
               }
               theCall.close();
               */

               Vector row          = (Vector)VRows.elementAt(0);
               int  iId            = Integer.parseInt((String)row.elementAt(0));
               int  iUser          = Integer.parseInt((String)row.elementAt(1));
               String QS           = "Insert into MessageTarget(Id,TargetUserCode,Status) values "+
                                     "("+iId+","+iUser+",0)"; 
               stat.executeUpdate(QS);
		stat.close(); 
          }
          catch(Exception ex)
          {
               System.out.println("Secondary RowSet 5"+ex);
          }
     }

     private String getCallSQS(String theProcedure,int isize)
     {
          String QS = "Call"+theProcedure+"(";

          for(int i=0;i<isize-1;i++)
               QS = QS+"?,";

          QS = QS + "?)";
          return QS;
     }

     private String getCallQS(String theProcedure,int isize)
     {
          String QS = "Call "+theProcedure+"(";

          for(int i=0;i<isize-1;i++)
               QS = QS+"?,";

          QS = QS + "?)";

          return QS;
     }


     /* ---------------Rent Vehicle Information -----------------*/

     public void  saveRentVehicleData(Vector vect)
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               String  QS          = " Insert into RentVehicleInfo(cardno,vehicleregno,vehiclename,purpose,ownercode,Status,RentalStatus,Id) Values('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"',"+vect.elementAt(4)+","+vect.elementAt(5)+","+vect.elementAt(6)+","+vect.elementAt(7)+")";
               st.executeUpdate(QS);
               st.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public Vector setRVInfo(String QS)
     {
          Vector vect = new Vector();
          int code=0;
          try
          {
               Class.forName(SDriver);                         
               Connection myConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
               Statement st        = myConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
               }
               rs.close();
               st.close();
               myConnection.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return vect;
     }



     public int setRVCheckCardAvailable(String QS)
     {
          int iCount=0;

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCount = (String)rs.getString(1);
                    iCount        = Integer.parseInt(SCount);
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return iCount;
     }

     public int setRVCheckCardStatus(String QS)
     {
          int card=-1;

          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    String SCard = (String)rs.getString(1);
                    card         = Integer.parseInt(SCard);
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return card;
     }

     public Vector setRentVehicleInfo(String QS)
     {
          Vector vect = new Vector();
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }
     public void insertRentVehicleValues(String QS1,String QS2)
     {
          try
          {

               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st1   =       theConnection.createStatement();
               Statement st2   =       theConnection.createStatement(); 
               st1.executeUpdate(QS1);
               st2.executeUpdate(QS2);
               st1.close();
               st2.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

     }
     public Vector setRVCurrentInfo(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
                    vect.addElement(rs.getString(4));
                    vect.addElement(rs.getString(5));
                    vect.addElement(rs.getString(6));
                    vect.addElement(rs.getString(7));
                    vect.addElement(rs.getString(8));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       
     public Vector setRVVehicleOutInfo(String QS)
     {
          Vector vehicleInVector     = new Vector();
          try
          {
                    if(theConnection==null)
                    {
                         JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                         theConnection       = jdbc.getConnection();
                    }
                    Statement st        = theConnection.createStatement();
                    ResultSet result    = st.executeQuery(QS);
                    while(result.next())
                    {
                         for(int i=0;i<10;i++)
                              vehicleInVector.addElement(result.getString(i+1));
                    }
                    result.close();
                    st.close();
          }
          catch(Exception exc)
          {
               exc.printStackTrace();
          }
          return vehicleInVector;
     }
     public Vector setRVActiveID(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =  theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
                    vect.addElement(rs.getString(3));
               }
               rs.close();
               st.close();
          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }
     public Vector setRVCurrentOutInfo(Vector VActiveId)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        =  theConnection.createStatement();

               int m=0;

               for(int i=0;i<VActiveId.size()/3;i++)
               {

                   String SId     = (String)VActiveId.elementAt(0+m);
                   String STripNo = (String)VActiveId.elementAt(1+m);
                   String SStatus = (String)VActiveId.elementAt(2+m);
                   m=m+3;

                   if(SStatus.equals("0"))
                        continue;

                   String QS = "Select OutDate,VehicleName,VehicleNo,OutTime,DriverName,EndKm,Purpose from RentVehicles where Id="+SId+" And TripNos="+STripNo ;


                   ResultSet rs = st.executeQuery(QS);
                   while(rs.next())
                   {
                        vect.addElement(rs.getString(1));
                        vect.addElement(rs.getString(2));
                        vect.addElement(rs.getString(3));
                        vect.addElement(rs.getString(4));
                        vect.addElement(rs.getString(5));
                        vect.addElement(rs.getString(6));
                        vect.addElement(rs.getString(7));
                   }
                   rs.close();
               }
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }
     public void setRVVehicleUpdate(String QS1,String QS2)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st1 = theConnection.createStatement();
               Statement st2 = theConnection.createStatement();
               st1.executeUpdate(QS1);
               st2.executeUpdate(QS2);
               st1.close();
               st2.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }



     /* ---------------End of Rent Vehicle Information -----------------*/


     /* ----------------------Thabal Information ----------------------*/


     public Vector setThabalDespMode(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       
     public Vector setThabalType(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       

     public Vector setDistinctCourier(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement stat     = theConnection.createStatement();

               ResultSet rs       = stat.executeQuery(QS);

               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               stat.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       

     public Vector setThabalSummary(Vector VDespCode,Vector VTypeCode,int iDate)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement stat     = theConnection.createStatement();


               for(int i=0;i<VDespCode.size();i++)
               {
                    String SDespCode=(String)VDespCode.elementAt(i);
                    for(int j=0;j<VTypeCode.size();j++)
                    {
                        String STypeCode=(String)VTypeCode.elementAt(j);
                        String QS = " Select count(*) from thabaloutwardnew where status=0 and updateflag=0 and despatchercode="+SDespCode+" and TypeCode="+STypeCode+" and outwarddate<="+iDate;
                        ResultSet rs = stat.executeQuery(QS);
                        while(rs.next())
                        {
                             vect.addElement(common.parseNull(rs.getString(1)));
                        }
                        rs.close();
                    }
               }
               stat.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       

     public void setUpdateThabalStatus(String QS)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st = theConnection.createStatement();
               st.executeUpdate(QS);
               st.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }

     public Vector setCourierDetails(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       
     public Vector setMessengerDetails(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    vect.addElement(rs.getString(1));
                    vect.addElement(rs.getString(2));
               }

               rs.close();
               st.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       

     public Vector setGateThabalInward(String QS)
     {
          Vector vect = new Vector();
          try
          {                               
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }

               Statement stat     = theConnection.createStatement();

               ResultSet rs        = stat.executeQuery(QS);

               while(rs.next())
               {
                    vect.addElement(common.parseNull(rs.getString(1)));
                    vect.addElement(common.parseNull(rs.getString(2)));
                    vect.addElement(common.parseNull(rs.getString(3)));
                    vect.addElement(common.parseNull(rs.getString(4)));
                    vect.addElement(common.parseNull(rs.getString(5)));
                    vect.addElement(common.parseNull(rs.getString(6)));
                    vect.addElement(common.parseNull(rs.getString(7)));
                    vect.addElement(common.parseNull(rs.getString(8)));
                    vect.addElement(common.parseNull(rs.getString(9)));
                    vect.addElement(common.parseDate(rs.getString(10)));
               }

               rs.close();
               stat.close();

          }
          catch(Exception e)
          {
                e.printStackTrace();
          }

          return vect;
     }       
     public void insertThabalGI(String QS)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st = theConnection.createStatement();
               st.executeUpdate(QS);
               st.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }
     public void setUpdateOldThabalGI(String QS)
     {
          try
          {
               if(theConnection==null)
               {
                    JDBCConnection jdbc = JDBCConnection.getJDBCConnection();
                    theConnection       = jdbc.getConnection();
               }
               Statement st = theConnection.createStatement();
               st.executeUpdate(QS);
               st.close();

          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

     }

/*     public int getMaxFoodId()
     {
          int       iMaxNo=0;
          String    QS    = " select Max(to_char(substr(Id,2,length(id)))) from Guest_Food_Details ";

          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement st        = theConnection.createStatement();
               ResultSet rs        = st.executeQuery(QS);
               while(rs.next())
               {
                    iMaxNo = rs.getInt(1);
               }
               rs.close();
               st.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }

          return iMaxNo+1;
     } */




     /* -------------------End of Thabal Information --------------------*/


}

 
     
