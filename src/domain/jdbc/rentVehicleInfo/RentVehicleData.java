package domain.jdbc.rentVehicleInfo;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

import java.sql.*;

public class RentVehicleData extends DataManager
{
        public RentVehicleData()
        {

        }

        public Vector getPurpose()
        {
                return setPurpose();
        }
        public Vector getPlaces()
        {
               return setPlaces();
        }  
        public int getRVCheckCardAvailable(String CardNo)
        {
                String QS = "Select count(*) from RentVehicleInfo where CardNo ='"+CardNo+"' And RentalStatus=0";

                return  setRVCheckCardAvailable(QS);
        }
        public int getRVCheckCardStatus(String CardNo)
        {
                String QS = "Select Status from RentVehicleInfo where CardNo ='"+CardNo+"' And RentalStatus=0";

                return  setRVCheckCardStatus(QS);
        }
        public Vector getRentVehicleInfo(String cardno)
        {
                String QS = "Select VehicleRegNo,VehicleName,Purpose,Id from RentVehicleInfo  where CardNo ='"+cardno+"' And Status=1 And RentalStatus=0";

                return setRentVehicleInfo(QS);
        }
        public void insertRVData(Vector vect)
        {
               String QS1 = " Insert into RentVehicles(Cardno,VehicleNo,VehicleName,InTime,Stkm,place,Purpose,InDate,DriverName,Status,Id,TripNos,NoOfPersons) "+
                            " Values('"+vect.elementAt(0)+"','"+vect.elementAt(1)+"','"+vect.elementAt(2)+"','"+vect.elementAt(3)+"', "+vect.elementAt(4)+","+
                            " '"+vect.elementAt(5)+"','"+vect.elementAt(6)+"','"+vect.elementAt(7)+"','"+vect.elementAt(8)+"',"+vect.elementAt(9)+","+
                            " "+vect.elementAt(10)+","+vect.elementAt(11)+","+vect.elementAt(12)+") ";

               String QS2 = " Update RentVehicleInfo set status=0 where CardNo='"+vect.elementAt(0)+"' And Id="+vect.elementAt(10)+" And RentalStatus=0";

               insertRentVehicleValues(QS1,QS2);

        }
        public Vector getRVCurrentInfo(int iDate)
        {
                String QS = "Select VehicleName,VehicleNo,InDate,InTime,DriverName,StKm,Purpose,NoOfPersons from RentVehicles where status=0 order by InDate,InTime desc";

                return setRVCurrentInfo(QS);
        }
        public String getMaxRVTripNo(int iId)
        {
               String QS= " Select Max(TripNos) from RentVehicles where ID="+iId;
               return setMaxCode(QS);
        }
        public Vector getRVVehicleOutInfo(String SCardNo)
        {
                String QS = "Select VehicleNo,VehicleName,DriverName,Purpose,Place,InTime,StKm,InDate,TripNos,Id from RentVehicles where CardNo= '"+SCardNo+"'  And Status=0 ";

                return setRVVehicleOutInfo(QS);
        }
        public Vector getRVActiveID()
        {
                String QS = "Select RentVehicleInfo.Id,max(RentVehicles.TripNos),RentVehicleInfo.Status from RentVehicleInfo "+
                            " Inner Join RentVehicles on RentVehicleInfo.Id=RentVehicles.Id "+
                            " Where RentVehicleInfo.RentalStatus=0 group by RentVehicleInfo.Id,RentVehicleInfo.Status order by RentVehicleInfo.Id Desc";

                return setRVActiveID(QS);
        }
        public Vector getRVCurrentOutInfo(Vector VActiveId)
        {
                return setRVCurrentOutInfo(VActiveId);
        }
        public void getRVVehicleUpdate(String SCardNo,String SOutTime,int iOutDate,int iEndKm,int iRunKm,String STripNo,String SId,int iRentStatus,String SLoad)
        {
                String QS1 = "Update RentVehicles set OutTime ='"+SOutTime+"',OutDate="+iOutDate+",EndKm="+iEndKm+",RunKm="+iRunKm+",Status=1 where CardNo = '"+SCardNo+"' And  Status=0 And Id="+SId+" And TripNos="+STripNo;  //,LoadNo='"+SLoad+"'

                String QS2 = "Update RentVehicleInfo set status=1,RentalStatus="+iRentStatus+" where CardNo='"+SCardNo+"' And Id="+SId;

                setRVVehicleUpdate(QS1,QS2);
        }


}


