package domain.jdbc.vehicleInfo;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

import java.sql.*;

public class VehicleData extends DataManager
{
        public VehicleData()
        {

        }
        public void  updateFuelData(String SVehicleNo,int iStKm,int iFuelKm)
        {
               updateData(SVehicleNo,iStKm,iFuelKm);
        }  
        public Vector getVehicleData()
        {
                return setVehicleData();
        }                             
        public Vector getVehicleNames()
        {
                return setVehicleNames();
        }                             
        public Vector getVehicleModelInfo(int InDate)
        {
                return setVehicleModelInfo(InDate);
        }                             
        public ResultSet getVehicleReport(String QS)
        {
               return setVehicleReport(QS);
        }  
        public ResultSet getVehicleFuelReport(String QS)
        {
               return setVehicleFuelReport(QS);
        }  

        public int getCheckCardStatus(String CardNo)
        {
                return  setCheckCardStatus(CardNo);
        }
        public Vector getCurrentInfo(int iDate)
        {
                return setCurrentInfo(iDate);
        }

        public Vector getCurrentInInfo(int iDate)
        {
                return setCurrentInInfo(iDate);
        }

        public void insertData(Vector vect)
        {
                insertVehicleValues(vect);
        }

        public void insertVehicleDetailsData(Vector vect)
        {
                insertVehicleMovementValues(vect);
        }

        public void insertVData(Vector vect)
        {
                insertVValues(vect);
        }
        public Vector getInfo(String cardno,int i)
        {
                return setInfo(cardno,i);
        }
        public Vector getPlaces()
        {
               return setPlaces();
        }  

        public Vector getDriverInfo()
        {
                return setDriverInfo();
        }
        public Vector getPurpose()
        {
                return setPurpose();
        }

        public Vector getSecurityInfo()
        {
                return setSecurityInfo();
        }

        public Vector getKm(String CardNo)
        {
                return setKm(CardNo);
        }
        public void updateVehicleKm(String CardNo,int endKm)
        {
                updateKm(CardNo,endKm);
        }
        public Vector getVehicleInInfo(String CardNo)
        {
                return setVehicleInInfo(CardNo);
        }

        public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime,int KDate,int KNo,int KQty)
        {
                setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime,KDate,KNo,KQty);     
        }

        public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime)
        {
                setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime);     
        }

        public int getVehicleCardNo(String VCardNo,int vdate)
        {
                return setVehicleCardNo(VCardNo,vdate);
        } 

}


