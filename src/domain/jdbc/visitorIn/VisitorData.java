
package domain.jdbc.visitorIn;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;
import java.io.*;

public class VisitorData extends DataManager
{
     public VisitorData()
     {

     }
     /*public Vector getFinanceYear()
     {
         return setFinanceYear();
     }*/

     public int getNextId()
     {
          return setNextId();
     }
     public void setMessageText(String str,int id)
     {
          try
          {
               FileWriter FW  = new FileWriter("d:/MailService/src/"+String.valueOf(id));
               FW.write(str);
               FW.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
     }
     public String getVisitorInitialCheck(String QS)
     {
          return setVisitorInitialCheck(QS);
     }
     public int getSlNo(String Seq)
     {
          String QS = "Select "+Seq+".nextVal from Dual";
          return setSlNo(QS);
     }
     public Vector getVisitorReport(int StDate,int EndDate)
     {
          return  setVisitorReport(StDate,EndDate);
     }
     public Vector getVisitorCheckReport(int StDate,int EndDate)
     {
          return  setVisitorCheckReport(StDate,EndDate);
     }

     public Vector getVisitorReport(int StDate,int EndDate,String SPurpose)
     {
          System.out.println("VisitorData : SPurpose "+SPurpose);
          return  setVisitorReport(StDate,EndDate,SPurpose);
     }

     public Vector getVisitorSlipNo(int StDate,int EndDate)
     {
          return  setVisitorSlipNo(StDate,EndDate);
     }

     public Vector getVisitorReport(int iSlipNo,int StDate,int EndDate)
     {
          return  setVisitorReport(iSlipNo,StDate,EndDate);
     }

     public Vector getVisitorReport1(int iSlipNo,int StDate,int EndDate)
     {
          return  setVisitorReport1(iSlipNo,StDate,EndDate);
     }


     public Vector getVisitorAbstractReport(int StDate,int EndDate,int CompanyCode,int CCode)
     {
          return  setVisitorAbstractReport(StDate,EndDate,CompanyCode,CCode);
     }
     public Vector getVisitorCompanyInfo()
     {
          return setVisitorCompanyInfo();
     }
     public Vector getErrectorCompanyInfo()
     {
	  return setErrectorCompanyInfo();
     }
     public Vector getVisitorInInfo(int sdate,int edate)
     {
          return setVisitorInInfo(sdate,edate);
     }
     public Vector getVisitorOutInfo(int sdate,int edate)
     {
          return setVisitorOutInfo(sdate,edate);
     }
     public Vector getErrectorWiseReport(int StDate,int EndDate,int iErrectorCode)
     {
          return  setErrectorWiseReport(StDate,EndDate,iErrectorCode);
     }
     
     public Vector getErrectorInfo(String SCompanyCode)
     {
          return setErrectorInfo(SCompanyCode);
     }

     public Vector getErrectorInInfo(int sdate,int edate)
     {
          return setErrectorInInfo(sdate,edate);
     }
     public Vector getErrectorOutInfo(int sdate,int edate)
     {
          return setErrectorOutInfo(sdate,edate);
     }
     public Vector getErrectorAsOnReport(int EndDate)
     {
          return  setErrectorAsOnReport(EndDate);
     }
     public Vector getErrectorAsOnReport1(int EndDate)
     {
          return  setErrectorAsOnReport1(EndDate);
     }

     public Vector getVisitorRepInfo(int CCode)
     {
          return setVisitorRepInfo(CCode);
     }

     public Vector getErrectorReport(int StDate,int EndDate)
     {
          return  setErrectorReport(StDate,EndDate);
     }
     public Vector getErrectorCheckReport(int StDate,int EndDate)
     {
          return  setErrectorCheckReport(StDate,EndDate);
     }

     public Vector getErrectorReport(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          return  setErrectorReport(iSlipNo,StDate,EndDate,sNewVersion);

     }
     public Vector getErrectorReport1(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          return  setErrectorReport1(iSlipNo,StDate,EndDate,sNewVersion);
     }

     public boolean isCardAlreadyExists(String SCardNo)
     {
          return isCardExists(SCardNo);
     }

     public Vector getCompanyInfo(int iVisitorType)
     {
	  String QS = "";
		 
		if(iVisitorType==1) // Visitor
		{
			 QS = " select  Code,Name from Company where DISPLAYSTATUS = 1 order by 2 ";
		}
		if(iVisitorType==2) // Errector
		{
			QS = " select PartyCode,PartyName from scm.PartyMaster order by 2 ";
		}
          return  setInfo(QS);
     }
     public Vector getPurposeInfo()
     {
          String QS = " Select code,name from VisitorPurpose where DisplayStatus=1 order by 2";
          return setInfo(QS);
     }
     public Vector getRepInfo(int iCode)
     {
          //String QS = " Select code,name from Representative where CompanyCode="+iCode+" order by 2";
		  String QS = " Select code,name from Representative where CompanyCode='"+iCode+"' and DISPLAYSTATUS = 1 order by 2";
          return setInfo(QS);
     }
	 public Vector getRepInfo(String SCode)
     {
          //String QS = " Select code,name from Representative where CompanyCode="+iCode+" order by 2";
		  String QS = " Select code,name from Representative where CompanyCode='"+SCode+"' and DISPLAYSTATUS = 1 order by 2";
          return setInfo(QS);
     }
     public Vector getDriverInfo(int iCode)
     {
          String QS = " Select code,name from OtherVehicleDrivers where CompanyCode="+iCode+" order by 2";
          return setInfo(QS);
     }

     public Vector getVehInfo(int iCode)
     {
          String QS = " Select code,RegNo from OtherVehicleInfo where CompanyCode="+iCode+" order by 1";
          return setInfo(QS);
     }

     public Vector getDeptInfo()
     {
          String QS = "Select DeptCode,DeptName from Department order by 2";
          return setInfo(QS);
     }
     public Vector getStaffInfo()
     {
          String QS = " Select EmpCode,EmpName from VisitorStaff order by 2";
          return setInfo(QS);
     }

     public String getMaxCode()
     {
          String QS = " Select max(code) from Company ";
          return setMaxCode(QS);
     }
     public String getMaxPurposeCode()
     {
          String QS = " Select max(code) from VisitorPurpose ";
          return setMaxCode(QS);
     }
     public String getMaxRegCode()
     {
          String QS = " Select max(code) from OtherVehicleInfo ";
          return setMaxCode(QS);
     }
     public String getMaxDriverCode()
     {
          String QS = " Select max(code) from OtherVehicleDrivers ";
          return setMaxCode(QS);
     }

     public String getMaxDeptCode()
     {
          String QS = " Select max(DeptCode) from Department ";
          return setMaxCode(QS);
     }

     public String getMaxRepCode()
     {
          String QS = " Select max(code) from Representative ";
          return setMaxCode(QS);
     }
     public String getMaxStaffCode()
     {
          String QS = " Select max(EmpCode) from VisitorStaff ";
          return setMaxCode(QS);
          //return "";
     }
     public String getMaxCompanyCode()
     {
          String QS1= " Select Max(Code) from Company ";
          return setMaxCode(QS1);
     }

     public void insertCompanyName(String SName,String code)
     {
         // String QS = " Insert into Company(Code,Name) values('"+code+"','"+SName+"') ";
   		  int  DisplayStatus= 1;
          String QS = " Insert into Company(Code,Name,DISPLAYSTATUS) values('"+code+"','"+SName+"','"+DisplayStatus+"') ";
          insertName(QS);
     }
     public void insertVisitorPurpose(String SName,String SCode)
     {
          int icode = Integer.parseInt(SCode); 
          String QS = " Insert into VisitorPurpose(Code,Name) values("+icode+",'"+SName+"') ";
          insertName(QS);
     }
     public void insertDept(int code,String SName)
     {
          String QS = " Insert into Department(DeptCode,DeptName) values("+code+",'"+SName+"') ";
          insertName(QS);
     }

     public void insertRep(String SName,String rCode)
     {    String seq  = " Select Rep_seq.nextVal from Dual ";
          int repCode = setSlNo(seq);
		  int  DisplayStatus= 1;
          //String QS = " Insert into Representative(Code,Name,CompanyCode,DISPLAYSTATUS) values("+repCode+",'"+SName+"','"+rCode+"') ";
		  String QS = " Insert into Representative(Code,Name,CompanyCode,DISPLAYSTATUS) values("+repCode+",'"+SName+"','"+rCode+"','"+DisplayStatus+"') ";
		 // System.out.println("Insert Rep==="+QS);
          insertName(QS);
     }
     public void insertReg(String SName,String rCode)
     {    String seq  = " Select Reg_seq.nextVal from Dual ";
          int repCode = setSlNo(seq);
          String QS = " Insert into OtherVehicleInfo(Code,RegNo,CompanyCode) values("+repCode+",'"+SName+"','"+rCode+"') ";
          insertName(QS);
     }
     public void insertDriver(String SName,String rCode)
     {    String seq  = " Select Driver_seq.nextVal from Dual ";
          int repCode = setSlNo(seq);
          String QS = " Insert into OtherVehicleDrivers(Code,Name,CompanyCode) values("+repCode+",'"+SName+"','"+rCode+"') ";
          insertName(QS);
     }

     public void insertStaff(int iCode,String SName)
     {
          String QS = " Insert into VisitorStaff(EmpCode,EmpName) values("+iCode+",'"+SName+"') ";
          insertName(QS);
     }

     public void saveData(String  slipNo,Vector vect)
     {
          storeData(slipNo,vect);  
     }
     public void saveErrectorData(String  slipNo,Vector vect)
     {
          storeErrectorData(slipNo,vect);  
     }

     public String getNextSlipNo(int iIndicator)
     {
          return setNextSlipNo(iIndicator);
     }
     public int getSlipNo(String cardNo)
     {
          return setSlipNo(cardNo);
     }
     public int getErrectorSlipNo(String cardNo)
     {
          return setErrectorSlipNo(cardNo);
     }
     public Vector getErrectorSlipNo(int iStDate,int iEndDate)
     {
          return setErrectorSlipNo(iStDate,iEndDate);
     }

     public void setOutTime(String STime,int RepCode)
     {
          updateOutTime(STime,RepCode);
     }
     public void setErrectorOutTime(String STime,int RepCode)
     {
          updateErrectorOutTime(STime,RepCode);
     }

     public Vector getVect(int slipNo)
     {
          return setVect(slipNo);
     }

     public Vector getErrectorVect(int slipNo)
     {
          return setErrectorVect(slipNo);
     }
     
     public int getDeptCode(int iStaffCode)
     {
          return setDeptCode(iStaffCode);
     }
     public String getHostId(int iDeptCode)
     {
          return setHostId(iDeptCode);
     }

     public int getHodCode(int iDeptCode)
     {
          return setHodCode(iDeptCode);
     }
     public Vector sendMessageSource(int id,RowSet rowSet)
     {
          String  MSG_SOURCE_COLUMNNAME[] = {"ID","UserCode","SubjectCode","MsgDate","MsgTime"};
          String  MSG_SOURCE_COLUMNTYPE[] = { "N","N","N","N","N"};

          rowSet.setColumnName(MSG_SOURCE_COLUMNNAME);
          rowSet.setColumnType(MSG_SOURCE_COLUMNTYPE);

//          System.out.println("Inside Messaege Source");
          return  setPrimaryRowSet(id,rowSet,"INSERTMSGSOURCE");
     }

     public void sendMessageTarget(RowSet rowSet)
     {
          String MSG_TARGET_COLUMNNAME[] = {"ID","TargetUserCode","Status"};
          String MSG_TARGET_COLUMNTYPE[] = {"N","N","N"};

          rowSet.setColumnName(MSG_TARGET_COLUMNNAME);
          rowSet.setColumnType(MSG_TARGET_COLUMNTYPE);

//          System.out.println("Inside MessageTarget");

          setSecondaryRowSet(rowSet,"INSERTMSGTARGET");
     }
     

}

