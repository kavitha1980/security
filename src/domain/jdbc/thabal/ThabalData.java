package domain.jdbc.thabal;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

import java.sql.*;

public class ThabalData extends DataManager
{
      public ThabalData()
      {

      }

      public Vector getThabalDespMode()
      {
            String QS = "Select Code,Name from thabaldespmode where code<=2 order by 1";
            return setThabalDespMode(QS);
      }
      public Vector getThabalType()
      {
            String QS = "Select Code,Name from thabaldesttype order by 1";
            return setThabalType(QS);
      }
      public Vector getDistinctCourier(String SDespModeCode,int iDate)
      {
            String QS = " Select Distinct thabaloutwardnew.DespatcherCode,thabalcourier.name from ThabalOutwardNew "+
                        " inner join thabalcourier on thabaloutwardnew.DespatcherCode=thabalcourier.code "+
                        " where thabaloutwardnew.outwarddate<="+iDate+
                        " and thabaloutwardnew.despmode="+SDespModeCode+
                        " and thabaloutwardnew.status=0 and thabaloutwardnew.updateflag=0 order by thabaloutwardnew.DespatcherCode";

            return setDistinctCourier(QS);
      }

      public Vector getThabalSummary(Vector VDespCode,Vector VTypeCode,int iDate)
      {
            return setThabalSummary(VDespCode,VTypeCode,iDate);
      }

      public void getUpdateThabalStatus(String SDespCode,int iDate)
      {
            String QS = " Update ThabalOutwardNew Set Status=1 where despatchercode="+SDespCode+" and outwarddate<="+iDate+" and updateflag=0";

            setUpdateThabalStatus(QS);
      }

      public Vector getCourierDetails()
      {
            String QS = "Select Code,Name from ThabalCourier where despmode=1 order by Name";

            return setCourierDetails(QS);
      }
      public Vector getMessengerDetails()
      {
            String QS = "Select Code,Name from ThabalCourier where despmode=2 order by Name";

            return setMessengerDetails(QS);
      }

      public Vector getGateThabalInward()
      {
            String QS = " Select thabaldespmode.name,thabalcourier.name,thabalgateinward.local,thabalgateinward.other,thabalgateinward.inter,thabalgateinward.parcel,thabalgateinward.totalthabal,thabalgateinward.slno,thabalcourier.code,thabalgateinward.indate "+
                        " From ThabalGateInward "+
                        " inner join thabaldespmode on thabalgateinward.despmode=thabaldespmode.code "+
                        " inner join thabalcourier on thabalgateinward.despatchercode=thabalcourier.code "+
                        " where thabalgateinward.status=0 and thabalgateinward.updateflag=0 "+
                        " and thabalgateinward.indate>=20060605 "+
                        " order by thabalgateinward.slno desc";

            return setGateThabalInward(QS);
      }
      public String getMaxThabalGateInward()
      {
            String QS = " Select max(SlNo) from thabalgateinward";

            return setMaxCode(QS);
      }
      public void insertThabalGateInward(Vector VValues)
      {

            String QS = " Insert into ThabalGateInward(SlNo,InDate,DespMode,DespatcherCode,Local,Other,Inter,Parcel,Totalthabal,UserCode,UserTime) values("+
                        ""+VValues.elementAt(0)+
                        ","+VValues.elementAt(1)+
                        ","+VValues.elementAt(2)+
                        ","+VValues.elementAt(3)+
                        ","+VValues.elementAt(4)+
                        ","+VValues.elementAt(5)+
                        ","+VValues.elementAt(6)+
                        ","+VValues.elementAt(7)+
                        ","+VValues.elementAt(8)+
                        ",'"+VValues.elementAt(9)+
                        "','"+VValues.elementAt(10)+"')";

            insertThabalGI(QS);
      }

      public String getThabalGateInwardStatus(String SNo)
      {
            String QS = " Select Status from thabalgateinward where slno="+SNo;

            return setMaxCode(QS);
      }

      public void getUpdateOldThabalGI(String SNo)
      {
            String QS = " Update ThabalGateInward Set updateflag=1 where slno="+SNo;

            setUpdateOldThabalGI(QS);
      }
}