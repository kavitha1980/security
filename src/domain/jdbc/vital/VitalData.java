/*
     The Collection of all Vital Data
     those are central for transactions.

     A client can access the content thro the three
     delegates.

          1) getData()
          2) findByIndex()
          3) findByPrimaryKey()

     Whereas accessing the data besides the above is limited,
     but is not a good practice.

     The above three delegates reduces the coding from the
     server,client and interfaces point of view drastically. 
*/

package domain.jdbc.vital;

import java.sql.*;
import java.util.*;

import util.*;
import java.awt.event.*;
import javax.swing.Timer;

import rndi.*;
import blf.*;

import domain.jdbc.*;

public class VitalData extends DataManager implements ActionListener
{
     protected Vector VVehRegNo,VVehicle;
     protected Vector VCompany,VCompCode;

     protected int    iServerDate=0;

     private javax.swing.Timer timer;

     Connection theConnection;
     
     public VitalData()
     {
          refreshMasterData();
     }

     public void refreshMasterData()
     {
          setVitalData();
     }

     public void refreshData()
     {
          setVitalData();
     }
     private void setVitalData()
     {
          try
          {
               if(theConnection == null)
               {
               JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
               theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               organizeVehicle(stat);
               organizeCompany(stat);
          }
          catch(Exception e){}
     }
     public int getServerDate()
     {
          return iServerDate;
     }

     private String getServerDateQS()
     {
          String QS = "Select to_number(to_char(Sysdate,'YYYYMMDD')) from dual";
          return QS;
     }
     private void organizeServerTime(Statement stat) throws Exception
     {
          ResultSet result = stat.executeQuery(getServerDateQS());

          if(result.next())
          {
               iServerDate = result.getInt(1);
          }

          result.close();
     }
     /*  Vehicle  Management */

     private void organizeVehicle(Statement stat) throws Exception
     {
          VVehRegNo      = new Vector();
          VVehicle       = new Vector();
          ResultSet result = stat.executeQuery(getVehicleQS());
          while(result.next())
          {
               VVehRegNo .addElement(result.getString(1));
               VVehicle  .addElement(result.getString(2));
          }
          result.close();
     }
     private String getVehicleQS()
     {
          String QS ="";
          QS = " Select VehicleRegNo,VehicleName from VehicleInfo where DisplayStatus=1 order by 2 ";
          return QS;
     }

     /* End of Vehicle Management */

     /*  Company  Management */

     private void organizeCompany(Statement stat) throws Exception
     {
          VCompany      = new Vector();
          VCompCode     = new Vector();
          ResultSet result = stat.executeQuery(getCompanyQS());
          while(result.next())
          {
               VCompCode .addElement(result.getString(1));
               VCompany  .addElement(result.getString(2));
          }
          result.close();
     }
     private String getCompanyQS()
     {
          String QS ="";
          QS = " Select Code,Name from Company order by 2 ";
          return QS;
     }

     /* End of Company Management */

     public Vector getData(String SAttribute)
     {
          try
          {
               if(SAttribute.equals("VEHICLEREGNO"))
                    return VVehRegNo;
               if(SAttribute.equals("VEHICLE"))
                    return VVehicle;

               if(SAttribute.equals("COMPANYCODE"))
                    return VCompCode;
               if(SAttribute.equals("COMPANY"))
                    return VCompany;
          }
          catch(Exception e){}
          System.out.println("Invalid Attribute "+SAttribute);
          return null;
     }
     public void runShedule()
     {
          timer = new javax.swing.Timer(1000,this);
          timer.setInitialDelay(0);
          timer.start();

     }
     public void actionPerformed(ActionEvent ae)
     {

          if(ae.getSource()==timer)
          {
               try
               {
                    setData();
               }
               catch(Exception ex)
               {
                    System.out.println(ex);
               }
          }
     }

     private void setData()
     {
          try
          {
               if(theConnection == null)
               {
                    JDBCConnection jdbc  = JDBCConnection.getJDBCConnection();
                    theConnection        = jdbc.getConnection();
               }
               Statement stat   = theConnection.createStatement();

               organizeServerTime(stat);

               stat.close();
          }                                    
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
}