package domain.jdbc.rawInfo;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

public class RawData extends DataManager
{
     public RawData()
     {

     }
     public void insertInwardData(RowSet rowset)
     {
          appendRowSet(rowset,"AGN","AGN_Seq","ID");
     }
     public void updateInwardData(RowSet rowSet,int iPK)
     {
          setRowSet(rowSet,"AGN","ID",String.valueOf(iPK));
     }
     
}
