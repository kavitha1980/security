/*
     A Singleton Class for getting the Connection
     object of a Database.

     Change this when you port in a different RDBMS
     environment

*/

package domain.jdbc;

import java.sql.*;

public class JDBCConnection1
{
     
     static JDBCConnection1 connect = null;

	Connection theConnection = null;

     String SDriver   = "oracle.jdbc.OracleDriver";
     //String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:amarml";
     String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SUser     = "hrdnew";
     String SPassword = "hrdnew";

     private JDBCConnection1()
	{
		try
		{
               Class.forName(SDriver);
               theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCConnection1 : "+e);
		}
	}

     public static JDBCConnection1 getJDBCConnection()
	{
		if (connect == null)
		{
               connect = new JDBCConnection1();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

