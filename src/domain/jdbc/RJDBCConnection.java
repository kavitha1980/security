/*
     A Singleton Class for getting the Connection
     object of a Database.

     Change this when you port in a different RDBMS
     environment

*/

package domain.jdbc;

import java.sql.*;

public class RJDBCConnection
{
     
     static RJDBCConnection connect = null;

	Connection theConnection = null;

     String SDriver   = "oracle.jdbc.OracleDriver";
     //String SDSN      = "jdbc:oracle:thin:@bulls:1521:amarml";
     String SDSN      = "jdbc:oracle:thin:@bulls:1521:arun";
     String SUser     = "SCM";
     String SPassword = "rawmat";

     private RJDBCConnection()
	{
		try
		{
               Class.forName(SDriver);
               theConnection = DriverManager.getConnection(SDSN,SUser,SPassword);
		}
		catch(Exception e)
		{
               System.out.println("JDBCConnection : "+e);
		}
	}

     public static RJDBCConnection getJDBCConnection()
	{
		if (connect == null)
		{
               connect = new RJDBCConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}

