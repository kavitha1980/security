
package domain.jdbc.master;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;
import java.io.*;

public class RentVehicleMasterData extends DataManager
{
     public RentVehicleMasterData()
     {

     }
     public String getMaxOwnerCode()
     {
          String QS= " Select Max(Code) from RentVehicleOwner ";
          return setMaxCode(QS);
     }
     public void insertOwnerName(String SName,String code)
     {
          String QS = " Insert into RentVehicleOwner(Code,Name) values('"+code+"','"+SName+"') ";
          insertName(QS);
     }
     public Vector getOwnerInfo()
     {
          String QS = " select  Code,Name from RentVehicleOwner order by 2 ";
          return  setInfo(QS);
     }
     public Vector getPurpose()
     {
          return setPurpose();
     }
     public String getMaxRVId()
     {
          String QS= " Select Max(ID) from RentVehicleInfo ";
          return setMaxCode(QS);
     }
     public void insertRentVehicle(Vector InfoVector)
     {
          saveRentVehicleData(InfoVector);  
     }
     public Vector getRVInfo(String SOwnerCode)
     {
          String QS = " select  VehicleRegNo,VehicleName,Id from RentVehicleInfo where OwnerCode="+SOwnerCode+" order by 2 ";
          return  setRVInfo(QS);
     }


}

