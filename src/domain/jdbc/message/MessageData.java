/*
          The Data Logic foundation of MESSAGE
          referenced in the Mail Server
*/

package domain.jdbc.message;

import java.io.*;

import java.util.*;
import domain.jdbc.*;
import util.RowSet;

public class MessageData extends DataManager
{
     public MessageData()
     {
     }
     public RowSet getMessages(int iUserCode) 
     {
          String QS = " Select MessageSource.Id,MailUser.UserName, "+
                      " Subject.SubjectName,MessageSource.MsgDate, "+
                      " MessageSource.MsgTime,MessageTarget.Status "+
                      " From ((MessageSource "+
                      " Inner Join MessageTarget On MessageSource.Id    = MessageTarget.Id) "+
                      " Inner Join MailUser On MessageSource.UserCode   = MailUser.UserCode) "+
                      " Inner Join Subject On MessageSource.SubjectCode = Subject.SubjectCode "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status < 3 ";

          return getRowSet(QS);
     }
     public RowSet getInfo(int Id)
     {
          String QS = " Select MessageSource.Id,MailUser.UserName, "+
                      " Subject.SubjectName,MessageSource.MsgDate, "+
                      " MessageSource.MsgTime,MessageTarget.Status "+
                      " From ((MessageSource "+
                      " Inner Join MessageTarget On MessageSource.Id    = MessageTarget.Id) "+
                      " Inner Join MailUser On MessageSource.UserCode   = MailUser.UserCode) "+
                      " Inner Join Subject On MessageSource.SubjectCode = Subject.SubjectCode "+
                      " Where MessageTarget.Id = "+Id+
                      " And MessageTarget.Status < 3 ";

          return getRowSet(QS);
     }     

     public RowSet getDeletedInfo(int Id)
     {
          String QS = " Select MessageSource.Id,MailUser.UserName, "+
                      " Subject.SubjectName,MessageSource.MsgDate, "+
                      " MessageSource.MsgTime,MessageTarget.Status "+
                      " From ((MessageSource "+
                      " Inner Join MessageTarget On MessageSource.Id    = MessageTarget.Id) "+
                      " Inner Join MailUser On MessageSource.UserCode   = MailUser.UserCode) "+
                      " Inner Join Subject On MessageSource.SubjectCode = Subject.SubjectCode "+
                      " Where MessageTarget.Id = "+Id+
                      " And MessageTarget.Status = 3 ";

          return getRowSet(QS);
     }     


     public RowSet getDeletedMessages(int iUserCode)
     {
          String QS = " Select MessageSource.Id,MailUser.UserName, "+
                      " Subject.SubjectName,MessageSource.MsgDate, "+
                      " MessageSource.MsgTime,MessageTarget.Status "+
                      " From ((MessageSource "+
                      " Inner Join MessageTarget On MessageSource.Id    = MessageTarget.Id) "+
                      " Inner Join MailUser On MessageSource.UserCode   = MailUser.UserCode) "+
                      " Inner Join Subject On MessageSource.SubjectCode = Subject.SubjectCode "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status = 3 ";

          return getRowSet(QS);
     }

     public RowSet getUnreadedMessages(int iUserCode)
     {
          String QS = " Select MessageSource.Id,MailUser.UserName, "+
                      " Subject.SubjectName,MessageSource.MsgDate, "+
                      " MessageSource.MsgTime,MessageTarget.Status "+
                      " From ((MessageSource "+
                      " Inner Join MessageTarget On MessageSource.Id    = MessageTarget.Id) "+
                      " Inner Join MailUser On MessageSource.UserCode   = MailUser.UserCode) "+
                      " Inner Join Subject On MessageSource.SubjectCode = Subject.SubjectCode "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status = 0 ";

          return getRowSet(QS);
     }

     public int countNewMessages(int iUserCode)
     {
          String QS = " Select Count(*) From MessageTarget "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status=0";
     
          return getIntValue(QS);
     }

     public int countReadMessages(int iUserCode) 
     {
          String QS = " Select Count(*) From MessageTarget "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status=2";

          return getIntValue(QS);
     }

     public int countUnReadMessages(int iUserCode)
     {
          String QS = " Select Count(*) From MessageTarget "+
                      " Where MessageTarget.TargetUserCode = "+iUserCode+
                      " And MessageTarget.Status=1";

          return getIntValue(QS);
     }

     public int getStatus(int Id)
     {
          String QS= "Select Status from MessageTarget where id ="+Id;
          return getIntValue(QS);
     }

     public Vector sendMessageSource(RowSet rowSet)
     {
          String  MSG_SOURCE_COLUMNNAME[] = {"ID","UserCode","SubjectCode","MsgDate","MsgTime"};
          String  MSG_SOURCE_COLUMNTYPE[] = { "N","N","N","N","N"};

          rowSet.setColumnName(MSG_SOURCE_COLUMNNAME);
          rowSet.setColumnType(MSG_SOURCE_COLUMNTYPE);

          return  setPrimaryRowSet(rowSet,"INSERTMSGSOURCE");
     }

     public void sendMessageTarget(RowSet rowSet)
     {
          String MSG_TARGET_COLUMNNAME[] = {"ID","TargetUserCode","Status"};
          String MSG_TARGET_COLUMNTYPE[] = {"N","N","N"};

          rowSet.setColumnName(MSG_TARGET_COLUMNNAME);
          rowSet.setColumnType(MSG_TARGET_COLUMNTYPE);

          setSecondaryRowSet(rowSet,"INSERTMSGTARGET");
     }

     /*
          Reading the Message from a text-file
          coz., the message is being stored as
          an ASCII file at the server.
     */

     public String getMessageText(int id)
     {
          String str="";
          try
          {
               Reader in = new FileReader("d:/selvaraj2/Mail/domain/"+String.valueOf(id));
               char[] buff = new char[4096];
               int nch;                                                                 
               while ((nch = in.read(buff, 0, buff.length)) != -1)
               {
                   str = str+new String(buff, 0, nch);
               }
          }
          catch(Exception ex)
          {
               System.out.println("Unable to Read From the File : "+ex.getMessage());
          }
          return str;
     }

     public RowSet getSubjects()
     {
          String QS = "Select subjectcode,subjectname from subject order by 2";
          return getRowSet(QS);
     }

     public void setMessageText(String str ,int id)
     {
          try
          {
               FileWriter fw = new FileWriter("d:/selvaraj2/Mail/Domain/"+id);

               fw.write(str);
          
               fw.close();
          }
          catch(Exception ex)
          {
          }
     }

     public void setStatus(int iUserCode,int iId)
     {
          String QS = " Update MessageTarget set Status = 1"+
                      " Where  TargetUserCode = "+ iUserCode +
                      " and id ="+ iId ;

          setUpdateStatus(QS);
     }

     public void setDeleteStatus(int iUserCode,int iId)
     {
          String QS = " Update MessageTarget set Status = 3"+
                      " Where  TargetUserCode = "+ iUserCode +
                      " and id ="+ iId ;

          setUpdateStatus(QS);
     }
     public void setDeletePermanentStatus(int iUserCode,int iId)
     {
          String QS = " Update MessageTarget set Status = 4"+
                      " Where  TargetUserCode = "+ iUserCode +
                      " and id ="+ iId ;

          setUpdateStatus(QS);
     }
     

}

