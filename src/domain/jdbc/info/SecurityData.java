package domain.jdbc.info;

import domain.jdbc.*;
import util.RowSet;
import java.util.*;

public class SecurityData extends DataManager
{
     public SecurityData()
     {

     }
     public int getInterviewCard(String sl)
     {
          return setInterviewCard(sl);
     }
     public int getGuestUpdateSlipNo(String sl)
     {
          return setGuestUpdateSlipNo(sl);
     }
     public int getCheckCard(String cardNo,int iDate)
     {
          return setCheckCard(cardNo,iDate);
     }   

     public int getInterviewInitialCheck(String QS)
     {
          return setInitialCheck(QS);
     }
     public int getGuestInitialCheck(String card)
     {
          return setGuestInitialCheck(card);
     }

     public int getSlipNo()
     {
          //String QS = "select Slip_seq.nextVal from Dual";
          String QS  = "Select Max(SlipNo) from InterviewNames";
          return setSlip(QS);
     }

     public Vector getGuestName()
     {
          return setGuestName();          
     }
     public Vector getCategory()
     {
          String QS = " select  CategoryName from Category Union All select DesignationName from Designation ";
          return  getVect(QS);
     }
     public Vector getWarden()
     {
         String   QS =  "select empname from staff where designationcode = 117" ;
         return getVect(QS);
     }

/*     public Vector getToMeetCode()
     {
          String QS =    "select SchemeApprentice.EmpName,"+
                         "SchemeApprentice.FatherName,"+
                         "SchemeApprentice.MotherName,"+
                         "SchemeApprentice.Address4,"+
                         "SchemeApprentice.DeptCode,"+
                         "hostel.hostelname,"+
                         "from SchemeApprentice"+
                         "inner join Hostel on"+
                         "SchemeApprentice.hostelcode= hostel.hostelcode"+
                         "UNION ALL"+
                         "Select ContractApprentice.EmpName,"+
                         "ContractApprentice.FatherName,"+
                         "ContractApprentice.MotherName,"+
                         "ContractApprentice.Address4,"+
                         "ContractApprentice.DeptCode,"+
                         "Hostel.hostelname,"+
                         "from ContractApprentice"+
                         "inner join Hostel on"+
                         "ContractApprentice.hostelcode = hostel.hostelcode";
                         
          return getToMeet(QS);
     }
*/
     public Vector getToMeetCode()
     {
          String QS =" select schemeapprentice.empname,schemeapprentice.fathername,"+
                     " schemeapprentice.mothername,schemeapprentice.address4,"+
                     " schemeapprentice.deptcode,hostel.hostelname,SchemeApprentice.HRDTicketNo,SchemeApprentice.EmpCode "+
                     " from schemeapprentice inner join hostel on"+
                     " schemeapprentice.hostelcode=hostel.hostelcode"+
                     " union all "+
                     " Select contractapprentice.empname,"+
                     " contractapprentice.fathername,"+
                     " contractapprentice.mothername,contractapprentice.address4,"+
                     " Contractapprentice.deptcode,hostel.hostelname,ContractApprentice.HRdTicketNo,ContractApprentice.EmpCode "+
                     " from contractapprentice inner join hostel on "+
                     " contractapprentice.hostelcode=hostel.hostelcode  "+
                     " Union All "+
                     " Select Staff.EmpName,Staff.FatherName,staff.MotherName,"+
                     " Staff.Address4,Staff.DeptCode,hostel.hostelName,staff.HrdTicketNo,Staff.EmpCode "+
                     " from Staff inner join Hostel on "+
                     " Staff.HostelCode = Hostel.HostelCode order by 1 ";

          return getToMeet(QS);
     }

     public Vector getHodCode()
     {
          String QS = " select  DeptCode from Department ";
          return  getCode(QS);
     }

     public Vector getAgent()
     {
          String QS = " select  BrokerName from Broker order by BrokerName ";
          return  getVect(QS);
     }

     public Vector getHod()
     {
          String QS = " select DeptName from Department ";
          return  getVect(QS);
     }
     public Vector getQuali()
     {
          String QS = " select  EducationName from Education ";
          return  getVect(QS);
     }
     public Vector getCategoryCode()
     {
          String QS = " select  CategoryCode from Category ";
          return  getCode(QS);
     }

     public Vector getAgentCode()
     {
          String QS = " select  BrokerCode from Broker ";
          return  getCode(QS);
     }

     public Vector getQualiCode()
     {
          String QS = " select  EducationCode from Education ";
          return  getCode(QS);
     }

     public int getId()
     {
          String  QS = "Select interview_seq.nextval from Dual";
          return getNextId(QS);
     }

     public int setCode(int code , Vector vect)
     {
          return setComboCode(code,vect);
     }

     public Vector initValues(int Date)
     {
          return setInitValues(Date);
     }

     public Vector initValues(int FromDate,int ToDate)
     {
          System.out.println("Security Data-> From : "+FromDate);
          System.out.println("Security Data-> To   : "+ToDate);

          return setInitValues(FromDate,ToDate);
     }          

     public Vector getNames()
     {
          return getName();
     }

     public Vector getEmployeeNames()
     {
          return getEmpName();
     }

     public Vector getValues(String slno)
     {
          return setData(slno);
     }

     public Vector getOutValues(int IDate)
     {
          return setOutValues(IDate);
     }

     public Vector getInterviewValues(int IDate1,int IDate2)
     {
          System.out.println("in securityData - Date1: "+IDate1);
          System.out.println("in securityData - Date2: "+IDate2);

          return setInterviewValues(IDate1,IDate2);
     }

     public Vector getReportVector(int iStDate ,int iEndDate)
     {
          return setReportVector(iStDate,iEndDate);
     }

     public Vector getAsOnReportVector(int iStDate ,int iEndDate)
     {
          return setAsOnReportVector(iStDate,iEndDate);
     }

     public Vector getInsideInterviewReportVector(int iStDate ,int iEndDate)
     {
          return setInsideInterviewReportVector(iStDate,iEndDate);
     }

     public Vector getReportCheckVector(int iStDate ,int iEndDate)
     {
          return setReportCheckVector(iStDate,iEndDate);
     }

     public Vector getReportVector1(int iStDate ,int iEndDate,String SResult,String SCatogory,String SAgent,String SPlace,String SQualification)
     {
          return setReportVector1(iStDate,iEndDate,SResult,SCatogory,SAgent,SPlace,SQualification);
     }

     public int setId(String sl)
     {
          return getId(sl);
     }

     public void getUpdateTimeAndDate(String SOutTime,int iOutDate,int Iid,int iResult,String SHod)
     {
          setUpdateTimeAndDate(SOutTime,iOutDate,Iid,iResult,SHod);
     }
      

     public Vector getGuestNames()
     {
          return setGuestNames();
     }

     public Vector getGuestInitValues(int Date)
     {
          return setGuestInitValues(Date);
     }          

     public Vector getGuestValues(String slno)
     {
          return setGuestData1(slno);
     }

     public void setGuestUpdateTime(int slno,String SOutTime,int iOutDate,String InTime)
     {
          GuestUpdateTime(slno,SOutTime,iOutDate,InTime);
     }

     public int getGuestSlipNo()
     {
          String QS = "Select Max(slipno) from GuestTable";
          return setGuestSlipNo(QS);
     }

     public Vector getGuestInInitValues(int Date)
     {
          return setGuestInInitValues(Date);
     }

     public void getGuestInsertData(String QS)
     {
          setGuestInsertData(QS);
     }   


     public void insertValues(Vector vect)
     {
          insertDetails(vect);
     }

/*     public int getMaxFoodId()
     {
          return getMaxFoodId();
     } */

}

