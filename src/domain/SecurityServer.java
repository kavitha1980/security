/*
          The RMI server that implements all
          the business methods of the system
          as defined in the business logic foundation package.

          Where as it simply delegates the data logic
          to appropriate data objects

               SecurityData
*/

package domain;

import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;

import java.sql.*;

import util.RowSet;


import blf.Info;
import blf.VisitorIn;
import blf.VehicleInfo;
import blf.RawInfo;
import blf.vital;
import blf.Key;
import blf.ThabalInfo;
import blf.RentVehicleInfo;
import blf.RentVehicleMasterInfo;

import domain.jdbc.info.*;
import domain.jdbc.visitorIn.*;
import domain.jdbc.vehicleInfo.*;
import domain.jdbc.rawInfo.*;
import domain.jdbc.vital.*;
import domain.jdbc.key.*;
import domain.jdbc.master.*;
import domain.jdbc.rentVehicleInfo.*;
import domain.jdbc.thabal.*;

import java.util.*;

public class SecurityServer extends UnicastRemoteObject implements VehicleInfo,Info,VisitorIn,RawInfo,vital,Key,RentVehicleMasterInfo,RentVehicleInfo,ThabalInfo,rndi.CodedNames
{
     SecurityData    theData;
     VisitorData     VisData;
     //StoresData stData;
     VehicleData vData;
     RawData     rData;
     VitalData   vital;
     KeyData     keyData;
     RentVehicleMasterData mData;
     RentVehicleData rvData;
     ThabalData  tData;


     public SecurityServer() throws RemoteException
     {
          theData    = new SecurityData();
          System.out.println("Security Server Starts...");
          vital      = new VitalData();
          System.out.println("Vital Data is Ready");
          VisData     = new VisitorData();    
          System.out.println("Visitor Data is Ready");
          vData      = new VehicleData();
          System.out.println("Vehicle Data is Ready");
          keyData      = new KeyData();
          System.out.println("Key Data is Ready");
          mData      = new RentVehicleMasterData();
          System.out.println("RentVehicleMasterData is Ready");
          rvData     = new RentVehicleData();
          System.out.println("RentVehicleData is Ready");
          tData      = new ThabalData();
          System.out.println("Thabal Data is Ready");
          //stData     = new StoresData();
          rData      = new RawData();    


          System.out.println("Security Server Started and Ready to Use");
     }
     public Vector getData(String SAttribute) throws RemoteException
     {
          return vital.getData(SAttribute);
     }

     /* ------------------key information-------------*/

     public Vector getKeyInformation(String SCardNo)
     {
          return keyData.getKeyInformation(SCardNo);
     }
     public void insertKeyInformation(Vector VInfo)
     {
          keyData.insertKeyInformation(VInfo);
     }
     public Vector getUserInformation()
     {
          return keyData.getUserInformation();
     }

     /* ------------------End of key information-------------*/

     public void insertInwardData(RowSet rowset) throws RemoteException
     {
          rData.insertInwardData(rowset);
     }
     public int getNextId()
     {
          return VisData.getNextId();
     }

     public void updateInwardData(RowSet rowset,int p) throws RemoteException
     {
          rData.updateInwardData(rowset,p);
     }

     public int getCheckCardStatus(String CardNo)
     {
          return vData.getCheckCardStatus(CardNo);
     }
     public String getVisitorInitialCheck(String QS)
     {
          return VisData.getVisitorInitialCheck(QS);
     }
     public int getHodCode(int iDeptCode) throws RemoteException
     {
          return VisData.getHodCode(iDeptCode);
     }
     public String getHostId(int iDeptCode) throws RemoteException
     {
          return VisData.getHostId(iDeptCode);
     }

     public int getDeptCode(int iStaffCode)
     {
          return VisData.getDeptCode(iStaffCode);
     }
     public boolean isCardAlreadyExists(String SCardNo)
     {
          return VisData.isCardAlreadyExists(SCardNo);
     }
     public Vector sendMessageSource(int id,RowSet rowset)
     {
          return VisData.sendMessageSource(id,rowset);
     }
     public void setMessageText(String str,int id)
     {
          VisData.setMessageText(str,id);
     }

     public void sendMessageTarget(RowSet rowset)
     {
          VisData.sendMessageTarget(rowset);
     }
     public int getGuestInitialCheck(String Card) throws RemoteException
     {
          return theData.getGuestInitialCheck(Card);
     }
     public int getInterviewInitialCheck(String QS) throws RemoteException
     {
          return theData.getInterviewInitialCheck(QS);
     }
     public int getCheckCard(String Card,int date) throws RemoteException
     {
          return theData.getCheckCard(Card,date);
     }
     public int getGuestUpdateSlipNo(String Card) throws RemoteException
     {
          return theData.getGuestUpdateSlipNo(Card);
     }
     public int getVehicleCardNo(String VCard,int vdate) throws RemoteException
     {
          return vData.getVehicleCardNo(VCard,vdate);
     }
     public Vector getVehicleInInfo(String CardNo)
     {
          return vData.setVehicleInInfo(CardNo);
     }
     public Vector getVehicleModelInfo(int InDate)
     {
          return vData.getVehicleModelInfo(InDate);
     }
     public Vector getVehicleData()
     {
          return vData.getVehicleData();
     }
     public Vector getVehicleNames()
     {
          return vData.getVehicleNames();
     }
     public Vector getGuestName()
     {
          return theData.setGuestName();
     }
     public ResultSet getVehicleReport(String QS)
     {
          return vData.getVehicleReport(QS);
     }
     public ResultSet getVehicleFuelReport(String QS)
     {
          return vData.getVehicleFuelReport(QS);
     }
     public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime,int KDate,int KNo,int KQty)
     {
          vData.setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime,KDate,KNo,KQty);     
     }

     public void getVehicleUpdate(String VCardNo,String SInTime,int IInDate,int EndKm,String SOutTime)
     {
          vData.setVehicleUpdate(VCardNo,SInTime,IInDate,EndKm,SOutTime);     
     }

     public void updateVehicleKm(String CardNo,int endkm)  throws RemoteException
     {
          vData.updateVehicleKm(CardNo,endkm);
     }   
     public Vector getKm(String CardNo)   throws RemoteException
     {
          return  vData.getKm(CardNo);
     }

     public Vector getSecurityInfo()   throws RemoteException
     {
          return vData.getSecurityInfo();
     } 
     public Vector getPlaces()   throws RemoteException
     {
          return vData.getPlaces();
     } 

     /*
     public Vector getFinanceYear()  throws RemoteException
     {
          return VisData.getFinanceYear();
     }
     */

     public Vector getCurrentInfo(int Date) throws RemoteException
     {
          return vData.getCurrentInfo(Date);
     }
                 
     public Vector getCurrentInInfo(int Date) throws RemoteException
     {
          return vData.getCurrentInInfo(Date);
     }
                 
     public void insertData(Vector vect) throws RemoteException
     {
          vData.insertData(vect);
     }
     //Insert Vehicle Movement Data
     public void insertVehicleDetailsData(Vector vect) throws RemoteException
     {
          vData.insertData(vect);
     }


     public void insertVData(Vector vect) throws RemoteException
     {
          vData.insertData(vect);
     }
     public Vector getDriverInfo() throws RemoteException
     {
          return vData.getDriverInfo();
     }
     public Vector getInfo(String CardNo,int i) throws RemoteException
     {
          return vData.getInfo(CardNo,i);
     }
                                     

     public Vector getPurpose() throws RemoteException
     {
          return vData.getPurpose();
     }

     public int getSlipNo() throws RemoteException
     {
          return theData.getSlipNo();
     }

     public int getInterviewCard(String sl) throws RemoteException
     {
          return theData.getInterviewCard(sl);
     }

     public Vector getCategory() throws RemoteException
     {
          return theData.getCategory();
     }

     public Vector getWarden() throws RemoteException
     {
         return theData.getWarden();
     }               



/*     public Vector getHostelCode() throws RemoteException
     {
          return theData.getHostelCode();
     }
*/
     public Vector getToMeetCode()
     {
          return theData.getToMeetCode();
     }
     public Vector getHod() throws RemoteException
     {
          return theData.getHod();
     }
     public Vector getGuestInitValues(int Date)
     {
          return theData.setGuestInitValues(Date);
     }
     public int getGuestSlipNo() throws   RemoteException
     {
          return theData.getGuestSlipNo();
     }
     public Vector getGuestNames()
     {
          return theData.getGuestNames();
     }
     public Vector getGuestInInitValues(int Date)
     {
          return theData.setGuestInInitValues(Date);
     }
     public Vector getGuestValues(String slno)
     {
          return theData.getGuestValues(slno);
     }
     public void setGuestUpdateTime(int slno,String SOutTime,int iOutDate,String InTime)
     {
          theData.setGuestUpdateTime(slno,SOutTime,iOutDate,InTime);
     }
     public void getGuestInsertData(String QS)
     {
          theData.getGuestInsertData(QS);
     }

     public Vector getAgent() throws RemoteException
     {
          return theData.getAgent();
     }
     public Vector getQuali() throws RemoteException
     {
          return theData.getQuali();
     }
     public int getId() throws   RemoteException
     {
          return theData.getId();
     }
     public Vector getCategoryCode() throws RemoteException
     {
          return theData.getCategoryCode();
     }
     public Vector getAgentCode() throws RemoteException
     {
          return theData.getAgentCode();
     }
     public Vector getHodCode() throws RemoteException
     {
          return theData.getHodCode();
     }
     public Vector getQualiCode() throws RemoteException
     {
          return theData.getQualiCode();
     }

     public int setCode(int code , Vector vect)
     {
          return theData.setCode(code,vect);
     }
     public Vector initValues(int Date)
     {
          return theData.setInitValues(Date);
     }
     public Vector initValues(int FromDate,int ToDate)
     {
          System.out.println("Security Server-> From : "+FromDate);
          System.out.println("Security Server-> To   : "+ToDate);
          return theData.setInitValues(FromDate,ToDate);
     }
     public void insertValues(Vector infoVector)
     {
          theData.insertValues(infoVector);
     }                   
     public Vector getNames()
     {
          return theData.getNames();
     }
     public Vector getEmployeeNames()
     {
          return theData.getEmployeeNames();
     }
     public Vector getValues(String slno)
     {
          return theData.getValues(slno);
     }
     public Vector getOutValues(int iDate)
     {
          return theData.getOutValues(iDate);
     }
     public Vector getInterviewValues(int iDate1,int iDate2)
     {
          return theData.getInterviewValues(iDate1,iDate2);
     }
     public Vector getReportVector(int iStDate,int iEndDate)
     {
          return theData.getReportVector(iStDate,iEndDate);
     }
     public Vector getAsOnReportVector(int iStDate,int iEndDate)
     {
          return theData.getAsOnReportVector(iStDate,iEndDate);
     }

     public Vector getInsideInterviewReportVector(int iStDate,int iEndDate)
     {
          return theData.getInsideInterviewReportVector(iStDate,iEndDate);
     }

     public Vector getReportCheckVector(int iStDate,int iEndDate)
     {
          return theData.getReportCheckVector(iStDate,iEndDate);
     }

     public Vector getReportVector1(int iStDate,int iEndDate,String SResult,String SCatogory,String SAgent,String SPlace,String SQualification)
     {
          return theData.getReportVector1(iStDate,iEndDate,SResult,SCatogory,SAgent,SPlace,SQualification);
     }

     public void getUpdateTimeAndDate(String SOutTime,int iOutDate,int id,int iResult,String SHod)
     {
          theData.getUpdateTimeAndDate(SOutTime,iOutDate,id,iResult,SHod);
     }
/*     public int getMaxFoodId()
     {
          return theData.getMaxFoodId();
     } */

     public int setId(String sl)
     {
          return theData.setId(sl);
     }
     public void insertCompanyName(String name,String Code)
     {
          VisData.insertCompanyName(name,Code);
     }
     public void insertVisitorPurpose(String name,String Scode)
     {
          VisData.insertVisitorPurpose(name,Scode);
     }
     public void insertDept(int Code,String name)
     {
          VisData.insertDept(Code,name);
     }

     public void insertRep(String name,String code)
     {
          VisData.insertRep(name,code);
     }
     public void insertReg(String name,String code)
     {
          VisData.insertReg(name,code);
     }
     public void insertDriver(String name,String code)
     {
          VisData.insertDriver(name,code);
     }

     public void insertStaff(int Code,String name)
     {
          VisData.insertStaff(Code,name);
     }
     public String getMaxCode()
     {
          return VisData.getMaxCode();
     }          
     public String getMaxDeptCode()
     {
          return VisData.getMaxDeptCode();
     }          

     public String getMaxPurposeCode()
     {
          return VisData.getMaxPurposeCode();
     }          
     public String getMaxRepCode()
     {
          return VisData.getMaxRepCode();
     }          
     public String getMaxRegCode()
     {
          return VisData.getMaxRegCode();
     }          
     public String getMaxDriverCode()
     {
          return VisData.getMaxDriverCode();
     }          

     public String getMaxStaffCode()
     {
          return VisData.getMaxStaffCode();
     }          
     public String getMaxCompanyCode()
     {
          return VisData.getMaxCompanyCode();
     }          

     public Vector getCompanyInfo(int iVisitorType)
     {
          return VisData.getCompanyInfo(iVisitorType);
     }
     public Vector getPurposeInfo()
     {
          return VisData.getPurposeInfo();
     }
     public Vector getStaffInfo()
     {
          return VisData.getStaffInfo();
     }
     public Vector getDeptInfo()
     {
          return VisData.getDeptInfo();
     }

     public Vector getRepInfo(int code)
     {
          return VisData.getRepInfo(code);
     }
	 public Vector getRepInfo(String scode)
     {
          return VisData.getRepInfo(scode);
     }
     public Vector getDriverInfo(int code)
     {
          return VisData.getDriverInfo(code);
     }
                                  
     public Vector getVehInfo(int code)
     {
          return VisData.getVehInfo(code);
     }

     public void saveData(String slipno,Vector vect)
     {
          VisData.saveData(slipno,vect);
     }
     public void saveErrectorData(String slipno,Vector vect)
     {
          VisData.saveErrectorData(slipno,vect);
     }

     public String getNextSlipNo(int iIndicator)
     {
          return VisData.getNextSlipNo(iIndicator);
     }
     public int getSlipNo(String CardNo)
     {
          return VisData.getSlipNo(CardNo);
     }                          
     public Vector getErrectorInfo(String SCompanyCode)
     {
          return VisData.setErrectorInfo(SCompanyCode);
     }
     public int getErrectorSlipNo(String CardNo)
     {
          return VisData.getErrectorSlipNo(CardNo);
     }                          
     public Vector getErrectorSlipNo(int iStDate,int iEndDate)
     {
          return VisData.getErrectorSlipNo(iStDate,iEndDate);
     }                          

     public int getSlNo(String Seq)
     {
          return VisData.getSlNo(Seq);
     }

     public Vector getVect(int slipno)
     {
          return VisData.getVect(slipno);
     }
     public Vector getErrectorVect(int slipno)
     {
          return VisData.getErrectorVect(slipno);
     }

     public Vector getErrectorAsOnReport(int EndDate)
     {
          return VisData.getErrectorAsOnReport(EndDate);
     }
     public Vector getErrectorAsOnReport1(int EndDate)
     {
          return VisData.getErrectorAsOnReport1(EndDate);
     }

     public Vector getVisitorReport(int StDate,int EndDate)
     {
          return VisData.getVisitorReport(StDate,EndDate);
     }
     public Vector getVisitorCheckReport(int StDate,int EndDate)
     {
          return VisData.getVisitorCheckReport(StDate,EndDate);
     }

     public Vector getVisitorReport(int StDate,int EndDate,String SPurpose)
     {
          return VisData.getVisitorReport(StDate,EndDate,SPurpose);
     }

     public Vector getVisitorReport(int iSlipNo,int StDate,int EndDate)
     {
          return VisData.getVisitorReport(iSlipNo,StDate,EndDate);
     }
     public Vector getVisitorReport1(int iSlipNo,int StDate,int EndDate)
     {
          return VisData.getVisitorReport1(iSlipNo,StDate,EndDate);
     }

     public Vector getVisitorSlipNo(int StDate,int EndDate)
     {
          return VisData.getVisitorSlipNo(StDate,EndDate);
     }

     public Vector getVisitorAbstractReport(int StDate,int EndDate,int CompanyCode,int CCode)
     {
          return VisData.getVisitorAbstractReport(StDate,EndDate,CompanyCode,CCode);
     }
     public Vector getVisitorInInfo(int StDate,int EndDate)
     {
          return VisData.getVisitorInInfo(StDate,EndDate);
     }
     public Vector getVisitorOutInfo(int StDate,int EndDate)
     {
          return VisData.getVisitorOutInfo(StDate,EndDate);
     }
     public Vector getErrectorInInfo(int StDate,int EndDate) 
     {
          return VisData.getErrectorInInfo(StDate,EndDate);
     }
     public Vector getErrectorOutInfo(int StDate,int EndDate)
     {
          return VisData.getErrectorOutInfo(StDate,EndDate);
     }
     public Vector getErrectorWiseReport(int StDate,int EndDate,int iErrectorCode)
     {
          return VisData.getErrectorWiseReport(StDate,EndDate,iErrectorCode);
     }
     public Vector getVisitorCompanyInfo()
     {
          return VisData.getVisitorCompanyInfo();
     }
     public Vector getErrectorCompanyInfo()
     {
	return VisData.getErrectorCompanyInfo();
     }
     public Vector getVisitorRepInfo(int CCode)
     {
          return VisData.getVisitorRepInfo(CCode);
     }

     public Vector getErrectorReport(int StDate,int EndDate)
     {
          return VisData.getErrectorReport(StDate,EndDate);
     }
     public Vector getErrectorCheckReport(int StDate,int EndDate)
     {
          return VisData.getErrectorCheckReport(StDate,EndDate);
     }

     public Vector getErrectorReport(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          return VisData.getErrectorReport(iSlipNo,StDate,EndDate,sNewVersion);
     }

     public Vector getErrectorReport1(int iSlipNo,int StDate,int EndDate,String sNewVersion)
     {
          return VisData.getErrectorReport1(iSlipNo,StDate,EndDate,sNewVersion);
     }

     /*public Vector getInvName()
     {
         return stData.getInvName();
     }*/
     /*public Vector getInvCode()
     {
         return stData.getInvCode();
     }*/
     
     /*public Vector getPendingNames(String QS)
     {
          return stData.getPendingNames(QS);
     }*/
     /*public Vector getSupplierData(String QS)
     {
          return stData.getSupplierData(QS);
     }*/
     /*public Vector getListVector(String QS)
     {
          return stData.getListVector(QS);
     }*/
     /*public Vector getRDCData(String QS)
     {
          return stData.getRDCData(QS);
     }*/
     /*public Vector getRDCOutData(String QS)
     {
          return stData.getRDCOutData(QS);
     }*/
     
     /*public void insertGISDetails(String QS)
     {
          stData.insertGISDetails(QS);
     }

     public void insertGIDSDetails(String QS)
     {
          stData.insertGIDSDetails(QS);
     }

     public void insertRDCInData(String QS)
     {
          stData.insertRDCInData(QS);
     }
     public void updateRDC(String QS)
     {
          stData.updateRDC(QS);
     }*/
     /*public void setUpdateStoresData(String no,String column)
     {
          stData.setUpdateStoresData(no,column);
     }*/

     /*public int getNo(String QS)
     {
          return stData.getNo(QS);
     }*/

     public void setOutTime(String STime,int RepCode)
     {
          VisData.setOutTime(STime,RepCode);
     }
     public void setErrectorOutTime(String STime,int RepCode)
     {
          VisData.setErrectorOutTime(STime,RepCode);
     }
     public void updateFuelData(String SVehicleNo,int iStKm,int iFuelKm)
     {
          vData.updateFuelData(SVehicleNo,iStKm,iFuelKm);
     }

/*
    ----- Methods for Rental Vehicle Master -------
        (except getPurpose and getPlaces)
*/



     public String getMaxOwnerCode()
     {
          return mData.getMaxOwnerCode();
     }          
     public void insertOwnerName(String name,String Code)
     {
          mData.insertOwnerName(name,Code);
     }
     public Vector getOwnerInfo()
     {
          return mData.getOwnerInfo();
     }
     public String getMaxRVId()
     {
          return mData.getMaxRVId();
     }          
     public void insertRentVehicle(Vector InfoVector)
     {
          mData.saveRentVehicleData(InfoVector);
     }
     public Vector getRVInfo(String SOwnerCode)
     {
          return mData.getRVInfo(SOwnerCode);
     }

     /* ----------End of Rental Vehicle Master information--------*/



/*
    ----- Methods for Rental Vehicle Transaction -------
        (except getPurpose and getPlaces)
*/


     public int getRVCheckCardAvailable(String CardNo)
     {
          return rvData.getRVCheckCardAvailable(CardNo);
     }
     public int getRVCheckCardStatus(String CardNo)
     {
          return rvData.getRVCheckCardStatus(CardNo);
     }
     public Vector getRentVehicleInfo(String CardNo) throws RemoteException
     {
          return rvData.getRentVehicleInfo(CardNo);
     }
     public void insertRVData(Vector vect) throws RemoteException
     {
          rvData.insertRVData(vect);
     }
     public Vector getRVCurrentInfo(int Date) throws RemoteException
     {
          return rvData.getRVCurrentInfo(Date);
     }
     public String getMaxRVTripNo(int iId)
     {
          return rvData.getMaxRVTripNo(iId);
     }          
     public Vector getRVVehicleOutInfo(String SCardNo) throws RemoteException
     {
          return rvData.getRVVehicleOutInfo(SCardNo);
     }
     public Vector getRVActiveID() throws RemoteException
     {
          return rvData.getRVActiveID();
     }
     public Vector getRVCurrentOutInfo(Vector VActiveId) throws RemoteException
     {
          return rvData.getRVCurrentOutInfo(VActiveId);
     }
     public void getRVVehicleUpdate(String SCardNo,String SOutTime,int iOutDate,int iEndKm,int iRunKm,String STripNo,String SId,int iRentStatus,String SLoad)
     {
          rvData.getRVVehicleUpdate(SCardNo,SOutTime,iOutDate,iEndKm,iRunKm,STripNo,SId,iRentStatus,SLoad);
     }


     /* ----------End of Rental Vehicle Transaction information--------*/





/*
    ----- Methods for Thabal Transaction -------
*/


     public Vector getThabalDespMode() throws RemoteException
     {
          return tData.getThabalDespMode();
     }

     public Vector getThabalType() throws RemoteException
     {
          return tData.getThabalType();
     }
     public Vector getDistinctCourier(String SDespModeCode,int iDate) throws RemoteException
     {
          return tData.getDistinctCourier(SDespModeCode,iDate);
     }
     public Vector getThabalSummary(Vector VDespCode,Vector VTypeCode,int iDate) throws RemoteException
     {
          return tData.getThabalSummary(VDespCode,VTypeCode,iDate);
     }
     public void getUpdateThabalStatus(String SDespCode,int iDate)
     {
          tData.getUpdateThabalStatus(SDespCode,iDate);
     }

     public Vector getCourierDetails() throws RemoteException
     {
          return tData.getCourierDetails();
     }
     public Vector getMessengerDetails() throws RemoteException
     {
          return tData.getMessengerDetails();
     }

     public Vector getGateThabalInward() throws RemoteException
     {
          return tData.getGateThabalInward();
     }
     public String getMaxThabalGateInward() throws RemoteException
     {
          return tData.getMaxThabalGateInward();
     }
     public void insertThabalGateInward(Vector VValues)
     {
          tData.insertThabalGateInward(VValues);
     }
     public String getThabalGateInwardStatus(String SNo) throws RemoteException
     {
          return tData.getThabalGateInwardStatus(SNo);
     }
     public void getUpdateOldThabalGI(String SNo)
     {
          tData.getUpdateOldThabalGI(SNo);
     }

     /* ---------------End of Thabal information--------*/




     public static void main(String arg[])
     {
          try
          {
               SecurityServer theSecurityServer = new SecurityServer();
               Registry theRegistry = LocateRegistry.createRegistry(RMIPORT);
               theRegistry.rebind(SECURITYDOMAIN,theSecurityServer);
          }
          catch(Exception ex)
          {
               System.out.println("SecurityServer : "+ex);
          }
     }
}

